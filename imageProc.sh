#!/bin/bash
# Start imageProc in debug mode using the classpath from Eclipse


CODEROOT="/home/specgui/code/imageProc"

ARGS=""
DELAY=0
DEBUG=false
DEBUGPORT=""
#DEBUGPORT="address=8007"
IMAGE_MEM="8G"
MAINCLASS="imageProc.app.ImageProcW7X"
ARGS=""
MAKECLASSPATH=false
while getopts "hdp:as:m:w:c" arg; do
  case $arg in
    h)
      echo "usage [-a(rchive write back)] [-p profile] [-s start sleep delay] [-m image memory e.g. 10G] [-d(ebug)] [-w JDWP debug port]"
      echo "If profile is 'by-host[:n]', then the n'th profile matching the given hostname is chosen. With no :n specificed, all matching profiles are started in the same JVM."
      echo "If w7x-load then W7XLoad is loaded as the source. Profiles in profiles/ path are searched for a matching ID." 
      echo "-a runs the writeBackSequential and all other parameters are interpreted for that: [-p path in DB] [-l (loop)] [-t (time range)]"
      exit 1
      ;;
    p)
      MAINCLASS="imageProc.app.ImageProcW7X_Profile"
      ARGS=$OPTARG
      echo "Loading profile $ARGS"
      ;;
    a)
      MAINCLASS="imageProc.app.WriteBackSequential"
      ARGS=$*
      echo "Loading Writeback with parameters: $ARGS" 
      break
      ;;
    s)
      DELAY=$OPTARG
      ;;
    m)
      IMAGE_MEM=$OPTARG
      ;;
    d)
      DEBUG=true
      ;;
    w)
      DEBUGPORT="address=${OPTARG}"
      ;;
    c)
      MAKECLASSPATH=true
  esac
done


#Environment for PrincetinInstruments cameras
if [ -f /etc/profile.d/picam.sh ]; then
	. /etc/profile.d/picam.sh
fi

#Environment for PCO cameras (PCO.SDK >= 2024)
if [ -f /etc/profile.d/pco.kaya-runtime.sh ]; then
	. /etc/profile.d/pco.kaya-runtime.sh
fi

if [ -f /etc/profile.d/pco.siso-runtime.sh ]; then
	. /etc/profile.d/pco.siso-runtime.sh
fi

if [ -d /opt/pco/pco.sdk/lib ]; then
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/pco/pco.sdk/lib/
fi

#environment for PCO Sensicam
if [ -f /opt/sensicam-2_02_11/pb525002.bit ]; then
	export PCOPB=/opt/sensicam-2_02_11/pb525002.bit
fi

#Environment for various local libraries, e.g. Optec filter wheel
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

#Environment for OceanOptics
export OOI_HOME=/opt/omniDriver-2.56/OOI_HOME/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OOI_HOME

#java arguments, take from 'ps -Af | grep java' while ImageProc is running
JAVA="/usr/lib/jvm/java-17-openjdk-amd64/bin/java"
VMARGS="-Xms100M -Xmx512M -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=${IMAGE_MEM} --add-opens java.base/java.nio=ALL-UNNAMED --add-exports java.base/jdk.internal.ref=ALL-UNNAMED"

if [ "$MAKECLASSPATH" = true ]; then
    echo "Generating classpath from workspace and maven..."
    ls -1d /home/specgui/code/imageProc/*/target/classes > classpath-workspace.txt 
    mvn dependency:build-classpath -Dmdep.outputFile=classpath-maven.txt
    cat classpath-workspace.txt classpath-maven.txt | tr '\n' ':' > classpath.txt
    exit
fi

if [ -f classpath.txt ]; then
   CLASSPATH=`cat classpath.txt` #create with mvn dependency:build-classpath -Dmdep.outputFile=classpath.txt
   CLASSPATH="${CLASSPATH}:$CODEROOT/ImageProc-w7x/target/classes"
else
    echo  "classpath.txt does not exist. Run imageProc.sh -c"
    exit
fi

if [ "$DEBUG" = true ]; then
  DEBUGARGS="-XX:+ShowCodeDetailsInExceptionMessages -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,$DEBUGPORT"
  AGENTJAR=`find /opt/eclipse/java-2021-12/ -name "javaagent-shaded.jar"`
  if [ "$AGENTJAR" == "" ]; then
    AGENTJAR=`find /home/specgui/.eclipse/ -name "javaagent-shaded.jar"`
  fi
  JAVAAGENTARGS="-javaagent:$AGENTJAR -Dfile.encoding=UTF-8"
  #JAVAAGENTARGS="-javaagent:/home/specgui/.eclipse/org.eclipse.platform_4.21.0_1998503505_linux_gtk_x86_64/configuration/org.eclipse.osgi/637/0/.cp/lib/javaagent-shaded.jar"
else
  DEBUGARGS=""
  JAVAAGENTARGS=""
fi

cd $CODEROOT/ImageProc-w7x/

#The command eclipse runs to start the app in debug mode
CMD="$JAVA $DEBUGARGS $VMARGS $JAVAAGENTARGS -classpath $CLASSPATH $MAINCLASS $ARGS"
pwd
echo "Executing ImageProc in JVM with:"
echo "  LD_LIBRARY_PATH: $LD_LIBRARY_PATH"
echo "  Command: $CMD"

if [ $DELAY -gt 0 ]; then
    echo -n "Waiting ${DELAY}s for desktop to init..."
    sleep $DELAY
    echo "OK"
fi
$CMD


