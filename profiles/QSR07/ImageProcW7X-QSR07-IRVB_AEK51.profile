# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK - NIFS Carbon
# Detector: Andor iXon CCD
# Purpose: Carbon NIFS Spectrometer (from OP2.1)
#
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSR07-IRVB_AEK51-W7XPilot
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=sv-coda-qsr07-1

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.flirSciCam.FLIRSciCamSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/FLIRSciCamSource/IRVB_AEK51-Default

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/IRVB_AEK51
imageProc.profile.sink2.init=true
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.init=true
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/IRVB_AEK51
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink8.init=false
imageProc.profile.sink8.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink0.init=true
imageProc.profile.sink8.sink2.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink8.sink2.init=false
imageProc.profile.sink8.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink8.sink3.init=false


	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSR07-IRVB
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=true
imageProc.w7xPilot.inShot.acquireStartTimeMS=59000
imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault=false
imageProc.w7xPilot.inShot.fault.resetPower=false
imageProc.w7xPilot.inShot.fault.cameraOffTime=10000
imageProc.w7xPilot.inShot.acquireTimeout=120000
imageProc.w7xPilot.inShot.saveTimeout=240000
imageProc.w7xPilot.inShot.processChainDelay=1000
imageProc.w7xPilot.inShot.acquireStartTimeout=120000
imageProc.w7xPilot.powerCtrlClass=None
imageProc.w7xPilot.cameraWarmUpTime=10000
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2