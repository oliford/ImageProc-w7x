# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSS Endoscopes
# Detector: DC1394
# Spectrometer: None. Endoscope cameras   
# Purpose: Development/Test of DC1394 acqusition.
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSS-DC1394Test
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=sv-coda-qss-1

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.dc1394.DC1394Source
#imageProc.profile.source.config=jsonfile:/data/jsonSettings/DC1394Source/Test


# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=false
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHangerW7X
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.init=true
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink7.init=false
imageProc.profile.sink9.class=imageProc.control.power.raritan.RaritanPowerControl
imageProc.profile.sink9.init=true
imageProc.profile.sink10.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink10.init=false

# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSS-Test
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2