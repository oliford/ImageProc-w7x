# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSL Laser Blow-off
# Detector: PICam - Princeton Instruments 512x512 CCD Camera
# Spectrometer: AUG-type with aspheric lenses
# Purpose: LBO / Passive FIDA
#

# Acquisition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSL-LBO-FIDA
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHostname=pc-e5-qsl-8

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.picam.PicamSource
#imageProc.profile.source.config=jsonfile:/data/minerva/jsonSettings/PicamSource/QSL-PICam-Default
imageProc.profile.source.config=jsonfile:/data/minerva/jsonSettings/PicamSource/QSL-PICam-C_VI-NeX-530nm

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=true
imageProc.profile.sink3.config=jsonfile:/data/minerva/jsonSettings/FastSpecProcessorW7X/LBO_Spectromter-CVI_NeX-530-allChans
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.init=true
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false

# W7X Archive sink database and path name
imageProc.w7x.archiveDB.path=w7x/Test/raw/W7X/QSL_LBO/LBO_Spectrometer_DATASTREAM/0/Images
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=false
	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSL-LBO
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=true 		# not for in the lab!
imageProc.w7xPilot.inShot.acquireStartTimeMS=59000
imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault=false
imageProc.w7xPilot.inShot.fault.resetPower=false
imageProc.w7xPilot.inShot.fault.cameraOffTime=10000
imageProc.w7xPilot.inShot.acquireTimeout=400000
imageProc.w7xPilot.inShot.saveTimeout=400000
imageProc.w7xPilot.inShot.processChainDelay=1000
imageProc.w7xPilot.inShot.acquireStartTimeout=120000
imageProc.w7xPilot.powerCtrlClass=None
imageProc.w7xPilot.cameraWarmUpTime=10000
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2