# ImageProc settings profile for acquisition system.
#
# Diagnostic: QRI - Coherence Imaging (AEQ21)
# Detector: PCO Edge 5.5
# Purpose: Scrape off layer flows with Coherence Imaging Spectroscopy, automatic analysis
#

# Automatic analysis of phase and selection of some islands
imageProc.w7xPilot.systemID=QRI-AEQ21-reproc
imageProc.w7xPilot.statusComm.defaultPort=9752

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSource
imageProc.profile.source.setDBPath=ArchiveDB/raw/W7X/QRI_CoherenceImaging/PCOEdgeAEQ21_DATASTREAM/0/Images

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink4.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.init=false
imageProc.profile.sink6.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink6.init=true
imageProc.profile.sink6.config=jsonfile:/data/minerva/jsonSettings/FastSpecProcessorW7X/AEQ21_radiationmovement_rawdata-autoSave

imageProc.profile.sink8.class=imageProc.proc.fft.FFTProcessor
imageProc.profile.sink8.init=true
imageProc.profile.sink8.config=jsonfile:/data/minerva/jsonSettings/FFTProcessor/AEQ21_CropWithAspect

imageProc.profile.sink8.sink1.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink1.init=false
imageProc.profile.sink8.sink2.class=imageProc.proc.demod.DSHDemod
imageProc.profile.sink8.sink2.init=true
imageProc.profile.sink8.sink2.config=jsonfile:/data/minerva/jsonSettings/DSHDemod/AEQ21-cropped-cal10

imageProc.profile.sink8.sink2.sink1.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink2.sink1.init=true
imageProc.profile.sink8.sink2.sink2.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink8.sink2.sink2.init=true
imageProc.profile.sink8.sink2.sink2.setDBPath=w7x/ArchiveDB/raw/W7XAnalysis/QRI_CoherenceImaging/PCOEdgeAEQ21_PhaseAutoProc_DATASTREAM/0/Images

imageProc.profile.sink8.sink2.sink3.class=imageProc.proc.softwareBinning.SoftwareROIsProcessor
imageProc.profile.sink8.sink2.sink3.init=true
imageProc.profile.sink8.sink2.sink3.config=jsonfile:/data/minerva/jsonSettings/SoftwareROIsProcessor/AEQ21-Islands_stdconfig

imageProc.profile.sink8.sink2.sink3.sink1.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink2.sink3.sink1.init=true
imageProc.profile.sink8.sink2.sink3.sink2.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink8.sink2.sink3.sink2.init=true
imageProc.profile.sink8.sink2.sink3.sink2.setDBPath=w7x/ArchiveDB/raw/W7XAnalysis/QRI_CoherenceImaging/PCOEdgeAEQ21_PhaseAutoProc_IslandROIs_DATASTREAM/0/Images


# W7X Archive sink database and path name
imageProc.w7x.archiveDB.path=null
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=true
	
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.shotPollIntervalMS=1000
imageProc.w7xPilot.inShot.dayPollIntervalMS=60000
imageProc.w7xPilot.inShot.startAtInit=false
imageProc.w7xPilot.inShot.acquireStartTimeMS=46000
imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault=false
imageProc.w7xPilot.inShot.fault.resetPower=false
imageProc.w7xPilot.inShot.fault.cameraOffTime=10000
imageProc.w7xPilot.inShot.acquireTimeout=400000
imageProc.w7xPilot.inShot.saveTimeout=400000
imageProc.w7xPilot.inShot.processChainDelay=1000
imageProc.w7xPilot.inShot.acquireStartTimeout=120000
imageProc.w7xPilot.powerCtrlClass=None
imageProc.w7xPilot.cameraWarmUpTime=10000
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2