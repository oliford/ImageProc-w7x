# ImageProc settings profile for acquisition system.
#
# Diagnostic: QRI - Coherence Imaging (AEF30)
# Detector: FLIR Blackfly S
# Purpose: Scrape off layer flows and ion temperature with Coherence Imaging Spectroscopy
#

# Acquisition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QRI-AEF30
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=pc-e4-qri-3
imageProc.window.iconChars=F30

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.flir.FLIRCamSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/FLIRCamSource/AEF30_OP2.2_50ms_20fps

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.geri.GeriPilot
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/GeriPilot/AEF30-Default
imageProc.profile.sink2.init=true
imageProc.profile.sink3.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink3.init=false
imageProc.profile.sink4.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.geriStream.GeriStreamSink
imageProc.profile.sink5.init=true
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/GeriStreamSink/AEF30_RawData
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.proc.transform.TransformProcessor
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.proc.fft.FFTProcessor
imageProc.profile.sink8.init=false
imageProc.profile.sink9.class=imageProc.proc.demod.DSHDemod
imageProc.profile.sink9.init=false
imageProc.profile.sink10.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink10.init=true
imageProc.profile.sink10.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/AEF30_OP2.2

# GMDS Sink (local files) 'experiment', path and starting shot	
# Valeria used 'DEFAULT', not much point in changing it now
imageProc.gmds.experiment=DEFAULT
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2