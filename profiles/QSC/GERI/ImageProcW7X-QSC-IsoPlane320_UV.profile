# ImageProc settings profile for acquisition system.
#
# Diagnostic: UV spectroscopy of W baffle tiles in AEK41, technically part of QSD (Hexos) but next to QSC spectrometers
# Detector: PICam - Princeton Instruments 1024x1024 CCD Camera
# Spectrometer: IsoPlane320_UV  
# Purpose: UV observation of baffle tiles from AEK41
# Note: this profile copied from QSS70/ImageProcW7X-QSS70-IsoPlane320_9.profile and modified
#

# Acqusition identity, host and status port for pilot st_9atus interrogation
imageProc.w7xPilot.systemID=QSC-IsoPlane320_UV
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=pc-e3-qsc-6
imageProc.window.iconChars=UV

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.picam.PicamSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/PicamSource/IsoPlane320_UV

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=false
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.geri.GeriPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/GeriPilot/IsoPlane320_UV
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false

imageProc.profile.sink5.class=imageProc.database.geriStream.GeriStreamSink
imageProc.profile.sink5.init=true
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/GeriStreamSink/IsoPlane320_UV

imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.control.isoplane.IsoPlaneControl
imageProc.profile.sink8.init=true
imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/IsoPlaneControl/IsoPlane320_UV-Default

imageProc.profile.sink9.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink9.init=true
imageProc.profile.sink9.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/IsoPlane320_UV-Default

imageProc.profile.sink11.class=imageProc.control.power.snmp.SNMPPowerControl
imageProc.profile.sink11.config=jsonfile:/data/jsonSettings/SNMPPowerControl/IsoPlane320_UV
imageProc.profile.sink11.init=true
	
# GMDS Sink (local files) 'experiment', path and starting shot
# Note: Commented below GMDS stuff out because I don't think it's used anymore
# imageProc.gmds.experiment=QSS-IsoPlane320_9-W7XPilot
# imageProc.gmds.pulse=0
# imageProc.gmds.path=/RAW

# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=true 		# not for in the lab!
imageProc.w7xPilot.inShot.acquireStartTimeMS=50000
imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault=false
imageProc.w7xPilot.inShot.fault.resetPower=false
imageProc.w7xPilot.inShot.fault.cameraOffTime=10000
imageProc.w7xPilot.inShot.acquireTimeout=120000
imageProc.w7xPilot.inShot.saveTimeout=240000
imageProc.w7xPilot.inShot.processChainDelay=1000
imageProc.w7xPilot.inShot.acquireStartTimeout=120000
imageProc.w7xPilot.powerCtrlClass=None
imageProc.w7xPilot.cameraWarmUpTime=10000
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2