# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK - Tespel Spectroscopy
# Detector: PCO Edge 4.2 M
# Purpose: Lithium ablation cloud monitoring
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSC-TespelLiSpec
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=false
imageProc.profile.host=pc-e3-qsc-5
imageProc.window.iconChars=Tsp

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.pcosdk.PCO_SDKSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/PCO_SDKSource/TespelLiSpec

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false

imageProc.profile.sink2.class=imageProc.pilot.geri.GeriPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/GeriPilot/TespelLiSpec

imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.geriStream.GeriStreamSink
imageProc.profile.sink5.init=true
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/GeriStreamSink/TespelLiSpec
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.proc.softwareBinning.SoftwareROIsProcessor
imageProc.profile.sink8.init=false
#imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/SoftwareROIsProcessor/TespelLiSpec

imageProc.profile.sink8.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink0.init=true

imageProc.profile.sink8.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink8.sink6.init=false

imageProc.profile.sink8.sink2.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink8.sink2.init=true
#imageProc.profile.sink8.sink2.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/TespelLiSpec-Binned

imageProc.profile.sink10.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink10.init=true
imageProc.profile.sink10.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/TespelLiSpec

# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-PelletsHBeta
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2