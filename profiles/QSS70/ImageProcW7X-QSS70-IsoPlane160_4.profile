# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSS - Divertor Spectroscopy
# Detector: PICam - Princeton Instruments 1024x1024 CCD Camera
# Spectrometer: IsoPlane160_4
# Purpose: Isoplane.....
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSS70-IsoPlane160_4-W7XPilot
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=pc-e4-qss-27

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.picam.PicamSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/PicamSource/IsoPlane160_4 21.11.2022

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=false
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/IsoPlane160_4-Default
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
#imageProc.profile.sink3.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/PICamTest-Default
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/IsoPlane160_4-Default
imageProc.profile.sink5.init=true
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.control.isoplane.IsoPlaneControl
imageProc.profile.sink8.init=true
imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/IsoPlaneControl/IsoPlane160_4-Default


# W7X Archive sink database and path name
imageProc.w7x.archiveDB.path=w7x/ArchiveDB/raw/W7X/QSS_DivertorSpectroscopy/PI_CCD_04_1-QSS60OC092_DATASTREAM/0/Images
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=true
	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSS-PICamSpecIsoplane4
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=true 		# not for in the lab!
imageProc.w7xPilot.inShot.acquireStartTimeMS=59000
imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault=false
imageProc.w7xPilot.inShot.fault.resetPower=false
imageProc.w7xPilot.inShot.fault.cameraOffTime=10000
imageProc.w7xPilot.inShot.acquireTimeout=120000
imageProc.w7xPilot.inShot.saveTimeout=240000
imageProc.w7xPilot.inShot.processChainDelay=1000
imageProc.w7xPilot.inShot.acquireStartTimeout=120000
imageProc.w7xPilot.powerCtrlClass=None
imageProc.w7xPilot.cameraWarmUpTime=10000
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2