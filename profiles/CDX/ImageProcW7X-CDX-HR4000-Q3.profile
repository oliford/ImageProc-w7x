E# ImageProc settings profile for acquisition system.
#
# Diagnostic: CDX - Neutral Injection - Data Acquisition
# Detector: OceanOptics HR4000 USB Spectrometer
# Spectrometer: OceanOptics HR4000 USB Spectrometer
# Purpose: NBI Neutraliser Spectroscopy, Q3
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=CDX-HR4000-Q3
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=sv-ana-cdx-2



#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.oceanOptics.OmniDriverSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/OmniDriverSource/CDX-HR4000-Q3-Default

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/CDX-Neutraliser-Q3
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=true
imageProc.profile.sink3.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/CDX-Q3-Neutraliser
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.init=true
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false

# W7X Archive sink database and path name
imageProc.w7x.archiveDB.path=w7x/ArchiveDB/raw/W7X/CDX20_Spectroscopy/USB_HR4000_Q3_DATASTREAM/0/Images
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=true

# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=CDX-HR4000-Q3
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# GUI config
imageProc.gui.colorLogScale=0.5
imageProc.gui.maximized=false
imageProc.gui.windowWidth=1280
imageProc.gui.windowHeight=512
imageProc.gui.scaleX=0.15
imageProc.gui.scaleY=160.0
