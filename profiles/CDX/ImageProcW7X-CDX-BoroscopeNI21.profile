E# ImageProc settings profile for acquisition system.
#
# Diagnostic: CDX - Neutral Injection - Data Acquisition
# Detector: Unknown camera on PIXCI Framegrabber via XCLIB
# Purpose: NBI Boroscope camera NI21
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=CDX-BoroscopeNI21
imageProc.w7xPilot.statusComm.defaultPort=9754
imageProc.profile.assertHost=false
imageProc.profile.host=sv-ana-cdx-4
imageProc.window.iconChars=B21



#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.xclib.XCLIBSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/XCLIBSource/CDX-BoroscopeNI21

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/CDX-BoroscopeNI21
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/CDX-BoroscopeNI21
imageProc.profile.sink5.init=true

imageProc.profile.sink9.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink9.init=false

# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=CDX-BoroscopeNI21
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW

imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de

	
# GUI config
imageProc.gui.colorLogScale=0.5
imageProc.gui.maximized=false
#imageProc.gui.windowWidth=1280
#imageProc.gui.windowHeight=512
#imageProc.gui.scaleX=1.0
#imageProc.gui.scaleY=1.0