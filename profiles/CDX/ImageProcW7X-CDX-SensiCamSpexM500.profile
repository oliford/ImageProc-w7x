E# ImageProc settings profile for acquisition system.
#
# Diagnostic: CDX - Neutral Injection - Data Acquisition
# Detector: Sensicam VGA
# Spectrometer: SpexM 500mm
# Purpose: NBI Neutraliser Spectroscopy, Q3,4,7,8
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=CDX-SensiCamSpexM500
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=false
imageProc.profile.host=sv-ana-cdx-5
imageProc.window.iconChars=NSp



#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.sensicam.SensicamSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/SensicamSource/CDX-SensiCamSpexM500_4source_reducedROI

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/CDX-SensiCamSpexM500
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/CDX-SensiCamSpexM500
imageProc.profile.sink5.init=true
imageProc.profile.sink7.class=imageProc.proc.softwareBinning.SoftwareROIsProcessor
imageProc.profile.sink7.init=true
imageProc.profile.sink7.config=jsonfile:/data/jsonSettings/SoftwareROIsProcessor/CDX-SensiCamSpexM500-ROI-reduced
imageProc.profile.sink7.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink7.sink0.init=true
imageProc.profile.sink7.sink1.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink7.sink1.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/CDX-SensiCamSpexM500-Binned
imageProc.profile.sink7.sink1.init=true
imageProc.profile.sink7.sink3.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink7.sink3.config=jsonfile:/data/jsonSettings/SpecCalProcessor/CDX-SensiCamSpexM500
imageProc.profile.sink7.sink3.init=true
imageProc.profile.sink7.sink5.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink7.sink5.init=true
imageProc.profile.sink7.sink5.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/CDX-SensicamSpexM500

imageProc.profile.sink9.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink9.init=false

# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=CDX-SensiCamSpexM500
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de

	
# GUI config
imageProc.gui.colorLogScale=0.5
imageProc.gui.maximized=false
#imageProc.gui.windowWidth=1280
#imageProc.gui.windowHeight=512
#imageProc.gui.scaleX=1.0
#imageProc.gui.scaleY=1.0
