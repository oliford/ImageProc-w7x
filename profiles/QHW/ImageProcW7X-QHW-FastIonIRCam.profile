# ImageProc settings profile for acquisition system.
#
# Diagnostic: QHW Fast ion wall loads monitor
# Detector: FLIR IR thing on USB (uvcvideo --> v4l)
# Spectrometer: FastIonIRCam   
# Purpose: Watching things melt
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QHW-FastIonIRCam
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=sv-e3-qhw-1
imageProc.window.iconChars=QHW

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.v4l.V4LSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/V4LSource/FastIonIRCam-Default


# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/FastIonIRCam
imageProc.profile.sink2.init=true
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/FastIonIRCam
imageProc.profile.sink5.init=true
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink7.init=false
imageProc.profile.sink10.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink10.init=false

imageProc.profile.sink11.class=imageProc.proc.infraredTemperature.InfraredTemperatureProcessor
imageProc.profile.sink11.init=true
imageProc.profile.sink11.config=jsonfile:/data/jsonSettings/InfraredTemperatureProcessor/FastIonIRCam_v10001

imageProc.profile.sink11.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink11.sink0.init=true

imageProc.profile.sink11.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink11.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/FastIonIRCam-Temperature
imageProc.profile.sink11.sink5.init=true

# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QHW-FastIonIRCam
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2