# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK - ILS Green
# Detector: Andor Neo
# Purpose: Green channel of ITER-Like Spectrometer  - Profile for reprocessing from binned data  
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-ILS_Green_binning
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.window.iconChars=Bi


#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSource
imageProc.profile.source.setDBPath=w7x/ArchiveDB/raw/W7X/ControlStation.71302/ILS_Green-1_DATASTREAM/0/Images

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=true
#imageProc.profile.sink7.config=jsonfile:/data/jsonSettings/MetaDataManager/fix-the-stupid-metadata-ILS_Red-OP2-autoExposure
imageProc.profile.sink8.class=imageProc.proc.softwareBinning.SoftwareROIsProcessor
imageProc.profile.sink8.init=true
imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/SoftwareROIsProcessor/ILS-Green-OP2-with-tilt-good-everywhere-2
imageProc.profile.sink9.sink2.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink9.sink2.init=false
imageProc.profile.sink10.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink10.sink3.init=false


#imageProc.profile.sink8.sink0.class=imageProc.core.swt.ImageWindow
#imageProc.profile.sink8.sink0.init=true
imageProc.profile.sink8.sink1.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink1.init=true
imageProc.profile.sink8.sink2.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink8.sink2.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/ILS_Green_Binned
imageProc.profile.sink8.sink2.init=true

# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-ILS-GREEN
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=false
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2