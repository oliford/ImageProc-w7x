# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK - ILS Red
# Detector: Andor Neo
# Purpose: Red channel of ITER-Like Spectrometer  - Profile for reprocessing from binned data  
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-ILS_Red-reproc
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.window.iconChars=Rp


#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSource
imageProc.profile.source.setDBPath=w7x/ArchiveDB/raw/W7X/QSK_CXRS/ILS_Red_DATASTREAM/0/Images

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=true
imageProc.profile.sink8.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink8.init=false
imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/SpecCalProcessor/ILS_Red_NeCal-gen7.txt


imageProc.profile.sink9.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink9.init=false
imageProc.profile.sink9.config=jsonfile:/data/jsonSettings/SeriesProcessorW7X/ILS_Red-BeamselOnly

#imageProc.profile.sink9.sink0.class=imageProc.core.swt.ImageWindow
#imageProc.profile.sink9.sink0.init=true
imageProc.profile.sink9.sink1.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink9.sink1.init=true
imageProc.profile.sink9.sink2.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink9.sink2.init=true
imageProc.profile.sink9.sink2.config=jsonfile:/data/jsonSettings/SpecCalProcessor/ILS_Red_NeCal-gen7.txt
imageProc.profile.sink9.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink9.sink3.init=true
imageProc.profile.sink9.sink3.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/ILS_Red_BES_Dev-AEA21


# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-ILS-GREEN
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=false
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2