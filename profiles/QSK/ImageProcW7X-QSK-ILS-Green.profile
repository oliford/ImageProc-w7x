# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK - ILS Green
# Detector: Andor Neo
# Purpose: Green channel of ITER-Like Spectrometer 
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-ILS_Green-W7XPilot
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=pc-e3-qsk-15
imageProc.window.iconChars=G

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.andorCam.AndorCamSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/AndorCamSource/ILS-Green-Plasma-Default

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/ILS_Green-Default
imageProc.profile.sink2.init=true
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/ILS_Green
imageProc.profile.sink5.init=true
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false

imageProc.profile.sink8.class=imageProc.proc.softwareBinning.SoftwareROIsProcessor
imageProc.profile.sink8.init=true
imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/SoftwareROIsProcessor/ILS-Green-OP2-with-tilt-good-everywhere-2
imageProc.profile.sink8.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink0.init=true
imageProc.profile.sink8.sink1.config=jsonfile:/data/jsonSettings/SpecCalProcessor/ILS_Green-generated.txt
imageProc.profile.sink8.sink2.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink8.sink2.init=true
imageProc.profile.sink8.sink2.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/ILS_Green
imageProc.profile.sink8.sink3.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink8.sink3.init=true
imageProc.profile.sink8.sink3.config=jsonfile:/data/jsonSettings/SeriesProcessorW7X/ILS_Green_BG_Subtraction
imageProc.profile.sink8.sink3.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink3.sink0.init=true
imageProc.profile.sink8.sink3.sink1.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink8.sink3.sink1.init=true
imageProc.profile.sink8.sink3.sink1.config=jsonfile:/data/jsonSettings/SpecCalProcessor/ILS_Green-OP2-FromDB
imageProc.profile.sink8.sink3.sink2.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink8.sink3.sink2.init=true
imageProc.profile.sink8.sink3.sink2.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/ILS_Green-OP2-CVI-ACX-only
imageProc.profile.sink9.class=imageProc.control.power.raritan.RaritanPowerControl
#imageProc.profile.sink9.config=jsonfile:/data/jsonSettings/RaritanPowerControl/ILS_Green
imageProc.profile.sink9.init=true

# W7X Archive sink database and path name
imageProc.w7x.archiveDB.database=W7X_TEST
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=false
	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-ILS-GREEN
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=true
imageProc.w7xPilot.inShot.acquireStartTimeMS=59000
imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault=false
imageProc.w7xPilot.inShot.fault.resetPower=false
imageProc.w7xPilot.inShot.fault.cameraOffTime=10000
imageProc.w7xPilot.inShot.acquireTimeout=120000
imageProc.w7xPilot.inShot.saveTimeout=240000
imageProc.w7xPilot.inShot.processChainDelay=1000
imageProc.w7xPilot.inShot.acquireStartTimeout=120000
imageProc.w7xPilot.powerCtrlClass=None
imageProc.w7xPilot.cameraWarmUpTime=10000
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2
