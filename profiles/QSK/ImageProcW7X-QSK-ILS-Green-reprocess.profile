# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK - ILS Green
# Detector: Andor Neo
# Purpose: Green channel of ITER-Like Spectrometer  - Profile for reprocessing from binned data  
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-ILS-Green-reproc
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.window.iconChars=Rp


#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSource
imageProc.profile.source.setDBPath=w7x/ArchiveDB/raw/W7X/QSK_CXRS/ILS_Green_Binned_DATASTREAM/0/Images

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=true
imageProc.profile.sink8.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink8.init=false
imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/SpecCalProcessor/ILS_Green-C_VI_529-gen7.txt


imageProc.profile.sink9.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink9.init=true
imageProc.profile.sink9.config=jsonfile:/data/jsonSettings/SeriesProcessorW7X/ILS_Green_BG_Subtraction
imageProc.profile.sink9.sink1.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink9.sink1.init=true
imageProc.profile.sink9.sink2.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink9.sink2.init=true
imageProc.profile.sink9.sink2.config=jsonfile:/data/jsonSettings/SpecCalProcessor/Blank-fromDB
imageProc.profile.sink9.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink9.sink3.init=true
imageProc.profile.sink9.sink3.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/ILS_Green-OP2-CVI-ACX-only
#imageProc.profile.sink9.sink4.class=imageProc.core.swt.ImageWindow
#imageProc.profile.sink9.sink4.init=true

# W7X Archive sink database and path name
imageProc.w7x.archiveDB.database=W7X_TEST
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=false
	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-ILS-GREEN
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=false
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2