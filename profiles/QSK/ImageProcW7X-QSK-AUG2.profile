# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK CXRS
# Detector: PICam - Princeton Instruments 1024x1024 CCD Camera
# Spectrometer: AUG2   
# Purpose: aug2
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-AUG2-W7XPilot
imageProc.w7xPilot.statusComm.defaultPort=9752
#imageProc.profile.assertHost=true
imageProc.profile.host=pc-e3-qsk-2
imageProc.window.iconChars=AU2

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.picam.PicamSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/PicamSource/AUG2-Default

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=false
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/AUG2
imageProc.profile.sink2.init=true
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
#imageProc.profile.sink3.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/QSS-PICamSP2750_1-Default
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.config=jsonfile:/data/jsonSettings/ArduinoCommHanger/AUG2
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/AUG2
imageProc.profile.sink5.init=true
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.control.grbl.GRBLControl
imageProc.profile.sink8.init=false
#imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/GRBLControl/AUG2
imageProc.profile.sink9.class=imageProc.control.power.snmp.SNMPPowerControl
imageProc.profile.sink9.config=jsonfile:/data/jsonSettings/SNMPPowerControl/AUG2
imageProc.profile.sink9.init=true
imageProc.profile.sink10.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink10.init=false
	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-AUG2
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2