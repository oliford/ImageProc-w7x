# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK - Wisconsin Heat Pulse Propagation, Spectrometer #5
# Detector: Andor iXon CCD
# Purpose: Helium NIFS Spectrometer 
#
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-WISC_5
imageProc.w7xPilot.statusComm.defaultPort=9756
imageProc.profile.assertHost=true
imageProc.profile.host=pc-e3-qsk-11
imageProc.window.iconChars=W5

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.andorV2.AndorV2Source
imageProc.profile.source.config=jsonfile:/data/jsonSettings/AndorV2Source/WISC-5-Default

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.geri.GeriPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/GeriPilot/WISC_5
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
imageProc.profile.sink4.class=imageProc.database.geriStream.GeriStreamSink
imageProc.profile.sink4.init=true
imageProc.profile.sink4.config=jsonfile:/data/jsonSettings/GeriStreamSink/WISC_5
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/WISC-5
imageProc.profile.sink5.init=true
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.control.standa.ximc.XIMCControl
imageProc.profile.sink8.init=false
imageProc.profile.sink9.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink9.init=false
	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-WISC-5
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2