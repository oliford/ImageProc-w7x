# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK - Spectral Motional Stark Effect
# Detector: ...
# Purpose: ... 
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-SMSE
imageProc.w7xPilot.statusComm.defaultPort=9752
<<<<<<< HEAD
imageProc.profile.assertHost=true
imageProc.profile.host=pc-e3-qsk-8
=======
imageProc.profile.assertHost=false
imageProc.profile.host=pc-e4-qri-4
imageProc.window.iconChars=MSE
>>>>>>> branch 'master' of https://DataAcqPCs:glpat-y1e6u2HasZFxaaupG1wM@git.ipp-hgw.mpg.de/ImageProc/ImageProc-w7x.git

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.pcosdk.PCO_SDKSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/PCO_SDKSource/Test

imageProc.sources.capture.andorCam.settingsType=json

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.geri.GeriPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/GeriPilot/SMSE
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=false
imageProc.profile.sink5.class=imageProc.database.geriStream.GeriStreamSink
imageProc.profile.sink5.init=true
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/GeriStreamSink/SMSE
imageProc.profile.sink6.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink6.init=true
imageProc.profile.sink6.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/SMSE
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.proc.softwareBinning.SoftwareROIsProcessor
imageProc.profile.sink8.init=true
imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/SoftwareROIsProcessor/SMSE

imageProc.profile.sink8.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink8.sink0.init=true

imageProc.profile.sink8.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink8.sink6.init=false

imageProc.profile.sink8.sink2.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink8.sink2.init=true
#imageProc.profile.sink8.sink2.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/SMSE-Binned

imageProc.profile.sink8.sink5.class=imageProc.database.geriStream.GeriStreamSink
imageProc.profile.sink8.sink5.init=true
imageProc.profile.sink8.sink5.config=jsonfile:/data/jsonSettings/GeriStreamSink/SMSE_Binned

imageProc.profile.sink9.class=imageProc.control.power.snmp.SNMPPowerControl
imageProc.profile.sink9.config=jsonfile:/data/jsonSettings/SNMPPowerControl/SMSE
imageProc.profile.sink9.init=true

# W7X Archive sink database and path name
imageProc.w7x.archiveDB.database=W7X_TEST
imageProc.w7x.archiveDB.path=/raw/W7X/QSK_CXRS/SMSE_DATASTREAM/0/Images
  # write to cahce but not actually DB and WriteBack will write it later
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=false
	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-SMSE
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2