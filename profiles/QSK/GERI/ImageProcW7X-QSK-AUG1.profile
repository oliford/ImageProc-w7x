# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK CXRS
# Detector: PICam - Princeton Instruments 1024x1024 CCD Camera
# Spectrometer: AUG1   
# Purpose: aug1
#

# Acqusition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-AUG1
imageProc.w7xPilot.statusComm.defaultPort=9752
imageProc.profile.assertHost=true
imageProc.profile.host=pc-e3-qsk-1
imageProc.window.iconChars=AU1

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.picam.PicamSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/PicamSource/AUG1-OP2-C_VI_529-Plasma


# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=false
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.geri.GeriPilot
imageProc.profile.sink2.init=true
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/GeriPilot/AUG1
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=false
#imageProc.profile.sink3.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/AUG1
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHangerW7X
imageProc.profile.sink4.config=jsonfile:/data/jsonSettings/ArduinoCommHangerW7X/AUG1
imageProc.profile.sink4.init=true
imageProc.profile.sink5.class=imageProc.database.geriStream.GeriStreamSink
imageProc.profile.sink5.init=true
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/GeriStreamSink/AUG1
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink8.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/AUG1
imageProc.profile.sink8.init=true
imageProc.profile.sink9.class=imageProc.control.power.snmp.SNMPPowerControl
imageProc.profile.sink9.config=jsonfile:/data/jsonSettings/SNMPPowerControl/AUG1
imageProc.profile.sink9.init=true
imageProc.profile.sink10.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink10.init=false

# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-AUG1
imageProc.gmds.pulse=0
imageProc.gmds.path=/RAW
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.2
imageProc.gui.scaleY=0.2