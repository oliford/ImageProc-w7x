# ImageProc settings profile for acquisition system.
#
# Diagnostic: QSK06 - Passive Spectroscopy / CXRS
# Detector: Avantes AvaSpec2048 USB Spectrometer
# Spectrometer: Avantes AvaSpec2048 USB Spectrometer
# Purpose: OP1.2a Overview spectroscopy on targets 
#
# Acquisition identity, host and status port for pilot status interrogation
imageProc.w7xPilot.systemID=QSK-Avaspec-W7XPilot
imageProc.w7xPilot.statusComm.defaultPort=9753
imageProc.profile.assertHost=true
imageProc.profile.host=pc-e3-qsk-1
imageProc.window.iconChars=Av

#The source to use and the configuration to load
imageProc.profile.source.class=imageProc.sources.capture.avaspec.AvaspecSource
imageProc.profile.source.config=jsonfile:/data/jsonSettings/AvaspecSource/Avaspec-Default

# All sinks to be available or initiated
imageProc.profile.sink0.class=imageProc.graph.GraphUtilProcessor
imageProc.profile.sink0.init=true
imageProc.profile.sink1.class=imageProc.database.gmds.GMDSSink
imageProc.profile.sink1.init=false
imageProc.profile.sink2.class=imageProc.pilot.w7x.W7XPilot
imageProc.profile.sink2.config=jsonfile:/data/jsonSettings/W7XPilot/Avaspec-Default
imageProc.profile.sink2.init=true
imageProc.profile.sink3.class=imageProc.proc.fastSpec.FastSpecProcessorW7X
imageProc.profile.sink3.init=true
imageProc.profile.sink3.config=jsonfile:/data/jsonSettings/FastSpecProcessorW7X/Avaspec-Default
imageProc.profile.sink4.class=imageProc.control.arduinoComm.ArduinoCommHanger
imageProc.profile.sink4.init=true
imageProc.profile.sink5.class=imageProc.database.w7xArchiveDB.W7XArchiveDBSink
imageProc.profile.sink5.config=jsonfile:/data/jsonSettings/W7XArchiveDBSink/Avaspec-Default
imageProc.profile.sink5.init=true
imageProc.profile.sink6.class=imageProc.proc.spec.SpecCalProcessor
imageProc.profile.sink6.init=false
imageProc.profile.sink7.class=imageProc.metaview.MetaDataManager
imageProc.profile.sink7.init=false
imageProc.profile.sink8.class=imageProc.proc.seriesAvg.SeriesProcessorW7X
imageProc.profile.sink8.init=false
imageProc.profile.sink9.class=imageProc.control.tte.TTEControl
imageProc.profile.sink9.config=jsonfile:/data/jsonSettings/TTEControl/Avaspec
imageProc.profile.sink9.init=true

# W7X Archive sink database and path name
imageProc.w7x.archiveDB.database=W7X_TEST
imageProc.w7x.archiveDB.path=w7x/ArchiveDB/raw/W7X/QSK_CXRS/USB_Avaspec_DATASTREAM/0/Images
# write to cahce but not actually DB and WriteBack will write it later
imageProc.w7x.archiveDB.writeToCache=true
imageProc.w7x.archiveDB.writeToDB=false
	
# GMDS Sink (local files) 'experiment', path and starting shot	
imageProc.gmds.experiment=QSK-AVASPEC
imageProc.gmds.pulse=600
imageProc.gmds.path=/RAW
	
# W7X Pilot configuration
imageProc.w7xPilot.inShot.startAtInit=true
imageProc.w7xPilot.inShot.acquireStartTimeMS=59000
imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault=false
imageProc.w7xPilot.inShot.fault.resetPower=false
imageProc.w7xPilot.inShot.fault.cameraOffTime=10000
imageProc.w7xPilot.inShot.acquireTimeout=120000
imageProc.w7xPilot.inShot.saveTimeout=240000
imageProc.w7xPilot.inShot.processChainDelay=1000
imageProc.w7xPilot.inShot.acquireStartTimeout=120000
imageProc.w7xPilot.powerCtrlClass=None
imageProc.w7xPilot.cameraWarmUpTime=10000
imageProc.w7xPilot.clockSyncCommand=/usr/sbin/ntpdate -u ntp.ipp-hgw.mpg.de
imageProc.w7xPilot.clockSyncAfterShot=false
	
# GUI config
imageProc.gui.colorLogScale=1.0
imageProc.gui.maximized=true
imageProc.gui.scaleX=0.15
imageProc.gui.scaleY=160.0