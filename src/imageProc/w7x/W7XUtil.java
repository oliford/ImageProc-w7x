package imageProc.w7x;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;

import binaryMatrixFile.AsciiMatrixFile;
import binaryMatrixFile.BinaryMatrixFile;
import descriptors.w7x.ArchiveDBDesc;
import fusionDefs.sensorInfo.SensorInfo;
import fusionDefs.sensorInfo.SensorInfoService;
import fusionDefs.sensorInfo.SensorInfoService.MetaDataGetter;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import otherSupport.SettingsManager;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.w7x.ArchiveDB;
import signals.w7x.ArchiveDB.Database;
import w7x.archive.ArchiveDBFetcher;
import w7x.archive.W7xCampaign;

public class W7XUtil {

	/** @returns Object[]{ long[] nanoTime, double[] beamEnergy, double beamCurrent[] } */
	public static Object[] getBeamIVSignals(ImgSource src, int beamIndex) {

		//find the timestamps of the first and last images
		//We can't use the program start time here because we might be
		//looking at NBI data outside of a W7X program		
		long t0 = Long.MAX_VALUE, t1 = Long.MIN_VALUE;
		
		long[] nanoTime = getBestNanotime(src);
		if(nanoTime != null) {
			for(int i=0; i < nanoTime.length; i++) {
				if(nanoTime[i] < t0 && nanoTime[i] != 0) t0 = nanoTime[i];
				if(nanoTime[i] > t1 && nanoTime[i] != 0) t1 = nanoTime[i];
			}
		}else {		
			MetaDataMap map = src.getCompleteSeriesMetaDataMap();
			for(Entry<String, MetaData> mapEntry : map.entrySet()){
				if(mapEntry.getKey().endsWith("nanoTime")){
					Object o = mapEntry.getValue().object;
					t0 = ((Number)Array.get(o, 0)).longValue() * 1;
					t1 = ((Number)Array.get(o, Array.getLength(o)-10)).longValue();
					
					break;
				}
			}
		}
		if(t0 == Long.MAX_VALUE)
			throw new RuntimeException("Unable to figure out the time of the middle of the images");
		if(t1 == 0)
			t1 = t0 + 100_000_000_000L;
		
		String voltagePath = null, currentPath = null;
		if(t0 < W7xCampaign.OP2_1.start()) {
			switch(beamIndex+1){
			case 7: 
				voltagePath = "w7x/ArchiveDB/codac/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/4/HGV_3 Monitor U";
				currentPath = "w7x/ArchiveDB/codac/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/5/HGV_3 Monitor I";
				break;
			case 8: 
				voltagePath = "w7x/ArchiveDB/codac/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/6/HGV_4 Monitor U";
				currentPath = "w7x/ArchiveDB/codac/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/7/HGV_4 Monitor I";
				break;
			
			default:
				throw new RuntimeException("Unsupported NBI source Q" + (beamIndex+1));
			}
			
		}else {			
			switch(beamIndex+1){
			case 3: 
				voltagePath = "w7x/ArchiveDB/codac/W7X/ControlStation.2176/BE000_DATASTREAM/12/HGV_3 Monitor U";
				currentPath = "w7x/ArchiveDB/codac/W7X/ControlStation.2176/BE000_DATASTREAM/13/HGV_3 Monitor I";
				break;
			case 4: 
				voltagePath = "w7x/ArchiveDB/codac/W7X/ControlStation.2176/BE000_DATASTREAM/14/HGV_4 Monitor U";
				currentPath = "w7x/ArchiveDB/codac/W7X/ControlStation.2176/BE000_DATASTREAM/15/HGV_4 Monitor I";
				break;
			case 7: 
				voltagePath = "w7x/ArchiveDB/codac/W7X/ControlStation.2092/BE000_DATASTREAM/12/HGV_3 Monitor U";
				currentPath = "w7x/ArchiveDB/codac/W7X/ControlStation.2092/BE000_DATASTREAM/13/HGV_3 Monitor I";
				break;
			case 8: 
				voltagePath = "w7x/ArchiveDB/codac/W7X/ControlStation.2092/BE000_DATASTREAM/14/HGV_4 Monitor U";
				currentPath = "w7x/ArchiveDB/codac/W7X/ControlStation.2092/BE000_DATASTREAM/15/HGV_4 Monitor I";
				break;

			default:
				throw new RuntimeException("Unsupported NBI source Q" + (beamIndex+1));
			}
		
		}
		
		//sanity check times
		if(t0 <= 1400000000000000000L || t1 > 4000000000000000000L || t1 < t0)
			throw new RuntimeException("Attempting to get beam signals for an unreasonable nanotime range: " +t0 + " to " + t1);
		
		ArchiveDBDesc descV = new ArchiveDBDesc(voltagePath);
		descV.setNanosRange(t0, t1);
		ArchiveDBDesc descI = new ArchiveDBDesc(currentPath);
		descI.setNanosRange(t0, t1);
		
		ArchiveDB sigV = (ArchiveDB) ArchiveDBFetcher.getExistingDefaultInstance().getSig(descV);
		ArchiveDB sigI = (ArchiveDB) ArchiveDBFetcher.getExistingDefaultInstance().getSig(descI);
		double V[] = sigV.getDataAsType(double.class);
		double I[] = sigI.getDataAsType(double.class);
		double P[] = new double[V.length];
		for(int i=0; i < V.length; i++){
			V[i] *= 1000 * 73 / 10; // "calibration" now in V. TODO: From proper signal once it's there
			I[i] *= 1000 * 0.250 / 10; // "calibration" now in A. TODO: From proper signal once it's there
			P[i] = V[i] * I[i] * 0.35; //approx power to plasma in W.
		}
		
		return new Object[]{ sigV.getTNanos(), V, I, P };
	}
	
	public static long[] getBestNanotime(ImgSource src){
		
		HashMap<String, long[]> matching = new HashMap();
		HashMap<String, long[]> longer = new HashMap();
		
		//if the user (somewhere) or something that think's it knows better has specified a
		//preferred timebase entry, and it exists, and is a long[] etc.., use it
		String preferred = (String)src.getSeriesMetaData("/timebase/preferred");
		if(preferred != null) {
			Object o = src.getSeriesMetaData(preferred);
			if(o != null) {
				o = Mat.fixWeirdContainers(o, true);
				
				if(!(o instanceof long[])){
					Logger.getLogger(W7XUtil.class.getName()).warning("Preferred nanoTime entry '"+preferred+"', which doesn't reduce to a long[]");
					o = null;
				}
				
				if(o != null && Array.getLength(o) != src.getNumImages()){
					Logger.getLogger(W7XUtil.class.getName()).warning("Preferred nanoTime entry '"+preferred+"' with "+Array.getLength(o)+" entries but there are "+src.getNumImages()+" images from the image source.");
					//since this is the forced 'preferred' we'll try to use it anyway
					//o = null;
				}
				
				if(o != null)
					return (long[])o;
				
			}
		}
		
		Logger logr = Logger.getLogger(W7XUtil.class.getSimpleName());
		
		for(Entry<String, MetaData> entry : src.getCompleteSeriesMetaDataMap().entrySet()){
			Object o = entry.getValue().object;
			if(entry.getValue().isTimeSeries && entry.getKey().endsWith("nanoTime")){				
				o = Mat.fixWeirdContainers(o, true);
				
				if(!(o instanceof long[])){
					logr.warning("Ignoring nanoTime entry '"+entry.getKey()+"', which doesn't reduce to a long[]");
					continue;
				}
				
				if(Array.getLength(o) < src.getNumImages()){
					logr.warning("Ignoring nanoTime entry '"+entry.getKey()+"' with "+Array.getLength(o)+" entries but there are "+src.getNumImages()+" images from the image source.");
					continue;
				}
				
				if(Array.getLength(o) > src.getNumImages()){
					longer.put(entry.getKey(), (long[])o);					
				}else {
					matching.put(entry.getKey(), (long[])o);
				}
			}
		}
		
		
		if((matching.size() + longer.size()) <= 0)
			throw new RuntimeException("No */nanoTime found in metadata (or none that is long[] which matches images, see stderr)");
		
		//best one is re-read from archive
		if(matching.containsKey("/w7x/archive/nanoTime"))
			return matching.get("/w7x/archive/nanoTime");
		
		//now any exact matching
		if(matching.size() > 0)
			return matching.entrySet().stream().findFirst().get().getValue();
		
		//otherwise something truncated	
		Entry e = longer.entrySet().stream().findFirst().get();
		Object o = e.getValue();
		
		logr.warning("Using truncated nanoTime entry '"+e.getKey()+"' with "+Array.getLength(o)+" entries because there are only "+src.getNumImages()+" images from the image source.");
		return Arrays.copyOf((long[])o, src.getNumImages());
		
	}

	/** Gets (or creates if not existant) beam energy per frame array from metadata 
	 * * @return { energy[iBeam][iTime], current[iBeam][iTime] } */
	public static double[][][] getBeamInfo(ImgSource src, boolean forceReload) {
		//a SeriesProc has been might have already given us a start/stop nanotime
		Object o = src.getSeriesMetaData("/SeriesProc/startNanos");
		if(o != null){
			long[] startNanos = (long[])Mat.fixWeirdContainers(o, true);
			long[] stopNanos = (long[])Mat.fixWeirdContainers(src.getSeriesMetaData("/SeriesProc/stopNanos"), true);

			return getBeamInfo(src, startNanos, stopNanos, forceReload);
		}else{
			return getBeamInfo(src, getBestNanotime(src), forceReload);
			
		}
	}
	
	public static double[][][] getBeamInfo(ImgSource src, long[] nanoTime, boolean forceReload) {
		long[][] ret = getStartStopNanotime(nanoTime);
		
		return getBeamInfo(src, ret[0], ret[1], forceReload);
	}

	public static long[][] getStartStopNanotime(long[] nanoTime) {
		int n = nanoTime.length;
		long[] startNanos = new long[nanoTime.length];
		long[] stopNanos = new long[nanoTime.length];
		for(int i=0; i < n-1; i++){				
			stopNanos[i] = (nanoTime[i] + nanoTime[i+1])/2;
			startNanos[i+1] = stopNanos[i];
		}
		startNanos[0] = stopNanos[0] - (stopNanos[1] - startNanos[1]);
		stopNanos[n-1] = startNanos[n-1] + (stopNanos[n-2] - startNanos[n-2]);
		return new long[][]{ startNanos, stopNanos };
	}

	public static double[] getFrameExposureTimes(ImgSource src, long[] nanoTime) {

		Object o = src.getSeriesMetaData("/SeriesProc/exposureTime");
		if(o != null){			
			return (double[])Mat.fixWeirdContainers(o, true);
			
		}else{
			//guess from step in nanotime
			double[] expTime = new double[nanoTime.length];
			for(int i=0; i < nanoTime.length-1; i++)
				expTime[i] = (nanoTime[i+1] - nanoTime[i]) / 1e9;
			return expTime;
		}
	}

	/** Gets beam energy per frame array from metadata. Fails if it doesn't exist.
	 * * @return { energy[iBeam][iTime], current[iBeam][iTime] } */
	public static double[][][] getExistingBeamInfo(ImgSource src) {		
		double beamEnergy[][] = new double[8][];
		double beamCurrent[][] = new double[8][];
		double beamPower[][] = new double[8][];
		double beamOnDuration[][] = new double[8][];
		boolean noneFound = true;
		for(int iB=0; iB < 8 ; iB++){
			beamEnergy[iB] = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("w7x/nbi/beamEnergyQ" + (iB+1)),true);
			beamCurrent[iB] = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("w7x/nbi/beamCurrentQ" + (iB+1)),true);
			beamPower[iB] = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("w7x/nbi/beamPowerQ" + (iB+1)),true);
			beamOnDuration[iB] = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("w7x/nbi/beamOnDurationQ" + (iB+1)),true);
			if(beamEnergy[iB] != null)
				noneFound = false;
		}
		if(noneFound)
			throw new RuntimeException("No beam power metadata. (You probably need a SeriesProcessor)");

		return new double[][][]{ beamEnergy, beamCurrent, beamPower, beamOnDuration };
	}
	
	
	
	/** Gets (or creates if not existant) beam energy and current per frame array from metadata.
	 * 
	 * @param src Image source to get/set metadata
	 * @param nanoTime nanotime at center of exposure [ns]
	 * @param expTime length of exposure window [s]
	 * @param forceReload
	 * 
	 * @return { energy[iBeam][iFrame], current[iBeam][iFrame], beamPower[iBeam][iFrame] } */
	public static double[][][] getBeamInfo(ImgSource src, long nanoFrameStart[], long nanoFrameStop[], boolean forceReload) {
		Logger logr = Logger.getLogger(W7XUtil.class.getName());
		
		double beamCurrentThreshold = 10; //[A]. For calculing beamOnTime. Typical is ~ 90A, noise is ~< 1A
		
		double beamEnergy[][] = new double[8][];
		double beamCurrent[][] = new double[8][];
		double beamPower[][] = new double[8][];
		double beamOnDuration[][] = new double[8][];
		for(int iB=0; iB < 8 ; iB++){
			
			if(!forceReload){
				beamEnergy[iB] = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("w7x/nbi/beamEnergyQ" + (iB+1)),true);
				beamCurrent[iB] = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("w7x/nbi/beamCurrentQ" + (iB+1)),true);
				beamPower[iB] = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("w7x/nbi/beamPowerQ" + (iB+1)),true);
				beamOnDuration[iB] = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("w7x/nbi/beamOnTimeQ" + (iB+1)),true);
			}
			
			if(beamEnergy[iB] != null)
				continue; //already in metadata
			
			ImgSource rootSrc = src;
			while(rootSrc instanceof ImgSink)
				rootSrc = ((ImgSink)rootSrc).getConnectedSource();
			
			Object[] ret;
			try {
				ret = getBeamIVSignals(rootSrc, iB);
			}catch(RuntimeException err) {
				if(err.getMessage().contains("Unsupported"))
					continue; //source don't exist yet
				
				logr.log(Level.WARNING, "Unable to read beam signals for S" + (iB+1), err);
				continue;
			}
			
			long beamNanos[] = (long[])ret[0];
			double beamV[] = (double[])ret[1];
			double beamI[] = (double[])ret[2];
			double beamP[] = (double[])ret[3];
			
			//hack on beam for testing
			/*if(iB == (8-1)) {
				long t0 = (long)src.getSeriesMetaData("/w7x/t0Nano");
				long t1 = t0 + (61_000 + 1_800) * 1_000_000L;
				long t2 = t1 + (20) * 1_000_000L;
				for(int i=0; i < beamNanos.length; i++) {
					if(beamNanos[i] >= t1 && beamNanos[i] < t2) {
						beamV[i] += 50000*1;
						beamI[i] += 60;
						beamP[i] += 1700000;
					}
				}
			}//*/				
			
			beamEnergy[iB] = new double[nanoFrameStart.length];
			beamCurrent[iB] = new double[nanoFrameStart.length];
			beamPower[iB] = new double[nanoFrameStart.length];
			beamOnDuration[iB] = new double[nanoFrameStart.length];
			for(int iF=0; iF < nanoFrameStart.length; iF++){
				//we want to average the beam data over the frame time window 
				
				//time length of frame
				//long dtF = (iF == 0) ? (nanoTime[1] - nanoTime[0]) : (nanoTime[iF] - nanoTime[iF-1]);
				long dtF = nanoFrameStop[iF] - nanoFrameStart[iF];
				long nanoTimeCenter = (nanoFrameStop[iF] + nanoFrameStart[iF]) / 2;
				
				//index of beam data point nearest centre time of frame
				int iBT = Mat.getNearestIndex(beamNanos, nanoTimeCenter);
				
				//time length of beam data point
				long dtB = (iBT == 0) ? (beamNanos[1] - beamNanos[0]) : (beamNanos[iBT] - beamNanos[iBT-1]);
				
				if(dtB == 0) {
					dtB = beamNanos[1] - beamNanos[0];
					Logger.getLogger(W7XUtil.class.getName()).warning("beamNanos["+iBT+"] to ["+(iBT-1)+"] are the same!");
				}
				
				// half number of beam data points in a frame
				int nBeamHalfWin = FastMath.ceilToInt(dtF / dtB / 2);
				int n=0;
				//for each (+/-pair) of beam data points in frame
				for(int i=0; i <= 2*nBeamHalfWin; i++){ 
					int iiBT = iBT - nBeamHalfWin + i;
					if(iiBT < 0 || iiBT >= beamNanos.length)
						continue;
					
					if(beamI[iiBT] > beamCurrentThreshold){
						beamEnergy[iB][iF] += beamV[iiBT];
						beamCurrent[iB][iF] += beamI[iiBT];
						beamPower[iB][iF] += beamP[iiBT];
						beamOnDuration[iB][iF] += (dtB * 1e-9);
						n++;
					}	
				}
				
				if(n > 0){
					beamEnergy[iB][iF] /= n;
					beamCurrent[iB][iF] /= n;
					beamPower[iB][iF] /= n;
				}else{
					beamEnergy[iB][iF] = 0;
					beamCurrent[iB][iF] = 0;
					beamPower[iB][iF] = 0;				
				}
				
			}
				
			src.setSeriesMetaData("w7x/nbi/beamEnergyQ" + (iB+1), beamEnergy[iB], true); 
			src.setSeriesMetaData("w7x/nbi/beamCurrentQ" + (iB+1), beamCurrent[iB], true);
			src.setSeriesMetaData("w7x/nbi/beamPowerQ" + (iB+1), beamPower[iB], true);
			src.setSeriesMetaData("w7x/nbi/beamOnDurationQ" + (iB+1), beamOnDuration[iB], true);
		}
		
		return new double[][][]{ beamEnergy, beamCurrent, beamPower, beamOnDuration };
	}

	public static String getVMECBestID(ImgSource src, boolean vaccumOnly) {
		long nanotime = getBestNanotime(src)[0];
		
		
		int numRetries = 3;
		String findRefURL = SettingsManager.defaultGlobal().getProperty("imageProc.util.vmecRef.url", "http://sv-coda-ana-2:8888/findRefEq");
		String progID = (String)src.getSeriesMetaData("/w7x/programID");
		if(progID == null) 
			throw new RuntimeException("No metadata entry '/w7x/programID' to request VMEC ID from webservice");
		
		//?xp=20240925.15
		findRefURL += "?xp=" + progID;
		
		String jsonString = ArchiveDBFetcher.readUrl(findRefURL, numRetries);		
		JsonObject obj = new JsonStreamParser(jsonString).next().getAsJsonObject();
		
		if(!vaccumOnly) {
			JsonElement id = obj.get("equilibriumID");
			if(id != null)
					return id.getAsString();
		}
	
		JsonElement id = obj.get("equilibriumID_vacuum");
		if(id != null)
				return id.getAsString();
	

		//old way ( < OP2.2) from archive stream 
		
		ArchiveDBFetcher adb = ArchiveDBFetcher.defaultInstance();
		
		ArchiveDBDesc aliasDesc = ArchiveDBDesc.aliasDescriptor(Database.W7X_ARCHIVE, "Minerva/Equilibrium/ReferenceEquilibrium", nanotime, nanotime);
		ArchiveDBDesc parlogDesc = adb.resolveAlias(aliasDesc);

		if (parlogDesc == null)
			throw new RuntimeException("Unable to resolve alias for VMEC ID:" + aliasDesc.getWebAPIRequestURI());
		
		parlogDesc.setFromNanos(nanotime);
		parlogDesc.setToNanos(nanotime);
		
		if(parlogDesc.getVersion() <= 0)
			parlogDesc.setVersion(adb.getHighestVersionNumber(parlogDesc));			
		JsonObject parlog = adb.getParlogJson(parlogDesc);
		if(parlog.get("label").toString().contains("EmptySignal"))
			throw new RuntimeException("No automatic VMEC ID available ("+parlogDesc + " = EmptySignal)");
		
		JsonArray array = parlog.get("values").getAsJsonArray();

		return array.get(0).getAsJsonObject().get(
							vaccumOnly ? "equilibriumID_vacuum" : "equilibriumID" 
							).getAsString();

	}

	//TODO: Get from database
	public static double[] getBeamStart(int beamIndex){
		final double[][] beamStart = { 
			{3.4801232483787587,    5.144471596167879, -0.305},
			{3.415671511324611,    5.17542087946828, -0.305},
			{3.415671511324611,    5.17542087946828, -0.305},
			{3.4801232483787587,    5.144471596167879, -0.305},

			{0.2083656846072355,    6.207530069936235, 0.305},
			{0.27869976749402436,    6.194684785564924, 0.305},
			{0.27869976749402436,    6.194684785564924, 0.305},
			{0.2083656846072355,    6.207530069936235, 0.305},
		};
		return beamStart[beamIndex];
	}
	
	public static double[] getBeamUVec(int beamIndex){
		final double beamUVec[][] = {
			{ -0.3659706092386248,    -0.926698150648448, -0.08541692313736746},
			{ -0.4944029793290909,    -0.8650258049747784, -0.08541692313736746},
			{ -0.4944029793290909,    -0.8650258049747784, 0.08541692313736746},
			{ -0.3659706092386248,    -0.926698150648448, 0.08541692313736746},
			
			{ -0.2486230639620658,   -0.9648266794133848, 0.08541692313736746},
			{ -0.10846899866975897,    -0.9904233567365652, 0.08541692313736746},
			{ -0.10846899866975897,    -0.9904233567365652, -0.08541692313736746},
			{ -0.2486230639620658,    -0.9648266794133848, -0.08541692313736746},
		};
		return beamUVec[beamIndex];
	}
}
