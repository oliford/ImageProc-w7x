package imageProc.app;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.swt.widgets.Display;

import descriptors.gmds.GMDSSignalDesc;
import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.control.arduinoComm.ArduinoCommHangerW7X;
import imageProc.control.grbl.GRBLControl;
import imageProc.control.isoplane.IsoPlaneControl;
import imageProc.control.optecFilterWheel.OptecFilterWheelControl;
import imageProc.control.power.raritan.RaritanPowerControl;
import imageProc.control.power.snmp.SNMPPowerControl;
import imageProc.control.standa.ximc.XIMCControl;
import imageProc.control.thing.ThingControl;
import imageProc.control.tte.TTEControl;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.WindowShell;
import imageProc.database.geriStream.GeriStreamSink;
import imageProc.database.gmds.GMDSSink;
import imageProc.database.gmds.GMDSSource;
import imageProc.database.logbook.LogbookControl;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSink;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.graph.GraphUtilProcessor;
import imageProc.metaview.MetaDataManager;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.w7x.W7XPilot;
import imageProc.proc.bes.densityNN.DensityNNProcessor;
import imageProc.proc.demod.DSHDemod;
import imageProc.proc.example.ExampleProcessor;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.fft.FFTProcessor;
import imageProc.proc.imgFit.ImageFitProcessor;
import imageProc.proc.infraredTemperature.InfraredTemperatureProcessor;
import imageProc.proc.seriesAvg.SeriesProcessorW7X;
import imageProc.proc.softwareBinning.SoftwareROIsProcessor;
import imageProc.proc.spec.SpecCalProcessor;
import imageProc.proc.transform.TransformProcessor;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.andorV2.AndorV2Source;
import imageProc.sources.capture.as5216.AvantesAS5216Source;
import imageProc.sources.capture.avaspec.AvaspecSource;
import imageProc.sources.capture.ebus.EBusSource;
import imageProc.sources.capture.example.ExampleSource;
import imageProc.sources.capture.flir.FLIRCamSource;
import imageProc.sources.capture.oceanOptics.OmniDriverSource;
import imageProc.sources.capture.pco.PCOCamSource;
import imageProc.sources.capture.pcosdk.PCO_SDKSource;
import imageProc.sources.capture.picam.PicamSource;
import imageProc.sources.capture.pylon.PylonSource;
import imageProc.sources.capture.sensicam.SensicamSource;
import imageProc.sources.capture.v4l.V4LSource;
import imageProc.sources.sim.dshGen.DSHGenSource;
import imageProc.sources.sim.noiseGen.NoiseGenSource;
import imageProc.sources.sim.platesGen.PlatesGenSource;
import otherSupport.SettingsManager;

/** The ImageProc GUI App to tie all the SWT modules together
 *  
 *
 * This version starts with no source selected and no sinks attached but adds plug-ins
 * for the  W7X Doppler CIS and Spectroscopy systems.
 * 
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 * 
 * For java > 8, need to allow access to deallocate mapped byte buffers
 * --add-opens java.base/java.nio=ALL-UNNAMED --add-exports java.base/jdk.internal.ref=ALL-UNNAMED
 * 
 * 
 * -Djava.util.logging.config.file=~/java.logging.properties
 * 
 * Environment variables need to be set for the following modules:
 * 
 * Bitflow CameraLink framegrabber for Andor Zyla camera: 
 *    BITFLOW_INSTALL_DIRS /opt/andor/bitflow
 *    LD_LIBRARY_PATH /opt/andor/bitflow/64b/lib
 * 
 * RXTX native library for all serial/serial over USB: 
 *    LD_LIBRARY_PATH /usr/lib64/rxtx or /usr/lib/jni
 *    
 * Ocean Optics OmniDriver:
 * 		LD_LIBRARY_PATH /opt/omniDriver-2.56/OOI_HOME
 *  
 */
public class ImageProcW7X {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		//do this here, because its really annoying later when it doesnt work
		//Vmec_Service vmec = new Vmec_Service();
		
		ImageProcUtil.setupLog("ImageProcW7X", ImageProcW7X.class.getName());

				
		Display swtDisplay = new Display();
		   
		//Read data from PCCIS MDS+ DB (HGW Lab)
		
		W7XArchiveDBSource adbSrc = new W7XArchiveDBSource();
		List<ImgSource> srcs = addStandardSources();
		addStandardSinks();
		
		GraphUtilProcessor gu = new GraphUtilProcessor();
		
		//GMDSSignalDesc sigDesc = new GMDSSignalDesc(0, IMSEProc.getMetaExp(noiseSrc), "/signalLists/blah");
		//IMSEProc.globalGMDS().getSig(sigDesc);
		gu.setSource(adbSrc);
		
		WindowShell imgWin1 = new WindowShell(swtDisplay, adbSrc, 0);
		
		ImageProcUtil.msgLoop(swtDisplay, true);
		
	}
	
	/** Create standard source classes
	 * (The list isn't really needed, but the function doesn't
	 * always get executed if it is not filled)
	 * 
	 * @return List of the sources. 
	 */
	public static List<ImgSource> addStandardSources() {

		List<ImgSource> srcs = ImageProcApp.addStandardSources();
		
		return srcs;
	}
	
	public static void addStandardSinks() {
		
		//Sinks get instantiated as required, but we need to register the classes
		ImageProcUtil.addImgPipeClass(GraphUtilProcessor.class);
		//IMSEProc.addImgPipeClass(JotterSink.class);
		ImageProcUtil.addImgPipeClass(GMDSSink.class);
		ImageProcUtil.addImgPipeClass(W7XArchiveDBSink.class);
		ImageProcUtil.addImgPipeClass(FFTProcessor.class);
		ImageProcUtil.addImgPipeClass(DSHDemod.class);
		//IMSEProc.addImgPipeClass(StepperHanger.class); // The old stepper driver, now done via DACTiming, or ArduinoCommHanger
		ImageProcUtil.addImgPipeClass(ImageFitProcessor.class);
		ImageProcUtil.addImgPipeClass(SpecCalProcessor.class);
		ImageProcUtil.addImgPipeClass(FastSpecProcessorW7X.class);
		ImageProcUtil.addImgPipeClass(GeriPilot.class);
		ImageProcUtil.addImgPipeClass(GeriStreamSink.class);
		ImageProcUtil.addImgPipeClass(W7XPilot.class);
		ImageProcUtil.addImgPipeClass(SeriesProcessorW7X.class);
		ImageProcUtil.addImgPipeClass(ArduinoCommHangerW7X.class);
		ImageProcUtil.addImgPipeClass(GRBLControl.class);
		ImageProcUtil.addImgPipeClass(XIMCControl.class);
		ImageProcUtil.addImgPipeClass(MetaDataManager.class);
		ImageProcUtil.addImgPipeClass(SoftwareROIsProcessor.class);
		ImageProcUtil.addImgPipeClass(ExampleProcessor.class);
		ImageProcUtil.addImgPipeClass(InfraredTemperatureProcessor.class);
		ImageProcUtil.addImgPipeClass(TransformProcessor.class);
		ImageProcUtil.addImgPipeClass(RaritanPowerControl.class);
		ImageProcUtil.addImgPipeClass(SNMPPowerControl.class);
		ImageProcUtil.addImgPipeClass(ThingControl.class);
		ImageProcUtil.addImgPipeClass(OptecFilterWheelControl.class);
		ImageProcUtil.addImgPipeClass(IsoPlaneControl.class);
		ImageProcUtil.addImgPipeClass(LogbookControl.class);
		ImageProcUtil.addImgPipeClass(TTEControl.class);
		ImageProcUtil.addImgPipeClass(DensityNNProcessor.class);
	}
}
