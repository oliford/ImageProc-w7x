package imageProc.app;

import org.eclipse.swt.widgets.Display;

import imageProc.control.arduinoComm.ArduinoCommHanger;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.WindowShell;
import imageProc.database.gmds.GMDSSource;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.graph.GraphUtilProcessor;
import imageProc.metaview.MetaDataManager;
import imageProc.proc.demod.DSHDemod;
import imageProc.proc.fastSpec.FastSpecProcessor;
import imageProc.proc.fft.FFTProcessor;
import imageProc.proc.imgFit.ImageFitProcessor;
import imageProc.proc.seriesAvg.SeriesProcessor;
import imageProc.sources.sim.noiseGen.NoiseGenSource;
import mds.AugShotfileFetcher;
import otherSupport.SettingsManager;

/** The ImageProc GUI App to tie all the SWT modules together
 *  
 *
 * This version starts with no source selected and no sinks attached but adds plug-ins
 * for the  W7X Doppler CIS and Spectroscopy systems.
 * 
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:+UseParNewGC -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 * 
 * Environment variables need to be set for the following modules:
 * 
 * Bitflow CameraLink framegrabber for Andor Zyla camera: 
 *    BITFLOW_INSTALL_DIRS /opt/andor/bitflow
 *    LD_LIBRARY_PATH /opt/andor/bitflow/64b/lib
 * 
 * RXTX native library for all serial/serial over USB: 
 *    LD_LIBRARY_PATH /usr/lib64/rxtx
 *    
 *  
 */
public class ImageProcW7X_VideoSpecView {
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		AugShotfileFetcher.defaultInstance().setIsolateFetch(true); //don't crash the program please
		ImageProcUtil.setPreferredSettings(JSONFileSettingsControl.class);
		
		Display swtDisplay = new Display();
		
		///ConcurrencyUtils.setThreadPool(IMSEProc.getThreadPool()); //bad idea :(
		   
		//Read data from PCCIS MDS+ DB (HGW Lab)
		
		//Sources need to be instantiated
		W7XArchiveDBSource adbSrc = new W7XArchiveDBSource();
		
		ImageProcW7X.addStandardSources();
		ImageProcW7X.addStandardSinks();

		GraphUtilProcessor graph1 = new GraphUtilProcessor();
		graph1.setSource(adbSrc);
		
		WindowShell imgWin1 = new WindowShell(swtDisplay, adbSrc, 0);
		imgWin1.rescale(0.2, 0.2, -1, -1, false);
		
		ImageProcUtil.msgLoop(swtDisplay, true);
	}
}