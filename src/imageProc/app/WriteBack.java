package imageProc.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBInfoDesc;
import descriptors.w7x.ArchiveDBJSONInfoDesc;
import descriptors.w7x.ArchiveDBTreeListDesc;
import descriptors.w7x.ArchiveDBJSONInfoDesc.ExtraJsonRequestArg;
import imageProc.database.w7xArchiveDB.Uploader;
import otherSupport.SettingsManager;
import w7x.archive.ArchiveDBFetcher;

/** Program to scan the DataSignals cache for ArchiveDB and write back
 * anything that's in the cache but not in the archive.
 * 
 */
public class WriteBack {

	private static ConcurrentLinkedQueue<ArchiveDBDesc> toUpload = new ConcurrentLinkedQueue<>();
	
	private static Thread[] threads;
	private static Runner[] runners;
	private static ArchiveDBFetcher adb;
	private static ArchiveDBFetcher adbNonLocal;
	
	private static PrintStream uploadedPS; 
	
	private static Uploader uploader;
	
	private static boolean clearExistingFromCache;
	
	public static void main(String[] args) {
		new SettingsManager("minerva", true);
		
		//if(true)
		//	throw new RuntimeException("Writeback doesn't really work anymore, and shouldn't be needed. Ask Oli");
		
		//String searchPath = "/w7x/ArchiveDB/raw/W7X/QSS_DivertorSpectroscopy/PI_CCD_12_1-QSS60OC267_PARLOG/";	
		//String searchPath = "w7x/Test/raw/W7X/";
		String searchPath = "w7x/ArchiveDB/raw/";
		//String searchPath = "/data/imageProcWriteJournal";
		
		int nThreads = 3;
		int initialScanAgeSecs = -1; //-1 = for all time 
		int rescanAgeSecs = -1;//14*24*60*60; //ignore data longer than this many seconds in the past (2 weeks)
		long scanPeriodSecs = 30;
		clearExistingFromCache = true; //delete from cache things that are /found/ in the archive (i.e. checked that they are there)
		
		ArchiveDBFetcher.splitCacheByDay  = true; //all OP1.2b data is old non-split style
		ArchiveDBFetcher.defaultInstance().setWriteInhibit(true);
		
			
		adb = new ArchiveDBFetcher();
		uploader = new Uploader(adb, true);
		adb.setMaxCachedSignals(1000); //basically get everything
		adb.setWriteInhibit(true);
		
		adbNonLocal = new ArchiveDBFetcher();
		adbNonLocal.enableMutableCache(false); //doesn't make sense without this
		adbNonLocal.setEnableMemoryCache(false);
		adbNonLocal.setWriteInhibit(true);
		
		
		if(args.length > 0){
			searchPath = args[0];
		}
		
		try {
			uploadedPS = new PrintStream(ArchiveDBFetcher.getDefaultCachePath() 
																+ "/uploadedLog.txt");			
		} catch (FileNotFoundException e1) {
			throw new RuntimeException(e1);
		}
		
		
		setupPool(nThreads);
		
		long lastScanTimeMS = System.currentTimeMillis();
		int scanAge = initialScanAgeSecs;
		
		while(true){
			long scanStartTimeMS = System.currentTimeMillis();
			System.out.println("Scanning " + searchPath);
			long earliestNanotime = scanAge < 0 ? 0 : Math.max(0, lastScanTimeMS * 1_000_000L - scanAge*1_000_000_000L);
			doScan(searchPath, earliestNanotime, clearExistingFromCache);
			
			lastScanTimeMS = scanStartTimeMS;
			scanAge = rescanAgeSecs;			
			
			System.out.println("Waiting " + ((System.currentTimeMillis() - lastScanTimeMS)/1000L) + " seconds and upload queue empty");
			while((System.currentTimeMillis() - lastScanTimeMS) < (scanPeriodSecs*1000L) || toUpload.size() > 0){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) { }
				System.out.println("Waiting, (" + toUpload.size() + " in queue)");
			}
		}
	}

	private static class Runner implements Runnable {
		public boolean death = false;
		
		@Override
		public void run() {			
			while(!death){
				ArchiveDBDesc desc = toUpload.poll();
				
				if(desc != null){
					try{
						if(uploader.upload(desc)){
							System.out.println(desc + ": Upload OK. ("+toUpload.size()+")");
							synchronized (uploadedPS) {
								uploadedPS.println(desc);
							}
						}else{
							System.out.println(desc + ": Upload aborted. ("+toUpload.size()+")");
						}
					}catch(RuntimeException err){
						System.out.println(desc + ": Upload FAILED: " + err.getMessage() + "("+toUpload.size()+")");
						err.printStackTrace();
					}
				}else{
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) { }
				}
			}
		}
	}
	
	private static void setupPool(int nThreads) {
		threads = new Thread[nThreads];
		runners = new Runner[nThreads];
		for(int i=0; i < nThreads; i++){
			runners[i] = new Runner();
			threads[i] = new Thread(runners[i]);
			threads[i].start();
		}
	}
	
	/** Scan for new cache files and write them up to the archiveDB
	 * if they arn't there. 
	 * 
	 * @param searchPath Base path in archiveDB cache to search, or a list file/journal
	 * @param earliestWrittenFilter	Only consider data starting after this time
	 */
	public static void doScan(String searchPath, long earliestNanotime, boolean clearExistingFromCache){
		
		adb.enableMutableCache(true); //doesn't make sense without this
		adb.setEnableMemoryCache(false); //for better thread safety
		
		
		List<ArchiveDBDesc> descs;
		if((new File(searchPath)).canRead()) {	
			
			descs = new ArrayList<ArchiveDBDesc>();
			FileReader fileReader;
			try {
				fileReader = new FileReader(new File(searchPath));
				BufferedReader input = new BufferedReader(fileReader);
				Stream<String> lines = input.lines();
				Stream<ArchiveDBDesc> descStream = lines.map(x -> new ArchiveDBDesc(x));			
				descStream = descStream.filter(x -> hasCacheFile(adb, x));
				descs = descStream.collect(Collectors.toList());
					
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			
		}else {
			ArchiveDBTreeListDesc searchDesc = new ArchiveDBTreeListDesc(searchPath);
			searchDesc.setNanosRange(earliestNanotime, Long.MAX_VALUE);
			
			Pattern p = Pattern.compile(".*/(20[0-9][0-9])/([0-9]*)/([0-9]*).*");
			Matcher m = p.matcher(searchPath);
			if(m.matches()) {
				int year = Integer.parseInt(m.group(1));
				int month = Integer.parseInt(m.group(2));
				int day = Integer.parseInt(m.group(2));
				long from = ArchiveDBDesc.toNanos(year, month, day, 0, 0, 0, 0);
				long to = ArchiveDBDesc.toNanos(year, month, day, 23, 59, 59, 999_999_999L);
				searchDesc.setNanosRange(from, to);
			}
			
			descs = adb.getCachedSignals(searchDesc);
			System.out.println("Found "+descs.size()+" cache entries under " + searchPath);
		}	
		
		for(ArchiveDBDesc desc : descs){			
			
			if(desc.getFromNanos() < earliestNanotime){
				System.out.println("Too early");
				continue;
			}
			
			if(checkInDatabase(desc))
				continue;
			
			//stupid hack for the descs losing their extra arguments and it not finding the cache file
			if(desc instanceof ArchiveDBJSONInfoDesc) {
				File file = uploader.findFile(desc);
				String fileName = file.toString();
								
				if(fileName.contains("_qm_allowNaN=true"))
					((ArchiveDBJSONInfoDesc)desc).setExtraRequestArguments(ExtraJsonRequestArg.ALLOW_NAN);
		
			}
			
			if(toUpload.contains(desc)){
				System.out.println(desc + ": Already in queue");
			}else{
				toUpload.add(desc);
				System.out.println(desc + ": Added");
			}
			
		}
	}

	public static boolean checkInDatabase(ArchiveDBDesc desc) {
		ArchiveDBDesc checkDesc = new ArchiveDBDesc(desc);
		if(checkDesc.getVersion() < 1)
			checkDesc.setVersion(0);
		boolean inDB = adbNonLocal.hasData(checkDesc);
			
		if(inDB){
			System.out.println(desc + ": Already in DB");
			if(clearExistingFromCache){
				File file = uploader.findFile(desc);
				if(file == null && desc instanceof ArchiveDBJSONInfoDesc){
					ArchiveDBJSONInfoDesc desc2 = new ArchiveDBJSONInfoDesc(desc);
					((ArchiveDBJSONInfoDesc)desc2).setExtraRequestArguments();
					file = uploader.findFile(desc2);
				}
				if(file != null){
					try{
						file.delete();
						System.out.println("Deleted " + file);
					}catch(RuntimeException err){
						err.printStackTrace();
					}
				}
			}
			return true;
		}
		
	
		return false;
	}

	private static boolean hasCacheFile(ArchiveDBFetcher adb, ArchiveDBDesc desc) {
		String fileName = adb.getCacheRoot() + "/" + desc.pathInCache();
		return new File(fileName).canRead();
	}
	
	
	
	
}
