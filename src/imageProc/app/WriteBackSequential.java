package imageProc.app;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import com.google.common.collect.Iterators;

import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBJSONInfoDesc;
import imageProc.database.w7xArchiveDB.Uploader;
import w7x.archive.ArchiveDBFetcher;

/** Sequential cache write-back 
 * 
 * Writes cached data streams and logs to ArchiveDB.
 * 
 * On each pass the program scans the Minerva cache directory (usually /data on diag PCs) and checks if each entry it finds is in the archive.
 * If it is it will delete it if clearExistingFromCache=true.
 * 
 * If not, it will try to upload it. The cache file is left in place whether or not the upload is sucessful.
 * (On the next run of the program, uploaded things will get detected in the archive and then delete). 
 * 
 * * DO NOT RUN MORE THAN 1 COPY OF THE PROGRAM WITH THE SAME CACHE SEARCH PATH! *
 * The WebAPI will get very upset if you multiple uploads are attempted to the same stream.
 * 
 * @author specgui
 *
 */
public class WriteBackSequential {
	public static ArchiveDBFetcher adb = new ArchiveDBFetcher();
	public static Uploader uploader = new Uploader(adb, true);
	private static ArchiveDBFetcher adbNonLocal;
	
	private static boolean clearExistingFromCache = true;
	
	private static String root = "/data";
	
	private static long tMin = Long.MIN_VALUE, tMax = Long.MAX_VALUE;
	
	/** If > Integer.MIN_VALUE then only process entries with this version */
	private static int version = Integer.MIN_VALUE;
	
	private static int nUploaded, nDeleted;
	
	private static DateTime abortTime = null;
	
	//read from list file and write back one-by-one in order
	public static void main(String[] args) {
		
		//String listFile = "/data/w7x/list-100000";
		//String path = "/data/list-WISC1-2023";
		String path = "/data/w7x/ArchiveDB/raw/W7X/";
		
		//args = new String[] { path, "20230216.063" };
		//tMin = 1683000000000000000L; tMax = 1690000000000000000L; //AEA21 post OP2.1 stage calibration (2 May - 22 Jul 2023)
		//tMin = 1695000000000000000L; tMax = 1800000000000000000L; //AEM2 calibration (18 Sep - 14 Nov)
		
		boolean loop = false;
		boolean loopForever = false;		
		String listFile = null;
		long loopDelayTime = 60000;
		
		//args = new String[] { "-a", "-h" };
		
		for(int i=0; i < args.length; i++) {
			String param = null;
			if(args[i].contains("=")) {
				String parts[] = args[i].split("=");
				param = parts[1];
				
			}else if(args[i].startsWith("-") && args[i].length() > 2) {
				param = args[i].substring(2);
				
			}else if(args.length > i+1) {
				param = args[i+1];
			}
			
			
			if(args[i].startsWith("-h")) {
				System.out.println("Usage: -h(elp), -p path, -v (version), -t (fromNano_toNano), -t (YYYYMMDD[.xxx]), -l (loop until {'forever','done',(hh:mm)}), -f pathListFile");
				return;
				
			}else if(args[i].startsWith("-a")) {
				//ignored, used for imageProc.sh to start this
				param = null; //takes no parameter
					
			}else if(args[i].startsWith("-p")) {
				path = param;
				
			}else if(args[i].startsWith("-v")) {
				version = Integer.parseInt(param);
				
			}else if(args[i].startsWith("-t")) {
				if(param.contains("_")) {
					String parts[] = param.split("_");
					tMin = Long.parseLong(parts[0]);
					tMax = Long.parseLong(parts[1]);
					
				}else {				
					int year = Integer.parseInt(param.substring(0,4));
					int month = Integer.parseInt(param.substring(4,6));
					int day = Integer.parseInt(param.substring(6,8));
					
					if(param.contains(".")) {
						int exp = Integer.parseInt(param.split("\\.")[1]);				
						
						System.out.println("Resolving nanotimes for program " + param);
						tMin = adb.getT1(year, month, day, exp, 2);
						tMax = adb.getScenarioEnd(tMin);
					}else {
						ZonedDateTime fromDate = ZonedDateTime.of(year, month, day, 0, 0, 0, 0, ZoneId.of("UTC"));
						tMin = ArchiveDBDesc.toNanos(fromDate);
				        
						ZonedDateTime toDate = ZonedDateTime.of(year, month, day, 23, 59, 59, 99999999, ZoneId.of("UTC"));
				        tMax = ArchiveDBDesc.toNanos(toDate);

					}
				}
			}else if(args[i].startsWith("-f")) {
				listFile = param;
				if(!Files.isRegularFile(Paths.get(path))) {
						System.err.println("Unable to read list file " + listFile);
						System.exit(-1);
				}
				
			}else if(args[i].startsWith("-l")) {
				if(param.equalsIgnoreCase("forever")) {
					loop = true;
					loopForever = true;
					System.out.println("Looping forever, even if done");
					
				}else if(param.equalsIgnoreCase("done")) {
					loop = true;
					loopForever = false;
					System.out.println("Looping until done");
					
				}else {
					LocalTime stopTime = LocalTime.parse(param);
					DateTime now =  new DateTime();					 
					abortTime = now.withTime(stopTime);
					if(abortTime.isBeforeNow())
						abortTime = abortTime.plusDays(1);
					System.out.println("Looping until done or " + abortTime.toString());
				}
				
			}
			
			if(param != null && args.length > i+1 && param == args[i+1])
				i++;
		}
		
		ArchiveDBFetcher.splitCacheByDay  = true; //all OP1.2b data is old non-split style
		ArchiveDBFetcher.defaultInstance().setWriteInhibit(true);
		
		adb = new ArchiveDBFetcher();
		uploader = new Uploader(adb, true);
		adb.setWriteInhibit(true);
		
		adbNonLocal = new ArchiveDBFetcher();
		adbNonLocal.enableMutableCache(false); //doesn't make sense without this
		adbNonLocal.setEnableMemoryCache(false);
		adbNonLocal.setWriteInhibit(true);
		
		
		do {
			nUploaded = 0;
			nDeleted = 0;
			try {
				if(listFile != null) {
					//is list file, read it
					Stream<String> lines = Files.lines(Paths.get(listFile));			
					lines.forEach(l -> process(new ArchiveDBDesc(l), null));
					
				}else {
					//is directory, scan it
					scanDir(path);				
					
				}
			} catch (IOException e) {
				System.err.println("Main scan loop failed");
				e.printStackTrace();
				
			}
			
			System.out.println("Scan complete (or aborted). " + nUploaded + " uploaded, " + nDeleted + " deleted.");
						
			if(abortTime != null && abortTime.isBeforeNow()) {
				System.out.println("Past abort time, exiting.");
				System.exit(-1); //abort, maybe not complete
			}
			
			if(loop) {
				if(!loopForever && nUploaded <= 0 && nDeleted <= 0) {
					System.out.println("Nothing more to do, exiting.");
					break; 
				}
				
				System.out.println("Waiting " + loopDelayTime + " seconds");
				try {
					Thread.sleep(loopDelayTime * 1000);
				}catch(InterruptedException err) { }
			}
			
		}while(loop);
		
		//Finished without loop or as expected
		System.exit(0); 
	}

	private static void scanDir(String path) {
		
		try {
			System.out.print("Scanning " + path + "...");
			DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(path));
			Stream<Path> sortedStream = StreamSupport.stream(stream.spliterator(), false).sorted(Comparator.comparing(Path::toString));
			
			DirectoryStream<Path> stream2 = Files.newDirectoryStream(Paths.get(path));
			int n = Iterators.size(stream2.iterator());
			
			
			int i=0;
			Iterator<Path> it = sortedStream.iterator();
			System.out.println(n + " entries");
			
			while(it.hasNext()) {
				Path p = it.next();
				System.out.print(i + "/" + n + ": ");
				if(processPath(p)) {
					if(i < n-1)
						System.out.println("Skipping all remaining " + (n-i-1) + " entries in directory with wrong version.") ;
					return;
				}
					
				i++;
			};
			
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * @param path
	 * @return true if an entry in the path was the wrong version (so all the others will be too, and the containing folder can be skipped)
	 */
	private static boolean processPath(Path path) {
		if(abortTime != null && abortTime.isBeforeNow())
			throw new RuntimeException("Past specified abort time");
				
		if(Files.isDirectory(path)) {
			scanDir(path.toString());
			return false;
		}
		
		String descPath = path.toString().replaceAll("/data/", "").replaceAll(".nc", "");
		//System.out.println(descPath);
		
		
		
		try {
			//String pathStr = path.get
			String subPath = path.getParent().toString();
			
			if(subPath.startsWith(root))
				subPath = subPath.substring(root.length());
			
			String fileName = path.getFileName().toString();
			
			
			ArchiveDBDesc desc = adb.descFromFileName(fileName, subPath, null);
			if(desc == null) {
				System.out.println("Not data: " + fileName);
				return false;
			}
			
			if(version > Integer.MIN_VALUE) {
				if(desc.getVersion() != version) {
					System.out.println("Entry has V" + desc.getVersion() + " which does not match requested V" + version);
					return true;
				}
			}
			
			if(desc.getFromNanos() < tMin || desc.getFromNanos() > tMax){
				System.out.println("Not in specified nanotime range");
				return false;
			}
			
			if(desc != null)
				process(desc, path.toString());
					
		}catch(RuntimeException err) {
			System.out.println(err.getMessage());
		}
		
		return false;
	}
	

	private static void process(ArchiveDBDesc desc, String pathToFile) {
		if(abortTime != null && abortTime.isBeforeNow())
			throw new RuntimeException("Past specified abort time");
		
		//check file exists first
		File file = null;
		if(pathToFile != null) {
			file = new File(pathToFile);
			
		}else {
			file = uploader.findFile(desc);		
			if(file == null && desc instanceof ArchiveDBJSONInfoDesc){
				ArchiveDBJSONInfoDesc desc2 = new ArchiveDBJSONInfoDesc(desc);
				((ArchiveDBJSONInfoDesc)desc2).setExtraRequestArguments();
				file = uploader.findFile(desc2);
			}
		}
		if(file == null) {
			System.out.println(desc + ": No file.");
			return;
		}
		
		if(checkInDatabase(desc, pathToFile))
			return;
		
		try{
			if(uploader.upload(desc)){
				System.out.println(desc + ": Upload OK.");
				//synchronized (uploadedPS) {
				//	uploadedPS.println(desc);
				//}
				nUploaded++;
			}else{
				System.out.println(desc + ": Upload aborted.");
			}
		}catch(RuntimeException err){
			System.out.println(desc + ": Upload FAILED: " + err.getMessage());
			err.printStackTrace();
		}
	}
	
	public static boolean checkInDatabase(ArchiveDBDesc desc, String pathToFile) {
		ArchiveDBDesc checkDesc = new ArchiveDBDesc(desc);
		if(checkDesc.getVersion() < 1)
			checkDesc.setVersion(0);
		boolean inDB = adbNonLocal.hasData(checkDesc);
			
		if(inDB){
			System.out.print(desc + ": Already in DB.");
			if(!clearExistingFromCache){
				System.out.println();
				return true;
			}
			
			File file;
			if(pathToFile != null) {
				file = new File(pathToFile);
			}else {
				file = uploader.findFile(desc);
				if(file == null && desc instanceof ArchiveDBJSONInfoDesc){
					ArchiveDBJSONInfoDesc desc2 = new ArchiveDBJSONInfoDesc(desc);
					((ArchiveDBJSONInfoDesc)desc2).setExtraRequestArguments();
					file = uploader.findFile(desc2);
				}
			}
			if(file != null){
				try{
					file.delete();
					System.out.println("Deleted " + file);
					nDeleted++;
				}catch(RuntimeException err){
					err.printStackTrace();
				}
			} else {
				System.out.println("No file");
				
			}
			
			return true;
		}
		
	
		return false;
	}
}
