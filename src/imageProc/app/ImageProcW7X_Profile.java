package imageProc.app;


import java.util.ArrayList;
import java.util.List;

import imageProc.control.isoplane.IsoPlaneControl;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.geriStream.GeriStreamSink;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSink;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSinkConfig;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.database.w7xArchiveDB.swt.W7XArchiveDBSourceSWTControl;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.w7x.W7XPilot;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.seriesAvg.SeriesProcessorW7X;
import imageProc.proc.spec.SpecCalProcessor;
import otherSupport.SettingsManager;

/** The ImageProc GUI App to tie all the SWT modules together
 *  Based on a settings-file style profile definition that has to be passed as a parameter
 *
 * All class names must be fully qualified
 * 
 * imageProc.profile.assertHostname=hostname	# Only run this profile if on this host
 *
 * imageProc.profile.source.class={Class}
 * imageProc.profile.source.config={ID} - call loadConfig() for source with this parameter}
 * 
 * imageProc.profile.sinkXX.class={Class} - sink class name
 * imageProc.profile.sinkXX.init=true/false - Instantiate this class? If not just available
 * imageProc.profile.sinkXX.config={config} - Configure the sink, if inited
 * 
 * W7X specifics:
 * 
 *  w7x.archive.dastbase={'W7X_TEST','W7X_ARCHIVE'}
	w7x.archive.path={path} where to save

	imageProc.w7xPilot.systemID
	imageProc.w7xPilot.logPath
	imageProc.w7xPilot.statusComm.defaultPort
	imageProc.w7xPilot.inShot.acquireStartTimeMS
	
	imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault
	imageProc.w7xPilot.inShot.fault.resetPower
	imageProc.w7xPilot.inShot.fault.cameraOffTime
	imageProc.w7xPilot.inShot.acquireTimeout
	imageProc.w7xPilot.inShot.saveTimeout
	imageProc.w7xPilot.inShot.processChainDelay
	imageProc.w7xPilot.inShot.acquireStartTimeout
	imageProc.w7xPilot.ips.host
	imageProc.w7xPilot.ips.username
	imageProc.w7xPilot.ips.password
	imageProc.w7xPilot.ips.faultResetOutlets
	imageProc.w7xPilot.powerCtrlClass
	imageProc.w7xPilot.cameraWarmUpTime
	
	 
 * 
 * 
 * 
 * ----- Memory notes -------
 * All the big image and DAC/ADC allocations are done via direct NIO
 * buffers, so it shouldn't need a huge amount of java heap space, but
 * does need the "-XX:MaxDirectMemorySize" setting to be turned up a bit.
 * 
 * Meanwhile, the heap size needs to be kept as small as possible.
 * 
 * Run with something like:
 * -Xms100M -Xmx512M -XX:MinHeapFreeRatio=10
 * -XX:MaxHeapFreeRatio=40 -XX:MaxDirectMemorySize=4096M
 * 
 * For java > 8, need to allow access to deallocate mapped byte buffers
 * --add-opens java.base/java.nio=ALL-UNNAMED --add-exports java.base/jdk.internal.ref=ALL-UNNAMED
 * 
 * Environment variables need to be set for the following modules:
 * 
 * Bitflow CameraLink framegrabber for Andor Zyla camera: 
 *    BITFLOW_INSTALL_DIRS /opt/andor/bitflow
 *    LD_LIBRARY_PATH /opt/andor/bitflow/64b/lib
 * 
 * RXTX native library for all serial/serial over USB: 
 *    LD_LIBRARY_PATH /usr/lib64/rxtx
 *    
 * Ocean Optics OmniDriver:
 * 		LD_LIBRARY_PATH /opt/omniDriver/OOI_HOME
 *  
 */
public class ImageProcW7X_Profile extends ImageProc_Profile {

	public static void main(String[] args) {
		(new ImageProcW7X_Profile()).run(args);
	}
	
	
	@Override
	protected void sourceSpecial(ImgSource source, SettingsManager profile) {
		//special get for W7XLoad, need to set path in GUI
		if(source instanceof W7XArchiveDBSource && profile.propertyDefined("imageProc.profile.source.setDBPath")){
			String path = profile.getProperty("imageProc.profile.source.setDBPath");
			((W7XArchiveDBSource)source).setTarget(path, -1, -1);
			
			for(ImagePipeController ctrl : source.getControllers()){
				if(ctrl instanceof W7XArchiveDBSourceSWTControl){
					((W7XArchiveDBSourceSWTControl)ctrl).setPath(path);
				}
			}
		}
	}
	
	@Override
	protected void sinkSpecial(ImgSink sink, SettingsManager profile, String basePath, int sinkID) {
		//private void sinkSpecial() {
		if(sink instanceof W7XArchiveDBSink && profile.propertyDefined(basePath + ".sink"+sinkID+".setDBPath")){
			W7XArchiveDBSinkConfig config = ((W7XArchiveDBSink)sink).getConfig();
			config.path = W7XArchiveDBSink.pathFixup(profile.getProperty(basePath + ".sink"+sinkID+".setDBPath"));
		}
	}
	
	@Override
	public List<ImgSource> addStandardSources() {
		List<ImgSource> srcs = super.addStandardSources();
		
		srcs.add(new W7XArchiveDBSource());
		
		return srcs;
	}
	
	@Override
	public void addStandardSinks() {
		super.addStandardSinks();

		ImageProcUtil.addImgPipeClass(W7XArchiveDBSink.class);
		ImageProcUtil.addImgPipeClass(SpecCalProcessor.class);
		ImageProcUtil.addImgPipeClass(FastSpecProcessorW7X.class);
		ImageProcUtil.addImgPipeClass(GeriPilot.class);
		ImageProcUtil.addImgPipeClass(GeriStreamSink.class);
		ImageProcUtil.addImgPipeClass(W7XPilot.class);
		ImageProcUtil.addImgPipeClass(SeriesProcessorW7X.class);
		ImageProcUtil.addImgPipeClass(IsoPlaneControl.class);
	}
}