package imageProc.control.isoplane.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.isoplane.IsoPlaneConfig;
import imageProc.control.isoplane.IsoPlaneControl;
import imageProc.core.ImageProcUtil;


public class ControlGroupSWTControl {
	
	private IsoPlaneControl proc;
	
	private Group swtGroup;
	private Spinner wavelengthSpinner;
	private Spinner widthSpinner;
	//private Text nameTextBox;
	private Combo gratingCombo;
	private Text currentGratingText;
	private Text currentWavelengthText;
	private Combo filterCombo;
	private Text currentFilterText;
	private Button setWavelengthButton;
	private Button setWidthButton;
	private Button setGratingButton;
	
	private Button getWavelengthButton;
	private Button getGratingButton;
	private Button getWidthButton;
	private Button getFilterButton;
	//private Button doThingNowButton;
	//private Button doThingOnTriggerCheckbox;
	
	private boolean dataChangeInhibit = false;

	public ControlGroupSWTControl(Composite parent, IsoPlaneControl proc) {
		this.proc = proc;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setText("Control");
		swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtGroup.setLayout(new GridLayout(4, false));
		
		// -----row 1-------
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Central Wavelength:");
		wavelengthSpinner = new Spinner(swtGroup, SWT.NONE);
		wavelengthSpinner.setDigits(1);
		wavelengthSpinner.setMinimum(0);
		wavelengthSpinner.setMaximum(100_000);
		wavelengthSpinner.setIncrement(1);
		wavelengthSpinner.setSelection(5_000);
		wavelengthSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		currentWavelengthText = new Text(swtGroup, SWT.READ_ONLY);
		currentWavelengthText.setText("?");
		currentWavelengthText.setToolTipText("Central wavelength last reported by spectrometer");
		currentWavelengthText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		setWavelengthButton = new Button(swtGroup, SWT.PUSH);
		setWavelengthButton.setText("  Set Wavelength  ");
		setWavelengthButton.setToolTipText("Set the Central Wavelength");
		setWavelengthButton.setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 1, 1));
		setWavelengthButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setPositionButtonEvent(event); } });

		// ------row 2------
		
		Label lC = new Label(swtGroup, SWT.NONE); lC.setText("Select Grating no. :");
		gratingCombo = new Combo(swtGroup, SWT.NONE);
		gratingCombo.setItems(new String[]{ }); //triple grating turret. Names are placeholders now, in practice put the actual grating name in and l/mm.
		gratingCombo.setToolTipText("Grating number select");
		gratingCombo.setLayoutData(new GridData(SWT.LEFT, SWT.BEGINNING, true, false, 1, 1));
		gratingCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
				
		currentGratingText = new Text(swtGroup, SWT.READ_ONLY);
		currentGratingText.setText("?"); //triple grating turret. Names are placeholders now, in practice put the actual grating name in and l/mm.
		currentGratingText.setToolTipText("Grating number last reported by spectrometer");
		currentGratingText.setLayoutData(new GridData(SWT.LEFT, SWT.BEGINNING, true, false, 1, 1));
		
		setGratingButton = new Button(swtGroup, SWT.PUSH);
		setGratingButton.setText("  Set  ");
		setGratingButton.setToolTipText("Set the Grating");
		setGratingButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		setGratingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setGratingButtonEvent(event); } });

		
		
		//----- row 3 -------
		Label lC2 = new Label(swtGroup, SWT.NONE); lC2.setText("Select Filter Wheel Pos. :");
		filterCombo = new Combo(swtGroup, SWT.NONE);
		filterCombo.setItems(new String[]{ }); //6 position filter wheel. Names are placeholders now, in practice put the actual filter name in.
		filterCombo.setToolTipText("Filter number select");
		filterCombo.setLayoutData(new GridData(SWT.LEFT, SWT.BEGINNING, true, false, 1, 1));
		filterCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		currentFilterText = new Text(swtGroup, SWT.READ_ONLY);
		currentFilterText.setText("?"); //triple grating turret. Names are placeholders now, in practice put the actual grating name in and l/mm.
		currentFilterText.setToolTipText("Filter number last reported by spectrometer");
		currentFilterText.setLayoutData(new GridData(SWT.LEFT, SWT.BEGINNING, true, false, 1, 1));
		
		setGratingButton = new Button(swtGroup, SWT.PUSH);
		setGratingButton.setText("  Set  ");
		setGratingButton.setToolTipText("Set the Filter");
		setGratingButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		setGratingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setFilterButtonEvent(event); } });

		
		// -------row 4-------
		
		Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Motorized Slit Width:");
		widthSpinner = new Spinner(swtGroup, SWT.NONE);
		widthSpinner.setValues(30, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 10, 1000);
		widthSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		setWidthButton = new Button(swtGroup, SWT.PUSH);
		setWidthButton.setText("Set Slit Width (um)");
		setWidthButton.setToolTipText("Set Slit Width (um)");
		setWidthButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		setWidthButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setWidthButtonEvent(event); } });
		
		  // ------row 5------
		
		getWavelengthButton = new Button(swtGroup, SWT.PUSH);
		getWavelengthButton.setText("Get Wavelength");
		getWavelengthButton.setToolTipText("Get Wavelength");
		getWavelengthButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		getWavelengthButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { getWavelengthButtonEvent(event); } });

		getGratingButton = new Button(swtGroup, SWT.PUSH);
		getGratingButton.setText("Get Grating");
		getGratingButton.setToolTipText("Get Grating");
		getGratingButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		getGratingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { getGratingButtonEvent(event); } });

		getWidthButton = new Button(swtGroup, SWT.PUSH);
		getWidthButton.setText("Get Slit Width");
		getWidthButton.setToolTipText("Get Slit Width");
		getWidthButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		getWidthButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { getWidthButtonEvent(event); } });

		getFilterButton = new Button(swtGroup, SWT.PUSH);
		getFilterButton.setText("Get Filter");
		getFilterButton.setToolTipText("Get Filter");
		getFilterButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		getFilterButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { getFilterButtonEvent(event); } });

		ImageProcUtil.addRevealWriggler(swtGroup);
	}
	
	

	/** Called by GUI components to set the config to changes of the GUI */
	protected void settingsChangedEvent(Event event) {
		if(dataChangeInhibit)
			return;
		dataChangeInhibit = true;
		
		IsoPlaneConfig config = proc.getConfig();
		config.targetGrating = gratingCombo.getSelectionIndex() + 1;
		config.targetFilter = filterCombo.getSelectionIndex() + 1;
		config.targetWavelength = wavelengthSpinner.getDigits();
		config.widthOfSlit = widthSpinner.getDigits();
		
		dataChangeInhibit = false;
	}

	protected void setPositionButtonEvent(Event event) {
		IsoPlaneConfig config = proc.getConfig();
		config.targetWavelength = wavelengthSpinner.getSelection();
		
		proc.sendCentralWavelength();
	}
	
	protected void setGratingButtonEvent(Event event) {
		proc.sendGratingNumber();
	}
	
	protected void setFilterButtonEvent(Event event) {		
		proc.sendFilterNumber();
	}
	
	protected void setWidthButtonEvent(Event event) {
		IsoPlaneConfig config = proc.getConfig();
		config.widthOfSlit = widthSpinner.getSelection();
		
		proc.sendSlitWidth();
	}
	
	/** Called by ImageProc to set the GUI to changes of the config */
	public void doUpdate() {
		if(dataChangeInhibit)
			return;
		dataChangeInhibit = true;
		
		IsoPlaneConfig config = proc.getConfig();
		if(!filterCombo.isFocusControl()) {
			filterCombo.setItems(config.availableFilters);
			filterCombo.select(config.targetFilter-1);
		}
		currentFilterText.setText(config.currentFilterName());
		if(!gratingCombo.isFocusControl()) {
			gratingCombo.setItems(config.availableGratings);
			gratingCombo.select(config.targetGrating-1);
		}
		currentGratingText.setText(config.currentGratingName());
		currentFilterText.setText(config.currentFilterName());
		currentWavelengthText.setText(config.currentWavelength());
		
		//nameTextBox.setText(config.nameOfAThing);
		wavelengthSpinner.setSelection((int) config.targetWavelength);
		widthSpinner.setSelection(config.widthOfSlit);
		
		dataChangeInhibit = false;
	}
	
	protected void getWavelengthButtonEvent(Event event) {
		proc.queryCentralWavelength();
	}
	
	protected void getWidthButtonEvent(Event event) {
		proc.querySlitWidth();
	}
	
	protected void getGratingButtonEvent(Event event) {
		proc.queryGratingNumber();
	}
	
	protected void getFilterButtonEvent(Event event) {
		proc.queryFilterNumber();
	}

	public Group getSWTGroup(){ return swtGroup; }
}

