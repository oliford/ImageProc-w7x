package imageProc.control.isoplane;

/** Configuration for the controller module
 * Should be all public and mostly simple variables.
 * Will get serialised to/from JSON files and maybe into the archive one day   
 * 
 * everything should be public
 */
public class IsoPlaneConfig {
	
	public String[] availableGratings = { "600", "500", "400" };
	
	public String[] availableFilters = { "a", "b", "c", "d", "e", "f" };	

	/** Serial port to connect to */
	public String portName = "/dev/ttyS1";
	
	/** BAUD rate to connect serial port with */
	public int baudRate = 9600;
	
	public int widthOfSlit = 30; //safety to prevent slit from fully closing

	/** 1-based index of filter last reported from spectrometer */
	public int currentGrating = -1;
	
	/** 1-based index of filter last reported from spectrometer */
	public int currentFilter = -1;
	
	/** 1-based index of filter to request */
	public int targetGrating = -1;
	
	/** 1-based index of filter to request */
	public int targetFilter = -1;
	
	/** wavelength last reported by spectrometer */
	public double currentWavelength = -1;
	
	/**wavelength to request */
	public double targetWavelength = -1;
	
	public String currentFilterName() {
		if(currentFilter >= 0 && currentFilter < availableFilters.length)
			return availableFilters[currentFilter];
		else 
			return "UNKNOWN("+currentFilter+")";
	}
	
	public String currentGratingName() {
		if(currentGrating >= 0 && currentGrating < availableGratings.length)
			return availableGratings[currentGrating];
		else 
			return "UNKNOWN("+currentGrating+")";
	}
	
	public String currentWavelength() {
		return "   " + currentWavelength + " nm   ";
	}
	
	
}
