package imageProc.control.isoplane;

import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.regex.Pattern;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imageProc.control.arduinoComm.LoggedComm;
import imageProc.control.isoplane.swt.IsoPlaneSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.TextCommandable;
import imageProc.core.EventReciever.Event;
import imageProc.sources.capture.picam.PicamSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Example module for controlling a thing by Serial */
public class IsoPlaneControl extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, 
												EventReciever, LoggedComm.LogUser, TextCommandable {

	/** Current configuration */
	private IsoPlaneConfig config = new IsoPlaneConfig();
	
	/** Class for communicating with serial port (stolen from Arduino controller) */
	protected LoggedComm serialComm;
	
	/** Instance of the GUI controller connected to this */
	private IsoPlaneSWTControl swtController;

	private String lastLoadedConfig;
	
	public IsoPlaneControl() { //hard code this for our systems. Is not expected to ever change.
		serialComm = new LoggedComm(this);
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	private String eventSyncObject1 = new String("eventSyncObject1");
	private String eventSyncObject2 = new String("eventSyncObject2");
	@Override
	public void event(Event event) {
		switch(event) {
		case AcquisitionInit:
		case PreSave:
			ImageProcUtil.ensureFinalUpdate(eventSyncObject1, new Runnable() {
				@Override
				public void run() { doInitEvent();  }
			});
			break;
			
		case AcquisitionStarted:
			ImageProcUtil.ensureFinalUpdate(eventSyncObject2, new Runnable() {
				@Override
				public void run() { doAcquiStartEvent();  }
			});
			break;
			
		default:
		}
	}

	protected void doInitEvent() {
		connectedSource.setSeriesMetaData("IsoPlaneControl/config/gratingNumber", config.targetGrating-1, true); //1 indexed
		connectedSource.setSeriesMetaData("IsoPlaneControl/config/filterNumber", config.targetFilter-1, true); //1 indexed
		connectedSource.setSeriesMetaData("IsoPlaneControl/config/slitWidth", config.widthOfSlit, true);
		connectedSource.setSeriesMetaData("IsoPlaneControl/config/centralWavelength", config.targetWavelength / 10, true); //Angstrom

	}
	
	private void doAcquiStartEvent() {
		queryCentralWavelength();
		queryFilterNumber();
		queryGratingNumber();
		querySlitWidth();
	}
	

	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, IsoPlaneConfig.class);
		
		updateAllControllers();	
	}

	public LoggedComm serialComm() { return serialComm; }

	public IsoPlaneConfig getConfig() { return config; }
	
	
	
	public void queryCentralWavelength(){
		serialComm.send("?NM");
	}
		
	public void sendCentralWavelength(){
		
		serialComm.send(config.targetWavelength / 10 + " GOTO");
	}
	
	public void querySlitWidth(){
		serialComm.send("SIDE-ENT-SLIT");
		serialComm.send("?MICRONS");
	}
	
	public void sendSlitWidth(){
		serialComm.send("SIDE-ENT-SLIT");
		serialComm.send(config.widthOfSlit + " MICRONS" );
	}
	
	public void queryFilterNumber(){
		serialComm.send("?FILTER");
	}
	
	public void sendFilterNumber(){		
		serialComm.send((config.targetFilter) + " FILTER");
		config.currentFilter = -1;
	}
	
	public void queryGratingNumber(){
		serialComm.send("?GRATING");
	}
	
	public void sendGratingNumber(){
		serialComm.send((config.targetGrating) + " GRATING");
		config.currentGrating = -1;
	}


	@Override
	/** Called when a line of text was recieved from the serial port */
	public void receivedLine(String lineStr) {
		
		if(lineStr.contains("FILTER")) {	
			String response[] = lineStr.split(" ");
			try {
				config.currentFilter = Integer.parseInt(response[0]) -1;
				connectedSource.setSeriesMetaData("IsoPlaneControl/atTrigger/filterNumber", config.currentFilter, true);
			}catch(NumberFormatException err) {
				config.currentFilter = -1;
			}
		}
		
		if(lineStr.contains("?FILTER")) {	
			String response[] = lineStr.split(" ");
			try {
				config.currentFilter = Integer.parseInt(response[1]) -1; //one indexed
				connectedSource.setSeriesMetaData("IsoPlaneControl/atTrigger/filterNumber", config.currentFilter, true);
			}catch(NumberFormatException err) {
				config.currentFilter = -1;
			}
		}
				
		
		if(lineStr.contains("GRATING")) {	
			String response[] = lineStr.split(" ");
			try {
				config.currentGrating = Integer.parseInt(response[0]) -1; //one indexed
				connectedSource.setSeriesMetaData("IsoPlaneControl/atTrigger/gratingNumber", config.currentGrating, true);
			}catch(NumberFormatException err) {
				config.currentGrating = -1;
			}
		}
		
		if(lineStr.contains("?GRATING")) {	
			String response[] = lineStr.split(" ");
			try {
				config.currentGrating = Integer.parseInt(response[1]) -1; //one indexed 
				connectedSource.setSeriesMetaData("IsoPlaneControl/atTrigger/gratingNumber", config.currentGrating, true);
			}catch(NumberFormatException err) {
				config.currentGrating = -1;
			}
		}
		
		if(lineStr.contains("GOTO")) {
			String response[] = lineStr.split(" ");
			try {
				config.currentWavelength = Double.parseDouble(response[0]);
				connectedSource.setSeriesMetaData("IsoPlaneControl/atTrigger/centralWavelength", config.currentWavelength, true);
			}catch(NumberFormatException err) {
				config.currentGrating = -1;
			}
		}
			
		if(lineStr.contains("?NM")) {
			String response[] = lineStr.split(" ");
			try {
				config.currentWavelength = Double.parseDouble(response[1]);
				connectedSource.setSeriesMetaData("IsoPlaneControl/atTrigger/centralWavelength", config.currentWavelength, true);
			}catch(NumberFormatException err) {
				config.currentGrating = -1;
			}
		
		}
		
		if(lineStr.contains("?MICRONS")) {
			String response[] = lineStr.split(" ");
			try {
				config.widthOfSlit = Integer.parseInt(response[1]);
				connectedSource.setSeriesMetaData("IsoPlaneControl/atTrigger/slitWidth", config.widthOfSlit, true);
			}catch(NumberFormatException err) {
				config.widthOfSlit = -1;
			}
		
		}
		
		if(lineStr.contains("MICRONS")) {
			String response[] = lineStr.split(" ");
			try {
				config.widthOfSlit = Integer.parseInt(response[0]);
				connectedSource.setSeriesMetaData("IsoPlaneControl/atTrigger/slitWidth", config.widthOfSlit, true);
			}catch(NumberFormatException err) {
				config.widthOfSlit = -1;
			}
		
		}
		
		
		logChanged();
		
		
	}

	@Override
	/** Called when anything is written to the serial log. 
	 * Really only to tell the GUI to update all the boxes and things */
	public void logChanged() {
		updateAllControllers(); //force update of GUI components
	}

	@Override
	/** Called when serial port is connected.
	 * Add initial configuration here */
	public void connected() {
		//config.targetWavelength = 0;
		//config.widthOfSlit = 30;
		//config.targetGrating = 1;
		//config.targetFilter = 1;
		
		sendCentralWavelength();
		sendFilterNumber();
		sendGratingNumber();
		sendSlitWidth();
	}

	
	/* --- Some things from ImageProc structure --- */
	@Override
	public boolean isIdle() { return true;	}
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new IsoPlaneSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}

	@Override
	/** Receives text commands from other modules, e.g. PilotStatusComm.
	 * @param command Text command as recieved
	 * @param pwReturn Writer connected to the return channel (e.g. TCP/IP socket).
	 * 
	 * This is called from the comms thread, so don't take too long (< 1 second).
	 * If using a worker thread to do something, synchronise on the pwReturn before sending anything. 
	 */
	public void command(String command, PrintWriter pwReturn) {
		String pwPrefix = "ModuleCommand " + getClass().getSimpleName() + " ";  
		
		if(command == null)
			return;
		
		try {
			
			//separate by ',' '=' or whitespace
			String parts[] = command.split("[\\s=,]");
			
			
			if(parts[0].equalsIgnoreCase("isConnected")) {
				pwReturn.println(pwPrefix + " isConnected =" + serialComm.isConnected());
			
			}else if(parts[0].equalsIgnoreCase("getConfig")) {
					Gson gson = new GsonBuilder()
									.serializeSpecialFloatingPointValues()
									.serializeNulls()
									//.setPrettyPrinting() //(we don't want newlines)
									.create(); 

				String json = gson.toJson(config);
					
				pwReturn.println(pwPrefix + " getConfig = " + json);
									
				
			} else if(parts[0].equalsIgnoreCase("setCentralWavelength")) {
				//set in our local configuration
				config.targetWavelength = Double.parseDouble(parts[1]);
				
				//send to spectrometer (if connection active)
				sendCentralWavelength();				
				
				//respond
				pwReturn.println(pwPrefix + " setCentralWavelength OK");
				
			}else if(parts[0].equalsIgnoreCase("setFilterNumber")) {
				config.targetFilter = Integer.parseInt(parts[1]);
				sendFilterNumber();
				pwReturn.println(pwPrefix + " setFilterNumber OK");
				
			}else if(parts[0].equalsIgnoreCase("setGratingNumber")) {
				config.targetGrating = Integer.parseInt(parts[1]);
				sendGratingNumber();
				pwReturn.println(pwPrefix + " setGratingNumber OK");
				
			}else if(parts[0].equalsIgnoreCase("setSlitWidth")) {
				config.widthOfSlit = Integer.parseInt(parts[1]);
				sendSlitWidth();
				pwReturn.println(pwPrefix + " setSlitWidth OK");
			
			}else if(parts[0].equalsIgnoreCase("queryParameters")) {
				//Send commands to spectrometer to request the parameters, which will then
				//be saved in the local configuration
				queryCentralWavelength();
				queryFilterNumber();
				queryGratingNumber();
				querySlitWidth();

				pwReturn.println(pwPrefix + " queryParamters OK");
				
			}else if(parts[0].equalsIgnoreCase("getCentralWavelength")) {
				//returns the current central wavelength in our local config
				pwReturn.println(pwPrefix + " getCentralWavelength =" + config.targetWavelength);
			
			}else if(parts[0].equalsIgnoreCase("getFilterNumber")) {
				//returns the current central wavelength in our local config
				pwReturn.println(pwPrefix + " getFilterNumber =" + config.targetFilter);
			
			}else if(parts[0].equalsIgnoreCase("getGratingNumber")) {
				//returns the current central wavelength in our local config
				pwReturn.println(pwPrefix + " getGratingNumber =" + config.targetGrating);
			
			}else if(parts[0].equalsIgnoreCase("getSlitWidth")) {
				//returns the current central wavelength in our local config
				pwReturn.println(pwPrefix + " getSlitWidth =" + config.widthOfSlit);
			
			
			}else{
				pwReturn.println("ERROR: Unrecognised command " + parts[0]);
			}
			
			//update GUI for any config changes
			updateAllControllers();
			
		}catch(RuntimeException err){
			logr.log(Level.WARNING, "Error processing module text command." + command, err);
			pwReturn.println("ERROR: " + err.toString());
		}
	}
}
