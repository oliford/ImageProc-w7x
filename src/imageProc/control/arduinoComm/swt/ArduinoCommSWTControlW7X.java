package imageProc.control.arduinoComm.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;

import imageProc.control.arduinoComm.ArduinoCommHangerW7X;
import imageProc.control.arduinoComm.ArduinoCommState;

public class ArduinoCommSWTControlW7X extends ArduinoCommSWTControl {
	
	private ArduinoCommHangerW7X procW7X;
	
	
	public ArduinoCommSWTControlW7X(ArduinoCommHangerW7X procW7X, Composite parent, int style) {
			super(procW7X, parent, style);
			this.procW7X = procW7X;
			
			
	}
	
	@Override
	protected void doUpdate() {
		super.doUpdate();
	}
	
}
