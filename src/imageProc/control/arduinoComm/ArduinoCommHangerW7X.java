package imageProc.control.arduinoComm;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import algorithmrepository.Mat;
import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBDesc.Type;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControlW7X;
import imageProc.core.ImagePipeController;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.proc.fastSpec.FastSpecConfigW7X;
import oneLiners.OneLiners;
import w7x.archive.ArchiveDBFetcher;
import w7x.archive.writing.ParameterLog;
import w7x.archive.writing.VersionData;
import w7x.archive.writing.W7xArchiveWrite;

public class ArduinoCommHangerW7X  extends ArduinoCommHanger {

	ArduinoCommConfigW7X configW7X;

	
	@Override
	public ArduinoCommConfigW7X config() { 
		if(configW7X == null)
			configW7X = new ArduinoCommConfigW7X(); //doesn't get init'ed in term for superclass constructor
		return configW7X;
	}
	

	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new ArduinoCommSWTControlW7X(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}
	
	public ArduinoCommSWTControlW7X getSWTController(){ return (ArduinoCommSWTControlW7X)swtController; }
	

	@Override
	public void saveStateNow() {
		ArduinoCommState state = state();
		String path = config().saveState.path;
		try {
			
			ArchiveDBDesc desc = new ArchiveDBDesc(path);
			desc.setFromNanos(ArchiveDBDesc.toNanos(ZonedDateTime.now()));
			desc.setToNanos(desc.getFromNanos());
			
			int ver = 1;
					
			
			int version = ver;
			String reason = "State";
			String codeRelease = "ArduinoCommHangerW7X";
			String producer = null;
			String analysisEnvironment = null;
			
			ParameterLog parLog2 = new ParameterLog(desc, reason, codeRelease);
			
			ArchiveDBFetcher fetcher = new ArchiveDBFetcher();
			
			ArchiveDBDesc desc2 = new ArchiveDBDesc();
			desc2.setDatabase(desc.getDatabase());
			desc2.setFromPartialPath(desc.getPartialPath());
			desc2.setType(Type.PARLOG);
			desc2.setVersion(version);
			
			VersionData versionObject = new VersionData(desc2, reason, producer, codeRelease, analysisEnvironment);
			versionObject.send();		
			
			JsonObject jsonObj = new JsonObject(); 
			
			jsonObj.add("label", W7xArchiveWrite.toJsonElement("parms"));									
			jsonObj.add("dimensions", W7xArchiveWrite.toJsonElement(new long[] { desc.getFromNanos(), desc.getToNanos() }));					
									
			Gson gson = new GsonBuilder()
					.serializeSpecialFloatingPointValues()
					.serializeNulls()
					/*.addSerializationExclusionStrategy(new ExclusionStrategy() {							
						@Override
						public boolean shouldSkipField(FieldAttributes attrbs) {								
							return attrbs.getName().startsWith("fit") ||
									attrbs.getName().startsWith("inferred");
						}
						
						@Override
						public boolean shouldSkipClass(Class<?> arg0) { return false; }
					})*/
					.create();
			
			JsonElement jsonElem = gson.toJsonTree(state);
			JsonObject values = jsonElem.getAsJsonObject();
			
			JsonArray outerValuesArray = new JsonArray();
			outerValuesArray.add(values);
			jsonObj.add("values", outerValuesArray);
			
			OneLiners.textToFile("/tmp/aa.json", jsonObj.toString());
			
			W7xArchiveWrite.send(W7xArchiveWrite.getDefaultBaseUri(), desc2, jsonObj);
	
			desc2.setVersion(ver);
			lastStateSaveStatus = "Written " + desc2 + "\n at "  + desc.getFromNanos() + " = " + ArchiveDBDesc.fromNanos(desc.getFromNanos());
			logr.info(lastStateSaveStatus);
			updateAllControllers(); 
			
		}catch(RuntimeException err) {
			lastStateSaveStatus = "Error saving state to " + path + ":" + err.getMessage();
			logr.log(Level.WARNING, lastStateSaveStatus, err);
			updateAllControllers();
		}
	}
	
	@Override
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		configW7X = gson.fromJson(jsonString, ArduinoCommConfigW7X.class);
		
		updateAllControllers();	
	
	}
}
