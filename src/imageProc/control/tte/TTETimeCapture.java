package imageProc.control.tte;

import java.util.logging.Level;
import java.util.logging.Logger;

import lttev2JNI.LibTriggerTiming;
import lttev2JNI.LibTriggerTiming.TTEVersion;

public class TTETimeCapture implements Runnable {
	public boolean death = false;
	public Thread thread;
	public Logger logr = Logger.getLogger(this.getClass().getName());
	
	private TTEControl ctrl;
	
	public TTETimeCapture(TTEControl ctrl) {
		this.ctrl = ctrl;
	}
	
	private LibTriggerTiming libTTE;
	
	private int nCollected = 0;
	
	private long timestamps[];
	
	private TTEVersion version;
	
	@Override
	public void run() {
		
		TTEConfig config = ctrl.getConfig();
		
		try {
			ctrl.setStatus("Connecting...");
					
			LibTriggerTiming tte = new LibTriggerTiming(config.hostname, config.port);
			
			ctrl.setStatus("Initialising...");
			
			version = tte.getVersion();
			
			tte.clockCreate(config.timeCapture.clockID, 0);
			
			tte.clockSetMode(config.timeCapture.clockID, LibTriggerTiming.Clock_mode_absolute, -1, -1);
			
			tte.triggerInCreate(config.timeCapture.triggerID, 1);
			
			tte.triggerInEnableInput(config.timeCapture.triggerID, 1);
			
			tte.triggerInEnableOutput(config.timeCapture.triggerID, 1);
			
			tte.timeCaptureCreate(config.timeCapture.captureID, 1);
			
			  //tte.timeCaptureSetMode(id, tte.TimeCapture_mode_periodic);
			
			tte.timeCaptureConnectInputTo(config.timeCapture.captureID, LibTriggerTiming.SIGTrigger_output, config.timeCapture.channel);
			
			tte.timeCaptureEnableInput(config.timeCapture.captureID, 1);
			
			logr.log(Level.INFO, "Setup ok, waiting for timestamps...");
			
			if(config.timeCapture.matchNumberOfImages)
				config.timeCapture.numToAllocate = ctrl.getConnectedSource().getNumImages();
			
			timestamps = new long[config.timeCapture.numToAllocate];
			ctrl.getConnectedSource().setSeriesMetaData(config.timeCapture.metadataName, timestamps, true);
			
			ctrl.setStatus("Ready. Waiting for first timestamp.");
			
			long lastWalltime = System.currentTimeMillis();
			nCollected = 0;
			long lastNum = 0;		  
			while(!death){

				long num = tte.timeCaptureGetTime(config.timeCapture.captureID, 1);
				long wallTime = System.currentTimeMillis();
				if(num > lastNum){
					logr.log(Level.FINER, String.format("\n%d, dt=%.03f ms, dWall=%d ms", num, (num-lastNum)/1e6, wallTime-lastWalltime));

					timestamps[nCollected] = num;
					
					lastNum = num;
					lastWalltime = wallTime;
					nCollected++;
					ctrl.setStatus("Collected " + nCollected + " timestamps");

				}else{
					//printf(".");
				}

				try {
					Thread.sleep(0, (int)config.timeCapture.pollPeriodUS*1000);
				}catch(InterruptedException err) { }

			}

			ctrl.setStatus("Stopped.");

		}catch(Throwable t) {			
			logr.log(Level.SEVERE, "Error in communication with TTE module/server", t);			
			ctrl.setStatus("ERROR:" + t.getMessage());
		}finally {
			if(libTTE != null) {
				try {
					libTTE.destroy();
				}catch(Throwable t) {}
			}
		}

	}
	
	public int getNumCollected() { return timestamps == null ? -1 : nCollected; }

	public int getNumAllocated() { return timestamps == null ? -1 : timestamps.length; }

	public TTEVersion getTTEVersion() { return version; }

}
