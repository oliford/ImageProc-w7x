package imageProc.control.tte.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.control.tte.TTEConfig;
import imageProc.control.tte.TTEControl;
import imageProc.control.tte.TTEConfig.TimeCaptureConfig;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import uk.co.oliford.jolu.OneLiners;

/** GUI control for example serial tte control module 
 * {@snippet :
 * 
 * }
 * */ 
public class TTESWTControl implements ImagePipeController, ImagePipeSWTController  {

	private TTEControl proc;		
	private Group swtGroup;
	private CTabItem thisTabItem;
	
	private Label statusLabel;
	private Text hostnameText;
	private Text portText;
	private Spinner channelSpinner;
	private Spinner numToAllocateText;
	private Button autosetNumToAllocateCheckbox;
	private Text metadataNameText;
	
	private Button startOnAcqInitCheckbox;
	private Button stopOnGlobalStopCheckbox;
	
	private Button startButton;
	private Button stopButton;
	
	private SWTSettingsControl settingsCtrl;
	
	public TTESWTControl(TTEControl proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Arduino Comm");
		swtGroup.setLayout(new GridLayout(6, false));

		Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");  
		statusLabel = new Label(swtGroup, SWT.NONE);
		statusLabel.setText("init\n.\n.\n");
		statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		Label lH = new Label(swtGroup, SWT.NONE); lH.setText("Host:");  
		hostnameText = new Text(swtGroup, SWT.NONE);
		hostnameText.setText("");
		hostnameText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		hostnameText.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Port:");  
		portText = new Text(swtGroup, SWT.NONE);
		portText.setText("");
		portText.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		portText.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Group swtTimeCaptureGroup = new Group(swtGroup, SWT.BORDER);
		swtTimeCaptureGroup.setText("Timestamp capture");
		swtTimeCaptureGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		swtTimeCaptureGroup.setLayout(new GridLayout(6, false));
		
		Label lC = new Label(swtTimeCaptureGroup, SWT.NONE); lC.setText("Channel:");  
		channelSpinner = new Spinner(swtTimeCaptureGroup, SWT.NONE);
		channelSpinner.setValues(0, 0, 100, 0, 1, 10);
		channelSpinner.setToolTipText("Which TTE channel to collect timestamps for");
		channelSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		channelSpinner.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lNA = new Label(swtTimeCaptureGroup, SWT.NONE); lNA.setText("Allocation:");  
		numToAllocateText = new Spinner(swtTimeCaptureGroup, SWT.NONE);
		numToAllocateText.setValues(0, 0, Integer.MAX_VALUE, 0, 100, 1000);
		numToAllocateText.setToolTipText("Number of timestamps to allocate metadata array for.");
		numToAllocateText.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		numToAllocateText.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		autosetNumToAllocateCheckbox = new Button(swtTimeCaptureGroup, SWT.CHECK);
		autosetNumToAllocateCheckbox.setText("Auto");
		autosetNumToAllocateCheckbox.setToolTipText("Automatically set the number of timestramps to allocate to the number of images allocated in the image source.");
		autosetNumToAllocateCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		autosetNumToAllocateCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });


		Label lM = new Label(swtTimeCaptureGroup, SWT.NONE); lM.setText("Metadata Name:");  
		metadataNameText = new Text(swtTimeCaptureGroup, SWT.NONE);
		metadataNameText.setText("");
		metadataNameText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		metadataNameText.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		
		Label lAS = new Label(swtGroup, SWT.NONE); lAS.setText("Auto:");  
		startOnAcqInitCheckbox = new Button(swtGroup, SWT.CHECK);
		startOnAcqInitCheckbox.setText("Start");
		startOnAcqInitCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		startOnAcqInitCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		stopOnGlobalStopCheckbox = new Button(swtGroup, SWT.CHECK);
		stopOnGlobalStopCheckbox.setText("Stop");
		stopOnGlobalStopCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		stopOnGlobalStopCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		
		Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("");  
		startButton = new Button(swtGroup, SWT.PUSH);
		startButton.setText("Start");
		startButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		startButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.startCapture(); } });

		stopButton = new Button(swtGroup, SWT.PUSH);
		stopButton.setText("Stop");
		stopButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		stopButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) {  proc.stopCapture(); } });

		Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");  
		lB.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 6, 1));
		
		generalControllerUpdate();	
	}

	protected void settingsChangedEvent(Event event) {
		TTEConfig config = proc.getConfig();
		
		config.hostname = hostnameText.getText();
		config.port = OneLiners.mustParseInt(portText.getText());
		config.timeCapture.channel = channelSpinner.getSelection();
		config.timeCapture.numToAllocate =  numToAllocateText.getSelection();
		config.timeCapture.matchNumberOfImages = autosetNumToAllocateCheckbox.getSelection();
		config.timeCapture.metadataName = metadataNameText.getText();
		config.startOnAcquisitionInit = startOnAcqInitCheckbox.getSelection();
		config.stopOnGlobalStop = stopOnGlobalStopCheckbox.getSelection();
	}

	/* ---- Some ttes we need to define for ImageProc ----- */
	@Override
	public void destroy() { proc.destroy(); }
	@Override
	public void setTabItem(CTabItem sinkTab) { this.thisTabItem = sinkTab; }
	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc; }
	@Override
	public Object getInterfacingObject() { return swtGroup; }

	/** Called by ImageProc core to update the GUI with the processor's current state */
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}
	
	private void doUpdate(){
		TTEConfig config = proc.getConfig();
		
		if(config.timeCapture == null)
			config.timeCapture = new TimeCaptureConfig();
		
		statusLabel.setText(proc.getStatus()
				+ "\nTTE Version = " + proc.getTTEVersion()
				+ "\nAllocated = " + proc.getNumAllocated()
				+ "\nCollected = " + proc.getNumCollected());
		ImageProcUtil.setColorByStatus(proc.getStatus(), statusLabel);
		
		hostnameText.setText(config.hostname);
		portText.setText(Integer.toString(config.port));
		channelSpinner.setSelection(config.timeCapture.channel);
		numToAllocateText.setSelection(config.timeCapture.numToAllocate);
		autosetNumToAllocateCheckbox.setSelection(config.timeCapture.matchNumberOfImages);
		metadataNameText.setText(config.timeCapture.metadataName);
		startOnAcqInitCheckbox.setSelection(config.startOnAcquisitionInit);
		stopOnGlobalStopCheckbox.setSelection(config.stopOnGlobalStop);
		
		settingsCtrl.doUpdate();
	}
	
}
