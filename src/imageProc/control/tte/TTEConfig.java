package imageProc.control.tte;


/** Configuration for the controller module
 * Should be all public and mostly simple variables.
 * Will get serialised to/from JSON files and maybe into the archive one day   
 * 
 * everytte should be public
 */
public class TTEConfig {

	/** Host where LTTEv2 server is running */
	public String hostname = "localhost";
	
	/** Port number of LTTEv2 server */
	public int port = 55555;
	
	/** Program TTE and start time capture on ImageProc event */
	public boolean startOnAcquisitionInit = true;
	
	public boolean stopOnGlobalStop = true;
	
	public static class TimeCaptureConfig {
		
		/** TTE channel to work with */
		public int channel = 1;
		
		public int clockID = 1;
		public int captureID = 1;		
		public int triggerID = 1;
		
		/** Poll period [us] */
		public long pollPeriodUS = 100;
		
		public String metadataName = "TTEControl/TimeCapture_ch01";
	
		/** Number of images to allocate (maybe set by matchNumberOfImages) */
		public int numToAllocate = 10000;
		
		/** If true, numToAllocate is set to the number of images (allocated) in the image source */
		public boolean matchNumberOfImages = true;
	}
	
	public TimeCaptureConfig timeCapture = new TimeCaptureConfig();
}
