package imageProc.control.tte;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imageProc.control.arduinoComm.LoggedComm;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.tte.swt.TTESWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.EventReciever.Event;
import lttev2JNI.LibTriggerTiming;
import lttev2JNI.LibTriggerTiming.TTEVersion;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** Example module for controlling a tte by Serial */
public class TTEControl extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, EventReciever {

	/** Current configuration */
	private TTEConfig config = new TTEConfig();

	/** Last status message */
	protected String status = "init";

	/** Instance of the GUI controller connected to this */
	private TTESWTControl swtController;

	private String lastLoadedConfig;
	
	private TTETimeCapture capture = null;
		
	public TTEControl() {
		
	}
	
	@Override
	public void event(Event event) {
		switch(event) {
		case AcquisitionInit:
			if(config.startOnAcquisitionInit)
				startCapture();
			break;
		case GlobalAbort:
		case AcquisitionStopped:
			if(config.stopOnGlobalStop)
				stopCapture();
			break;
		default:
			break;
		}		
	}
	
	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, TTEConfig.class);
		
		updateAllControllers();	
	}

	public TTEConfig config() { return config; }
	
		
	/* --- Some ttes from ImageProc structure --- */
	@Override
	public boolean isIdle() { return true;	}
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new TTESWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}
	
	
	public void saveStateNow() {
		throw new RuntimeException("No general implementation for saveState");
	}

	public void startCapture() {
		stopCapture();
		
		capture = new TTETimeCapture(this);
		
		capture.thread = new Thread(capture);
		capture.thread.start();
	}

	public void stopCapture() {
		if(capture == null)
			return;
		
		capture.death = true;
		if(capture.thread != null && capture.thread.isAlive())
			capture.thread.interrupt();
	}
	
	public TTEConfig getConfig() { 	return config;	}
	
	public String getStatus() { return status;	}

	public void setStatus(String status) {
		this.status = status;
		updateAllControllers();
	}

	public int getNumAllocated() { return capture != null ? capture.getNumAllocated() : -1; }
	
	public int getNumCollected() { return capture != null ? capture.getNumCollected() : -1; }

	public TTEVersion getTTEVersion() { return capture != null ? capture.getTTEVersion() : null; }
	
	
}
