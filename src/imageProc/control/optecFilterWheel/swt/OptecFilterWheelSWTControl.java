package imageProc.control.optecFilterWheel.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.control.optecFilterWheel.OptecFilterWheelControl;
import imageProc.control.optecFilterWheel.OptecFilterWheelConfig;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;

/** GUI control for example serial thing control module 
 * {@snippet :
 * 
 * }
 * */ 
public class OptecFilterWheelSWTControl implements ImagePipeController, ImagePipeSWTController  {

	private OptecFilterWheelControl proc;	
	
	private Group swtGroup;
	private CTabItem thisTabItem;
	
	private Label statusLabel;
	private Label wheelLabel;
	private Label positionLabel;
	
	private Combo portDeviceTextbox;
	private Button connectButton;
	private Button refreshButton;
	private Button disconnectButton;
	
	private Button pos1Button;
	private Button pos2Button;
	private Button pos3Button;
	private Button pos4Button;
	private Button pos5Button;
	
	private CTabFolder swtTabFoler;	
	
	/**
	private CTabItem swtSerialTab;
	private SerialSWTGroup serialGroup;
	
	private CTabItem swtExampleTab;	
	private ExampleGroupSWTControl exampleGroup;
	**/
	//private CTabItem swthsfwTab;
	//private hsfwSWTGroup hsfwGroup;
	
	private CTabItem mcSWTTab;
	private MultiConfigPanel mcGroup;
	
	private CTabItem nsTab;
	private NameSettings nsGroup;
	
	private Button multiConfigEnableCheckbox;
	private Button multiConfigTrigger;
	private Button multiConfigSoft;
	
	private SWTSettingsControl settingsCtrl;
	
	public OptecFilterWheelSWTControl(OptecFilterWheelControl proc, Composite parent, int style) {
		this.proc = proc;
		
		OptecFilterWheelConfig config = proc.getConfig();
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Optec FW Control");
		swtGroup.setLayout(new GridLayout(6, false));
		
	    Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
	    statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
        wheelLabel = new Label(swtGroup, SWT.NONE);
        wheelLabel.setText("Wheel: None ");
        wheelLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2,1));
        
        positionLabel = new Label(swtGroup, SWT.NONE);
        positionLabel.setText("Pos: None");
        positionLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1,1));
        
        Label lPN = new Label(swtGroup, SWT.NONE); lPN.setText("Device:");
	    portDeviceTextbox = new Combo(swtGroup, SWT.MULTI);
	    portDeviceTextbox.setItems(proc.serial_numbers_str.toArray(new String[0]));
	    portDeviceTextbox.select(0);
	    portDeviceTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
	    portDeviceTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { portDeviceTextboxEvent(event); } });

	    connectButton = new Button(swtGroup, SWT.PUSH);
	    connectButton.setText("Connect");
	    connectButton.setToolTipText("Open the chosen filter wheel");
	    connectButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    connectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { connectButtonEvent(event); } });

	    refreshButton = new Button(swtGroup, SWT.PUSH);
	    refreshButton.setText("Refresh");
	    refreshButton.setToolTipText("Looking for connected filter wheels.");
	    refreshButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    refreshButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { refreshButtonEvent(event); } });

	    disconnectButton = new Button(swtGroup, SWT.PUSH);
	    disconnectButton.setText("Disconnect");
	    disconnectButton.setToolTipText("Close connection with the open filter wheel");
	    disconnectButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    disconnectButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { disconnectButtonEvent(event); } });

	    Label filters = new Label(swtGroup, SWT.NONE); filters.setText("Manual selection:");
	    pos1Button = new Button(swtGroup, SWT.PUSH);
	    pos1Button.setText(config.filterNames.get(0));
	    pos1Button.setToolTipText("Moves FW to first position");
	    pos1Button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    pos1Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pos1ButtonEvent(event); } });
	    
	    pos2Button = new Button(swtGroup, SWT.PUSH);
	    pos2Button.setText(config.filterNames.get(1));
	    pos2Button.setToolTipText("Moves FW to second position");
	    pos2Button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    pos2Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pos2ButtonEvent(event); } });
	    
	    pos3Button = new Button(swtGroup, SWT.PUSH);
	    pos3Button.setText(config.filterNames.get(2));
	    pos3Button.setToolTipText("Moves FW to third position");
	    pos3Button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    pos3Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pos3ButtonEvent(event); } });
	    
	    pos4Button = new Button(swtGroup, SWT.PUSH);
	    pos4Button.setText(config.filterNames.get(3));
	    pos4Button.setToolTipText("Moves FW to fourth position");
	    pos4Button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    pos4Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pos4ButtonEvent(event); } });
	    
	    pos5Button = new Button(swtGroup, SWT.PUSH);
	    pos5Button.setText(config.filterNames.get(4));
	    pos5Button.setToolTipText("Moves FW to fifth position");
	    pos5Button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    pos5Button.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pos5ButtonEvent(event); } });

	    Label lMCa = new Label(swtGroup, SWT.NONE); lMCa.setText("Multi Config:");
		
	    multiConfigEnableCheckbox = new Button(swtGroup, SWT.CHECK);
		multiConfigEnableCheckbox.setText("Enable");
		multiConfigEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		multiConfigEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
		
		multiConfigTrigger = new Button(swtGroup, SWT.PUSH);
		multiConfigTrigger.setText("Manual Trigger");
		multiConfigTrigger.setToolTipText("This button manually triggers the multiConfig sequence");
		multiConfigTrigger.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		multiConfigTrigger.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigTrigger(event); } });
		
		multiConfigSoft = new Button(swtGroup, SWT.CHECK);
		multiConfigSoft.setText("S/W Trigger");
		multiConfigSoft.setToolTipText("If checked, multiConfig sequence is initialized by a software trigger (still not working)");
		multiConfigSoft.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		multiConfigSoft.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigSoft(event); } });
		
	    swtTabFoler = new CTabFolder(swtGroup, SWT.BORDER);
		swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, true, 6, 1));

		mcGroup = new MultiConfigPanel(swtTabFoler, SWT.NONE, proc);
		mcSWTTab = new CTabItem(swtTabFoler, SWT.NONE);
		mcSWTTab.setControl(mcGroup.getSWTGroup());
		mcSWTTab.setText("Multi settings");

		nsGroup = new NameSettings(swtTabFoler, SWT.NONE, proc);
		nsTab = new CTabItem(swtTabFoler, SWT.NONE);
		nsTab.setControl(nsGroup.getSWTGroup());
		nsTab.setText("Rename Wheel");
		
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		generalControllerUpdate();	
		doUpdate();
	}

	/* ---- Some things we need to define for ImageProc ----- */
	@Override
	public void destroy() { 
		proc.destroy();
	}
	
	@Override
	public void setTabItem(CTabItem sinkTab) { this.thisTabItem = sinkTab;}
	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc; }
	@Override
	public Object getInterfacingObject() { return swtGroup; }

	/** Called by ImageProc core to update the GUI with the processor's current state */
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}
	
	private void doUpdate(){
		configToGUI();
		nsGroup.configToGUI();
		mcGroup.configToGUI();
		settingsCtrl.doUpdate();	
	}
	
	private void _updatePos() {
		OptecFilterWheelConfig config = proc.getConfig();
		
		if (config.moveFailed) {
			positionLabel.setText("Move failed, last pos: " +config.posName);
			} else {
				positionLabel.setText("Pos: " + config.posName);
				}
		}
	
	private void _updateWheelName() {
		OptecFilterWheelConfig config = proc.getConfig();
		wheelLabel.setText("Wheel: " +config.wheelname);
	}
	
	private void _updateStatus() {
		OptecFilterWheelConfig config = proc.getConfig();
		statusLabel.setText(proc.isConnected() ? "Open" : "Not open/Closed");	
	}
	
	private void _updateFilterNames() {
		OptecFilterWheelConfig config = proc.getConfig();	
		pos1Button.setText(config.filterNames.get(0));
		pos2Button.setText(config.filterNames.get(1));
		pos3Button.setText(config.filterNames.get(2));
		pos4Button.setText(config.filterNames.get(3));
		pos5Button.setText(config.filterNames.get(4));
	}
	
	public void configToGUI() {
		OptecFilterWheelConfig config = proc.getConfig();
		
		_updateStatus();
		_updateWheelName();
		_updateFilterNames();
		_updatePos();
		
		if (proc.isConnected()) {
			connectButton.setEnabled(false);
			disconnectButton.setEnabled(true);
		} else if (proc.serial_number_ptr==null) {
			connectButton.setEnabled(false);
			disconnectButton.setEnabled(false);
		} else {
			connectButton.setEnabled(true);
			disconnectButton.setEnabled(false);
		}
		if (config.multiConfigAllowed) {
			multiConfigEnableCheckbox.setSelection(true);
			pos1Button.setEnabled(false);
			pos2Button.setEnabled(false);
			pos3Button.setEnabled(false);
			pos4Button.setEnabled(false);
			pos5Button.setEnabled(false);	
		} else {
			multiConfigEnableCheckbox.setSelection(false);
			pos1Button.setEnabled(true);
			pos2Button.setEnabled(true);
			pos3Button.setEnabled(true);
			pos4Button.setEnabled(true);
			pos5Button.setEnabled(true);
		}
		if (config.multiConfigSoftStart) {
			multiConfigSoft.setSelection(true);
			multiConfigTrigger.setEnabled(false);
		} else {
			multiConfigSoft.setSelection(false);
			multiConfigTrigger.setEnabled(true);
			}
		
		mcGroup.configToGUI();
		nsGroup.configToGUI();
	}
	
	private void multiConfigEvent(Event event) {
		OptecFilterWheelConfig config = proc.getConfig();
		config.multiConfigAllowed = multiConfigEnableCheckbox.getSelection();
		doUpdate();
	}
	
	private void portDeviceTextboxEvent(Event event) {
		OptecFilterWheelConfig config = proc.getConfig();
		proc.vendor_id = proc.vendor_ids.get(portDeviceTextbox.getSelectionIndex());
		proc.product_id = proc.product_ids.get(portDeviceTextbox.getSelectionIndex());
		proc.serial_number_ptr = proc.serial_numbers_ptr.get(portDeviceTextbox.getSelectionIndex());
		doUpdate();
	}
	
	private void connectButtonEvent(Event event) {
		proc.openWheel();
		doUpdate();
	}

	private void refreshButtonEvent(Event event) {
		proc.getAvailableWheels();
		OptecFilterWheelConfig config = proc.getConfig();
		portDeviceTextbox.setItems(proc.serial_numbers_str.toArray(new String[0]));
		doUpdate();
	}
	
	private void disconnectButtonEvent(Event event) {
		proc.closeWheel();
		doUpdate();
	}
	
	private void pos1ButtonEvent(Event event) {
		proc.moveWheel(1);
		doUpdate();
	}
	
	private void pos2ButtonEvent(Event event) {
		proc.moveWheel(2);
		doUpdate();
	}
	
	private void pos3ButtonEvent(Event event) {
		proc.moveWheel(3);
		doUpdate();
	}
	
	private void pos4ButtonEvent(Event event) {
		proc.moveWheel(4);
		doUpdate();
	}
	
	private void pos5ButtonEvent(Event event) {
		proc.moveWheel(5);
		doUpdate();
	}
	
	private void multiConfigTrigger(Event event) {
		proc.sendSequence();
		doUpdate();
		
	}
	
	private void multiConfigSoft(Event event) {
		OptecFilterWheelConfig config = proc.getConfig();
		config.multiConfigSoftStart = multiConfigSoft.getSelection();
		doUpdate();
	}
}
