package imageProc.control.optecFilterWheel.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.control.optecFilterWheel.OptecFilterWheelConfig;
import imageProc.control.optecFilterWheel.OptecFilterWheelControl;


public class MultiConfigPanel {
	
	private OptecFilterWheelControl proc;
	private Composite swtGroup;
			
	private Button multiConfigEnableCheckbox;
	private Combo mcFilterCombo[];
	private Spinner mcTimeSpinner[];
	private Button mcAutoConfigMaster;
	private Button mcAutoConfigRadio[];
	
	public MultiConfigPanel(Composite parent, int style,OptecFilterWheelControl proc) {
		
		this.proc = proc;
		OptecFilterWheelConfig config = proc.getConfig();
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(4, false));
		
		multiConfigEnableCheckbox = new Button(swtGroup, SWT.PUSH);
		multiConfigEnableCheckbox.setText("Init panel");
		multiConfigEnableCheckbox.setToolTipText("This buttons only purpose is to initialize this panel.");
		multiConfigEnableCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		multiConfigEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
				
		Label lMCa = new Label(swtGroup, SWT.NONE); lMCa.setText("No.");
		Label lMCb = new Label(swtGroup, SWT.NONE); lMCb.setText("Filter");
		Label lMCc = new Label(swtGroup, SWT.NONE); lMCc.setText("Exposure time [s]"); 
		lMCc.setToolTipText("Time spent in each position before moving to the next one in seconds");
		
		mcAutoConfigMaster = new Button(swtGroup, SWT.CHECK);
		mcAutoConfigMaster.setText("Autoconfig");
		mcAutoConfigMaster.setToolTipText("If set allows the filter exposure time to be autoconfigured from other modules (e.g. Pilots)");
		mcAutoConfigMaster.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		mcAutoConfigMaster.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
		
		int numMultiCfg = 5;
		
		mcFilterCombo = new Combo[numMultiCfg];
		mcTimeSpinner = new Spinner[numMultiCfg];
		mcAutoConfigRadio = new Button[numMultiCfg];
		
		for(int i=0; i < numMultiCfg; i++){
			
			Label lMCn = new Label(swtGroup, SWT.NONE); lMCn.setText(i + ":");
			
			mcFilterCombo[i] = new Combo(swtGroup, SWT.NONE);
			mcFilterCombo[i].setItems(config.filterNames.toArray(new String[0]));
			mcFilterCombo[i].setToolTipText("Which filter is used at each step");
			mcFilterCombo[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcFilterCombo[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			
			mcTimeSpinner[i] = new Spinner(swtGroup, SWT.NONE);
			mcTimeSpinner[i].setValues(0, 0, 100000, 0, 1, 10);
			mcTimeSpinner[i].setToolTipText("Time in seconds each filter is left. Keep it longer than ~2s to prevent potential problems.");
			mcTimeSpinner[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));		
			mcTimeSpinner[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			
			mcAutoConfigRadio[i] = new Button(swtGroup, SWT.RADIO);
			mcAutoConfigRadio[i].setText(Integer.toString(i));
			mcAutoConfigRadio[i].setSelection(false);
			mcAutoConfigRadio[i].setToolTipText("Should the number of images in this block be modified by the autoconfiguration");
			mcAutoConfigRadio[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
			mcAutoConfigRadio[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { multiConfigEvent(event); } });
			mcAutoConfigRadio[i].addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { autoconfClickEvent(event); } });	
		}
		
			
			
		configToGUI();
	}

	protected void autoconfClickEvent(Event event) {
		OptecFilterWheelConfig config = proc.getConfig();
		//Helper: if the user tries to click a specific autoconf but the master is off, turn it on and set them
		if(!mcAutoConfigMaster.getSelection()) {
			mcAutoConfigMaster.setSelection(true);
			((Button)event.item).setSelection(true);
			
			config.autoConfigMaster = mcAutoConfigMaster.getSelection();
			guiToConfig();
			configToGUI();
		}	
	}

	void configToGUI(){
		OptecFilterWheelConfig config = proc.getConfig();
		
		multiConfigEnableCheckbox.setSelection(config.multiConfigAllowed);
		mcAutoConfigMaster.setSelection(config.autoConfigMaster);
		
		if(config.multiConfigAutoconf == null)
			config.multiConfigAutoconf = new boolean[mcFilterCombo.length];
		
		if(config.multiConfigAllowed){
			for(int i=0; i < mcFilterCombo.length; i++){
				mcFilterCombo[i].setItems(config.filterNames.toArray(new String[0]));
				mcFilterCombo[i].select(config.multiConfigFilters[i]-1); // Filter Wheel positions start at 1 but Combo.select starts at 0
				mcTimeSpinner[i].setSelection(config.multiConfigTimes[i]);
				mcAutoConfigRadio[i].setSelection(config.multiConfigAutoconf[i]);
				config.autoConfigMaster |= config.multiConfigAutoconf[i];

				mcFilterCombo[i].setEnabled(true);
				mcTimeSpinner[i].setEnabled(true);
				mcAutoConfigRadio[i].setEnabled(true);
			
			}
			mcAutoConfigMaster.setSelection(config.autoConfigMaster);
		}else{
			for(int i=0; i < mcFilterCombo.length; i++){
				
				mcFilterCombo[i].setItems(config.filterNames.toArray(new String[0]));
				mcFilterCombo[i].clearSelection();
				mcTimeSpinner[i].setSelection(0);
				mcAutoConfigRadio[i].setSelection(false);
				
				mcFilterCombo[i].setEnabled(false);
				mcTimeSpinner[i].setEnabled(false);
				mcAutoConfigRadio[i].setEnabled(false);
				
			}
			mcAutoConfigMaster.setSelection(false);
		}
	}
	
	void guiToConfig(){
		OptecFilterWheelConfig config = proc.getConfig();
		
		if(config.multiConfigAllowed){
			
			config.multiConfigFilters = new int[mcFilterCombo.length];
			config.multiConfigTimes = new int[mcFilterCombo.length];
			config.multiConfigAutoconf = new boolean[mcFilterCombo.length];
			config.autoConfigMaster = mcAutoConfigMaster.getSelection();
			
			//boolean anyAutoconf = false;
			
			for(int i=0; i < mcFilterCombo.length; i++){
				
				config.multiConfigFilters[i] = mcFilterCombo[i].getSelectionIndex()+1; // Combo starts at 0 but Filter Wheel counts from 1
				config.multiConfigTimes[i] = mcTimeSpinner[i].getSelection();
				config.multiConfigAutoconf[i] = mcAutoConfigMaster.getSelection() && mcAutoConfigRadio[i].getSelection();
				config.autoConfigMaster |= config.multiConfigAutoconf[i];
			}
			
			if(mcAutoConfigMaster.getSelection() && !config.autoConfigMaster) {
				config.multiConfigAutoconf[0] = true;
				mcAutoConfigRadio[0].setSelection(true);
			}
			for(int i=0; i < mcFilterCombo.length; i++){
				
				mcFilterCombo[i].setEnabled(true);
				mcTimeSpinner[i].setEnabled(true);
				mcAutoConfigRadio[i].setEnabled(mcAutoConfigMaster.getSelection());
			}
			
		}else{
			
			config.multiConfigFilters = null;
			config.multiConfigTimes = null;
			config.multiConfigAutoconf = null;	
			config.autoConfigMaster = false;
			
			for(int i=0; i < mcFilterCombo.length; i++){
				
				mcFilterCombo[i].setEnabled(false);
				mcTimeSpinner[i].setEnabled(false);
				mcAutoConfigRadio[i].setEnabled(false);	
			}
		}
	}
	
	private void multiConfigEvent(Event event) {
	OptecFilterWheelConfig config = proc.getConfig();
		guiToConfig();	
		
	}
	
	public Control getSWTGroup() { return swtGroup; }
}
