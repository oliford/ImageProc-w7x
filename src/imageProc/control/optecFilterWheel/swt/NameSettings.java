package imageProc.control.optecFilterWheel.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.control.optecFilterWheel.OptecFilterWheelConfig;
import imageProc.control.optecFilterWheel.OptecFilterWheelControl;



public class NameSettings {
	
	private OptecFilterWheelControl proc;
	private Composite swtGroup;
		
	private Text WheelNameText;
	private Button updateWheelNameButton;
	
	private Text FiltersText[];
	private Button updateFiltersButton[];
	private Label FiltersLabel[];
	
	public NameSettings(Composite parent, int style,OptecFilterWheelControl proc) {
		
		this.proc = proc;
		OptecFilterWheelConfig config = proc.getConfig();
		
		swtGroup = new Composite(parent, style);
		swtGroup.setLayout(new GridLayout(4, false));
		
		Label lMCa = new Label(swtGroup, SWT.NONE); lMCa.setText("Wheel Name:");
		
		WheelNameText = new Text(swtGroup, SWT.BORDER);
		WheelNameText.setText(config.wheelname);
		WheelNameText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		updateWheelNameButton = new Button(swtGroup, SWT.PUSH);
		updateWheelNameButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		updateWheelNameButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { updateWheelNameEvent(event); } });
		updateWheelNameButton.setText("Update");
		
		// TODO: get number of filters from filter wheel driver
		int numFilters = 5;
		
		FiltersText = new Text[numFilters];
		updateFiltersButton = new Button[numFilters];
		FiltersLabel = new Label[numFilters];
		
		for(int i=0; i < numFilters; i++){
			
			FiltersLabel[i] = new Label(swtGroup, SWT.NONE); FiltersLabel[i].setText("Filter "+ String.valueOf(i+1)+":");
			
			FiltersText[i] = new Text(swtGroup, SWT.BORDER);
			FiltersText[i].setText(config.filterNames.get(i));
			FiltersText[i].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
			
			int pos = i;
			updateFiltersButton[i] = new Button(swtGroup, SWT.PUSH);
			updateFiltersButton[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
			updateFiltersButton[i].setText("Update");
			updateFiltersButton[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { updateFilterNameEvent(event, pos); } });
		}
				
		configToGUI();
	}
		
	private void updateWheelNameEvent(Event event) {
		proc.renameWheel(WheelNameText.getText());
		guiToConfig();
		configToGUI();
	}
	
	private void updateFilterNameEvent(Event event, int pos) {
		
		if (FiltersText[pos].getText()!="") {
			proc.renameFilter(pos+1, FiltersText[pos].getText());
		}
		guiToConfig();
		configToGUI();
	}

	void configToGUI(){
		OptecFilterWheelConfig config = proc.getConfig();
		WheelNameText.setText(config.wheelname);
		for (int i=0; i < FiltersText.length; i++) {
			FiltersText[i].setText(config.filterNames.get(i));
		}
	}
	
	void guiToConfig(){
		OptecFilterWheelConfig config = proc.getConfig();
		config.filterNames = proc.wheel.getFilterNames();
		config.wheelname = proc.wheel.getName();
		
	}
	
	public Control getSWTGroup() { return swtGroup; }
}
