package imageProc.control.optecFilterWheel;

/** Configuration for the controller module
 * Should be all public and mostly simple variables.
 * Will get serialised to/from JSON files and maybe into the archive one day   
 * 
 * everything should be public
 */

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import com.sun.jna.Pointer;

public class OptecFilterWheelConfig {


	/** Send command to do a thing when the software start trigger is given 
	 * e.g. by the W7XPilot */
	public boolean doThingOnSoftwareStart;
	
	/** List of vendor_ids of available wheels */
	//public ArrayList<Short> vendor_ids = new ArrayList<>();
	/** List of product_ids of available wheels */
	//public ArrayList<Short> product_ids = new ArrayList<>();
	/** List of serial_numbers of available wheels as pointers */
	//public ArrayList<Pointer> serial_numbers_ptr = new ArrayList<>();
	/** List of serial_numbers of available wheels as strings */
	//public ArrayList<String> serial_numbers_str = new ArrayList<>();
	
	/** vendor_id of chosen wheel*/
	//public short vendor_id = 0;
	/** product_id of chosen wheel*/
	//public short product_id = 0;
	/** serial number of chosen wheel*/
	//public Pointer serial_number_ptr = null;
	
	/** Bool to check if filter wheel is connected
	 * Is state, not config. moved to control */
	//public boolean isConnected = false;
	
	/** List of filter names */
	public List<String> filterNames = Arrays.asList("Pos1", "Pos2", "Pos3", "Pos4", "Pos5");
	
	/** Current filter position of the wheel. When FW is open, it goes 1-5 */
	public int pos = 0;
	/** Name of the Filter being currently used*/
	public String posName;
	
	/** Bool to check if moving filter failed*/
	public boolean moveFailed;
	
	/** Name of the wheel */
	public String wheelname = "None";
	
	// MultiConfig related variables
	/** Is multiconfig allowed? */
	public boolean multiConfigAllowed = false;
	
	/** Filters used for the multiConfig. First filter is 1 (not 0). */
	public int[] multiConfigFilters = null;
	
	/** Time each filter stays on*/
	public int[] multiConfigTimes = null;
	
	/** Is any of the filters set to use Autoconfig? */
	public boolean autoConfigMaster = false;
	
	/** Is the time this filter stays on set by Autoconfig? */
	public boolean[] multiConfigAutoconf = null;
	
	/** multiConfig sequence triggered by software start */
	public boolean multiConfigSoftStart = false;
	

	
	
	/** Serial port to connect to */
	//public String portName;
	
	/** BAUD rate to connect serial port with */
	//public int baudRate;
	
	/** Position of the thing A to set when pressing the button 'Set A' */
	//public int positionOfA;
	
	
	
	/** Commenting the code that was previously so that I can unscrew my mess */
	//public String nameOfAThing = "Do a thing";
		
	/**
	public String selectedStuff;
	**/
	/** Some floating point number */
	//public double value;
	
}
