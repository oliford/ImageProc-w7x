package imageProc.control.optecFilterWheel;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jna.Pointer;

import imageProc.control.arduinoComm.LoggedComm;
import imageProc.control.arduinoComm.swt.ArduinoCommSWTControl;
import imageProc.control.optecFilterWheel.swt.OptecFilterWheelSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.pilot.common.ShotLengthReceiver;
import imageProc.core.EventReciever.Event;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import hsfw.HSFW;
import hsfw.HSFWLibrary;
import hsfw.Wheel;
import hsfw.WheelInfo;
import otherSupport.SettingsManager;

import imageProc.control.optecFilterWheel.swt.OptecFilterWheelSWTControl;


/** Example module for controlling a thing by Serial */
public class OptecFilterWheelControl extends ImgSourceOrSinkImpl
		implements ImgSink, ConfigurableByID, EventReciever, LoggedComm.LogUser, ShotLengthReceiver {

	/** Current configuration */
	private OptecFilterWheelConfig config = new OptecFilterWheelConfig();

	
	/** Class for communicating with serial port (stolen from Arduino controller) */
	protected LoggedComm serialComm;

	/** Instance of the GUI controller connected to this */
	private OptecFilterWheelSWTControl swtController;

	/** Wheel library interface */
	private HSFW fwLib; 

	/** Wheel class for control */
	public Wheel wheel = null;
	
	/** Class running over available wheels */
	private WheelInfo inf = null;
	
	/** List of vendor_ids of available wheels */
	public ArrayList<Short> vendor_ids = new ArrayList<>();
	/** List of product_ids of available wheels */
	public ArrayList<Short> product_ids = new ArrayList<>();
	/** List of serial_numbers of available wheels as pointers */
	public ArrayList<Pointer> serial_numbers_ptr = new ArrayList<>();
	/** List of serial_numbers of available wheels as strings */
	public ArrayList<String> serial_numbers_str = new ArrayList<>();
	
	/** vendor_id of chosen wheel*/
	public short vendor_id = 0;
	/** product_id of chosen wheel*/
	public short product_id = 0;
	/** serial number of chosen wheel*/
	public Pointer serial_number_ptr = null;
	
	/** Bool to check if filter wheel is connected
	 * This should be replaced by an actual check in isConnected() to intterrogate the Wheel object
	 * and poke the library. This bool just tells us if we think it should be connected */
	private boolean isConnected = false;	

	private String lastLoadedConfig;
	
	public OptecFilterWheelControl() {
		
		
		//config.portName = SettingsManager.defaultGlobal().getProperty("imageProc.thing.portName", "/dev/ttyS0");
		//config.baudRate = Algorithms
		//		.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.thing.baudRate", "115200"));
		serialComm = new LoggedComm(this);
		
		try {
			fwLib = new HSFW();
		}catch(Throwable err) {
			fwLib = null;
			logr.log(Level.WARNING, "Unable to init HSFW library. Probably no hidapi-libusb on the library path", err);
		}
	}
	
	private long timeOfStart;

	@Override
	public void event(Event event) {
		switch (event) {
		case GlobalStart:
			getAvailableWheels();
			
			break;
			
		case AcquisitionStarted:
			
			if (config.multiConfigAllowed && config.multiConfigSoftStart) {
			
			long delayMS = 1000; // this is actually not needed here 
			
			Timer t = new Timer("moveTheWheel");
			t.schedule(new TimerTask() { @Override public void run() { 
					//wheel.move(4);
					sendSequence();
				}}, delayMS);
			}
			//swtController.configToGUI();
			//from code outside the SWT thing (the GUI), you should always call updateAllControllers() 
			// and never call anything from the SWT directly, because that runs in a special SWT thread
			// and we can't access it's options from here.
			updateAllControllers();
			
			break;
		default:
			break;
		}
	}
	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id) {
		if (id.startsWith("jsonfile:")) {
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}

	public void saveConfigJSON(String fileName) {
		Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().setPrettyPrinting()
				.create();

		String json = gson.toJson(config);

		OneLiners.textToFile(fileName, json);
	}

	@Override
	public void loadConfig(String id) {
		if (id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}

	@Override
	public String getLastLoadedConfig() {
		return lastLoadedConfig;
	}

	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);

		Gson gson = new Gson();

		config = gson.fromJson(jsonString, OptecFilterWheelConfig.class);

		updateAllControllers();
	}

//	public LoggedComm serialComm() {
//		return serialComm;
//	}
	
	/** Get OptecFilterWheel Config class*/
	public OptecFilterWheelConfig getConfig() {
		return config;
	}
	
	/** Load data for available wheels and return list with info */
	public void getAvailableWheels() {
		
		product_ids.clear();
		vendor_ids.clear();
		serial_numbers_ptr.clear();
		serial_numbers_str.clear();
		
		inf = fwLib.enumerateWheels();
		while (inf != null) {
			
			vendor_ids.add(inf.vendor_id);
			product_ids.add(inf.product_id);
			serial_numbers_ptr.add(inf.serial_number_ptr);
			serial_numbers_str.add(inf.getSerialNumber());
			inf=inf.getNext();
		}
	}
	
	/** Open the connection with the Filter Wheel */
	public void openWheel() {
		try {
			wheel = fwLib.open(vendor_id, product_id, serial_number_ptr);
			config.filterNames = wheel.getFilterNames();
			wheel.home();
			config.pos = 1;
			config.posName = config.filterNames.get(0);
			config.wheelname = wheel.getName();
			isConnected = true;
			updateAllControllers();
		
		} catch (Exception e) {
			logr.log(Level.WARNING, "Error opening wheel", e);
			if(wheel != null)
				wheel.close();
			wheel = null;
			isConnected = false;
		}
		
		
	}
	/** Close the connection with the Filter Wheel*/
	public void closeWheel( ) {	
		if(wheel != null)
			wheel.close();
		wheel = null;
		isConnected = false;
		updateAllControllers();
	}
	
	/** Move Filter Wheel to the desired position. Input goes 1-5*/
	// TODO: fix wheel driver and get status from wheel instead of sleeping
	public void moveWheel(int pos) {
		try {
			wheel.move(pos);
			config.pos = pos;
			config.posName = config.filterNames.get(pos-1);
			config.moveFailed = false;
			updateAllControllers();
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) { }
			
		} catch(Exception e) {
			logr.log(Level.WARNING, "Error moving wheel", e);
			config.moveFailed = true;
		}
	} 
	
	/** Run the multiConfig sequence*/
	public void sendSequence() {
		
		if (config.multiConfigAllowed) {	
			for (int i=0; i<config.multiConfigFilters.length; i++) {
				if (config.multiConfigFilters[i]!=0 && config.multiConfigTimes[i]!=0) {
					moveWheel(config.multiConfigFilters[i]);
					try {
						Thread.sleep(config.multiConfigTimes[i]*1000-1000); // seconds to milliseconds, and there is already a 1 s thread.sleep in moveWheel
					} catch(InterruptedException e) {}
				}
			}		
		}
	}
	
	// Renaming related functions
	/** Renames the name of the wheel*/
	public void renameWheel(String name) {
		if (name!="") {
			try {
				wheel.writeWheelName(name);
			}catch(Exception e) {
				logr.log(Level.WARNING, "Error writing wheel name", e);
			}
		}
		config.wheelname = wheel.getName();
		updateAllControllers();
	}
	/** Rename the filter selected. pos = 1,..,5*/
	public void renameFilter(int pos, String name) {
		if (!name.equals("")) {
			try {
				wheel.writeFilterName(pos, name);
			}catch(Exception e) {
				logr.log(Level.WARNING, "Error writing filter name", e);				
			}
		}
		config.filterNames = wheel.getFilterNames();
		config.posName = config.filterNames.get(config.pos-1);
		updateAllControllers();
	}
	
	@Override
	/** Called when a line of text was recieved from the serial port */
	public void receivedLine(String lineStr) {

	}

	@Override
	/**
	 * Called when anything is written to the serial log. Really only to tell the
	 * GUI to update all the boxes and things
	 */
	public void logChanged() {
		updateAllControllers(); // force update of GUI components
	}
/**
	@Override
	 * Called when serial port is connected. Add initial configuration here
	 */
	
	public void connected() {
		//sendPosition();
	}

	/* --- Some things from ImageProc structure --- */
	@Override
	public boolean isIdle() {
		return true;
	}

	@Override
	public boolean getAutoUpdate() {
		return false;
	}

	@Override
	public void setAutoUpdate(boolean enable) {
	}

	@Override
	public void calc() {
	}

	@Override
	public boolean wasLastCalcComplete() {
		return true;
	}

	@Override
	public void abortCalc() {
	}

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if (interfacingClass == Composite.class) {
			if (swtController == null) {
				swtController = new OptecFilterWheelSWTControl(this, (Composite) args[0], (Integer) args[1]);
				controllers.add(swtController);
			}
			return swtController;
		} else
			return null;
	}

	public static final int MS_TO_SEC = 1000; // millisecond to second

	@Override
	/** Called by pilot autoconfiguration system when the shot length has been set */
	public void setShotLength(long shotLengthMS) {
		
		if (config.multiConfigAllowed && config.autoConfigMaster) {
			for (int i=0; i<config.multiConfigTimes.length; i++) {
				if (config.multiConfigAutoconf[i]) {
					
					config.multiConfigTimes[i] = (int) shotLengthMS / MS_TO_SEC;
				}	
			}	
		}
		System.out.println("Optec set shot length " + shotLengthMS);
	}
	
	@Override
	public void destroy() {
		closeWheel();
		super.destroy();
	}
	
	public boolean isConnected() { 
		return isConnected; 
	}
}
