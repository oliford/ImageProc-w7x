package imageProc.database.logbook.swt;

import java.time.ZonedDateTime;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.LogDesc;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeSWTController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.database.logbook.LogbookConfig;
import imageProc.database.logbook.LogbookControl;
import imageProc.w7x.W7XUtil;

public class LogbookSWTControl implements ImagePipeController, ImagePipeSWTController {
	
	private LogbookControl proc;
	
	private Group swtGroup;
	private CTabItem thisTabItem;
	private Label statusLabel;
	
	private Text timeFromTextbox;
	private Text timeToTextbox;
	private Button timefromImagesButton;
	private Text componentIDTextbox;
	
	private Combo logCombo;
	private Button refreshEventButton;
	private Button createEventButton;
	
	private Button typeExperimentButton;
	private Button typeStandAloneButton;
	private Button typeEventButton;
		
	private Text descriptionText;
	private Text contentTextbox;
	private Text commentsTextbox;
	private Text tagsTextbox;
	
	private Button loadLogButton;
	private Button saveLogButton;
	
	private SWTSettingsControl settingsCtrl;
	
	private boolean dataChanging = false;
	
	public LogbookSWTControl(LogbookControl proc, Composite parent, int style) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Arduino Comm");
		swtGroup.setLayout(new GridLayout(5, false));
		
		Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
		statusLabel = new Label(swtGroup, SWT.NONE);
		statusLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 4, 1));
		
		Label lPD = new Label(swtGroup, SWT.NONE); lPD.setText("Time:");
		timeFromTextbox = new Text(swtGroup, SWT.NONE);
		timeFromTextbox.setText("");
		timeFromTextbox.setToolTipText("Time from which to look for or create log entries");
		timeFromTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		//stimeFromTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		timeToTextbox = new Text(swtGroup, SWT.NONE);
		timeToTextbox.setText("");
		timeToTextbox.setToolTipText("Time to which to look for or create log entries");
		timeToTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		//timeToTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		timefromImagesButton = new Button(swtGroup, SWT.PUSH);
		timefromImagesButton.setText("From images");
		timefromImagesButton.setToolTipText("Set the program ID according to the times of the source images");
		timefromImagesButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		timefromImagesButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.getTimeFromImages(); } });
		
		Label lPI = new Label(swtGroup, SWT.NONE); lPI.setText("Log IDs:");
		logCombo = new Combo(swtGroup, SWT.NONE);
		logCombo.setText("");
		logCombo.setToolTipText("Log IDs available");
		logCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		logCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
			
		refreshEventButton = new Button(swtGroup, SWT.PUSH);
		refreshEventButton.setText("Refresh");
		refreshEventButton.setToolTipText("Get the list of logs available at the current time");
		refreshEventButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		refreshEventButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.refreshLogs(); } });
		
		createEventButton = new Button(swtGroup, SWT.PUSH);
		createEventButton.setText("Create event");
		createEventButton.setToolTipText("Create an event for the given KKS component");
		createEventButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		createEventButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { createEventButtonEvent(event); } });
		
		Label lCI = new Label(swtGroup, SWT.NONE); lCI.setText("Component ID:");
		componentIDTextbox = new Text(swtGroup, SWT.NONE);
		componentIDTextbox.setText("");
		componentIDTextbox.setToolTipText("Filter logs by component ID e.g. 'QSK' - only show these.");
		componentIDTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		componentIDTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lLD = new Label(swtGroup, SWT.NONE); lLD.setText("Description:");
		descriptionText = new Text(swtGroup, SWT.NONE);
		descriptionText.setText("");
		descriptionText.setToolTipText("Program/Event description");
		descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		descriptionText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lLT = new Label(swtGroup, SWT.NONE); lLT.setText("Type:");
		
		typeExperimentButton = new Button(swtGroup, SWT.PUSH);
		typeExperimentButton.setText("Experiment log");
		typeExperimentButton.setToolTipText("Log is a W7-X experiment log");
		typeExperimentButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		typeStandAloneButton = new Button(swtGroup, SWT.PUSH);
		typeStandAloneButton.setText("Stand-alone component log");
		typeStandAloneButton.setToolTipText("Log is a W7-X experiment log");
		typeStandAloneButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		typeEventButton = new Button(swtGroup, SWT.PUSH);
		typeEventButton.setText("Component event log");
		typeEventButton.setToolTipText("Log is a W7-X experiment log");
		typeEventButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		
		
		SashForm swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		swtSashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		
		Group swtLogGroup = new Group(swtSashForm, SWT.NONE);
		swtLogGroup.setLayout(new GridLayout(5, false));
		Label lLC = new Label(swtLogGroup, SWT.NONE); lLC.setText("Content:");
		contentTextbox = new Text(swtLogGroup, SWT.MULTI | SWT.V_SCROLL);
		contentTextbox.setText("\n\n\n\n");
		contentTextbox.setToolTipText("Log");
		contentTextbox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		//logTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Group swtCommentsGroup = new Group(swtSashForm, SWT.NONE);
		swtCommentsGroup.setLayout(new GridLayout(5, false));
		Label lCT = new Label(swtCommentsGroup, SWT.NONE); lCT.setText("Comments:");
		commentsTextbox = new Text(swtCommentsGroup, SWT.MULTI | SWT.V_SCROLL);
		commentsTextbox.setText("\n\n\n\n");
		commentsTextbox.setToolTipText("Comments");
		commentsTextbox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		//logTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Group swtTagsGroup = new Group(swtSashForm, SWT.NONE);
		swtTagsGroup.setLayout(new GridLayout(5, false));
		Label lTT = new Label(swtTagsGroup, SWT.NONE); lTT.setText("Tags:");
		tagsTextbox = new Text(swtTagsGroup, SWT.MULTI  | SWT.V_SCROLL);
		tagsTextbox.setText("\n\n\n\n");
		tagsTextbox.setToolTipText("Tags");
		tagsTextbox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		//logTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lB = new Label(swtGroup, SWT.None); lB.setText("");		
		loadLogButton = new Button(swtGroup, SWT.PUSH);
		loadLogButton.setText("Load");
		loadLogButton.setToolTipText("Get the log text for the given shot/component");
		loadLogButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		loadLogButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadLogButtonEvent(event); } });
		
		saveLogButton = new Button(swtGroup, SWT.PUSH);
		saveLogButton.setText("Save");
		saveLogButton.setToolTipText("Save the log text for the given shot/component");
		saveLogButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		saveLogButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveLogButtonEvent(event); } });
		
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		generalControllerUpdate();	
	}

	protected void createEventButtonEvent(Event event) {
		proc.createEvent(logCombo.getText(), descriptionText.getText());
	}


	protected void saveLogButtonEvent(Event event) {
		proc.saveLog(logCombo.getText(), contentTextbox.getText(), tagsTextbox.getText());
	}

	protected void loadLogButtonEvent(Event event) {
		proc.loadLog(logCombo.getText());
	}

	protected void settingsChangedEvent(Event event) {
		if(dataChanging)
			return;
		
		try {
			dataChanging = true;
			
			LogbookConfig config = proc.getConfig();
			
			config.componentID = componentIDTextbox.getText();
			
			proc.setTimeFrom(ArchiveDBDesc.toNanos(ZonedDateTime.parse(timeFromTextbox.getText())));
			proc.setTimeTo(ArchiveDBDesc.toNanos(ZonedDateTime.parse(timeToTextbox.getText())));
						
			proc.setComponentLog(contentTextbox.getText());
			proc.setComponentComments(commentsTextbox.getText());
			proc.setComponentTags(tagsTextbox.getText());
		}finally {
			dataChanging = false;
		}
	}

	/* ---- Some things we need to define for ImageProc ----- */
	@Override
	public void destroy() { proc.destroy(); }
	@Override
	public void setTabItem(CTabItem sinkTab) { this.thisTabItem = sinkTab; }
	@Override
	public ImgSourceOrSinkImpl getPipe() { return proc; }
	@Override
	public Object getInterfacingObject() { return swtGroup; }

	/** Called by ImageProc core to update the GUI with the processor's current state */
	@Override
	public void generalControllerUpdate() {
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			@Override
			public void run() { doUpdate();  }
		});
		
	}
	
	private void doUpdate(){
		if(dataChanging)
			return;
		
		try {
			dataChanging = true;			
			
			String current = logCombo.getText();
			boolean found = false;
			logCombo.removeAll();
			if(proc.getLogs() != null)
				for(LogDesc log : proc.getLogs()) {
					String id = log.getId() != null ? log.getId() : log.getRefId();
					if(id.equals(current))
						found = true;
					logCombo.add(id);
				}
			logCombo.setText(current);
			
			//Color cBG = swtGroup.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
			//boolean isDarkMode = (cBG.getRed() + cBG.getGreen() + cBG.getBlue())/3 < 128;
			
			logCombo.setForeground(swtGroup.getDisplay().getSystemColor(found ? SWT.COLOR_WIDGET_FOREGROUND : SWT.COLOR_RED));
			
					
			LogbookConfig config = proc.getConfig();
			
			componentIDTextbox.setText(config.componentID != null ? config.componentID : "");
			
			timeFromTextbox.setText(ArchiveDBDesc.fromNanos(proc.getTimeFrom()).toString());
			timeToTextbox.setText(ArchiveDBDesc.fromNanos(proc.getTimeTo()).toString());
			
			contentTextbox.setText(proc.getComponentLog() != null ? proc.getComponentLog() : "");
			commentsTextbox.setText(proc.getComponentComments() != null ? proc.getComponentComments() : "");
			tagsTextbox.setText(proc.getComponentTags() != null ? proc.getComponentTags() : "");
		}finally {
			dataChanging = false;
		}
	}
	
}
