package imageProc.database.logbook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import descriptors.w7x.CommentDesc;
import descriptors.w7x.LogDesc;
import descriptors.w7x.LogbookRequestDesc;
import descriptors.w7x.TagDesc;
import descriptors.w7x.TagDesc.TagCategory;
import descriptors.w7x.LogDesc.ComponentLogType;
import descriptors.w7x.LogDesc.EventLogType;
import imageProc.control.thing.ThingConfig;
import imageProc.control.thing.swt.ThingSWTControl;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.database.logbook.swt.LogbookSWTControl;
import imageProc.w7x.W7XUtil;
import oneLiners.OneLiners;
import otherSupport.SettingsManager;
import signals.w7x.Logbook;
import w7x.logbook.LogbookFetcher;
import w7x.logbook.LogbookRequest;
import w7x.logbook.LogbookRequest.Combinators;
import w7x.logbook.writing.LogbookWriter;

public class LogbookControl extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID {

	private boolean lastCalcComplete = false;
	
	private LogbookSWTControl swtController;
	private LogbookConfig config = new LogbookConfig();
	
	private long timeFrom, timeTo;
	private ArrayList<LogDesc> logs = new ArrayList<LogDesc>();	
	
	private String componentLog;
	private String componentComments;
	private String componentTags;
	
	private String lastLoadedConfig;

	@Override
	public boolean isIdle() { return true;	}

	@Override
	public boolean getAutoUpdate() {  return false; }

	@Override
	public void setAutoUpdate(boolean enable) { }

	@Override
	public boolean wasLastCalcComplete() {
		return lastCalcComplete ;
	}
	
	public LogbookConfig getConfig() { return config; }

	@Override
	public void abortCalc() { }
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new LogbookSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}
	
	/* --- Loading and saving configuration to JSON files --- */
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
			lastLoadedConfig = id;
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, LogbookConfig.class);
		
		updateAllControllers();	
	}

	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub
		
	}

	public void loadLog(String logID) {
		
		//q=(id:{ID}) to get a log
		//q=(ref_id:{ID}) to get anything attached to a log, e.g. component logs
		// ID can be XP_yyyymmdd.ss
		// ID can be events like 'QSK_20231217-event1'
		
		//finding IDs in range
		// q=from>xxxx AND from<xxxxx
		try {
			
			componentLog = "(Not found)";
			componentComments = "(Not found)";
			componentTags = "(Not found)";
			
			LogDesc lDesc = null;			
			if(logID.startsWith("XP")) {
				lDesc = new LogDesc(ComponentLogType.EXPERIMENT);
				lDesc.setComponentId(config.componentID);
				lDesc.setRefId(logID);
			}else if(logID.contains("event")) {
				lDesc = new LogDesc(ComponentLogType.EVENT);
				lDesc.setComponentId(config.componentID);
				lDesc.setId(logID);
			}else {
				lDesc = new LogDesc(ComponentLogType.STAND_ALONE);
				lDesc.setComponentId(config.componentID);
				lDesc.setRefId(logID);
			}
			
			lDesc.read();
			
			componentLog = lDesc.getDescription();
			StringBuilder sb = new StringBuilder();
			for(TagDesc tag : lDesc.getTags()) {
				sb.append(tag.getName() + ((tag.getValue() != null) ? " = " + tag.getValue() : "") + "\n");
			}
			componentTags = sb.toString();
			
			int commentNum = 0;
			sb.setLength(0);
			while(true) {
				CommentDesc comment = new CommentDesc();
				comment.setComponentId(config.componentID);
				comment.setRefId(logID);
				
				comment.read(commentNum);
				if(comment.getContent() == null)
					break;
				
				sb.append("\n------------- Comment " + commentNum + "------------\n" + comment.getContent() + "\n");
				commentNum++;
			}
			componentComments = sb.toString();
			
		}finally {
			updateAllControllers();
		}
	}

	public void saveLog(String logID, String content, String tagsText) {
		
		
		
		LogDesc lDesc = null;			
		if(logID.startsWith("XP")) {
			lDesc = new LogDesc(ComponentLogType.EXPERIMENT);
			lDesc.setComponentId(config.componentID);
			lDesc.setRefId(logID);
		}else if(logID.contains("event")) {
			lDesc = new LogDesc(ComponentLogType.EVENT);
			lDesc.setComponentId(config.componentID);
			lDesc.setId(logID);
		}else {
			lDesc = new LogDesc(ComponentLogType.STAND_ALONE);
			lDesc.setComponentId(config.componentID);
			lDesc.setRefId(logID);
		}
		
		lDesc.read();
		
		lDesc.setDescription(content);
		String[] lines = tagsText.split("\n");
		ArrayList<TagDesc> tags = new ArrayList<>();
		for(int i=0; i < lines.length; i++) {
			int j = lines[i].indexOf('=');
			String name, value;
			if(j >= 0) {
				name = lines[i].substring(0, j);
				value = lines[i].substring(j+1);				
			}else {
				name = lines[i];
				value = null;
			}
			
			TagDesc tag = new TagDesc(name, value, null, name, TagCategory.Diagnostics);
			tags.add(tag);
		}
		
		lDesc.setTags(tags);
		
		lDesc.setEventType(EventLogType.Event);
		
		lDesc.update();
		
		
		
	}
	
	public String getComponentLog() { return componentLog; }
	public void setComponentLog(String componentLog) { this.componentLog = componentLog; }
	public String getComponentComments() { return componentComments; }
	public void setComponentComments(String componentComments) { this.componentComments = componentComments; }
	public String getComponentTags() { return componentTags; }
	public void setComponentTags(String componentTags) { this.componentTags = componentTags; }

	public void createEvent(String id, String content) {
		// TODO Auto-generated method stub
		
		long nanotime[] = W7XUtil.getBestNanotime(connectedSource);
		long tFrom = nanotime[0]; 
		long tTo = nanotime[nanotime.length-1];
		//long tFrom = 1702848800000000000L;
		//long tTo = 1702848900000000000L;
		
		
		String logbookAddress = SettingsManager.defaultGlobal().getProperty("logbook.server.w7x", "https://w7x-logbook.ipp-hgw.mpg.de");
		
		//String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJvbGlmb3JkIiwiaXNzIjoiaHR0cHM6Ly93N3gtbG9nYm9vay5pcHAtaGd3Lm1wZy5kZS8iLCJpYXQiOjE3MDMwMDg1NzYsImV4cCI6MTcxMDg3MDk3Nn0.I_1b91MkZjK-a-IlSqIXOrfFZFqS6Uwla5fUmZQPBXk";
		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJvbGlmb3JkIiwiaXNzIjoiaHR0cHM6Ly93N3gtbG9nYm9vay5pcHAtaGd3Lm1wZy5kZS8iLCJpYXQiOjE3Mzg2NjU2MjQsImV4cCI6MTc0NjM1MTYyNH0.IrqqnkD0ZYqM9bASho9PMlBnrBWJrzS0l5FiVM4UCO4";
				     
		

		String json = "{ 'name' : '"+id+"'"
				+ ", 'description' : '"+content+"'"
				+ ", 'type': 'Event'"
				+ ", 'from': "+tFrom
				+ ", 'upto': "+tTo
				+" }";
		
		LogbookFetcher lbf = new LogbookFetcher();
		
		
		LogbookWriter.postToLogbookWithToken(logbookAddress, "api/events/"+config.componentID, json, token, 0);
			
		System.out.println("Done");
	}

	public void getTimeFromImages() {
		long nanotime[] = W7XUtil.getBestNanotime(connectedSource);
		timeFrom = Long.MAX_VALUE;
		timeTo = Long.MIN_VALUE;
		for(int i=0;i < nanotime.length; i++) {
			if(nanotime[i] > 0 && nanotime[i] < Long.MAX_VALUE) {
				if(nanotime[i] < timeFrom) timeFrom = nanotime[i];
				if(nanotime[i] > timeTo) timeTo = nanotime[i];
			}
		}
		
		updateAllControllers();
	}
	
	public void refreshLogs() {
		
		/*if(config.componentID == null || config.componentID.length() <= 0) {
			//see if we can work this out from any path in the loaded metadata
			String streamPath = (String) getSeriesMetaData("w7x/path");
			config.componentID =  imageProc.w7xspec.Defaults.getTableFromDatastream(streamPath);
			
			updateAllControllers();
			
			//imageProc.w7xspec.Defaults.getSpectrometerFromDatastream(streamPath);
			throw new RuntimeException("componentID is empty and one could not be identified from available metadata (W7XLoad)");
		}*/
		
		
		
		LogbookFetcher lbf = new LogbookFetcher();
		
		LogbookRequest request = new LogbookRequest();
		request.setCombinator(Combinators.NONE);
		request.setArgs("from:[0 TO "+timeFrom+"] AND upto:["+timeTo+" TO *]"  //any overlap
						 	+ (config.componentID != null ? ("AND id:" + config.componentID + "*") : ""));
		
		LogbookRequestDesc desc = new LogbookRequestDesc();
		desc.setRequest(request);
		
		Logbook lb = (Logbook)lbf.getSig(desc);
		JsonArray ja = lb.getJsonObject().get("hits").getAsJsonObject().get("hits").getAsJsonArray();
		ArrayList<String> ids = new ArrayList<>();
		if(logs == null)
			logs = new ArrayList<LogDesc>();
		logs.clear();
		for(JsonElement je : ja) {
			JsonObject hit = je.getAsJsonObject();
			//System.out.println(hit.toString());
			
			JsonElement je2 = hit.get("_source");
			JsonObject jo2 = je2.getAsJsonObject();
			
			ComponentLogType type = null;			
			/*if(jo2.get("type") != null) {
				String typeStr = jo2.get("type").getAsString();
				if(typeStr.equals("Planned"))
					type = ComponentLogType.EXPERIMENT;
				
			}else {
				type = ComponentLogType.EXPERIMENT;
			}*/
			
			
			LogDesc log = null;
			if(jo2.get("id") != null) {
				log = new LogDesc(ComponentLogType.EVENT);
				log.setId(jo2.get("id").getAsString());
			} else if(jo2.get("ref_id") != null) {
				log = new LogDesc(ComponentLogType.EXPERIMENT);
				log.setRefId(jo2.get("ref_id").getAsString());
				
			}else 
				throw new RuntimeException("No field for ID");
			
			//LogDesc log = new LogDesc(type);
			if(jo2.get("kks") != null) {
				log.setComponentId(jo2.get("kks").getAsString());
			}else {
				log.setComponentId("XP");
			}
			
			
				
			log.read();
			System.out.println("Log type="+log.getType()+", id=" + log.getId() + ", RefID=" + log.getRefId() + ", component=" + log.getComponentId() + ", name=" + log.getName());
			logs.add(log);
				
			
		}
		
		updateAllControllers();
		
	}

	public long getTimeFrom() { return this.timeFrom; }
	public long getTimeTo() { return this.timeTo; }
	public void setTimeFrom(long timeFrom) { this.timeFrom = timeFrom; }
	public void setTimeTo(long timeTo) { this.timeTo = timeTo; }
	
	public ArrayList<LogDesc> getLogs() { return logs; }

}
