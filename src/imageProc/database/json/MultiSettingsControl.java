package imageProc.database.json;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;

import imageProc.core.ConfigurableByID;
import imageProc.core.swt.SWTSettingsControl;

public class MultiSettingsControl implements SWTSettingsControl {
	private CTabFolder swtTabFolder;
	
	private JSONFileSettingsControl jsonSettings;
	private CTabItem tabItemJson;
	
	private ParlogSettingsControl parlogSettings;
	private CTabItem tabItemParlog;
	
	@Override
	public void buildControl(Composite parent, int style, ConfigurableByID proc) {
		// TODO Auto-generated method stub
		//swtTabFolder.setText("JSON File Save/Load settings");
		
		swtTabFolder = new CTabFolder(parent, SWT.BORDER | SWT.TOP);		
		swtTabFolder.setLayout(new GridLayout(4, false));
		
		jsonSettings = new JSONFileSettingsControl();
		jsonSettings.buildControl(swtTabFolder, SWT.BORDER, proc);
		tabItemJson = new CTabItem(swtTabFolder, SWT.NONE);
		tabItemJson.setControl(jsonSettings.getComposite());
		tabItemJson.setText("JSON");  
		tabItemJson.setToolTipText("Load/save settings to/from JSON file on disk");
		
		
		parlogSettings = new ParlogSettingsControl();
		parlogSettings.buildControl(swtTabFolder, SWT.BORDER, proc);
		tabItemParlog = new CTabItem(swtTabFolder, SWT.NONE);
		tabItemParlog.setControl(parlogSettings.getComposite());
		tabItemParlog.setText("Parlog");  
		tabItemParlog.setToolTipText("Load/save settings to/from PARLOG in W7X Archive");
		

		swtTabFolder.setSelection(tabItemJson);
	}

	@Override
	public Composite getComposite() { return swtTabFolder; }

	@Override
	public void doUpdate() {
		parlogSettings.doUpdate();
		jsonSettings.doUpdate();
	}	

}
