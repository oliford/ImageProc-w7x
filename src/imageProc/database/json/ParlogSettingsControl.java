package imageProc.database.json;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import descriptors.SignalDesc;
import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBDesc.Type;
import descriptors.w7x.ArchiveDBJSONInfoDesc.ExtraJsonRequestArg;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.proc.fastSpec.FastSpecProcessor;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.w7x.W7XUtil;
import otherSupport.SettingsManager;
import w7x.archive.ArchiveDBFetcher;

/** Insert for saving/load settings to/from json from a parlog */
public class ParlogSettingsControl implements SWTSettingsControl {	
	private Group swtGroup;
	
	private Combo settingsPathCombo; 
	private List versionsList;
	private Button loadButton;
	private Button saveButton;
	
	private ConfigurableByID proc;
	
	@Override
	public void buildControl(Composite parent, int style, ConfigurableByID proc) {
		this.proc = proc;
		
		swtGroup = new Group(parent, style);
		swtGroup.setLayout(new GridLayout(4, false));
		swtGroup.setText("W7X PARLOG Save/Load settings");
		
		Label lOP = new Label(swtGroup, SWT.NONE); lOP.setText("Load:");		
		settingsPathCombo = new Combo(swtGroup, SWT.NONE);
		settingsPathCombo.setItems(new String[]{ " -- Empty --                                          " });
		settingsPathCombo.setToolTipText("Archive path to save/load module settings to.");
		settingsPathCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		settingsPathCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { refreshSettingsList(); } });
		
		loadButton = new Button(swtGroup, SWT.PUSH);
		loadButton.setText("Load");
		loadButton.setToolTipText("Load the selected JSON settings for this module.");
		loadButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		loadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadAltPulseButtonEvent(event); } });
		
		saveButton = new Button(swtGroup, SWT.PUSH);
		saveButton.setText("Save");
		saveButton.setToolTipText("Save the settings for this module to the specified path.");
		saveButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		saveButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
		
		versionsList = new List(swtGroup, SWT.V_SCROLL);
		versionsList.setItems(new String[]{ " -- None -- "});
		versionsList.setToolTipText("List of versions available for given PARLOG. Select to pick version.");
		versionsList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		versionsList.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { listSelectEvent(event); } });
		
		refreshSettingsList();
	}
	
	private void refreshSettingsList(){
		String selected = settingsPathCombo.getText();
		
		ArrayList<String> names = new ArrayList<String>();
		
		//TODO; fill names[]
		
		Collections.sort(names);
		
		settingsPathCombo.setItems(new String[0]);
		int selIdx = -1;
		int idx=0;
		for(String name : names){
			if(selected.equals(name))
				selIdx = idx;
			
			settingsPathCombo.add(name);
			idx++;
		}
		
		if(selIdx >= 0)
			settingsPathCombo.select(selIdx);
		else
			settingsPathCombo.setText(selected);
		
		updateVersionsList();
	}
	
	private void loadAltPulseButtonEvent(Event e) {
		if(settingsPathCombo.getText().length() > 0){
			proc.loadConfig("parlog:" + settingsPathCombo.getText());			
		}
		refreshSettingsList();
	}
	
	private void saveButtonEvent(Event e) {
		if(settingsPathCombo.getText().length() > 0)
			proc.saveConfig("parlog:" + settingsPathCombo.getText());
		refreshSettingsList(); //may have created a file
	}
	
	private void updateVersionsList(){
		ImgSource src = (proc instanceof ImgSource) ? (ImgSource)proc : ((ImgSink)proc).getConnectedSource();
		
		
		//Load directly from previously stored Parlog
		try{
			String streamPath = settingsPathCombo.getText();
					
			ArchiveDBDesc desc = new ArchiveDBDesc(streamPath);			
			desc.setType(Type.PARLOG);
			if(!desc.hasTimerange()){
				long nanos[] = W7XUtil.getBestNanotime(src);
				desc.setNanosRange(nanos[0], nanos[nanos.length-1]);
				desc.setVersion(0);
			}
			Logger.getLogger(this.getClass().getName()).fine("Looking for versions of stream '"+desc.getWebAPIRequestURI()+"'");
				
			int maxVersion = ArchiveDBFetcher.defaultInstance().getHighestVersionNumber(desc);
			if(maxVersion == 0){
				versionsList.setItems(new String[]{ " -- No versions for this timestamp -- " });
				return;
			}
			
			versionsList.removeAll();
			for(int version=1; version <= maxVersion; version++){
				String verDesc = null;
			
				if(proc instanceof FastSpecProcessorW7X){
					try{
						desc.setVersion(version);										
						verDesc = ArchiveDBFetcher.defaultInstance().getParLogEntry(desc, "/parms/saveResultsDescription", String.class);
						
					}catch(RuntimeException err){
						verDesc = "ERROR: " + err.getMessage();
					}
				}else{
					verDesc = "???";
				}
					
				versionsList.add("V" + version + ": " + verDesc);
					
			}
			
		}catch(RuntimeException err){
			versionsList.setItems(new String[]{ " -- Versions lookup failed: " + err.toString() + " -- " });
			err.printStackTrace();
		}
		
	}
	
	private void listSelectEvent(Event e) {
		String str = versionsList.getSelection()[0];
		if(str.startsWith(" --"))
			return;
		
		String streamPath = settingsPathCombo.getText();
		
		int ver = Integer.parseInt(str.replaceAll("V([0-9]*):.*", "$1"));
		
		ArchiveDBDesc desc = new ArchiveDBDesc(streamPath);			
		desc.setType(Type.PARLOG);
		desc.setVersion(ver);
		settingsPathCombo.setText(desc.path());
	}
	
	@Override
	public Composite getComposite() { return swtGroup;	}

	@Override
	public void doUpdate() { 
		String config = proc.getLastLoadedConfig();
		if(config == null)
			return;
		String parts[] = config.strip().split(":");
		if(parts.length >=2 && parts[0].equals("parlog")) {
			settingsPathCombo.setText(parts[1]); 
		}
	}
}
