package imageProc.database.w7xArchiveDB;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.internal.LazilyParsedNumber;

import de.mpg.ipp.codac.w7xtime.NanoTime;
import de.mpg.ipp.codac.w7xtime.TimeInterval;
import descriptors.SignalDesc;
import descriptors.gmds.GMDSSignalDesc;
import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBDesc.Type;
import descriptors.w7x.ArchiveDBIntervalsDesc;
import descriptors.w7x.ArchiveDBJSONInfoDesc;
import descriptors.w7x.ArchiveDBTreeListDesc;
import imageProc.core.BulkImageAllocation;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSource;
import mds.GMDSFetcher;
import mds.MdsPlusException;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.Signal;
import signals.gmds.GMDSSignal;
import signals.w7x.ArchiveDB;
import signals.w7x.ArchiveDB.Database;
import w7x.archive.ArchiveDBFetcher;

/** Source for loading from W7X Archive DB */
public class W7XArchiveDBSource extends W7XArchiveDBPipe implements ImgSource, ConfigurableByID {

	private W7XArchiveDBSourceConfig config = new W7XArchiveDBSourceConfig();

	BulkImageAllocation<ByteBufferImage> imageAlloc = new BulkImageAllocation<ByteBufferImage>(this);

	private List<Program> programDataList;
	private Calendar programDataListDate;
	private String programDataListStatus;

	private int[] programCountPerDayOfMonth;
	private int[] dataCountPerDayOfMonth;

	private int numberOfParlogsInRange = -1;

	public W7XArchiveDBSource() {
		this(ArchiveDBFetcher.defaultInstance(), getDefaultPath(), -1, -1);
	}

	public W7XArchiveDBSource(ArchiveDBFetcher adb, String path, long fromNanos, long toNanos) {
		super(adb);
		config.path = getDefaultPath();

		config.splitOutOfProgramPauseNS = Algorithms.mustParseInt(SettingsManager.defaultGlobal()
				.getProperty("imageProc.w7x.archiveDB.splitOutOfProgramDataMillisecs", "30000")) * 1_000_000L; // 30
																												// secs

		config.afterProgramToleranceNS = Algorithms.mustParseInt(SettingsManager.defaultGlobal()
				.getProperty("imageProc.w7x.archiveDB.afterProgramToleranceMillisecs", "30000")) * 1_000_000L; // 30
																												// secs

		config.beforeProgramToleranceNS = Algorithms.mustParseInt(SettingsManager.defaultGlobal()
				.getProperty("imageProc.w7x.archiveDB.beforeProgramToleranceMillisecs", "10000")) * 1_000_000L; // 10
																												// secs

		programDataList = null;
		programDataListStatus = " -- Not loaded -- ";
		loadTarget(path, fromNanos, toNanos);
	}

	public void load() {
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() {
			@Override
			public void run() {
				doLoad();
			}
		});
	}

	@Override
	public String getPath() {
		return pathFixup(config.path);
	}

	public void doLoad() {
		String path = config.path;
		long fromNanos = this.fromNanos;
		long toNanos = this.toNanos;
		abortCalc = false;

		addPathToHistory(path);
		setStatus("Loading...");
		try {

			seriesMetaData.clear();
			imageAlloc.invalidateAll();

			// and hang the experiment, pulse etc on the image series so stuff
			// knows
			// where to write direct data
			setSeriesMetaData("/w7x/path", path, false);
			setSeriesMetaData("/w7x/fromNanosLoaded", fromNanos, false);
			setSeriesMetaData("/w7x/toNanosLoaded", toNanos, false);

			ArchiveDBDesc desc = resolveDescriptor(path, fromNanos, toNanos);

			setStatus("Loading (intervals merge)...");
			long[][] intervals = getMergedIntervals(desc);
			setStatus("Loading...");

			// initially use the box intervals as timebase. This only works when each image
			// is in a box.
			// readSeperateImages() will reallocate it and sort it out if not
			setSeriesMetaData("/w7x/archive/nanoTime", intervals[0], true);

			desc.setNanosRangeNone();
			path = null;

			// of course we don't necessarily know that there arn't multiple
			// time points in an interval
			// is that a thing? how does it actually distinguish them??

			int nImages = intervals[0].length;
			if (maxImages > 0 && nImages > maxImages)
				nImages = maxImages;

			try {
				loadMetaData(desc, intervals[0], fromNanos, toNanos);
			} catch (RuntimeException err) {
				logr.log(Level.WARNING, "ERROR: No MetaData for '" + desc + "' because:" + err.getMessage(), err);
			}

			createTimebases();

			readSeparateImages(desc, fromNanos, toNanos, nImages, intervals);

			// there was a bug here in GMDSSource somewhere, so do metadata
			// again here, just to make sure
			// loadMetaData(database, path, intervals[0], fromNanos, toNanos);

			setSeriesMetaData("/w7x/path", desc.path(), false);
			setSeriesMetaData("/w7x/fromNanosLoaded", fromNanos, false);
			setSeriesMetaData("/w7x/toNanosLoaded", toNanos, false);
			if (getSeriesMetaData("/w7x/archive/nanoTime") == null) // was probably overwritten by readSeperateImages()
				setSeriesMetaData("/w7x/archive/nanoTime", intervals[0], true);

			createTimebases();

			addPathToHistory(desc.path());

			// setStatus("Load complete: " + nImages + " images/boxes");

		} catch (Throwable err) { // out of memery errors too
			System.err.println("Unable to read image sequence from database: " + err);
			err.printStackTrace();

			notifyImageSetChanged();
			setStatus("ERROR: " + err);
		}
	}

	/** create various useful timebases from nanotimes */
	private void createTimebases() {
		long nanotime[] = (long[]) Mat.fixWeirdContainers(getSeriesMetaData("/w7x/archive/nanoTime"), true);
		int nImages = Math.min(nanotime.length, getNumImages()); // avoid out of range exceptions by using smallest

		// create a normal 'time' from the intervals, relative to the start
		// of the scenario
		double time[] = new double[nImages];
		long scenarioT0;
		try {
			scenarioT0 = adb.getScenarioBegin(nanotime[0]);

			for (int i = 0; i < nImages; i++) {
				time[i] = (double) (nanotime[i] - scenarioT0) / 1e9;
			}
			setSeriesMetaData("/w7x/archive/timeInScenario", time, true);

		} catch (RuntimeException err) {
			err.printStackTrace();
		}

		time = new double[nImages];
		try {
			for (int i = 0; i < nImages; i++) {
				time[i] = (double) (nanotime[i] - nanotime[0]) / 1e9;
			}
			setSeriesMetaData("/w7x/archive/timeFromFrame0", time, true);

		} catch (RuntimeException err) {
			err.printStackTrace();
		}

		time = new double[nImages];
		long t1;
		try {
			t1 = adb.getT1TriggerCorrespondingToNano(nanotime[0]);
			setSeriesMetaData("/w7x/archive/t1Nano", t1, true);
			for (int i = 0; i < nImages; i++) {
				time[i] = (double) (nanotime[i] - t1) / 1e9;
			}
			setSeriesMetaData("/w7x/archive/time", time, true); // this is a universal enough definition to use
																// generally
			setSeriesMetaData("/w7x/archive/timeFromT1", time, true);
		} catch (RuntimeException err) {
			logr.log(Level.WARNING,
					"Unable to generate 'w7x/archive/time'. Couldn't find a t0 trigger for this time region: ", err);
		}
	}

	private ArchiveDBDesc resolveDescriptor(String path, long fromNanos, long toNanos) {
		ArchiveDBDesc desc = new ArchiveDBDesc(path, fromNanos, toNanos);

		// resolve aliases
		if (desc.isAlias()) {
			ArchiveDBDesc desc2 = adb.resolveAlias(desc);
			desc2.setChannelNumber(desc.getChannelNumber());
			desc2.setChannelName(desc.getChannelName());
			desc = desc2;
		}

		// if the channel name is missing, try to find it
		if (desc.getChannelName() == null) {
			ArchiveDBTreeListDesc[] subtrees = adb.getSubtreeEntries(desc);

			if (subtrees.length != 1)
				throw new RuntimeException(
						"No channel name in path, and datastream has " + subtrees.length + " tree entries.");
			desc = new ArchiveDBDesc(subtrees[0]);
			desc.setFromNanos(fromNanos);
			desc.setToNanos(toNanos);

		}

		return desc;
	}

	private long[][] getMergedIntervals(ArchiveDBDesc desc) {
		// get intervals and load them individually because signalAccess API
		// can't cope
		// and to allow sensible caching across weird from/to requests
		long intervals[][] = null;
		try {
			intervals = adb.getIntervals(desc);
		} catch (RuntimeException err) {
			logr.log(Level.WARNING, "Fetching intervals for '" + desc + "', using only local intervals (or maybe none if both fail). ", err);
		}
		if (intervals == null)
			intervals = new long[1][0];

		// get intervals from the cache too, since there may be images that aren't
		// uploaded
		// this is very slow
		long intervalsInCache[][] = null;
		try {
			intervalsInCache = adb.getIntervalsInCache(desc);			
		} catch (RuntimeException err) {
			logr.log(Level.WARNING, "Getting intervals for '" + desc + "' from cache failed, using only remote intervals (or maybe none if both fail).", err);
		}
		if (intervalsInCache == null)
			intervalsInCache = new long[1][0];
		
		ArrayList<long[]> imgList = new ArrayList<long[]>(intervals[0].length + intervalsInCache[0].length);

		for (int i = 0; i < intervals[0].length; i++)
			imgList.add(new long[] { intervals[0][i], intervals[1][i] });

		iiC: for (int i = 0; i < intervalsInCache[0].length; i++) {
			for (int j = 0; j < intervals[0].length; j++) {
				if (intervals[0][j] == intervalsInCache[0][i])
					continue iiC;
			}
			imgList.add(new long[] { intervalsInCache[0][i], intervalsInCache[1][i] });

		}

		Collections.sort(imgList, new Comparator<long[]>() {
			@Override
			public int compare(long[] o1, long[] o2) {
				return Long.compare(o1[0], o2[0]);
			}

		});

		if (imgList.size() == 0) {
			throw new RuntimeException("No intervals for given path in range, either in DB or cache");
		}

		intervals = new long[2][imgList.size()];
		for (int i = 0; i < intervals[0].length; i++) {
			intervals[0][i] = imgList.get(i)[0];
			intervals[1][i] = imgList.get(i)[1];
		}

		return intervals;
	}

	/**
	 * 
	 */
	private void readSeparateImages(ArchiveDBDesc desc, long fromNanos, long toNanos, int nBoxes, long intervals[][]) {

		ByteOrder byteOrder = ByteOrder.LITTLE_ENDIAN;

		int nFields = -1, width = -1, height = -1;

		ByteBufferImage images[] = null;

		/** Number of images read so far */
		int nextImg = 0;

		long[] nanotime = (long[]) Mat.fixWeirdContainers(getSeriesMetaData("/w7x/archive/nanoTime"), true);

		/** max number of images we might read. This is the number of boxes * number of images in first valid box found
		//the real number of images might be slightly less */
		int nImagesAllocated = -1;
		
		for (int iBox = 0; iBox < nBoxes; iBox++) {
			if (abortCalc)
				return;

			ArchiveDBDesc imgDesc = new ArchiveDBDesc(desc);
			imgDesc.setNanosRange(intervals[0][iBox], intervals[1][iBox]);
			if (imgDesc.isAlias())
				imgDesc = adb.resolveAlias(imgDesc);

			Signal imgSig = null;
			// if version wasn't specified, see if there's sometihng in the 'yet-to-write'
			// cache first
			if (imgDesc.getVersion() == 0) {
				try {
					imgDesc.setVersion(-1);
					imgSig = adb.getSigFromCache(imgDesc);
				} catch (RuntimeException err) {
					imgDesc.setVersion(0);
				}

			}

			if (imgSig == null) {
				try {
					imgSig = adb.getSig(imgDesc);
				} catch (RuntimeException err) {
					logr.log(Level.SEVERE, "Error reading box " + iBox + " from signal '" + imgDesc + "'.", err);
					setStatus("Error loading box " + iBox + " / " + nBoxes + " from " + imgDesc + ".");
					// if (images != null && images.length > i && images[i] != null)
					// images[i].invalidate();
					continue;
				}
			}

			Object timeArray = imgSig.getCoords(0);

			int rank = imgSig.getRank();
			if (rank < 2 || rank > 3)
				throw new RuntimeException("Invalid rank " + rank + " of image in signal " + imgDesc);

			Object imageData = imgSig.getData();
			Class dataType;
			Object nextLevel = imageData;

			int nF = 1, nImagesInBox = 1;
			// 3rd dimension used either for fields, or for multiples images in a box
			// (codastation streamed)
			if (rank == 3) {
				if (config.separateBoxedImages) {
					nImagesInBox = Array.getLength(nextLevel);
					nF = 1;
				} else {
					nF = Array.getLength(nextLevel);
					nImagesInBox = 1;
				}

				nextLevel = Array.get(nextLevel, 0);
			}


			int h = Array.getLength(nextLevel);
			nextLevel = Array.get(nextLevel, 0);
			int w = Array.getLength(nextLevel);
			dataType = nextLevel.getClass().getComponentType();

			if (width > 0) {
				if (nFields != nF || height != h || width != w) {
					logr.warning("Image in signal " + imgDesc + " is " + w + "x" + h + " x " + nF + " but first was "
							+ width + "x" + height + "x" + nFields);
					// images[i].invalidate();
					continue;
				}

			} else {
				// now we have width/height info, do bulk allocation
				nFields = nF;
				height = h;
				width = w;

				int bitDepth = 0;

				if (dataType == byte.class) {
					bitDepth = 8;
				}else if (dataType == short.class) {
					bitDepth = 16;
				} else if (dataType == int.class) {
					bitDepth = 32;
				} else if (dataType == float.class) {
					bitDepth = -32;
				} else if (dataType == double.class) {
					bitDepth = -64;
				} else {
					throw new RuntimeException("Unknown data type " + dataType);
				}

				nImagesAllocated = nBoxes * nImagesInBox;
				setStatus("Allocating " + nImagesAllocated + " x (" + nFields + " x " + width + " x " + height + " x " + bitDepth
						+ " bpp)");

				ByteBufferImage templateImage = new ByteBufferImage(null, -1, width, height, nFields, bitDepth, false);
				templateImage.setByteOrder(byteOrder);
				if (imageAlloc.reallocate(templateImage, nImagesAllocated)) {
					notifyImageSetChanged();
				} else {
					imageAlloc.invalidateAll();
				}

				images = imageAlloc.getImages();

			}

			if (nanotime == null)
				nanotime = new long[nImagesInBox * nBoxes];
			if (nanotime.length < nImagesInBox * nBoxes)
				nanotime = Arrays.copyOf(nanotime, nImagesInBox * nBoxes);

			for (int imgInBox = 0; imgInBox < nImagesInBox; imgInBox++, nextImg++) {
				if (timeArray != null) {
					Object val = Array.get(timeArray, imgInBox);

					if (val instanceof Double && ((Double) val) < 1e9) {
						// ugh, undo stupid conversion in DataSignals which converts to seconds as
						// double and
						// subtracts the fromNano() of the given descriptor. Since we got this entry
						// with the given
						// description, that should be the same as our one. Cached entries wouldn't be
						// retrieved from the cache if the request descriptor nanostamp were different,
						// even if the data
						// is the same
						// LOSS OF PRECISION of a few 100 nanoseconds, which is probably fine, but
						// nanostamps will now
						// not be an exact match to others in the metadata. ugggggh. whhhhy?

						nanotime[nextImg] = imgDesc.getFromNanos() + (long) (((Double) val) * 1e9);

					} else {
						nanotime[nextImg] = ((Number) val).longValue();
					}
				}

				WriteLock writeLock = images[nextImg].writeLock();
				try {
					writeLock.lockInterruptibly();
				} catch (InterruptedException e) {
					images[nextImg].invalidate();
					continue;
				}

				try {
					ByteBuffer buf = images[nextImg].getWritableBuffer(writeLock);

					if (dataType == byte.class) {
						for (int iF = 0; iF < nFields; iF++) {
							int i3 = imgInBox * nF + iF;
							Object fieldData = (rank == 3) ? Array.get(imageData, i3) : imageData;
							for (int iY = 0; iY < height; iY++) {
								buf.put(((byte[][]) fieldData)[iY]);
							}
						}
					} else if (dataType == short.class) {
						ShortBuffer iBuff = buf.asShortBuffer();
						for (int iF = 0; iF < nFields; iF++) {
							int i3 = imgInBox * nF + iF;
							Object fieldData = (rank == 3) ? Array.get(imageData, i3) : imageData;
							for (int iY = 0; iY < height; iY++) {
								iBuff.put(((short[][]) fieldData)[iY]);
							}
						}
					} else if (dataType == int.class) {
						IntBuffer iBuff = buf.asIntBuffer();
						for (int iF = 0; iF < nFields; iF++) {
							int i3 = imgInBox * nF + iF;
							Object fieldData = (rank == 3) ? Array.get(imageData, i3) : imageData;
							for (int iY = 0; iY < height; iY++) {
								iBuff.put(((int[][]) fieldData)[iY]);
							}
						}

					} else if (dataType == float.class) {
						FloatBuffer iBuff = buf.asFloatBuffer();
						for (int iF = 0; iF < nFields; iF++) {
							int i3 = imgInBox * nF + iF;
							Object fieldData = (rank == 3) ? Array.get(imageData, i3) : imageData;
							for (int iY = 0; iY < height; iY++) {
								iBuff.put(((float[][]) fieldData)[iY]);
							}
						}
					} else if (dataType == double.class) {
						DoubleBuffer iBuff = buf.asDoubleBuffer();
						for (int iF = 0; iF < nFields; iF++) {
							int i3 = imgInBox * nF + iF;
							Object fieldData = (rank == 3) ? Array.get(imageData, i3) : imageData;
							for (int iY = 0; iY < height; iY++) {
								iBuff.put(((double[][]) fieldData)[iY]);
							}
						}
					} else {
						throw new RuntimeException("Unknown data type");
					}

				} finally {
					writeLock.unlock();
				}

				images[nextImg].imageChanged(false);

				setStatus("Loaded " + nextImg + " of " + nImagesAllocated + " images\n");

				if (abortCalc) {
					setStatus("Aborted after " + nextImg + " of " + nImagesAllocated + " images");
					return;
				}
			}

			logr.info("Box " + iBox + " / " + nBoxes + " read complete, contained " + nImagesInBox
					+ " images. Now have " + nextImg + " / " + nImagesAllocated + " images.");
			setStatus("Loaded " + nextImg + " of " + nImagesAllocated + " images");

		}

		// still no size info??
		if (images == null) {
			imageAlloc.destroy();
			throw new RuntimeException("Never found any size info to allocate for " + nBoxes + " boxes.");
		}

		if (getNumImages() > nextImg) {
			//truncate image set to number actually loaded
			//(usually because last box had less images)
			imageAlloc.truncateImageSet(nextImg);
		}

		if (nanotime != null && nanotime.length > nextImg) {
			//also truncate nanotime to correct length
			nanotime = Arrays.copyOf(nanotime, nextImg);
		}
		
		//use the image by image generated nanotime, which is better than the box times done before
		setSeriesMetaData("/w7x/archive/nanoTime", nanotime, true);
		
		updateAllControllers();

	}

	private void loadMetaData(ArchiveDBDesc desc, long imageNanos[], long fromNanos, long toNanos) {

		numberOfParlogsInRange = -1; //we dont yet know
		
		
		/*
		 * old format (< 10/6/18): _PARLOG per image, each containing chanDescs and 1
		 * entry of time series data _MetaData_PATLOG for whole program, containing only
		 * non-series metadata new format (> 10/6/18): single _PARLOG for program
		 * containing chanDescs and all metadata entry called 'timeSeriesEntries'
		 * String[] paths of entries that are time series
		 */

		// full sequence meta data, and for non-ImageProc data
		ArchiveDBDesc parlogDesc = new ArchiveDBDesc(desc);
		parlogDesc.setType(Type.PARLOG);
		parlogDesc.setChannelNumber(-1);
		parlogDesc.setChannelName(null);
		loadMetaDataFromParlog(parlogDesc, imageNanos, false);

		// Also the general parlog when GERI/Codastaton streams, as opposed to the one
		// attached
		// to the stream which only contains the chanDescs
		if (parlogDesc.getSignalGroup().contains("ControlStation")) {
			ArchiveDBDesc parlog2Desc = new ArchiveDBDesc(parlogDesc);
			String parts[] = parlogDesc.getSignalName().split("-");
			try {
				int streamNum = Integer.parseInt(parts[parts.length - 1]);
				parts[parts.length - 1] = Integer.toString(streamNum - 1);
				parlog2Desc.setSignalName(String.join("-", parts));

				loadMetaDataFromParlog(parlog2Desc, imageNanos, false);
			} catch (NumberFormatException err) {
			}

		}

		// old time series data from ImageProc
		parlogDesc.setSignalName(parlogDesc.getSignalName() + "_MetaData");
		loadMetaDataFromParlog(parlogDesc, imageNanos, true);

		Object o2 = getSeriesMetaData("timeSeriesEntries");
		if (o2 instanceof String[]) {
			String[] timeSeriesEntries = (String[]) o2;
			for (String id : timeSeriesEntries) {
				Object o = getSeriesMetaData(id);
				if (o != null) {
					setSeriesMetaData(id, o, true);
				}
			}

		} else if (o2 instanceof Object[]) { // goddam it java
			Object[] timeSeriesEntries = (Object[]) o2;
			for (Object id : timeSeriesEntries) {
				if (id instanceof String) {
					Object o = getSeriesMetaData((String) id);
					if (o != null) {
						setSeriesMetaData((String) id, o, true);
					}
				}
			}
		} else {
			logr.warning("timeSeriesEntries is not a String[] or Object[], importing all metadata as non-time series");
		}

	}

	private void loadMetaDataFromParlog(ArchiveDBDesc desc, long imageNanos[], boolean notSeriesData) {

		ArchiveDBDesc parlogDesc = new ArchiveDBDesc(desc);
		parlogDesc.setNanosRange(fromNanos, toNanos + config.afterProgramToleranceNS);
		logr.fine("Loading MetaData from '" + parlogDesc + "'  ");
		setStatus("Loading MetaData from '" + parlogDesc + "'  ");

		JsonObject jsonParLog = null;
		if (parlogDesc.getVersion() == 0) {
			// if the version isnt explicitly specified
			// first try to see if there's something in the write-through cache

			// don't allow the 'invalid V-1' response to be written to cache
			ArchiveDBFetcher adbLocal = new ArchiveDBFetcher();
			adbLocal.setCacheSettings(false, true, true, null);
			adbLocal.setWriteInhibit(true);

			parlogDesc.setVersion(-1);
			jsonParLog = adbLocal.getParlogJson(parlogDesc);
			JsonElement l = jsonParLog.get("label");
			JsonElement r = jsonParLog.get("result");
			if ((l != null && l.getAsString() != null && l.getAsString().equals("EmptySignal"))
					|| (r != null && r.getAsString() != null && r.getAsString().equals("error"))) {
				jsonParLog = null;
				parlogDesc.setVersion(0);
			}
		}

		if (jsonParLog == null)
			jsonParLog = adb.getParlogJson(parlogDesc);
		// that has values[] has dimensions[] for multiple parlogs over the time
		// period

		JsonElement l = jsonParLog.get("label");
		if (l != null && l.getAsString() != null && l.getAsString().equals("EmptySignal")) {
			// try with immutable caching turned off, in case we cached the EmptySignal
			// response
			// this is actually a failure mode of the cache, which I can't think of a way to
			// solve right now
			ArchiveDBFetcher adb2 = new ArchiveDBFetcher();
			adb2.setCacheSettings(true, true, false, new Date(), true);
			jsonParLog = adb2.getParlogJson(parlogDesc);

			l = jsonParLog.get("label");
			if (l != null && l.getAsString() != null && l.getAsString().equals("EmptySignal")) {
				// try image nanos instead. Sometimes this picks something up from cache
				// since that was actually stored with the time box of the images, not whatever
				// fromNanos and toNanos was, and the cache doesn't do box logic like the actual
				// archive.

				// of course, this wont work if the actual acquisition ran over the end of the
				// program
				parlogDesc.setNanosRange(imageNanos[0], imageNanos[imageNanos.length - 1] + 100_000_000L);
				jsonParLog = adb.getParlogJson(parlogDesc);
			}

		}

		JsonElement vals = jsonParLog.get("values");
		if (vals == null)
			return;
		JsonArray valuesArr = vals.getAsJsonArray();
		JsonArray dimensionsArr = jsonParLog.get("dimensions").getAsJsonArray();
		if (valuesArr.size() < 1) {
			if (!parlogDesc.getSignalName().endsWith("_MetaData")) // dont show this
				// warning when failing to find old style metdata, it's
				// probably just the new type
				logr.warning("Parameter log has no entries for given time range. (" + parlogDesc
								+ "). There might be no metadata!");
			
				if(numberOfParlogsInRange < 0) //only if not already set of some other way of loading parlogs
					numberOfParlogsInRange = 0;
			
			return;
		} else if (valuesArr.size() == 1 || notSeriesData) {
			JsonObject jsonParlog0 = valuesArr.get(0).getAsJsonObject();

			// exactly one parlog entry for whole range --> load each as single
			// object
			numberOfParlogsInRange = 1;
			
			for (Entry<String, JsonElement> entry : jsonParlog0.entrySet()) {

				if (abortCalc)
					return;

				String key = entry.getKey();
				try {
					Object o = figureOutJsonMetadata(key, entry.getValue(), -1, imageNanos.length, false);
					if (o != null) {
						seriesMetaData.put(key, o, false);
						System.out.println("+METADATA(" + key + "):" + o.toString());
					} else {
						figureOutJsonMetadata(key, entry.getValue(), -1, imageNanos.length, true);
					}
				} catch (RuntimeException err) {
					logr.log(Level.SEVERE, "Unable to (completely) load metadata entry '" + key + "'.", err);;
					
				}
			}

		} else {// { //assume its one thing per image
			boolean singleParlogOnly = false;
			if (valuesArr.size() != imageNanos.length) {
				numberOfParlogsInRange  = valuesArr.size();
				logr.warning("ParameterLog " + parlogDesc + " has weird size (" + valuesArr.size()
						+ "). It's not 1 and doesn't match number of images " + imageNanos.length 
						+ ", just taking the last major entry ");
				singleParlogOnly = true;
			}

			for (int iJ = 0; iJ < valuesArr.size(); iJ++) { // for each index of json stupidity
				if (abortCalc)
					return;
				
				if(singleParlogOnly) {
					if(config.selectParlog >= 0) {
						if(iJ != config.selectParlog)
							continue;
					}else {
						//they want the last parlog						
						if(iJ < (valuesArr.size() - 1))
							continue;
					}
					
				}

				JsonObject parlogSection = valuesArr.get(iJ).getAsJsonObject();
				long sectionT = dimensionsArr.get(iJ * 2).getAsLong();

				int iF = Mat.getNearestLowerIndex(imageNanos, sectionT);

				for (Entry<String, JsonElement> entry : parlogSection.entrySet()) {
					String key = entry.getKey();
					
					if(singleParlogOnly)
						iF = -1;
					
					Object o = figureOutJsonMetadata(key, entry.getValue(), iF, imageNanos.length, false);
					if (o != null) {
						// seriesMetaData.put(key, o, false);
						addToMetaMap(key, o, iF, imageNanos.length);
					} else {
						figureOutJsonMetadata(key, entry.getValue(), iF, imageNanos.length, true);
					}
				}

			}
		}
	}

	/**
	 * Try to convert this JSON thing into something sensible and Java'ey for
	 * our Metadata. Groan... Java type mayhem again :/
	 * 
	 * @param imageIndex
	 *            If -1, put the entries in as metadata. Otherwise put them in
	 *            as this element in an array
	 */
	private Object figureOutJsonMetadata(String key, JsonElement thing, int imageIndex, int nImages,
			boolean addSubEntries) {
		
		if (thing.isJsonPrimitive()) {
			JsonPrimitive prim = thing.getAsJsonPrimitive();
			Object o;
			if (prim.isNumber()) {
				Number n = prim.getAsNumber();
				if (n instanceof LazilyParsedNumber) { // seriously??, what the
														// hell am I supposed to
														// do with that??
					if (!n.toString().contains(".")) // If it ain't got a dot
														// then it's probably
														// Long
						return n.longValue(); // memory is cheap, right?
					else
						return n.doubleValue();
				}
				return n;

			} else if (prim.isBoolean()) {
				return prim.getAsBoolean();
			} else { // who knows
				String str = prim.getAsString();

				// NaN is not a number... well yes, but... but... oh I hate you
				// JSON
				if (str.equals("NaN"))
					return Double.NaN;
				if (str.equals("-Infinity"))
					return Double.NEGATIVE_INFINITY;
				if (str.equals("Infinity"))
					return Double.POSITIVE_INFINITY;

				return prim.getAsString();
			}
		}

		if (thing.isJsonArray()) { // don't know if this ever gets hit
			JsonArray jsonArr = thing.getAsJsonArray();
			Object objArr[] = new Object[jsonArr.size()];
			for (int i = 0; i < objArr.length; i++) {
				objArr[i] = figureOutJsonMetadata("UnknownMetaDataArray", jsonArr.get(i), imageIndex, nImages, false);
			}

			Object o = Mat.fixWeirdContainers(objArr);
			if (addSubEntries)
				addToMetaMap(key, objArr, imageIndex, nImages);
			return null; // done
		}

		if (thing.isJsonObject()) {
			JsonObject thingObj = thing.getAsJsonObject();
			// common here for some reason is to have arrays as maps stored with
			// "[i]" as the key, arrrrg!

			Set<Entry<String, JsonElement>> entries = thingObj.entrySet();

			if (thingObj.has("[0]")) {
				Object o = attemptCleanTypeArray(key, entries, imageIndex, nImages);

				if (o != null) { // did that work??
					if (addSubEntries)
						addToMetaMap(key, o, imageIndex, nImages);
					else
						return o; // ok, done
				}
			}

			// ok... maybe it's a structurey thing, so we tree it. This will
			// also apply to arrays with non-primitives in them
			if (entries.size() > 100) {
				// no thanks, too much crap in the tree
				logr.warning("Parameter log entry too big to splurge into metadata. (" + key + ")");
				return thing.toString();
			}

			//try our best to get everything, even if some fail
			RuntimeException oneFailed = null;
			for (Entry<String, JsonElement> entry : entries) {
				String subKey = key + "/" + entry.getKey();
				try {
					Object o = figureOutJsonMetadata(subKey, entry.getValue(), imageIndex, nImages, false); // put
																											// it
																											// in
																											// further
																											// down
																											// the
																											// tree
					if (o != null) {
						if (addSubEntries)
							addToMetaMap(subKey, o, imageIndex, nImages);
					} else {
						// fine, do it again adding all the sub entries
						figureOutJsonMetadata(subKey, entry.getValue(), imageIndex, nImages, true);
					}
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Failed to process entry '"+subKey+"' in metdata, trying the rest of the tree and throwing last error", err);
					oneFailed = err;
				}
			}
			if(oneFailed != null)
				throw oneFailed;
			return null;
		}

		return thing.toString(); // no idea, give up and get stringy
	}

	private void addToMetaMap(String key, Object o, int iF, int nImages) {
		if (key.contains("SpecCal") && key.contains("["))
			System.out.println("Fuck you");

		try {
			if (iF >= 0) {
				Object arr = seriesMetaData.get(key);
				if (arr == null)
					arr = Array.newInstance(o.getClass(), nImages);
				Array.set(arr, iF, o);
				seriesMetaData.put(key, arr, true);
				System.out.println("+METADATA(" + key + "," + iF + "):" + o.toString());
			} else {
				seriesMetaData.put(key, o, false);
				System.out.println("+METADATA(" + key + "):" + o.toString());
			}
		} catch (RuntimeException err) {
			System.err.println("ERROR: Metadata entry '" + key + "', entry " + iF + "/" + nImages + ", existing="
					+ ((seriesMetaData.get(key) == null) ? "(null)" : seriesMetaData.get(key).getClass()) + ", insert="
					+ ((o == null) ? "(null)" : o.getClass()) + ", err= " + err);
		}
	}

	private Object attemptCleanTypeArray(String key, Set<Entry<String, JsonElement>> entries, int frameIndex,
			int nImages) {
		Object objArr[] = new Object[entries.size()];
		for (Entry<String, JsonElement> entry : entries) {
			String idxKey = entry.getKey();
			if (!idxKey.matches("\\[[0-9]*\\]")) {
				logr.warning("Weird JSON array type thing in parameter log found, ignoring entry. (" + key + ")");
				return null;
			}

			int idx = Algorithms.mustParseInt(idxKey.substring(1, idxKey.length() - 1));
			objArr[idx] = figureOutJsonMetadata(key, entry.getValue(), frameIndex, nImages, false);
		}

		try {
			return Mat.fixWeirdContainers(objArr);
		} catch (RuntimeException err) {
			// weird stuff happened, give up
			return null;
		}
	}

	public void setTarget(String path, long fromNanos, long toNanos) {
		config.path = path;
		this.fromNanos = fromNanos;
		this.toNanos = toNanos;
		this.abortCalc = true; // cancel any existing run
	}

	public void loadTarget(String path, long fromNanos, long toNanos) {
		setTarget(path, fromNanos, toNanos);
		load();
	}

	@Override
	public int getNumImages() {
		ByteBufferImage images[] = imageAlloc.getImages();
		return (images == null) ? 0 : images.length;
	}

	@Override
	public Img getImage(int imgIdx) {
		ByteBufferImage images[] = imageAlloc.getImages();
		return (images != null && imgIdx >= 0 && imgIdx < images.length) ? images[imgIdx] : null;
	}

	@Override
	public Img[] getImageSet() {
		return imageAlloc.getImages();
	}

	@Override
	public W7XArchiveDBSource clone() {
		return new W7XArchiveDBSource(adb, config.path, fromNanos, toNanos);
	}

	public boolean isLoading() {
		return !isIdle();
	}

	@Override
	public void destroy() {
		super.destroy();
		imageAlloc.destroy();
	}

	@Override
	public String toShortString() {
		return "W7X Load";
	}

	@Override
	public void saveNotes(String path, long fromNanos, long toNanos, String notes) {
		clearNotesCache(); // because of issues
		super.saveNotes(path, fromNanos, toNanos, notes);
	}

	private String genProgListSyncObj = new String("genProgListSyncObj");

	public List<Program> getPrograms(int year, int month, int day, boolean forceRefresh) {

		if (forceRefresh) {
			adb.setCacheSettings(adb.isCachingToMemory(), adb.isCachingToDisk(), adb.isCachingMutables(), new Date(),
					true);
		}

		JsonArray jsonArr = adb.getAllProgramsForDay(year, month, day);

		if (forceRefresh) {
			adb.setCacheSettings(adb.isCachingToMemory(), adb.isCachingToDisk(), adb.isCachingMutables(), null, false);
		}

		ArrayList<Program> programs = new ArrayList<Program>();

		for (int i = 0; i < jsonArr.size(); i++) {
			JsonObject jsonObj = jsonArr.get(i).getAsJsonObject();

			Program prog = new Program();
			prog.id = jsonObj.get("id").getAsString();
			prog.name = jsonObj.get("name").getAsString();
			prog.isReal = true;
			prog.progFrom = jsonObj.get("from").getAsLong();
			prog.progUpto = jsonObj.get("upto").getAsLong();
			prog.dataFrom = -1;
			prog.dataUpto = -1;
			prog.nImageInCache = 0;
			prog.nImagesInArchive = 0;
			prog.images = new ArrayList<ImageAtTimestamp>();
			programs.add(prog);
		}

		return programs;
	}

	public long[] getProgramRange(int year, int month, int day, int programIndex) {
		JsonArray jsonArr = adb.getAllProgramsForDay(year, month, day);
		JsonObject jsonObj = jsonArr.get(programIndex).getAsJsonObject();
		return new long[] { jsonObj.get("from").getAsLong(), jsonObj.get("upto").getAsLong() };
	}

	/*
	 * public long[] getProgramRange(int year, int month, int day, String
	 * programIDAndName) { List<String> programs = getProgramNames(year, month,
	 * day); int i=0; for(String program : programs) {
	 * if(programIDAndName.startsWith(program)) { //programName can have extra stuff
	 * return getProgramRange(year, month, day, i); } i++; } return null; }
	 */

	public void generateProgramList(String path, int year, int month, int day, boolean forceRefresh) {
		ImageProcUtil.ensureFinalUpdate(genProgListSyncObj, new Runnable() {
			@Override
			public void run() {
				programDataList = null;
				programDataListDate = null;
				programDataListStatus = "Loading program list...";
				updateAllControllers();

				try {
					programDataList = getPrograms(year, month, day, forceRefresh);
					programDataListStatus = "OK";
					programDataListDate = new GregorianCalendar(year, month - 1, day);

				} catch (RuntimeException err) {
					programDataList = null;
					programDataListStatus = "ERROR: " + err.getMessage();
					programDataListDate = null;
					err.printStackTrace();
				}

				updateAllControllers();
			}
		});
	}

	public void generateProgramDataList(String path, int year, int month, int day) {
		ImageProcUtil.ensureFinalUpdate(genProgListSyncObj, new Runnable() {
			@Override
			public void run() {
				try {
					doGenerateProgramDataList(path, year, month, day);
				} catch (RuntimeException err) {
					programDataList = null;
					programDataListDate = null;
					programDataListStatus = "ERROR: " + err.getMessage();
					err.printStackTrace();
				}

				updateAllControllers();
			}
		});
	}

	private String genProgCountsSyncObj = new String("genProgCountsSyncObj");
	private String genDataCountsSyncObj = new String("genDataCountsSyncObj");

	private String lastLoadedConfig;

	public void generateProgramDataCounts(String path, int year, int month) {

		ImageProcUtil.ensureFinalUpdate(genProgCountsSyncObj, new Runnable() {
			@Override
			public void run() {
				try {
					doGenerateProgCountsPerDay(path, year, month);
				} catch (RuntimeException err) {
					programCountPerDayOfMonth = new int[0];
					err.printStackTrace();
				}

				updateAllControllers();
			}
		});

		ImageProcUtil.ensureFinalUpdate(genDataCountsSyncObj, new Runnable() {
			@Override
			public void run() {
				try {
					doGenerateDataCountsPerDay(path, year, month);
				} catch (RuntimeException err) {
					dataCountPerDayOfMonth = new int[0];
					err.printStackTrace();
				}

				updateAllControllers();
			}
		});
	}

	/*
	 * public long[] getNanoRangeOfEntry(int year, int month, int day, String
	 * programName) {
	 * 
	 * if (programName.startsWith("OOP_")) { Pattern pattern =
	 * Pattern.compile("Out of program \\(([0-9]*) - ([0-9]*)\\).*"); Matcher
	 * matcher = pattern.matcher(programName); if (matcher.find()) { return new
	 * long[] { Algorithms.mustParseLong(matcher.group(1)),
	 * Algorithms.mustParseLong(matcher.group(2)) }; } else { return new long[] { 0,
	 * 0 }; } } else { return getProgramRange(year, month, day, programName); } }
	 */

	private static class ImageAtTimestamp {
		public long time;
		public boolean inCache;
		public boolean inDB;
	}

	public static class Program {
		public String id;
		public boolean isReal;
		public String name;

		// the real program range
		public long progFrom;
		public long progUpto;

		// range of the data
		public long dataFrom;
		public long dataUpto;

		public int nImageInCache;
		public int nImagesInArchive;

		public ArrayList<ImageAtTimestamp> images;

		public long from() {
			if (progFrom <= 0)
				return dataFrom;
			else if (dataFrom <= 0)
				return progFrom;
			else
				return Math.min(dataFrom, progFrom);
		}

		public long upto() {
			if (progUpto <= 0)
				return dataUpto;
			else if (dataUpto <= 0)
				return progUpto;
			else
				return Math.max(dataUpto, progUpto);
		}
	}

	private List<ImageAtTimestamp> getImagesInRange(String path, long startOfDay, long endOfDay, boolean cache) {
		// Get intervals in cache
		// loading from the cache is slow as it has to parse all the files in
		// the cache dir
		// so do this only once
		long intervalsInCache[][] = getIntervalsInRange(path, startOfDay, endOfDay, cache);
		if (intervalsInCache == null)
			return new ArrayList<ImageAtTimestamp>(0);

		ArrayList<ImageAtTimestamp> images = new ArrayList<ImageAtTimestamp>(intervalsInCache.length);
		for (Long l : intervalsInCache[0]) {
			ImageAtTimestamp image = new ImageAtTimestamp();
			image.time = l;
			image.inCache = cache;
			image.inDB = !cache;
			images.add(image);
		}
		return images;
	}

	/** clobbers inCache */
	private List<ImageAtTimestamp> mergeAndSortImageLists(List<ImageAtTimestamp> inDB, List<ImageAtTimestamp> inCache) {

		int i = 0, n = inDB.size();
		inDB: for (ImageAtTimestamp imgInDB : inDB) {
			for (ImageAtTimestamp imgInCache : inCache) {
				if (imgInDB.time == imgInCache.time) {
					imgInCache.inDB = true;
					continue inDB;
				}
			}
			inCache.add(imgInDB);

			programDataListStatus = "Merging data (" + (int) (100.0 * i / n) + " %)...";
			logr.fine(programDataListStatus);
			updateAllControllers();
			i++;

			if (abortCalc)
				throw new RuntimeException("Aborting in cache-db merge.");
		}

		return inCache;
	}

	public void doGenerateProgramDataList(String path, int year, int month, int day) {
		abortCalc = false;

		long startOfDay = ArchiveDBDesc.toNanos(year, month, day, 0, 0, 0, 0);
		long endOfDay = ArchiveDBDesc.toNanos(year, month, day + 1, 0, 0, 0, 0);

		programDataList = null;
		programDataListDate = null;

		programDataListStatus = "Scanning cached images...";
		logr.fine(programDataListStatus);
		updateAllControllers();

		List<ImageAtTimestamp> inCache;
		try {
			inCache = getImagesInRange(path, startOfDay, endOfDay, true);
		}catch(RuntimeException err) {
			logr.log(Level.WARNING, "Unable to list cached images", err);
			inCache = new ArrayList<ImageAtTimestamp>();
		}

		programDataListStatus = "Scanning ArchiveDB images...";
		logr.fine(programDataListStatus);
		updateAllControllers();

		List<ImageAtTimestamp> inDB;
		try {
			inDB = getImagesInRange(path, startOfDay, endOfDay, false);
		}catch(RuntimeException err) {
			logr.log(Level.WARNING, "Unable to list archiveDB images", err);
			inDB = new ArrayList<ImageAtTimestamp>();			
		}

		programDataListStatus = "Merging data...";
		logr.fine(programDataListStatus);
		updateAllControllers();

		List<ImageAtTimestamp> images = mergeAndSortImageLists(inDB, inCache);

		programDataListStatus = "Loading programs...";
		logr.fine(programDataListStatus);
		updateAllControllers();

		programDataListStatus = "Building list...";
		logr.fine(programDataListStatus);
		updateAllControllers();

		// get programs
		List<Program> programs;
		try {
			programs = getPrograms(year, month, day, false);
		} catch (RuntimeException err) {
			programs = new ArrayList<Program>();
			Program p = new Program();
			p.id = "ERR";
			p.name = "ERROR: reading programs: " + err.getMessage();
			p.images = new ArrayList<ImageAtTimestamp>();
			p.isReal = false;
			programs.add(p);
		}

		// move images within programs to those programs
		Iterator<ImageAtTimestamp> it = images.iterator();
		while (it.hasNext()) {
			ImageAtTimestamp img = it.next();

			for (Program prog : programs) {
				if (img.time >= (prog.progFrom - config.beforeProgramToleranceNS)
						&& img.time < (prog.progUpto + config.afterProgramToleranceNS)) {
					prog.images.add(img);
					try {
						it.remove();
					} catch (IllegalStateException err) {
						// wtf??? I'm just going to ignore this
						err.printStackTrace();
					}

					if (prog.dataFrom < 0 || img.time < prog.dataFrom)
						prog.dataFrom = img.time;

					if (prog.dataUpto < 0 || img.time > prog.dataUpto)
						prog.dataUpto = img.time;

				}
			}
		}

		// we're now left with all data outside programs.
		if (images.size() > 0) {

			// First sort it
			Collections.sort(images, new Comparator<ImageAtTimestamp>() {
				@Override
				public int compare(ImageAtTimestamp o1, ImageAtTimestamp o2) {
					return Long.compare(o1.time, o2.time);
				}
			});

			// now run through making up new fake programs where there are gaps
			// bigger than specified (we assume/hope this inherently breaks over
			// the programs)
			ArrayList<ImageAtTimestamp> inNext = new ArrayList<ImageAtTimestamp>();
			long currentFrom = images.get(0).time;
			long currentUpto = images.get(0).time;

			int iOOP = 0;
			for (ImageAtTimestamp img : images) {
				if (img.time > (currentUpto + config.splitOutOfProgramPauseNS)) {
					Program prog = new Program();
					prog.id = "OOP_" + iOOP;
					prog.isReal = false;
					prog.dataFrom = currentFrom;
					prog.dataUpto = currentUpto;
					prog.name = "Out of program (" + prog.dataFrom + " - " + prog.dataUpto + ")";
					prog.images = inNext;
					inNext = new ArrayList<ImageAtTimestamp>();
					programs.add(prog);

					iOOP++;
					currentFrom = img.time;
				}

				inNext.add(img);
				currentUpto = img.time;
			}

			// and the last images left over go in the final program
			Program prog = new Program();
			prog.id = "OOP_" + iOOP;
			prog.isReal = false;
			prog.progFrom = -1;
			prog.progUpto = -1;
			prog.dataFrom = currentFrom;
			prog.dataUpto = currentUpto;
			prog.name = "Out of program (" + prog.dataFrom + " - " + prog.dataUpto + ")";
			prog.images = inNext;
			programs.add(prog);
		}

		// now generate the text list
		// First sort it
		Collections.sort(programs, new Comparator<Program>() {
			@Override
			public int compare(Program o1, Program o2) {
				// real programs always have a progFrom, non-real always have a dataFrom
				// otherwise is unknown if they do or don't
				return Long.compare(o1.from(), o2.from());
			}
		});

		for (Program prog : programs) {
			// count the number of images in cache and in db
			prog.nImageInCache = 0;
			prog.nImagesInArchive = 0;
			for (ImageAtTimestamp img : prog.images) {
				if (img.inCache)
					prog.nImageInCache++;
				if (img.inDB)
					prog.nImagesInArchive++;
			}
		}

		if (programs.size() > 0) {
			programDataList = programs;
			programDataListDate = new GregorianCalendar(year, month - 1, day);
		} else {
			programDataList = null;
			programDataListStatus = " -- No programs and no data -- ";
			programDataListDate = null;
		}
	}

	/**
	 * generates counts of number of images and programs available on each day
	 */
	private void doGenerateProgCountsPerDay(String path, int year, int month) {
		programCountPerDayOfMonth = new int[0];

		Calendar c = Calendar.getInstance();
		c.set(year, month - 1, 1);
		int nDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);

		int[] count = new int[nDays + 2];
		count[0] = year;
		count[1] = month;

		for (int i = 0; i < nDays; i++) {
			int progs[] = adb.getProgramNumbersForDay(year, month, i + 1);
			count[2 + i] = progs.length;
		}

		programCountPerDayOfMonth = count;
	}

	/**
	 * generates counts of number of images and programs available on each day
	 */
	private void doGenerateDataCountsPerDay(String path, int year, int month) {
		dataCountPerDayOfMonth = new int[0];

		Calendar c = Calendar.getInstance();
		c.set(year, month - 1, 1);
		int nDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);

		long startOfMonth = ArchiveDBDesc.toNanos(year, month, 1, 0, 0, 0, 0);
		long endOfMonth = ArchiveDBDesc.toNanos(year, month + 1, 1, 0, 0, 0, 0);
		
		
		
		// System.out.println("Getting data in cache for year="+year+",
		// month="+month+"...");
		// List<ImageAtTimestamp> inCache = getImagesInRange(path, startOfMonth,
		// endOfMonth, true);

		System.out.println("Getting data in archive for year=" + year + ", month=" + month + "...");
		List<ImageAtTimestamp> inDB = getImagesInRange(path, startOfMonth, endOfMonth, false);

		// System.out.println("Merging lists for year="+year+",
		// month="+month+"...");
		// List<ImageAtTimestamp> images = mergeAndSortImageLists(inDB,
		// inCache);

		System.out.println("Parsing list for year=" + year + ", month=" + month + "...");
		int[] count = new int[nDays + 2];
		count[0] = year;
		count[1] = month;

		for (int i = 0; i < nDays; i++) {
			long startOfDay = ArchiveDBDesc.toNanos(year, month, i + 1, 00, 0, 0, 0);
			long endOfDay = ArchiveDBDesc.toNanos(year, month, i + 1, 23, 59, 59, 999);

			for (ImageAtTimestamp img : inDB) {
				if (img.time >= startOfDay && img.time < endOfDay)
					count[2 + i]++;
			}
		}
		dataCountPerDayOfMonth = count;
	}

	/**
	 * Returns the list generated by generateProgramList or generateProgramDataList
	 */
	public List<Program> getProgramDataList() {
		return programDataList;
	}

	/** Get the date of the last generated program list */
	public Calendar getProgramListDate() {
		return programDataListDate;
	}

	/** Get the status of the last generated program list */
	public String getProgramListStatus() {
		return programDataListStatus;
	}

	/**
	 * Returns the count of programs and images on each day int[dayOfMonth(2-based)]
	 * elem0=year, elem1=month
	 */
	public int[] getProgramCountPerDayOfMonth() {
		return programCountPerDayOfMonth;
	}

	/**
	 * Returns the count of programs and images on each day int[dayOfMonth(2-based)]
	 * elem0=year, elem1=month
	 */
	public int[] getDataCountPerDayOfMonth() {
		return dataCountPerDayOfMonth;
	}

	public void uploadTarget(String path, long fromNanos, long toNanos) {
		config.path = path;
		this.fromNanos = fromNanos;
		this.toNanos = toNanos;
		this.abortCalc = true; // cancel any existing run

		upload();
	}

	private static String uploadSyncObj = new String("UploadSyncObj");

	public void upload() {
		ImageProcUtil.ensureFinalUpdate(uploadSyncObj, new Runnable() {
			@Override
			public void run() {
				doUpload();
			}
		});
	}

	public void doUpload() {
		String path = config.path;
		long fromNanos = this.fromNanos - config.afterProgramToleranceNS;
		long toNanos = this.toNanos + config.afterProgramToleranceNS;
		abortCalc = false;

		// upload all signals with matching path (up to _{Type})
		ArchiveDBDesc desc = resolveDescriptor(path, fromNanos, toNanos);
		ArchiveDBTreeListDesc searchDesc = new ArchiveDBTreeListDesc(desc);
		searchDesc.setChannelName(null);
		searchDesc.setChannelNumber(-1);

		searchDesc.setType(Type.PARLOG);
		List<ArchiveDBDesc> descs = adb.getCachedSignals(searchDesc);

		searchDesc.setType(Type.DATASTREAM);
		descs.addAll(adb.getCachedSignals(searchDesc));

		ArchiveDBFetcher adbNonLocal = new ArchiveDBFetcher();
		adbNonLocal.enableMutableCache(false); // doesn't make sense without this
		adbNonLocal.setUseMemoryCache(false);

		Uploader uploader = new Uploader(adb, false);
		int i = 0;
		for (ArchiveDBDesc desc2 : descs) {
			if (abortCalc)
				return;

			String status = "Upload (" + i + " / " + descs.size() + "): ";
			try {

				boolean inDB = adbNonLocal.hasData(desc2);

				if (inDB) {
					status += "Already in DB.";

				} else {

					uploader.upload(desc2);
					status += "Uploaded OK.";
				}

			} catch (RuntimeException err) {
				status += "FAILED: " + err.getMessage();
				err.printStackTrace();
			}
			System.out.println(status + ": [" + desc2 + "]");
			setStatus(status);

			i++;
		}
		/*
		 * 
		 * ... Should we do this with knowledge of images, or just like writeback for
		 * all matching signals in the given time range??
		 * 
		 * addPathToHistory(path); setStatus("Loading..."); try {
		 * 
		 * seriesMetaData.clear();
		 * 
		 * // and hang the experiment, pulse etc on the image series so stuff knows //
		 * where to write direct data setSeriesMetaData("/w7x/path", path, false);
		 * setSeriesMetaData("/w7x/fromNanosLoaded", fromNanos, false);
		 * setSeriesMetaData("/w7x/toNanosLoaded", toNanos, false);
		 * 
		 * 
		 * ArchiveDBDesc desc = resolveDescriptor(path, fromNanos, toNanos);
		 * 
		 * long[][] intervals = getMergedIntervals(desc);
		 * 
		 * for (int i = 0; i < intervals[0].length; i++) { if(abortCalc) return;
		 * 
		 * ArchiveDBDesc imgDesc = new ArchiveDBDesc(path, intervals[0][i],
		 * intervals[1][i]);
		 * 
		 * Signal imgSig; try { imgSig = adb.getSig(imgDesc); } catch (RuntimeException
		 * err) { System.err.println("W7XArchiveDBSource: Error reading image " + i +
		 * " from signal '" + imgDesc + "' for upload"); continue; } }
		 * 
		 * }catch (RuntimeException err) { err.printStackTrace(); }
		 */
	}

	public ArchiveDBFetcher getADBFetcher() {
		return adb;
	}

	public W7XArchiveDBSourceConfig getConfig() {
		return config;
	}

	@Override
	public void saveConfig(String id) {
		if (id.startsWith("jsonfile:")) {
			saveConfigJSON(id.substring(9));
		}
	}

	public void saveConfigJSON(String fileName) {
		Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().setPrettyPrinting()
				.create();

		String json = gson.toJson(config);

		json = json.replaceAll("},", "},\n");

		OneLiners.textToFile(fileName, json);
	}

	@Override
	public void loadConfig(String id) {
		if (id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}

	@Override
	public String getLastLoadedConfig() {
		return lastLoadedConfig;
	}

	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);

		Gson gson = new Gson();

		config = gson.fromJson(jsonString, W7XArchiveDBSourceConfig.class);

		updateAllControllers();
	}
	
	public int getNumberOfParlogsInRange() { return numberOfParlogsInRange;	}
}
