package imageProc.database.w7xArchiveDB.swt;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import de.mpg.ipp.codac.w7xtime.NanoTime;
import descriptors.w7x.ArchiveDBDesc;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSource;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.database.w7xArchiveDB.W7XArchiveDBPipe;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSink;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSinkConfig;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSinkConfig.TimebaseMode;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** GUI panel for W7X Archive DB source/sink */
public class W7XArchiveDBSinkSWTControl implements ImagePipeController {
	private W7XArchiveDBSink sink;
	
	private Group swtGroup;
	private Combo swtPathCombo;
	private Button swtVersionCheckbox;
	private Spinner swtVersionSpinner;
	
	private Text swtFromNanos;
	private Text swtToNanos;
	private Label swtFromNanosHuman;
	private Label swtToNanosHuman;
	
	private Combo swtTimeMode;
	private Combo swtTimeMetadata;
	private Button swtCheckTimesButton;
	private Label swtCheckTimesInfoLabel;

	private Button saveInvalidCheckbox;
	
	private Button swtSaveButton;
	private Button overwriteInDBCheckbox;
	private Button writeCacheCheckbox;
	private Button writeToDBCheckbox;
	private Spinner maxImagesSpinner;
	private Label swtStatusLabel;
	private Label swtLastWrittenLabel;
	
	private Button swtWriteAliasCheckbox;
	private Combo swtAliasCombo;
	private Button writeAliasNowButton;
	
	private Text swtNotesTextbox;	
	private Button swtLoadNotesButton;
	private Button swtSaveNotesButton;
	
	private SWTSettingsControl settingsCtrl;
	
	private boolean dataChangeInhibit = false;
	
	/** Notes in GUI modified. Don't overwrite with updates! */ 
	private boolean notesModified = false;
	/** Original nanotime of notes being modified */
	private long notesNanotime = -1;
	
		
	public W7XArchiveDBSinkSWTControl(Composite parent, Integer style, W7XArchiveDBSink sink) {
		this.sink = sink;
		swtGroup = new Group(parent, style);
		
		dataChangeInhibit = true;
		
		swtGroup.setLayout(new GridLayout(5, false));
		swtGroup.setText("W7X Sink");
				
		Label lS = new Label(swtGroup, 0); lS.setText("Status:");
		swtStatusLabel = new Label(swtGroup, SWT.NONE);
		swtStatusLabel.setText("init\n.\n.\n.");
		swtStatusLabel.setToolTipText("Save status of images, of time series meta data (per image) and of per-series metadata");
		swtStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

		Label lLW = new Label(swtGroup, 0); lLW.setText("Last saved:");
		swtLastWrittenLabel = new Label(swtGroup, SWT.NONE);
		swtLastWrittenLabel.setText("None");		
		swtLastWrittenLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lP = new Label(swtGroup, 0); lP.setText("Path:");
		swtPathCombo = new Combo(swtGroup, SWT.None);
		swtPathCombo.setToolTipText("DataSignals path. This is 'w7x/' followed by the full path including database that can be "
										+ "copied/pasted to/from archive-webapi.ipp-hgw.mpg.de.");
		swtPathCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtPathCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lV = new Label(swtGroup, 0); lV.setText("Version:");
		swtVersionSpinner = new Spinner(swtGroup, SWT.NONE);
		swtVersionSpinner.setValues(0, 0, Integer.MAX_VALUE, 0, 1, 10);
		swtVersionSpinner.setToolTipText("Version to write to, or 0 to write to latest version.");
		swtVersionSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtVersionSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
				
		swtVersionCheckbox = new Button(swtGroup, SWT.CHECK);
		swtVersionCheckbox.setText("Versioned");
		swtVersionCheckbox.setToolTipText("If set, images are written to a versioned datastream with the given or latest version.");
		swtVersionCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtVersionCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
				
		Label lTS = new Label(swtGroup, 0); lTS.setText("Timestamp:");
		swtTimeMode = new Combo(swtGroup, SWT.None);
		swtTimeMode.setItems(new String[]{ "Auto", "Image Number", "Specified" });
		swtTimeMode.setToolTipText("Source of the timestamps. From meta data or 'Image Number' - One image every second up to 'now'");
		swtTimeMode.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		swtTimeMode.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		swtTimeMetadata = new Combo(swtGroup, SWT.None);
		swtTimeMetadata.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		swtTimeMetadata.setToolTipText("The metadata entry to be used as timestamps. Must be an array of longs");
		swtTimeMetadata.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		try {
			updateTimeMetadataCombo();
		}catch(RuntimeException err){
			Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Error listing timebases", err);
		}

		swtCheckTimesButton = new Button(swtGroup, SWT.PUSH);				
		swtCheckTimesButton.setText("Check");
		swtCheckTimesButton.setToolTipText("Determine the timestamp source, generate timestamp range and see what is already stored in the database");
		swtCheckTimesButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { swtUpdateTimesButton(event); } });
		swtCheckTimesButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Label lFN = new Label(swtGroup, SWT.None); lFN.setText("From:");
		swtFromNanos = new Text(swtGroup, SWT.None);
		swtFromNanos.setText("0");
		swtFromNanos.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Label lTN = new Label(swtGroup, SWT.None); lTN.setText("To:");
		swtToNanos = new Text(swtGroup, SWT.None);
		swtToNanos.setText("0");
		swtToNanos.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		swtFromNanos.setEditable(false);
		swtToNanos.setEditable(false);
		
		Label lFNH = new Label(swtGroup, SWT.NONE); lFNH.setText("");
		swtFromNanosHuman = new Label(swtGroup, SWT.NONE);
		swtFromNanosHuman.setText(" --- ");
		swtFromNanosHuman.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		Label lTNH = new Label(swtGroup, SWT.NONE); lTNH.setText("");
		swtToNanosHuman = new Label(swtGroup, SWT.NONE);
		swtToNanosHuman.setText(" --- ");
		swtToNanosHuman.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));	
		
		swtCheckTimesInfoLabel = new Label(swtGroup, SWT.NONE); 
		swtCheckTimesInfoLabel.setText("Check result: --");
		swtCheckTimesInfoLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		
		saveInvalidCheckbox = new Button(swtGroup, SWT.CHECK);
		saveInvalidCheckbox.setText("Save invalid");
		saveInvalidCheckbox.setToolTipText("If set, all images are saved regardless of whether they have validated ranges. This means some images might be saved from old acquisitions, but should be at the right timestamp, maybe.");
		saveInvalidCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		saveInvalidCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Label lSM = new Label(swtGroup, SWT.NONE); lSM.setText("Max images:");
		maxImagesSpinner = new Spinner(swtGroup, SWT.NONE);
		maxImagesSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 10, 100);
		maxImagesSpinner.setToolTipText("Save ony the first n images to the database. Useful if the data or timestamps are corrupt in the middle of the allocated set");
		maxImagesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { maxImagesSpinnerEvent(event); } });
		maxImagesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		
		writeCacheCheckbox = new Button(swtGroup, SWT.CHECK);
		writeCacheCheckbox.setText("Write to cache");
		writeCacheCheckbox.setToolTipText("Write data to the local archiveDB cache, and don't actually attempt to write to the WebAPI now.");
		writeCacheCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		writeCacheCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		writeToDBCheckbox = new Button(swtGroup, SWT.CHECK);
		writeToDBCheckbox.setText("Upload to archiveDB");
		writeToDBCheckbox.setToolTipText("Upload data to the W7-X archive via WebAPI now.");
		writeToDBCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		writeToDBCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		overwriteInDBCheckbox = new Button(swtGroup, SWT.CHECK);
		overwriteInDBCheckbox.setText("Overwrite in DB");
		overwriteInDBCheckbox.setToolTipText("Attempts to write data to archive even if data is already detected at the timestamps.");
		overwriteInDBCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		overwriteInDBCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Label lAP = new Label(swtGroup, 0); lAP.setText("Alias:");
		swtAliasCombo = new Combo(swtGroup, SWT.None);
		swtAliasCombo.setToolTipText("Alias path to update to point to this stream for the image range being saved. The alias is written to the database "
								+ "regardless of whether the images are written to the cache or the database.");
		swtAliasCombo.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event);	 } });
		swtAliasCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		swtWriteAliasCheckbox = new Button(swtGroup, SWT.CHECK);
		swtWriteAliasCheckbox.setText("Set on save");
		swtWriteAliasCheckbox.setToolTipText("Update the given alias when saving the images.");
		swtWriteAliasCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtWriteAliasCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		writeAliasNowButton = new Button(swtGroup, SWT.PUSH);
		writeAliasNowButton.setText("Set");
		writeAliasNowButton.setToolTipText("Update the given alias now for the current image set.");
		writeAliasNowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { writeAliasButtonEvent(event); } });
		writeAliasNowButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		swtSaveButton = new Button(swtGroup, SWT.PUSH);				
		swtSaveButton.setText("Save images");		
		swtSaveButton.setToolTipText("Saves images and metadata to archiveDB.");
		swtSaveButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveButtonEvent(event); } });
		swtSaveButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
						
		Group swtNotesGroup = new Group(swtGroup, SWT.BORDER);
		swtNotesGroup.setText("Notes (xxxx_Notes_PARLOG)");
		swtNotesGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		swtNotesGroup.setLayout(new GridLayout(2, true));
		
		swtNotesTextbox = new Text(swtNotesGroup, SWT.MULTI);
		swtNotesTextbox.setText("[EMPTY]");
		swtNotesTextbox.setEditable(true);
		swtNotesTextbox.setToolTipText("Notes to be written to the to xxxx_Notes_PARLOG.\nBlack: Unmodified\nBlue: Modified/unsaved. \nRed: Modified from a different timestamp. Will overwrite!");
		swtNotesTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { notesModifiedButtonEvent(event); } });
		swtNotesTextbox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		swtSaveNotesButton = new Button(swtNotesGroup, SWT.PUSH);				
		swtSaveNotesButton.setText("Save notes");		
		swtSaveNotesButton.setToolTipText("Saves the notes to xxx_Notes_PARLOG at the current time range.");
		swtSaveNotesButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveNotesButtonEvent(event); } });
		swtSaveNotesButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
				
		swtLoadNotesButton = new Button(swtNotesGroup, SWT.PUSH);				
		swtLoadNotesButton.setText("Load notes");		
		swtLoadNotesButton.setToolTipText("Load the notes to xxx_Notes_PARLOG at the current time range.");
		swtLoadNotesButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadNotesButtonEvent(event); } });
		swtLoadNotesButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, sink);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 5, 1));
		

		
		//swtNotesGroup.layout(true, true);
		swtGroup.layout(true, true);
				
		dataChangeInhibit = false;
		generalControllerUpdate();
	}
	
	protected void writeAliasButtonEvent(Event event) {
		sink.updateAlias();
	}

	private void updateTimeMetadataCombo() {
		ArrayList<String> entries = ((W7XArchiveDBSink)sink).getPossibleTimebases();
		
		String oldSelected = swtTimeMetadata.getText();
		swtTimeMetadata.removeAll();
		int idx = 0, idxSel = -1;
		for(String entry : entries) { 
			swtTimeMetadata.add(entry);
			if(oldSelected.equalsIgnoreCase(entry))
				idxSel = idx;			
			idx++;
		}
					
		swtTimeMetadata.select(idxSel >= 0 ? idxSel : 0);
		
	}
	
	private void setSinkTimebaseMetadata() {
		switch(swtTimeMode.getSelectionIndex()){
			case 0:
				sink.setTimebaseMetadata(TimebaseMode.AUTOSELECT, null);
				break;
			case 1:
				sink.setTimebaseMetadata(TimebaseMode.IMGNUM, null);
				break;
			case 2:
				sink.setTimebaseMetadata(TimebaseMode.SPECIFIED, swtTimeMetadata.getText());
				break;
		}
	}
		
	private void settingsChangedEvent(Event event){
		if(dataChangeInhibit)
			return;
		
		try {		
			dataChangeInhibit = true;
			W7XArchiveDBSinkConfig config = sink.getConfig();
			
			config.path = W7XArchiveDBPipe.pathFixup(swtPathCombo.getText());
			//sink.setNotes(swtNotesTextbox.getText());
			
			if(swtVersionCheckbox.getSelection()){
				config.version = swtVersionSpinner.getSelection();
				swtVersionSpinner.setEnabled(true);
			}else{
				config.version = -1;
				swtVersionSpinner.setEnabled(false);
			}
			
			sink.setCacheEnable(false);
			
			config.saveInvalid = saveInvalidCheckbox.getSelection();
			config.overwriteInDB = overwriteInDBCheckbox.getSelection();		
			config.writeToDB = writeToDBCheckbox.getSelection();
			config.writeToCache = writeCacheCheckbox.getSelection();
			
			config.aliasPath = W7XArchiveDBPipe.pathFixup(swtAliasCombo.getText());
			config.updateAlias = swtWriteAliasCheckbox.getSelection();
			
			setSinkTimebaseMetadata();
			
			setTextColour();
		}catch(RuntimeException err) {
			Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Exception processing GUI settings", err);
			
		} finally {
			dataChangeInhibit = false;
		}
	}
	
	private void setPath() {
		W7XArchiveDBSinkConfig config = sink.getConfig();		
		config.path = W7XArchiveDBPipe.pathFixup(swtPathCombo.getText());		
	}
	
	private void swtUpdateTimesButton(Event event) {
		setPath();
		updateTimeMetadataCombo();
		try{
			setSinkTimebaseMetadata(); //set the timebase
			
			//and ask for the currently existing images (blocks!)
			int nExists = sink.getExistingCount();
			
			String status = sink.getLastTimestampWarning();
			if(status == null){
				swtCheckTimesInfoLabel.setText("Timebase OK, " + nExists + " images already in DataStream.");
			
				if(nExists > 0){
					swtCheckTimesInfoLabel.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLUE));
				}else{
					swtCheckTimesInfoLabel.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLACK));
				}
			}else{
				swtCheckTimesInfoLabel.setText("Timebase warning(s), last: " + status);
				swtCheckTimesInfoLabel.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_DARK_RED));
				
			}
			
		}catch(RuntimeException err){
			swtCheckTimesInfoLabel.setText("ERROR: " + err);
			swtCheckTimesInfoLabel.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_RED));
		}
		
		
		//sink.updateAllControllers();
		generalControllerUpdate();
	}
		
	private void setTextColour(){
		Color textCol;
		if(swtPathCombo.getText().equals(sink.getPath()) && 
				Algorithms.mustParseLong(swtFromNanos.getText()) == sink.getFromNanos() &&
				Algorithms.mustParseLong(swtToNanos.getText()) == sink.getToNanos()){
			
			
				textCol = swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLACK);
			
		}else{
			textCol = swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLUE);
		}
		
		swtPathCombo.setForeground(textCol);
		swtFromNanos.setForeground(textCol);
		swtToNanos.setForeground(textCol);
		
	}
				
	public void maxImagesSpinnerEvent(Event event){
		sink.setMaxImages(maxImagesSpinner.getSelection());		
	}
	
	public void saveSettingsChangeEvent(Event event){
		// nothing to do
	}

	public void saveButtonEvent(Event event){
		setPath();
		setSinkTimebaseMetadata();
		sink.save();
	}
	
	public void notesModifiedButtonEvent(Event event){
		notesModified = true;
		swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLUE));
	}
	
	public void saveNotesButtonEvent(Event event){
		setPath();
		setSinkTimebaseMetadata();
		
		sink.saveNotes(swtNotesTextbox.getText());
				
		notesModified = false;
		notesNanotime = sink.getFromNanos();
		swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		
	}
	
	public void loadNotesButtonEvent(Event event){
		setPath();
		setSinkTimebaseMetadata(); //need the times to be set right
		
		//invalidate notes cache to force when the button is explicitly pressed
		ArchiveDBDesc desc = new ArchiveDBDesc(sink.getPath(), sink.getFromNanos(), sink.getToNanos());
		sink.loadedNotes.put(desc.toString(), null);
		
		swtNotesTextbox.setText(sink.loadNotes());
		
		swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		notesModified = false;
		notesNanotime = sink.getFromNanos();
	}
	
	public W7XArchiveDBPipe getSource() { return sink; };

	@Override
	public Object getInterfacingObject() { return swtGroup; }

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this,  new Runnable() { 
			@Override public void run() {
				if(!dataChangeInhibit) {
					try {
						dataChangeInhibit = true; //don't allow dataChangingEvent() re-fire this update
						doUpdate();
					}finally {
						dataChangeInhibit = false;
					}
				} 
			}});
	}

	private void doUpdate(){
		
		W7XArchiveDBSinkConfig config = sink.getConfig();
		
		swtStatusLabel.setText(sink.getStatus());
		
		swtLastWrittenLabel.setText(sink.getPath() + "\n/" + 
									sink.getFromNanos() + "_" + sink.getToNanos() + ":\n ");
		
		switch(config.timebaseMode){			
			case AUTOSELECT: swtTimeMode.select(0); break;
			case IMGNUM: swtTimeMode.select(1); break;
			case SPECIFIED: swtTimeMode.select(2); break;		
		}
		swtTimeMetadata.setText((config.timebaseMetadataName != null) ? config.timebaseMetadataName : "-- None available --");
						
		swtFromNanos.setText(Long.toString(sink.getFromNanos()));
		swtToNanos.setText(Long.toString(sink.getToNanos()));
		swtFromNanosHuman.setText(NanoTime.toUTCString(sink.getFromNanos()));
		swtToNanosHuman.setText(NanoTime.toUTCString(sink.getToNanos()));
		
		
		saveInvalidCheckbox.setSelection(config.saveInvalid);		
		overwriteInDBCheckbox.setSelection(config.overwriteInDB);
		writeCacheCheckbox.setSelection(config.writeToCache);
		writeToDBCheckbox.setSelection(config.writeToDB);
		
		if(!swtPathCombo.isFocusControl()) {
			String current = swtPathCombo.getText();
			String hist[] = sink.getPathHistory();
			swtPathCombo.setItems(hist);
			int i;
			for(i=0; i < hist.length; i++) {
				if(current.equals(hist[i])) {
					swtPathCombo.select(i);
					break;
				}
			}
			if(i >= hist.length) {
				swtPathCombo.setText(sink.getPath());
			}
		}
		
		swtVersionCheckbox.setSelection((config.version >= 0));
		swtVersionSpinner.setSelection(config.version);

		if(!notesModified && !swtNotesTextbox.isFocusControl()){
			try{
				String notes = sink.loadNotes();
				swtNotesTextbox.setText(notes != null ? notes : "[EMPTY]");				
			}catch(Exception err){
				err.printStackTrace();
			}
			notesModified = false;
			notesNanotime = sink.getFromNanos();
			swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		}else{
			swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(
					(notesNanotime == sink.getFromNanos()) ? SWT.COLOR_BLUE : SWT.COLOR_RED));
		}
		
		setTextColour();
		
		swtAliasCombo.setText(config.aliasPath == null ? "" : config.aliasPath);
		swtWriteAliasCheckbox.setSelection(config.updateAlias);
		
	}
	
	@Override
	public void destroy() {
		swtGroup.dispose();		
		sink.controllerDestroyed(this);
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return sink; }
}
