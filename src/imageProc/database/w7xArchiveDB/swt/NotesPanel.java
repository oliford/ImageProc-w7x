package imageProc.database.w7xArchiveDB.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import de.mpg.ipp.codac.w7xtime.NanoTime;
import imageProc.core.ImageProcUtil;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource.Program;

public class NotesPanel {
	private W7XArchiveDBSource src;

	private Group swtGroup;
	private W7XArchiveDBSourceSWTControl srcCtrl;

	private Text swtNotesTextbox;
	private Button swtSaveNotesButton;
	private Button swtLoadNotesButton;
	
	/** Notes in GUI modified. Don't overwrite with updates! */ 
	private boolean notesModified = false;
	/** Original nanotime of notes being modified */
	private long notesNanotime = -1;
	
	private boolean dataChangeInhibit = false;

			
	public NotesPanel(W7XArchiveDBSourceSWTControl srcCtrl, W7XArchiveDBSource src, Composite parent, int style) {
		this.srcCtrl = srcCtrl;
		this.src = src;
		
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setLayout(new GridLayout(2, false));
		
		Label lN = new Label(swtGroup, SWT.NONE);
		lN.setText("Notes (from xxxx_Notes_Parlog):");
		lN.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		swtNotesTextbox = new Text(swtGroup, SWT.MULTI);
		swtNotesTextbox.setText(".\n.\n.\n");
		swtNotesTextbox.setToolTipText("Notes to be written to the to xxxx_Notes_PARLOG.\nBlack: Unmodified\nBlue: Modified/unsaved. \nRed: Modified from a different timestamp. Will overwrite!");
		swtNotesTextbox.setEditable(true);
		swtNotesTextbox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		swtNotesTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { notesModifiedButtonEvent(event); } });

		swtSaveNotesButton = new Button(swtGroup, SWT.PUSH);				
		swtSaveNotesButton.setText("Save notes");		
		swtSaveNotesButton.setToolTipText("Saves the notes to xxx_Notes_PARLOG at the current time range.");
		swtSaveNotesButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { saveNotesButtonEvent(event); } });
		swtSaveNotesButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		swtLoadNotesButton = new Button(swtGroup, SWT.PUSH);				
		swtLoadNotesButton.setText("Load notes");		
		swtLoadNotesButton.setToolTipText("Load the notes to xxx_Notes_PARLOG at the current time range.");
		swtLoadNotesButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadNotesButtonEvent(event); } });
		swtLoadNotesButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		ImageProcUtil.addRevealWriggler(swtGroup);
        doUpdate();
	}
	

	public void programChangedEvent(long minNanoRange, long maxNanoRange){
		if(dataChangeInhibit)
			return;
		
		if(srcCtrl.programList != null && srcCtrl.programList.size() > 0){
				
			if(!notesModified){
				int selIndices[] = srcCtrl.swtProgramList.getSelectionIndices();
				Program prog = srcCtrl.programList.get(selIndices[0]);				
				String notes = src.getNotes(srcCtrl.swtPathCombo.getText(), prog.from(), prog.upto());
						
				swtNotesTextbox.setText(notes);
				notesModified = false;
				notesNanotime = minNanoRange;
				swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLACK));
				
			}else if(minNanoRange != notesNanotime){
				
				swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_RED));
			}
		}else{

			swtNotesTextbox.setText("No program selected");
			notesModified = false;
			notesNanotime = -1;
			swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_GRAY));
		}
		
	}


	public Group getSWTGroup() { return swtGroup; }


	public void notesModifiedButtonEvent(Event event){
		notesModified = true;
		swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLUE));
	}

	public void saveNotesButtonEvent(Event event){
		int selIndices[] = srcCtrl.swtProgramList.getSelectionIndices();
		Program prog = srcCtrl.programList.get(selIndices[0]);				
		
		src.saveNotes(srcCtrl.swtPathCombo.getText().trim(),
				prog.from(),
				prog.upto(),
				swtNotesTextbox.getText());
			
		notesModified = false;
		swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLACK));
			
	}

	public void loadNotesButtonEvent(Event event){
		srcCtrl.checkGraffiti();
		
		int selIndices[] = srcCtrl.swtProgramList.getSelectionIndices();
		Program prog = srcCtrl.programList.get(selIndices[0]);
		
		src.clearNotesCache(); //clear to goddam cash goddamit
		
		String notes = src.getNotes(srcCtrl.swtPathCombo.getText(), prog.from(), prog.upto());		
		swtNotesTextbox.setText(notes);
		swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		notesModified = false;
	}


	void doUpdate() {

		//fill in notes of first entry
		if(!notesModified){
			String programNames[] = srcCtrl.swtProgramList.getSelection();
			if(programNames.length > 0){
				int selIndices[] = srcCtrl.swtProgramList.getSelectionIndices();
				Program prog = srcCtrl.programList.get(selIndices[0]);				
				String notes = src.getNotes(srcCtrl.swtPathCombo.getText(), prog.from(), prog.upto());
				
				swtNotesTextbox.setText(notes);
				notesModified = false;
				swtNotesTextbox.setForeground(swtNotesTextbox.getDisplay().getSystemColor(SWT.COLOR_BLACK));
			}
		}
		
	}

}