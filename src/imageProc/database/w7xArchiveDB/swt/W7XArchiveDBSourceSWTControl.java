package imageProc.database.w7xArchiveDB.swt;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.mpg.ipp.codac.w7xtime.NanoTime;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.database.w7xArchiveDB.W7XArchiveDBPipe;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSinkConfig;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource.Program;
import imageProc.pilot.geri.swt.StateSWTControl;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSourceConfig;
import imageProc.w7xspec.Defaults.Table;
import imageProc.w7xspec.LightPaths;
import imageProc.w7xspec.LightPaths.Component;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

/** GUI panel for W7X Archive DB source/sink */
public class W7XArchiveDBSourceSWTControl implements ImagePipeController {
	private W7XArchiveDBSource pipe;
	
	private Group swtGroup;
	private Button refreshProgramsButton;
	private Button getProgDataListButton;
	private Button getProgDataAvailableButton;
	private Combo swtLPTableCombo;
	private Combo swtLPComponentCombo;
	Combo swtPathCombo;
	private DateTime swtDateTime;
	private Text jumpToTextbox;
	
	org.eclipse.swt.widgets.List swtProgramList;
	
	List<Program> programList = null;
	private int programListHashCode = 0;
	
	private Text swtFromNanos;
	private Text swtToNanos;
	private Label swtFromNanosHuman;
	private Label swtToNanosHuman;
	
	
	private Button swtLoadButton;
	private Button swtUploadButton;
	private Label swtStatusLabel;
	private Label swtLastLoadedLabel;
	
	private NotesPanel notesPanel;
	private SettingsPanel settingsPanel;
	
	private CTabFolder swtTabFoler;	
	private CTabItem swtNotesTab;	
	private CTabItem swtSettingsTab;	
	
	private boolean dataChangeInhibit = false;
	
	/** Select the given program number in the list once the list has been loaded. 
	 *  This is set when putting a shot number in the jump text box */
	private int jumpToProgramOnListLoad = -1;
	
	private ArrayList<Composite> swtGraffitiComposites;

	/** Currently selected LigthPaths component */
	private Component selectedLPComponent = null;

	/** List of lightPaths components from currently selected table */
	private Component[] lpComponents;
			
	private Logger logr = Logger.getLogger(this.getClass().getName());
	
	public W7XArchiveDBSourceSWTControl(Composite parent, Integer style, W7XArchiveDBSource pipe) {		
		this.pipe = pipe;
		dataChangeInhibit = true;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setLayout(new GridLayout(1, false));
		swtGroup.setText("W7X Archive Source");
		
		SashForm swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		swtSashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        swtSashForm.setLayout(new FillLayout());
       
        Group swtTopGroup = new Group(swtSashForm, SWT.BORDER);
        swtTopGroup.setLayout(new GridLayout(5, false));
		//swtTopGroup.setText("W7X Archive Source");
		
		Label lS = new Label(swtTopGroup, 0); lS.setText("Status:");
		swtStatusLabel = new Label(swtTopGroup, SWT.NONE);
		swtStatusLabel.setText("Initialising\n-\n-\n-\n");
		swtStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lLW = new Label(swtTopGroup, 0); lLW.setText("Last loaded:");
		swtLastLoadedLabel = new Label(swtTopGroup, SWT.NONE);
		swtLastLoadedLabel.setText("None");
		swtLastLoadedLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
	
		Composite swtGroupLeft = new Composite(swtTopGroup, SWT.NONE);
		swtGroupLeft.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		swtGroupLeft.setLayout(new GridLayout(5, false));
		
		Label lLPT = new Label(swtGroupLeft, 0); lLPT.setText("LightPaths: Table:");
		swtLPTableCombo = new Combo(swtGroupLeft, SWT.None);
		swtLPTableCombo.setToolTipText("LightPaths table. i.e. diagnostics KKS code");
		swtLPTableCombo.setItems(new String[] { " -- Not selected -- "} );
		for(String name : Mat.toStringArray(Table.values()))
			swtLPTableCombo.add(name);
		swtLPTableCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { lpTableChangingEvent(); } });
		swtLPTableCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Label lLPC = new Label(swtGroupLeft, 0); lLPC.setText("Component:");
		swtLPComponentCombo = new Combo(swtGroupLeft, SWT.None);
		swtLPComponentCombo.setToolTipText("LightPaths component.");
		swtLPComponentCombo.setItems(new String[0]);
		swtLPComponentCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { lpComponentChangingEvent(); } });
		swtLPComponentCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));

		Label lP = new Label(swtGroupLeft, 0); lP.setText("Path:");
		swtPathCombo = new Combo(swtGroupLeft, SWT.None);
		swtPathCombo.setToolTipText("DataSignals path. This is 'w7x/' followed by the full path including database that can be "
				+ "copied/pasted to/from archive-webapi.ipp-hgw.mpg.de.\n"
				+ "i.e. 'w7x/ArchiveDB/raw/... Available paths in LightPaths DB are displayed if component is selected, otherwise "
				+ "recently used items on this PC are shown.");
		swtPathCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { dataChangingEvent(); } });
		swtPathCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

		Label lB = new Label(swtGroupLeft, 0); lB.setText("");
		refreshProgramsButton = new Button(swtGroupLeft, SWT.PUSH);	
		refreshProgramsButton.setText("Refresh programs");
		refreshProgramsButton.setToolTipText("Invalidate and refetch programs list from archive");
		refreshProgramsButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		refreshProgramsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { refreshProgramsButtonEvent(event); } });
			
		getProgDataListButton = new Button(swtGroupLeft, SWT.PUSH);	
		getProgDataListButton.setText("Fill list");
		getProgDataListButton.setToolTipText("Fill the list with all available images for the day, whether or not they're in a W7X program.");
		getProgDataListButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		getProgDataListButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { genProgDataListButtonEvent(event); } });
		
		getProgDataAvailableButton = new Button(swtGroupLeft, SWT.PUSH);	
		getProgDataAvailableButton.setText("Fill Calendar");
		getProgDataAvailableButton.setToolTipText("Fill the calendar with boxes representing which days have programs and/or data.\nUpper box: Blue=Programs, Gray=None\nLower box: Green=Data, Gray=None");
		getProgDataAvailableButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		getProgDataAvailableButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { genProgDataCountsButtonEvent(event); } });
				
		Label lI = new Label(swtGroupLeft, 0); lI.setText("\n\nPrograms and data:\nID: Full Name [images in cache / in archive]");
		lI.setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 5, 1));
		
		Composite swtGroupRight = new Composite(swtTopGroup, SWT.NONE);
		swtGroupRight.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		swtGroupRight.setLayout(new GridLayout(4, false));
		
		swtDateTime = new DateTime(swtGroupRight, SWT.CALENDAR);
		swtDateTime.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1));
		swtDateTime.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { dateChangedEvent(); } });
		Calendar now = Calendar.getInstance();
		swtDateTime.setDate(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
		
		Label lPN = new Label(swtGroupRight, SWT.None); lPN.setText("Jump to:");
		jumpToTextbox = new Text(swtGroupRight, SWT.NONE);
		jumpToTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 3, 1));
		jumpToTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { jumpToTextboxEvent(event); } });
		jumpToTextbox.addListener(SWT.KeyUp, new Listener() { 
			@Override public void handleEvent(Event event) { 
				if(event.character == '\r')
					jumpToTextboxEvent(event); } 
			});
			
		
		swtGraffitiComposites = new ArrayList<Composite>();
		
		Group swtMidGroup = new Group(swtSashForm, SWT.BORDER);
		swtMidGroup.setLayout(new GridLayout(5, false));
		//swtTopGroup.setText("W7X Archive Source");
		
		swtProgramList = new org.eclipse.swt.widgets.List(swtMidGroup, SWT.V_SCROLL | SWT.MULTI);
		swtProgramList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		swtProgramList.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { programChangedEvent(); } });
			
		Label lFN = new Label(swtMidGroup, SWT.None); lFN.setText("From:");
		swtFromNanos = new Text(swtMidGroup, SWT.None);
		swtFromNanos.setText("0");
		swtFromNanos.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		Label lTN = new Label(swtMidGroup, SWT.None); lTN.setText("To:");
		swtToNanos = new Text(swtMidGroup, SWT.None);
		swtToNanos.setText("0");
		swtToNanos.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		swtLoadButton = new Button(swtMidGroup, SWT.PUSH);			
		swtLoadButton.setText("Load");
		swtLoadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { loadButtonEvent(event); } });
		
		Label lFNH = new Label(swtMidGroup, SWT.NONE); lFNH.setText("");
		swtFromNanosHuman = new Label(swtMidGroup, SWT.NONE);
		swtFromNanosHuman.setText(" --- ");
		swtFromNanosHuman.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		Label lTNH = new Label(swtMidGroup, SWT.NONE); lTNH.setText("");
		swtToNanosHuman = new Label(swtMidGroup, SWT.NONE);
		swtToNanosHuman.setText(" --- ");		
		swtToNanosHuman.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));	
		
		swtUploadButton = new Button(swtMidGroup, SWT.PUSH);			
		swtUploadButton.setText("Upload");
		swtUploadButton.setToolTipText("Upload all cached signals in the selected time range to archiveDB, if they are not already there.");
		swtUploadButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { uploadButtonEvent(event); } });
		
		swtTabFoler = new CTabFolder(swtSashForm, SWT.BORDER);
        swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));

        notesPanel = new NotesPanel(this, pipe, swtTabFoler, SWT.NONE);
		swtNotesTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtNotesTab.setControl(notesPanel.getSWTGroup());
		swtNotesTab.setText("Notes");

        settingsPanel = new SettingsPanel(this, pipe, swtTabFoler, SWT.NONE);
		swtSettingsTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtSettingsTab.setControl(settingsPanel.getSWTGroup());
		swtSettingsTab.setText("Settings");
        
		swtPathCombo.setItems(pipe.getPathHistory());
		swtPathCombo.setText(pipe.getPath());
		checkPathPrefix();
		swtFromNanos.setText(Long.toString(pipe.getFromNanos()));
		swtToNanos.setText(Long.toString(pipe.getToNanos()));
		swtFromNanosHuman.setText(NanoTime.toUTCString(pipe.getFromNanos()));
		swtToNanosHuman.setText(NanoTime.toUTCString(pipe.getToNanos()));

		swtSashForm.setWeights(new int[]{ 2, 2, 1 });
		swtTopGroup.layout(true, true);
				
		dataChangeInhibit = false;
		generalControllerUpdate();
	}
	
	protected void jumpToTextboxEvent(Event event) {
		String jump = jumpToTextbox.getText();

		Pattern p = Pattern.compile("^(20)?([0-9][0-9])([0-9][0-9])([0-9][0-9])(\\.[0-9]*)?");
		Matcher m = p.matcher(jump);
		
		int year = -1;
		int month = -1;
		int day = -1;
		int program = -1;
		
		if(m.matches()){
			year = 2000 + Integer.parseInt(m.group(2));
			month = Integer.parseInt(m.group(3)) - 1; //0 based month
			day = Integer.parseInt(m.group(4));
			program = (m.group(5) != null) ? Integer.parseInt(m.group(5).substring(1)) : -1;
		}
		
		if(year < 2015 || year > 3000 || month < 0 || month > 11 || day <= 0 || day > 30){
			pipe.setStatus("Invalid jump format of '"+jump+"'. Expected YYMMDD[.xxx]");
			return;
		}		
		
		Calendar currentDate = pipe.getProgramListDate();
		if(currentDate == null ||
				year != currentDate.get(Calendar.YEAR) ||
				month != currentDate.get(Calendar.MONTH) ||
				day != currentDate.get(Calendar.DAY_OF_MONTH)){		
			//date is changed, 
			swtDateTime.setDate(year, month, day); //change the calendar
			jumpToProgramOnListLoad = program; //queue the shot selection once list is loaded
			dateChangedEvent(); //and trigger list load
		}else{
			//otherwise we only need to select the shot
			selectProgram(program);
		}
	}
	
	private void selectProgram(int program){

		List<Program> progList = pipe.getProgramDataList();
		String progIDEnd = "." + String.format("%03d", program);		
		for(int i=0; i < progList.size(); i++){
			String id = progList.get(i).id;
			if(id != null && id.endsWith(progIDEnd)){
				swtProgramList.setSelection(i);
				programChangedEvent();
			}
		}
	}

	private String lpUpdateSyncObj = new String("lpUpdateSyncObj"); 
	protected void lpTableChangingEvent() {
		final String table = swtLPTableCombo.getText();
		if(table.startsWith(" --")) {
			swtLPComponentCombo.setText(" -- Not selected -- ");
			pipe.updateAllControllers();
			return;
		}
			
		swtLPComponentCombo.removeAll();		
		swtLPComponentCombo.setText(" -- Loading -- ");
		pipe.updateAllControllers();
		lpComponents = null;
		
		final long time = NanoTime.convertYMDhmsUTCtoW7Xtime(
				swtDateTime.getYear(), 
				swtDateTime.getMonth(), 
				swtDateTime.getDay(), 12, 0, 0, 0);
		
		ImageProcUtil.ensureFinalUpdate(lpUpdateSyncObj, new Runnable() {
			
			@Override
			public void run() {
				lpComponents = null;
				try {
					lpComponents = LightPaths.getComponents(table, time);
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Error getting components from lightPaths DB", err);
				}
				ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), fillLPComponentsListSyncObj, () -> fillLPComponentsList());
			}
		});
	}
	
	private String fillLPComponentsListSyncObj = new String("fillLPComponentsListSyncObj"); 
	private void fillLPComponentsList() {
		
		swtLPComponentCombo.removeAll();
		try {
			if(lpComponents == null) {
				swtLPComponentCombo.add(" -- Error or no entries -- ");
				swtLPComponentCombo.setEnabled(false);
				swtLPComponentCombo.select(0);
				return;
			}
			
			swtLPComponentCombo.add(" -- Not selected -- ");
			for(Component c : lpComponents){
				System.out.println(c.name);
				if(c.datastreamPaths != null && c.datastreamPaths.length > 0)
					swtLPComponentCombo.add(c.name);
			}
			swtLPComponentCombo.setEnabled(true);
			swtLPComponentCombo.select(0);
		}catch(RuntimeException err) {
			String table = swtLPTableCombo.getText();
			long time = NanoTime.convertYMDhmsUTCtoW7Xtime(
					swtDateTime.getYear(), 
					swtDateTime.getMonth(), 
					swtDateTime.getDay(), 12, 0, 0, 0);
			
			logr.log(Level.WARNING, "Unable to load components for table '"+table+"' at time '"+time+"'", err);
			
			swtLPComponentCombo.setText(" -- Error loading components -- ");
			swtLPComponentCombo.setEnabled(false);
		}
	}
	
	protected void lpComponentChangingEvent() {
		
		String componentName = swtLPComponentCombo.getText();
		if(componentName.startsWith(" --") || componentName.length() == 0) {
			selectedLPComponent = null;
			swtPathCombo.setItems(pipe.getPathHistory());
			return;
		}
		try {
			selectedLPComponent = null;
			for(int i=0; i < lpComponents.length; i++) {
				if(componentName.equals(lpComponents[i].name)) {
					selectedLPComponent = lpComponents[i];
					break;
				}
			}
			//selectedLPComponent = LightPaths.getComponent(table, componentName, time);
			
			if(selectedLPComponent != null && selectedLPComponent.datastreamPaths != null)
				swtPathCombo.setItems(selectedLPComponent.datastreamPaths);
			
			swtPathCombo.select(0);
		}catch(RuntimeException err) {
			logr.log(Level.WARNING, "Error getting component info for '"+componentName+"' from LightPaths.", err);
			selectedLPComponent = null;
			
		}
	}
	

	public void dateChangedEvent(){
		if(dataChangeInhibit)
			return;

		pipe.generateProgramList(swtPathCombo.getText().trim(), swtDateTime.getYear(), swtDateTime.getMonth()+1, swtDateTime.getDay(), false);
		
	}
	
	public void programChangedEvent(){
		if(dataChangeInhibit)
			return;
		
		long minNanoRange = Long.MAX_VALUE;
		long maxNanoRange = Long.MIN_VALUE;
		if(programList != null && programList.size() > 0){
			int selIndices[] = swtProgramList.getSelectionIndices();
			for(int idx : selIndices){
				Program prog = programList.get(idx);
				
				if(prog.from() < minNanoRange)
					minNanoRange = prog.from();
				if(prog.upto() > maxNanoRange)
					maxNanoRange = prog.upto();
			}
		}
		swtFromNanos.setText(Long.toString(minNanoRange));
		swtToNanos.setText(Long.toString(maxNanoRange));
		swtFromNanosHuman.setText(NanoTime.toUTCString(minNanoRange));
		swtToNanosHuman.setText(NanoTime.toUTCString(maxNanoRange));

		notesPanel.programChangedEvent(minNanoRange, maxNanoRange);
		settingsPanel.programChangedEvent(minNanoRange, maxNanoRange);
		
	}
	
		
	public void dataChangingEvent(){
		if(dataChangeInhibit)
			return;
		
		try{
			dataChangeInhibit = true;
			
			setTextColour();
			
		}finally {
			dataChangeInhibit = false;
		}
	}
	
		
	public void setTextColour(){
		Color textCol;
		if(swtPathCombo.getText().equals(pipe.getPath()) && 
				Algorithms.mustParseLong(swtFromNanos.getText()) == pipe.getFromNanos() &&
				Algorithms.mustParseLong(swtToNanos.getText()) == pipe.getToNanos()){
			
			if(pipe instanceof W7XArchiveDBSource){
				textCol = swtGroup.getDisplay().getSystemColor(((W7XArchiveDBSource)pipe).isLoading() ? SWT.COLOR_RED : SWT.COLOR_BLACK);
				
			}else{
				textCol = swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLACK);
			}
			
		}else{
			textCol = swtGroup.getDisplay().getSystemColor(SWT.COLOR_BLUE);
		}
		
		swtPathCombo.setForeground(textCol);
		swtFromNanos.setForeground(textCol);
		swtToNanos.setForeground(textCol);
		
	}
			
	public void loadButtonEvent(Event event){
		checkPathPrefix();
		((W7XArchiveDBSource)pipe).loadTarget(
				swtPathCombo.getText().trim(),
				Algorithms.mustParseLong(swtFromNanos.getText()),
				Algorithms.mustParseLong(swtToNanos.getText()));
		
	}
	
	public void uploadButtonEvent(Event event){
		checkPathPrefix();
		((W7XArchiveDBSource)pipe).uploadTarget(
				swtPathCombo.getText().trim(),
				Algorithms.mustParseLong(swtFromNanos.getText()),
				Algorithms.mustParseLong(swtToNanos.getText()));
		
	}
	
	
	public void refreshProgramsButtonEvent(Event event){
		pipe.generateProgramList(swtPathCombo.getText().trim(), swtDateTime.getYear(), swtDateTime.getMonth()+1, swtDateTime.getDay(), true);
		
		dateChangedEvent(); //immediately update
	}	
	
	public void genProgDataListButtonEvent(Event event){
		checkPathPrefix();
		
		pipe.generateProgramDataList(swtPathCombo.getText().trim(), swtDateTime.getYear(), swtDateTime.getMonth()+1, swtDateTime.getDay());
	}
	
	public void genProgDataCountsButtonEvent(Event event){
		checkPathPrefix();
		
		pipe.generateProgramDataCounts(swtPathCombo.getText().trim(), swtDateTime.getYear(), swtDateTime.getMonth()+1);
	}
	

	public W7XArchiveDBPipe getSource() { return pipe; };

	@Override
	public Object getInterfacingObject() { return swtGroup; }

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}
	
	
	void checkGraffiti() {
		//remove all existing
		for(Composite comp : swtGraffitiComposites){			
			comp.dispose();
		}
		swtGraffitiComposites.clear();
		
		
		//fiugre out how the weeks and days in the calendar will line up with rows and cols
		//the table is always 6x7 = 42 days. Somewhere in the first row is day 1
		
		Calendar c = Calendar.getInstance();
		int year = swtDateTime.getYear();
		int month = swtDateTime.getMonth();
		c.set(year, month, 1);
		
		// offset of first day of month into the first week/row (1=sun, 2=mon ... 7=sat)
		int firstDayOfMonth = c.get(Calendar.DAY_OF_WEEK);
		
		int dayOffset = Algorithms.mustParseInt(SettingsManager.defaultGlobal().getProperty("imageProc.w7x.archiveDB.swt.calendar.dayOffset","1"));
				
		//the GTK calendar starts with sunday, convert to that, 0-based (0=sun, 1=mon, 2=tue ... 6=sat)
		firstDayOfMonth = (firstDayOfMonth - dayOffset + 7) % 7;
		
		//the GTK calenday displays a whole row of the last month when the first day is exactly monday
		if(firstDayOfMonth == 0)
			firstDayOfMonth += 7;
		
		Point p = swtDateTime.getSize();
		
		//see what data there is for this datastream
		int progCounts[] = pipe.getProgramCountPerDayOfMonth();
		if(progCounts != null && progCounts.length > 2 && progCounts[0] == year && progCounts[1] == (month+1)) {
			
			for(int day=0; day < (progCounts.length-2); day++){
				int col;			
				if(progCounts != null && progCounts.length > (day+2) && progCounts[2+day] > 0){ //there are programs
					col = SWT.COLOR_BLUE;
				}else {
					 col = SWT.COLOR_GRAY;
				}
							
				int boxNum = day + firstDayOfMonth;
				int yDay = boxNum / 7;
				int xDay = boxNum - yDay*7;
				
				int x = (int)((0.2 + xDay) * p.x / 7.2);
				//int y = (int)((2.8 + yDay) * p.y / 9); //linux gtk
				//int y = (int)((3.5 + yDay) * p.y / 9.5); //windows
				int y = (int)((3.15 + yDay) * p.y / 9.25); //average
				
				//System.out.println(day +" " + xDay + " " + yDay + " " + x + " " + y + " " + p.x + " " + p.y);
				
				Composite comp = new Composite(swtDateTime, SWT.NONE);
				comp.setLocation(x, y);
				comp.setSize(p.x / 35, p.y / 25);
				comp.setBackground(swtDateTime.getDisplay().getSystemColor(col));
				swtGraffitiComposites.add(comp);
			}
		}
		

		//see what data there is for this datastream
		int dataCounts[] = pipe.getDataCountPerDayOfMonth();
		if(dataCounts != null && dataCounts.length > 2 && dataCounts[0] == year && dataCounts[1] == (month+1)) {
			
			for(int day=0; day < (dataCounts.length-2); day++){
				int col;			
				if(dataCounts != null && dataCounts.length > (day+2) && dataCounts[2+day] > 0){ //there are programs
					col = SWT.COLOR_GREEN;
				}else {
					 col = SWT.COLOR_DARK_GRAY;
				}
							
				int boxNum = day + firstDayOfMonth;
				int yDay = boxNum / 7;
				int xDay = boxNum - yDay*7;
				
				int x = (int)((0.2 + xDay) * p.x / 7.2);
				//int y = (int)((2.8 + yDay) * p.y / 9); //linux gtk
				//int y = (int)((3.5 + yDay) * p.y / 9.5); //windows
				int y = (int)((3.55 + yDay) * p.y / 9.25); //average
				
				//System.out.println(day +" " + xDay + " " + yDay + " " + x + " " + y + " " + p.x + " " + p.y);
				
				Composite comp = new Composite(swtDateTime, SWT.NONE);
				comp.setLocation(x, y);
				comp.setSize(p.x / 35, p.y / 25);
				comp.setBackground(swtDateTime.getDisplay().getSystemColor(col));
				swtGraffitiComposites.add(comp);
			}
		}
			
		
	}
	
	private void doUpdate(){
		checkPathPrefix();
		swtStatusLabel.setText(pipe.getStatus());
		swtLastLoadedLabel.setText(pipe.getPath() + "\n/" + 
								pipe.getFromNanos() + "_" + pipe.getToNanos() + ":\n ");
		
		Date d = pipe.getADBFetcher().getCacheExpiry();
		
		W7XArchiveDBSourceConfig config = pipe.getConfig();
		
		//only update list box when its really changed, since it loses selections and scrolls
		List<Program> newProgList = pipe.getProgramDataList();
		
		if(newProgList == null){
			//no list, just a string
			swtProgramList.setItems(new String[] { pipe.getProgramListStatus() });
			programListHashCode = -1;
		}else{
			int newHash = newProgList.hashCode();
			if(newProgList.size() == 0){
				swtProgramList.setItems(new String[] { " -- No programs or data -- " });
				
			}else if(newHash != programListHashCode){
				//if it's changed, repopulate the list
				programList = null;
				swtProgramList.removeAll();
				for(Program prog : newProgList){
					String progText = prog.id + ": " + prog.name + "  [" + prog.nImageInCache + " / " + prog.nImagesInArchive + "]";
					swtProgramList.add(progText);
				}
				programList = newProgList;
				programListHashCode = newHash;
				
				if(jumpToProgramOnListLoad >= 0){
					selectProgram(jumpToProgramOnListLoad);
					jumpToProgramOnListLoad = -1;
				}
			}
		}
		
		if(!swtPathCombo.isFocusControl()) {
			String current = swtPathCombo.getText().trim();
			
			String[] paths;
			if(selectedLPComponent != null && selectedLPComponent.datastreamPaths != null) {
				paths = selectedLPComponent.datastreamPaths;
			}else {
				paths = pipe.getPathHistory();
			}
			
			swtPathCombo.setItems(paths);
			int i;
			for(i=0; i < paths.length; i++) {
				if(current.equals(paths[i])) {
					swtPathCombo.select(i);
					break;
				}
			}
			if(i >= paths.length) {
				swtPathCombo.setText(current); //can keep this, it is set when 'load' is clicked
			}
		}
		
		checkGraffiti();
		setTextColour();
		
		settingsPanel.doUpdate();
		notesPanel.doUpdate();
		
	}
	
	@Override
	public void destroy() {
		swtGroup.dispose();		
		pipe.controllerDestroyed(this);
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return pipe; }
	
	/** Check that the path has the 'w7x/' bit for DataSignals on the front */
	private void checkPathPrefix(){
		String path = swtPathCombo.getText();
		path = path.replaceAll("^/*", ""); //strip leading slashes 
		if(!path.trim().startsWith("w7x/"))
			swtPathCombo.setText("w7x/" + path.trim());
	}

	public void setPath(String path) {
		swtPathCombo.setText(path);		
	}
	
	
}
