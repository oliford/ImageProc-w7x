package imageProc.database.w7xArchiveDB.swt;

import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImageProcUtil;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSourceConfig;

public class SettingsPanel {
	private W7XArchiveDBSource src;
	W7XArchiveDBSourceSWTControl srcCtrl;

	private Group swtGroup;
	
	private Button enableCacheWriteCheckbox;
	private Button invalidateCacheCheckbox;
	private Spinner groupImagesTimeSpinner;
	private Spinner preProgramToleranceTimeSpinner;
	private Spinner postProgramToleranceTimeSpinner;
	private Button swtEnableMemCacheCheckbox;	
	private Spinner maxImagesSpinner;
	private Label parlogLabel;
	private Spinner parlogSpinner;
	
	private boolean dataChangeInhibit = false;
	
	private SWTSettingsControl settingsCtrl;
	
	public SettingsPanel(W7XArchiveDBSourceSWTControl srcCtrl, W7XArchiveDBSource src, Composite parent, int style) {
		this.srcCtrl = srcCtrl;
		this.src = src;
		
		W7XArchiveDBSourceConfig config = src.getConfig();
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setLayout(new GridLayout(6, false));

		Label lSM = new Label(swtGroup, SWT.NONE); lSM.setText("Max images:");
		maxImagesSpinner = new Spinner(swtGroup, SWT.NONE);
		maxImagesSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 10, 100);
		maxImagesSpinner.setToolTipText("Only load this many images from the selected time range. (Useful if there's not enough memory).");
		maxImagesSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { maxImagesSpinnerEvent(event); } });
		maxImagesSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		parlogLabel = new Label(swtGroup, SWT.NONE); parlogLabel.setText("Parlog (last load had ??):");
		parlogSpinner = new Spinner(swtGroup, SWT.NONE);
		parlogSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		parlogSpinner.setToolTipText("From which parlog should metadata be loaded if multiple parlogs are available within the selected data time interval (number will be displated to the left in brackets after first load attempt). -1 means to load the last one.");
		parlogSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(); } });
		parlogSpinner.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		Label lC = new Label(swtGroup, 0); lC.setText("Cache:");		
		swtEnableMemCacheCheckbox = new Button(swtGroup, SWT.CHECK);
		swtEnableMemCacheCheckbox.setText("Enable Mem Cache");	
		swtEnableMemCacheCheckbox.setToolTipText("Enable caching of information and images in DataSignals memory cache (java heap). Generally a bad idea!");
		swtEnableMemCacheCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { enableCacheButtonEvent(event); } });
		swtEnableMemCacheCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		enableCacheWriteCheckbox = new Button(swtGroup, SWT.CHECK);	
		enableCacheWriteCheckbox.setText("Write to cache");
		enableCacheWriteCheckbox.setToolTipText("Enable writing to cache of data downloaded from archive.");
		enableCacheWriteCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		enableCacheWriteCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(); } });
		
		invalidateCacheCheckbox = new Button(swtGroup, SWT.CHECK);	
		invalidateCacheCheckbox.setText("Cache invalid before:\nNever");
		invalidateCacheCheckbox.setToolTipText("Invalidate all data in the cache older than when this button is pressed. All future information will need to be requested from the archiveDB at least once.");
		invalidateCacheCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		invalidateCacheCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { invalidateCacheButtonEvent(event); } });

		
		Label lST = new Label(swtGroup, 0); lST.setText("OOP Split time:");
		groupImagesTimeSpinner = new Spinner(swtGroup, SWT.NONE);
		groupImagesTimeSpinner.setValues((int)(config.splitOutOfProgramPauseNS / 1_000_000_000L), 1, Integer.MAX_VALUE, 0, 10, 60);
		groupImagesTimeSpinner.setToolTipText("Sets the time within which multiple images outside a W7-X program will be grouped into one out-of-program 'shot'");
		groupImagesTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		groupImagesTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(); } });
		
		Label lBP = new Label(swtGroup, 0); lBP.setText("Shot tolerance: Pre:");
		preProgramToleranceTimeSpinner = new Spinner(swtGroup, SWT.NONE);
		preProgramToleranceTimeSpinner.setValues((int)(config.beforeProgramToleranceNS / 1_000_000_000L), 1, Integer.MAX_VALUE, 0, 10, 60);
		preProgramToleranceTimeSpinner.setToolTipText("Sets the time within which images before a shot are included in that shot.");
		preProgramToleranceTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		preProgramToleranceTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(); } });
		
		Label lAP = new Label(swtGroup, 0); lAP.setText("Post:");
		postProgramToleranceTimeSpinner = new Spinner(swtGroup, SWT.NONE);
		postProgramToleranceTimeSpinner.setValues((int)(config.afterProgramToleranceNS / 1_000_000_000L), 1, Integer.MAX_VALUE, 0, 10, 60);
		postProgramToleranceTimeSpinner.setToolTipText("Sets the time within which images after a shot are included in that shot.");
		postProgramToleranceTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		postProgramToleranceTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(); } });
				
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, src);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 6, 1));
		
		ImageProcUtil.addRevealWriggler(swtGroup);
        doUpdate();
	}
	
	public void settingsChangedEvent(){
		if(dataChangeInhibit)
			return;

		try {
			dataChangeInhibit = true;
			
			W7XArchiveDBSourceConfig config = src.getConfig();
			config.splitOutOfProgramPauseNS = groupImagesTimeSpinner.getSelection() * 1_000_000_000L;
			config.beforeProgramToleranceNS = preProgramToleranceTimeSpinner.getSelection() * 1_000_000_000L;
			config.afterProgramToleranceNS = postProgramToleranceTimeSpinner.getSelection() * 1_000_000_000L;
			config.selectParlog = parlogSpinner.getSelection(); 
			
			src.getADBFetcher().setWriteInhibit(!enableCacheWriteCheckbox.getSelection());

		}finally {
			dataChangeInhibit = false;
		}
	}
	
	public Group getSWTGroup() { return swtGroup; }
	
	public void invalidateCacheButtonEvent(Event event){
		src.invalidateCache(invalidateCacheCheckbox.getSelection());
		settingsChangedEvent(); //immediately update
	}
	
	public void enableCacheButtonEvent(Event event){
		src.setCacheEnable(swtEnableMemCacheCheckbox.getSelection());		
	}
	
	public void maxImagesSpinnerEvent(Event event){
		src.setMaxImages(maxImagesSpinner.getSelection());		
	}
		
	void doUpdate(){
		if(dataChangeInhibit)
			return;
		
		try {
			dataChangeInhibit = true;
			
			
			Date d = src.getADBFetcher().getCacheExpiry();
			invalidateCacheCheckbox.setText("Cache invalid before:\n" + (d == null ? "Never" : d.toString()));
			enableCacheWriteCheckbox.setSelection(!src.getADBFetcher().isWriteInhibited());
			parlogLabel.setText("Parlog (last load had " + src.getNumberOfParlogsInRange() + "):");
			
			W7XArchiveDBSourceConfig config = src.getConfig();
			
			groupImagesTimeSpinner.setSelection((int)(config.splitOutOfProgramPauseNS / 1_000_000_000L));
			preProgramToleranceTimeSpinner.setSelection((int)(config.beforeProgramToleranceNS / 1_000_000_000L));
			postProgramToleranceTimeSpinner.setSelection((int)(config.afterProgramToleranceNS / 1_000_000_000L));
			
		}finally {
			dataChangeInhibit = false;
		}
	}

	void programChangedEvent(long minNanoRange, long maxNanoRanges) {
		
	}	
}