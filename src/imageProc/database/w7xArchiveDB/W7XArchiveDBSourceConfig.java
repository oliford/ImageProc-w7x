package imageProc.database.w7xArchiveDB;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

public class W7XArchiveDBSourceConfig {
	
	public String path;
	
	/** If image data from archive is 3D, interpret the outer dimension as multiple images
	 * stored in the same box, as can be done from e.g. the codastation streaming.
	 * Otherwise the outer dimension is interpreted as multiple fields of each image 
	 * as e.g. the FFT outputs the complex components as different fields  */
	public boolean separateBoxedImages = true;
	
	public long splitOutOfProgramPauseNS = 30_000_000_000L; // 30 secs
	
	public long afterProgramToleranceNS = 30_000_000_000L; // 30 secs
	
	public long beforeProgramToleranceNS = 30_000_000_000L; // 30 secs

	/** From which parlog should metadata be loaded if multiple parlogs are available within 
	 * the selected data time interval (they will be displated listed here). < 0 means to load 
	 * the last one.");	 */
	public int selectParlog = -1;

}
