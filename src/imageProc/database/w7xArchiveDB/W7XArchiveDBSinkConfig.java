package imageProc.database.w7xArchiveDB;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

public class W7XArchiveDBSinkConfig {
	public static enum TimebaseMode {
		AUTOSELECT, 
		IMGNUM, 
		SPECIFIED;
	}
	
	public String path;
	
	/** -1 =unversioned, 0=latest version */
	public int version = -1;
	
	
	public TimebaseMode timebaseMode = TimebaseMode.AUTOSELECT;
	
	/** Currently selected timebase metadata */
	public String timebaseMetadataName = "";
	
	/** Attempt to save images marked as invalid (no ranges) */ 
	public boolean saveInvalid = false;
	
	/** Report invalid or null images as a failure to write (even if we don't attempt it) 
	 * Currently only applies to writing to cache */
	public boolean errorOnInvalid = false;

	public boolean overwriteInDB = false;
	public boolean writeToCache = true;
	public boolean writeToDB = false;
		
	public int imagesPerUpload = 10;

	/** Alias path to update to point to this stream for the image range being saved. The alias is written to the database
	 * regardless of whether the images are written to the cache or the database */
	public String aliasPath = "w7x/Sandbox/views/KKS/QS-_TestSpectroscopy/Raw_Camera1";
	
	/** Update the given alias when saving the images. */
	public boolean updateAlias = false;

	/** When writing an alias for an acquisition in a W7X program, write the alias
	 * to cover the whole program, otherwise it only covers the range of 
	 * the first/last written images. */	
	public boolean writeAliasForWholeProgram = true;
}
