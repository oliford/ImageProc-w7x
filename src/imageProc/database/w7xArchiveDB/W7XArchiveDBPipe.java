package imageProc.database.w7xArchiveDB;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

import org.eclipse.swt.widgets.Composite;

import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBDesc.Type;
import descriptors.w7x.ArchiveDBJSONInfoDesc;
import descriptors.w7x.ArchiveDBTreeListDesc;
import descriptors.w7x.CommentDesc;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.database.w7xArchiveDB.swt.W7XArchiveDBSinkSWTControl;
import imageProc.database.w7xArchiveDB.swt.W7XArchiveDBSourceSWTControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.w7x.ArchiveDB.Database;
import signals.w7x.ArchiveDBJSONObject;
import w7x.archive.ArchiveDBFetcher;
import w7x.archive.writing.ParameterLog;
import w7x.logbook.writing.LogbookWriter;

/**  W7X Archive DB source/sink base code */
public abstract class W7XArchiveDBPipe extends ImgSourceOrSinkImpl implements EventReciever  {
	
	protected ArchiveDBFetcher adb;
	
	protected long fromNanos, toNanos;
	
	protected int maxImages = -1;
	
	protected boolean abortCalc = false;
	
	protected String status;
		
	public W7XArchiveDBPipe() {
		this(ArchiveDBFetcher.defaultInstance());
	}

	public W7XArchiveDBPipe(ArchiveDBFetcher adb) {
		this.adb = adb;
		//we do want to cache all the info requests
		adb.setEnableMemoryCache(true); 
		adb.enableMutableCache(true);
		status = "init\n.\n.\n.";
	}	
		
	public static String getDefaultPath(){
		String path = SettingsManager.defaultGlobal().getProperty("imageProc.w7x.archiveDB.path", "w7x/ArchiveDB/raw/W7X/QSK_CXRS/ILS_Red_Binned_DATASTREAM/0/Images");
		if(!path.matches("/*w7x.*"))
			path = "w7x/" + path;
		return path;
	}
	
	/** List of previously used paths from minerva-settings */ 
	public String[] getPathHistory() {
		ArrayList<String> list = new ArrayList<String>();
		
		String userDir = OneLiners.isWindows() ? System.getenv("USERPROFILE") : System.getenv("HOME");
		String historyFile = SettingsManager.defaultGlobal().getProperty("imageProc.w7x.archiveDB.streamsListFile", userDir + "/.imageproc/archiveDBStreams.txt");
		OneLiners.makePath(historyFile);
		
		if((new File(historyFile)).canRead()){
			String fromFile[] = OneLiners.linesFromFile(historyFile);
			for(String path : fromFile){
				if(path.length() == 0)
					continue;
				
				//check that it actually passes as a datasignals path
				try{
					ArchiveDBDesc desc = new ArchiveDBDesc(path);
				}catch(RuntimeException err){
					Logger.getLogger(this.getClass().getName())
								.finest("WARNING: W7X archiveDB path history entry failed to parse: " 
									+ path + " (" + err.getMessage() + ").");
					continue;
				}
				if(!list.contains(path)) //ignore duplicates
					list.add(path);
			}
		}
		
		if(list.size() == 0) {
			int j=0;
			for(String newPath : new String[] {
					"w7x/ArchiveDB/raw/W7X/QSK_CXRS/ILS_Red_Binned_DATASTREAM/0/Images",
					"w7x/ArchiveDB/raw/W7X/QSK_CXRS/ILS_Green_Binned_DATASTREAM/0/Images",
					"w7x/ArchiveDB/raw/W7X/QSK_CXRS/ILS_AUG1_Binned_DATASTREAM/0/Images",
					"w7x/ArchiveDB/raw/W7X/QSK_CXRS/ILS_AUG2_Binned_DATASTREAM/0/Images",
					"w7x/ArchiveDB/raw/W7X/QSK06_PassiveSpectroscopy/PCOSpexM750_DATASTREAM/0/Images",
					"w7x/ArchiveDB/raw/W7X/QSK06_PassiveSpectroscopy/USB_HR4000_DATASTREAM/0/Images",
					"w7x/ArchiveDB/raw/W7X/QSK06_PassiveSpectroscopy/USB_Avaspec_DATASTREAM/0/Images",
					"w7x/Test/raw/W7X/QSZ_Zeff/PI_CCD_07_1-QSS60OC094_DATASTREAM/",
					"w7x/Test/raw/W7X/QSS_DivertorSpectroscopy/USB-Spectrometer_RedTide650_1-QSS60OC042_DATASTREAM/",					
				}) {
				
				list.add(newPath);
			}
		}
		
		Collections.sort(list);
		
		try{
			OneLiners.textToFile(historyFile, String.join("\n", list.toArray(new String[0])));
		}catch(RuntimeException err){ }
				
		return list.toArray(new String[list.size()]);
	}
	
	protected void addPathToHistory(String path){
		
		for(String oldPath : getPathHistory()){
			if(path.equals(oldPath))
				return;
		}
		
		String userDir = OneLiners.isWindows() ? System.getenv("USERPROFILE") : System.getenv("HOME");
		String historyFile = SettingsManager.defaultGlobal().getPathProperty("imageProc.w7x.archiveDB.streamsListFile", userDir + "/.imageproc/archiveDBStreams.txt");
		
		try (FileOutputStream fOut = new FileOutputStream(historyFile, true)) {
			fOut.write('\n');
			fOut.write(path.getBytes());
		} catch (IOException err) {
			throw new RuntimeException(err);
		}
	}
	
	public static String pathFixup(String path){
		if(!path.replaceAll("^[/\\s]*", "").startsWith("w7x/"))
			path = "w7x/" + path.trim();
		
		return path;
	}
	
	public long getFromNanos(){ return fromNanos; }
	public long getToNanos(){ return toNanos; } 
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){
			if(asSink)
				controller = new W7XArchiveDBSinkSWTControl((Composite)args[0], (Integer)args[1], (W7XArchiveDBSink)this);
			else
				controller = new W7XArchiveDBSourceSWTControl((Composite)args[0], (Integer)args[1], (W7XArchiveDBSource)this);
			controllers.add(controller);			
		}
		return controller;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + hashCode() + "]: " + getPath() +"]";
	}
	
	public abstract String getPath();
	
	public final static String clearCacheUpdateObj = "clearCacheUpdateObj"; 
	public void setCacheEnable(boolean enable) {
		adb.setUseMemoryCache(enable);
		if(!enable){
			adb.clearMemoryCache();
			System.gc(); //once here
			ImageProcUtil.ensureFinalUpdate(clearCacheUpdateObj, new Runnable() {
				@Override
				public void run() {
					System.gc(); //once again
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) { }
					System.gc(); //god dammit, do it already!
				}
			});
		}
	}
		
		
	public boolean isIdle(){ 
		return !ImageProcUtil.isUpdateInProgress(this);
	}

	public void setMaxImages(int maxImages) {
		this.maxImages = maxImages;
	}

	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalStart:
			break;
		case GlobalAbort:
			abortCalc = true;		
		default:
		}
	}
	
	public int getNumIntervalsInRange(String path, long fromNanos, long toNanos, boolean cache) {
		long intervals[][] = getIntervalsInRange(path, fromNanos, toNanos, cache);
		return (intervals == null || intervals.length ==0 || intervals[0].length==0) 
				? 0 : intervals[0].length;
	}
	
	public long[][] getIntervalsInRange(String path, long fromNanos, long toNanos, boolean cache){		
		try{			
			ArchiveDBDesc desc = new ArchiveDBDesc(path, fromNanos, toNanos);
			if(desc.isAlias()) {
				//stupidity
				ArchiveDBDesc desc2 = adb.resolveAlias(desc);
				if(desc2.getChannelName() == null) {
					desc2.setChannelNumber(desc.getChannelNumber());
					desc2.setChannelName(desc.getChannelName());
					desc=desc2;				
				}
				
				if(!desc.isAlias() && desc.getChannelName() == null) {		
					//user didn't specify a channel, get single channel if there's only 1					
					ArchiveDBTreeListDesc[]  leafDescs = adb.getSubtreeEntries(desc);
					
					if(leafDescs.length != 1) 
						throw new RuntimeException("No channel name in path, and datastream has "+leafDescs.length+" tree entries.");
					desc = new ArchiveDBDesc(leafDescs[0]);
					desc.setFromNanos(fromNanos);
					desc.setToNanos(toNanos);
				}
			}
			
			adb.setMaxCachedSignals(100000);
			long intervals[][] = cache ? adb.getIntervalsInCache(desc) : adb.getIntervals(desc);			
			return intervals;
		}catch(RuntimeException err){
			err.printStackTrace();
			return null;
		}
	}

	public String[] getDatabaseNames() {
		String names[] = new String[Database.values().length];
		int i = 0;
		for(Database d : Database.values()){
			names[i++] = d.name();
		}
		return names;
	}
	
	public void invalidateCache(boolean invalidate) {
		Date expiry = invalidate ? new Date() : null;
		
		//set the expiry to now, so everything else gets reloaded
		adb.setCacheSettings(adb.isCachingToMemory(),
				adb.isCachingToDisk(), 
				adb.isCachingMutables(), 
				expiry, 
				true);
	
		updateAllControllers();
	}
	
	public void setStatus(String status) {
		this.status = status;
		updateAllControllers();
	}
	
	public String getStatus() {
		return status;
	}
	
	public HashMap<String, String> loadedNotes = new HashMap<String, String>();

	protected String notesFetchSyncObj = new String("notesFetchSyncObj");
	protected ConcurrentLinkedQueue<ArchiveDBDesc> notesToFetch = new ConcurrentLinkedQueue<ArchiveDBDesc>();

	public void clearNotesCache() {
		loadedNotes.clear();
	}

	public String getNotes(String path, long nanoTimeMin, long nanoTimeMax) {
		if(nanoTimeMin <= 0)
			return " -- Invalid from nanotime -- ";
		
		ArchiveDBDesc desc = new ArchiveDBDesc(path, nanoTimeMin, nanoTimeMax);

		String notes = loadedNotes.get(desc.toString());
		if (notes != null) {
			return notes;
		}

		final String busyStr = "[ Fetching... ]";
		loadedNotes.put(desc.toString(), busyStr);

		notesToFetch.add(desc);

		ImageProcUtil.ensureFinalUpdate(notesFetchSyncObj, new Runnable() {
			@Override
			public void run() {
				fetchNotes();
			}
		});

		return busyStr;
	}

	private void fetchNotes() {
		ArchiveDBDesc desc;
		while ((desc = notesToFetch.poll()) != null) {
			try {
				String notes = loadNotes(desc);

				loadedNotes.put(desc.toString(), notes);

			} catch (Exception err) {
				loadedNotes.put(desc.toString(), err.toString());
			}
			updateAllControllers();

		}
	}

	
	public String loadNotes(String path, long nanoTime) {
		ArchiveDBDesc desc = new ArchiveDBDesc(path, nanoTime, nanoTime);
		return loadNotes(desc);
	}
	
	public String loadNotes(String path, long fromNanos, long toNanos) {
		ArchiveDBDesc desc = new ArchiveDBDesc(path, fromNanos, toNanos);
		return loadNotes(desc);
	}
	
	public String loadNotes(ArchiveDBDesc dataDesc) {
		ArchiveDBDesc desc = new ArchiveDBDesc(dataDesc);
		
		String signalName = desc.getSignalName();		
		desc.setSignalName(signalName + "_Notes");
		desc.setType(Type.PARLOG);
		desc.setChannelName(null);
		desc.setChannelNumber(-1);
		//desc.setVersion(00);
		
		System.out.println(desc.getWebAPIRequestURI());
		
		System.out.println("Looking for notes at '" + desc + "'");
		
		try{
			adb.enableMutableCache(false);
			int latest = adb.getHighestVersionNumber(desc);
			adb.enableMutableCache(true);
			desc.setVersion(latest);
		
			Object o = adb.getParLogEntry(desc, "parms/notes", String.class);		
			if(o != null){
				return (String)o;
			}
		}catch(RuntimeException err){
			logr.warning("No ParLog entry " + desc + ": " + err.getMessage());
		}

		try{
				
			//ok, try notes entry in parlog
			desc.setSignalName(signalName);			
			desc.setType(Type.PARLOG);
			
			System.out.println("Looking for embedded notes in-metadata '" + desc + "'");
			Object o = adb.getParLogEntry(desc, "parms/notes", String.class);
			if(o != null) {
				//and write back to proper place
				saveNotes(signalName, fromNanos, toNanos, (String)o);
				return (String)o;
			}

		}catch(RuntimeException err){
			logr.warning("No ParLog entry " + desc + ": " + err.getMessage());
		}

		try{
				
			//ok, try notes entry in old xxx_MetaData
			desc.setSignalName(signalName + "_MetaData");			
			desc.setType(Type.PARLOG);
			
			System.out.println("Looking for old-style in-metadata notes at '" + desc + "'");
			Object o = adb.getParLogEntry(desc, "parms/notes", String.class);
			if(o != null) {
				//and write back to proper place
				saveNotes(signalName, fromNanos, toNanos, (String)o);
				return (String)o;
			}
			
		}catch(RuntimeException err){
			System.err.println("No ParLog entry " + desc + ": " + err.getMessage());
		}

		return "-- No notes found -- ";//for " + desc + " --";		
	}
	
	public void saveNotes(String path, long fromNanos, long toNanos, String notes) {
		
		//the versioning currently doesn't work in the cache
		//so we need to use the actual archive for this
		
		ArchiveDBDesc desc = new ArchiveDBDesc(path, fromNanos, toNanos);
		String signalName = desc.getSignalName();		
		desc.setSignalName(signalName + "_Notes");
		desc.setType(Type.PARLOG);
		desc.setChannelName(null);
		desc.setChannelNumber(-1);
		desc.setVersion(0);
		
		ArchiveDBJSONInfoDesc parLogDesc = new ArchiveDBJSONInfoDesc(desc);
		
		//find the latest version (definitely remotely)		
		boolean en = adb.isCachingMutables();
		adb.enableMutableCache(false);
		int ver2 = adb.getHighestVersionNumber(parLogDesc);
		//int ver2 = adb.getHighestVersionNumberForAllTime(parLogDesc);
					
		//write the next version (to cache as well)
		parLogDesc.setVersion(ver2 + 1);					
					
		//ParameterLog2 parLog = new ParameterLog2(parLogDesc, "Notes", getClass().getCanonicalName());
		ParameterLog parLog = new ParameterLog(parLogDesc.getDatabase(), 
				parLogDesc.getSection().toString() + "/" +
						parLogDesc.getProject() +  "/"+ 
						parLogDesc.getSignalGroup() + "/"+ 
						parLogDesc.getSignalName(),
						ver2 + 1, 
				fromNanos, toNanos, "notes", getClass().getCanonicalName());
		
		///how is this not the same thing????
		
		parLog.addParameter("notes", notes);
		
		String jsonString = parLog.toJson().toString();
		ArchiveDBJSONObject jsonObj = new ArchiveDBJSONObject(parLogDesc, jsonString);
		
		ArchiveDBFetcher.defaultInstance().writeToCache(jsonObj);
		
		System.out.println("Saving notes at '" + parLogDesc + "'");
		parLogDesc.setVersion(0);
		parLog.send(false);
		
		int newVersion = adb.getHighestVersionNumber(parLogDesc);
		
		//make sure new version is pulled to the cache
		parLogDesc.setVersion(newVersion);
		adb.getParlogJson(parLogDesc);
		
		adb.enableMutableCache(en);
		
		ImageProcUtil.ensureFinalUpdate(saveLogbookNotesSyncObj, () -> saveLogbookNotes(desc, fromNanos, toNanos, notes));
	}
	
	private void saveLogbookNotes(ArchiveDBDesc desc, long fromNanos, long toNanos, String notes) {
		var cdesc = new CommentDesc();
		var middle = fromNanos + (toNanos-fromNanos)/2;
		var fetcher = new ArchiveDBFetcher().quiet();
		fetcher.setEnableDiskCache(false);
		var fullId = fetcher.getFullId(middle);
		var probablyKks = desc.getSignalGroup().split("_")[0];

		cdesc.setComponentId(probablyKks);
		String user;
		try {
			user = SettingsManager.defaultGlobal().getProperty("w7x.logbook.user");
		} catch (RuntimeException e) {
			System.err.println("Probably missing 'w7x.logbook.user' in minerva-settings");
			e.printStackTrace();
			return;
		}
		cdesc.setUser(user);
		cdesc.setRefId(LogbookWriter.fullIdToXpId(fullId));
		cdesc.setFrom(fromNanos);
		cdesc.setTo(toNanos);
		cdesc.setContent(NOTES_LOGBOOK_IDENTIFIER+notes);
		try {
			cdesc.create();
		} catch (RuntimeException e) {
			System.err.println("Maybe 'w7x.logbook.user' and 'w7x.logbook.token' don't match (in minerva-settings)?");
			e.printStackTrace();
			return;
		}
	}

	public static final String NOTES_LOGBOOK_IDENTIFIER = "<!-- THIS COMMENT IS MANAGED BY IMAGEPROC_W7X -->";
	public String saveLogbookNotesSyncObj = "saveLogbookNotesSyncObj";
}
