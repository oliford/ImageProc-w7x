package imageProc.database.w7xArchiveDB;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import descriptors.w7x.ArchiveDBDesc;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.w7x.ArchiveDB;
import signals.w7x.ArchiveDBJSONObject;
import w7x.archive.ArchiveDBFetcher;
import w7x.archive.writing.W7xArchiveWrite;
import w7x.archive.writing.WebApiHdf5Writer;

public class Uploader {
	private static String failedPath = "w7xWriteBackFailed"; //replaces the first '/w7x/' in cache path
	private ArchiveDBFetcher adb;
	
	private boolean moveFailed = false;
	
	public Uploader(ArchiveDBFetcher adb, boolean moveFailed) {
		this.adb = adb;
		this.moveFailed = moveFailed;
	}
	
	public boolean upload(ArchiveDBDesc desc){
		return upload(desc, null);	
	}
	
	public boolean upload(ArchiveDBDesc desc, String fileName){
		File cacheFile;
		if(fileName != null) {
			cacheFile = new File(fileName);
		}else {
			cacheFile = findFile(desc);
		}
		if(cacheFile == null || !cacheFile.canRead()) {
			System.err.println("Unable to find or read cache file " + cacheFile + " for " + desc);
			return false;
		}
		//*/
		
		Object sig = null;
		try{
			sig = adb.getSig(desc);
		}catch(RuntimeException err){
			System.err.println("Unable to read entry found in cache '"+desc+"'. Moving to " + failedPath);
			if(moveFailed)
				moveToFailed(cacheFile, false);
			return false;
		}//*/
		
		if(sig instanceof ArchiveDBJSONObject){
				
			try{
				JsonObject jObj = ((ArchiveDBJSONObject) sig).getJsonObject();
				
				//get rid of various garbage things that we don't want in cache
				//and definitely don't want to try to upload
				Object jLabel = jObj.get("label");
				if(jLabel != null && jLabel instanceof JsonPrimitive 
						&& ((JsonPrimitive)jLabel).isString()
						&& ((JsonPrimitive)jLabel).getAsString().equals("EmptySignal")){
					
					System.out.println("Signal " + desc + " is a cached empty entity. Deleting.");

					if(moveFailed)
						moveToFailed(cacheFile, true);
					return false;
				}
				Object jMessage = jObj.get("message");
				if(jMessage != null && jMessage instanceof JsonPrimitive 
						&& ((JsonPrimitive)jMessage).isString()
						&& ((JsonPrimitive)jMessage).getAsString().startsWith("Requested version number")){
					
					System.out.println("Signal " + desc + " is a failed request for a non-existing version. Moving to w7xWritebackFailed.");

					if(moveFailed)
						moveToFailed(cacheFile, true);
					return false;
				}
				//this is the cache's way of saying to write to the latest
				//version, which we do by omitting the version spec in the partial path
				//which is described by v=0
				if(desc.getVersion() == -1) 
					desc.setVersion(0);
					
				W7xArchiveWrite.send(W7xArchiveWrite.getDefaultBaseUri(), 
							desc, 
							 ((ArchiveDBJSONObject) sig).getJsonObject());
				
				return true;
			}catch(RuntimeException err){
				if(moveFailed)
					moveToFailed(cacheFile, false);
				throw err;
			}
				
		}else if(sig instanceof ArchiveDB){
			try{
				//so that it knows we're writing a single time point, we need to wrap
				//in a 1 element array... sometimes!
				//sometimes its already done
				//maybe
				//oh ffs I hate weak specs
				
				Object o = ((ArchiveDB) sig).getData();
				if(Array.getLength(o) != 1 ||
						!Array.get(o, 0).getClass().isArray() ||
						!Array.get(Array.get(o, 0),0).getClass().isArray()){
					
					Object wrappedArray = Array.newInstance(o.getClass(), 1);
					Array.set(wrappedArray, 0, o);
					o = wrappedArray;
				}
							
				ArchiveDB sig2 = new ArchiveDB(desc, o, ((ArchiveDB) sig).getTNanos());
				WebApiHdf5Writer.write((ArchiveDB)sig2, null);
				
				return true;
				
			}catch(RuntimeException err){
				if(moveFailed) {
					try {
						moveToFailed(cacheFile, false);
					}catch(RuntimeException err2) {
						err2.printStackTrace();
					}
				}
				throw err;
			}
				
		}else{
			throw new RuntimeException(desc + ": UNKNOWN TYPE!");
		}
		
	}

	/** Find the filename for a DB entry */ 
	public File findFile(ArchiveDBDesc desc){
		
		String parts[] = desc.pathInCache().split("/");
		String path = adb.getCacheRoot();
		for(int i=0; i < (parts.length-1); i++){
			path += "/" + parts[i];
		}
		
		Path fsPath = FileSystems.getDefault().getPath(path);
		DirectoryStream<Path> dirStream;
		try {
			dirStream = Files.newDirectoryStream(fsPath);
		} catch (IOException e) {
			//maybe try without date on end??
			Pattern p = Pattern.compile("(.*)/[0-9][0-9][0-9][0-9]/[0-9][0-9]/[0-9][0-9]/?");
			Matcher m = p.matcher(path);
			if(!m.matches()){
				throw new RuntimeException(e);
			}
			path = m.group(1);
			fsPath = FileSystems.getDefault().getPath(path);
			try{
				dirStream = Files.newDirectoryStream(fsPath);
			} catch (IOException e2) {
				throw new RuntimeException(e);				
			}
		}
		
		String entryName = parts[parts.length-1];
		
		entryName = entryName.replaceAll("\\?allowNaN=true$", "");
		
		//some hackery of something datasignals does
		entryName = entryName.replaceAll("\\?", "_qm_");
		
		entryName = entryName.replaceAll("\\.json$", "");

		for (Path path2 : dirStream) {
			String fileName = path2.getFileName().toString();
			if(fileName.startsWith(entryName)){
		
				File file = new File(path + "/" + fileName);
				return file;
			}
		}
		return null;
	}
	
	public void moveToFailed(File cacheFile, boolean deleteIfSmall) {		
		
		if(cacheFile.length() == 0 || (deleteIfSmall && cacheFile.length() < 200)){ 
			System.err.println("Failed cache file with "+cacheFile.length()+" length, deleteing.");
			cacheFile.delete();			
			return;
		}
		
		String target = cacheFile.getAbsolutePath().replaceFirst("/w7x/", "/"+failedPath+"/");
		
		OneLiners.makePath(target);
		boolean moveOk = cacheFile.renameTo(new File(target));
		
		if(!moveOk){
			System.err.println("Unable to moved failed file '"+cacheFile+"' to " + target);
		}else{
			System.err.println("Moved failed file '"+cacheFile+"' to " + target);
		}
	}
}
