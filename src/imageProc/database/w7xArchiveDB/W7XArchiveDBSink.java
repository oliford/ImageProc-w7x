package imageProc.database.w7xArchiveDB;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;

import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBDesc.Type;
import descriptors.w7x.ArchiveDBJSONInfoDesc;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.DatabaseSink;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.database.geriStream.GeriStreamConfig;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSinkConfig.TimebaseMode;
import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.w7x.W7XUtil;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.w7x.ArchiveDB;
import signals.w7x.ArchiveDBJSONObject;
import w7x.archive.ArchiveDBFetcher;
import w7x.archive.writing.ParameterLog;
import w7x.archive.writing.W7xArchiveWrite;
import w7x.archive.writing.WebApiHdf5Writer;

/** Sink for saving to W7X Archive DB */
public class W7XArchiveDBSink extends W7XArchiveDBPipe implements ImgSink, DatabaseSink, ConfigurableByID {
	
	/** Non-null if any was wrong with the timestamps */
	protected String lastTimestampsWarning;
	
	private boolean lastSaveCompleted = false;

	private W7XArchiveDBSinkConfig config = new W7XArchiveDBSinkConfig();

	private String lastLoadedConfig;
	
	public static class InvalidImageException extends RuntimeException {
		public InvalidImageException(String message) {
			super(message);
		}
	}
	
	public W7XArchiveDBSink() {
		this(ArchiveDBFetcher.defaultInstance());		
	}

	public W7XArchiveDBSink(ArchiveDBFetcher adb) {
		super(adb);
		
		//pick up any old style settings in profile
		config.path = getDefaultPath();
		config.overwriteInDB = Algorithms.mustParseBoolean(SettingsManager.defaultGlobal().getProperty("imageProc.w7x.archiveDB.overwriteInDB", "false"));
		config.writeToCache = Algorithms.mustParseBoolean(SettingsManager.defaultGlobal().getProperty("imageProc.w7x.archiveDB.writeToCache", "false"));
		config.writeToDB = Algorithms.mustParseBoolean(SettingsManager.defaultGlobal().getProperty("imageProc.w7x.archiveDB.writeToDB", "false"));
	}
		
	/** Save the current image set */
	public void save(){
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doSequenceSaveCheck(); } }); 
	}
	
	public static String getDefaultTimebase(){
		return SettingsManager.defaultGlobal().getProperty("imageProc.w7x.archiveDB.timebase", "nanoTime");		
	}
	
	private static class StatusDetail {
		public int nImages;
		public int imagesSaved = 0, imagesExist = 0, imagesInvalid = 0, imagesFailed = 0;
		public boolean seriesMetaSaved = false, seriesMetaFailed = false, seriesMetaExists = false;
		public boolean aliasSaved = false, aliasFailed = false;
		public Exception lastError = null;
	}
	
	/** The actual save operation, run by the workers */
	private void doSequenceSaveCheck() {
		abortCalc = false;
		
		if(connectedSource == null) 
			return;
		
		if(!config.writeToCache && !config.writeToDB)
			throw new RuntimeException("Writing to niether cache nor archive. Nothing would be saved anywhere!!");
		
		
		setStatus("Saving...");
		StatusDetail stat = new StatusDetail();
		stat.nImages = connectedSource.getNumImages();
		lastSaveCompleted = false;
		
		if(config.updateAlias)
			doUpdateAlias(stat);
		
		try{
			
			
			System.out.println("W7XSink: nImages="+stat.nImages+", maxImages=" + maxImages);
			if(maxImages >= 0 && stat.nImages > maxImages)
				stat.nImages = maxImages;
	
			//need to sort out a timebase first			
			long ret[][] = prepNanotimes(stat.nImages);
			
			long nanoTime[] = ret[0];			
			fromNanos = ret[1][0];
			toNanos = ret[1][1];
			boolean isOrdered = (ret[1][2] != 0);
			
			//if timestamp order is wrong, we have to do image by image
			int imagesPerUpload = isOrdered ? config.imagesPerUpload : 1;
			
			//pick up bugs causing loss of w7x/ prefix
			if(!config.path.matches("/*w7x.*"))
				config.path = "w7x/" + config.path;
			
			ArchiveDBDesc desc = new ArchiveDBDesc(config.path, nanoTime[0], nanoTime[stat.nImages-1]);
			desc.setVersion(config.version);
			
			if(desc.getChannelName() == null) {
				desc.setChannelName("Images");
			}
			
			setStatus("Writing series metadata parLog");
			//write the parlog first, so the data always has some validity
			try{
				if(writeSingleImageParameterLog(desc, fromNanos, toNanos))
					stat.seriesMetaSaved = true;
				else
					stat.seriesMetaExists = true;
				
			}catch(Exception err){
				stat.seriesMetaFailed = true;
				err.printStackTrace();				
				stat.lastError = err;				
			}
			setProgressStatus(stat);
			
			
			if(config.writeToCache){
				//write all images to cache first, one by one
				for(int iT=0; iT < stat.nImages; iT++) {
					try{					
						
						storeImageInCache(desc, nanoTime, iT);
						if(!config.writeToDB){
							stat.imagesSaved++;
							setProgressStatus(stat);

							if(abortCalc){
								return;
							}
						}
					}catch(RuntimeException err){
						if(err instanceof InvalidImageException && !config.errorOnInvalid) {
							logr.log(Level.WARNING, "W7XArchiveDBSink: Invalid or null image "+iT + ". Not writing to cache: ", err);
							if(!config.writeToDB)
								stat.imagesInvalid++;
							
						}else {
							
							logr.log(Level.SEVERE, "W7XArchiveDBSink: Failed to write image "+iT + " to cache: ", err);
							if(!config.writeToDB)
								stat.imagesFailed++;
							stat.lastError = err;
						}
					}
				}
			}
				
			if(config.writeToDB){					
				//now write the images, in parts made of imagesPerUpload images
				for(int iT0=0; iT0 < stat.nImages; iT0 += imagesPerUpload) {				
					int nInUpload = Math.min(imagesPerUpload, stat.nImages - iT0);
	
					try{					
						
						if(writeImagesPart(desc, nanoTime, iT0, nInUpload))
							stat.imagesSaved += nInUpload;
						else
							stat.imagesExist += nInUpload;
						
						logr.fine("W7XArchiveDBSink: Successfully wrote images "+iT0 + " to " + (iT0 + nInUpload - 1));
						
					}catch(RuntimeException err){
						logr.log(Level.SEVERE, "W7XArchiveDBSink: Failed to write images "+iT0 + " to " + (iT0 + nInUpload - 1), err);
						stat.imagesFailed += nInUpload;
						err.printStackTrace();
						stat.lastError = err;
					}
					
					setProgressStatus(stat);
					
					if(abortCalc){
						return;
					}
				}
			}
			
			addPathToHistory(config.path);
			
		}catch(Exception err){
			err.printStackTrace();			
			stat.lastError = err;
			//assume all remaining failed
			stat.seriesMetaFailed = !stat.seriesMetaSaved && stat.seriesMetaExists;
			stat.imagesFailed = stat.nImages - stat.imagesSaved - stat.imagesExist - stat.imagesInvalid;
			
		}finally{			
			setProgressStatus(stat);
			
			lastSaveCompleted = (stat.imagesSaved == (stat.nImages - stat.imagesInvalid) && stat.seriesMetaSaved);
		}
	}
	
	private String aliasUpdateSyncObj = new String("aliasUpdateSyncObj");
	public void updateAlias() {
		ImageProcUtil.ensureFinalUpdate(aliasUpdateSyncObj, new Runnable() {
			@Override
			public void run() {
				StatusDetail stat = new StatusDetail();
				doUpdateAlias(stat);
				setProgressStatus(stat);
			}
		});				
	}
	
	private void doUpdateAlias(StatusDetail stat) {

		try{
			long nanoTime[] = W7XUtil.getBestNanotime(connectedSource);
			long tStart = nanoTime[0];
			long tEnd = nanoTime[nanoTime.length-1];
			
			if(config.writeAliasForWholeProgram){
				try {
					long tAvg = (tStart + tEnd) / 2;
					//could use ArchiveDBFetcher.defaultInstance().getAllProgramsInRangeJsonObj(tStart, tEnd); if visible			
					long tFrom = ((JsonPrimitive)ArchiveDBFetcher.defaultInstance().getProgramInfo(tAvg, "from")).getAsLong();
					long tTo = ((JsonPrimitive)ArchiveDBFetcher.defaultInstance().getProgramInfo(tAvg, "upto")).getAsLong();
					if(tFrom > 0){
						tStart = tFrom;
						tEnd = tTo;
					}
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Unable to determine whole program to set alias. Will write over acquisition period only.", err);
				}
			}
			
			ArchiveDBDesc aliasDesc = new ArchiveDBDesc(config.aliasPath);		
			//ArchiveDBDesc alias = ArchiveDBDesc.aliasDescriptor(Database.W7X_SANDBOX, "Minerva/Test/bla", 123L);
			aliasDesc.setFromNanos(tStart);		
			aliasDesc.setToNanos(tEnd);
			if(!aliasDesc.isAlias())
				throw new IllegalArgumentException("Alias isn't an alias (i.e. it's not in /views)");
	
			String targetPath = config.path;
			ArchiveDBDesc targetDesc = new ArchiveDBDesc(targetPath);
			//targetDesc.setVersion(config.lastVersionSaved);
			
			W7xArchiveWrite.writeAlias(aliasDesc, targetDesc);
			
			String str = getStatus();
			setStatus(str + " +Alias set.");
			stat.aliasSaved = true;
			updateAllControllers();
			
		}catch(RuntimeException err){
			logr.log(Level.WARNING, "Error while writing alias '"+config.aliasPath+"'", err);//
			String str = getStatus();
			//setStatus(str + " *Alias failed*.");
			stat.lastError = err;
			if(!config.writeToDB)
				stat.aliasFailed = true;
			updateAllControllers();
		}
		
	}

	private void setProgressStatus(StatusDetail s){
		setStatus("Save " + (abortCalc ? "aborted" : "")
				+ " "+s.nImages+" images: " 
						+ s.imagesSaved + " OK, " + s.imagesExist + " exist, " + s.imagesInvalid + " invalid, " + s.imagesFailed 
						+ " failed\n  SeriesMeta: " + (s.seriesMetaSaved ? "OK": (s.seriesMetaExists ? "Exists" : "Failed"))
						+ "\n Alias: " + (s.aliasSaved ? "OK" : (s.aliasFailed ? "Failed" : "Not updated"))
						+ ((s.lastError != null) ? ("\nERROR: " + s.lastError) : ""));

		updateAllControllers();
		
	}
	
	private void storeImageInCache(ArchiveDBDesc desc, long[] nanoTime, int iT) {
		Object imageData = collectImageData(iT);
		
		ArchiveDBDesc singleDesc = new ArchiveDBDesc(desc);
		singleDesc.setNanosRange(nanoTime[iT], nanoTime[iT]);
		
		ArchiveDB sig = new ArchiveDB(singleDesc, imageData, new long[]{ nanoTime[iT] });
		
		//add it to the write journal, so we don't get in a mess for stuff written
		//that may or may not be in the archive
		// FIXME: this is a inter-campaign hack solution
		String journalFile = ArchiveDBFetcher.defaultInstance().getCacheRoot() + "/imageProcWriteJournal";
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(journalFile, true)));
		    out.println(singleDesc.toString());
		    out.close();
		} catch (IOException e) {
		    //exception handling left as an exercise for the reader
		}
			
		ArchiveDBFetcher.defaultInstance().writeToCache(sig);
	}
	
	/** Write a section of a small number of images to WebAPI via HDF5. 
	 * Also writes them to the local DataSignals cache */
	private boolean writeImagesPart(ArchiveDBDesc desc, long nanoTime[], int iT0, int nInUpload){
		int iT1 = iT0 + nInUpload - 1;
		
		Object uploadArray = null;
		long uploadNanos[] = new long[nInUpload];
		
		for(int jT = 0; jT < nInUpload; jT++) {
			int iT = iT0 + jT;
			
			Object imageData = collectImageData(iT);
		
			if(uploadArray == null) {
				uploadArray = Array.newInstance(imageData.getClass(), nInUpload);
			}
			Array.set(uploadArray, jT, imageData);
			
			uploadNanos[jT] = nanoTime[iT];
		}
			
		
		ArchiveDBDesc partDesc = new ArchiveDBDesc(desc);			
		partDesc.setNanosRange(nanoTime[iT0], nanoTime[iT1]);
		if(partDesc.getVersion() < 0){
			partDesc.setVersion(0); //seems to work better. ugh
		}

		return sendImagePart(partDesc, uploadArray, uploadNanos);	
	}

	/** collect data from given image type into appropriate array */
	private Object collectImageData(int iT) {

		Img img = connectedSource.getImage(iT);
		if(img == null) {		
			throw new InvalidImageException("Image " + iT + " is null.");			
		}
		
		if(!config.saveInvalid && !img.isRangeValid()){
			throw new InvalidImageException("Image " + iT + " is invalid (ranges).");
		}
		
		Object imageData;

		ReadLock readLock = img.readLock();
		try{
			readLock.lockInterruptibly();			
		}catch(InterruptedException e){
			throw new RuntimeException(toShortString() + ": collectImageData(" + img + ") interrupted", e);
		}

		try{ // with read lock
				
			int nF = img.getNFields(), nY = img.getHeight(), nX = img.getWidth();
			if(img instanceof ByteBufferImage) {
				ByteBufferImage bbImg = (ByteBufferImage)img;
				ByteBuffer bBuff = bbImg.getReadOnlyBuffer();
				
				bBuff.order(bbImg.getByteOrder());
				bBuff.rewind();
				bBuff.position(bbImg.getHeaderSize());
				
				
				int bitDepth = bbImg.getBitDepth();					
				if(bitDepth > 0 && bitDepth <= 8){
					byte bArr[][][] = new byte[nF][nY][nX];
					for(int iF=0; iF < img.getNFields(); iF++)
						for(int iY=0; iY < img.getHeight(); iY++)
							bBuff.get(bArr[iF][iY]);
					imageData = bArr;
				}else if(bitDepth > 8 && bitDepth <= 16){
					short sArr[][][] = new short[nF][nY][nX];
					ShortBuffer sBuff = bBuff.asShortBuffer();
					for(int iF=0; iF < img.getNFields(); iF++)
						for(int iY=0; iY < img.getHeight(); iY++)
							sBuff.get(sArr[iF][iY]);
					imageData = sArr;
				}else if(bitDepth > 16 && bitDepth <= 32){
					int iArr[][][] = new int[nF][nY][nX];
					IntBuffer iBuff = bBuff.asIntBuffer();
					for(int iF=0; iF < img.getNFields(); iF++)
						for(int iY=0; iY < img.getHeight(); iY++)
							iBuff.get(iArr[iF][iY]);
					imageData = iArr;
				}else if(bitDepth == ByteBufferImage.DEPTH_FLOAT){
					float fArr[][][] = new float[nF][nY][nX];
					FloatBuffer fBuff = bBuff.asFloatBuffer();
					for(int iF=0; iF < img.getNFields(); iF++)
						for(int iY=0; iY < img.getHeight(); iY++)
							fBuff.get(fArr[iF][iY]);
					imageData = fArr;
				}else if(bitDepth == ByteBufferImage.DEPTH_DOUBLE){
					double dArr[][][] = new double[nF][nY][nX];
					DoubleBuffer dBuff = bBuff.asDoubleBuffer();
					for(int iF=0; iF < img.getNFields(); iF++)
						for(int iY=0; iY < img.getHeight(); iY++)
							dBuff.get(dArr[iF][iY]);
					imageData = dArr;
				}else {
					throw new IllegalArgumentException("Invalid bit depth " + bitDepth);
				}
			
			}else{		
				double dArr[][][] = new double[nF][nY][nX];
				for(int iF=0; iF < nF; iF++)
					for(int iY=0; iY < nY; iY++) 
						for(int iX=0; iX < nX; iX++) 
								dArr[iF][iY][iX] = img.getPixelValue(readLock, iF, iX, iY);					
				imageData = dArr;
			}
	
			if(nF == 1)	 //write images with 1 field as 2D
				imageData = Array.get(imageData, 0);
			
			//we can release the lock before writing, as we now have it as an array
		}finally{
			readLock.unlock();
		}
		
		return imageData; 
	}
	

	/** Pack the collected image data into HDF5 and hurl it at the WebAPI */
	private boolean sendImagePart(ArchiveDBDesc partDesc, Object uploadArray, long[] uploadNanos) {

		boolean exists;
		try{
			exists = adb.hasDataInArchive(partDesc);
		}catch(RuntimeException err){
			System.err.println("WARNING: Unable to check if data exists: " + err.getMessage());
			exists = false; //assume it doesn't exist. Probably we have no connection, but we can dump it to cache at least
		}
		
		if(exists && !config.overwriteInDB){
			System.err.println("WARNING: Images in part exist. Not writing. (" + partDesc + " )");
			return false;
		}
		
		int rank = Mat.getArrayDimensions(uploadArray.getClass());
		if(rank != 3)
			throw new RuntimeException("Upload rank is not 3. Does the image set have more than 1 field?");
		
		ArchiveDB sig = new ArchiveDB(partDesc, uploadArray, uploadNanos);		
		
		WebApiHdf5Writer.write(sig, null); //don't write params via HDF5, otherwise we write a Parlog for each image
		return true;
	}
	
	/** @return new long[][]{ timestamps, long[]{ min, max, isOrdered } } */
	private long[][] prepNanotimes(int nImages) {
		long t[] = new long[nImages];
		
		if(config.timebaseMode == TimebaseMode.AUTOSELECT){
			
			ArrayList<String> timebaseNames = getPossibleTimebases();
			config.timebaseMetadataName = null;
			for(String name : timebaseNames){
				if(name.endsWith("nanoTime")){
					config.timebaseMetadataName = name;
				}
			}
			
		}else if(config.timebaseMode == TimebaseMode.IMGNUM){ //image number as seconds, up until now
			long tNow = ArchiveDBDesc.toNanos(ZonedDateTime.now());
			for(int i=0; i < nImages; i++){
				t[i] = tNow - 1_000_000_000L * (nImages-1 - i);
			}
			lastTimestampsWarning = null;
			return new long[][]{ t, { t[0], t[nImages-1], 1 } };
		}	
		
		lastTimestampsWarning = null; //ok
		
		if(config.timebaseMetadataName == null) {
			setStatus("ERROR: No valid timebase found/selected");
			throw new IllegalArgumentException("No valid timebase found/selected");
		}
		
		MetaDataMap map = getCompleteSeriesMetaDataMap();
		Object o = map.get(config.timebaseMetadataName);
		
		//Hack for old Sensicam which has weird wrong naming 
		if(config.timebaseMetadataName.equals("Sensicam/selectTimeSecs")){
			double[] d = (double[])Mat.fixWeirdContainers(o, true);
			long l[] = new long[d.length];
			for(int i=0; i<d.length;i++)
				l[i] = (long)(d[i]*1e9);
			o = l;
		}

		if(o == null || !o.getClass().isArray()
				|| o.getClass().getComponentType().isArray()					
				|| !(Long.class.isAssignableFrom(o.getClass().getComponentType())
					 || long.class.isAssignableFrom(o.getClass().getComponentType()))
				|| Array.getLength(o) < nImages){
			throw new RuntimeException("Metadata '" + config.timebaseMetadataName + "' is not a 1D array of length at lesat " + nImages + " assignable to a long");
		}

		//o might actually be longer, if it were allocated for more images than actually recorded
		for(int i=0; i < nImages; i++){
			t[i] = (long)Array.get(o, i);
		}

		boolean isOrdered = true;
		int nBumpedTimestamps = 0;
		//detect / fix stupid/broken timestamps
		//.... sorry :/
		for(int i=0; i < nImages; i++){	
			if(abortCalc){ //this loop can be quite length when the PC is slow and AcqDevice is fast
				throw new RuntimeException("Aborted");
			}
			
			if(i > 0){
				if(t[i] == 0) {
					lastTimestampsWarning = "WARNING: Timestamp t["+i+"]=0";
					
					int j=i;
					for(; j < nImages; j++) {
						if(t[j] != 0)
							break;
					}
					if(j == nImages) {
						lastTimestampsWarning = "WARNING: Timestamps t["+i+" - end]=0, assuming short capture";
						break;
					}else {						
						lastTimestampsWarning = "WARNING: Timestamps t["+i+" - "+j+"] = 0, but t["+j+" - end] != 0";
						i = j; //short the outer loop up to the good timestamps
					}
					
					
				} else if(t[i] < t[i-1]){
					lastTimestampsWarning = "WARNING: Timestamp ordering problem: t["+i+"]="+t[i]+" < t["+(i-1)+"]="+t[i-1];
					isOrdered = false;
					
					
				}else if(t[i] == t[i-1]){
					lastTimestampsWarning = "WARNING: Timestamps equal t["+(i-1)+"] == t["+i+"] = "+t[i]+", incremaing by 1ns:";
					
					for(int j=i; j < nImages; j++){
						if(t[j] == t[i-1]){
							t[j] += 1L;
							nBumpedTimestamps++;
						}else
							break;
					}				
				}
			}
		}
		System.err.println("WARNING: Bumped "+nBumpedTimestamps+" timestamps by +1ns as they were equal");

		//horrible hack for Valeria's broken timestamps
		if(t[0] > 1_500_000_000_000L && t[0] < 1_500_000_000_000_000L){
			System.err.println("WARNING: timestamp looks like milisecs sincs 1970, multiplying by 1e6");
			
			for(int i=0; i < t.length; i++){
				t[i] *= 1_000_000L;
			}
		}
		
		//long nano2000 = 946_684_800_000_000L; //isn't this 10th jan 1970? nvm, works for this purpose
		long nano2000 = 946_080_000_000_000_000L;
		long nano2060 = 2_893_106_684_800_000_000L; //if w7x, let alone this code is still going, we've got bigger problems
		
		if(t[0] < nano2000){
			throw new RuntimeException("Timestamps are before year 2000. Something is probably wrong with them!");
		}
		
		//find min max sane times
		long tMin = Long.MAX_VALUE, tMax = Long.MIN_VALUE;
		for(int i=0; i < t.length; i++){
			if(t[i] > nano2000 && t[i] < nano2060 && t[i] < tMin) tMin = t[i];
			if(t[i] > nano2000 && t[i] < nano2060 && t[i] > tMax) tMax = t[i];
		}
			
		return new long[][]{ t, { tMin, tMax, isOrdered?1:0 } };


	}
	
	/** Writes the PARLOG containing ALL the metadata. 
	 * As of 10.6.18 this includes both time series and non-time series. */
	private boolean writeSingleImageParameterLog(ArchiveDBDesc desc, long tMin, long tMax) {

		ArchiveDBJSONInfoDesc parLogDesc = new ArchiveDBJSONInfoDesc(desc);
		parLogDesc.setType(Type.PARLOG);
		parLogDesc.setNanosRange(tMin, tMax);
		parLogDesc.setChannelName(null);
		parLogDesc.setChannelNumber(-1);
				
		//see if it already exists
		boolean exists;
		try{
			//for unversioned streams, this only works if version==MIN_INT, because someone somewhere is a complete moron
		
			ArchiveDBJSONInfoDesc parLogCheckDesc = new ArchiveDBJSONInfoDesc(parLogDesc);
			if(parLogCheckDesc.getVersion() < 0)
				parLogCheckDesc.setVersion(Integer.MIN_VALUE); //seems to work, -1 doesn't
			exists = adb.hasDataInArchive(parLogCheckDesc);
		}catch(RuntimeException err){
			System.err.println("WARNING: Unable to check if image parLog exists: " + err.getMessage());
			exists = false; //assume it doesn't exist. Probably we have no connection, but we can dump it to cache at least
		}
		
		if(exists && !config.overwriteInDB){
			System.err.println("WARNING:Image parLog exists. Not writing. (" + parLogDesc + ")");
			return false;
		}
		
		//ParameterLog2 parLogB = new ParameterLog2(parLogDesc, "Saving images", getClass().getCanonicalName());
		ParameterLog parLog = new ParameterLog(desc.getDatabase(), 
				desc.getSection().toString() + "/" +
					desc.getProject() +  "/"+ 
						desc.getSignalGroup() + "/"+ 
						desc.getSignalName(),
						parLogDesc.getVersion() < 0 ? 0 : parLogDesc.getVersion(), 
				tMin, tMax, "Saving images", getClass().getCanonicalName());
		
		///how is this not the same thing????
		
		parLog.setDeployParamsToTree(true);
		
		//specific parameters to make the images work in the database
		//parLog.addParameter("dataBoxSize", nanos.length); //do we need this???		
		//parLog.addParameter("datatype", OneLiners.getElementType(signal.getData().getClass()).getSimpleName()); 
		parLog.addParameter("unsigned", 0);

		parLog.addParameter("chanDescs/[0]/name", desc.getChannelName());
		parLog.addParameter("chanDescs/[0]/active", 1);
		
		//time series entries
		ArrayList<String> timeSeriesNames = new ArrayList<>();
		MetaDataMap map = connectedSource.getCompleteSeriesMetaDataMap();
		for(Entry<String, MetaData> entry : map.entrySet()) {
			if(entry.getValue() != null){
				if(entry.getValue().isTimeSeries)
					timeSeriesNames.add(entry.getKey());
					
				parLog.addParameter(entry.getKey(), entry.getValue().object);
			}
		}
		
		//write the list of metadata entries which are time series
		parLog.addParameter("timeSeriesEntries", timeSeriesNames.toArray(new String[0]));

		//put it in our local DataSignals cache first
		//version should be set to -1 by config to say to write it to newest (same as data)
		//if(parLogDesc.getVersion() <= 0)
			//parLogDesc.setVersion(); //we can't actually know what version we're going to write it to
		//unless they specified a version
		
		String jsonString = parLog.toJson().toString();
		ArchiveDBJSONObject jsonObj = new ArchiveDBJSONObject(parLogDesc, jsonString);
		
		if(config.writeToCache) {
			//add it to the write journal, so we don't get in a mess for stuff written
			//that may or may not be in the archive
			// FIXME: this is a inter-campaign hack solution
			String journalFile = ArchiveDBFetcher.defaultInstance().getCacheRoot() + "/imageProcWriteJournal";
			try {
			    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(journalFile, true)));
			    out.println(parLogDesc.toString());
			    out.close();
			} catch (IOException e) {
			    //exception handling left as an exercise for the reader
			}
			
			ArchiveDBFetcher.defaultInstance().writeToCache(jsonObj);
		}
		
		if(config.writeToDB)			
			parLog.send(false);
		
		return true;

	}
	
	@Override
	/** Save triggered by a software controller of some kind.
	 * So we advance, write a note and save */
	public void triggerSave() {
		//TODO: Add 'Autosaved' message to some kind of notes/log/? ... 
		
		updateAllControllers();
		
		save();
	}
	
	@Override
	public void triggerSave(int id) {
		triggerSave(); //id is irrelevant, since we write under timestamps
	}
	
	@Override
	public String getPath() {
		return pathFixup(config.path);
	}

	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	} //we don't really want to save based on the calc signal
	@Override
	public boolean wasLastCalcComplete(){ return lastSaveCompleted; }
	@Override
	public void abortCalc(){ }

	/** Get names of all D number metadata that could be used as a timebase */ 
	public ArrayList<String> getPossibleTimebases() {
		return ImageProcUtil.getPossibleTimebases(connectedSource);		
	}
	

	public void setTimebaseMetadata(TimebaseMode mode, String name) {
		try {
			abortCalc = false;
			
			config.timebaseMetadataName = name;
			config.timebaseMode = mode;
			
			if(connectedSource == null)
				return;
			
			fromNanos = -1; //in case of exceptions
			toNanos = -1;
			
			int nImagesToSave = connectedSource.getNumImages();
			if(maxImages > 0)
				nImagesToSave = Math.min(maxImages, nImagesToSave);
			
			//we can update the time as well
			
			long ret[][] = prepNanotimes(nImagesToSave);
			long nanoTime[] = ret[0];			
			fromNanos = ret[1][0];
			toNanos = ret[1][1];
			boolean isOrdered = (ret[1][2] != 0);
			
		}catch(RuntimeException err) {
			status = "Error settings timestamps:" + err.getMessage();
			updateAllControllers();
			throw err;
		}
	}

	@Override
	public String toShortString() {
		return "W7X Save";
	}

	public W7XArchiveDBSinkConfig getConfig() { return config;	}
	

	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		json = json.replaceAll("},", "},\n");
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, W7XArchiveDBSinkConfig.class);
		
		updateAllControllers();	
	}

		
	public int getExistingCount() {
		abortCalc = false;
		
		int nImages = connectedSource.getNumImages();
		
		if(maxImages >= 0 && nImages > maxImages)
			nImages = maxImages;

		//need to sort out a timebase first			
		long ret[][] = prepNanotimes(nImages);
		long nanoTime[] = ret[0];			
		fromNanos = ret[1][0];
		toNanos = ret[1][1];
		boolean isOrdered = (ret[1][2] != 0);
			
		ArchiveDBDesc desc = new ArchiveDBDesc(getPath(), nanoTime[0], nanoTime[nImages-1]);
		if(config.version >= 0){
			desc.setVersion(config.version);
		}else{ //-ve version means unversioned, but it specifically needs to be MIN_INT for getIntervals to work
			desc.setVersion(Integer.MIN_VALUE);
		}
		if(desc.getChannelName() == null) {
			desc.setChannelName("Images");
		}
		
		long existingTimes[][] = adb.getIntervals(desc, true); //disable cache!
			
		for(int i=0; i < existingTimes[0].length; i++){
			if(existingTimes[0][i] != nanoTime[i])
				throw new RuntimeException("Existing timebase mismatches. i="+i+", existing="
									+existingTimes[0][i]+", toWrite=" + nanoTime[i] );	
		}
			
		return existingTimes[0].length; //hope they're the same
		
	}

	public String getLastTimestampWarning(){ return lastTimestampsWarning; }

	public void saveNotes(String notes) { super.saveNotes(getPath(), fromNanos, toNanos, notes); }	
	public String loadNotes() { return super.getNotes(getPath(), fromNanos, toNanos); }

	public static long[] getBestNanotimebase(ImgSource src) {
		ArrayList<String> timebases = ImageProcUtil.getPossibleTimebases(src);
		long nanoTime[] = null;
		for(String timebaseName : timebases){
			if(timebaseName.endsWith("/nanoTime")){ //any just named 'nanoTime' is best
				nanoTime = (long[]) Mat.fixWeirdContainers(src.getSeriesMetaData(timebaseName), true);
				break;
			}else if(timebaseName.endsWith("anoTime")){ //otherwise xxx[nN]anoTime is maybe also ok
				nanoTime = (long[]) Mat.fixWeirdContainers(src.getSeriesMetaData(timebaseName), true);
				//but keep looking for a /nanoTime
			}
		}
		
		if(nanoTime == null)
			throw new RuntimeException("Unable to find a nanoTime metadata entry.");
		
		return nanoTime;
	}
}
