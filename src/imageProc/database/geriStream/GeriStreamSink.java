package imageProc.database.geriStream;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.LongStream;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;

import geri.Types;
import geri.providers.LogProvider;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ByteBufferImage;
import imageProc.core.ConfigurableByID;
import imageProc.core.DatabaseSink;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.geriStream.ImageProcImageProvider.ImagePass;
import imageProc.database.geriStream.ImageProcImageProvider.ImageStatus;
import imageProc.database.geriStream.swt.GeriStreamSWTControl;
import imageProc.pilot.geri.GeriPilot;
import imageProc.w7x.W7XUtil;
import oneLiners.OneLiners;
import w7x.archive.ArchiveDBFetcher;
import w7x.archive.writing.W7xArchiveWrite;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import de.mpg.ipp.codac.signalaccess.SignalAddress;
import de.mpg.ipp.codac.signalaccess.SignalAddressBuilder;
import de.mpg.ipp.codac.signalaccess.SignalReader;
import de.mpg.ipp.codac.signalaccess.experimental.ExperimentalSignal;
import de.mpg.ipp.codac.signalaccess.objylike.ArchieToolsFactory;
import de.mpg.ipp.codac.signalaccess.readoptions.ReadOptions;
import de.mpg.ipp.codac.w7xtime.TimeInterval;
import descriptors.w7x.ArchiveDBDesc;

/** Image sink for streaming to codastation via GERI 
 * Couples to GeriPilot 
 */
public class GeriStreamSink extends ImgSourceOrSinkImpl implements ImgSink, DatabaseSink, ConfigurableByID, EventReciever {
	private String status = "init";
	
	/** Link to the pilot module, in order to give it the streamer which will talk to the codastation via GERI */
	private GeriPilot pilot;
	
	private ImageProcImageProvider imageProvider = new ImageProcImageProvider(this);
	private	MetaDataLogProvider logProvider = new MetaDataLogProvider(this);
	
	private GeriStreamConfig config = new GeriStreamConfig();

	private String lastLoadedConfig;
		
	public String getStatus() { 
		return status + "\n" + imageProvider.getStreamingStatus();
	}

	public GeriStreamConfig getConfig() { return config; }
	
	public GeriStreamSink() {
		
		
	}
	
	/** Walk the entire source/sink tree to find the GeriPilot */
	private GeriPilot findGeriPilot(ImgSource source) {
		for(ImgSink sink : source.getConnectedSinks()) {
			if(sink instanceof GeriPilot)
				return (GeriPilot)sink;
			
			if(sink instanceof ImgSource) {
				GeriPilot ret = findGeriPilot((ImgSource)sink);
				if(ret != null)
					return ret;
			}
		}
		return null;
	}
	
	@Override
	public void setSource(ImgSource source) {
		super.setSource(source);
		
		while(source instanceof ImgSink) 
			source = ((ImgSink)source).getConnectedSource();
		
		this.pilot = findGeriPilot(source);
		if(this.pilot == null)
			throw new RuntimeException("Cannot add a GeriStreamSink without a GeriPilot. The pilot needs to be added first");
		
		//register the streamer with the pilot so that it can get the provider when a connection is made
		pilot.addProvider(logProvider);
		pilot.addProvider(imageProvider);
	}
	
	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object[] args, boolean asSink) {
		ImagePipeController controller = null;
		if(interfacingClass == Composite.class){			
			controller = new GeriStreamSWTControl((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);			
		}
		return controller;		
	}
	
	@Override
	public void triggerSave(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void triggerSave() {
		// TODO Auto-generated method stub
		
	}
	
	public GeriPilot getPilot() { return pilot; }

	
	@Override
	public void imageChangedLowLevel(int idx) {
		//if(!provider.isInited() || !pilot.isConnected())
			//return;		
		imageProvider.imageChangedLowLevel(idx);
	}
	
	@Override
	public void notifySourceChanged() {
		super.notifySourceChanged();
		imageProvider.imageSetChanged();
		
	}

	@Override
	public boolean isIdle() { return !imageProvider.isStreamingActive();	}

	@Override
	public boolean getAutoUpdate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setAutoUpdate(boolean enable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void calc() {
		//if we are a sinks of processing source, we have to write the parlog
		//after the processing, as this is not called from the Automation
		/*boolean configWriteParlogOnProcess = !(connectedSource instanceof AcquisitionDevice);
		if(configWriteParlogOnProcess)
			sendMetadata(imageProvider.getLatestNanotime());
			*/
	}

	@Override
	public boolean wasLastCalcComplete() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void abortCalc() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		json = json.replaceAll("},", "},\n");
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, GeriStreamConfig.class);
		
		logProvider.setMaxLength(config.maximumParlogSize);
		
		updateAllControllers();	
	}

	public void setImageParamsFromSource() {
		Img img = connectedSource.getImage(0);
		config.imageWidth = img.getWidth();
		config.imageHeight = img.getHeight();
		if(img instanceof ByteBufferImage)
			switch(((ByteBufferImage)img).getBitDepth()){
			case 8: config.imageType = Types.int8; break;
			case 16: config.imageType = Types.int16; break;
			case 32: config.imageType = Types.int32; break;
			case 64: config.imageType = Types.int64; break;
			case ByteBufferImage.DEPTH_FLOAT: config.imageType = Types.float32; break;
			case ByteBufferImage.DEPTH_DOUBLE: config.imageType = Types.float64; break;
			default:
				throw new RuntimeException("Couldn't determine type of image");
		}
			 
		updateAllControllers();	
		
	}

	public ArrayList<ImagePass> getImageHistory() { return imageProvider.getImageHistory(); }

	public void startStreaming() {
		logr.log(Level.FINE, "Starting streaming. (geri state currently " + pilot.getGERIState() + ")");
		pilot.getLog().write("Starting streaming. (geri state currently " + pilot.getGERIState() + ")");
		imageProvider.startStreaming();
		logProvider.startStreaming();
		
		updateAllControllers();
	}
	
	public void stopStreaming(boolean interrupt) {
		logr.log(Level.FINE, "Stopping streaming. (geri state currently " + pilot.getGERIState() + "), interrupt = " + interrupt);
		pilot.getLog().write("Stopping streaming. (geri state currently " + pilot.getGERIState() + "), interrupt = " + interrupt);
		imageProvider.stopStreaming(interrupt);
		logProvider.stopStreaming(interrupt);
		
		updateAllControllers();
	}

	public boolean isStreamingActive() { return imageProvider.isStreamingActive() || logProvider.isStreamingActive(); }
	public boolean isDataStreamingActive() { return imageProvider.isStreamingActive(); }
	public boolean isParlogStreamingActive() { return logProvider.isStreamingActive(); }

	public boolean isProviderConnected() { return imageProvider.isConnected(); }

	public ArrayList<String> getPossibleTimebases() {
		return ImageProcUtil.getPossibleTimebases(connectedSource);
	}

	/** Sends the metadata as a parlog via the parlog streaming provider */
	public void sendMetadata(long timestamp) {
		logProvider.queueMetadata(timestamp);
		logProvider.startStreaming();
	}

	@Override
	public void event(Event event) {
		switch(event){
			case AcquisitionInit:
				imageProvider.resetImageNumber();
				break;
			case GlobalAbort:
				stopStreaming(false);
				break;
		}
	}
	
	public ImageProcImageProvider getImageProvider() { return imageProvider; }

	/** Gets the current transmission rate of frame per second */
	public double getCurrentTransmitRate() { return imageProvider.getCurrentTransmitRate();	}

	private String aliasUpdateSyncObj = new String("aliasUpdateSyncObj");
	public void updateAlias() {
		ImageProcUtil.ensureFinalUpdate(aliasUpdateSyncObj, () -> doUpdateAlias());				
	}
	
	private void doUpdateAlias() {
		try{
			long nanoTime[] = imageProvider.getNanotime();
			long tStart = nanoTime[0];
			long tEnd = nanoTime[nanoTime.length-1];
			
			if(config.writeAliasForWholeProgram){
				try {
					long tAvg = (tStart + tEnd) / 2;
					//could use ArchiveDBFetcher.defaultInstance().getAllProgramsInRangeJsonObj(tStart, tEnd); if visible			
					long tFrom = ((JsonPrimitive)ArchiveDBFetcher.defaultInstance().getProgramInfo(tAvg, "from")).getAsLong();
					long tTo = ((JsonPrimitive)ArchiveDBFetcher.defaultInstance().getProgramInfo(tAvg, "upto")).getAsLong();
					if(tFrom > 0){
						tStart = tFrom;
						tEnd = tTo;
					}
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Unable to determine whole program to set alias. Will write over acquisition period only.", err);
				}
			}
			
			ArchiveDBDesc aliasDesc = new ArchiveDBDesc(config.aliasPath);		
			//ArchiveDBDesc alias = ArchiveDBDesc.aliasDescriptor(Database.W7X_SANDBOX, "Minerva/Test/bla", 123L);
			aliasDesc.setFromNanos(tStart);		
			aliasDesc.setToNanos(tEnd);
			if(!aliasDesc.isAlias())
				throw new IllegalArgumentException("Alias isn't an alias (i.e. it's not in /views)");
	
			String targetPath = config.datastreamName;
			ArchiveDBDesc targetDesc = new ArchiveDBDesc(targetPath);
			//targetDesc.setVersion(config.lastVersionSaved);
			
			W7xArchiveWrite.writeAlias(aliasDesc, targetDesc);
			
			if(!status.contains("Alias set"))
				status += " +Alias set. ";
			
				
			updateAllControllers();
			
		}catch(RuntimeException err){
			String errStr = "Error while writing alias '"+config.aliasPath+"'";
			logr.log(Level.WARNING, errStr, err);//
			
			status += "Alias: " + errStr;
			updateAllControllers();
		}
		
	}

	/** Check whether images have been successfully written to archive and updates the image status accordingly */ 
	public void checkDataInArchive() {
				
		if(config.datastreamName == null || config.datastreamName.length() == 0)
			return; //disabled
		//config.datastreamName="w7x/ArchiveDB/raw/W7X/ControlStation.71301/ILS_Red-1_DATASTREAM/0/Images";
		ArchiveDBDesc desc = new ArchiveDBDesc(config.datastreamName);
				
		
		long fromNanos = imageProvider.getEarliestNanotime();
		long toNanos = imageProvider.getLatestNanotime();
		
		desc.setNanosRange(fromNanos, toNanos);
		
		long[][] tNanos = checkDataInArchive(desc, !config.checkArchiveIntervals);
		if(tNanos == null) {
			logr.info("No images (intervals) found in database (returned null).");
			return;
		}
		
		logr.info("Found " + tNanos[0].length + " images/intervals in archive");
		
		imageProvider.confirmImages(tNanos);
		
        updateAllControllers();
	}
	
	/**
	 * 
	 * @param desc
	 * @param readIndividualSamples If true, read the data to get timestamps of each sample, otherwise return the intervals
	 * @return Intervals or sample timestamps
	 */
	public long[][] checkDataInArchive(ArchiveDBDesc desc, boolean readIndividualSamples) {		
		long tNanos[] = null;
		
        try (ArchieToolsFactory stf = ArchieToolsFactory.remoteArchive(desc.getDatabase().toString())) {

            SignalAddressBuilder sab = stf.makeSignalAddressBuilder();
            
            //dbPath = dbPath + ((desc.getCodacScaling() != null) ? ("/" + desc.getCodacScaling().name()) : "");
            
            TimeInterval timeInterval = TimeInterval.with(desc.getFromNanos(), desc.getToNanos());
            
            SignalAddress adr = sab.newBuilder()
                    .path(desc.getPartialPath())
                    .validityInterval(timeInterval)
                    .getSignalAddress();
            
            try ( final SignalReader sr  = stf.makeSignalReader( adr )) {
            	
            	if(!readIndividualSamples) {
	            	//get intervals first
	            	List<TimeInterval> intervals = sr.availableIntervals(timeInterval);
	            	
	            	long ret[][] = new long[2][intervals.size()];
	            	
	            	//list of lists of things of lists... ugh
	            	int i=0;
	            	for(TimeInterval interval : intervals) {
	            		ret[0][i] = interval.from();
	            		ret[1][i] = interval.upto();
	            		i++;	            			
	            	}
	            	return ret;
            	}
	            	
                ReadOptions readOptions = ReadOptions.fetchAll();

                final de.mpg.ipp.codac.signalaccess.Signal sig = sr.readSignal(timeInterval, readOptions);

                final ExperimentalSignal s = (ExperimentalSignal) sig;//s.get(sa);
                
                if (sig == null) {
                    throw new RuntimeException("Could not read signal '" + desc);
                }
                
                ExperimentalSignal s_t = (ExperimentalSignal) sig.getDimensionSignal(0);
                tNanos = LongStream.of(s_t.getValuesLong()).toArray();
                
                if(tNanos == null)
                	logr.info("No times found in archive for " + desc);
                	
                return new long[][] { tNanos, tNanos };

            } catch ( Exception e) {
                logr.log(Level.WARNING, "checkDataInArchive caught exception in MultiSignalReader loop", e);
            }

        } catch (Exception e) {
            logr.log(Level.WARNING, "checkDataInArchive caught exception in ArchieToolsFactory catch block", e);
            return null;
        }
        return null;
        
	}

	public void setStatus(String status) {
		this.status = status;
		
		updateAllControllers();
	}
}
