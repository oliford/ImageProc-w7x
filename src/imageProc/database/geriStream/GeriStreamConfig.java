package imageProc.database.geriStream;

import geri.Types;
import geri.Types.Scalar;
import imageProc.pilot.geri.ImageProcResource;
import imageProc.pilot.geri.ImageProcResource.GeriState;

/** Configuration for streaming to codastation via GERI */
public class GeriStreamConfig {

	/** GERI unit ID of data stream. This needs to be set before the codastation connects to the GeriPilot server! */ 
	public int imageStreamID = -1;

	/** GERI unit ID of parlog stream matched to this data stream */
	public int parlogStreamID = -1;
	
	/** Expected width, height and type of image. This is given to the codastation when it connects,
	 * which may be before the acquisition device has made an acquisition, so we won't yet know
	 * what it is. The user can set it here to give that info in advance.
	 * 
	 * If the actual images do not match this type, the streaming will not be started and the streaming
	 * module will report failure.
	 */
	public int imageWidth = 0;	
	public int imageHeight = 0;	
	public Scalar imageType = Types.int16;
	
	/** if true swaps bytes in type length before sending. */
	public boolean swapEndianess = true;
	
	/** After/during streaming, check that images have arrived in archive at given datastream */	
	public boolean checkArchiveArrival = true;
	
	/** When checkArchiveArrival = true, Only get intervals of images and assume all images in each box
	 * are there, rather than reading the actual signal */
	public boolean checkArchiveIntervals = true;
	
	/** What we think the datastream name is. Not used for streaming but only
	 * to check if everything arrives in the archive and to point the alias at.
	 * The stream target is specified by the XML config file into the codastation, or maybe the
	 * configuration DB, who knows. */
	public String datastreamName = "ArchiveDB/raw/W7X/ControlStation.xxxxx/DEVICE-1_DATASTREAM";

	/** Name of metadata entry to use for timebase */
	public String timebase;
	
	/** Maximum bandwidth to send to codastation in MB/s.
	 * Achieved by delaying individual images.
	 * From basic testing from 4x PCs in the QSK rack to sv-coda-wsvc-28
	 *   each shifts about 40MB/s ok will all 4 running. That goes up to 120MB/s
	 *   for a single one.
	 */ 
	public double bandwidthThrottleMBPs = 30.0;

	/** Number of images to include in a single 'packet' transmission to the codastation.
	 * This is limited to the number of samples accepted by the codastation. */
	public int imagesPerTransmit = 1;

	/** Maximum size for parlog providers reported to the codastation when it connects.
	 * This won't have any affect after the connection is made. */
	public int maximumParlogSize = 20*1024*1024-5;

	/** Alias path to update to point to this stream for the image range being saved. */
	public String aliasPath = "w7x/Sandbox/views/KKS/QS-_TestSpectroscopy/Raw_Camera1";
	
	/** Update the given alias when the image streaming starts. */
	public boolean updateAlias = false;

	/** When writing an alias for an acquisition in a W7X program, write the alias
	 * to cover the whole program, otherwise it only covers the range of 
	 * the first/last written images. */	
	public boolean writeAliasForWholeProgram = true;

	/** Specific hack for OceanOptics OmniDriver acquisition that reads shorts but it comes as doubles */
	public boolean convertDoubleToShort = false;

	/** Check if the images actually match the image type stored here to be reported to codastation 
	 * If not, throw an exception. */
	public boolean checkImageType = true;

	/** By how many images to advance the streaming when an overrun is imminent.
	 * This has to be at least 1 (otherwise we loose track of the head/tail)
	 * If 1, we'll basically send every image we can, and they will be accordingly sparse.
	 * If more (e.g. 10) then we end up sending reasonable continuous blocks, with blocks of gaps. 
	 */
	public int streamAdvanceOnOverrun = 1;
	
	/** timeout in ms to wait for a valid image with no timestamp to get a timestamp before skipping 
	 * to the next image. If <=0, then it'll wait forever */
	public long timestampWaitTimeout = 500;
	
	public boolean stopOnNoImageWhenAcquisitionStopped = false;
}
