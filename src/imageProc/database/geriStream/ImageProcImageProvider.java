package imageProc.database.geriStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.apache.http.config.SocketConfig;

import de.mpg.ipp.codac.signalaccess.SignalAddress;
import de.mpg.ipp.codac.signalaccess.SignalAddressBuilder;
import de.mpg.ipp.codac.signalaccess.SignalReader;
import de.mpg.ipp.codac.signalaccess.SignalsTreeLister;
import de.mpg.ipp.codac.signalaccess.experimental.ExperimentalSignal;
import de.mpg.ipp.codac.signalaccess.objylike.ArchieToolsFactory;
import de.mpg.ipp.codac.signalaccess.objylike.MultiSignalReader;
import de.mpg.ipp.codac.signalaccess.readoptions.ReadOptions;
import de.mpg.ipp.codac.w7xtime.TimeInterval;
import descriptors.w7x.ArchiveDBDesc;
import geri.BufferDescription;
import geri.SocketConnection;
import geri.Types;
import geri.Types.Scalar;
import geri.providers.ImageProvider;
import geri.providers.SignalProvider;
import imageProc.core.AcquisitionDevice;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.database.geriStream.ImageProcImageProvider.ImageStatus;
import imageProc.core.ByteBufferBackedImage;
import imageProc.core.ByteBufferImage;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSource;
import imageProc.pilot.common.PilotLog;
import imageProc.w7x.W7XUtil;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import ru.ximc.libximc.JXimc.pid_settings_t;
import signals.w7x.ArchiveDB;
import w7x.archive.ArchiveDBFetcher;

/** Provider of images to Geri from imageProc's source buffer at a regular continuous rate */
public class ImageProcImageProvider extends ImageProvider {
	private GeriStreamSink sink;	
	private Logger logr = Logger.getLogger(this.getClass().getName());
	
	/** The next image be expect to be changed/filled. This is effectively the head of the streaming ring buffer */
	private int nextImageToFill = -1;
	
	/** The next image to stream. This is effectively the tail of the ring buffer */
	private int nextToStream = 0;
	
	/** Earliest nanotime sent since last (re)initialisation */
	private long firstImageNanotime = -1;
	
	/** Local wall time of last send */
	private long lastTransmit = 0;
	/** Latest nanotime sent (in the last send) */
	private long lastTransmitNanotime = -1;
	
	private long lastFPSUpdate = 0;
	private int fpsUpdatePeriod = 1000;
	private double lastFPS = 0;
	private int imagesSinceLastFPSUpdate = 0;
	
	private long timestampTimeoutStart = -1;
	
	/** ms timestamp of last call to process(), for debugging/status */
	private long lastProcessCall = -1;
	
	/** Information about a pass of acquisition and streaming over images array */
	public static class ImagePass {
		public ImagePass(int n) {
			status = new ImageStatus[n];
			changeID = new int[n];
			timestamp = new long[n];
			for(int i=0; i < n; i++) {
				status[i] = ImageStatus.UNKNOWN;
				changeID[i] = Integer.MIN_VALUE;
				timestamp[i] = -1;
				
			}
		}
		public ImageStatus[] status;
		public int[] changeID;
		public long[] timestamp;
	}
	
	/** Information about each pass of acquisition and streaming over images array */
	private ArrayList<ImagePass> imageHistory = new ArrayList<>();
	
	@Override
	public void startStreaming() {
		//hack sim
		/*initedDataType = new Type[numChannels];
		for(int i=0; i < numChannels; i++)
			initedDataType[i] = getType(i);				
		*/
		
		//in case we do this at PRE and the codastation takes a little bit of time to (ask us to) open the connections
		//delay here (yes, I know hacky hack)
		try {
			Thread.sleep(500);
		}catch(InterruptedException err) { }
		
		//also recheck image parameters
		try {
			logr.log(Level.FINE, "+startStreaming()");
			super.startStreaming();
			
			checkImageParameters();

			sink.setStatus("Streaming started");
			logr.log(Level.FINE, "-startStreaming() OK");
			
		}catch(RuntimeException err) {
			stopStreaming(true);
			String str = "ERROR: Check image parameters failed: " + err.getMessage();
			sink.getPilot().getLog().write(str);
			sink.setStatus(str);
			throw err;
		}
	}
	
	@Override
	public void stopStreaming(boolean interrupt) {
		logr.log(Level.FINE, "stopStreaming("+interrupt+")");
		super.stopStreaming(interrupt);
		
		sink.setStatus("Streaming stopped");
	}
	
	public ImageProcImageProvider(GeriStreamSink sink) { 
		super(-1, -1, 0, null);
		this.sink = sink;
	}
	
	@Override
	public String describe() {
		//called when the codastation connects to our GERI server
		try {
			checkImageParameters();
		}catch(RuntimeException err) {
			sink.getPilot().getLog().write("ERROR:" + err.getMessage());
		}
		
		return super.describe();
	}
	
	private void checkImageParameters() {	
	
		//here we need to have the ID configured and the image type set
		PilotLog log = sink.getPilot().getLog();
		
		GeriStreamConfig cfg = sink.getConfig();
		int id = cfg.imageStreamID;
		if(id < 0) 
			throw new IllegalArgumentException("GERI unit ID must be configured before codastation connects.");
		setID(id);
		
		Img image = sink.getConnectedSource().getImage(0);
		if(image == null) { 
			log.write("No images defined. Passing configured 'expected' resolution to codastation");
		}else {
			//otherwise check it
				
			if(!(image instanceof ByteBufferImage))
				throw new IllegalArgumentException("Images must be simple ByteBufferImage type");
		
			ByteBufferImage bbImg = (ByteBufferImage)image;
		
			if(bbImg.getWidth() != cfg.imageWidth || bbImg.getHeight() != cfg.imageHeight) {
				if(cfg.checkImageType)
					throw new RuntimeException("Configured image width/height mismatches expected (and this is configured to fail).");
				
				log.write("Configured image width/height mismatches expected. Resetting.");
				cfg.imageWidth = bbImg.getWidth();
				cfg.imageHeight = bbImg.getHeight();
			}
			
			//if(bbImg.getBitDepth() != 16)
			//	throw new RuntimeException("Currently only supports 16-bit images");
			
		}
		
		//if(cfg.imageType != Types.int16)
		//	throw new RuntimeException("Currently only supports 16-bit signed images, not " + cfg.imageType.toString());
		
		logr.info("ImageProcImageProvider init for image " + cfg.imageWidth + " x " + cfg.imageHeight + " x " + cfg.imageType.format());
		
		
		setImageParameters(cfg.imageWidth, cfg.imageHeight, cfg.imageType);
		
	}
	
	/** Called when the image set is changed, (reallocated etc) */
	public void imageSetChanged() {
		
		ImgSource src = sink.getConnectedSource();		
		int n = src.getNumImages();
		
		synchronized (imageHistory) {
			imageHistory.clear();		
						
			ImagePass pass0 = new ImagePass(n);
			imageHistory.add(pass0);
			
			nextImageToFill = 0;
			nextToStream = 0;
			timestampTimeoutStart = -1;
			firstImageNanotime = -1;
		}
		sink.updateAllControllers();
	}

	@Override
	protected boolean queueIsEmpty() { return false; } //we have to check, there is no queue per-se
		
	@Override
	/** Try to process the next image(s)
	 * @return True if anything was done. */
	protected boolean process() {
		lastProcessCall = System.currentTimeMillis();
		//hack sim
		/*numSamples = 1;
		dataType = new Types.Image(Types.int16, sink.getConnectedSource().getImage(0).getWidth(), sink.getConnectedSource().getImage(0).getHeight());
		*/
		logr.log(Level.FINEST, "process()");
		try {
			GeriStreamConfig cfg = sink.getConfig();
			ImgSource src = sink.getConnectedSource();
			
			long imageSize = cfg.imageWidth * cfg.imageHeight * cfg.imageType.size();
			double fps = cfg.bandwidthThrottleMBPs / (imageSize * numSamples / 1e6);
			long throttleDelayMS = (long)(1000.0 / fps);
			
			if((System.currentTimeMillis() - lastTransmit) < throttleDelayMS) {
				logr.log(Level.FINEST, "Exit due to throttling");
				return false;
			}
			
			int nImagesInMulti = cfg.imagesPerTransmit;
			if(nImagesInMulti > numSamples) {
				logr.warning("imagesPerTransmit is greater than that accepted by codastation (" + cfg.imagesPerTransmit + " > " + numSamples + ". Reducing to max.");
				nImagesInMulti = numSamples;
			}
			
			ByteBuffer multiBuf = null;
			ByteBuffer timestampBuf = null;
			LongBuffer timestampLongBuf = null;
			int nImagesInBuffer = 0;
					
			//which image and pass index go into the buffer, so we can set the statuses properly after it's sent (or not)
			int[] imageIdxInBuff = new int[nImagesInMulti];
			int[] passIdxInBuff = new int[nImagesInMulti];
			
			int nImgs = sink.getConnectedSource().getNumImages();
			if(nImgs <= 0)
				throw new RuntimeException("No images in set. Can't start streaming loop.");
			
			while(nImagesInBuffer < nImagesInMulti) { //this loop is only collecting n in buffer, we don't wait in process()!!
				if(nextToStream >= nImgs)
					nextToStream = 0;
				
				if(connection == null || !connection.is_open()) {
					logr.warning("Connection closed, ditching packet of " + nImagesInBuffer + " images");
					break; //hack sim (comment out)
				}
				if(!isStreamingActive()) {
					logr.warning("Streaming inactive, ditching packet of " + nImagesInBuffer + " images");
					break;
					
				}
				
				ByteBufferBackedImage image = (ByteBufferBackedImage)src.getImage(nextToStream);
				
				if(image == null) { //skip really null images, whatever that means
					logr.log(Level.FINE, "Image " + nextToStream + " is null.");
					nextToStream++;
					continue;
				}				
				
				if(multiBuf == null) { //allocate buffer for combined/reversed data		
					timestampBuf = ByteBuffer.allocate(nImagesInMulti * 8);
					timestampLongBuf = timestampBuf.asLongBuffer();
					multiBuf = ByteBuffer.allocate(nImagesInMulti * getType(0).size());				
				}	
				
				
				ReadLock readLock = null;
				long timestamp = -1;
				int passIdx = -1;
				try { //readlock
					
					ImagePass pass;
					
					//find the last status array with something for this image
					synchronized (imageHistory) { //Don't stop here! imageChangedeLowLevel() also sync's on this
						for(passIdx = imageHistory.size()-1; passIdx >= 0; passIdx--) {
							if(imageHistory.get(passIdx).status[nextToStream] != ImageStatus.UNKNOWN)
								break;
						}
					}
					
					// if no image yet (at all), give up
					if(passIdx < 0) 
						break;  //don't advance, we'll wait here for an image
					
					pass = imageHistory.get(passIdx);
					
					//the last entered image at this slot was already sent
					if((pass.status[nextToStream] == ImageStatus.COPIED 
							|| pass.status[nextToStream] == ImageStatus.SENT 
							|| pass.status[nextToStream] == ImageStatus.CONFIRMED)) {
						break; //don't advance
					}

					//see if there is a valid timestamp for this image	
					timestamp = getTimestamp(nextToStream, pass.status);					
					if(timestamp < sink.getPilot().getLastPreTransitionTimestamp()) {
						//timestamp is before the GERI PRE, so we can't sent it
						pass.status[nextToStream] = ImageStatus.NO_TIMESTAMP;
						
						logr.warning("Timestamp for image " + nextToStream + " is " + ((double)(sink.getPilot().getLastPreTransitionTimestamp() - timestamp))/1e9 + " sec  before GERI PRE, so can not be sent, advancing");
						//and advance (if the buffer is not empty)
						if(nextToStream != nextImageToFill)
							nextToStream++;
						break;
						
						
					} else if(timestamp < 0) { //timestamp is not there yet
						//we also should never send images from before the GERI state change to PRE 
						
						pass.status[nextToStream] = ImageStatus.NO_TIMESTAMP;
						
						if(timestampTimeoutStart > 0) {
							if((System.currentTimeMillis() - timestampTimeoutStart) > cfg.timestampWaitTimeout) {
								logr.warning("Timed out waiting for timestamp for image " + nextToStream + ", advancing");
								timestampTimeoutStart = -1; //clear the timeout
								//and advance (if the buffer is not empty)
								if(nextToStream != nextImageToFill)
									nextToStream++;
								break; 
							}
							
						}else {
							//begin the timeout counting for this image's timestamp
							timestampTimeoutStart = System.currentTimeMillis();
						}
						
						break; //don't advance, we'll come back to this image
					}

					//mark that we are sending this image, so we don't try to send it again
					pass.status[nextToStream] = ImageStatus.COPIED;				
					
					timestampTimeoutStart = -1;
					
					pass.timestamp[nextToStream] = timestamp;
					
					//grab a read image lock on this image to stop it being overwritten while we copy it
					readLock = image.readLock();					
					try {
						readLock.lockInterruptibly();
					} catch (InterruptedException e) {
						logr.warning("Interrupted waiting for read lock on image " + nextToStream + " for streaming. Streaming anyway.");
					}
					
					//copy the image and the timestamp
					copyImage(multiBuf, image, readLock);
	
					//timestampBuf.order(ByteOrder.LITTLE_ENDIAN);
					timestampLongBuf.put(timestamp);
	
					imageIdxInBuff[nImagesInBuffer] = nextToStream;
					passIdxInBuff[nImagesInBuffer] = passIdx;				
					
					logr.log(Level.INFO, "Added image " +nextToStream + " pass " + passIdx + ", timestamp = " + timestamp + " to multi-image send buffer[" + nImagesInBuffer + "]");
					
					nImagesInBuffer++;
					
					if(nextToStream != nextImageToFill)
						nextToStream++;
					
				}finally {
					synchronized (imageHistory) {
											
						//and then give up it's read lock before unsyncing from imageStatus
						if(readLock != null)
							readLock.unlock();
					}
				}
				
				
				
			} //while nImagesInBuffer < nImagesInMulti
			
			if(nImagesInBuffer <= 0) {
				//logr.warning("No valid images to send");
				
				
				//check to see if we're in principle done and can stop streaming
				if(cfg.stopOnNoImageWhenAcquisitionStopped 
								&& nextToStream == nextImageToFill 
								&& src instanceof AcquisitionDevice) {
					Status state = ((AcquisitionDevice)src).getAcquisitionStatus();
					switch(state) {
						case notInitied:
						case completeOK:
						case aborted:
						case errored: 
							//all states where no new images are going to appear, we can stop
							logr.info("AcquisitionDevice not running (" + state + ") and next image hasn't change. Stopping streaming.");
							stopStreaming(false);
							break;
						default:
					}
				
				}
				
				return false;	
			}
			
			//we have a buffer of images to send
			
			multiBuf.rewind();
			multiBuf.limit(nImagesInBuffer * (int)imageSize);
			timestampBuf.rewind();
			timestampBuf.limit(nImagesInBuffer * 8);
			QueueEntry entry = new QueueEntry("multi-i0_" + imageIdxInBuff[0] + "-n_" + nImagesInBuffer, nImagesInBuffer, timestampBuf, multiBuf);
			
			sendEntry(entry); //hack sim (comment out)
			imageSent(entry.id); 
			
			lastTransmit = System.currentTimeMillis();		
			lastTransmitNanotime = timestampLongBuf.get(nImagesInBuffer-1);
			if(firstImageNanotime < 0) {
				firstImageNanotime = timestampLongBuf.get(0);
			}
			
			imagesSinceLastFPSUpdate++;
			if((System.currentTimeMillis() - lastFPSUpdate) > fpsUpdatePeriod) {
				
				lastFPS = 1000.0 * imagesSinceLastFPSUpdate / fpsUpdatePeriod;
				imagesSinceLastFPSUpdate = 0;
				lastFPSUpdate = lastTransmit;
			}
			
			int passIdx = -1;
			ImagePass passN = null;
			
			//update the image statuses for those that we sent
			synchronized (imageHistory) {
				for(int i=0; i < nImagesInBuffer; i++) {
					int imgIdx = imageIdxInBuff[i];
					if(passIdx != passIdxInBuff[i]) {
						passIdx = passIdxInBuff[i];
						while(passIdx >= imageHistory.size())
							imageHistory.add(new ImagePass(nImgs));
						
						passN = imageHistory.get(passIdx);
						logr.log(Level.FINE, "Marking image " + imgIdx + " with timestamp " + passN.timestamp[i] + " in pass " + passIdx + " as sent.");
					}
					passN.status[imgIdx] = ImageStatus.SENT;				
				}
			}
			
			long t0 = timestampLongBuf.get(0);
			long t1 = timestampLongBuf.get(nImagesInBuffer-1);
			logr.log(Level.INFO, "Transmitted " + nImagesInBuffer + " images, timestamps " + t0 + " - " + t1);
			
			sink.updateAllControllers();
			
			return true;	
		}catch(Throwable t) {
			logr.log(Level.SEVERE, "Pass-through exception in streaming.process()", t);
			sink.setStatus("ERROR: " + t.toString());			
			throw t;
		}
	}
	
	/** Build a detailed description of the current streaming status */
	public String getStreamingStatus() {
		StringBuffer statusStr = new StringBuffer(128);
		
		statusStr.append(this.getClass().getName() + "\n");
		if(connection == null) {
			statusStr.append("  Connection: null\n");
		}else {
			statusStr.append("  Connection: " + connection.toString() + ", open = " + connection.is_open() + ", writing: " + connection.isInWrite() + "\n");
		}
		
		if(runner == null) {
			statusStr.append("  Runner: null\n");
		}else {
			statusStr.append("  Runner: " + runner.toString() + ", death = " + runner.death 
								+ ", thread = " + (runner.thread == null ? "null\n" : (runner.thread.getName() + ": alive = " + runner.thread.isAlive() + "\n")));
		}
		
		statusStr.append("Last process() call: " + (lastProcessCall < 0 ? "never" : (System.currentTimeMillis() - lastProcessCall)) + "\n");
		
		statusStr.append("nextImageToFill: " + nextImageToFill + ", nextToStream: " + nextToStream + "\n");
		
		
		
		return statusStr.toString();
	}
	
	private void copyImage(ByteBuffer multiBuf, ByteBufferBackedImage image, ReadLock readLock) {
		GeriStreamConfig cfg = sink.getConfig();
		
		ByteBuffer imageBuf = image.getReadOnlyBuffer();
		imageBuf.rewind();
		imageBuf.limit(getType(0).size());		
		
		if(cfg.convertDoubleToShort) {
			//Hack to convert doubles to shorts for OceanOptics devices that 
			//   return integer values in doubles (for some reason)
			
			int bytesPerPixelOut = getType(0).item_size();
			imageBuf.rewind();			
			imageBuf.limit(image.getWidth() * image.getHeight() * Double.BYTES);
			imageBuf.order(ByteOrder.LITTLE_ENDIAN);
			DoubleBuffer dblImageBuffer = imageBuf.asDoubleBuffer();
			
			if(bytesPerPixelOut == 2) {
				multiBuf.order(ByteOrder.BIG_ENDIAN);
				//ShortBuffer shortMultiBuf = multiBuf.asShortBuffer();
				while(dblImageBuffer.remaining() > 0) {
					//shortMultiBuf.put((short)dblImageBuffer.get());
					multiBuf.putShort((short)dblImageBuffer.get());
				}
				
			}else {
				throw new RuntimeException("Unsupported target type/width in conversion of doubles for streaming");
			}
		}else if(cfg.swapEndianess) {
			//reverse it here (into the multi image buffer) 
			reverseBuffer(imageBuf, cfg.imageType.item_size(), multiBuf);
		}else {
			//or copy if not reversing
			multiBuf.put(imageBuf);
		}
	}

	private long getTimestamp(int idx, ImageStatus[] imageStatusN) {
				
		Object o = sink.getConnectedSource().getImageMetaData(sink.getConfig().timebase, idx);
		if(o == null) {
			logr.warning("No timestamp for image " + idx);
			imageStatusN[idx] = ImageStatus.NO_TIMESTAMP;
			return -1;
		}
		
		long timestamp = ((Number)o).longValue();
		if(timestamp <= 0) {
			logr.warning("Timestamp of image " + idx + " is <= 0");
			imageStatusN[idx] = ImageStatus.NO_TIMESTAMP;
			return -1;
		}
		
		if(timestamp <= lastTransmitNanotime) {
			//something broke, we're sending out of order images
			logr.warning("Image " + idx + " wasn't sent yet but its timestamp " + timestamp +" is <= last timestamp " + lastTransmitNanotime + ", nudging it by 1.");
			timestamp += 1;
			imageStatusN[idx] = ImageStatus.ERROR;
			return timestamp;
		}
		
		return timestamp;
	}

	public static ByteBuffer reverseBuffer(ByteBuffer data, int nBytesToReverse, ByteBuffer outBuf) {
		if(outBuf == null)
			outBuf = ByteBuffer.allocate(data.limit());
		
		int i=0;
		data.rewind();
		
		//long winded but faster
		if(nBytesToReverse == 2) {
			ByteOrder newOrder = data.order() == ByteOrder.LITTLE_ENDIAN ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN;
			
			ShortBuffer inBuf = data.asShortBuffer();
			outBuf.order(newOrder);
			ShortBuffer typBuf = outBuf.asShortBuffer();
						
			typBuf.put(inBuf);
			outBuf.position(outBuf.position() + 2 * typBuf.position()); //advance underlying buffer too
			//outBuf.rewind();
			return outBuf;
			
		}else if(nBytesToReverse == 4) {
			ByteOrder newOrder = data.order() == ByteOrder.LITTLE_ENDIAN ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN;
			
			IntBuffer inBuf = data.asIntBuffer();
			outBuf.order(newOrder);
			IntBuffer typBuf = outBuf.asIntBuffer();
						
			typBuf.put(inBuf);
			
			outBuf.position(outBuf.position() + 2 * typBuf.position()); //advance underlying buffer too
			//outBuf.rewind();
			return outBuf;
			
		}
		
		//general, but slower
		while(i < data.limit()) {
			for(int j=0; j < nBytesToReverse; j++) {
				int iGet = i + nBytesToReverse - j - 1;
				if(iGet < 0 || iGet >= data.limit())
					System.out.println("Buffer limits failure " + data.capacity() + ", " + data.limit());
				if(data.remaining() < 1)
					System.out.println("No data!");
				byte b = data.get(iGet);
				outBuf.put(b);
			}

			i += nBytesToReverse;
			if(Thread.interrupted())
				throw new RuntimeException("Interrupted in streaming data");
		}

		outBuf.rewind();
		return outBuf;
	}
	
	public enum ImageStatus {
		/** not known state, probably uninitialised memory */
		UNKNOWN, 
		
		/** image is null */
		NULL, 
		
		/**  new image - change notified, with valid ranges */
		NORANGE,
		
		/**  new image - change notified, with valid ranges */
		CHANGED,  		
		
		/**  image present and valid, but has no timestamp, so we can't send it */
		NO_TIMESTAMP, 
		
		/**  image has been copied into the send buffer, but not yet sent */
		COPIED, 
		
		/**  image has been sent to codastation (socket was still open) */
		SENT, 
		
		/**  image has been confirmed as existing in the archive */
		CONFIRMED,

		/** The image was filled with new data before the old got streamed, so it was lost. */
		LOST, 
		
		/** something went wrong */
		ERROR 
	}
	
	public ArrayList<ImagePass> getImageHistory() {
		return imageHistory;
	}
	
	/*private void deadCode() {
			
			GeriStreamConfig cfg = sink.getConfig();
			long t[] = null;
			if(cfg.timebase != null) {
				Object o = Mat.fixWeirdContainers(sink.getSeriesMetaData(cfg.timebase), true);
				if(o instanceof long[] && Array.getLength(o) == n)
					t = (long[])o;
			}
			
			for(int i=0; i < n; i++) {
				Img img = src.getImage(i);
				if(img == null){
					imageStatus[i] = ImageStatus.NULL;
					continue;
					
				}else  if(!img.isRangeValid()) {
					imageStatus[i] = ImageStatus.NORANGE;
					continue;					
				}
				
				int changeID = img.getChangeID();
				if(changeID != imageStatusChangeID[i]){ 
					if(t == null || t[i] <= 0)
						imageStatus[i] = ImageStatus.NO_TIMESTAMP;
					else
						imageStatus[i] = ImageStatus.CHANGED;
				}
			}
			
			return imageStatus.clone();
		}
		
	}	*/
	
	public void imageChangedLowLevel(int idx) {
		//This function needs to be fast, so should only be accounting and variables, no copying!
		synchronized (imageHistory) { //so this implies anything with synchronized(imageHistory) should also be fast
			if(imageHistory.size() == 0)
				return; //all is lost, give up
			
			//what pass are we currently on
			int passIdx = imageHistory.size()-1;
			
			int nImgs = sink.getConnectedSource().getNumImages();
			
			ImagePass pass = imageHistory.get(passIdx);

			//if this image in the last pass was already filled (this shouldn't happen)
			if(pass.status[idx] != ImageStatus.UNKNOWN) {
				pass = new ImagePass(nImgs);
				imageHistory.add(pass);
				logr.warning("Image " + idx + " already in pass " + (passIdx-1) + ", so moving to new pass " + passIdx);
			}
			
			//did an image get overwritten that had not been sent yet?
			if(passIdx > 0) {
				ImagePass prevPass = imageHistory.get(passIdx-1);
				if(prevPass.status[idx] == ImageStatus.CHANGED || prevPass.status[idx] == ImageStatus.NO_TIMESTAMP
						|| prevPass.status[idx] == ImageStatus.NORANGE) {
					prevPass.status[idx] = ImageStatus.LOST;
				}	
			}
			
			//mark the new level of this image as changed
			pass.status[idx] = ImageStatus.CHANGED;
			Img img = sink.getConnectedSource().getImage(idx);
			pass.changeID[idx] = (img == null) ? 0 : img.getChangeID();
			pass.timestamp[idx] = -1;
			
			//sanity check that the image was really changed compared to the same image in the last pass
			if(passIdx > 0 && pass.changeID[idx] !=  imageHistory.get(passIdx-1).changeID[idx]) {
				logr.warning("Pass " + passIdx + " of image " + idx + " has changeID " + pass.changeID[idx] + " which is the same as for pass " + (passIdx-1));
			}
			
			//and note where we expect the next to be 
			nextImageToFill = idx+1;
			//if the next image will go back to the start, we move to the next pass (this should happen, rather than the upper if block)
			if(nextImageToFill >= nImgs) {
				nextImageToFill = 0;
				passIdx++;				
				pass = new ImagePass(nImgs);
				imageHistory.add(pass);
				logr.fine("Wrapped image index in pass " + (passIdx-1) + ", so moving to new pass " + passIdx);
			}
			
			//will the next recieved image overrun?
			if(nextImageToFill == nextToStream) {
				int nAdvance = sink.getConfig().streamAdvanceOnOverrun;
				logr.warning("Next image (" + idx + ") to change will likely overrun the streaming. Nudging the streaming on by " + nAdvance);
				nextToStream += nAdvance;
				if(nextToStream >= nImgs) //wrap
					nextToStream = 0;
			}

			
		}
				
		sink.updateAllControllers();
	}

	public void resetImageNumber() {
		imageSetChanged();		
	}

	public double getCurrentTransmitRate() { return lastFPS;  }
	
	@Override
	protected void streamEnding() {
		super.streamEnding();
		if(sink == null) //sometimes we get called too early in another thread
			return;
		
		logr.info("Stream connection ended");

		GeriStreamConfig cfg = sink.getConfig();
		
		//queue alias update
		if(cfg.updateAlias)
			sink.updateAlias();		
		
		sink.updateAllControllers();
	}
	
	@Override
	protected void streamStarting() {
		logr.fine("Streaming thread started");
		sink.updateAllControllers();
	}
	
	public long[] getNanotime() {
		GeriStreamConfig cfg = sink.getConfig();
		
		Object o = sink.getConnectedSource().getSeriesMetaData(cfg.timebase);
		if(o == null)
			return null;
		
		o = Mat.fixWeirdContainers(o, true);
		if(!(o instanceof long[])) {
			logr.warning("Timestamps " + cfg.timebase + " do not reduce to long[]");
			return null;
		}
		
		return (long[])o;
	}

	public void confirmImages(long[][] tNanos) {
		synchronized (imageHistory) {
			for(int passIdx=0; passIdx < imageHistory.size(); passIdx++) {
				ImagePass pass = imageHistory.get(passIdx); 
						
		        for(int i=0; i < pass.status.length; i++) {
		        	if(pass.status[i] != ImageStatus.SENT)
		        		continue;
		        	
		        	long nanotime = pass.timestamp[i];
		        	
		        	for(int j=0; j < tNanos[0].length; j++) {		        		
		        		if(nanotime >= tNanos[0][j] && nanotime <= tNanos[1][j]) {
		        			pass.status[i] = ImageStatus.CONFIRMED;
		        			break;
		        		}
		        	}
		        }	
			}
		}		
	}

	public long getLatestNanotime() { return lastTransmitNanotime; }
	public long getEarliestNanotime() { return firstImageNanotime; }

}
