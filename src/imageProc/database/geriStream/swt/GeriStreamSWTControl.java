package imageProc.database.geriStream.swt;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import geri.Types;
import geri.Types.Scalar;
import geri.Types.Type;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.geriStream.GeriStreamConfig;
import imageProc.database.geriStream.GeriStreamSink;
import imageProc.database.geriStream.ImageProcImageProvider.ImagePass;
import imageProc.database.geriStream.ImageProcImageProvider.ImageStatus;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.database.w7xArchiveDB.W7XArchiveDBPipe;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSink;
import jakarta.xml.ws.Provider;

public class GeriStreamSWTControl  implements ImagePipeController {
	private GeriStreamSink sink;
	private Group swtGroup;
	private Label swtStatusLabel;
	
	private Spinner imageStreamIDSpinner;
	private Spinner parlogStreamIDSpinner;
	
	private Spinner timestampTimeoutSpinner;
	private Button stopOnAcquireStopCheckbox;
	
	private Spinner imageWidthSpinner;
	private Spinner imageHeightSpinner;
	private Combo imageTypeCombo;
	private Button swapEndiannessCheckbox;
	private Button hackInt16Checkbox;
	private Button setImageParamsButton;
	private Button imageTypeCheckCheckbox;	
	private Canvas imageStatusCanvas;
	
	private Spinner imagesPerTransmitSpinner;
	private Label maxImagesPerTransmitLabel;
	private Spinner bandwidthThrottleSpinner;
	private Spinner imagePerSecondSpinner;
	private Label framePerSecondLabel;
	
	private Spinner skipOnOverrunSpinner;
		
	private Combo swtTimeMetadata;
		
	private SWTSettingsControl settingsCtrl;
	private boolean updateInhibit = false;
	
	private Button dataStreamActiveCheckbox;
	private Button parlogStreamActiveCheckbox;
	private Button startStreamingButton;
	private Button stopStreamingButton;
	private Button interruptStreamingButton;
	
	private Button checkArchiveCheckbox;
	private Text archiveStreamTextbox;
	private Button checkNowButton;
	private Button intervalsOnlyCheckbox;
	
	private Button swtWriteAliasCheckbox;
	private Combo swtAliasCombo;
	private Button writeAliasNowButton;
	
	
	private Scalar[] validTypes = new Scalar[] { 
			//Types.uint8, Types.uint16, Types.uint32, Types.uint64,
			Types.int8, Types.int16, Types.int32, Types.int64,
			Types.float32, Types.float64
	};
	
	public GeriStreamSWTControl(Composite parent, Integer style, GeriStreamSink sink) {
		this.sink = sink;
		
		swtGroup = new Group(parent, SWT.BORDER);
		swtGroup.setLayout(new GridLayout(8, false));
		swtGroup.setText("GERI Stream");
		
		Label lS = new Label(swtGroup, 0); lS.setText("Status:");
		swtStatusLabel = new Label(swtGroup, SWT.NONE);
		swtStatusLabel.setText("init\n.\n.\n.");
		swtStatusLabel.setToolTipText("Save status of images, of time series meta data (per image) and of per-series metadata");
		swtStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 7, 1));
		
		Label lSS = new Label(swtGroup, 0); lSS.setText("Streaming:");
		imageStatusCanvas = new Canvas(swtGroup, SWT.NO_BACKGROUND | SWT.BORDER);		
		imageStatusCanvas.addPaintListener(new PaintListener() { @Override public void paintControl(PaintEvent e) { canvasPaintEvent(e); }});
		imageStatusCanvas.setToolTipText("Status of images in streaming: white=invalid, magenta=image but no timestamp, yellow=ready, blue=sent, green=confirmed, gray=unknown. If the acqusition wraps, the lowest row(s) shows the current status of that image, and the higher rows the historical status of images that were acquired, maybe streamed and later overwritten.");
		imageStatusCanvas.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 7, 1));
		
		Label lB1 = new Label(swtGroup, 0); lB1.setText("");
		startStreamingButton = new Button(swtGroup, SWT.PUSH);
		startStreamingButton.setText("Start");
		startStreamingButton.setToolTipText("Try to start the streaming thread. This is normally automatically activated by the GeriPilot's automation module.");
		startStreamingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sink.startStreaming(); } });
		startStreamingButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		
		stopStreamingButton = new Button(swtGroup, SWT.PUSH);
		stopStreamingButton.setText("Stop");
		stopStreamingButton.setToolTipText("Stop the streaming thread. This is normally automatically done by the GeriPilot's automation module.");
		stopStreamingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sink.stopStreaming(false); } });
		stopStreamingButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		interruptStreamingButton = new Button(swtGroup, SWT.PUSH);
		interruptStreamingButton.setText("Interrupt");
		interruptStreamingButton.setToolTipText("Interrput the thread currently stuck in the socket write. If the streaming is currently sending, this will abort and codastation will probably close the connection.");
		interruptStreamingButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sink.stopStreaming(true); } });
		interruptStreamingButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		
		stopOnAcquireStopCheckbox = new Button(swtGroup, SWT.CHECK);
		stopOnAcquireStopCheckbox.setText("Stop when done.");
		stopOnAcquireStopCheckbox.setToolTipText("Stop the streaming thread when there are no more images to stream and the acquisition thread is done");
		stopOnAcquireStopCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		stopOnAcquireStopCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		
		
		Label lU = new Label(swtGroup, SWT.NONE); lU.setText("Stream unit IDs:");
		Label lUD = new Label(swtGroup, SWT.NONE); lUD.setText("Images UID:");
		imageStreamIDSpinner = new Spinner(swtGroup, SWT.NONE);
		imageStreamIDSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		imageStreamIDSpinner.setToolTipText("GERI unit ID to stream images to. Must match the codastation XML config");
		imageStreamIDSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		imageStreamIDSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		
		dataStreamActiveCheckbox = new Button(swtGroup, SWT.CHECK);
		dataStreamActiveCheckbox.setEnabled(false);
		dataStreamActiveCheckbox.setText("Active");
		dataStreamActiveCheckbox.setToolTipText("Streaming thread is active and queued images are send to the codastation. This is normally automatically activated by the GeriPilot's automation module.");
		dataStreamActiveCheckbox.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { sink.updateAllControllers(); } });
		dataStreamActiveCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		
		
		Label lB2 = new Label(swtGroup, 0); lB2.setText("");
		Label lUP = new Label(swtGroup, SWT.NONE); lUP.setText("ParLog UID:");
		parlogStreamIDSpinner = new Spinner(swtGroup, SWT.NONE);
		parlogStreamIDSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		parlogStreamIDSpinner.setToolTipText("GERI unit ID to stream parlog to. Must match the codastation XML config");
		parlogStreamIDSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		parlogStreamIDSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));

		parlogStreamActiveCheckbox = new Button(swtGroup, SWT.CHECK);
		parlogStreamActiveCheckbox.setEnabled(false);
		parlogStreamActiveCheckbox.setText("Active");
		parlogStreamActiveCheckbox.setToolTipText("Streaming thread is active and queued images are send to the codastation. This is normally automatically activated by the GeriPilot's automation module.");
		parlogStreamActiveCheckbox.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { sink.updateAllControllers(); } });
		parlogStreamActiveCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 3, 1));
		
		Group imageFormatGroup = new Group(swtGroup, SWT.BORDER);
		imageFormatGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 8, 1));
		imageFormatGroup.setLayout(new GridLayout(5, false));
		imageFormatGroup.setText("Expected image format");
		
		Label lIW = new Label(imageFormatGroup, SWT.NONE); lIW.setText("Width:");		
		imageWidthSpinner = new Spinner(imageFormatGroup, SWT.NONE);
		imageWidthSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		imageWidthSpinner.setToolTipText("Expected image width. Used if codastation connects before first acquisition.");
		imageWidthSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		imageWidthSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));

		Label lIH = new Label(imageFormatGroup, SWT.NONE); lIH.setText("Height:");		
		imageHeightSpinner = new Spinner(imageFormatGroup, SWT.NONE);
		imageHeightSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		imageHeightSpinner.setToolTipText("Expected image height. Used if codastation connects before first acquisition.");
		imageHeightSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		imageHeightSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
						
		Label lIT = new Label(imageFormatGroup, SWT.NONE); lIT.setText("Type:");		
		imageTypeCombo = new Combo(imageFormatGroup, SWT.NONE);
		for(Scalar t : validTypes)
			imageTypeCombo.add(t.shortFormat());		
		imageTypeCombo.setToolTipText("Expected image type. Used if codastation connects before first acquisition. These are signed types regardless of whether actual data "
				+ "is signed or unsigned, because Java can't do unsigned, so we just give the bits to the codastation anyway. Probably they end up in the archive the same anyway.");
		imageTypeCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		imageTypeCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
						
		setImageParamsButton = new Button(imageFormatGroup, SWT.NONE);
		setImageParamsButton.setText("Set to current");
		setImageParamsButton.setToolTipText("Set the width, height and datatype in this module to those of the current image set.");
		setImageParamsButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sink.setImageParamsFromSource(); } });
		setImageParamsButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, true, 3, 1));
		
		imageTypeCheckCheckbox = new Button(imageFormatGroup, SWT.CHECK);
		imageTypeCheckCheckbox.setText("Check if images match");
		imageTypeCheckCheckbox.setToolTipText("Checks whether or not this information matches the images before streaming. If not, an exception is thrown (which might cause the automation to write the data via W7X-Save)");
		imageTypeCheckCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		imageTypeCheckCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, true, 1, 1));
		
		swapEndiannessCheckbox = new Button(imageFormatGroup, SWT.CHECK);
		swapEndiannessCheckbox.setText("Swap endianess");
		swapEndiannessCheckbox.setToolTipText("Swap endianess of bytes in image type length. e.g. swap every 2 bytes if an int32");
		swapEndiannessCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swapEndiannessCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));

		hackInt16Checkbox = new Button(imageFormatGroup, SWT.CHECK);
		hackInt16Checkbox.setText("Force convert to int16");
		hackInt16Checkbox.setToolTipText("Convert floating point images to int16. Used for OceanOptics USB devices where the library gives doubles, but the data is actually just integers.");
		hackInt16Checkbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		hackInt16Checkbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));

		
		
		//Label lTX = new Label(swtGroup, SWT.NONE); lTX.setText("Transmission:");		
		Group transmitGroup = new Group(swtGroup, SWT.BORDER);
		transmitGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 8, 1));
		transmitGroup.setLayout(new GridLayout(6, false));
		transmitGroup.setText("Transmission settings");
		
		
		Label lIpT = new Label(transmitGroup, 0); lIpT.setText("Image per transmit:");		
		imagesPerTransmitSpinner = new Spinner(transmitGroup, SWT.NONE);
		imagesPerTransmitSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 10, 100);
		imagesPerTransmitSpinner.setToolTipText("Number of images to send in one tranmission chunk/packet to the codastation (limited by codastation max num. samples).");
		imagesPerTransmitSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		imagesPerTransmitSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		maxImagesPerTransmitLabel = new Label(transmitGroup, 0); 
		maxImagesPerTransmitLabel.setText("(max accepted by codastation = ???)");		
		maxImagesPerTransmitLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		
		Label lTB = new Label(transmitGroup, 0); lTB.setText("Bandwidth:");
		bandwidthThrottleSpinner = new Spinner(transmitGroup, SWT.NONE);
		bandwidthThrottleSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 100, 1000);
		bandwidthThrottleSpinner.setToolTipText("Bandwidth limit in kilobytes per second.");
		bandwidthThrottleSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { bandwidthSpinnerEvent(event); } });
		bandwidthThrottleSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		Label lBW = new Label(transmitGroup, SWT.NONE); lBW.setText("kBps, Rate:");				
		imagePerSecondSpinner = new Spinner(transmitGroup, SWT.NONE);
		imagePerSecondSpinner.setValues(-1, -1, Integer.MAX_VALUE, 3, 100, 1000);
		imagePerSecondSpinner.setToolTipText("Bandwidth limit expressed as frames per second.");
		imagePerSecondSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { fpsSpinnerEvent(event); } });
		imagePerSecondSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		//Label lFPS = new Label(transmitGroup, SWT.NONE); 
		//lFPS.setText("fps.");
		//lFPS.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		
		//Label lCFPS = new Label(transmitGroup, SWT.NONE); lCFPS.setText("Current:");
		framePerSecondLabel = new Label(transmitGroup, SWT.NONE); 
		framePerSecondLabel.setText("fps. Current = ???.???????? fps");		
		framePerSecondLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		
		Label lSO = new Label(transmitGroup, SWT.NONE); lSO.setText("Overrun skip:");	
		skipOnOverrunSpinner = new Spinner(transmitGroup, SWT.NONE);
		skipOnOverrunSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 10, 100);
		skipOnOverrunSpinner.setToolTipText("Number of images to skip streaming forward if the changed (i.e. acquired) images overrun the current streaming position.");
		skipOnOverrunSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		skipOnOverrunSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		
		Label lTS = new Label(swtGroup, 0); lTS.setText("Timestamps:");		
		swtTimeMetadata = new Combo(swtGroup, SWT.DROP_DOWN);
		swtTimeMetadata.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 7, 1));
		swtTimeMetadata.setToolTipText("The metadata entry to be used as timestamps. Must be an array of longs");
		swtTimeMetadata.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lTT = new Label(swtGroup, SWT.NONE); lTT.setText("Timestamp timeout [ms]:");
		lTT.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		timestampTimeoutSpinner = new Spinner(swtGroup, SWT.NONE);
		timestampTimeoutSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 1, 10);
		timestampTimeoutSpinner.setToolTipText("How long to wait in ms on a given image that has no timestamp before moving to the next image.");
		timestampTimeoutSpinner.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		timestampTimeoutSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 6, 1));
		
				
		checkArchiveCheckbox = new Button(swtGroup, SWT.CHECK);
		checkArchiveCheckbox.setText("Check archive:");
		checkArchiveCheckbox.setToolTipText("Periodically check if the data has arrived in the archive.");
		checkArchiveCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		checkArchiveCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		archiveStreamTextbox = new Text(swtGroup, SWT.NONE);
		archiveStreamTextbox.setText("---");
		archiveStreamTextbox.setToolTipText("What we think the datastream name is. Not used for streaming but only to check if everything arrives in the archive and to "
											+ "point the alias at. The stream target is specified by the XML config file into the codastation, or maybe the configuration DB, who knows.");
		archiveStreamTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		archiveStreamTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 1));
		
		intervalsOnlyCheckbox = new Button(swtGroup, SWT.CHECK);
		intervalsOnlyCheckbox.setText("Intervals");
		intervalsOnlyCheckbox.setToolTipText("Request only intervals/boxes and not whole images (faster) and assume all images inside the detected intervals were written.");
		intervalsOnlyCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		intervalsOnlyCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		checkNowButton = new Button(swtGroup, SWT.PUSH);
		checkNowButton.setText("Force");
		checkNowButton.setToolTipText("Force the archive check now.");
		checkNowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { sink.checkDataInArchive(); } });
		checkNowButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		
		Label lAP = new Label(swtGroup, 0); lAP.setText("Alias:");
		swtAliasCombo = new Combo(swtGroup, SWT.None);
		swtAliasCombo.setToolTipText("Alias path to update to point to this stream for the image range being saved.");
		swtAliasCombo.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event);	 } });
		swtAliasCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		
		swtWriteAliasCheckbox = new Button(swtGroup, SWT.CHECK);
		swtWriteAliasCheckbox.setText("Set on stream end");
		swtWriteAliasCheckbox.setToolTipText("Update the given alias when streaming the images completes or aborts.");
		swtWriteAliasCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtWriteAliasCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		
		writeAliasNowButton = new Button(swtGroup, SWT.PUSH);
		writeAliasNowButton.setText("Set");
		writeAliasNowButton.setToolTipText("Update the given alias now for the current image set.");
		writeAliasNowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { writeAliasButtonEvent(event); } });
		writeAliasNowButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		Label lBlank = new Label(swtGroup, SWT.NONE); lBlank.setText("");
		lBlank.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 8, 1));
	
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtGroup, SWT.BORDER, sink);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 8, 1));
		
		generalControllerUpdate();
	}

	protected void writeAliasButtonEvent(Event event) {
		sink.updateAlias();
	}

	protected void fpsSpinnerEvent(Event event) {
		if(updateInhibit)
			return;
	
		updateInhibit = true;
		try {
			GeriStreamConfig config = sink.getConfig();
			int imageSize = config.imageWidth * config.imageHeight * config.imageType.size();
			double fps = imagePerSecondSpinner.getSelection() / 1000.0;
			
			config.bandwidthThrottleMBPs = fps * imageSize / 1e6;
			
		}finally {
			updateInhibit = false;
		}
		generalControllerUpdate();
	}

	protected void bandwidthSpinnerEvent(Event event) {
		if(updateInhibit)
			return;
	
		updateInhibit = true;
		try {
			
			GeriStreamConfig config = sink.getConfig();
			config.bandwidthThrottleMBPs = bandwidthThrottleSpinner.getSelection() / 1000.0;
			
		}finally {
			updateInhibit = false;
		}
		generalControllerUpdate();
	}

	protected void canvasPaintEvent(PaintEvent event) {
		Rectangle clientArea = imageStatusCanvas.getClientArea();
		ArrayList<ImagePass> imageHistory = sink.getImageHistory();
		
		int nPasses = imageHistory.size();
		
		for(int passIdx=0; passIdx < nPasses; passIdx++) {
			ImagePass pass = imageHistory.get(passIdx);
						
			double dy = clientArea.height / nPasses;
			int y0 = (int)Math.floor(passIdx*dy);
			
			if(pass.status == null){
				event.gc.setForeground(imageStatusCanvas.getDisplay().getSystemColor(SWT.COLOR_GRAY));
		        event.gc.fillRectangle(0, y0, clientArea.width, (int)Math.ceil(dy));
	        	return;
			}
			
	        for(int i=0; i < pass.status.length; i++) {
	        	int col = SWT.COLOR_BLACK;
	        	
	        	if(pass.status[i] != null) {
		        	switch(pass.status[i]) {
			        	case NULL: col = SWT.COLOR_RED; break;
			        	case NORANGE: col = SWT.COLOR_WHITE; break;
			        	case CHANGED: col = SWT.COLOR_YELLOW; break;
			        	case NO_TIMESTAMP: col = SWT.COLOR_MAGENTA; break;
			        	case SENT: col = SWT.COLOR_BLUE; break;
			        	case CONFIRMED: col = SWT.COLOR_GREEN; break;
			        	case LOST: col = SWT.COLOR_DARK_RED; break;
			        	case ERROR: col = SWT.COLOR_RED; break;
			        	case COPIED: col = SWT.COLOR_CYAN; break;
			        	default: col = SWT.COLOR_GRAY;
		        	}
	        	}
	        	event.gc.setBackground(imageStatusCanvas.getDisplay().getSystemColor(col));
		        
	        	double dx = (double)clientArea.width / pass.status.length;
	        	event.gc.fillRectangle((int)Math.floor(i*dx), y0, (int)Math.ceil(dx), (int)Math.ceil(dy));
	        }
		}
	}

	private void settingsChangedEvent(Event event) {
		if(updateInhibit)
			return;
		updateInhibit = true;
		try {
			GeriStreamConfig config = sink.getConfig();
			config.imageStreamID = imageStreamIDSpinner.getSelection();
			config.parlogStreamID = parlogStreamIDSpinner.getSelection();
			config.stopOnNoImageWhenAcquisitionStopped = stopOnAcquireStopCheckbox.getSelection();
			config.timestampWaitTimeout = timestampTimeoutSpinner.getSelection();
			config.imageWidth = imageWidthSpinner.getSelection();
			config.imageHeight = imageHeightSpinner.getSelection();
			config.swapEndianess = swapEndiannessCheckbox.getSelection();
			config.convertDoubleToShort = hackInt16Checkbox.getSelection();
			config.checkImageType = imageTypeCheckCheckbox.getSelection(); 
			config.imagesPerTransmit = imagesPerTransmitSpinner.getSelection();
			config.streamAdvanceOnOverrun = skipOnOverrunSpinner.getSelection();
			config.timebase = swtTimeMetadata.getText();		
			config.checkArchiveArrival = checkArchiveCheckbox.getSelection();
			config.checkArchiveIntervals = intervalsOnlyCheckbox.getSelection();		
			config.datastreamName = archiveStreamTextbox.getText();
			config.timebase = swtTimeMetadata.getText();			
			
			config.aliasPath = W7XArchiveDBPipe.pathFixup(swtAliasCombo.getText());
			config.updateAlias = swtWriteAliasCheckbox.getSelection();
			
			try {
				String tName = imageTypeCombo.getText();
				config.imageType = Types.int16;
				for(Scalar t : validTypes) {
					if(tName.equalsIgnoreCase(t.shortFormat())) {
						config.imageType = t;
					}	
				}			
			}catch(RuntimeException err) { 
				config.imageType = Types.int16; 
			}
		}finally {
			updateInhibit = false;
		}
		doUpdate();
	}

	@Override
	public Object getInterfacingObject() { return swtGroup; }

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}

	private void doUpdate(){
		if(updateInhibit)
			return;
		updateInhibit = true;
		try {
			GeriStreamConfig config = sink.getConfig();
					
			swtStatusLabel.setText(sink.getStatus() + (sink.isProviderConnected() ? " (connected)" : "(not connected"));
			imageStreamIDSpinner.setSelection(config.imageStreamID);
			parlogStreamIDSpinner.setSelection(config.parlogStreamID);

			stopOnAcquireStopCheckbox.setSelection(config.stopOnNoImageWhenAcquisitionStopped);
			timestampTimeoutSpinner.setSelection((int)config.timestampWaitTimeout);
			imageWidthSpinner.setSelection(config.imageWidth);
			imageHeightSpinner.setSelection(config.imageHeight);
			imageTypeCombo.setText(config.imageType.shortFormat());
			swapEndiannessCheckbox.setSelection(config.swapEndianess);
			hackInt16Checkbox.setSelection(config.convertDoubleToShort);
			imageTypeCheckCheckbox.setSelection(config.checkImageType);
			imagesPerTransmitSpinner.setSelection(config.imagesPerTransmit); 
			skipOnOverrunSpinner.setSelection(config.streamAdvanceOnOverrun);
			int nS = sink.getImageProvider().getNumSamples();
			maxImagesPerTransmitLabel.setText("(max accepted by codastation = " + nS + ")");
			if(nS == -1) 
				maxImagesPerTransmitLabel.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));				
			else if(nS < config.imagesPerTransmit)
				maxImagesPerTransmitLabel.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_RED));				
			else 
				maxImagesPerTransmitLabel.setForeground(swtGroup.getDisplay().getSystemColor(SWT.COLOR_WIDGET_FOREGROUND));
			
			dataStreamActiveCheckbox.setSelection(sink.isDataStreamingActive());
			dataStreamActiveCheckbox.setSelection(sink.isParlogStreamingActive());
			checkArchiveCheckbox.setSelection(config.checkArchiveArrival);
			intervalsOnlyCheckbox.setSelection(config.checkArchiveIntervals);
			if(!archiveStreamTextbox.isFocusControl())
				archiveStreamTextbox.setText(config.datastreamName);
			if(!swtTimeMetadata.isFocusControl())
				swtTimeMetadata.setText(config.timebase);
			
			if(!swtAliasCombo.isFocusControl())
				swtAliasCombo.setText(config.aliasPath == null ? "" : config.aliasPath);
			swtWriteAliasCheckbox.setSelection(config.updateAlias);
						
			imageStatusCanvas.redraw();
			imageStatusCanvas.update();
			updateTimeMetadataCombo();
			
			int imageSize = config.imageWidth * config.imageHeight * config.imageType.size();
			
			if(!bandwidthThrottleSpinner.isFocusControl()) {
				bandwidthThrottleSpinner.setSelection((int)(config.bandwidthThrottleMBPs * 1000));
			}
			
			if(!imagePerSecondSpinner.isFocusControl()) {
				double fps = config.bandwidthThrottleMBPs * 1e6 / imageSize;  
				imagePerSecondSpinner.setSelection((int)(fps * 1000));
			}
			
			framePerSecondLabel.setText("fps. Current = " + String.format("%5.3f", sink.getCurrentTransmitRate()) + " fps");
			
		}finally {
			updateInhibit = false;
		}
		
	}
	
	private void updateTimeMetadataCombo() {
		GeriStreamConfig config = sink.getConfig();
		
		ArrayList<String> entries = sink.getPossibleTimebases();
		
		swtTimeMetadata.removeAll();
		int idx = 0, idxSel = -1;
		for(String entry : entries) { 
			swtTimeMetadata.add(entry);
			if(config.timebase != null && config.timebase.equalsIgnoreCase(entry))
				idxSel = idx;			
			idx++;
		}					
		if(idxSel >= 0)
			swtTimeMetadata.select(idxSel);
		else
			swtTimeMetadata.setText(config.timebase);
	}
	
	@Override
	public void destroy() {
		swtGroup.dispose();		
		sink.controllerDestroyed(this);
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { return sink; }
}
