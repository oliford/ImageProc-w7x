package imageProc.database.geriStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import geri.providers.LogProvider;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;

public class MetaDataLogProvider extends LogProvider {

	private GeriStreamSink sink;
	public MetaDataLogProvider(GeriStreamSink sink) {
		super(-1, 1024*1024-5); //just a default, overridden by stream config via setMaxLength() later
		this.sink = sink;
	}
	
	@Override
	public String describe() {
		//called when the codastation connects to our GERI server
		GeriStreamConfig cfg = sink.getConfig();
		int id = cfg.parlogStreamID;
		if(id < 0) 
			throw new IllegalArgumentException("Parlog GERI unit ID must be configured before codastation connects!");
		setID(id);
		
		return super.describe();
	}
	
	public void queueMetadata(long timestamp) {
		// convert metadata to json
		String jsonString = createMetadataJSON();
		
		queue(timestamp, jsonString);
		
		logr.fine("Queued log of length " + jsonString.length() + " at " + timestamp);
		
	}
	
	public String createMetadataJSON() {
		MetaDataMap map = sink.getCompleteSeriesMetaDataMap();
		
		HashMap<String, Object> treeMap = new HashMap<>();
		
		//put /x/y/z entries in a tree form
		for(Entry<String, MetaData> entry : map.entrySet()) {
			String name = entry.getKey().replaceAll("^/*", "").replaceAll("/*$", "");
			String parts[] = name.split("/");
			
			HashMap<String, Object> treeLevel = treeMap;
			for(int i=0;i < (parts.length-1); i++) {
				Object o = treeLevel.get(parts[i]);
				if(!(o instanceof HashMap))
					o = null;
				HashMap<String, Object> newLevel = (HashMap<String, Object>)o;
				if(newLevel == null) { 
					newLevel = new HashMap<String, Object>();
					treeLevel.put(parts[i], newLevel);
				}
				treeLevel = newLevel;
				
			}
			
			treeLevel.put(parts[parts.length-1], entry.getValue().object);			
		}
		
		//time series entries
		ArrayList<String> timeSeriesNames = new ArrayList<>();
		for(Entry<String, MetaData> entry : map.entrySet()) {
			if(entry.getValue() != null){
				if(entry.getValue().isTimeSeries)
					timeSeriesNames.add(entry.getKey());
			}
		}
		
		//write the list of metadata entries which are time series		
		treeMap.put("timeSeriesEntries", timeSeriesNames.toArray(new String[0]));
		
		//let google do the heavy lifting
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.setPrettyPrinting()
				.create();
		
		String jsonString = gson.toJson(treeMap);
		
		return jsonString;
	}
}
