package imageProc.proc.bes.densityNN.swt;

import imageProc.core.ImgProcPipe;
import imageProc.core.ImgProcessor;
import imageProc.core.swt.ImageProcessorSWTController;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.proc.bes.densityNN.DensityNNProcessor;

public class DensityNNSWTController extends ImageProcessorSWTController {
	private DensityNNProcessor proc;
	
	public DensityNNSWTController(DensityNNProcessor proc) {
		this.proc = proc;		
	}

	@Override
	public DensityNNProcessor getInterfacingObject() {  return proc; }

	@Override
	public void destroy() {
		
	}

	@Override
	public DensityNNProcessor getPipe() { return proc;	}
	
}
