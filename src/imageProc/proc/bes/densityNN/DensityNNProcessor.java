package imageProc.proc.bes.densityNN;

import imageProc.core.ImgProcessor;
import imageProc.core.ImgProcessorConfig;
import imageProc.core.ImgSink;

/** Processor for producing fast central density from beam emission spectropscopy 
 * 
 * Requires:
 *   SoftwareROIsProcessor 
 *   SpecCalProcessor (for calibrations)
 *   SeriesProcessor (to find beam-on frames and perhaps do background subtraction)
 *   
 */
public class DensityNNProcessor extends ImgProcessor implements ImgSink {

	private DensityNNConfig config = new DensityNNConfig();
	
	
	@Override
	protected void doCalcScan() {
		
	}

	@Override
	public ImgProcessor clone() {
		return new DensityNNProcessor();
	}

	@Override
	public ImgProcessorConfig getConfig() { return config; }

}
