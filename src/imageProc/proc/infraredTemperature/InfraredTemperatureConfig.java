package imageProc.proc.infraredTemperature;

import java.util.HashMap;

import imageProc.core.ImgProcPipeMultithreadConfig;

/** Configuration of InfraredTemperatureProcessor */
public class InfraredTemperatureConfig extends ImgProcPipeMultithreadConfig {
	
	/** Thing to add */
	public double addValue;
	
	/** Thing to multiply */
	public double multiplyValue;
	
	/** Relative (to calibration area) emissivity */
	public double emissivity;
	
	/** Calibration temperatures array - 1 for each value of the black body radiance */
	public double[] temperatures;
	
	/** Calibration relative radiance B of black body - the same shape as temperatures */
	public double[] radiance;
	
	/** Free form comment about config */
	public String comment;
	
	/** Adds settings to Map. Used for adding the configuration to metadata */
	public void addArraysToMap(HashMap<String, Object> map, String prefix, boolean enabledOnly) {
			
		map.put(prefix + "/addValue", addValue);
		map.put(prefix + "/multiplyValue", multiplyValue);			
		
	}

}
