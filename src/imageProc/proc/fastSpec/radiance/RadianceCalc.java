package imageProc.proc.fastSpec.radiance;

import java.util.HashMap;

import org.apache.commons.math3.util.FastMath;

import algorithmrepository.Interpolation1D;
import imageProc.core.ImgSource;
import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.fastSpec.FastSpecProcessor.CalcStage;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class RadianceCalc {

	private FastSpecProcessorW7X proc;
	
	public RadianceCalc(FastSpecProcessorW7X proc) {
		this.proc = proc;
	}
	
	public void calcRadiance(CalcStage stage, FastSpecEntry specEntryOnly, int iF) {
		
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfigW7X();

		//TODO: Absolute calibrated intensity (more formally known as SpectralRadiance)
		
		//input things:
		// cameraModeInfo/photoelectronsPerADCU - [photoelectronsPerADCU] 
		// cameraModeInfo/exposureTime - [s]
		//  - need to multiply by exposure time incase of summing
		// /SpecCal/intensity - intensity calibration [photoelectrons (photon m^-2 SR^-1 nm^-1)^-1 ]
		
		double peFactor;//, exposureTime;
		
		//int numImages[];
		//double[] beamOnDurationQ7, beamOnDurationQ8;
		double[][] intensityCal;
		
		try{ 
			ImgSource src = proc.getConnectedSource();
			peFactor = (double)src.getSeriesMetaData("cameraModeInfo/photoelectronsPerADCU");
			//exposureTime = (double)src.getSeriesMetaData("cameraModeInfo/exposureTime");
			
			//numImages = (int[]) Mat.fixWeirdContainers(src.getSeriesMetaData("/SeriesProc/numInputImagesUsed"), true);
			//beamOnDurationQ7 = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("/w7x/nbi/beamOnDurationQ7"), true);
			//beamOnDurationQ8 = (double[])Mat.fixWeirdContainers(src.getSeriesMetaData("/w7x/nbi/beamOnDurationQ8"), true);
			Object o = src.getSeriesMetaData("/SpecCal/intensity"); 
			o = Mat.fixWeirdContainers(o, true);
			intensityCal = (double[][])o;
		 
		}catch(RuntimeException err){
			throw new RuntimeException("Error getting necessary input from metadata:", err);
		}

		for (FastSpecEntry specEntry : configW7X.specEntries) {
			
			double[] specRad = new double[specEntry.fit.Amplitude.length];
			double[] specRadErr = new double[specEntry.fit.Amplitude.length];
			
			for(int i=0; i < specEntry.fit.Amplitude.length; i++){
				
				// Amp - integrated area under the gaussian? [ADCU nm]
				//         - (assuming it's fitted again wavelength)
				specRad[i] = specEntry.fit.Amplitude[i];
				
				//convert to [photoelectron nm]
				specRad[i] *= peFactor;
				
				
				//convert to radiant energy
				//[photoelectrons nm] / [photoelectrons (photon m^-2 SR^-1 nm^-1)^-1 ]
				//--> [photons m^-2 SR^-1]
				int row = (int)specEntry.y0;
				int x = proc.getPixelForWavelength(row, specEntry.fit.X0[i]);
				if(x < 0)
					x=0;
				if(x >= intensityCal[row].length)
					x = intensityCal[row].length - 1;
				specRad[i] /= intensityCal[row][x];

				/*
				//maybe convert to [photoelectrons s^-1 nm]
				switch(configW7X.radianceConfig.mode){
					case Energy:
						//nothing todo, it's already the radiant energy
						break;
					case RadianceAverageOfFrame:
						
						specRad[i]/= exposureTime;
						if(numImages != null) //also take account of SeriesProc summing images
							specRad[i] /= numImages[i];
						
						break;
					case RadianceAverageOfNBIBlip:
						double durationQ7 = (beamOnDurationQ7 != null) ? beamOnDurationQ7[i] : 0;
						double durationQ8 = (beamOnDurationQ8 != null) ? beamOnDurationQ8[i] : 0;
						double maxDuration = FastMath.max(durationQ7, durationQ8);
						
						specRad[i] /= maxDuration;
						break;
				}
				*/
				//simple scaled uncertainty
				specRadErr[i] = specEntry.fit.UncertaintyAmpltiude[i] * specRad[i] / specEntry.fit.Amplitude[i];
			}

			specEntry.fit.inferredQuantities.put("SpectralRadiantEnergy", specRad);			
			specEntry.fit.inferredQuantities.put("SpectralRadiantEnergy_uncertainty", specRadErr);			
		}

		if(!configW7X.inferredQuantities.contains("SpectralRadiantEnergy"))
			configW7X.inferredQuantities.add("SpectralRadiantEnergy");		
		if(!configW7X.inferredQuantities.contains("SpectralRadiantEnergy_uncertainty"))
			configW7X.inferredQuantities.add("SpectralRadiantEnergy_uncertainty");
		
	}

	public void saveResults() {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfigW7X();

		proc.saveFitResults(configW7X.saveResultsPath + "_SpectralRadiantEnergy_DATASTREAM", "SpectralRadiantEnergy");
		if(configW7X.calcFitUncertainty)
			proc.saveFitResults(configW7X.saveResultsPath + "_SpectralRadiantEnergy_uncertainty_DATASTREAM", "SpectralRadiantEnergy_uncertainty");
	}
}
