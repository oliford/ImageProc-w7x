package imageProc.proc.fastSpec.radiance;


/** Configuration for calibrated spectral radiance and things calculated from this
 * (maybe CX impurity densities)  
 */
public class RadianceConfig {

	/** Enable the radiance calculation */
	public boolean enable = false;
		
}
