package imageProc.proc.fastSpec;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.swt.widgets.Composite;


import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import descriptors.w7x.ArchiveDBDesc;
import descriptors.w7x.ArchiveDBDesc.Type;
import descriptors.w7x.ArchiveDBJSONInfoDesc.ExtraJsonRequestArg;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.proc.fastSpec.FastSpecEntry.FitResults;
import imageProc.proc.fastSpec.bes.BeamEmissionCalc;
import imageProc.proc.fastSpec.ionTemp.IonTempCalc;
import imageProc.proc.fastSpec.nbiSpec.NeutraliserSpecCalc;
import imageProc.proc.fastSpec.passiveInit.PassiveInitialiser;
import imageProc.proc.fastSpec.radiance.RadianceCalc;
import imageProc.w7x.W7XUtil;
import imageProc.core.MetaDataMap.MetaData;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.w7x.ArchiveDB;
import w7x.archive.ArchiveDBFetcher;
import w7x.archive.writing.Channel;
import w7x.archive.writing.DataStream;
import w7x.archive.writing.ParameterLog;
import w7x.archive.writing.VersionData;
import w7x.archive.writing.W7xArchiveWrite;

public class FastSpecProcessorW7X extends FastSpecProcessor {
	
	//specifics calculations
	private NeutraliserSpecCalc nbiCalc = new NeutraliserSpecCalc(this);
	private BeamEmissionCalc besCalc = new BeamEmissionCalc(this);
	private IonTempCalc ionTempCalc = new IonTempCalc(this);
	private RadianceCalc radianceCalc = new RadianceCalc(this);
	private PassiveInitialiser passiveInit = new PassiveInitialiser(this);
	
	private static enum ParameterSel {
		Amp, X0, Sigma, Y0, Slope, Power,
		ParameterLog, 
		InferredQuantity,
		RMSFitError
	}
	
	/** status for writing */
	private int nWritten, nFailed;

	public FastSpecProcessorW7X(SettingsManager settingsMgr){
		super(settingsMgr);
		config = new FastSpecConfigW7X();
	}

	public FastSpecProcessorW7X() { this(null); }
	
	public FastSpecProcessorW7X(ImgSource source, int selectedIndex) {
		super(source, selectedIndex);
		config = new FastSpecConfigW7X();		
	}
		
	@Override
	protected void inferredQuantities(CalcStage stage, FastSpecEntry specEntryOnly, int iF){
		super.inferredQuantities(stage, specEntryOnly, iF);

		//H/He ratio, beam energies etc should all go in inferredQuantities
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)config;
		
		if(configW7X.ionTemp.enable) {
			ionTempCalc.calcTiCorrected(stage, specEntryOnly, iF);
			
			if(configW7X.ionTemp.averageSets != null && configW7X.ionTemp.averageSets.size() > 0)
				ionTempCalc.doAverages(stage, specEntryOnly, iF);			
		}
		
		if(configW7X.neutralSpec.saveNBIComponentProperties) //post process only
			nbiCalc.inferredQuantities(stage, specEntryOnly, iF);
		
		if(configW7X.radianceConfig.enable)
			radianceCalc.calcRadiance(stage, specEntryOnly, iF);
		
		if(configW7X.passiveInit.enable && stage == CalcStage.Prepare)
			passiveInit.prepare();
				
		if(stage == CalcStage.Prepare){ //pre-processing only
			double beamISect[][][] = (double[][][]) Mat.fixWeirdContainers(connectedSource.getSeriesMetaData("SpecCal/linesOfSight/activeBeamIntersectionPos"), true);
			double beamISectReff[] = (double[]) Mat.fixWeirdContainers(connectedSource.getSeriesMetaData("SpecCal/linesOfSight/nominalBeamIntersectionREff"), true);
			if(configW7X.ionTemp.enable && beamISect != null){
				long nanoTime[] = W7XUtil.getBestNanotime(connectedSource);
										
				boolean allInvalid = true;
				for(FastSpecEntry specEntry : configW7X.specEntries){
					int row = (int)specEntry.y0;
					
					double entryBeamISect[][] = new double[5][nanoTime.length];
					for(int i=0; i < nanoTime.length; i++){
						for(int k=0; k < 3; k++){
							entryBeamISect[k][i] = beamISect[i][row][k];
							entryBeamISect[3][i] += FastMath.pow2(entryBeamISect[k][i]);
							entryBeamISect[4][i] = (beamISectReff != null) ? beamISectReff[row] : Double.NaN;
							if(!Double.isNaN(entryBeamISect[k][i]))
								allInvalid = false;
						}
						entryBeamISect[3][i] = FastMath.sqrt(entryBeamISect[3][i]);
					}
					
					specEntry.fit.inferredQuantities.put("BeamIntersectionX", entryBeamISect[0]);
					specEntry.fit.inferredQuantities.put("BeamIntersectionY", entryBeamISect[1]);
					specEntry.fit.inferredQuantities.put("BeamIntersectionZ", entryBeamISect[2]);
					//specEntry.inferredQuantities.put("ProfileX", entryBeamISect[3]);					
					specEntry.fit.inferredQuantities.put("ProfileX", Mat.mulElem(entryBeamISect[4],1/0.52));					
				}
					
				if(!config.inferredQuantities.contains("BeamIntersectionX")){
					config.inferredQuantities.add("BeamIntersectionX");
					config.inferredQuantities.add("BeamIntersectionY");
					config.inferredQuantities.add("ProfileX");
				}
				
				if(allInvalid)
					//throw new RuntimeException("CalcTi is enabled but all beam intersections positions invalid. Something is broken");
					System.err.println("CalcTi is enabled but all beam intersections positions invalid. Something is broken");
			}
		}
	}
		
	@Override
	protected void doSaveResults(){
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)config;
		
		if(configW7X.saveResultsPath != null 
					&& configW7X.saveResultsPath.trim().length() > 0){
			
			//add ImageProc-core git commit ID and date to metadata			
			String gitCommit[] = ImageProcUtil.getGitCommitInfo("ImageProc-w7x");
			connectedSource.setSeriesMetaData("git/ImageProc-w7x/commit", gitCommit[0], false);
			connectedSource.setSeriesMetaData("git/ImageProc-w7x/date", gitCommit[1], false);

			nWritten = 0;
			nFailed = 0;
			
			W7xArchiveWrite.setTimeout(10000);
			
			if(configW7X.saveResultsPath.contains("/raw/W7X/")){
				throw new RuntimeException("FastSpec results can not go in '/raw/W7X/', try '/raw/W7XAnalysis/'.");
			}
			
			//see if we should be saving the power (any entry has maxPower > minPower)
			configW7X.savePower = false;
			for(FastSpecEntry entry : config.specEntries)
				if(entry.powerMax > entry.powerMin)
					configW7X.savePower = true;
			
			configW7X.lastVersionSaved = -1;
			
			//2D within a channel/timepoint doesn't seem to work 			
			saveFitResults(configW7X.saveResultsPath + "_Amplitude_DATASTREAM", ParameterSel.Amp);
			
			if(configW7X.lastVersionSaved < 0)
				throw new IllegalArgumentException("Amplitude write failed. Aborting write sequence.");
			
			saveFitResults(configW7X.saveResultsPath + "_X0_DATASTREAM", ParameterSel.X0);
			saveFitResults(configW7X.saveResultsPath + "_Sigma_DATASTREAM", ParameterSel.Sigma);
			if(configW7X.savePower)
				saveFitResults(configW7X.saveResultsPath + "_Power_DATASTREAM", ParameterSel.Power);
			saveFitResults(configW7X.saveResultsPath + "_Y0_DATASTREAM", ParameterSel.Y0);
			saveFitResults(configW7X.saveResultsPath + "_Slope_DATASTREAM", ParameterSel.Slope);
			saveFitResults(configW7X.saveResultsPath + "_PARLOG", ParameterSel.ParameterLog);
			
			if(configW7X.calcFitUncertainty){
				saveFitResults(configW7X.saveResultsPath + "_Amplitude_uncertainty_DATASTREAM", ParameterSel.Amp);
				saveFitResults(configW7X.saveResultsPath + "_X0_uncertainty_DATASTREAM", ParameterSel.X0);
				saveFitResults(configW7X.saveResultsPath + "_Sigma_uncertainty_DATASTREAM", ParameterSel.Sigma);
				if(configW7X.savePower)
					saveFitResults(configW7X.saveResultsPath + "_Power_uncertainty_DATASTREAM", ParameterSel.Power);
				saveFitResults(configW7X.saveResultsPath + "_Y0_uncertainty_DATASTREAM", ParameterSel.Y0);
				//saveFitResults(configW7X.saveResultsPath + "_Slope_uncertainty_DATASTREAM", ParameterSel.Slope);				
			}
				
			saveFitResults(configW7X.saveResultsPath + "_RMSFitError_DATASTREAM", ParameterSel.RMSFitError);
			
			if(configW7X.saveHHeRatio){
				saveLineRatio(configW7X.saveResultsPath + "_HHeRatio_DATASTREAM", "H_I_486", "He_II_468", configW7X.heliumIntensityThreshold);
				saveLineRatio(configW7X.saveResultsPath + "_HHeIRatio_DATASTREAM", "H_I_486", "He_I_492", configW7X.heliumIntensityThreshold);
			}
	
			if(configW7X.ionTemp.enable)
				ionTempCalc.saveResults();
			
			if(configW7X.radianceConfig.enable)
				radianceCalc.saveResults();
			
			if(configW7X.neutralSpec.saveNBIComponentProperties)
				nbiCalc.saveResults();
		
			logr.fine("FastSpecProcessorW7X.doSaveResults() complete.");

			setStatus("Done. Datastreams: " + nWritten + " OK, " + nFailed + " Failed");
			
			if(configW7X.setAliasOnWrite)
				saveAlias();
		}
	}
	
	public void saveFitResults(String path, String inferredQuantity) {
		saveFitResults(path, ParameterSel.InferredQuantity, inferredQuantity);
	}
	public void saveFitResults(String path, ParameterSel paramSel) {
		saveFitResults(path, paramSel, null);
	}
	public void saveFitResults(String path, ParameterSel paramSel, String inferredQuantity) {
		ArchiveDBFetcher adb = new ArchiveDBFetcher();
		adb.enableMutableCache(false);
		
		boolean uncertainty = path.contains("_uncertainty"); //TODO: As parameter

		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)config;
		

		setStatus("Saving fits: "+nWritten + " OK, " + nFailed + " failed");
		
		try{
			ArchiveDBDesc desc = new ArchiveDBDesc(path);
			
			long nanoTime[] = W7XUtil.getBestNanotime(connectedSource);
			int n = connectedSource.getNumImages();
			if(nanoTime.length != n){
				logr.warning("Nanotime array has length "+nanoTime.length+" but we have " + n + " images, truncating/extending to match");
				nanoTime = Arrays.copyOf(nanoTime, n);
			}
			boolean goodFrames[] = getGoodFramesWithValidNanotimes(nanoTime);
			long goodNanoTime[] = Mat.extractElems(nanoTime, goodFrames);
			
						
			desc.setNanosRange(goodNanoTime[0], goodNanoTime[goodNanoTime.length-1]);
			//int ver = (configW7X.saveResultsVersion > 0) ? configW7X.saveResultsVersion : (adb.getHighestVersionNumber(desc) + 1);
			int ver;
			if(configW7X.saveResultsVersion > 0){
				ver = configW7X.saveResultsVersion;
				configW7X.lastVersionSaved = ver;
				
			}else if(paramSel == ParameterSel.Amp && !uncertainty){ //first one
				ver = adb.getHighestVersionNumber(desc)+1;			

			}else{ //otherwise always try to write same version as first stream
				ver = configW7X.lastVersionSaved;
				if(ver < 0)
					throw new IllegalArgumentException("Invalid version. Did previous stream save fail?");
			}
			
			if(paramSel == ParameterSel.ParameterLog){ //config 
				writeParlog(desc, ver);
				return;
				
			}
			
			
			//don't write any info to the versions, because these are shot specific
			//the actual descirption of the change is just written as part of writing
			//the config to the parlog (config.saveResultsDescription)			
			DataStream dataStream = new DataStream(desc.getDatabase(), 
					desc.getPartialPath(), ver, goodNanoTime, "FastSpec", "ImageProc.FastSpecProcessor");
			desc.setVersion(ver);
				
								
			int chIdx = 0;
			ArrayList<String> losNames = new ArrayList<String>(); 
			for(FastSpecEntry specEntry : config.specEntries){
				if(!specEntry.enable)
					continue;
				
				Object data;
				switch(paramSel){
					case Amp: data = uncertainty ? specEntry.fit.UncertaintyAmpltiude : specEntry.fit.Amplitude; break;
					case X0: data = uncertainty ? specEntry.fit.UncertaintyX0 : specEntry.fit.X0; break;
					case Sigma: data = uncertainty ? specEntry.fit.UncertaintySigma : specEntry.fit.Sigma; break;
					case Power: data = uncertainty ? specEntry.fit.UncertaintyPower : specEntry.fit.Power; break;
					case Y0: data = uncertainty ? specEntry.fit.UncertaintyY0 : specEntry.fit.Y0; break;
					case Slope: data = uncertainty ? null : specEntry.fit.Slope; break;																	
					case RMSFitError: data = specEntry.fit.RMSError; break;					
					case InferredQuantity:						
						data = specEntry.fit.inferredQuantities.get(inferredQuantity);
						if(data == null){
							System.err.println("No inferred quantity named '"+inferredQuantity+"' in entry '"+specEntry.id+"'.");
							data = Mat.fillArray(Double.NaN, nanoTime.length);
							//continue;
						} 
						break;
					default: throw new IllegalArgumentException();
				}			
						
				//if(configW7X.saveOnlyGoodFrames)
				data = Mat.extractElems(data, goodFrames);
				
				for(int i=0; i < Array.getLength(data); i++){
					if(Array.get(data, i) == null){
						System.err.println("data["+i+"] is null");
					}
				}
				
				//String chanName = specEntry.getTransition() + "_" + specEntry.nominalWavelength;
				Channel chan = new Channel(chIdx, specEntry.id, data, 
						specEntry.getTransition() + "_" + specEntry.nominalWavelength, "", true);
				
				chIdx++;
						
				dataStream.addChannel(chan);
				losNames.add(specEntry.losName == null ? "UNKNOWN" : specEntry.losName);
			}
			
			dataStream.addParameter("losNames", losNames.toArray(new String[losNames.size()]));
			
			if(!dataStream.hasChannels()){
				System.err.println("No channels to write to '"+desc+"'. Not writing stream.");
				return;
			}
						
			W7xArchiveWrite.setTimeout(60000);
			dataStream.send();
			
			System.out.println("Written " + desc);
			configW7X.lastVersionSaved = ver;			
			nWritten++;
			
		}catch(Throwable err){
			logr.log(Level.SEVERE, "Error writing stream "  +path, err);	
			nFailed++;
			
		}finally{
			if(abortCalc)
				throw new RuntimeException("Aborted in saveResults()");
			
		}
	}
	

	private boolean[] getGoodFramesWithValidNanotimes(long nanoTime[]) {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)config;
		
		boolean goodFrames[] = null;
		if(configW7X.saveOnlyGoodFrames){
			goodFrames = (boolean[])connectedSource.getSeriesMetaData("isGoodData");				
		}else 
			goodFrames = Mat.fillArray(true, nanoTime.length);
		
		//check for nonzero nanotimes	
		int nBadNanotimes = 0;
		for(int i=0; i < nanoTime.length; i++) 
			if(nanoTime[i] <= 0 && goodFrames[i]) {
				goodFrames[i] = false;
				nBadNanotimes++;
			}
			
		if(nBadNanotimes > 0)
			logr.warning("Abandoning " + nBadNanotimes + " frames with good data because their nanotime are zero");
		
		return goodFrames;
	}

	private void writeParlog(ArchiveDBDesc desc, int ver){
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)config;
		
		int version = ver;
		String reason = "FastSpec";
		String codeRelease = "FastSpecProcessor";
		String producer = null;
		String analysisEnvironment = null;
		
		ParameterLog parLog2 = new ParameterLog(desc, reason, codeRelease);
		
		ArchiveDBFetcher fetcher = new ArchiveDBFetcher();
		fetcher.setQuiet(true);
		
		ArchiveDBDesc desc2 = new ArchiveDBDesc();
		desc2.setDatabase(desc.getDatabase());
		desc2.setFromPartialPath(desc.getPartialPath());
		desc2.setType(Type.PARLOG);
		desc2.setVersion(version);
		
		VersionData versionObject = new VersionData(desc2, reason, producer, codeRelease, analysisEnvironment);
		versionObject.send();		
		
		JsonObject jsonObj = new JsonObject(); 
		
		jsonObj.add("label", W7xArchiveWrite.toJsonElement("parms"));									
		jsonObj.add("dimensions", W7xArchiveWrite.toJsonElement(new long[] { desc.getFromNanos(), desc.getToNanos() }));					
								
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.addSerializationExclusionStrategy(new ExclusionStrategy() {							
					@Override
					public boolean shouldSkipField(FieldAttributes attrbs) {								
						return attrbs.getName().startsWith("fit") ||
								attrbs.getName().startsWith("inferred");
					}
					
					@Override
					public boolean shouldSkipClass(Class<?> arg0) { return false; }
				})
				.create();
		
		JsonElement jsonElem = gson.toJsonTree(config);
		JsonObject values = jsonElem.getAsJsonObject();
		
		configW7X.saveTimeSeriesPatterns = new String[]{ "/?w7x/nbi/.*" };
		
		long nanoTime[] = W7XUtil.getBestNanotime(connectedSource);
		boolean goodFrames[] = getGoodFramesWithValidNanotimes(nanoTime);
		//long goodNanoTime[] = Mat.extractElems(nanoTime, goodFrames);
						
		//add all upsteam metadata. 
		if(configW7X.saveSourceMetadata){
			ArrayList<String> timeSeriesNames = new ArrayList<>();
			MetaDataMap map = connectedSource.getCompleteSeriesMetaDataMap();
			
			for(Entry<String, MetaData> entry : map.entrySet()) {
				if(entry.getValue() == null)
					continue;
				
				String paramName = entry.getKey();				
				
				if(entry.getValue().isTimeSeries){
					boolean matched = false;
					for(String exp : configW7X.saveTimeSeriesPatterns)
						matched |= paramName.matches(exp);					
					if(!matched)
						continue;					
		
					timeSeriesNames.add(paramName);
				}

					
				try{
					paramName = "sourceMetaData/" + paramName;
					paramName = paramName.replaceAll("/+", "/").replaceFirst("^/*", "").replaceFirst("/*$", "");
					
					Object data = entry.getValue().object;
					if(configW7X.saveOnlyGoodFrames && entry.getValue().isTimeSeries){
						data = Mat.extractElems(data, goodFrames);
					}
					
					ParameterLog.addToTree(values, paramName, data);					
						
				}catch(Exception err){
					System.err.println("WARNING: Error adding parameter '"+paramName+"' to parLog: " + err.getMessage());
					//err.printStackTrace();
				}
				
			}
			
			//write the list of metadata entries which are time series
			ParameterLog.addToTree(values, "sourceMetaData/timeSeriesEntries", 
				W7xArchiveWrite.toJsonElement(timeSeriesNames.toArray(new String[0])));
		}

		JsonArray outerValuesArray = new JsonArray();
		outerValuesArray.add(values);
		jsonObj.add("values", outerValuesArray);
		
		//OneLiners.textToFile("/tmp/aa.json", jsonObj.toString());
		
		W7xArchiveWrite.send(W7xArchiveWrite.getDefaultBaseUri(), desc2, jsonObj);

		desc2.setVersion(ver);
		System.out.println("Written " + desc2);
	}
	
	public void saveAlias(){
		try{
			FastSpecConfigW7X configW7X = (FastSpecConfigW7X)config;
			
			
			long nanoTime[] = W7XUtil.getBestNanotime(connectedSource);
			boolean goodFrames[] = getGoodFramesWithValidNanotimes(nanoTime);
			long goodNanoTime[] = Mat.extractElems(nanoTime, goodFrames);
			
			long tStart = goodNanoTime[0];
			long tEnd = goodNanoTime[nanoTime.length-1];
			
			if(configW7X.writeAliasForWholeProgram){
				long tAvg = (tStart + tEnd) / 2;
				//could use ArchiveDBFetcher.defaultInstance().getAllProgramsInRangeJsonObj(tStart, tEnd); if visible
				try {
					long tFrom = ((JsonPrimitive)ArchiveDBFetcher.defaultInstance().getProgramInfo(tAvg, "from")).getAsLong();
					long tTo = ((JsonPrimitive)ArchiveDBFetcher.defaultInstance().getProgramInfo(tAvg, "upto")).getAsLong();
					if(tFrom > 0){
						tStart = tFrom;
						tEnd = tTo;
					}
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Unable to extend alias time period to cover program.", err);
				}
			}
			
			ArchiveDBDesc aliasDesc = new ArchiveDBDesc(configW7X.aliasPath);		
			//ArchiveDBDesc alias = ArchiveDBDesc.aliasDescriptor(Database.W7X_SANDBOX, "Minerva/Test/bla", 123L);
			aliasDesc.setFromNanos(tStart);		
			aliasDesc.setToNanos(tEnd);
			if(!aliasDesc.isAlias())
				throw new IllegalArgumentException("Alias isn't an alias (i.e. it's not in /views)");
	
			String targetPath = configW7X.saveResultsPath + "_Amplitude_DATASTREAM";
			ArchiveDBDesc targetDesc = new ArchiveDBDesc(targetPath);
			targetDesc.setVersion(configW7X.lastVersionSaved);
			
			W7xArchiveWrite.writeAlias(aliasDesc, targetDesc);
			
			String str = getStatus();
			setStatus(str + " +Alias set.");
			
		}catch(RuntimeException err){
			logr.log(Level.WARNING, "Error while writing alias '"+"'", err);//configW7X.aliasPath
			String str = getStatus();
			setStatus(str + " *Alias failed*.");
		}
	}
	

	private void saveLineRatio(String path, String entryID1, String entryID2, int threshold) {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)config;
		
		ArchiveDBFetcher adb = new ArchiveDBFetcher();
		
		try{

			FastSpecEntry specH = config.getPointByName(entryID1);
			FastSpecEntry specHe = config.getPointByName(entryID2);
			if(specH == null || specHe == null){
				System.err.println("No H or He line present");
				return;
			}

			double ratio[] = new double[specH.fit.Amplitude.length];
			double ratioUncertainty[] = new double[specH.fit.Amplitude.length];
			for(int i=0; i < specH.fit.Amplitude.length; i++){
				double sum = (specH.fit.Amplitude[i] + specHe.fit.Amplitude[i]);
				ratio[i] = specH.fit.Amplitude[i] / sum;
				if(sum < threshold)
					ratio[i] = 0; //clean up
			}
			
			String ratioDesc = "Line ratio "+entryID1+" / ("+entryID1+" + "+entryID2+")"; 
			specH.fit.inferredQuantities.put(ratioDesc, ratio);
			specHe.fit.inferredQuantities.put(ratioDesc, ratio);
				
			if(!config.inferredQuantities.contains(ratioDesc))
				config.inferredQuantities.add(ratioDesc);

			
			long nanoTime[] = W7XUtil.getBestNanotime(connectedSource);
			boolean goodFrames[] = getGoodFramesWithValidNanotimes(nanoTime);
			long goodNanoTime[] = Mat.extractElems(nanoTime, goodFrames);
						
			ArchiveDBDesc desc = new ArchiveDBDesc(path);						

			desc.setNanosRange(goodNanoTime[0], goodNanoTime[goodNanoTime.length-1]);
			ratio = Mat.extractElems(ratio, goodFrames);
			
			int ver;
			if(configW7X.saveResultsVersion > 0){
				ver = configW7X.saveResultsVersion;
				configW7X.lastVersionSaved = ver;
				
			//}else if(paramSel == ParameterSel.Amp && !uncertainty){ //first one
			//	ver = adb.getHighestVersionNumber(desc)+1;			

			}else{ //otherwise always try to write same version as first stream
				ver = configW7X.lastVersionSaved;
				if(ver < 0)
					throw new IllegalArgumentException("Invalid version. Did previous stream save fail?");
			}
			//int ver = (configW7X.saveResultsVersion > 0) ? configW7X.saveResultsVersion : (adb.getHighestVersionNumber(desc) + 1);
			DataStream dataStream = new DataStream(desc.getDatabase(), 
					desc.getPartialPath(), ver, goodNanoTime, "FastSpec", "FastSpecProcessor");
			desc.setVersion(ver);
			Channel chan = new Channel(0, "HHeRatio", ratio, ratioDesc, "", true);

			dataStream.addChannel(chan);

			System.out.println("Written " + desc);
			
			
			dataStream.send();
			nWritten++;

		}catch(RuntimeException err){
			err.printStackTrace();
			nFailed++;
		}
	}

	public void saveScalarStream(String path, double[] values, String chanName, String descString) {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)config;
		
		ArchiveDBFetcher adb = new ArchiveDBFetcher();
		
		try{

			
			long nanoTime[] = W7XUtil.getBestNanotime(connectedSource);
			int n = connectedSource.getNumImages();
			if(nanoTime.length > n){
				nanoTime = Arrays.copyOf(nanoTime, n);
			}

			ArchiveDBDesc desc = new ArchiveDBDesc(path);						

			desc.setNanosRange(nanoTime[0], nanoTime[nanoTime.length-1]);
			
			int ver;
			if(configW7X.saveResultsVersion > 0){
				ver = configW7X.saveResultsVersion;
				configW7X.lastVersionSaved = ver;
				
			//}else if(paramSel == ParameterSel.Amp && !uncertainty){ //first one
			//	ver = adb.getHighestVersionNumber(desc)+1;			

			}else{ //otherwise always try to write same version as first stream
				ver = configW7X.lastVersionSaved;
				if(ver < 0)
					throw new IllegalArgumentException("Invalid version. Did previous stream save fail?");
			}

			//int ver = (configW7X.saveResultsVersion > 0) ? configW7X.saveResultsVersion : (adb.getHighestVersionNumber(desc) + 1);
			DataStream dataStream = new DataStream(desc.getDatabase(), 
					desc.getPartialPath(), ver, nanoTime, "FastSpec", "FastSpecProcessor");
			desc.setVersion(ver);
			Channel chan = new Channel(0, chanName, values, descString, "", true);

			dataStream.addChannel(chan);

			logr.info("Written " + desc);			
			
			dataStream.send();
			nWritten++;

		}catch(RuntimeException err){
			err.printStackTrace();
			nFailed++;
		}
	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			FastSpecSWTControllerW7X controller = new FastSpecSWTControllerW7X((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	@Override
	protected FastSpecConfig configFromJson(Gson gson, String jsonString) {
		return gson.fromJson(jsonString, FastSpecConfigW7X.class);
	}
	
	@Override
	public void loadConfig(String id) {
		if (id.startsWith("parlog:")) {
			loadConfigFromArchive(id);
			lastLoadedConfig = id;
		}else {
			super.loadConfig(id);
		}
	}
	
	private void loadConfigFromArchive(String id){
		//Load directly from previously stored Parlog
		try{
			String streamPath = id.substring(7);
				
			ArchiveDBDesc desc = new ArchiveDBDesc(streamPath);			
			desc.setType(Type.PARLOG);
			if(!desc.hasTimerange()){
				long nanos[] = W7XUtil.getBestNanotime(connectedSource);
				desc.setNanosRange(nanos[0], nanos[nanos.length-1]);
				
			}
			System.out.println(desc.path());
			
			
			if(desc.getVersion() <= 0){
				int version = ArchiveDBFetcher.defaultInstance().getHighestVersionNumber(desc);
				if(version == 0)
					throw new RuntimeException("No versions (no data) at " + desc.getWebAPIRequestURI());
				desc.setVersion(version);
			}
			//ArchiveDBJSONInfoDesc jDesc = new ArchiveDBJSONInfoDesc(desc);
			//jDesc.addExtraRequestArgument(ExtraJsonRequestArg.MAP_TO_ARRAY);
			
			JsonObject a = ArchiveDBFetcher.defaultInstance().getParlogJson(desc, ExtraJsonRequestArg.MAP_TO_ARRAY);
			
			JsonObject o = a.get("values").getAsJsonArray().get(0).getAsJsonObject();
			//.getAsJsonObject().get("config")
			String jsonStr = o.toString();
			jsonStr = jsonStr.replaceAll("},", "},\n");
			//System.out.println(jsonStr);
			
			Gson gson = new Gson();
			config = configFromJson(gson, jsonStr);
			
			//some annoying config corrections where they were missing in old parlogs
			for (FastSpecEntry specEntry : config.specEntries) {				
				if (specEntry.amplitudeInitFraction == 0){
					specEntry.amplitudeInitFraction = 1.0;
				}
			}
			
			loadFitSignal(desc, "Amplitude", "fitAmplitude");
			loadFitSignal(desc, "Amplitude_Uncertainty", "fitUncertaintyAmpltiude");
			loadFitSignal(desc, "X0", "fitX0");
			loadFitSignal(desc, "X0_Uncertainty", "fitUncertaintyX0");
			loadFitSignal(desc, "Y0", "fitY0");
			loadFitSignal(desc, "Y0_Uncertainty", "fitUncertaintyY0");
			loadFitSignal(desc, "Sigma", "fitSigma");
			loadFitSignal(desc, "Sigma_Uncertainty", "fitUncertaintySigma");
			loadFitSignal(desc, "Slope", "fitSlope");
			//loadFitSignal(desc, "Slope_Uncertainty", "fitUncertaintySlope");
			loadFitSignal(desc, "Power", "fitPower");
			loadFitSignal(desc, "Power_Uncertainty", "fitUncertaintyPower");
			
			for (FastSpecEntry specEntry : config.specEntries) {
				
				if(specEntry.powerInit == 0) specEntry.powerInit = 2;
				if(specEntry.powerMin == 0) specEntry.powerMin = 2;
				if(specEntry.powerMax == 0) specEntry.powerMax = 2;
				if(specEntry.powerPriorMean == 0) specEntry.powerPriorMean = Double.NaN;
				if(specEntry.powerPriorSigma == 0) specEntry.powerPriorSigma = Double.NaN;
				
				if (specEntry.fit == null)
					specEntry.fit = new FitResults();
				if (specEntry.fit.Power == null)
					specEntry.fit.Power = Mat.fillArray(2.0, connectedSource.getNumImages());
				if (specEntry.fit.RMSError == null)
					specEntry.fit.RMSError = Mat.fillArray(Double.NaN, connectedSource.getNumImages());
			}
			
			config.inferredQuantities = null;
				
			setStatus("Loaded " + desc);
		}catch(RuntimeException err){
			setStatus("Load failed: " + err.toString());
			err.printStackTrace();
		}
		updateAllControllers();
		return;
	}
	
	private void loadFitSignal(ArchiveDBDesc parlogDesc, String paramName, String memberName){
		String chanNames[];
		double data[][];
		
		boolean goodFrames[] = (boolean[])connectedSource.getSeriesMetaData("isGoodData");
		if(config.saveOnlyGoodFrames && goodFrames == null)
			throw new RuntimeException("Data was saved for only good frames but we don't have a good "
										+ "frames index here. Is SeriesProc setup the same?");
		int nImages = connectedSource.getNumImages();
		ArchiveDBFetcher.defaultInstance().setEnableDiskCache(false);
		ArchiveDBFetcher.defaultInstance().setEnableMemoryCache(false);
		try{
			ArchiveDBDesc desc = new ArchiveDBDesc(parlogDesc);
			desc.setType(Type.DATASTREAM);
			desc.setSignalName(desc.getSignalName() + "_" + paramName);
			ArchiveDB sig = (ArchiveDB) ArchiveDBFetcher.defaultInstance().getSig(desc);
			data = sig.getDataAsType(double.class);
	
			chanNames = ArchiveDBFetcher.defaultInstance().getChannelsName(desc);
			
			for(int iC=0; iC < chanNames.length; iC++){
				FastSpecEntry specEntry = config.getPointByName(chanNames[iC]);
				Field field = specEntry.getClass().getDeclaredField(memberName);
				
				
				if(config.saveOnlyGoodFrames){
					double[] fullData = new double[nImages];
					
					int j=0;
					for(int i=0; i < nImages; i++){
						fullData[i] = goodFrames[i] ? data[iC][j++] : Double.NaN;
							
					}

					field.set(specEntry, fullData);
				}else{
					if(data[iC].length != nImages)
						throw new RuntimeException("Number of frames mismatches");
					
					field.set(specEntry, data[iC]);
				}				
				
			}
			return;
		}catch(Exception err){
			Logger.getLogger(this.getClass().getName()).warning("Unable to load signal for " + memberName + ", setting NaN: " + err.getMessage());
		}		
		
		int n = connectedSource.getNumImages();
		for(FastSpecEntry specEntry : config.specEntries){
			try {
				Field field = specEntry.getClass().getDeclaredField(memberName);
				Object o = field.get(specEntry);
				if(o == null){
					field.set(specEntry, Mat.fillArray(Double.NaN, n));
				}
			
			} catch (Exception err) { 
				Logger.getLogger(this.getClass().getName()).warning("Unable to set " + memberName + " to NaN: " + err.getMessage());
			}			
		}
	}
	
	@Override
	protected void doCalc(boolean singleFrame) {
		try{
			nbiCalc.doCalc();
			besCalc.doCalc();
			
			super.doCalc(singleFrame);
			
		}catch(RuntimeException err){
			err.printStackTrace();
			setStatus("ERROR: " + err);
		}
	}

	public FastSpecConfigW7X getConfigW7X() { return (FastSpecConfigW7X)config; }

	public BeamEmissionCalc getBESCalc(){ return besCalc; }
	public IonTempCalc getIonTempCalc(){ return ionTempCalc; }
	public PassiveInitialiser getPassiveInit() { return passiveInit; }

}
