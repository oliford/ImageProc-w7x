package imageProc.proc.fastSpec;

import imageProc.proc.fastSpec.bes.BeamEmissionConfig;
import imageProc.proc.fastSpec.ionTemp.IonTempConfig;
import imageProc.proc.fastSpec.nbiSpec.NeutraliserSpecConfig;
import imageProc.proc.fastSpec.passiveInit.PassiveInitialiserConfig;
import imageProc.proc.fastSpec.radiance.RadianceConfig;
import imageProc.proc.obsPos.FastSpecObsPosConfig;

public class FastSpecConfigW7X extends FastSpecConfig {
	

	public FastSpecConfigW7X() {
		super();
	}
	
	
	/** Path in ArchiveDB */
	public String saveResultsPath = null;
	
	/** Version in ArchiveDB */
	public int saveResultsVersion = 0;

	/** Description of version/changes */
	public String saveResultsDescription;
	
	/** Datastream version of last results set saved, for information/GUI */
	public int lastVersionSaved = -1;
		
	public boolean saveHHeRatio = false;
	
	/** Point this alias at the written version in saveResultsPath for the same time range */
	public String aliasPath = null;
	
	/** Set the alias once the data is written */
	public boolean setAliasOnWrite = false;

	public NeutraliserSpecConfig neutralSpec = new NeutraliserSpecConfig();
	
	public BeamEmissionConfig bes = new BeamEmissionConfig();
	
	public IonTempConfig ionTemp = new IonTempConfig();
	
	public RadianceConfig radianceConfig = new RadianceConfig();
	
	public FastSpecObsPosConfig obsPos = new FastSpecObsPosConfig();
	
	public PassiveInitialiserConfig passiveInit = new PassiveInitialiserConfig();

	/** Whether to save the datastream of the Gaussian 
	 * exponent (usually fixed to 2.0 for an actual Gaussian) */
	public boolean savePower = false;
	
	/** Save the source metadata */
	boolean saveSourceMetadata = true;
	
	/** Only save timeseries source metadata that matches any of these regexs */
	String[] saveTimeSeriesPatterns = new String[0]; //Seems excessive otherwise

	/** Thresohold for instensity of He above which to actually calculate H/He ratio */
	public int heliumIntensityThreshold = 10;

	public double beamOnPowerThreshold = 50000;

	/** When writing an alias for an acqusition in a W7X program, write the alias
	 * to cover the whole program, otherwise it only covers the range of 
	 * the first/last written images. */
	public boolean writeAliasForWholeProgram = true;
	
}
