package imageProc.proc.fastSpec.nbiSpec;

import algorithmrepository.Algorithms;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Conversion of NBI neutraliser spectroscopy instensity fractions to
 * particle density, current and power fractions
 * 
 * The current fraction is BEFORE the neutraliser - i.e. the fraction of extracted current carried by each species (not yet dissociated), including particles that are eventually going to the ion dumps
 * The neutral density fraction is AFTER the neutraliser - i.e. the density of dissociated single neutrals carrying each energy, just after the neutraliser
 * The power fractions are the power carried by neutral particles, after the neutrlaiser. (see neutral density fractions)  
 * 
 * Data (and complex calculations arriving at them) provided by Niek Den Harder (IPP Garching)
 * @author specadmin
 *
 */
public class W7XNBISpeciesFractions {
	// E (keV) C1 C2 C3 fnE1 fnE2 FnE3
	private static final double interpTable[][] = Mat.transpose(new double[][]{
			{ 2.000000000000000000e+01, 1.000000000000000000e+00, 5.176387549369895202e-01, 4.054277162835575599e-01, 8.201306342115598502e-01, 1.536228707049108300e+00, 2.289107576610655670e+00},
			{ 2.500000000000000000e+01, 1.000000000000000000e+00, 5.224799765513641336e-01, 3.868896277630182734e-01, 7.749843609730727723e-01, 1.558540733814073542e+00, 2.312163487453950950e+00},
			{ 3.000000000000000000e+01, 1.000000000000000000e+00, 5.300648286593928615e-01, 3.670719355288722952e-01, 7.252154527380385574e-01, 1.573288587868217681e+00, 2.331779529603498435e+00},
			{ 3.500000000000000000e+01, 1.000000000000000000e+00, 5.256562631107353223e-01, 3.460518635009359745e-01, 6.727841655427119205e-01, 1.571551788469365052e+00, 2.352561884960860272e+00},
			{ 4.000000000000000000e+01, 1.000000000000000000e+00, 4.842361482029167163e-01, 3.189116428598836595e-01, 6.193584737571562471e-01, 1.554499616150067043e+00, 2.368882944520582345e+00},
			{ 4.500000000000000000e+01, 1.000000000000000000e+00, 4.379789736042398740e-01, 3.020405789726730572e-01, 5.664289434370145537e-01, 1.526151294480677434e+00, 2.375931512036321358e+00},
			{ 5.000000000000000000e+01, 1.000000000000000000e+00, 3.937216914190263539e-01, 2.796501693532467692e-01, 4.797177706265478569e-01, 1.490150641623964711e+00, 2.372128601329607900e+00},
			{ 5.500000000000000000e+01, 1.000000000000000000e+00, 3.566913854988562615e-01, 2.525697784852247718e-01, 4.402911078331715466e-01, 1.448846394044206454e+00, 2.358089146692895710e+00},
			{ 6.000000000000000000e+01, 1.000000000000000000e+00, 3.112159906115218866e-01, 2.207208346919648789e-01, 4.026666801808443386e-01, 1.404190055321983044e+00, 2.335350301434213183e+00},
			{ 6.500000000000000000e+01, 1.000000000000000000e+00, 2.961753701390707483e-01, 2.020812136644217216e-01, 3.674869051846165191e-01, 1.357376732730549929e+00, 2.305612695481281893e+00},
			{ 7.000000000000000000e+01, 1.000000000000000000e+00, 2.748240235010480093e-01, 1.799525052714358586e-01, 3.350814350903976169e-01, 1.309203346494978382e+00, 2.270412172778263127e+00},
			{ 7.500000000000000000e+01, 1.000000000000000000e+00, 2.549854842678026534e-01, 1.626781295674765704e-01, 3.055557233071473644e-01, 1.260205518480476572e+00, 2.231028329622964712e+00},
			{ 8.000000000000000000e+01, 1.000000000000000000e+00, 2.341930305383306821e-01, 1.443581503481136119e-01, 2.788645531885288187e-01, 1.210766791787312835e+00, 2.188211879567434792e+00},
			{ 8.500000000000000000e+01, 1.000000000000000000e+00, 2.118013120522751203e-01, 1.256064044107468580e-01, 2.548685138477945755e-01, 1.161200145491884106e+00, 2.142956112283915360e+00},
			{ 9.000000000000000000e+01, 1.000000000000000000e+00, 1.910253380233046527e-01, 1.094002123510493252e-01, 2.333751222896994493e-01, 1.111794890529927304e+00, 2.095898662851407579e+00},
			{ 9.500000000000000000e+01, 1.000000000000000000e+00, 1.759210632101977367e-01, 9.831195053673526252e-02, 2.172822868632283411e-01, 1.062831715917731845e+00, 2.047545726877728178e+00},
		});
	
	private double[] mulAndNorm(double a[], double b[]){
		double sum = 0;
		for(int j=0; j < a.length; j++){
			a[j] *= b[j];
			sum += a[j];
		}
		for(int j=0; j < a.length; j++)
			a[j] /= sum;
		return a;
	}
			
	private double[] interpFractions(double energyKV, int tabIdx){
		int i = Mat.getNearestLowerIndex(interpTable[0], energyKV);
		double f = (energyKV - interpTable[0][i]) / (interpTable[0][i+1] - interpTable[0][i]);
		
		double ret[] = new double[3];
		for(int j=0; j < 3; j++)		
			ret[j] = (1.0 - f) * interpTable[tabIdx+j][i] + f * interpTable[tabIdx+j][i+1];
				
		return ret;
	}

	/** * The current fraction is BEFORE the neutraliser - i.e. the fraction of extracted current 
	 *  carried by each species (not yet dissociated), including particles that are eventually going to the ion dumps */
	public double[] getCurrentFractions(double energyKV, double intensityFractions[]) {
		double C[] = interpFractions(energyKV, 1);
		return mulAndNorm(C, intensityFractions);
	}
	
	/** The neutral density fraction is AFTER the neutraliser - i.e. the density of dissociated single neutrals carrying each energy, just after the neutraliser */
	public double[] getNeutralParticleFractions(double energyKV, double intensityFractions[]) {
		double currentFractions[] = getCurrentFractions(energyKV, intensityFractions);
		double C[] = interpFractions(energyKV, 4);
		return mulAndNorm(C, currentFractions);
	}	

	/** The power fractions are the power carried by neutral particles, after the neutrlaiser. (see neutral density fractions)  */
	public double[] getPowerFractions(double energyKV, double intensityFractions[]) {
		double npFrac[] = getNeutralParticleFractions(energyKV, intensityFractions);		
		return mulAndNorm(npFrac, new double[]{ 1.0, 1.0/2.0, 1.0/3.0 });
	}
	
	public static void main(String[] args) {
		double E = 44;
		double iFrac[] = {41.49603305,  64.77852863,  18.18487787};
		
		W7XNBISpeciesFractions specInterp = new W7XNBISpeciesFractions();
		
		System.out.print("current frac: ");
		Mat.dumpArray(specInterp.getCurrentFractions(E, iFrac));
		
		System.out.print("particle frac: ");
		Mat.dumpArray(specInterp.getNeutralParticleFractions(E, iFrac));
				
		System.out.print("power frac: ");
		Mat.dumpArray(specInterp.getPowerFractions(E, iFrac));
		
	}
	
	
}
