package imageProc.proc.fastSpec.nbiSpec;

import java.util.HashMap;
import java.util.logging.Logger;

import com.google.re2j.Pattern;

import algorithmrepository.Algorithms;
import algorithmrepository.LinearInterpolation1D;
import de.mpg.ipp.codac.w7xtime.NanoTime;
import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.fastSpec.FastSpecProcessor.CalcStage;
import imageProc.w7x.W7XUtil;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class NeutraliserSpecCalc {

	private final double electronCharge = 1.60217662e-19;
	private final double protonMass = 1.6726219e-27; //kg
	private final double c = 299792458;
	private final double lambdaHalpha = 656.279; 
	private static W7XNBISpeciesFractions nbiSpeciesInterp = new W7XNBISpeciesFractions();
	
	private FastSpecProcessorW7X proc; 

	public NeutraliserSpecCalc(FastSpecProcessorW7X proc) {
		this.proc = proc;
	}

	private void adjustNBIEnergyComponents() {
		proc.setStatus("NSpec: Setting wavelength windows from beam voltage.");
		
		HashMap <Integer, Double> sourceVoltageSignals = new HashMap<>();

		//get voltage signal
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();

		for(FastSpecEntry entry : configW7X.specEntries){
			if(entry.id.startsWith("E")){
				if(!entry.channelID.matches("[SQ][0-9]*"))
					throw new RuntimeException("Unable to identify a beam index for entry " + entry.id + ". Set the channel ID, e.g. to S7");
				
				int beamIndex = Algorithms.mustParseInt(entry.channelID.substring(1)) - 1;				
					
				Double beamVoltage = sourceVoltageSignals.get(beamIndex);
				if(beamVoltage == null) {
					
					Object[] ret = W7XUtil.getBeamIVSignals(proc.getConnectedSource(), beamIndex);
					double[] V = (double[]) ret[1];
					beamVoltage = Mat.max(V);
					
					sourceVoltageSignals.put(beamIndex, beamVoltage);
					
				}
				
				if(beamVoltage < 5000) {
					Logger.getLogger(this.getClass().getName()).warning("Beam voltage for S"+(beamIndex+1)+" under 5kV, not adjusting windows");
					continue;
				}
			
				int frac = Algorithms.mustParseInt(entry.id.split("_")[0].substring(1));
				double dl = calcWavelengthShiftFromBeamEnergy(beamVoltage / frac);
				entry.x0 = lambdaHalpha + (dl * configW7X.neutralSpec.nbiNeutraliserWavelengthWindows[frac-1][0]);
				entry.x1 = lambdaHalpha + (dl * configW7X.neutralSpec.nbiNeutraliserWavelengthWindows[frac-1][2]);

				//restrict fitting to a bit inside that
				entry.x0Min = lambdaHalpha + (dl * configW7X.neutralSpec.nbiNeutraliserWavelengthWindows[frac-1][0]);
				entry.x0Max = lambdaHalpha + (dl * configW7X.neutralSpec.nbiNeutraliserWavelengthWindows[frac-1][2]);
				entry.x0Init = lambdaHalpha + (dl * configW7X.neutralSpec.nbiNeutraliserWavelengthWindows[frac-1][1]);

			}
		}
	}

	public void inferredQuantities(CalcStage stage, FastSpecEntry specEntryOnly, int singleFrame) {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		if(stage == CalcStage.Prepare) {
			if(!configW7X.inferredQuantities.contains("CurrentFractions"))
				configW7X.inferredQuantities.add("CurrentFractions");
			if(!configW7X.inferredQuantities.contains("NeutralParticleFractions"))
				configW7X.inferredQuantities.add("NeutralParticleFractions");
			if(!configW7X.inferredQuantities.contains("PowerFractions"))
				configW7X.inferredQuantities.add("PowerFractions");
			return;
		}
		
		//if(stage != CalcStage.PostProcess)
			//return;
		
		
		for(FastSpecEntry specEntry : configW7X.specEntries) {
			if(specEntryOnly == null || specEntryOnly == specEntry)
				calcNBIBeamEnergy(specEntry, singleFrame);
		}

		//involves summation
		calcIntensityFractions(stage, specEntryOnly, singleFrame);
		
		for(int iB=0; iB < 8; iB++) {
			//find the 3 energy components (for this beam)
			FastSpecEntry species[] = new FastSpecEntry[3];
			int found = 0;
			for(FastSpecEntry specEntry : configW7X.specEntries)
				for(int j=0; j < 3; j++)
					if(specEntry.id.startsWith("E" + (j+1)) && specEntry.channelID.startsWith("S" + (iB+1))) {
						species[j] = specEntry;
						found++;
					}
			
			if(found == 0) {
				Logger.getLogger(this.getClass().getName()).fine("No specEntries found for beam " + (iB+1));
				continue;
			}				
	
			int nFrames = species[0].fit.Amplitude.length;
		
			long[] beamVTime;
			double[] beamV;// = species[0].inferredQuantities.get("BeamEnergy");
			Object[] ret = W7XUtil.getBeamIVSignals(proc.getConnectedSource(), iB);
			beamVTime = (long[]) ret[0];
			beamV = (double[]) ret[1];			
			
			long nanoTime[] = W7XUtil.getBestNanotime(proc.getConnectedSource());
	
			double currFrac[][] = new double[3][];
			double partFrac[][] = new double[3][];
			double powerFrac[][] = new double[3][];
			
			for(int j=0; j < 3; j++){
				currFrac[j] = species[j].fit.inferredQuantities.get("CurrentFractions");
				partFrac[j] = species[j].fit.inferredQuantities.get("NeutralParticleFractions");
				powerFrac[j] = species[j].fit.inferredQuantities.get("PowerFractions");
				
				if(currFrac[j] == null) currFrac[j] = Mat.fillArray(Double.NaN, nFrames);
				if(partFrac[j] == null) partFrac[j] = Mat.fillArray(Double.NaN, nFrames);
				if(powerFrac[j] == null) powerFrac[j] = Mat.fillArray(Double.NaN, nFrames);
		
			}
	
			//foreach frame
			for(int iF=0; iF < nFrames; iF++){
				if(singleFrame >= 0 && iF != singleFrame)
					continue;
				
				//find max beam energy within frame time 
				long dt = (iF==0) ? (nanoTime[1] - nanoTime[0]) : (nanoTime[iF] - nanoTime[iF-1]);			
				int beamVIdx0 = Mat.getNearestIndex(beamVTime, nanoTime[iF] - dt/2);
				int beamVIdx1 = Mat.getNearestIndex(beamVTime, nanoTime[iF] + dt/2);
				double energyKV = Double.NEGATIVE_INFINITY;
				for(int j=beamVIdx0; j <= beamVIdx1; j++){
					if(beamV[j] > energyKV)
						energyKV = beamV[j];
				}
				energyKV /= 1e3;
				
				double intensityFractions[] = new double[3];
				for(int j=0; j < 3; j++)
					intensityFractions[j] = species[j].fit.Amplitude[iF];
							
				proc.getConnectedSource().setImageMetaData("energyKV", iF, energyKV);
				
				if(energyKV < 5.0){ //probably dead
					for(int j=0; j < 3; j++) {
						currFrac[j][iF] = Double.NaN;
						partFrac[j][iF] = Double.NaN;
						powerFrac[j][iF] = Double.NaN;
					}
				}else{
					double[] currF = nbiSpeciesInterp.getCurrentFractions(energyKV, intensityFractions);
					double[] partF = nbiSpeciesInterp.getNeutralParticleFractions(energyKV, intensityFractions);
					double[] powrF = nbiSpeciesInterp.getPowerFractions(energyKV, intensityFractions);
					
					for(int j=0; j < 3; j++) {
						currFrac[j][iF] = currF[j];
						partFrac[j][iF] = partF[j];
						powerFrac[j][iF] = powrF[j]; 
					}
				}
			}	
	
			for(int j=0; j < 3; j++){
				species[j].fit.inferredQuantities.put("CurrentFractions", currFrac[j]);
				species[j].fit.inferredQuantities.put("NeutralParticleFractions", partFrac[j]);
				species[j].fit.inferredQuantities.put("PowerFractions", powerFrac[j]);
			}
	
			
		}
	}

	/** Calculate NBI energy from fit wavelength and divergence from fit sigma 
	 * @param singleFrame */ 
	private double[] calcNBIBeamEnergy(FastSpecEntry specEntry, int singleFrame){
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		double instrumentSigmaSq = FastMath.pow2(configW7X.neutralSpec.nbiNeutraliserInstrumentFuncSigma);
				
		int nFrames = specEntry.fit.X0.length;

		double[] data = specEntry.fit.inferredQuantities.get("BeamEnergy");
		double[] dataSigma = specEntry.fit.inferredQuantities.get("BeamEnergy_uncertainty");
		double[] divergence = specEntry.fit.inferredQuantities.get("Divergence");	
		double[] divergenceSigma = specEntry.fit.inferredQuantities.get("Divergence_uncertainty");
		
		if(data == null) data = Mat.fillArray(Double.NaN, nFrames);
		if(dataSigma == null) dataSigma = Mat.fillArray(Double.NaN, nFrames);
		if(divergence == null) divergence = Mat.fillArray(Double.NaN, nFrames);
		if(divergenceSigma == null) divergenceSigma = Mat.fillArray(Double.NaN, nFrames);
		
		FastSpecEntry specEntryHalpha = null;
		if(configW7X.neutralSpec.energyRelativeToHalpha) {
			for(FastSpecEntry sp : configW7X.specEntries) {
				if(sp.id.startsWith("Halpha") && sp.channelID.equals(specEntry.channelID)) {
					specEntryHalpha = sp;
					break;
				}
			}
			
			if(specEntryHalpha == null) {
				Logger.getLogger(this.getClass().getName()).warning("No matching 'Halpha' entry found for " + specEntry.id + ", assuming proper Halpha wavelength. Beam voltage will not be corrected for spectrometer drift!");
			}
		}
		
		for(int i=0; i < nFrames; i++){
			if(singleFrame >= 0 && singleFrame != i)
				continue;
			
			double lambdaHalphaFit = (specEntryHalpha != null) ? specEntryHalpha.fit.X0[i] : lambdaHalpha;
			
			// Beam energy first
			double velocityLOS = c * (specEntry.fit.X0[i] - lambdaHalphaFit) / lambdaHalpha;
			double velocityBeam = velocityLOS / FastMath.cos(configW7X.neutralSpec.nbiNeutraliserOpticAngle);
			double beamEnergy = 0.5 * protonMass * velocityBeam*velocityBeam / electronCharge;
			
			double beamEnergySigma = 2 * beamEnergy * specEntry.fit.UncertaintyX0[i] / (specEntry.fit.X0[i] - lambdaHalpha);
						
			if(specEntry.fit.Amplitude[i] < 1.0
					|| (specEntry.fit.Sigma[i]*2.35) < 0.02
					|| beamEnergy > 200000){
				 beamEnergy = Double.NaN;
				 beamEnergySigma = Double.NaN;
			}
			data[i] = beamEnergy;
			dataSigma[i] = beamEnergySigma; 
			
			
			//divergence
			double sigma = FastMath.sqrt(FastMath.pow2(specEntry.fit.Sigma[i]) - instrumentSigmaSq);
			divergence[i] = (c/velocityBeam) * (sigma / specEntry.fit.X0[i]) / FastMath.sin(configW7X.neutralSpec.nbiNeutraliserOpticAngle);
			divergenceSigma[i] = divergence[i] * (specEntry.fit.UncertaintySigma[i] / sigma);
			
			divergence[i] *= 180 / Math.PI;
			divergenceSigma[i] *= 180 / Math.PI;
			
		}	

		specEntry.fit.inferredQuantities.put("BeamEnergy", data);
		specEntry.fit.inferredQuantities.put("BeamEnergy_uncertainty", dataSigma);
		specEntry.fit.inferredQuantities.put("Divergence", divergence);	
		specEntry.fit.inferredQuantities.put("Divergence_uncertainty", divergenceSigma);
		
		
		if(!configW7X.inferredQuantities.contains("BeamEnergy"))
			configW7X.inferredQuantities.add("BeamEnergy");
		if(!configW7X.inferredQuantities.contains("BeamEnergy_uncertainty"))
			configW7X.inferredQuantities.add("BeamEnergy_uncertainty");
		if(!configW7X.inferredQuantities.contains("Divergence"))
			configW7X.inferredQuantities.add("Divergence");
		if(!configW7X.inferredQuantities.contains("Divergence_uncertainty"))
			configW7X.inferredQuantities.add("Divergence_uncertainty");		
		
		return data;
	}
	
	private double calcWavelengthShiftFromBeamEnergy(double beamEnergy){
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		double velocityBeam = FastMath.sqrt(beamEnergy * electronCharge * 2 /protonMass);		
		double velocityLOS = velocityBeam * FastMath.cos(configW7X.neutralSpec.nbiNeutraliserOpticAngle);
		
		return (velocityLOS * lambdaHalpha / c);
	}
	
	private void calcIntensityFractions(CalcStage stage, FastSpecEntry specEntryOnly, int singleFrame){
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		int nFrames = proc.getConnectedSource().getNumImages();
		int iB = -1;
				
		double sum[][] = new double[9][nFrames];
		for(FastSpecEntry specEntry : configW7X.specEntries){
			if(specEntry.id.startsWith("E")){
				if(!specEntry.channelID.startsWith("S"))
					throw new RuntimeException("Channels IDs must be S[0-9] to specify beam");
				
				iB = Algorithms.mustParseInt(specEntry.channelID.substring(1));
				
				for(int i=0; i < nFrames; i++)							
					sum[iB][i] += specEntry.fit.Amplitude[i];
			}
		}
		
		for(FastSpecEntry specEntry : configW7X.specEntries){
			if(specEntry.id.startsWith("E")){
				iB = Algorithms.mustParseInt(specEntry.channelID.substring(1));
				
				double d[] = new double[nFrames]; 
				for(int i=0; i < nFrames; i++){
					d[i] = specEntry.fit.Amplitude[i] / sum[iB][i];
				}	
				
				specEntry.fit.inferredQuantities.put("IntensityFraction", d);
				if(!configW7X.inferredQuantities.contains("IntensityFraction"))
					configW7X.inferredQuantities.add("IntensityFraction");
				
			}
			
			if(specEntry.id.startsWith("E1")){				
				specEntry.fit.inferredQuantities.put("IntensitySum", sum[iB]);
				if(!configW7X.inferredQuantities.contains("IntensitySum"))
					configW7X.inferredQuantities.add("IntensitySum");				
			}
		}
	}

	public void doCalc() {
		//For NBI processing, automatically set energy ranges
		if(((FastSpecConfigW7X)proc.getConfig()).neutralSpec.adjustNBIEnergyComponents){
			try{
				adjustNBIEnergyComponents();			
			}catch(RuntimeException err){
				err.printStackTrace();
			}
		}
	}

	public void saveResults() {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		proc.saveFitResults(configW7X.saveResultsPath + "_BeamEnergies_DATASTREAM", "BeamEnergy");
		proc.saveFitResults(configW7X.saveResultsPath + "_BeamEnergies_uncertainty_DATASTREAM", "BeamEnergy_uncertainty");
		proc.saveFitResults(configW7X.saveResultsPath + "_CurrentFractions_DATASTREAM", "CurrentFractions");
		proc.saveFitResults(configW7X.saveResultsPath + "_NeutralParticleFractions_DATASTREAM", "NeutralParticleFractions");
		proc.saveFitResults(configW7X.saveResultsPath + "_PowerFractions_DATASTREAM", "PowerFractions");
		proc.saveFitResults(configW7X.saveResultsPath + "_Divergence_DATASTREAM", "Divergence");
		proc.saveFitResults(configW7X.saveResultsPath + "_Divergence_uncertainty_DATASTREAM", "Divergence_uncertainty");
		
	}	
}
