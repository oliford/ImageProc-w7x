package imageProc.proc.fastSpec.nbiSpec;


/** Autoconfig parameters for neutraliser spectroscopy 
 * These are used to generate/modify the main parameters in FastSpecConfigW7X 
 */
public class NeutraliserSpecConfig {
	/** Set boxes for energy components 'E1', 'E2', 'E3' to their theory values from the dialed in votlage */ 
	public boolean adjustNBIEnergyComponents = false;

	public boolean saveNBIComponentProperties = false;
	
	/** Calculate the beam energy from the wavlength realteive to the Halpha, to correct for small spectrometer drifts */
	public boolean energyRelativeToHalpha = true;

	public double nbiNeutraliserOpticAngle = 50 * Math.PI / 180; //From CAD drawings. Really should never change
	public double nbiNeutraliserInstrumentFuncSigma = 0.03; // From Ne Cal 27.Jul.2018, TODO: goes into configW7X

	/** Wavelength windows of each energy component for neutraliser spectroscopy
	 * as multiplier to theoretically calculated Doppler shift wavelength. In nm. [E,E/2,E/3][min/init/max] */
	public double nbiNeutraliserWavelengthWindows[][] = { 
			{ 0.9, 1.0, 1.1 },
			{ 0.9, 1.0, 1.1 },
			{ 0.9, 1.0, 1.1 } };


	/* ----- Beam emission spectroscopy specifics ------ */
	public boolean adjustBESComponents = false;

}
