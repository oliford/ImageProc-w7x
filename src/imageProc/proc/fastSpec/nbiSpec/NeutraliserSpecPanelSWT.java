package imageProc.proc.fastSpec.nbiSpec;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.fastSpec.swt.FastSpecSpecificPanel;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class NeutraliserSpecPanelSWT extends FastSpecSpecificPanel {
	private Button saveNBIInfoCheckbox;
	private Text nbiNeutraliserOpticAngleTextbox;	
	private Button adjustNBIEnergyComponentsCheckbox;
	private Button energyRelativeToHalphaCheckbox;
	private Text adjustNBIEnergyWindows[][];
	private boolean inhibitSettingsChanged;
	
	private FastSpecProcessorW7X proc;
	
	public NeutraliserSpecPanelSWT(FastSpecProcessorW7X proc, Composite parent) {
		this.proc = proc;
		
		Label lAC = new Label(parent, SWT.NONE); lAC.setText("");
		adjustNBIEnergyComponentsCheckbox = new Button(parent, SWT.CHECK);
		adjustNBIEnergyComponentsCheckbox.setText("Voltage relative windows:");
		adjustNBIEnergyComponentsCheckbox.setToolTipText("For entries where theoryWavelength is outside x0-x1 window, multiply x parameters (window, init, min/max etc) by beam energy in eV (for Beam emission).");
		adjustNBIEnergyComponentsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		adjustNBIEnergyComponentsCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Group adjustNBIEnergyWindowsGroup = new Group(parent, SWT.NONE);
		adjustNBIEnergyWindowsGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		adjustNBIEnergyWindowsGroup.setLayout(new GridLayout(6, true));
		
		adjustNBIEnergyWindows = new Text[3][3];
		for(int i=0; i < 3; i++){
			Label l = new Label(adjustNBIEnergyWindowsGroup, SWT.NONE);
			l.setText("E/" + i + "   Min:");		
		
			adjustNBIEnergyWindows[i][0] = new Text(adjustNBIEnergyWindowsGroup, SWT.NONE);
			adjustNBIEnergyWindows[i][0].setToolTipText("Multiplier to theoretical Doppler shift for minimum edge of fitting box");
			adjustNBIEnergyWindows[i][0].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			adjustNBIEnergyWindows[i][0].addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

			Label lI = new Label(adjustNBIEnergyWindowsGroup, SWT.NONE);
			lI.setText("Init:");		
		
			adjustNBIEnergyWindows[i][1] = new Text(adjustNBIEnergyWindowsGroup, SWT.NONE);
			adjustNBIEnergyWindows[i][1].setToolTipText("Multiplier to theoretical Doppler shift for initial value");
			adjustNBIEnergyWindows[i][1].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			adjustNBIEnergyWindows[i][1].addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

			Label lX = new Label(adjustNBIEnergyWindowsGroup, SWT.NONE);
			lX.setText("Max:");		
		
			adjustNBIEnergyWindows[i][2] = new Text(adjustNBIEnergyWindowsGroup, SWT.NONE);
			adjustNBIEnergyWindows[i][2].setToolTipText("Multiplier to theoretical Doppler shift for max edge of fitting box");
			adjustNBIEnergyWindows[i][2].setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
			adjustNBIEnergyWindows[i][2].addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		}
				
		Label lNI = new Label(parent, SWT.NONE); lNI.setText("");
		saveNBIInfoCheckbox = new Button(parent, SWT.CHECK);
		saveNBIInfoCheckbox.setText("Save NBI Component info");
		saveNBIInfoCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		saveNBIInfoCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lOA = new Label(parent, SWT.NONE); lOA.setText("Optic angle (deg):");
		nbiNeutraliserOpticAngleTextbox = new Text(parent, SWT.CHECK);
		nbiNeutraliserOpticAngleTextbox.setText("50.00");
		nbiNeutraliserOpticAngleTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		nbiNeutraliserOpticAngleTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lER = new Label(parent, SWT.NONE); lER.setText("");
		energyRelativeToHalphaCheckbox = new Button(parent, SWT.CHECK);
		energyRelativeToHalphaCheckbox.setText("Energy from wavelength relative to Halpha:");
		energyRelativeToHalphaCheckbox.setToolTipText("");
		energyRelativeToHalphaCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		energyRelativeToHalphaCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
	}
	
	@Override
	public void doUpdate() {
		inhibitSettingsChanged = true;
		try{
			FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
			saveNBIInfoCheckbox.setSelection(configW7X.neutralSpec.saveNBIComponentProperties);
			adjustNBIEnergyComponentsCheckbox.setSelection(configW7X.neutralSpec.adjustNBIEnergyComponents);
			if(!nbiNeutraliserOpticAngleTextbox.isFocusControl())
				nbiNeutraliserOpticAngleTextbox.setText(String.format("%5.02f", configW7X.neutralSpec.nbiNeutraliserOpticAngle * 180.0 / Math.PI));
			energyRelativeToHalphaCheckbox.setSelection(configW7X.neutralSpec.energyRelativeToHalpha);
			

			for(int i=0; i < adjustNBIEnergyWindows.length; i++){
				for(int j=0; j < adjustNBIEnergyWindows[i].length; j++){
					adjustNBIEnergyWindows[i][j].setText(Double.toString(configW7X.neutralSpec.nbiNeutraliserWavelengthWindows[i][j]));
				}
			}
			
		}finally{
			inhibitSettingsChanged = false;
		}
	}
	
	private void settingsChangedEvent(Event e) {
		if(inhibitSettingsChanged)
			return;
		
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		configW7X.neutralSpec.saveNBIComponentProperties = saveNBIInfoCheckbox.getSelection();
		configW7X.neutralSpec.adjustNBIEnergyComponents = adjustNBIEnergyComponentsCheckbox.getSelection();
		configW7X.neutralSpec.nbiNeutraliserOpticAngle = Algorithms.mustParseDouble(nbiNeutraliserOpticAngleTextbox.getText()) * Math.PI / 180.0;
		configW7X.neutralSpec.energyRelativeToHalpha = energyRelativeToHalphaCheckbox.getSelection();
		
		for(int i=0; i < adjustNBIEnergyWindows.length; i++){
			for(int j=0; j < adjustNBIEnergyWindows[i].length; j++){
				configW7X.neutralSpec.nbiNeutraliserWavelengthWindows[i][j] = Algorithms.mustParseDouble(adjustNBIEnergyWindows[i][j].getText());
			}
		}
	}
}
