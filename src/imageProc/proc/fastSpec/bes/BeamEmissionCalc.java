package imageProc.proc.fastSpec.bes;

import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import algorithmrepository.Algorithms;
import descriptors.w7x.ArchiveDBDesc;
import fusionOptics.Util;
import imageProc.proc.fastSpec.FastSpecConfig.AmpFitMode;
import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.w7x.W7XUtil;
import ipp.w7x.vmec.server.Vmec;
import ipp.w7x.vmec.server.Vmec_Service;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import seed.ws.math.types.Points3D;
import signals.w7x.ArchiveDB.Database;
import w7x.archive.ArchiveDBFetcher;

public class BeamEmissionCalc {

	private FastSpecProcessorW7X proc;

	public BeamEmissionCalc(FastSpecProcessorW7X proc) {
		this.proc = proc;
	}
	
	private void setupComponents() {
		final double electronCharge = 1.60217662e-19;
		final double protonMass = 1.6726219e-27; //kg
		final double c = 299792458;
		final double lambdaHalpha = 656.279; 
		final double bohrRadius = 0.52917721067e-10;
		final double plancksConstant = 6.62607004e-34;
		
		//TODO: Get these from somewhere
		double[][] beamStart = { 
				{ 3.4801232483787587,    5.144471596167879, -0.305},
				{ 3.415671511324611,    5.17542087946828, -0.305},
				{ 3.415671511324611,    5.17542087946828, -0.305},
				{ 3.4801232483787587,    5.144471596167879, -0.305},

				{ 0.2083656846072355,    6.207530069936235, 0.305},
				{ 0.27869976749402436,    6.194684785564924, 0.305},
				{ 0.27869976749402436,    6.194684785564924, 0.305},
				{ 0.2083656846072355,    6.207530069936235, 0.305},
			};
		
		double beamUVec[][] = {
				{ -0.3659706092386248,    -0.926698150648448, -0.08541692313736746},
				{ -0.4944029793290909,    -0.8650258049747784, -0.08541692313736746},
				{ -0.4944029793290909,    -0.8650258049747784, 0.08541692313736746},
				{ -0.3659706092386248,    -0.926698150648448, 0.08541692313736746},
				
				{ -0.2486230639620658,   -0.9648266794133848, 0.08541692313736746},
				{ -0.10846899866975897,    -0.9904233567365652, 0.08541692313736746},
				{ -0.10846899866975897,    -0.9904233567365652, -0.08541692313736746},
				{ -0.2486230639620658,    -0.9648266794133848, -0.08541692313736746},
			};
		
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X) proc.getConfig();
		BeamEmissionConfig cfg = configW7X.bes; 
				
		// ---- To go into config ----
		/** Scales only the BES internsity, one per head port (according to portNames) */
		double[] cfgPerPortBESIntensityScale = { 1.0, 3.0, 1.0 };
		// ----------------------
		
		// Artificially expand errors near the cold Halpha, which isn't well described by a Gaussian
		
		configW7X.expandErrorsValue = 10000.0 / 35 * cfg.intensityScale;
		configW7X.expandErrorsX0 = 656.28;
		configW7X.expandErrorsXSigma = 0.15;
		
		//things no longer used, mark invalid so the user knows we won't use them, and didn't		
		cfg.beamFieldAngle = Double.NaN;
		cfg.ampInitM = null;
		//cfg.B = Double.NaN;
		
		//Names are e.g:
		// BES_Q#_E#_{Sigma,Pi+,Pi-}		
		String mseName[] = { "Pi-", "Sigma", "Pi+"  };
		
		//Grab LOS info written to metadata by SpecCalProcessor
		//Object losStartPos = proc.getSeriesMetaData("/SpecCal/linesOfSight/startPos");
		double[][] losUVec = (double[][])Mat.fixWeirdContainers(proc.getSeriesMetaData("/SpecCal/linesOfSight/unitVec"), true);
		if(losUVec == null)
			throw new RuntimeException("No line of sight data to calculate Doppler shift");
		
		for(int i=0; i < losUVec.length; i++){			
			losUVec[i] = Util.reNorm(Algorithms.rotateVectorAboutAxis(losUVec[i], 0.0 * Math.PI / 180, 2));
			losUVec[i] = Util.reNorm(Algorithms.rotateVector(
					Algorithms.rotationMatrix(beamUVec[7-1], 0.0 * Math.PI / 180), losUVec[i]));
		}
		
		//get observation position for angle between beam and field
		double[][] obsPos = (double[][])Mat.fixWeirdContainers(proc.getSeriesMetaData("/SpecCal/linesOfSight/nominalBeamIntersectionPos"), true);
		if(obsPos == null)
			throw new RuntimeException("No observation position data. Did you turn on 'Process beam data' in SpecCalProcessor?");
		
		String[] losNames = (String[])Mat.fixWeirdContainers(proc.getSeriesMetaData("/SpecCal/linesOfSight/names"), true);
		if(losNames == null)
			throw new RuntimeException("No line of sight data to calculate Doppler shift");

		//we need the channels names of each row, which is not necessary tied to number
		String[] lightPaths = (String[])Mat.fixWeirdContainers(proc.getSeriesMetaData("/SpecCal/linesOfSight/lightPaths"), true);
		if(lightPaths == null)
			throw new RuntimeException("No linePath data to figure out channels numbers");
		
		
		int nRows = proc.getConnectedSource().getImage(0).getHeight();
		int chanNums[] = new int[nRows];
		for(int i=0; i < nRows; i++){
			chanNums[i] = -1;
			if(lightPaths[i] == null)
				continue;
			
			String parts[] = lightPaths[i].split("=");
			parts[0] = parts[0].trim();
			parts = parts[0].split(":");
			if(parts.length > 0)
				chanNums[i] = Algorithms.mustParseInt(parts[1]);
		}
		
		double B[][] = null;
		
		if(configW7X.obsPos.vmecID != null && configW7X.obsPos.vmecID.equalsIgnoreCase("noField")){
			B = new double[obsPos.length][3];
		}else{
			
			String vmecID;
			if(configW7X.obsPos.vmecID == null || configW7X.obsPos.vmecID.startsWith("auto")){
				String pre = configW7X.obsPos.vmecID.replaceAll(":.*", "");
				vmecID = W7XUtil.getVMECBestID(proc.getConnectedSource(), 
						configW7X.obsPos.vmecID.startsWith("autoVacuum"));
				configW7X.obsPos.vmecID = pre.trim() + ":" + vmecID.trim();
			
			}else{
				vmecID = configW7X.obsPos.vmecID;
			}
			
			//get local magnetic field vector from VMEC for all observation points 
			Vmec_Service vmecService = new Vmec_Service();		
			Vmec vmec = vmecService.getVmecSOAP();
			
			Points3D p3d = new Points3D();
			
			double obsPosT[][] = Mat.transpose(obsPos);
			
			p3d.setX1(obsPosT[0]);
			p3d.setX2(obsPosT[1]);
			p3d.setX3(obsPosT[2]);
			
			Points3D b3d = vmec.magneticField(vmecID, p3d);
			
			B = Mat.transpose(new double[][]{ b3d.getX1(), b3d.getX2(), b3d.getX3() });

			if(cfg.B >= 2.2 && cfg.B < 2.9)
				throw new RuntimeException("With VMEC ID set, config.B is relative to the VMEC calculated value but the set value looks like a W7-X field value (2.1 - 2.9)");
			
		}

		
		for(int iB=7-1; iB<8; iB++){ //for each beam
			//Object[] ret = W7XUtil.getBeamIVSignals(proc.getConnectedSource(), 7);
			//double[] V = (double[]) ret[1];
			
			double P[] = (double[])Mat.fixWeirdContainers(proc.getSeriesMetaData("/w7x/nbi/beamPowerQ" + (iB+1)), true);
			
			int nF = proc.getConnectedSource().getNumImages();
			boolean entryEnable = false;
			boolean frameEnable[] = new boolean[nF];
			//determine which frames have this beam
			for(int iF=0; iF < nF; iF++){
				frameEnable[iF] = (P[iF] > cfg.beamPowerThreshold && !cfg.beamInhibit[iB]);
				entryEnable |= frameEnable[iF];
			}

			double V[] = (double[])Mat.fixWeirdContainers(proc.getSeriesMetaData("/w7x/nbi/beamEnergyQ" + (iB+1)), true);			
			double beamVoltage = Mat.max(V);
			
			
			for(int iRow=0; iRow < nRows; iRow++){ //for each channel
				
				//angle between LOS and beam axis
				double cosLOSBeam = Mat.dot(losUVec[iRow], beamUVec[iB]);				
				cosLOSBeam = FastMath.cos(FastMath.acos(cosLOSBeam));
				
				//find adjustment entry for this los set
				double adjustDoppler[][] = null;
				int losChan = -1;
				for(Entry<String, double[][]> entry : cfg.adjustDopplerTable.entrySet()){
					String entryMatch = entry.getKey();
					int beamMatch = -1;
					
					//is this a beam specific entry?
					if(entryMatch.contains("/Q")){
						String parts[] = entry.getKey().split("/Q");
						entryMatch = parts[0];
						beamMatch = Integer.parseInt(parts[1]) - 1;						
					}
					
					if(losNames[iRow].contains(entryMatch)){
						if(beamMatch >= 0 && beamMatch != iB)
								continue; //not for this beam						
						adjustDoppler = entry.getValue();
						String parts[] = losNames[iRow].split(":");
						losChan = Integer.parseInt(parts[1]);
					}
				}
				
				if(adjustDoppler == null){
					System.err.println("WARNING: No adjustDoppler entry for '"+losNames[iRow]+"'");
				}
				
				int iPort;
				for(iPort=0; iPort < cfg.portNames.length; iPort++){
					if(losNames[iRow].contains(cfg.portNames[iPort]))
						break;
				}
				if(iPort == cfg.portNames.length){
					System.err.println("WARNING: No BES portName entry for '"+losNames[iRow]+"'. Skipping channel.");
					continue;
				}
				
				double beamDensityProfile = 1.0 - ((double)losChan / 40.0) * (1.0 - cfg.shinethrough);
				if(beamDensityProfile < cfg.shinethrough) //sometimes losChan > 40
					beamDensityProfile = cfg.shinethrough;
				
				for(int iE=1; iE <= 3; iE++){ //for each beam energy component
					double vBeam = FastMath.sqrt(2 * beamVoltage/iE * electronCharge / protonMass);
					
					//E = v x B
					double v[] = Mat.mul(beamUVec[iB], vBeam);
					double E[] = Mat.cross3d(v, B[iRow]);
					double magE = Mat.veclength(E) 
							* cfg.B; //adjust B
					
					//angle between E and los
					double cos2LOSStark = FastMath.pow2(Mat.dot(losUVec[iRow], E) / magE);
					double sin2LOSStark = 1 - cos2LOSStark*cos2LOSStark;
					
					double ratioPiSigma = sin2LOSStark / (1 + cos2LOSStark);
			
					//ratioPiSigma *= 2;
					double ampInitM[];
					if(magE == 0){
						ampInitM = new double[]{ 0, 1, 0 }; //all sigma, no splitting anyway 
					}else{
						ampInitM = new double[]{ ratioPiSigma*0.5, 1.0, ratioPiSigma*0.5 };
						ampInitM = Mat.mul(ampInitM, 1.0/(1.0 + ratioPiSigma));
					}
										
					//double obsR = FastMath.sqrt(obsPos[iRow][0]*obsPos[iRow][0] + obsPos[iRow][1]*obsPos[iRow][1]);
					//System.out.println(losNames[iRow] + ", R = "+obsR+", Ipi/Isig = " + ratioPiSigma);
										
					double deltaSplit = 206.23 * (magE/1e6/1e9); //  units is 1/wavelength or 'per nm'
					
					
					for(int iM=-1; iM <= 1; iM++){ //for each MSE component
						String componentName = "BES_Q" + (iB+1) + "_E" + iE + "_" + mseName[iM+1] 
												+ "_ch" + String.format("%02d", chanNums[iRow]);
						FastSpecEntry specEntry = configW7X.getPointByName(componentName);
						if(specEntry == null){
							if(!cfg.createEntries)
								continue;
							specEntry = new FastSpecEntry(componentName);
							configW7X.specEntries.add(specEntry);
						}
						
						// Disable if the beam isn't on, or there's no field and its a Pi component						
						specEntry.enable = entryEnable && !(cfg.B == 0 && iM != 0);
						specEntry.fit.frameEnable = frameEnable;
						if(!specEntry.enable){
							specEntry.x0 = Double.NaN; //don't display either
							specEntry.x1 = Double.NaN; //don't display either
							continue;
						}
						
						double vLOS = vBeam * cosLOSBeam;
						double dLambda = lambdaHalpha * vLOS / c; //doppler shift
						if(adjustDoppler != null){
							dLambda += adjustDoppler[iE-1][0] 
									+ ((double)losChan / 40.0) * (adjustDoppler[iE-1][1] - adjustDoppler[iE-1][0]);
						}
						
						double lambda = lambdaHalpha + dLambda;
						
						//double starkSplitting = lambdaHalpha * (1.0 - 1.0/(1.0 + lambdaHalpha * delta));
						double lambdaPi = 1.0 / ((1.0/lambda) + deltaSplit);
						double starkSplitting = FastMath.abs(lambdaPi - lambda);
												
						lambda += iM * starkSplitting; 
								
						specEntry.nominalWavelength = lambda;
						specEntry.x0 = lambda + cfg.windowSize/2;
						specEntry.x1 = lambda - cfg.windowSize/2;
						specEntry.x0Min = lambda - cfg.fitWidth/2;
						specEntry.x0Max = lambda + cfg.fitWidth/2;
						specEntry.x0Init = lambda;
						specEntry.x0PriorMean = specEntry.x0Init;
						specEntry.x0PriorSigma = 0.050;
												
						specEntry.sigmaInit = FastMath.sqrt(FastMath.pow2(dLambda * cfg.widthShiftScaling) + cfg.insFuncSigma[iPort]*cfg.insFuncSigma[iPort]);
						specEntry.sigmaMin = specEntry.sigmaInit * 0.4;
						specEntry.sigmaMax = specEntry.sigmaInit * 1.7;
						specEntry.sigmaPriorMean = specEntry.sigmaInit;
						specEntry.sigmaPriorSigma = specEntry.sigmaInit * cfg.sigmaPriorWidthRelative;
						
						specEntry.y0Init = Double.NaN;
						specEntry.y0Min = Double.NEGATIVE_INFINITY;
						specEntry.y0Max = Double.POSITIVE_INFINITY;
						specEntry.clampY0ToBaselineInit = 0.01;
						
						specEntry.amplitudeInit = 
								cfg.ampInitE[iE-1] 
								* cfg.intensityScale 
								* ampInitM[iM+1] 
								* beamDensityProfile
								* cfgPerPortBESIntensityScale[iPort];
						specEntry.amplitudeMin = 0 * specEntry.amplitudeInit;
						specEntry.amplitudeMax = 10 * specEntry.amplitudeInit;
						specEntry.amplitudePriorMean = Double.NaN;
						specEntry.amplitudePriorSigma = Double.NaN;
						
						
						specEntry.powerInit = 1.8;
						specEntry.powerMin = specEntry.powerInit;
						specEntry.powerMax = specEntry.powerInit;
						
						//match amplitude and sigma components of Pi- to Pi
						
						specEntry.amplitudeMatch = null;
						specEntry.sigmaMatch = null;
						
						switch (cfg.piSigmaRatio){
						case PiSigmaRatios:
							if(iM != 0){
								String matchID = componentName.replaceAll("Pi[+-]", "Sigma");
								Double f = cfg.ratioPiSigmaExp.get(chanNums[iRow]);
								if(f == null)
									throw new RuntimeException("No ratioPiSigmaExp set for channel" + chanNums[iRow]);

								specEntry.amplitudeMatch = matchID + "*" + f;
								specEntry.sigmaMatch = matchID + ((iM == -1) ? "*1.0" : "*1.0");
							}
							break;
							
						case EqualPis:
							if(iM == -1){
								String matchID = componentName.replaceAll("Pi-", "Pi+");
								specEntry.amplitudeMatch = matchID;
								specEntry.sigmaMatch = matchID;
								//specEntry.sigmaMatch = null;
							}
							break;
							
						case Free:
						default:
							break;
						}
						
						if(iE == 1 && iM == 1){
							specEntry.powerInit = 2.0;
							specEntry.powerMin = specEntry.powerInit;
							specEntry.powerMax = specEntry.powerInit;
							
									
						}else{
							specEntry.powerInit = 2.0;
							specEntry.powerMin = specEntry.powerInit;
							specEntry.powerMax = specEntry.powerInit;
							
						}
					}
				}
				
				String chStr = "_ch" + String.format("%02d", chanNums[iRow]);
				//impurity lines				
				for(FastSpecEntry specEntry : configW7X.specEntries){
					if(specEntry.id.startsWith("Imp_") && specEntry.id.endsWith(chStr)){
						double lineScale = 1.0;
						if(specEntry.id.contains("Imp_660")){
							lineScale = 0.1;
						}else if(specEntry.id.contains("Imp_661")){
							lineScale = 0.1;
						}
						specEntry.amplitudeInit = cfg.impurityIntensity * cfg.intensityScale * lineScale;
						specEntry.amplitudeMin = 0;
						specEntry.amplitudeMax = 10 * specEntry.amplitudeInit;
						
						double shift = 0;
						if(specEntry.id.contains("_m")){
							//don't know what this was for
							//double shift = (isMA ? -1 : 1) * 0.005 * cfg.B + (is658 ? 0.05 : 0);						
							boolean isMA = specEntry.id.contains("_mA");
							shift = (isMA ? -1 : 1) * cfg.impurityZeemanWavelengthSplit;
							
						}
						
						if(specEntry.id.startsWith("Imp_657.8"))
							shift += -0.050;

						if(specEntry.id.startsWith("Imp_658.3"))
							shift += 0.030;
						
						if(specEntry.id.startsWith("Imp_659.2") ||
							specEntry.id.startsWith("Imp_660.1")){
							//specEntry.powerInit = 1.8;
							//specEntry.powerMin = specEntry.powerInit;
							//specEntry.powerMax = specEntry.powerInit;
							
							//specEntry.sigmaInit = 1.0;
							//specEntry.sigmaMin = 1.0;
							//specEntry.sigmaMax = 1.0;
							
							specEntry.amplitudeInit = 200;
							specEntry.amplitudeMin = 0;
							specEntry.amplitudeMax = 500;
						}
						
						specEntry.x0Init = specEntry.nominalWavelength + shift;
						specEntry.x0Min = specEntry.x0Init - FastMath.abs(shift * 0.2);
						specEntry.x0Max = specEntry.x0Init + FastMath.abs(shift * 0.2);
						
						if(specEntry.id.startsWith("Imp_658.3")){
							//match C_II Imp_658.3_mA and _mB to Imp_657.8_mA						
							specEntry.amplitudeMatch = specEntry.id.replaceAll("658.3_m[AB]", "657.8_mA");
							specEntry.sigmaMatch = specEntry.amplitudeMatch; //also match sigma
							specEntry.amplitudeMatch += "*0.5";
							
						}else if(specEntry.id.startsWith("Imp_660.1")){
							//match molecular Imp_660.1_mA and _mB to Imp_659.2_mA					
							
							specEntry.amplitudeMatch = specEntry.id.replaceAll("660.1", "659.2");
							specEntry.sigmaMatch = specEntry.amplitudeMatch; //also match sigma							
								
						}else if(specEntry.id.contains("_mB")){
							//otherwise match any _mA to corresponding _mB
							specEntry.amplitudeMatch = specEntry.id.replaceAll("_mB", "_mA");
							specEntry.sigmaMatch = specEntry.amplitudeMatch;
						}
						
					}
				}
				
				//set Halo parameters
				String componentName = "Halo" + chStr;
				FastSpecEntry specEntry = configW7X.getPointByName(componentName);
				if(specEntry != null){
					specEntry.enable = cfg.haloIntensity > 0;
					specEntry.amplitudeInit=cfg.haloIntensity * cfg.intensityScale * beamDensityProfile;
					specEntry.amplitudeMin=0;
					specEntry.amplitudeMax=5 * specEntry.amplitudeInit;
					
					double haloWindowWidth = 12.0;
					specEntry.x0 = lambdaHalpha + haloWindowWidth / 2;
					specEntry.x1 = lambdaHalpha - haloWindowWidth / 2;
					
					specEntry.y0Init = Double.NaN;
					specEntry.y0Min = Double.NEGATIVE_INFINITY;
					specEntry.y0Max = Double.POSITIVE_INFINITY;
					specEntry.clampY0ToBaselineInit = 0.01;
				}
				
				//set Halpha parameters
				componentName = "Halpha_narrow" + chStr;
				specEntry = configW7X.getPointByName(componentName);
				double halphaWindowWidth = 2.0;
				double halphaFitWindowWidth = 0.1;
				if(specEntry != null){
					specEntry.enable = cfg.haloIntensity > 0;
					specEntry.amplitudeInit=cfg.HalphaIntensity*3/5 * cfg.intensityScale;
					specEntry.amplitudeMin=0;
					specEntry.amplitudeMax=5 * specEntry.amplitudeInit;

					specEntry.x0 = lambdaHalpha + halphaWindowWidth / 2;
					specEntry.x1 = lambdaHalpha - halphaWindowWidth / 2;
					specEntry.x0Min = lambdaHalpha - halphaFitWindowWidth / 2;
					specEntry.x0Max = lambdaHalpha + halphaFitWindowWidth / 2;

					specEntry.y0Init = Double.NaN;
					specEntry.y0Min = Double.NEGATIVE_INFINITY;
					specEntry.y0Max = Double.POSITIVE_INFINITY;
					specEntry.clampY0ToBaselineInit = 0.01;
				}
				
				componentName = "Halpha_wide" + chStr;
				specEntry = configW7X.getPointByName(componentName);
				if(specEntry != null){
					specEntry.enable = cfg.haloIntensity > 0;
					specEntry.amplitudeInit=cfg.HalphaIntensity*2/5 * cfg.intensityScale;
					specEntry.amplitudeMin=0.2 * specEntry.amplitudeInit;
					specEntry.amplitudeMax=5 * specEntry.amplitudeInit;

					specEntry.x0 = lambdaHalpha + halphaWindowWidth / 2;
					specEntry.x1 = lambdaHalpha - halphaWindowWidth / 2;
					specEntry.x0Min = lambdaHalpha - halphaFitWindowWidth / 2;
					specEntry.x0Max = lambdaHalpha + halphaFitWindowWidth / 2;

					specEntry.y0Init = Double.NaN;
					specEntry.y0Min = Double.NEGATIVE_INFINITY;
					specEntry.y0Max = Double.POSITIVE_INFINITY;
					specEntry.clampY0ToBaselineInit = 0.01;
				}
			}
		}
		for(FastSpecEntry specEntry : configW7X.specEntries){
			specEntry.clampY0ToBaselineInit = 0.01;
		}
	}

	public void doCalc() {
		//For NBI processing, automatically set energy ranges
		if(((FastSpecConfigW7X)proc.getConfig()).bes.enable){

			setupComponents();			
		}
	}

}
