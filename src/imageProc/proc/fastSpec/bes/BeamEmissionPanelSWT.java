package imageProc.proc.fastSpec.bes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.fastSpec.swt.FastSpecSpecificPanel;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class BeamEmissionPanelSWT extends FastSpecSpecificPanel {
	
	private Button adjustComponentsCheckbox;
	
	private boolean inhibitSettingsChanged;
	
	private FastSpecProcessorW7X proc;
	
	private Text intensityScaleText;
	private Text magneticFieldText;
	private Text jsonText;
	private Button createJsonButton;
	private Button parseJsonButton;
	private Button doAutoconfig;
	
	public BeamEmissionPanelSWT(FastSpecProcessorW7X proc, Composite parent) {
		this.proc = proc;
		
		Label lAC = new Label(parent, SWT.NONE); lAC.setText("");
		adjustComponentsCheckbox = new Button(parent, SWT.CHECK);
		adjustComponentsCheckbox.setText("Beam Emission Spectrum Autoconfiguration");
		adjustComponentsCheckbox.setToolTipText("Enables BES Autoconfig based on beam energy, beam, LOS gemeorty etc.");
		adjustComponentsCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		adjustComponentsCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lIS = new Label(parent, SWT.NONE); lIS.setText("Intensity scaling:");
		intensityScaleText = new Text(parent, SWT.NONE);
		intensityScaleText.setText("");
		intensityScaleText.setToolTipText("Scale all intensity values by this. Allows adjustment for gas/plasma density");
		intensityScaleText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		intensityScaleText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		

		Label lMF = new Label(parent, SWT.NONE); lMF.setText("Magnetic field:");
		magneticFieldText = new Text(parent, SWT.NONE);
		magneticFieldText.setText("");
		magneticFieldText.setToolTipText("Magnetic field for Stark splitting calculation. Set to 0 to disable splitting.");
		magneticFieldText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		magneticFieldText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		

		Label lJB = new Label(parent, SWT.NONE); lJB.setText("");

		createJsonButton = new Button(parent, SWT.PUSH);
		createJsonButton.setText("Get");
		createJsonButton.setToolTipText("Fill the above box with JSON of the current BES autoconfig settings");
		createJsonButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		createJsonButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { createJSONEvent(event); } });

		parseJsonButton = new Button(parent, SWT.PUSH);
		parseJsonButton.setText("Set");
		parseJsonButton.setToolTipText("Attempt to parse the JSON text in the above box into the BES autoconfig");
		parseJsonButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		parseJsonButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { parseJSONEvent(event); } });


		doAutoconfig = new Button(parent, SWT.PUSH);
		doAutoconfig.setText("Apply autoconfig");
		doAutoconfig.setToolTipText("Configure the fit components according to the given autoconfig now, without then doing the fitting.");
		doAutoconfig.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
		doAutoconfig.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { doAutoConfig(event); } });
		

		Label lJ = new Label(parent, SWT.NONE); lJ.setText("JSON:");
		jsonText = new Text(parent, SWT.MULTI | SWT.V_SCROLL);
		jsonText.setText(" \n \n \n \n \n.\n");
		jsonText.setToolTipText("JSON text to write/parse from/to BES autoconfig settings");
		jsonText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
		jsonText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
	}
	
	protected void doAutoConfig(Event event){
		proc.getBESCalc().doCalc();				
	}
	
	protected void createJSONEvent(Event event) {
		JsonParser parser = new JsonParser();
		int topPos = jsonText.getTopIndex();
		int textPos = jsonText.getSelection().x;
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.addSerializationExclusionStrategy(new ExclusionStrategy() {
					@Override
					public boolean shouldSkipField(FieldAttributes attrbs) {
						return false; //attrbs.getName().startsWith("fit");
					}

					@Override
					public boolean shouldSkipClass(Class<?> arg0) {
						return false;
					}
				})
				//.setPrettyPrinting()
				.create();
		
		String jsonString = gson.toJson(proc.getConfigW7X().bes);
		//jsonString = jsonString.replaceAll("(,)(?=(?:[^\"]|\"[^\"]*\")", "*");
		//jsonString = "blah,merp,(rar,derp,herp),terp";
		//jsonString = jsonString.replaceAll("(,)(?=(?:[^\"|\\[[^\\]*\\])*$)", "*");
		//jsonString = jsonString.replaceAll(",(?![^\\(]* \\))", "*");
		//jsonString = jsonString.replaceAll("\\(.*?\\)|(,)", "*");
		
		
		int l=0;
		StringBuilder sb = new StringBuilder(jsonString.length()*2);
		for(int i=0; i < jsonString.length(); i++){
			char c = jsonString.charAt(i);
			switch(c){
				case '[': l++; break;
				case ']': if(l>0)l--; break;
				case ',': sb.append((l<=0) ? ",\n" : ", "); continue;				
				case ':': if(l<=0){ sb.append(" : "); continue; }; break;
			}
			sb.append(c);
		}
		
		jsonText.setText(sb.toString());
		jsonText.setSelection(textPos);
		jsonText.setTopIndex(topPos);
		
		/*Point s = jsonText.getSize();
		s.y = 200;
		jsonText.setSize(s);
		
		s=  jsonText.getParent().getParent().getSize();
		s.y=300;
		jsonText.getParent().getParent().setSize(s);
		System.out.println(s);*/
	}

	protected void parseJSONEvent(Event event) {
		Gson gson = new Gson();
		
		proc.getConfigW7X().bes = gson.fromJson(jsonText.getText(), BeamEmissionConfig.class);

		doUpdate();
	}

	@Override
	public void doUpdate() {
		inhibitSettingsChanged = true;
		try{
			FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
			
			adjustComponentsCheckbox.setSelection(configW7X.bes.enable);
			intensityScaleText.setText(Double.toString(configW7X.bes.intensityScale));
			magneticFieldText.setText(Double.toString(configW7X.bes.B));
			
		}finally{
			inhibitSettingsChanged = false;
		}
	}
	
	private void settingsChangedEvent(Event e) {
		if(inhibitSettingsChanged)
			return;
		
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		configW7X.bes.enable = adjustComponentsCheckbox.getSelection();
		configW7X.bes.intensityScale = Algorithms.mustParseDouble(intensityScaleText.getText());
		configW7X.bes.B = Algorithms.mustParseDouble(magneticFieldText.getText());
		
	}
}
