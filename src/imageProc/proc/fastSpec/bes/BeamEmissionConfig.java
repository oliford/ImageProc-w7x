package imageProc.proc.fastSpec.bes;

import java.util.HashMap;


/** Auto-configuration parameters for beam emission spectroscopy 
 * These are used to generate/modify the main parameters in FastSpecConfigW7X
 * 
 *  They are very much hacked in to match the beam into empry vessel without field, with field, then plasma
 */
public class BeamEmissionConfig {
		/* ----- Beam emission spectroscopy specifics ------ */
	public boolean enable = false;
	
	/** Adjustment to calculated Doppler shift 
	 * Seems to be a bit different for each port.
	 * linear interpolation of LOS channel number, given at ch0 and ch40 
	 *    double[E1,E2,E3][offsetCh0, offsetChXX] 
	 * 
	 * */
	public HashMap<String, double[][]> adjustDopplerTable = new HashMap<String, double[][]>();	
	public BeamEmissionConfig() {
		adjustDopplerTable.put("AEA21_A/Q7", new double[][] {{0.01, -0.002}, {0.018, -0.0015}, {0.02, -0.0013}});
		adjustDopplerTable.put("AEM21_S7/Q7", new double[][] {{0.06, -0.001}, {0.04, -0.001}, {0.02, -0.001}});
		adjustDopplerTable.put("AEM21_S8/Q7", new double[][] {{0.10, -0.001}, {0.06, -0.001}, {0.045, -0.001}});

		adjustDopplerTable.put("AEA21_A/Q8", new double[][] {{0.01, -0.002}, {0.018, -0.0015}, {0.02, -0.0013}});
		adjustDopplerTable.put("AEM21_S7/Q8", new double[][] {{0.06, -0.001}, {0.04, -0.001}, {0.02, -0.001}});
		adjustDopplerTable.put("AEM21_S8/Q8", new double[][] {{0.10, -0.001}, {0.06, -0.001}, {0.045, -0.001}});
		
		//AEA21 1009.034
		/*
		ratioPiSigmaExp.put(3, 0.75);
		ratioPiSigmaExp.put(5, 0.75);
		ratioPiSigmaExp.put(7, 0.75);
		ratioPiSigmaExp.put(9, 0.75);
		ratioPiSigmaExp.put(11, 0.75);
		ratioPiSigmaExp.put(13, 0.75);
		ratioPiSigmaExp.put(15, 0.75);
		ratioPiSigmaExp.put(17, 0.75);
		ratioPiSigmaExp.put(19, 0.75);
		ratioPiSigmaExp.put(21, 0.75);
		ratioPiSigmaExp.put(23, 0.75);
		ratioPiSigmaExp.put(25, 0.75);
		ratioPiSigmaExp.put(27, 0.75);
		ratioPiSigmaExp.put(29, 0.80);
		ratioPiSigmaExp.put(31, 0.85);
		ratioPiSigmaExp.put(33, 0.85);
		ratioPiSigmaExp.put(35, 0.85);
		ratioPiSigmaExp.put(46, 0.85);
		 */
		
		//AEA21 0920.034
		ratioPiSigmaExp.put(3, 0.75);
		ratioPiSigmaExp.put(5, 0.75);
		ratioPiSigmaExp.put(7, 0.75);
		ratioPiSigmaExp.put(9, 0.75);
		ratioPiSigmaExp.put(11, 0.75);
		ratioPiSigmaExp.put(13, 0.75);
		ratioPiSigmaExp.put(15, 0.75);
		ratioPiSigmaExp.put(17, 0.75);
		ratioPiSigmaExp.put(19, 0.75);
		ratioPiSigmaExp.put(21, 0.75);
		ratioPiSigmaExp.put(23, 0.75);
		ratioPiSigmaExp.put(25, 0.75);
		ratioPiSigmaExp.put(27, 0.75);
		ratioPiSigmaExp.put(29, 0.80);
		ratioPiSigmaExp.put(31, 0.85);
		ratioPiSigmaExp.put(33, 0.85);
		ratioPiSigmaExp.put(35, 0.85);
		ratioPiSigmaExp.put(46, 0.85);
		
		//AEM21
		ratioPiSigmaExp.put( 2, 0.20);
		ratioPiSigmaExp.put( 4, 0.20);
		ratioPiSigmaExp.put( 6, 0.20);
		ratioPiSigmaExp.put( 8, 0.20);
		ratioPiSigmaExp.put(10, 0.20);
		ratioPiSigmaExp.put(12, 0.20);
		ratioPiSigmaExp.put(14, 0.20);
		ratioPiSigmaExp.put(16, 0.20);
		ratioPiSigmaExp.put(18, 0.20);
		ratioPiSigmaExp.put(20, 0.20);
		ratioPiSigmaExp.put(22, 0.20);
		ratioPiSigmaExp.put(24, 0.20);
		ratioPiSigmaExp.put(26, 0.20);
		ratioPiSigmaExp.put(28, 0.15);
		ratioPiSigmaExp.put(30, 0.15);
		ratioPiSigmaExp.put(32, 0.15);
		ratioPiSigmaExp.put(34, 0.20);
		ratioPiSigmaExp.put(38, 0.20);
		ratioPiSigmaExp.put(42, 0.20);
		ratioPiSigmaExp.put(44, 0.20);
	}
	
	/** Create the component entries that don't exist */ 
	public boolean createEntries = true;
	
	/** dont fit components of given beam, even if it's supposedly on */
	public boolean beamInhibit[] = { false, false, false, false, false, false, false, false };

	/** Ampltiude adjustment per energy component */
	double[] ampInitE = { 600, 1800, 300 };
	
	/** Index into this array same as index into ampInitM and insFuncSigma. Matched against losName */
	public String[] portNames = { "AEA21", "AEM21" };
	
	/** Ampltiude adjustment per Stark split component (which is obviously different depending on obs system */
	double[][] ampInitM = {{ 0.7, 1.0, 0.7 }, { 0.2, 1.0, 0.2 }}; // AEA21, AEM21
	
	/** Component width is made of a static part, which should be instrument function but does seem to change
	 * with obs system, and shouldn't. */
	double insFuncSigma[] = { 0.05, 0.10 }; //instrument function width, for sigmaInit
	
	/** how sigmaInit scales with calculated doppler shift */
	double widthShiftScaling = 0.030; // Seems to be the same for A21 and M21
	
	/** Initial intensity of halo components (will be multiplied by intensityScale) */
	double haloIntensity = 10000;
	
	/** general scaling of all amplitudes, adjust for differnt gas/plasma density */
	double intensityScale = 0.6;
	
	/** Multiplication factor of field from VMEC */
	double B = 1.00;  // Toroidal magnetic field, ish

	/** Approx angle between beam and magnetic field, for Stark splitting calculation */
	public double beamFieldAngle = 60 * Math.PI/180;
	
	/** Size of wavelength window for each component */
	double windowSize = 2.0;
	
	/** Allowed wavelength range for fitter */
	double fitWidth = 0.05;

	/** Relative initial intensity of HAlpha components */
	double HalphaIntensity = 1;
	
	/** Relative initial intensity of impurity lines*/
	double impurityIntensity = 1;

	/** Power of beam over which to consider it on, and to enable components [W] */
	public double beamPowerThreshold = 500000;

	/** How to match Sigma,Pi+,Pi- amplitudes/sigmas */
	public enum PiSigmaRatio {
		/** All components separate and free */
		Free, 
		/** Match amplitude and sigma of Pi- to Pi+ */
		EqualPis,
		
		/** Match Pi amplitudes and sigmas to Sigma, but with the ratioPiSigmaExp of that channel */ 
		PiSigmaRatios,		
	}
	public PiSigmaRatio piSigmaRatio = PiSigmaRatio.EqualPis;

	/** Wavelength distance of components of impurities due to Zeeman splitting */
	//double impurityZeemanWavelengthSplit = 0.010; 
	public double impurityZeemanWavelengthSplit = 0.005;
	
	/** Freedom to give each sigma (in it's prior), as a fraction of the calculated init values */
	public double sigmaPriorWidthRelative = 0.02;
	
	/** Experimentally determined pi/sigma ratios */
	public HashMap<Integer,Double> ratioPiSigmaExp = new HashMap<>();
	
	
	/** Shinethrough, for linear fall-off of beam density (so BES and halo) with channel number */
	public double shinethrough = 0.2;


}
