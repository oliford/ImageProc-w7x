package imageProc.proc.fastSpec;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import otherSupport.RandomManager;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.gmds.GMDSSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.database.json.MultiSettingsControl;
import imageProc.graph.JFreeChartGraph;
import imageProc.graph.Series;
import imageProc.proc.fastSpec.FastSpecConfig.Optimiser;
import imageProc.proc.fastSpec.bes.BeamEmissionPanelSWT;
import imageProc.proc.fastSpec.ionTemp.IonTempCalc;
import imageProc.proc.fastSpec.ionTemp.IonTempPanelSWT;
import imageProc.proc.fastSpec.nbiSpec.NeutraliserSpecPanelSWT;
import imageProc.proc.fastSpec.passiveInit.PassiveInitialiserPanelSWT;
import imageProc.proc.fastSpec.swt.FastSpecSWTController;
import imageProc.proc.fastSpec.swt.FastSpecSpecificPanel;
import imageProc.proc.obsPos.FastSpecObsPosPanelSWT;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class FastSpecSWTControllerW7X extends FastSpecSWTController {

	private Combo saveResultsPathCombo;
	private Text saveResultsDescriptionTextbox;
	private Spinner saveResultsVersion;
	
	private Combo aliasPathCombo;
	private Button setAliasCheckbox;
	private Button setAliasButton;
	
	private Label lastVersionLabel;
	private Button saveOnlyGoodFramesCheckbox;
	
	private Button saveHHeRatioCheckbox;
	
	private NeutraliserSpecPanelSWT neutralizerSpecPanel;
	
	private boolean inhibitSettingsChanged = false;
	
	public FastSpecSWTControllerW7X(Composite parent, int style, FastSpecProcessor proc) {
		super(parent, style, proc);
	}
	
	@Override
	protected void addSaveControls(Composite parent) {
				
		Label lP = new Label(parent, SWT.NONE); lP.setText("DB path:");
		saveResultsPathCombo = new Combo(parent, SWT.DROP_DOWN);
		saveResultsPathCombo.setItems(new String[] {
				"w7x/Test/raw/W7XAnalysis/QSK-_TestSpectroscopy/FastSpec_Unknown" });
		saveResultsPathCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		saveResultsPathCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lV = new Label(parent, SWT.NONE); lV.setText("Version (0 for latest):");
		saveResultsVersion = new Spinner(parent, SWT.NONE);
		saveResultsVersion.setValues(0, 0, Integer.MAX_VALUE, 0, 1, 10);
		saveResultsVersion.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		saveResultsVersion.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		lastVersionLabel = new Label(parent, SWT.NONE);
		lastVersionLabel.setText("Last saved: Vxxxx");
		lastVersionLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		Label lVD = new Label(parent, SWT.NONE); lVD.setText("Version description:");
		
		saveResultsDescriptionTextbox = new Text(parent, SWT.DROP_DOWN);
		saveResultsDescriptionTextbox.setText("[Empty]");
		saveResultsDescriptionTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		saveResultsDescriptionTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lA = new Label(parent, SWT.NONE); lA.setText("Alias path:");
		aliasPathCombo = new Combo(parent, SWT.DROP_DOWN);
		aliasPathCombo.setItems(new String[] {
				"w7x/Sandbox/views/KKS/QSK_CXRS/TestAlias" });
		aliasPathCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		aliasPathCombo.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		setAliasCheckbox = new Button(parent, SWT.CHECK);
		setAliasCheckbox.setText("Set on write");
		setAliasCheckbox.setToolTipText("Sets the given alias to the written amplitude stream version of when written");
		setAliasCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		setAliasCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		setAliasButton = new Button(parent, SWT.PUSH);
		setAliasButton.setText("Set");
		setAliasButton.setToolTipText("Sets the given alias to the last written version of the amplitude stream now");
		setAliasButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		setAliasButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setAliasButtonEvent(event); } });

		
		Label lSGF = new Label(parent, SWT.NONE); lSGF.setText("");
		saveOnlyGoodFramesCheckbox = new Button(parent, SWT.CHECK);
		saveOnlyGoodFramesCheckbox.setText("Save only 'good' frames");
		saveOnlyGoodFramesCheckbox.setToolTipText("Save fits only from frames marked as 'good' e.g. from SeriesProcessor");
		saveOnlyGoodFramesCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		saveOnlyGoodFramesCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lHHR = new Label(parent, SWT.NONE); lHHR.setText("");
		saveHHeRatioCheckbox = new Button(parent, SWT.CHECK);
		saveHHeRatioCheckbox.setText("Save H/(H+He)");
		saveHHeRatioCheckbox.setToolTipText("Calculated the H/He flux ratio from H/He line intensity ratio");
		saveHHeRatioCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		saveHHeRatioCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

	}
	
	@Override
	protected void addSpecificPanels() {
		specifcPanels = new FastSpecSpecificPanel[5];
		specificTabItems = new CTabItem[5];
		

		Composite ionTempTabComp = new Composite(tabFolder, SWT.NONE);
		ionTempTabComp.setLayout(new GridLayout(6, false));		
		specificTabItems[0] = new CTabItem(tabFolder, SWT.NONE);
		specificTabItems[0].setControl(ionTempTabComp);
		specificTabItems[0].setText("Ti");  
		specificTabItems[0].setToolTipText("Ion temperture (Zeeman/FS/InsFunc corrections)");
		
		specifcPanels[0] = new IonTempPanelSWT((FastSpecProcessorW7X)proc, ionTempTabComp);
		

		Composite obsPosTabComp = new Composite(tabFolder, SWT.NONE);
		obsPosTabComp.setLayout(new GridLayout(6, false));		
		specificTabItems[1] = new CTabItem(tabFolder, SWT.NONE);
		specificTabItems[1].setControl(obsPosTabComp);
		specificTabItems[1].setText("Geom");  
		specificTabItems[1].setToolTipText("Geomtry / observation position, VMEC and NBI intersection.");
		
		specifcPanels[1] = new FastSpecObsPosPanelSWT((FastSpecProcessorW7X)proc, obsPosTabComp);
		
		
		Composite nspecTabComp = new Composite(tabFolder, SWT.NONE);
		nspecTabComp.setLayout(new GridLayout(6, false));		
		specificTabItems[2] = new CTabItem(tabFolder, SWT.NONE);
		specificTabItems[2].setControl(nspecTabComp);
		specificTabItems[2].setText("NBI NS");  
		specificTabItems[2].setToolTipText("NBI Neutraliser spectroscopy specific settings");
		
		specifcPanels[2] = new NeutraliserSpecPanelSWT((FastSpecProcessorW7X)proc, nspecTabComp);

		Composite besTabComp = new Composite(tabFolder, SWT.NONE);
		besTabComp.setLayout(new GridLayout(6, false));		
		specificTabItems[3] = new CTabItem(tabFolder, SWT.NONE);
		specificTabItems[3].setControl(besTabComp);
		specificTabItems[3].setText("BES");  
		specificTabItems[3].setToolTipText("Beam Emission Spectroscopy specific settings");
		
		specifcPanels[3] = new BeamEmissionPanelSWT((FastSpecProcessorW7X)proc, besTabComp);
		
		Composite pasvTabComp = new Composite(tabFolder, SWT.NONE);
		pasvTabComp.setLayout(new GridLayout(6, false));		
		specificTabItems[4] = new CTabItem(tabFolder, SWT.NONE);
		specificTabItems[4].setControl(pasvTabComp);
		specificTabItems[4].setText("Passive");  
		specificTabItems[4].setToolTipText("Passive line component from other fits");
		
		specifcPanels[4] = new PassiveInitialiserPanelSWT((FastSpecProcessorW7X)proc, pasvTabComp);
		
	}

	@Override
	protected void addSettingsWidget(Composite parent){
		settingsCtrl = new MultiSettingsControl();
		settingsCtrl.buildControl(parent, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.END, true, false, 6, 1));		
	}
	
	private void setAliasButtonEvent(Event event){
		((FastSpecProcessorW7X)proc).saveAlias();
	}
	
	@Override
	protected void settingsChangedEvent(Event e) {
		if(inhibitSettingsChanged)
			return;
		super.settingsChangedEvent(e);
		
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		configW7X.saveResultsPath = saveResultsPathCombo.getText();
		configW7X.saveResultsDescription = saveResultsDescriptionTextbox.getText();
		configW7X.saveResultsVersion = saveResultsVersion.getSelection();
		configW7X.aliasPath = aliasPathCombo.getText();
		configW7X.setAliasOnWrite = setAliasCheckbox.getSelection();
		configW7X.saveHHeRatio = saveHHeRatioCheckbox.getSelection();
		configW7X.saveOnlyGoodFrames = saveOnlyGoodFramesCheckbox.getSelection();
		
	}
	
	@Override
	protected void doUpdate() {
		super.doUpdate();
		inhibitSettingsChanged = true;
		try{
			FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
			saveResultsVersion.setSelection(configW7X.saveResultsVersion);		
			saveResultsDescriptionTextbox.setText(configW7X.saveResultsDescription == null ? "null" : configW7X.saveResultsDescription);
			lastVersionLabel.setText("Last saved: V" + String.format("%4d", configW7X.lastVersionSaved));
			setAliasCheckbox.setSelection(configW7X.setAliasOnWrite);
			saveOnlyGoodFramesCheckbox.setSelection(configW7X.saveOnlyGoodFrames);
			saveHHeRatioCheckbox.setSelection(configW7X.saveHHeRatio);
			if(!saveResultsPathCombo.isFocusControl())
				saveResultsPathCombo.setText(configW7X.saveResultsPath == null ? "" : configW7X.saveResultsPath);
			if(!aliasPathCombo.isFocusControl())
				aliasPathCombo.setText(configW7X.aliasPath == null ? "" : configW7X.aliasPath);			

		}finally{
			inhibitSettingsChanged = false;
		}
	}
}
