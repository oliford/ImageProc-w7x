package imageProc.proc.fastSpec.ionTemp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import algorithmrepository.Algorithms;
import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import binaryMatrixFile.AsciiMatrixFile;
import descriptors.w7x.ArchiveDBDesc;
import fusionOptics.Util;
import imageProc.proc.fastSpec.FastSpecConfig.AmpFitMode;
import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecProcessor.CalcStage;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.spec.SpecCalProcessor;
import imageProc.w7x.W7XUtil;
import imageProc.w7xspec.Zeeman;
import imageProc.w7xspec.Zeeman.GaussianFitsResults;
import ipp.w7x.vmec.server.Vmec;
import ipp.w7x.vmec.server.Vmec_Service;
import net.jafama.FastMath;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import seed.ws.math.types.Points3D;
import signals.w7x.ArchiveDB.Database;
import w7x.archive.ArchiveDBFetcher;

public class IonTempCalc {

	private FastSpecProcessorW7X proc;
	
	private HashMap<FastSpecEntry, Interpolation1D> corrections;
	
	private double[][] B;
	
	private String[] averageSetName;
	private double[][] averageTi;
	private double[][][] averageTiSource;
	

	public IonTempCalc(FastSpecProcessorW7X proc) {
		this.proc = proc;
	}

	public void calcTiCorrected(CalcStage stage, FastSpecEntry specEntryOnly, int iF) {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfigW7X();
				
		if(stage == CalcStage.Prepare || corrections == null){ //preparation: clear all fits and reget them
			corrections = new HashMap<FastSpecEntry, Interpolation1D>();
		}
		
		//Grab LOS info written to metadata by SpecCalProcessor
		double[][] losUVec = (double[][])Mat.fixWeirdContainers(proc.getSeriesMetaData("/SpecCal/linesOfSight/unitVec"), true);
		if(losUVec == null)
			throw new RuntimeException("No line of sight data to calculate Doppler shift");
		
		//need this if we want to set to NaN when the beams of a given LOS are off
		double[][][] beamPos = (double[][][])Mat.fixWeirdContainers(proc.getSeriesMetaData("/SpecCal/linesOfSight/activeBeamIntersectionPos"), true);
		
		if(stage == CalcStage.Prepare || B == null) {
			try {
				B = getBField();
			}catch(RuntimeException err) {				
				if(configW7X.ionTemp.allowWithoutFieldInfo) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error getting B field for ion temp corrections", err);
					B = new double[54][3];
				}
				else throw err;
			}
		}
		
		for (FastSpecEntry specEntry : configW7X.specEntries) {
			if(specEntryOnly != null && specEntryOnly != specEntry)
				continue; 
			
			if (specEntry.atomicMass > 0) {
				
				//double wavelength[][] = null;
				double insFuncWidth[][] = null;	
				double insFuncPower[][] = null;		
				insFuncWidth = (double[][]) Mat.fixWeirdContainers(proc.getConnectedSource().getSeriesMetaData("SpecCal/instrumentFuncWidth"), true);
				insFuncPower = (double[][]) Mat.fixWeirdContainers(proc.getConnectedSource().getSeriesMetaData("SpecCal/instrumentFuncPower"), true);
				if(insFuncWidth == null || insFuncPower == null)
					throw new RuntimeException("calcZeemanCorrectedTi is set but SpecCal/wavelength or SpecCal/instrumentFuncWidth(/Power) were not found.");
				
				
				//get the instrument function parameters at the nominal wavelength (so it's not time point dependent)
				int y = (int)specEntry.y0;
				double fitWavelength = specEntry.nominalWavelength;				
				int x = proc.getPixelForWavelength(y, fitWavelength);
				if(x < 0) x=0;
				if(x >= (insFuncWidth[y].length-1)) x = insFuncWidth[y].length-2;					
				double sigmaPx = insFuncWidth[y][x];
				double instrPower = insFuncPower[y][x];

				//convert to wavelength
				double dispersion = proc.getDispersionAtPixel(y, x);
				double instrSigma =  sigmaPx * FastMath.abs(dispersion);
				
				//need field/LOS angle and field string info
				int iRow = (int)specEntry.y0;
				double bMag = Mat.veclength(B[iRow]);
				double cosLOSBeam = Mat.dot(losUVec[iRow], B[iRow]) / bMag;				
				double thetaDeg = FastMath.acos(cosLOSBeam) * 180 / Math.PI;
				
				if(configW7X.ionTemp.allowWithoutFieldInfo && bMag == 0)
					thetaDeg = 0;
				
				double[] Ti = null, TiErr = null;
				Interpolation1D interpTi = null;

				Ti = specEntry.fit.inferredQuantities.get("Ti");
				TiErr = specEntry.fit.inferredQuantities.get("TiErr");
				
				if(Ti == null || Ti.length != specEntry.fit.Sigma.length) {
					Ti = Mat.fillArray(Double.NaN, specEntry.fit.Sigma.length); 
					specEntry.fit.inferredQuantities.put("Ti", Ti);
					if (!configW7X.inferredQuantities.contains("Ti"))
						configW7X.inferredQuantities.add("Ti");
					
					TiErr = Mat.fillArray(Double.NaN, specEntry.fit.Sigma.length);
					specEntry.fit.inferredQuantities.put("Ti_uncertainty", TiErr);
					if (!configW7X.inferredQuantities.contains("Ti_uncertainty"))
						configW7X.inferredQuantities.add("Ti_uncertainty");
				}
				
				if(stage == CalcStage.Prepare && configW7X.ionTemp.useZeemanService){ //preparation
					
					if(Double.isFinite(instrSigma) && Double.isFinite(instrPower) 
							&& Double.isFinite(thetaDeg) && Double.isFinite(bMag)){
							
						proc.setStatus("Getting Zeeman/FS corrections for row " + iRow + " / " + losUVec.length + " ");
						String lineName = configW7X.ionTemp.serviceLineName;
						if(lineName == null || lineName.length() == 0 || lineName.equalsIgnoreCase("auto")){
							int atomicNumber = specEntry.atomicNumber;
							if(specEntry.atomicNumber <= 0)
								continue;
							
							if(specEntry.ionisationState == atomicNumber-1){
								//Is hydrogen like
							}else if(specEntry.ionisationState == atomicNumber-3){
								//Li-like - assume the two other electrons effectively screen two of the protons
								atomicNumber -= 2;
							}else{
								throw new IllegalArgumentException("Currently only know how to deal with H-like and Li-like atoms");
							}
							
							double spin = 0.5;
							
							lineName = String.format("Lorenz-M_%.3f-Z_%d-n_%d_%d-S_%.1f", specEntry.atomicMass, atomicNumber, specEntry.upperState, specEntry.lowerState, spin);
							 
						}
						
						GaussianFitsResults fits = Zeeman.getGaussianFits(lineName, 
								bMag, 
								thetaDeg, 
								configW7X.ionTemp.inhibitInstrumentFucntion ? 0.0 : instrSigma, 
								instrPower,
								configW7X.ionTemp.inhibitFineStructure);
						
						if(proc.isAborting())
							throw new RuntimeException("Aborted during Zeeman/FS service call");
						
						interpTi = new Interpolation1D(fits.sigmaFit, fits.TiTrue, InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT, Double.NaN);
						corrections.put(specEntry, interpTi);
						
					}else{
						corrections.put(specEntry, null);
					}
					
				}else{
					Ti = specEntry.fit.inferredQuantities.get("Ti");
					TiErr = specEntry.fit.inferredQuantities.get("Ti_uncertainty");
					interpTi = corrections.get(specEntry);
				}
				
				if(!configW7X.ionTemp.useZeemanService){
					int n = specEntry.fit.Sigma.length;
										
					final double e = 1.60217662e-19; // C
					final double mp = 1.6726219e-27; // kg
					final double c = 3e8; // ms^-1
					
					for (int i = 0; i < n; i++) {
						if(configW7X.ionTemp.noTiWhenNanPos && beamPos != null && beamPos[iRow] != null && Double.isNaN(beamPos[iRow][0][i])){
							Ti[i] = Double.NaN;
							continue;
						}
						
						double measuredSigma2 = FastMath.pow2(specEntry.fit.Sigma[i]);
						Ti[i] = (measuredSigma2 - instrSigma*instrSigma) / FastMath.pow2(specEntry.nominalWavelength) * specEntry.atomicMass * mp * c
							* c / e;
					
						if(configW7X.calcFitUncertainty){
							//Ti = k sigma^2
							//dTi/dSigma = 2 k sigma = 2 Ti / sigma
							//dTi = dSigma * 2 Ti / sigma
							TiErr[i] = specEntry.fit.UncertaintySigma[i] * 2 * Ti[i] / specEntry.fit.Sigma[i];
						}
					}
				}else if(interpTi != null){					 
					 
					int n = specEntry.fit.Sigma.length;
					double dTds[] = new double[1];
										
					for (int i = 0; i < n; i++) {
						if(iF >= 0 && iF != i)
							continue;
						if(!configW7X.ionTemp.allowWithoutFieldInfo && configW7X.ionTemp.noTiWhenNanPos && beamPos != null && beamPos.length > i && beamPos[i] != null && Double.isNaN(beamPos[i][iRow][0])){
							Ti[i] = Double.NaN;
							continue;
						}
						Ti[i] = interpTi.eval(new double[]{ specEntry.fit.Sigma[i] }, dTds)[0];
					
						if(configW7X.calcFitUncertainty){
							//Error reflects the sensitivity of the corrections, i.e.
							// that the error gets much bigger as the measured sigma approaches
							// the instrument function sigma
							TiErr[i] = dTds[0] * specEntry.fit.UncertaintySigma[i]; 
						}
					}					
				}
			
			}

		}
		
		//proc.setStatus("Zeeman/FS corrections done.");
	}
	
	public void doAverages(CalcStage stage, FastSpecEntry specEntryOnly, int iF) {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfigW7X();
		
		int nT = proc.getConnectedSource().getNumImages();
		int nSets =  configW7X.ionTemp.averageSets.size();
		
		if(stage == CalcStage.Prepare || averageTi == null){ //preparation: clear all fits and reget them
			if(configW7X.ionTemp.averageSets == null)
				configW7X.ionTemp.averageSets = new HashMap<>();
			
			averageSetName = new String[nSets];
			averageTi = new double[nSets][nT];
			averageTiSource = new double[nSets][][];
			
			int iS=0;
		
			for(Entry<String,List<String>> entry : configW7X.ionTemp.averageSets.entrySet()) {
				averageSetName[iS] = entry.getKey();
				List<String> specEntryNames = entry.getValue();
				
				averageTiSource[iS] = new double[specEntryNames.size()][];
				for(int iSE=0; iSE < specEntryNames.size(); iSE++) {
					FastSpecEntry specEntry = configW7X.getPointByName(specEntryNames.get(iSE));
					averageTiSource[iS][iSE] = specEntry.fit.inferredQuantities.get("Ti");
				}
				
				iS++;
			}
		}
				
		for(int iS=0; iS < nSets; iS++) {
			double[] values = new double[averageTiSource[iS].length];
			
			for(int iT=0; iT < nT; iT++) {
				if(iF >= 0 && iF != iT)
					continue;
				
				int iV = 0;
				for(int iSE=0; iSE < averageTiSource[iS].length; iSE++) {
					if(Double.isFinite(averageTiSource[iS][iSE][iT]))
						values[iV++] = averageTiSource[iS][iSE][iT];
				}
				
				averageTi[iS][iT] = (iV == 0) ? Double.NaN :
					Algorithms.median(Arrays.copyOf(values, iV));
			}
			
			String qName = "TiMedian_" + averageSetName[iS];
			if(!configW7X.inferredQuantities.contains(qName))
				configW7X.inferredQuantities.add(qName);

			for(String specEntryName : configW7X.ionTemp.averageSets.get(averageSetName[iS])) {
				FastSpecEntry specEntry = configW7X.getPointByName(specEntryName);
				specEntry.fit.inferredQuantities.put(qName, averageTi[iS]);
			}
		}
			
	}

	private double[][] getBField() {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfigW7X();
		
		//get local magnetic field vector from VMEC for all observation points 
		Vmec_Service vmecService = new Vmec_Service();		
		Vmec vmec = vmecService.getVmecSOAP();
		
		
		String vmecID;
		if(configW7X.obsPos.vmecID == null || configW7X.obsPos.vmecID.startsWith("auto")){
			proc.setStatus("Getting VMEC ID");
			
			String pre = configW7X.obsPos.vmecID.replaceAll(":.*", "");
			vmecID = W7XUtil.getVMECBestID(proc.getConnectedSource(), 
									configW7X.obsPos.vmecID.startsWith("autoVacuum"));
			configW7X.obsPos.vmecID = pre.trim() + ":" + vmecID.trim();	
		}else{
			vmecID = configW7X.obsPos.vmecID;
		}
		
		
		//get observation position for angle between beam and field
		double[][] obsPos = (double[][])Mat.fixWeirdContainers(proc.getSeriesMetaData("/SpecCal/linesOfSight/nominalBeamIntersectionPos"), true);
		if(obsPos == null)
			throw new RuntimeException("No observation position data. Did you turn on 'Process beam data' in SpecCalProcessor?");
			
				
		Points3D p3d = new Points3D();
		
		double obsPosT[][] = Mat.transpose(obsPos);
		if(!Mat.hasNotOnlyNaN(obsPosT[0]))
			throw new RuntimeException("All observation positions are NaN, something broke.");
		
		p3d.setX1(obsPosT[0]);
		p3d.setX2(obsPosT[1]);
		p3d.setX3(obsPosT[2]);
		
		Points3D b3d = vmec.magneticField(vmecID, p3d);
		
		//pick up Reff while we're here
		double nearestREff[] = vmec.getReff(vmecID, p3d);
		proc.getConnectedSource().setSeriesMetaData("SpecCal/linesOfSight/nominalNearestBeamIntersectionREff", nearestREff, false);
		proc.getConnectedSource().setSeriesMetaData("SpecCal/linesOfSight/nominalBeamIntersectionREff", nearestREff, false);
		
		double nearestB[][] = Mat.transpose(new double[][]{ b3d.getX1(), b3d.getX2(), b3d.getX3() });
		
		if(configW7X.obsPos.weightedAverageREff) {
			int imageHeight = proc.getConnectedSource().getImage(0).getHeight();
			
			double[][] losStart = (double[][]) proc.getConnectedSource().getSeriesMetaData("SpecCal/linesOfSight/startPos");
			double[][] losUVec = (double[][]) proc.getConnectedSource().getSeriesMetaData("SpecCal/linesOfSight/unitVec");	
			double maxL = configW7X.obsPos.maxDistToBeam;
			double dl = configW7X.obsPos.losResolution;
			
			double beamInfo[][][] = null;
			try{
				beamInfo = W7XUtil.getExistingBeamInfo(proc.getConnectedSource());
				//beamInfo = W7XUtil.getBeamInfo(connectedSource, false);
			}catch(Exception err){
				throw new RuntimeException("No Beam energy for beamOn selection. Add a SeriesProcessor!", err);
			}
			if(beamInfo == null)
				throw new RuntimeException("No Beam energy for beamOn selection. Add a SeriesProcessor!");
			
			double beamPower[][] = beamInfo[2];

			//double weightedB[][] = new double[imageHeight][3];
			double weightedREff[] = new double[imageHeight];
			for(int iY=0; iY < imageHeight; iY++) {				
				double sumI = 0;
				double sumIr = 0;
				//double sumIB[] = new double[3];
				
				if(proc.isAborting())
					throw new RuntimeException("Aborted during weighted <rEff> calc (row " + iY + ")");				
							
				proc.setStatus("Getting weighted <rEff> for row " + iY + " / " + imageHeight + " ");
				
				for(int iB=0; iB < 8; iB++) {
					//Is this beam on at this time and also in view (< threshold distance from LOS)
					//we dont have time, so... err.. is ever on??
					if(beamPower == null || beamPower[iB] == null || Mat.max(beamPower[iB]) < configW7X.beamOnPowerThreshold){
						continue;
					}
						
					double beamStart[] = W7XUtil.getBeamStart(iB);
					double beamUVec[] = W7XUtil.getBeamUVec(iB);
					
					double s = Algorithms.pointOnLineNearestAnotherLine(losStart[iY], losUVec[iY], beamStart, beamUVec);
					double pOnLOS[] = Mat.add(losStart[iY], Mat.mul(losUVec[iY], s));
					if(!Double.isFinite(s) || s < 0 || s > configW7X.obsPos.losLength) 
						continue; //ignore beams behind us
					
					s = Algorithms.pointOnLineNearestAnotherLine(beamStart, beamUVec, losStart[iY], losUVec[iY]);
					double pOnBeam[] = Mat.add(beamStart, Mat.mul(beamUVec, s));
					
					// Gaussian beam density, perp to beam, find weighted average of pos along LOS					
					double l[] = Mat.linspace(-maxL, maxL, dl);
					double sig = configW7X.obsPos.beamFWHM / 2.35;
					obsPos = new double[3][l.length];	
					for(int iL=0; iL < l.length; iL++) {
						double p[] = Mat.add(pOnLOS, Mat.mul(beamUVec, l[iL]));
						
						double r = Mat.veclength(Mat.subtract(p, pOnBeam));
						
						if(r <= configW7X.obsPos.maxDistToBeam) {						
							obsPos[0][iL] = p[0];
							obsPos[1][iL] = p[1];
							obsPos[2][iL] = p[2];
						}else {						
							obsPos[0][iL] = Double.NaN;
							obsPos[1][iL] = Double.NaN;
							obsPos[2][iL] = Double.NaN;							
						}
					}					
					p3d = new Points3D();
					
					if(!Mat.hasNotOnlyNaN(obsPos[0]))
						Logger.getLogger(this.getClass().getName()).warning("LOS " + iY + " observation positions are all NaN.");
					
					p3d.setX1(obsPos[0]);
					p3d.setX2(obsPos[1]);
					p3d.setX3(obsPos[2]);
						
					//b3d = vmec.magneticField(vmecID, p3d);
					//double B[][] = Mat.transpose(new double[][]{ b3d.getX1(), b3d.getX2(), b3d.getX3() });
					
					double rEff[] = vmec.getReff(vmecID, p3d);					
					double dbg[][] = new double[l.length][3];
					for(int iL=0; iL < l.length; iL++) {
						if(!Double.isFinite(rEff[iL]))
							continue;
						
						//radius on beam (ish)
						double r = Mat.veclength(Mat.subtract(Mat.getColumn(obsPos, iL), pOnBeam));
						
						double I = FastMath.exp(-(r*r)/(2*sig*sig));					
						sumI += I;
						sumIr += I*rEff[iL];
						
						dbg[iL] = new double[] { rEff[iL], r, I };						
					}

					//if(iY == 20) {
					//	Mat.mustWriteAscii("/tmp/dbg.txt", dbg);
					//}
				}
				weightedREff[iY] = sumIr / sumI; 
				//weightedB[iY] = new double[] { sumIB[0] / sumI, sumIB[1] / sumI, sumIB[2] / sumI };
				
			}
			proc.getConnectedSource().setSeriesMetaData("SpecCal/linesOfSight/nominalWeightedBeamIntersectionREff", weightedREff, false);
			proc.getConnectedSource().setSeriesMetaData("SpecCal/linesOfSight/nominalBeamIntersectionREff", weightedREff, false);
			
			if(!Mat.hasNotOnlyNaN(weightedREff))
				throw new RuntimeException("All observation positions are NaN, something broke.");
			
			//return weightedB;
		}
		
		return nearestB;
	}

	public void saveResults() {
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfigW7X();
		
		proc.saveFitResults(configW7X.saveResultsPath + "_Ti_DATASTREAM", "Ti");
		if(configW7X.calcFitUncertainty)
			proc.saveFitResults(configW7X.saveResultsPath + "_Ti_uncertainty_DATASTREAM", "Ti_uncertainty");
		proc.saveFitResults(configW7X.saveResultsPath + "_BeamIntersectionX_DATASTREAM", "BeamIntersectionX");
		proc.saveFitResults(configW7X.saveResultsPath + "_BeamIntersectionY_DATASTREAM", "BeamIntersectionY");
		proc.saveFitResults(configW7X.saveResultsPath + "_BeamIntersectionZ_DATASTREAM", "BeamIntersectionZ");
		
		if(averageSetName != null) {
			for(int iS=0; iS < averageSetName.length; iS++) {
				proc.saveScalarStream(configW7X.saveResultsPath + "_TiMedian_" + averageSetName[iS], averageTi[iS], "TiMedian", "Median average Ti over multiple spatial channels");
			}
		}
	}

}
