package imageProc.proc.fastSpec.ionTemp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Configuration for ion temperature calculation
 * including instrument function, zeeman and fine structure corrections 
 */
public class IonTempConfig {

	/** Calculate ion temperature from fitted width, theoretical wavelength and atomic mass 
	 * corrected for zeemand splitting, fine structure and instrument function
	 * using the gaussian fitting function in the Zeeman webservice */
	public boolean enable = false;
	
	/** Must be on to include Zeeman/FS corrections, otherwise only a simplified 
	 * instrument function correction is done. */
	public boolean useZeemanService = true;
	
	/** Line name for the Zeeman/FS service */
	public String serviceLineName = "sulfur436line_argonMass";
	//public String serviceLineName = "carbon529line";

	public boolean inhibitInstrumentFucntion = false;
	
	public boolean inhibitFineStructure = false;
	
	/** When B field information isn't available, e.g. if beam information isn't (beams are off)
	 * the Zeeman correction will normally fail. If this is set, ignore that calculate with 0 field
	 * (no zeeman correction) */
	boolean allowWithoutFieldInfo = false;
	
	/** For each map entry, average the channels with the names of the map value string array
	 * and place in a stream with name of the map key */
	public Map<String,List<String>> averageSets;

	/** Set Ti to NaN if the per-timepoint intersection location is NaN. This stops
	 * the productiong of Ti values for channels that don't look at any beams that are on. */
	public boolean noTiWhenNanPos = true;
}
