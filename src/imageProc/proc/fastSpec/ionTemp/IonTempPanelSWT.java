package imageProc.proc.fastSpec.ionTemp;

import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.fastSpec.swt.FastSpecSpecificPanel;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class IonTempPanelSWT extends FastSpecSpecificPanel {
	
	private FastSpecProcessorW7X proc;
		
	private Button enableCheckbox;
	private Button useServiceCheckbox;
	private Button inhibitInstrumentFunctionCheckbox;
	private Button inhibitFineStructureCheckbox;
	private Button allowWithoutFieldInfoCheckbox;
	private Button noTiWhenNanPosCheckbox;
	private Text averageSetsTextbox;
	
	private Button radiantEnergyCheckbox;
	
	private Text lineNameText;
	
	private boolean inhibitSettingsChanged;
	
	public IonTempPanelSWT(FastSpecProcessorW7X proc, Composite parent) {
		this.proc = proc;
		
		Label lEN = new Label(parent, SWT.NONE); lEN.setText("");
		enableCheckbox = new Button(parent, SWT.CHECK);
		enableCheckbox.setText("Enable");
		enableCheckbox.setToolTipText("Enables calculation of Ti");
		enableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		enableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lUZ = new Label(parent, SWT.NONE); lUZ.setText("");
		useServiceCheckbox = new Button(parent, SWT.CHECK);
		useServiceCheckbox.setText("Use Zeeman service to return 'true' Ti based on fitted sigma. Otherwise just calculate it using the naive Doppler broadening ignoring "
								+ "Zeeman/fine structure corrections and taking the instrument function width as a normal Gaussian (not super-Gaussian)");
		useServiceCheckbox.setToolTipText("Enables Zeeman/FS/Instrument function corrections using Zeeman service");
		useServiceCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		useServiceCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lII = new Label(parent, SWT.NONE); lII.setText("");
		inhibitInstrumentFunctionCheckbox = new Button(parent, SWT.CHECK);
		inhibitInstrumentFunctionCheckbox.setText("Inhibit instrument function");
		inhibitInstrumentFunctionCheckbox.setToolTipText("Do not include correction of instrument function.");
		inhibitInstrumentFunctionCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		inhibitInstrumentFunctionCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });


		Label lIF = new Label(parent, SWT.NONE); lIF.setText("");
		inhibitFineStructureCheckbox = new Button(parent, SWT.CHECK);
		inhibitFineStructureCheckbox.setText("Inhibit fine structure/Zeeman");
		inhibitFineStructureCheckbox.setToolTipText("Do not include fine structure or Zeeman splitting.");
		inhibitFineStructureCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		inhibitFineStructureCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lWF = new Label(parent, SWT.NONE); lWF.setText("");
		allowWithoutFieldInfoCheckbox = new Button(parent, SWT.CHECK);
		allowWithoutFieldInfoCheckbox.setText("Allow Ti corrections without field/location info");
		allowWithoutFieldInfoCheckbox.setToolTipText("When location and/or field information is not available, make Ti correction calculations with 0 magnetic field, so no Zeeman correction.");
		allowWithoutFieldInfoCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		allowWithoutFieldInfoCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lNT = new Label(parent, SWT.NONE); lWF.setText("");
		noTiWhenNanPosCheckbox = new Button(parent, SWT.CHECK);
		noTiWhenNanPosCheckbox.setText("Invalidate Ti when intersection position invalid");
		noTiWhenNanPosCheckbox.setToolTipText("Set Ti to NaN if the per-timepoint intersection location is NaN.\n This stops the production of Ti values for channels that don't look at any beams that are on.");
		noTiWhenNanPosCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		noTiWhenNanPosCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		Label lLN = new Label(parent, SWT.NONE); lLN.setText("Line ID:");
		lineNameText = new Text(parent, SWT.NONE);
		lineNameText.setText("Line ID/name in Zeeman service");
		lineNameText.setToolTipText("Scale all intensity values by this. Allows adjustment for gas/plasma density");
		lineNameText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		lineNameText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lRE = new Label(parent, SWT.NONE); lRE.setText("");
		radiantEnergyCheckbox = new Button(parent, SWT.CHECK);
		radiantEnergyCheckbox.setText("Calculate radiant spectral energy");
		radiantEnergyCheckbox.setToolTipText("Caclulate the frame integral of the spectral radiance from the intensity [photons m^-2 SR^-1].");
		radiantEnergyCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		radiantEnergyCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lAS = new Label(parent, SWT.NONE); lAS.setText("Average sets:");		
		averageSetsTextbox = new Text(parent, SWT.MULTI);
		averageSetsTextbox.setText("{}");
		averageSetsTextbox.setToolTipText("JSON: Average sets of channels and store in stream with set name, e.g. {'core':['ch1','ch2'],'edge':['ch30','ch31']}");
		averageSetsTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		averageSetsTextbox.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		doUpdate();
	}
	
	@Override
	public void doUpdate() {
		inhibitSettingsChanged = true;
		try{
			FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
			
			enableCheckbox.setSelection(configW7X.ionTemp.enable);
			useServiceCheckbox.setSelection(configW7X.ionTemp.useZeemanService);
			lineNameText.setText(configW7X.ionTemp.serviceLineName);
			inhibitInstrumentFunctionCheckbox.setSelection(configW7X.ionTemp.inhibitInstrumentFucntion);
			inhibitFineStructureCheckbox.setSelection(configW7X.ionTemp.inhibitFineStructure);
			radiantEnergyCheckbox.setSelection(configW7X.radianceConfig.enable);
			allowWithoutFieldInfoCheckbox.setSelection(configW7X.ionTemp.allowWithoutFieldInfo);
			noTiWhenNanPosCheckbox.setSelection(configW7X.ionTemp.noTiWhenNanPos);
			Gson gson = new Gson();
			if(!averageSetsTextbox.isFocusControl())
				averageSetsTextbox.setText(gson.toJson(configW7X.ionTemp.averageSets));
			
			
		}finally{
			inhibitSettingsChanged = false;
		}
	}
	
	private void settingsChangedEvent(Event e) {
		if(inhibitSettingsChanged)
			return;
		
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		configW7X.ionTemp.enable = enableCheckbox.getSelection();
		configW7X.ionTemp.useZeemanService = useServiceCheckbox.getSelection();
		configW7X.ionTemp.serviceLineName = lineNameText.getText();
		configW7X.ionTemp.inhibitInstrumentFucntion = inhibitInstrumentFunctionCheckbox.getSelection();
		configW7X.ionTemp.inhibitFineStructure = inhibitFineStructureCheckbox.getSelection();
		configW7X.ionTemp.allowWithoutFieldInfo = allowWithoutFieldInfoCheckbox.getSelection();
		configW7X.ionTemp.noTiWhenNanPos = noTiWhenNanPosCheckbox.getSelection();		
		configW7X.radianceConfig.enable = radiantEnergyCheckbox.getSelection();
		Gson gson = new Gson();
		configW7X.ionTemp.averageSets = gson.fromJson(averageSetsTextbox.getText(), Map.class);
		
	}
}
