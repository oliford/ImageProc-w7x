package imageProc.proc.fastSpec.passiveInit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.fastSpec.swt.FastSpecSpecificPanel;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class PassiveInitialiserPanelSWT extends FastSpecSpecificPanel {
	
	private Text sourceFitsPathTextbox;	
	
	private Button enableCheckbox;
	private Button setAmplitudesCheckbox;
	private Button setSigmasCheckbox;
	private Button setPowerCheckbox;
	private Button makePriorCheckbox;
	private Button applyButton;
	
	private boolean inhibitSettingsChanged;
	
	private FastSpecProcessorW7X proc;
	
	public PassiveInitialiserPanelSWT(FastSpecProcessorW7X proc, Composite parent) {
		this.proc = proc;
		
		Label lE = new Label(parent, SWT.NONE); lE.setText("");
		enableCheckbox = new Button(parent, SWT.CHECK);
		enableCheckbox.setText("Enable");
		enableCheckbox.setToolTipText("Enable");
		enableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		enableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lAC = new Label(parent, SWT.NONE); lAC.setText("Set:");
		setAmplitudesCheckbox = new Button(parent, SWT.CHECK);
		setAmplitudesCheckbox.setText("Amplitude");
		setAmplitudesCheckbox.setToolTipText("Set the passive amplitudes according to the predictions.");
		setAmplitudesCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		setAmplitudesCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		setSigmasCheckbox = new Button(parent, SWT.CHECK);
		setSigmasCheckbox.setText("Sigma");
		setSigmasCheckbox.setToolTipText("Set the passive sigma according to the predictions.");
		setSigmasCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		setSigmasCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		setPowerCheckbox = new Button(parent, SWT.CHECK);
		setPowerCheckbox.setText("Power");
		setPowerCheckbox.setToolTipText("Set the passive power according to the predictions.");
		setPowerCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
		setPowerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label l2 = new Label(parent, SWT.CHECK); l2.setText("");
		makePriorCheckbox = new Button(parent, SWT.CHECK);
		makePriorCheckbox.setText("Set prior");
		makePriorCheckbox.setToolTipText("Sets the prior to the calculated value instead of the init/fixed value.");
		makePriorCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		makePriorCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label l3 = new Label(parent, SWT.CHECK); l3.setText("");
		applyButton = new Button(parent, SWT.PUSH);
		applyButton.setText("Apply");
		applyButton.setToolTipText("Set the init/prior ");
		applyButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		applyButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.getPassiveInit().prepare(); } });

		doUpdate();
		
	}
	
	@Override
	public void doUpdate() {
		inhibitSettingsChanged = true;
		try{
			FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();

			enableCheckbox.setSelection(configW7X.passiveInit.enable);
			makePriorCheckbox.setSelection(configW7X.passiveInit.setPrior);
			setAmplitudesCheckbox.setSelection(configW7X.passiveInit.setAmplitude);
			setSigmasCheckbox.setSelection(configW7X.passiveInit.setSigma);
			setPowerCheckbox.setSelection(configW7X.passiveInit.setPower);
			
		}finally{
			inhibitSettingsChanged = false;
		}
	}
	
	private void settingsChangedEvent(Event e) {
		if(inhibitSettingsChanged)
			return;
		
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
	
		configW7X.passiveInit.enable = enableCheckbox.getSelection();
		configW7X.passiveInit.setPrior = makePriorCheckbox.getSelection();
		configW7X.passiveInit.setAmplitude = setAmplitudesCheckbox.getSelection();
		configW7X.passiveInit.setSigma = setSigmasCheckbox.getSelection();
		configW7X.passiveInit.setPower = setPowerCheckbox.getSelection();
		
	}
}
