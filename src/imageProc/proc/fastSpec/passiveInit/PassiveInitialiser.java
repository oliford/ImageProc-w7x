package imageProc.proc.fastSpec.passiveInit;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import algorithmrepository.Algorithms;
import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.Mat;
import binaryMatrixFile.AsciiMatrixFile;
import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecEntry.FitResults;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.w7x.W7XUtil;
import oneLiners.OneLiners;

/** per-frame initialisation of passive impurity emission based on fits
 * from a different spectrometer and some model or machine learning training 
 * @author specgui
 *
 */
public class PassiveInitialiser {

	private FastSpecProcessorW7X proc;
	
	public PassiveInitialiser(FastSpecProcessorW7X proc) {
		this.proc = proc;
	}

	public void prepare() {
		FastSpecConfigW7X configW7X = proc.getConfigW7X();
		
		int nFrames = proc.getConnectedSource().getNumImages();
		
		String progID = (String)proc.getConnectedSource().getSeriesMetaData("w7x/programID");
				
		double predAll[][] = Mat.mustLoadAscii("/data/passiveInit/"+progID+"/prediction.txt", true);
		double t[] = Mat.mustLoadAscii("/data/passiveInit/"+progID+"/nanotime.txt", true)[0];
		String chans[] = OneLiners.linesFromFile("/data/passiveInit/"+progID+"/channels.txt");
				
//		for(FastSpecEntry specEntry : configW7X.specEntries) {
		Pattern p = Pattern.compile("(.*)/(.*)=(.*)/(.*)");
		Logger logr = Logger.getLogger(this.getClass().getName());
		for(int iC=0; iC < chans.length; iC++) {
			Matcher m = p.matcher(chans[iC]);
			if(!m.matches()) {
				logr.warning("Unrecognised channel format for " + chans[iC]);
				continue;
			}
			String spectrometer = m.group(1);
			String chan = m.group(2);
			String los = m.group(3);
			String property = m.group(4);
			
			FastSpecEntry specEntry = configW7X.getPointByName(chan + "_PCX");
			if(specEntry == null) {
				logr.warning("No matching channel '" + chan + "' found for predicted " + chans[iC]);
				continue;
			}
			
			double[] pred = predAll[iC];
			Interpolation1D interp = new Interpolation1D(t, pred, InterpolationMode.LINEAR, ExtrapolationMode.CONSTANT, Double.NaN);
			
			long nanotimeL[] = W7XUtil.getBestNanotime(proc.getConnectedSource());
			double nanotimeD[] = new double[nanotimeL.length];
			Mat.convert1DArray(nanotimeL, nanotimeD);
			
			if(nanotimeD[nanotimeD.length-1] < t[0] || nanotimeD[0] > t[t.length-1]) {
				throw new RuntimeException("Predictions are entirely out of time range of the data");
			}

			double[] predInterp = interp.eval(nanotimeD);
			
			if(specEntry.fit == null)
				specEntry.fit = new FitResults();
			
			if(property.equals("Amplitude")) {
				if(configW7X.passiveInit.enable && configW7X.passiveInit.setAmplitude) {

					specEntry.fit.frameAmplitudeInit = predInterp;
					if(configW7X.passiveInit.setPrior) {
						specEntry.fit.frameAmplitudePriorMean = predInterp;
					}else {
						specEntry.fit.frameAmplitudePriorMean = null;
					}
				}else {
					specEntry.fit.frameAmplitudePriorMean = null;
					specEntry.fit.frameAmplitudeInit = null;
				}
			}else if(property.equals("Sigma")) {
				if(configW7X.passiveInit.enable && configW7X.passiveInit.setSigma) {

					specEntry.fit.frameSigmaInit = predInterp;
					if(configW7X.passiveInit.setPrior) {
						specEntry.fit.frameSigmaPriorMean = predInterp;
					}else {
						specEntry.fit.frameSigmaPriorMean = null;
					}
				}else {
					specEntry.fit.frameSigmaPriorMean = null;
					specEntry.fit.frameSigmaInit = null;
				}
			}else if(property.equals("Power")) {
				if(configW7X.passiveInit.enable && configW7X.passiveInit.setPower) {

					specEntry.fit.framePowerInit = predInterp;
					if(configW7X.passiveInit.setPrior) {
						specEntry.fit.framePowerPriorMean = predInterp;
					}else {
						specEntry.fit.framePowerPriorMean = null;
					}
				}else {
					specEntry.fit.framePowerPriorMean = null;
					specEntry.fit.framePowerInit = null;
				}
			}
		}
		
	}
}
