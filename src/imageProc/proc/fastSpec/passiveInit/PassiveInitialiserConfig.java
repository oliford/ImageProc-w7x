package imageProc.proc.fastSpec.passiveInit;

public class PassiveInitialiserConfig {
	
	public boolean enable = false;

	public boolean setPrior = false;
	
	public boolean setAmplitude = true;
	
	public boolean setSigma = true;
	
	public boolean setPower = true;

}
