package imageProc.proc.obsPos;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;


import imageProc.proc.fastSpec.FastSpecConfigW7X;
import imageProc.proc.fastSpec.FastSpecProcessorW7X;
import imageProc.proc.fastSpec.swt.FastSpecSpecificPanel;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class FastSpecObsPosPanelSWT extends FastSpecSpecificPanel {

	private FastSpecProcessorW7X proc;
	
	private Button weightedReffCheckbox;
	private Text vmecIDText;
	
	private Text beamFWHMText;
	
	private boolean inhibitSettingsChanged;
	
	public FastSpecObsPosPanelSWT(FastSpecProcessorW7X proc, Composite parent) {
		this.proc = proc;
		
		Label lEN = new Label(parent, SWT.NONE); lEN.setText("");
		weightedReffCheckbox = new Button(parent, SWT.CHECK);
		weightedReffCheckbox.setText("Enable weighted <rEff>");
		weightedReffCheckbox.setToolTipText("Enable calculation of weighted Reff over a Gaussian beam");
		weightedReffCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		weightedReffCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lVM = new Label(parent, SWT.NONE); lVM.setText("VMEC ID:");
		vmecIDText = new Text(parent, SWT.NONE);
		vmecIDText.setText("auto");
		vmecIDText.setToolTipText("VMEC ID to use for B field, 'auto' or 'autoVacuum'. Used to calculated B.LOS angle. The correction is not usually very sensitive to this.");
		vmecIDText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		vmecIDText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lLN = new Label(parent, SWT.NONE); lLN.setText("Beam width:");
		beamFWHMText = new Text(parent, SWT.NONE);
		beamFWHMText.setText("0");
		beamFWHMText.setToolTipText("Beam full width half max [m]");
		beamFWHMText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
		beamFWHMText.addListener(SWT.Modify, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		doUpdate();
	}

	@Override
	public void doUpdate() {
		inhibitSettingsChanged = true;
		try{
			FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
			
			weightedReffCheckbox.setSelection(configW7X.obsPos.weightedAverageREff);
			beamFWHMText.setText(Double.toString(configW7X.obsPos.beamFWHM));
			vmecIDText.setText(configW7X.obsPos.vmecID);

		}finally{
			inhibitSettingsChanged = false;
		}
	}
	
	private void settingsChangedEvent(Event e) {
		if(inhibitSettingsChanged)
			return;
		
		FastSpecConfigW7X configW7X = (FastSpecConfigW7X)proc.getConfig();
		
		configW7X.obsPos.weightedAverageREff = weightedReffCheckbox.getSelection();
		configW7X.obsPos.beamFWHM = Algorithms.mustParseDouble(beamFWHMText.getText());
		configW7X.obsPos.vmecID = vmecIDText.getText();
		
	}
}
