package imageProc.proc.obsPos;

/** Configuration for geometry calculations, particularly vmec and intersection of LOS with NBI. */
public class FastSpecObsPosConfig {

	/** VMEC ID to use for magnetic field */
	public String vmecID = "auto";
	
	public boolean weightedAverageREff = true;
	
	/** Maximem length of LOS from start to evaluate. Beyond this we ignore crossings with beams */
	public double losLength = 3;
	
	public double maxDistToBeam = 0.500; //maximum distance to a beam to consider it intersecting it
	
	/** Resolution of points along LOS to evaluate beam density and Reff */ 
	public double losResolution = 0.02;
	
	/** FWHM of Gaussian beam density, perp to beam */
	public double beamFWHM = 0.20;
	
	/** Power threshold to consider beam as contributing to signal */
	public double beamOnPowerThreshold = 50000; //50 kW
	
}
