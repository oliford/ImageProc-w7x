package imageProc.proc.seriesAvg;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.logging.Level;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonStreamParser;

import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.proc.seriesAvg.SeriesProcessorW7XConfig.AutocorrelationSearchConfig;
import imageProc.proc.seriesAvg.SeriesProcessorW7XConfig.EntryCollect;
import imageProc.w7x.W7XUtil;
import oneLiners.OneLiners;
import uk.co.oliford.jolu.AsciiMatrixFile;
import uk.co.oliford.jolu.DataConvertPureJava;
import algorithmrepository.Algorithms;
import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.Mat;
import algorithmrepository.matrix.Matrix;
import descriptors.w7x.ArchiveDBDesc;

/** Image series processor.
 * 
 * Time series manipulation:
 * 		- Selection cuts (only processing specific image ranges)
 * 		- Radiation removal (Medial average, spike removal ... )
 * 		- Temporal Averaging
 * 
 * @author oliford
 *
 */
public class SeriesProcessorW7X extends SeriesProcessor {

	private final double beamOnPowerThreshold = 100000; //500 kW
		
	private SeriesProcessorW7XConfig configW7X;
	
	public SeriesProcessorW7X() {
		this(null, -1);
	}
	
	public SeriesProcessorW7X(ImgSource source, int selectedIndex) {
		super(source, selectedIndex);
		configW7X = new SeriesProcessorW7XConfig();
		config = configW7X;
		config.map.clear();
		//bulkAlloc.setMaxMemoryBufferAlloc(Long.MIN_VALUE);
	}
	
	@Override
	public SeriesProcessorW7X clone() { return new SeriesProcessorW7X(connectedSource, getSelectedSourceIndex());	}

	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			SeriesSWTControllerW7X controller = new SeriesSWTControllerW7X((Composite)args[0], (Integer)args[1], this, asSink);
			controllers.add(controller);
			return controller;
		}
		return null;
	}
	
	@Override
	protected boolean checkOutputSet(int nImagesIn) {
		//if(nImagesIn <= 0) //break profile init
			//throw new RuntimeException("No input images");
		if(configW7X.timebaseOffsetSearch != null && configW7X.timebaseOffsetSearch.enable)
			doFindOffset();
		
		if(configW7X.autoBeamConfig != null){	
			boolean anyEnabled = configFromNBI(configW7X.autoBeamConfig);
			if(!anyEnabled)
				throw new RuntimeException("Beam autoconfig enabled but no beams were found");
			
		}else{
			//for all _ON, look for _PRE and _POST for BG subtract
			configFromNBI(null);
		}
		
		//some debugging output about determination of original frames		
		int inputIsBackground[] = new int[nImagesIn];
		int inputIsSignal[] = new int[nImagesIn];
		int inputIsGoodData[] = new int[nImagesIn];
		int inputFirstIndex[] = new int[nImagesIn];
		for(ConfigEntry a : config.map.values()){
			for(int i=a.inIdx0; i < a.inIdx1; i++){
				inputFirstIndex[i] = a.inIdx0;
				if(a.enable){
					if(a.name.endsWith("_BG_PRE") || a.name.endsWith("_POST")){
						inputIsBackground[i]+=10000;
					}else{
						inputIsSignal[i]+=10000;
					}
					if(a.markAsGoodData)
						inputIsGoodData[i]+=10000;
				}
			}
		}
		
		connectedSource.addNonTimeSeriesMetaDataMap("SeriesProc/config/", configW7X.toMap());
		connectedSource.setSeriesMetaData("SeriesProc/inputIsBackground", inputIsBackground, true);
		connectedSource.setSeriesMetaData("SeriesProc/inputIsSignal", inputIsSignal, true);
		connectedSource.setSeriesMetaData("SeriesProc/inputIsGoodData", inputIsGoodData, true);
		connectedSource.setSeriesMetaData("SeriesProc/inputFirstIndex", inputFirstIndex, true);
		try{
			setSeriesMetaData("SeriesProc/config/autoBeamConfig", configW7X.autoBeamConfig, false);
			setSeriesMetaData("SeriesProc/config/combineBeamOnPhases", configW7X.combineBeamOnPhases.toString(), false);
			setSeriesMetaData("SeriesProc/config/combineMaxBlockDurations", Double.toString(configW7X.combineMaxBlockDuration), false);
			setSeriesMetaData("SeriesProc/config/extendBeamOnWindow", configW7X.extendBeamOnWindow, false);
			setSeriesMetaData("SeriesProc/config/numBackgroundFrames", configW7X.numBackgroundFrames, false);
			setSeriesMetaData("SeriesProc/config/subtractBackground", configW7X.subtractBackground, false);
			
		}catch(RuntimeException err){
			err.printStackTrace();
		}
		return super.checkOutputSet(nImagesIn);
	}
	

	/**
	 * @param beamsOn for each beam, -1=don't want, 0=don't care, 1=require, 2=at least one of these, 3=exactly one of these
	 * @return True if any frames matched the criteria. I.e. if any other than the background frame are enabled  
	 */
	public boolean configFromNBI(int beamSel[]) {
		DecimalFormat fmt = new DecimalFormat("000");
			
		long nanoTime[] = W7XUtil.getBestNanotime(connectedSource).clone();
		
		if(beamSel != null){
			
			config.map.clear();
			config.map.put("0Level", 
						new ConfigEntry("0Level", false, 0, 1, 1, -1, false, ConfigEntry.RAD_NONE, -1));
			
						
			double beamInfo[][][] = getBeamInfo();
			double beamPower[][] = beamInfo[2];
			
			String currentCfgName = null;
			ConfigEntry currentCfg = null;
			boolean currentBeams[] = null;
			
			int nImagesIn = connectedSource.getNumImages();
			int configNum = 0;
			for(int iF=0; iF < nImagesIn; iF++){
				//long nano = nanoTime[iF] + (long)(timebaseOffset*1e9);
				
				//work out what the NBI combination is
				boolean beams[] = new boolean[8];
				boolean isSameCfg = true;
				int j=0;
				
				for(int iB=0; iB < 8; iB++){
					if(beamPower[iB] == null){
						beams[iB] = false;
					}else{
						for(int k = Integer.max(0, iF-configW7X.extendBeamOnWindow); k <= Integer.min(beamPower[iB].length-1, iF+configW7X.extendBeamOnWindow); k++)
							beams[iB] |= beamPower[iB][k] > beamOnPowerThreshold;
					}
					isSameCfg &= (currentBeams != null && (beams[j] == currentBeams[j] || beamSel[j] == SeriesProcessorW7XConfig.BEAMSEL_CANTSEE));
					j++;					
				}
				
				if(!isSameCfg){
					
					//config has changed (or is the first)
					if(currentCfg != null){
							
						currentCfg.inIdx1 = iF; //terminate
						currentCfg.radRemoval = ConfigEntry.RAD_NONE;
						currentCfg.spikeThreshold = 0;
											
						config.map.put(currentCfgName, currentCfg);
						configNum++;
					}
					
					//now set up the new one
					currentBeams = beams;
					//generate name
					String beamIDs = "";
					boolean enable = true;
					int nAtLeastOne = -1, nOnlyOne = -1;
					for(j=0; j < 8; j++){
						if(beams[j]  && beamSel[j] != SeriesProcessorW7XConfig.BEAMSEL_CANTSEE)
							beamIDs += (j+1); //beam 'Q' names are 1 based
						
						switch (beamSel[j]) {
							case SeriesProcessorW7XConfig.BEAMSEL_DONTWANT:
								if(beams[j])
									enable = false;
								break;
								
							case SeriesProcessorW7XConfig.BEAMSEL_CANTSEE:							
							case SeriesProcessorW7XConfig.BEAMSEL_DONTCARE:
								break;
								
							case SeriesProcessorW7XConfig.BEAMSEL_REQUIRE:
								if(!beams[j])
									enable = false;
								break;
								
							case SeriesProcessorW7XConfig.BEAMSEL_AT_LEAST_ONE:
								if(nAtLeastOne < 0)
									nAtLeastOne = 0;
								if(beams[j])
									nAtLeastOne++;
								break;
								
								
							case SeriesProcessorW7XConfig.BEAMSEL_ONLY_ONE:
								if(nOnlyOne < 0)
									nOnlyOne = 0;
								if(beams[j])
									nOnlyOne++;
						}
					}
					currentCfgName = "AUTO_" + fmt.format(configNum) + 
							((beamIDs.length() > 0) ? ("_Q"+beamIDs+"_ON") : "_NONE");
					
					if(nAtLeastOne == 0)
						enable = false;
					if(nOnlyOne == 0 || nOnlyOne > 1)
						enable = false;
					
					//if specified, enabled when the selected one is on, otherwise any
					
					currentCfg = new ConfigEntry(currentCfgName, enable, iF, iF+1, 1, -1, false, ConfigEntry.RAD_NONE, -1);
				}
			}
	
			
			if(currentCfg != null){
				currentCfg.inIdx1 = nImagesIn; //terminate at end
				currentCfg.radRemoval = ConfigEntry.RAD_NONE;
				currentCfg.spikeThreshold = 0;
				config.map.put(currentCfgName, currentCfg);
				configNum++;
			}
		}
		
		for(Entry<String, ConfigEntry> entry : config.map.entrySet()){
			if(entry.getValue().name == null){
				entry.getValue().name = entry.getKey();
				System.out.println("WARNING: Set null name on config entry '"+entry.getValue().name+"'");
			}
		}
		
		//Add config blocks for average of frames before/after  beam on sections
		if(configW7X.numBackgroundFrames > 0 && beamSel != null){
			ArrayList<ConfigEntry> newEntries = new ArrayList<ConfigEntry>();
			
			for(ConfigEntry beamOnEntry : config.map.values()){ 
				
				if(beamOnEntry.name.contains("_ON")){
					beamOnEntry.markAsGoodData = true;
					double durationSecs = (nanoTime[beamOnEntry.inIdx1] - nanoTime[beamOnEntry.inIdx0]) / 1e9;
										
					if(configW7X.combineBeamOnPhases != EntryCollect.None 
								&& (configW7X.combineMaxBlockDuration <= 0 || durationSecs <= configW7X.combineMaxBlockDuration)) {						
						beamOnEntry.timeSmoothSigma = Double.POSITIVE_INFINITY;
						beamOnEntry.inIdxStep = beamOnEntry.inIdx1 - beamOnEntry.inIdx0;
						beamOnEntry.integrate = (configW7X.combineBeamOnPhases == EntryCollect.Sum);							
					}else {
						beamOnEntry.timeSmoothSigma = -1;
						beamOnEntry.inIdxStep = 1;
						beamOnEntry.integrate = false;						
					}
					int entryIndex = Integer.parseInt(beamOnEntry.name.substring(5, 8));
				
					//is there some background before?
					String preEntryName = "AUTO_" + fmt.format(entryIndex-1) + "_NONE";
					ConfigEntry preEntry = config.map.get(preEntryName);
					if(preEntry != null){
						int newEnd = Integer.max(preEntry.inIdx0, preEntry.inIdx1 - configW7X.numBackgroundFrames);						
						//preEntry.inIdx1 = newEnd;

						String newEntryName = beamOnEntry.name.replaceAll("_ON", configW7X.subtractPre ? "_BG_PRE" : "_BG_PRE_OFF");
						ConfigEntry newEntry = new ConfigEntry();
						newEntry.inIdx0 = newEnd;
						newEntry.inIdx1 = beamOnEntry.inIdx0; //terminate at start of beam phase
						newEntry.timeSmoothSigma  = (configW7X.subtractBackground && configW7X.subtractPre) ? Double.POSITIVE_INFINITY : -1;
						newEntry.inIdxStep = configW7X.subtractBackground ? (newEntry.inIdx1 - newEntry.inIdx0) : 1;
						newEntry.radRemoval = ConfigEntry.RAD_NONE;
						newEntry.spikeThreshold = 0;
						newEntry.markAsGoodData = false;
						newEntry.enable = configW7X.subtractPre;
						newEntry.name = newEntryName;
						newEntries.add(newEntry);
						beamOnEntry.subtractPre = configW7X.subtractPre ? newEntry : null;
						
					}
							
				
					//is there some background after?
					String postEntryName = "AUTO_" + fmt.format(entryIndex+1) + "_NONE";
					ConfigEntry postEntry = config.map.get(postEntryName);
					if(postEntry != null){
						int newStart = Integer.min(postEntry.inIdx1, postEntry.inIdx0 + configW7X.numBackgroundFrames);
						//postEntry.inIdx0 = newStart;

						String newEntryName = beamOnEntry.name.replaceAll("_ON", configW7X.subtractPost ? "_POST" : "_POST_OFF");
						ConfigEntry newEntry = new ConfigEntry();
						newEntry.inIdx0 = beamOnEntry.inIdx1;
						newEntry.inIdx1 = newStart;
						newEntry.timeSmoothSigma  = (configW7X.subtractBackground && configW7X.subtractPost) ? Double.POSITIVE_INFINITY : -1;
						newEntry.inIdxStep = configW7X.subtractBackground ? (newEntry.inIdx1 - newEntry.inIdx0) : 1;
						newEntry.radRemoval = ConfigEntry.RAD_NONE;
						newEntry.spikeThreshold = 0;
						newEntry.markAsGoodData = false;
						newEntry.enable = configW7X.subtractPost;
						newEntry.name = newEntryName;
						newEntries.add(newEntry);
						beamOnEntry.subtractPost = configW7X.subtractPost ? newEntry : null;
						
					}
				}else{ //not beam on
					beamOnEntry.markAsGoodData = false;
				}
			}
			
			for(ConfigEntry newEntry : newEntries){
				config.map.put(newEntry.name, newEntry);
			}
		}
		

		//make sure everything has a pre and a post, even if its a post from later
		//or a pre from earlier
		for(ConfigEntry beamOnEntry : config.map.values()){ 
		
			if(beamOnEntry.name.endsWith("_ON")){
				if(!configW7X.subtractBackground){
					beamOnEntry.subtractPre = null;
					beamOnEntry.subtractPost = null;
					continue;
				}/*else if(!subtractPre){
					beamOnEntry.subtractPre = null;
				}else if(!subtractPost){
					beamOnEntry.subtractPost = null;
				}*/
				
				
				if(beamOnEntry.subtractPre == null){					
					for(ConfigEntry entry : config.map.values()){
						if(entry.inIdx1 <= beamOnEntry.inIdx0 && //is before
							(beamOnEntry.subtractPre == null || entry.inIdx1 > beamOnEntry.subtractPre.inIdx1) && //is later than current best
							(entry.name.endsWith("_PRE") || entry.name.endsWith("_POST"))){ //is a background frame
														
							beamOnEntry.subtractPre = entry;
						}
					}
				}
				
				if(beamOnEntry.subtractPost == null){					
					for(ConfigEntry entry : config.map.values()){
						if(entry.inIdx0 >= beamOnEntry.inIdx1 && //is after
							(beamOnEntry.subtractPost == null || entry.inIdx0 < beamOnEntry.subtractPost.inIdx0) && //is earlier than current best
							(entry.name.endsWith("_PRE") || entry.name.endsWith("_POST"))){ //is a background frame
														
							beamOnEntry.subtractPost= entry;
						}
					}
				}//*/
				System.out.println("Entry '"+beamOnEntry.name+"'. Pre = " + 
								(beamOnEntry.subtractPre != null ? beamOnEntry.subtractPre.name : "none") 
								+ "Post = " + 
								(beamOnEntry.subtractPost != null ? beamOnEntry.subtractPost.name : "none") 
								+ ".");
			}else{
				beamOnEntry.subtractPre = null;
				beamOnEntry.subtractPost = null;
			}
		}
	
		
		updateAllControllers();
		
		
		boolean anyEnabled = false;
		for(ConfigEntry entry : config.map.values())			
			anyEnabled |= entry.enable;
		
		return anyEnabled;
	}
	
	private double[][][] getBeamInfo() {
		long nanoTime[] = W7XUtil.getBestNanotime(connectedSource).clone();
		//need to apply any time correction so the beam energies line up properly
		if(config.timebaseOffset != 0){
			for(int i=0; i < nanoTime.length; i++){
				nanoTime[i]	+= config.timebaseOffset * 1e9;
			}
		}
		double[] expTime = W7XUtil.getFrameExposureTimes(connectedSource, nanoTime);
		double beamInfo[][][] = W7XUtil.getBeamInfo(connectedSource, nanoTime, true);
		return beamInfo;
	}

	@Override
	public String toShortString() {
		String hhc = Integer.toHexString(hashCode());
		return "SeriesProcW7X[" + hhc.substring(hhc.length()-2, hhc.length()) + "]"; 
	}

	public void setNumBackgroundFrames(int selection) { configW7X.numBackgroundFrames = selection; updateAllControllers(); }
	public int getNumBackgroundFrames() { return configW7X.numBackgroundFrames;	}

	public void setAutoConfigFromNBI(int[] autoBeamConfig) { configW7X.autoBeamConfig = autoBeamConfig; updateAllControllers(); }
	public int[] getAutoConfigFromNBI() { return configW7X.autoBeamConfig; }
	
	@Override
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		//JsonObject jobj = JsonParser.parseString(jsonString).getAsJsonObject();
		JsonObject jobj = new JsonStreamParser(jsonString).next().getAsJsonObject();
		
		Gson gson = new Gson();
		
		configW7X = gson.fromJson(jsonString, SeriesProcessorW7XConfig.class);
		config = configW7X;
					
		updateAllControllers();
	}
	
	@Override
	public void saveConfigJSON(String fileName) {		
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.setPrettyPrinting()
				.create(); 
		

		JsonElement jobj = gson.toJsonTree(configW7X);
		
		OneLiners.textToFile(fileName, jobj.toString());
	}

	
	@Override
	protected void processMetaData() {
		super.processMetaData();
		//some specific things...
		
		
		// --- Integrate the NBI beam on times over summed frames ---
		//beam on time should be the sum over the included frames, not the average
		MetaDataMap map = this.getCompleteSeriesMetaDataMap();
		
		//list of input indices that make up this frame
		int idxs[][] = (int[][]) Mat.fixWeirdContainers(map.get("/SeriesProc/inputImagesUsed"), true);
		if(idxs == null)
			return; //this often isnt here for several attempts but eventually is
		

		//assuming no upstream SeriesProc(!), work out the start end of each output frame		
		int imgs[][] = (int[][])this.getSeriesMetaData("/SeriesProc/inputImagesUsed");
		if(imgs != null){
			try {
				long[] nanoTime = W7XUtil.getBestNanotime(connectedSource).clone();
				if(config.timebaseOffset != 0){
					for(int i=0; i < nanoTime.length; i++){
						nanoTime[i]	+= config.timebaseOffset * 1e9;
					}
				}
				
				long[][] upstreamStartStopNanos = W7XUtil.getStartStopNanotime(nanoTime);
				long[] startNanos = new long[imgs.length];
				long[] stopNanos = new long[imgs.length];
				for(int i=0; i < imgs.length; i++){
					startNanos[i] = upstreamStartStopNanos[0][imgs[i][0]];
					stopNanos[i] = upstreamStartStopNanos[1][imgs[i][imgs[i].length-1]];
				}
				
				this.setSeriesMetaData("/SeriesProc/startNanos", startNanos, true);
				this.setSeriesMetaData("/SeriesProc/stopNanos", stopNanos, true);
				
				try {
					//not sure exactly why this was done here, but we can at least ignore failures			
					W7XUtil.getBeamInfo(this, startNanos, stopNanos, true);
				}catch(RuntimeException err) { }
				
			}catch(RuntimeException err) {
				logr.log(Level.WARNING, "Exception while trying to produce /SeriesProc/startNanos", err);
			}
		}
	}
	
	private String findOffsetSyncObj = new String("findOffsetSyncObj");
	public void findOffset() {
		abortCalc = false;
		ImageProcUtil.ensureFinalUpdate(findOffsetSyncObj, () -> doFindOffset());
	}

	public void doFindOffset() {
		boolean configBeamVisible[] = new boolean[] { false, false, false, false, false, false, true, true };
		boolean configAddNotes = false;		
		int configBeamTimebaseCompact = 5;
		
		try {
			
			AutocorrelationSearchConfig oCfg = configW7X.timebaseOffsetSearch;
			
			double offsets[] = Mat.linspace(oCfg.minOffset, oCfg.maxOffset, oCfg.deltaOffset);
			
			int nOffsets = offsets.length;
			
			long nanoTime[] = W7XUtil.getBestNanotime(connectedSource).clone();		
			//double[] expTime = W7XUtil.getFrameExposureTimes(connectedSource, nanoTime);
			
			//we need the raw beam voltage signals (not the already per-frame processed)
			//find root source
			ImgSource rootSrc = connectedSource;
			while(rootSrc instanceof ImgSink)
				rootSrc = ((ImgSink)rootSrc).getConnectedSource();
			
			long beamNanos[] = null;
			double beamVSum[] = null;
			
			status = "Find offset: Collecting beam information...";
			updateAllControllers();
			
			for(int iB=0; iB < 8; iB++) {
				if(!configBeamVisible[iB])
					continue;
				
				Object[] ret;
				try {
					ret = W7XUtil.getBeamIVSignals(rootSrc, iB);
				}catch(RuntimeException err) {
					if(err.getMessage().contains("Unsupported"))
						continue; //source don't exist yet
					
					logr.warning("Unable to read beam signals for Q" + (iB+1) + " due to: " + err.getMessage());
					continue;
				}
				
				if(beamNanos != null && beamNanos.length != ((long[])ret[0]).length)
					throw new UnsupportedOperationException("Active beams have different time bases. Can't deal with this yet");
				
				beamNanos = (long[])ret[0];
				if(beamVSum == null) {
					beamVSum = (double[])ret[1];
				}else {
					for(int i=0; i < beamVSum.length; i++){
						beamVSum[i] += ((double[])ret[1])[i];
					}
				}
				
				if(abortCalc)
					throw new RuntimeException("Aborted");
			}
			if(beamNanos == null)
				throw new RuntimeException("No active beams had a valid beam voltage signal");
			
			//optional reducting of NBI voltage timebase
			if(configBeamTimebaseCompact > 1) {
				int n = beamNanos.length / configBeamTimebaseCompact;
				long beamNanos1[] = new long[n];
				double beamVSum1[] = new double[n];
				for(int i=0; i < n; i++) {
					for(int j=0; j < configBeamTimebaseCompact; j++) {
						int ii = i * configBeamTimebaseCompact + j;
						if(ii >= beamNanos.length)
							break;
						beamNanos1[i] += beamNanos[ii];
						beamVSum1[i] += beamVSum[ii]; 
					}
					beamNanos1[i] /= configBeamTimebaseCompact;
					beamVSum1[i] /= configBeamTimebaseCompact;
				}
				beamNanos = beamNanos1;
				beamVSum = beamVSum1;
			}
			
			for(int i=0; i < beamVSum.length; i++)
				if(beamVSum[i] < 0)
					beamVSum[i] = 0;
			
			status = "Find offset: Averaging image area";
			updateAllControllers();
			
			//sum image area for each frame
			int nImages = connectedSource.getNumImages();
			double areaSum[] = new double[nImages];
			double tImg[] = new double[nImages];  //time in seconds from nanoTime[0]
			for(int iI=0; iI < nImages; iI++) {
				tImg[iI] = (nanoTime[iI] - nanoTime[0]) / 1e9;
				
				Img img = connectedSource.getImage(iI);
				try {
					ReadLock readLock = img.readLock();
					readLock.lockInterruptibly();
					try {					
						//sum image area
						for(int iY = oCfg.y0; iY < oCfg.y1; iY+=oCfg.dy) {
							for(int iX = oCfg.x0; iX < oCfg.x1; iX+=oCfg.dx) {
								areaSum[iI] += img.getPixelValue(readLock, iX, iY);					
							}	
							if(abortCalc)
								throw new RuntimeException("Aborted");
						}	
					}finally {
						readLock.unlock();
					}
					
				}catch(InterruptedException err){ }
				
				status = "Find offset: Averaging image area (frame " + iI + " / " + nImages + ") ...";
				updateAllControllers();
				
			}
					
			double maxAuto = Double.NEGATIVE_INFINITY;
			int iMaxAuto = -1;
			
			AsciiMatrixFile.mustWrite("/tmp/i", new double[][] { tImg, areaSum }, true);
			double tBeam0[] = new double[beamNanos.length];
			for(int i=0; i < beamNanos.length; i++)
				tBeam0[i] = (beamNanos[i] - nanoTime[0]) / 1e9;
			AsciiMatrixFile.mustWrite("/tmp/b", new double[][] { tBeam0, beamVSum }, true);
			//*/
			
			//produce autocorrection for each offset
			double autocorrel[] = new double[nOffsets];
			double areaSumAvg = 0, beamSumAvg = 0;
			for(int iO=0; iO < nOffsets; iO++) {
				status = String.format("Find offset: Scanning autocorrelation (offset %.5f ms (%d / %d) ...", offsets[iO], iO, nOffsets);
				updateAllControllers();
				
				double beamSum[] = new double[nImages];
				for(int i=0; i < nImages; i++) {
					//find the window of beam voltage that lines up with this frame
					double t0,t1;
					if(i == 0) {
						t0 = tImg[0] - (tImg[1] - tImg[0]) / 2;
						t1 = (tImg[0] + tImg[1])/2;
					}else if(i == (nImages-1)) {
						t0 = (tImg[i-1] + tImg[i])/2;
						t1 = tImg[i] + (tImg[i] - tImg[i-1])/2;
					}else {
						t0 = (tImg[i-1] + tImg[i])/2;
						t1 = (tImg[i] + tImg[i+1])/2;					
					}
					
					for(int iTB=0; iTB <beamNanos.length; iTB++) {
						double tBeam = (beamNanos[iTB] - nanoTime[0]) / 1e9 - offsets[iO];
						
						if(tBeam >= t0 && tBeam < t1)
							beamSum[i] += beamVSum[iTB];
					}
					
					areaSumAvg += areaSum[i];
					beamSumAvg += beamSum[i];
					
					if(abortCalc)
						throw new RuntimeException("Aborted");
				}
				areaSumAvg /= nImages;
				beamSumAvg /= nImages;				
				
				for(int i=0; i < nImages; i++) {
					autocorrel[iO] += (areaSum[i] - areaSumAvg) * (beamSum[i] - beamSumAvg);
				}

				
				if(autocorrel[iO] > maxAuto) {
					maxAuto = autocorrel[iO];
					iMaxAuto = iO;
				}					
				System.out.println(iO + " / " + nOffsets);
			}
			
			AsciiMatrixFile.mustWrite("/tmp/o", new double[][] { offsets, autocorrel }, true);
			
			if(iMaxAuto > 0 && iMaxAuto < (nOffsets-1)) {
				configW7X.timebaseOffset = offsets[iMaxAuto];
				
				if(configAddNotes) {
					if(rootSrc instanceof W7XArchiveDBSource) {
						W7XArchiveDBSource w7xLoad = (W7XArchiveDBSource)rootSrc;
						long t1 = Long.MIN_VALUE;
						for(int i=0; i < nanoTime.length; i++)
							if(nanoTime[i] > t1)
								t1 = nanoTime[i];
						
						status = "Find offset: Loading notes (to add offset) ...";
						updateAllControllers();
						String notes = w7xLoad.loadNotes(new ArchiveDBDesc(w7xLoad.getPath(), nanoTime[0], t1));
						if(notes == null)
							notes = "";
						
						if(notes.contains("SeriesProc/timebaseOffset=")) {
							notes.replaceAll("SeriesProc/timebaseOffset=[-0-9.]*", "SeriesProc/timebaseOffset=" + configW7X.timebaseOffset);
						} else {
							notes += "\n" + "SeriesProc/timebaseOffset=" + configW7X.timebaseOffset;
						}
						
						w7xLoad.saveNotes(w7xLoad.getPath(), nanoTime[0], nanoTime[nanoTime.length-1], notes);
					}					
				}
				
				status = "Find offset: Autocorrelation complete, found offset at " + String.format("%.5f", configW7X.timebaseOffset);
				
				
			}else {
				configW7X.timebaseOffset = Double.NaN;
				status = "Find offset: Max offset was index " + iMaxAuto + ", not setting offset";			
			}
			connectedSource.setSeriesMetaData("SeriesProc/findOffset/offsets", offsets, false);
			connectedSource.setSeriesMetaData("SeriesProc/findOffset/autocorrelation", autocorrel, false);
			connectedSource.setSeriesMetaData("SeriesProc/findOffset/bestOffset", offsets[iMaxAuto], false);
			connectedSource.setSeriesMetaData("SeriesProc/findOffset/accuracy", offsets[1] - offsets[0], false);
			
			
			updateAllControllers();
			
		}catch(RuntimeException err) {
			logr.log(Level.WARNING, "Error looking for timebase offset with autocorrelation. Leaving timebaseOffset as it was.", err);
			status = "Find offset: ERROR: " + err.getMessage();			
			updateAllControllers();			
		}
	}

}
