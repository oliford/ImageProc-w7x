package imageProc.proc.seriesAvg;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;

import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.database.gmds.GMDSPipe;
import imageProc.proc.seriesAvg.ConfigEntry;
import imageProc.proc.seriesAvg.SeriesSWTController;
import imageProc.proc.seriesAvg.SeriesProcessorConfig.SmearCorrectionMode;
import imageProc.proc.seriesAvg.SeriesProcessorW7XConfig.AutocorrelationSearchConfig;
import imageProc.proc.seriesAvg.SeriesProcessorW7XConfig.EntryCollect;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** The table directly relfects the config data in the processor's map
* Edits are made into that cfg immediately.
*  
* @author oliford
*/
public class SeriesSWTControllerW7X extends SeriesSWTController implements ImagePipeControllerROISettable {
	private final static int MAX_BEAMS = 8;
	
	private Group swtBeamConfigGroup;
	private Button configIgnoreRadios[];
	private Button configAcceptRadios[];
	private Button configRequireRadios[];
	private Button configRejectRadios[];
	private Button configSingleRadios[];
	private Button configMultiRadios[];
	
	private Button configNowButton;
	private Button configOnUpdateCheckbox;
	private Button backgroundSubtractCheckbox;
	private Button backgroundSubtractPreCheckbox;
	private Button backgroundSubtractPostCheckbox;
	private Combo combineOnPhasesCombo;
	private Spinner combineMaxTimeSpinner;
	
	private Spinner backgroundFramesSpinner;
	private Spinner extendBeamWindowsSpinner;
			
	private Button findOffsetSetAreaCheckbox;
	private Button findOffsetButton;
	private Text findOffsetWindowText;
	
	public SeriesSWTControllerW7X(Composite parent, int style, SeriesProcessorW7X proc, boolean isSinkController) {
		super(parent, style, proc, isSinkController);
	}
	
	@Override
	protected void addAutoConfigControls(Composite parent, int style) {	
		
		swtBeamConfigGroup = new Group(parent, SWT.BORDER);
		swtBeamConfigGroup.setText("Configure from NBI:");
		swtBeamConfigGroup.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 0));
		swtBeamConfigGroup.setLayout(new GridLayout(2, false));
		
		configOnUpdateCheckbox = new Button(swtBeamConfigGroup, SWT.CHECK);
		configOnUpdateCheckbox.setText("On Update");
		configOnUpdateCheckbox.setToolTipText("Configure according to beam signals on update");
		configOnUpdateCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 0));
		configOnUpdateCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } }); 
		
		configNowButton = new Button(swtBeamConfigGroup, SWT.PUSH);
		configNowButton.setText("Config Now");
		configNowButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 0));
		configNowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { configNowButtonEvent(event); } }); 
				
		configIgnoreRadios = new Button[8];
		configAcceptRadios = new Button[8];
		configRequireRadios = new Button[8];
		configRejectRadios = new Button[8];
		configSingleRadios = new Button[8];
		configMultiRadios = new Button[8];
		
		for(int i=2; i < MAX_BEAMS; i++){
			if(i==4 || i == 5)
				continue;
			
			Label lQ = new Label(swtBeamConfigGroup, SWT.NONE);
			lQ.setText("Q"+(i+1)+":");
			
			Composite beamSelComp = new Composite(swtBeamConfigGroup, SWT.NONE);
			beamSelComp.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 0));
			beamSelComp.setLayout(new GridLayout(7, false));
						
			configIgnoreRadios[i] = new Button(beamSelComp, SWT.RADIO);			
			configIgnoreRadios[i].setText("Ignore");
			configIgnoreRadios[i].setToolTipText("Don't take into account changes of the beam state of this beam. Use e.g. if the beam is not seen by this system.");
			configIgnoreRadios[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		
			configAcceptRadios[i] = new Button(beamSelComp, SWT.RADIO);
			configAcceptRadios[i].setText("Accept");
			configAcceptRadios[i].setToolTipText("Include time points where the beam is either on or off, but split the configuration when its state changes.");
			configAcceptRadios[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		
			configRequireRadios[i] = new Button(beamSelComp, SWT.RADIO);
			configRequireRadios[i].setText("Require");
			configRequireRadios[i].setToolTipText("Do not include frames where this beam is off.");
			configRequireRadios[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		
			configRejectRadios[i] = new Button(beamSelComp, SWT.RADIO);
			configRejectRadios[i].setText("Reject");
			configRejectRadios[i].setToolTipText("Do not include frames where this beam is on");
			configRejectRadios[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
			
			configSingleRadios[i] = new Button(beamSelComp, SWT.RADIO);
			configSingleRadios[i].setText("Single");
			configSingleRadios[i].setToolTipText("Include frames where exactly one of these beams is on");
			configSingleRadios[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		
			configMultiRadios[i] = new Button(beamSelComp, SWT.RADIO);
			configMultiRadios[i].setText("Multi");
			configMultiRadios[i].setToolTipText("Include frame where at least one of these beams is on");
			configMultiRadios[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
			configMultiRadios[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 0));
		
		}
		
		Label lNB = new Label(parent, SWT.NONE); lNB.setText("Background frames:");
		backgroundFramesSpinner = new Spinner(parent, SWT.NONE);
		backgroundFramesSpinner.setValues(3, 0, Integer.MAX_VALUE, 0, 1, 5);
		backgroundFramesSpinner.setToolTipText("Number of frames before/after each ON phase to average.");
		backgroundFramesSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 0));
		backgroundFramesSpinner.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { w7xSettingsEvent(event); } }); 

		backgroundSubtractCheckbox = new Button(parent, SWT.CHECK);
		backgroundSubtractCheckbox.setText("Subtract background");
		backgroundSubtractCheckbox.setToolTipText("Subtract interpolation of background pre/post from each ON phase");
		backgroundSubtractCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 0));
		backgroundSubtractCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		backgroundSubtractPreCheckbox = new Button(parent, SWT.CHECK);
		backgroundSubtractPreCheckbox.setText("(before)");
		backgroundSubtractPreCheckbox.setToolTipText("Include data before beam switches on in determination of background");
		backgroundSubtractPreCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 0));
		backgroundSubtractPreCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		backgroundSubtractPostCheckbox = new Button(parent, SWT.CHECK);
		backgroundSubtractPostCheckbox.setText("(after)");
		backgroundSubtractPostCheckbox.setToolTipText("Include data after beam switches off in determination of background");
		backgroundSubtractPostCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 0));
		backgroundSubtractPostCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
	
		Label lCO = new Label(parent, SWT.NONE); lCO.setText("Sum/avg on phases:");
		combineOnPhasesCombo = new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);
		combineOnPhasesCombo.setItems(Mat.toStringArray(EntryCollect.values()));
		combineOnPhasesCombo.setToolTipText("Average/Sum all of each ON phase into a single frame.");
		combineOnPhasesCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 0));
		combineOnPhasesCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lML = new Label(parent, SWT.NONE); lML.setText("Max length [ms]:");
		combineMaxTimeSpinner = new Spinner(parent, SWT.NONE);
		combineMaxTimeSpinner.setValues(1, 0, Integer.MAX_VALUE, 0, 1, 5);
		combineMaxTimeSpinner.setToolTipText("Maximum length of a configuration block to sum/average. Longer than this gets treated frame by frame");
		combineMaxTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 0));
		combineMaxTimeSpinner.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { w7xSettingsEvent(event); } }); 
		
		/** Only average/sum entries with less than this many frames */
		//public int combineMaxFrames = 5;
		
		Label lEW = new Label(parent, SWT.NONE); lEW.setText("Extend beam windows:");
		extendBeamWindowsSpinner = new Spinner(parent, SWT.NONE);
		extendBeamWindowsSpinner.setValues(1, 0, Integer.MAX_VALUE, 0, 1, 5);
		extendBeamWindowsSpinner.setToolTipText("Number of frames to extend beam-on windows at start/end. Good for averaging blips.");
		extendBeamWindowsSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 5, 0));
		extendBeamWindowsSpinner.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { w7xSettingsEvent(event); } }); 
		
		Label lFO = new Label(parent, SWT.NONE); lFO.setText("Autocorrelation window: ");
		findOffsetWindowText = new Text(swtGroup, SWT.NONE);	
		findOffsetWindowText.setText("");
		findOffsetWindowText.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 0));
		findOffsetWindowText.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		findOffsetSetAreaCheckbox = new Button(parent, SWT.CHECK);	
		findOffsetSetAreaCheckbox.setText("Set window");
		findOffsetSetAreaCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 0));
		findOffsetSetAreaCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		findOffsetButton = new Button(parent, SWT.NONE);	
		findOffsetButton.setText("Find");
		findOffsetButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 0));
		findOffsetButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { findOffsetButtonEvent(event); } });

	}

	protected void w7xSettingsEvent(Event event) {
		SeriesProcessorW7XConfig configW7X = (SeriesProcessorW7XConfig)proc.getConfig(); 
		configW7X.numBackgroundFrames = backgroundFramesSpinner.getSelection();
		configW7X.extendBeamOnWindow = extendBeamWindowsSpinner.getSelection();
		configW7X.combineMaxBlockDuration = combineMaxTimeSpinner.getSelection() / 1000.0;
	}
	
	private int[] getBeamConfig(){
		int config[] = new int[MAX_BEAMS];
		for(int i=0; i < MAX_BEAMS; i++){
			config[i] = SeriesProcessorW7XConfig.BEAMSEL_CANTSEE;
			if(configIgnoreRadios[i] == null)
				continue;
			
			if(configIgnoreRadios[i].getSelection())
				config[i] = SeriesProcessorW7XConfig.BEAMSEL_CANTSEE;
			else if(configAcceptRadios[i].getSelection())
				config[i] = SeriesProcessorW7XConfig.BEAMSEL_DONTCARE;
			else if(configRequireRadios[i].getSelection())
				config[i] = SeriesProcessorW7XConfig.BEAMSEL_REQUIRE;
			else if(configRejectRadios[i].getSelection())
				config[i] = SeriesProcessorW7XConfig.BEAMSEL_DONTWANT;
			else if(configSingleRadios[i].getSelection())
				config[i] = SeriesProcessorW7XConfig.BEAMSEL_ONLY_ONE;
			else if(configMultiRadios[i].getSelection())
				config[i] = SeriesProcessorW7XConfig.BEAMSEL_AT_LEAST_ONE;	
		}
		
		return config;	
	}
	
	private void setBeamConfig(int config[]){
		for(int i=0; i < MAX_BEAMS; i++){
			if(configIgnoreRadios[i] == null)
				continue;

			configIgnoreRadios[i].setSelection(config[i] == SeriesProcessorW7XConfig.BEAMSEL_CANTSEE);
			configAcceptRadios[i].setSelection(config[i] == SeriesProcessorW7XConfig.BEAMSEL_DONTCARE);
			configRequireRadios[i].setSelection(config[i] == SeriesProcessorW7XConfig.BEAMSEL_REQUIRE);
			configRejectRadios[i].setSelection(config[i] == SeriesProcessorW7XConfig.BEAMSEL_DONTWANT);
			configSingleRadios[i].setSelection(config[i] == SeriesProcessorW7XConfig.BEAMSEL_ONLY_ONE);
			configMultiRadios[i].setSelection(config[i] == SeriesProcessorW7XConfig.BEAMSEL_AT_LEAST_ONE);
		}
	}

	private void settingsChangedEvent(Event event){
		SeriesProcessorW7XConfig configW7X = (SeriesProcessorW7XConfig)proc.getConfig();
		configW7X.subtractBackground = backgroundSubtractCheckbox.getSelection();
		configW7X.subtractPre = backgroundSubtractPreCheckbox.getSelection();
		configW7X.subtractPost = backgroundSubtractPostCheckbox.getSelection();
		configW7X.combineBeamOnPhases = EntryCollect.valueOf(combineOnPhasesCombo.getText());
		
		if(configOnUpdateCheckbox.getSelection()){			
			((SeriesProcessorW7X)proc).setAutoConfigFromNBI(getBeamConfig());
		}else{
			((SeriesProcessorW7X)proc).setAutoConfigFromNBI(null);
		}
		
		try {
			Gson gson = new Gson();
			configW7X.timebaseOffsetSearch = gson.fromJson(findOffsetWindowText.getText(), AutocorrelationSearchConfig.class);
			
		}catch(RuntimeException err){
			Logger.getLogger(this.getClass().getName()).warning("Could not pass JSON for timebaseOffsetSearch");
			configW7X.timebaseOffsetSearch = new AutocorrelationSearchConfig();
		}
		
	}

	private void configNowButtonEvent(Event event){
		((SeriesProcessorW7X)proc).configFromNBI(getBeamConfig());
	}
	
	@Override
	protected void doUpdate() {
		super.doUpdate();		
		
		SeriesProcessorW7XConfig configW7X = (SeriesProcessorW7XConfig)proc.getConfig();
		backgroundFramesSpinner.setSelection(configW7X.numBackgroundFrames);
		backgroundSubtractCheckbox.setSelection(configW7X.subtractBackground);
		backgroundSubtractPreCheckbox.setSelection(configW7X.subtractPre);
		backgroundSubtractPostCheckbox.setSelection(configW7X.subtractPost);
		combineOnPhasesCombo.setText(configW7X.combineBeamOnPhases.toString());
		extendBeamWindowsSpinner.setSelection(configW7X.extendBeamOnWindow);
		combineMaxTimeSpinner.setSelection((int)(configW7X.combineMaxBlockDuration * 1000));
		
		int beamConfig[] = ((SeriesProcessorW7X)proc).getAutoConfigFromNBI();
		configOnUpdateCheckbox.setSelection(beamConfig != null);
		if(beamConfig != null){
			setBeamConfig(beamConfig);
		}
		
		if(!findOffsetWindowText.isFocusControl()) {
			try {
				Gson gson = new Gson();
				findOffsetWindowText.setText(gson.toJson(configW7X.timebaseOffsetSearch));
				
			}catch(RuntimeException err){
				Logger.getLogger(this.getClass().getName()).warning("Could not make JSON of timebaseOffsetSearch");
				findOffsetWindowText.setText("ERROR: " + err.getMessage());
			}
		}
	}

	protected void findOffsetButtonEvent(Event event) {
		((SeriesProcessorW7X)proc).findOffset();
	}
	
	@Override
	public SeriesProcessorW7X getPipe() { return (SeriesProcessorW7X)proc;	}


	@Override
	public void movingPos(int x, int y) { 	}

	@Override
	public void fixedPos(int x, int y) { }

	@Override
	public void setRect(int x0, int y0, int width, int height) {
		SeriesProcessorW7XConfig configW7X = (SeriesProcessorW7XConfig)proc.getConfig();
		
		if(findOffsetSetAreaCheckbox.getSelection()) {
			if(configW7X.timebaseOffsetSearch == null)
				configW7X.timebaseOffsetSearch = new AutocorrelationSearchConfig();
			
			AutocorrelationSearchConfig oCfg = configW7X.timebaseOffsetSearch;
			
			oCfg.x0 = x0;
			oCfg.y0 = y0;
			oCfg.x1 = x0 + width;
			oCfg.y1 = y0 + height;
			
			proc.updateAllControllers();
		}
	}
}
