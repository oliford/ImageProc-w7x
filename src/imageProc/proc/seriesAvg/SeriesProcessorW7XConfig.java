package imageProc.proc.seriesAvg;

import java.util.HashMap;

public class SeriesProcessorW7XConfig extends SeriesProcessorConfig {
	//params to configFromNBI(),  also used in aug-apps/MultiCalSupport
	public static final int BEAMSEL_DONTWANT = -1;
	public static final int BEAMSEL_CANTSEE = 0; // can't see the beam, so don't care if it changes
	public static final int BEAMSEL_DONTCARE = 1; //can see it, so break the config block, but don't care if it's on
	public static final int BEAMSEL_REQUIRE = 2; //must be on
	public static final int BEAMSEL_AT_LEAST_ONE = 3; //at least one marked with this can be on
	public static final int BEAMSEL_ONLY_ONE = 4; //only on marked with this can be on
			
	/** Subtract interpolation of nearby beam-off phases from beam-on phase */	
	public boolean subtractBackground = false;	
	
	/** Include data before beam in background */ 
	public boolean subtractPre = true;
	
	/** Include data after beam in background 
	 * (if this and subtractPre are false, 0 is subtracted effectively making subtractBackground=false), 
	 * but with all the metadata as if it is eanbled) */ 
	public boolean subtractPost = true;
	
	public int numBackgroundFrames = 3;
	
	/** If/how to combine frames in entries. */ 
	public enum EntryCollect { None, Sum, Average	}
	
	/** Average together all of beam-on phases (e.g. for single measurement blips) */
	public EntryCollect combineBeamOnPhases = EntryCollect.None;
	
	/** Only average/sum entries with less than this time length */
	public double combineMaxBlockDuration = Double.POSITIVE_INFINITY;
	
	/** Artificially extend 'beam-on' windows by this many frames in each direction.
	 * Useful for averaging over blips when timing isn't quite aligned */
	public int extendBeamOnWindow = 1;
		
	public int[] autoBeamConfig =  //op1.2b default (Q7/8)
			new int[]{	SeriesProcessorW7XConfig.BEAMSEL_CANTSEE, 
					SeriesProcessorW7XConfig.BEAMSEL_CANTSEE, 
					SeriesProcessorW7XConfig.BEAMSEL_CANTSEE, 
					SeriesProcessorW7XConfig.BEAMSEL_CANTSEE, 
					SeriesProcessorW7XConfig.BEAMSEL_CANTSEE,
					SeriesProcessorW7XConfig.BEAMSEL_CANTSEE,
					SeriesProcessorW7XConfig.BEAMSEL_AT_LEAST_ONE,
					SeriesProcessorW7XConfig.BEAMSEL_AT_LEAST_ONE };
	

	/* Region of image space to use for autocorrelation search of best timebase offset relative to NBI voltage signal */
	public static class AutocorrelationSearchConfig {
		public boolean enable = false;
		
		/** Window on image to sum for autocorrelation */
		public int x0=0, y0=0, x1=100, y1=100, dx=1, dy=1;
		
		/** Offset space to search autocorrelation */
		public double minOffset=-0.1, maxOffset=0.1, deltaOffset=0.005;
		
	};
	
	public AutocorrelationSearchConfig timebaseOffsetSearch = new AutocorrelationSearchConfig();

	public HashMap<String, Object> toMap() {
		HashMap<String, Object> map = super.toMap();
		
		map.put("subtractBackground", subtractBackground);
		map.put("subtractPre", subtractPre);
		map.put("subtractPost", subtractPost);
		map.put("numBackgroundFrames", numBackgroundFrames);
		map.put("combineMaxBlockDuration", combineMaxBlockDuration);
		map.put("extendBeamOnWindow", extendBeamOnWindow);
		map.put("autoBeamConfig", autoBeamConfig);
		
		map.put("timebaseOffsetSearch/enable", timebaseOffsetSearch.enable);
		map.put("timebaseOffsetSearch/x0", timebaseOffsetSearch.x0);
		map.put("timebaseOffsetSearch/y0", timebaseOffsetSearch.y0);
		map.put("timebaseOffsetSearch/x1", timebaseOffsetSearch.x1);
		map.put("timebaseOffsetSearch/y1", timebaseOffsetSearch.y1);
		map.put("timebaseOffsetSearch/dx", timebaseOffsetSearch.dx);
		map.put("timebaseOffsetSearch/dy", timebaseOffsetSearch.dy);
		map.put("timebaseOffsetSearch/minOffset", timebaseOffsetSearch.minOffset);
		map.put("timebaseOffsetSearch/maxOffset", timebaseOffsetSearch.maxOffset);
		map.put("timebaseOffsetSearch/deltaOffset", timebaseOffsetSearch.deltaOffset);
		
		return map;
	}
	
}
