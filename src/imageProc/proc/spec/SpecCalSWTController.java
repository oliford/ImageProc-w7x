package imageProc.proc.spec;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import andor2JNI.AndorV2ROIs;
import andor2JNI.AndorV2ROIs.AndorV2ROI;
import otherSupport.RandomManager;
import fusionDefs.transform.PhysicalROIGeometry;
import imageProc.core.ImagePipeController;
import imageProc.core.ImagePipeControllerROISettable;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.swt.ImagePanel;
import imageProc.core.swt.ImgProcPipeSWTController;
import imageProc.core.swt.SWTControllerInfoDraw;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.gmds.GMDSPipe;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.graph.shapeFit.FuncFitterSWTControl;
import imageProc.proc.spec.SpecCalConfig.TrackConfig;
import imageProc.proc.spec.SpecCalConfig.TrackConfig.ParamType;
import imageProc.proc.spec.SpecCalProcessor.CalType;
import imageProc.sources.capture.andorV2.AndorV2Config;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class SpecCalSWTController implements ImagePipeController {
	public static final String colNames[] = { "-", "ROI name", "row#", "Light path", "Wavelength", "Intensity", "Instr. Width" };
	
	public static final int COL_HANDLE = 0;
	public static final int COL_NAME = 1;
	public static final int COL_ROW_NUM = 2;
	public static final int COL_LIGHT_PATH = 3;
	public static final int COL_WAVELEN = 4;
	public static final int COL_INTENSITY = 5;
	public static final int COL_INSTR_WIDTH = 6;
	
	public static final DecimalFormat tableValueFormat = new DecimalFormat("#.######");

	private SpecCalProcessor proc;

	protected Group swtGroup;

	private Label statusLabel;	
	private Text swtWavelenOffsetTextbox;
	private Text swtInsFuncWidthOffsetTextbox;
	
	private SashForm swtSashForm;
	private Composite swtTopComp;
	private Composite swtBottomComp;
	
	private Button autoUpdateCheckbox;
	private Button swtUpdateButton;

	private Table table;

	private SWTSettingsControl settingsCtrl;
	
	private Button processBeamData;
	private Button writeCXSFitFileCheckbox;
	
	protected CTabFolder swtTabFoler;	
	private CTabItem swtFromDBTab;
	private CTabItem swtWavelenTab;
	private CTabItem swtIntensityTab;
	private CTabItem swtInstrumentFuncTab;
	
	private FromDBSWTController fromDBConfig;
	private ParameterisedCurveSWTController wavelenConfig;
	private ParameterisedCurveSWTController intensityConfig;
	private ParameterisedCurveSWTController instrumentFuncConfig;
	
	public SpecCalSWTController(Composite parent, int style, SpecCalProcessor proc) {
		this.proc = proc;

		swtGroup = new Group(parent, style);
		swtGroup.setText("SpecCal Control (" + proc.toShortString() + ")");
		swtGroup.setLayout(new FillLayout());
		swtGroup.addListener(SWT.Dispose, new Listener() { @Override public void handleEvent(Event event) { destroy(); } });
		
		swtSashForm = new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		swtSashForm.setLayout(new FillLayout());

		swtTopComp = new Composite(swtSashForm, SWT.BORDER);
		swtTopComp.setLayout(new GridLayout(6, false));

		Label lStat = new Label(swtTopComp, SWT.NONE);
		lStat.setText("Status:");
		statusLabel = new Label(swtTopComp, SWT.NONE);
		statusLabel.setText("init");
		statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		
		Label lWO = new Label(swtTopComp, SWT.NONE); lWO.setText("Wavelength offset:");
		swtWavelenOffsetTextbox = new Text(swtTopComp, SWT.NONE);
		swtWavelenOffsetTextbox.setToolTipText("Applies a constant offset to the wavelength calibration of all rows.");
		swtWavelenOffsetTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		swtWavelenOffsetTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		Label lIO = new Label(swtTopComp, SWT.NONE); lIO.setText("Ins func offset:");
		swtInsFuncWidthOffsetTextbox = new Text(swtTopComp, SWT.NONE);
		swtInsFuncWidthOffsetTextbox.setToolTipText("Applies a constant offset to the instrument function width (sigma) for all rows.");
		swtInsFuncWidthOffsetTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 2, 1));
		swtInsFuncWidthOffsetTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		Label lU = new Label(swtTopComp, SWT.NONE); lU.setText("");
		swtUpdateButton = new Button(swtTopComp, SWT.PUSH);
		swtUpdateButton.setText("Update");
		swtUpdateButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		swtUpdateButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { updateButtonEvent(event); } });

		autoUpdateCheckbox = new Button(swtTopComp, SWT.CHECK);		
		autoUpdateCheckbox.setText("Auto");
		autoUpdateCheckbox.setToolTipText("Trigger refit on all settings and image source changes");
		autoUpdateCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		autoUpdateCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));

		
		processBeamData = new Button(swtTopComp, SWT.CHECK);
		processBeamData.setText("Process Beam Data");
		processBeamData.setToolTipText("(CXRS, requires SeriesProcessor) Loads beam voltage data and generates arrays for the average crossing point of each LOS with the active beam.");
		processBeamData.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		processBeamData.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		writeCXSFitFileCheckbox = new Button(swtTopComp, SWT.CHECK);
		writeCXSFitFileCheckbox.setText("Write CXSFit file");
		writeCXSFitFileCheckbox.setToolTipText("Writes a NetCDF information file used by CXFIT/w7x to load this data.");
		writeCXSFitFileCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 2, 1));
		writeCXSFitFileCheckbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
				
		table = new Table(swtTopComp, SWT.BORDER | SWT.MULTI);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 0));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn cols[] = new TableColumn[colNames.length];
		for (int i = 0; i < colNames.length; i++) {
			cols[i] = new TableColumn(table, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			cols[i].setText(colNames[i]);
		}

		configToGUI();

		for (int i = 0; i < colNames.length; i++)
			cols[i].pack();

		table.addListener(SWT.MouseDoubleClick, new Listener() { 
			@Override
			public void handleEvent(Event event) {
				for (int i = 0; i < colNames.length; i++)
					cols[i].pack();
				proc.updateAllControllers();
			}			
		});

		tableEditor = new TableEditor(table);
		tableEditor.horizontalAlignment = SWT.LEFT;
		tableEditor.grabHorizontal = true;
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		table.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
		
		swtBottomComp = new Composite(swtSashForm, SWT.BORDER);
		swtBottomComp.setLayout(new GridLayout(6, false));

		swtTabFoler = new CTabFolder(swtBottomComp, SWT.BORDER | SWT.TOP);		
		swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1)); //this doesn't work
		
		fromDBConfig = new FromDBSWTController(swtTabFoler, SWT.BORDER, proc, this, CalType.WAVELEN);
		swtFromDBTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtFromDBTab.setControl(fromDBConfig.getSWTGroup());
		swtFromDBTab.setText("Init from DB");  
		swtFromDBTab.setToolTipText("Settings to init track config from database via W7XSpec.");
		

		wavelenConfig = new ParameterisedCurveSWTController(swtTabFoler, SWT.BORDER, proc, this, CalType.WAVELEN);
		swtWavelenTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtWavelenTab.setControl(wavelenConfig.getSWTGroup());
		swtWavelenTab.setText("Wavelength");  
		swtWavelenTab.setToolTipText("Parameterisation of wavelength calibration across track.");

		intensityConfig = new ParameterisedCurveSWTController(swtTabFoler, SWT.BORDER, proc, this, CalType.INTENSITY);
		swtWavelenTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtWavelenTab.setControl(intensityConfig.getSWTGroup());
		swtWavelenTab.setText("Intensity");  
		swtWavelenTab.setToolTipText("Parameterisation of intensity calibration across track.");

		instrumentFuncConfig = new ParameterisedCurveSWTController(swtTabFoler, SWT.BORDER, proc, this, CalType.INSTRUMENT);
		swtWavelenTab = new CTabItem(swtTabFoler, SWT.NONE);
		swtWavelenTab.setControl(instrumentFuncConfig.getSWTGroup());
		swtWavelenTab.setText("Instrument Function");  
		swtWavelenTab.setToolTipText("Parameterisation of instrument function width across track.");
				
		settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtBottomComp, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));

		swtGroup.pack();
		doUpdate();
	}

	protected void settingsChangedEvent(Event event) {
		
		SpecCalConfig cfg = proc.getConfig();
		cfg.wavelnCommonOffset = Algorithms.mustParseDouble(swtWavelenOffsetTextbox.getText());
		cfg.instrumentFunctionWidthOffset = Algorithms.mustParseDouble(swtInsFuncWidthOffsetTextbox.getText());
		
		cfg.procBeamData = processBeamData.getSelection();
        cfg.writeCXSFitFile = writeCXSFitFileCheckbox.getSelection();
		
        proc.setAutoUpdate(autoUpdateCheckbox.getSelection());
		proc.configModified();
	}
	
	protected void updateButtonEvent(Event event) {
		proc.calc();
	}

	@Override
	public final void generalControllerUpdate() {
		if (swtGroup.isDisposed())
			return;
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() { @Override public void run() { doUpdate(); } });
	}

	public final String slowUpdateSyncObject = new String("SlowUpdateSyncObject");

	private TableEditor tableEditor;
	private TableItem tableItemEditing = null;
	private Text tableEditBox = null;	

	protected void doUpdate() {
		SpecCalConfig cfg = proc.getConfig();
		
		autoUpdateCheckbox.setSelection(proc.getAutoUpdate());
		swtWavelenOffsetTextbox.setText(Double.toString(cfg.wavelnCommonOffset));
		swtInsFuncWidthOffsetTextbox.setText(Double.toString(cfg.instrumentFunctionWidthOffset));
		
		processBeamData.setSelection(cfg.procBeamData);
		writeCXSFitFileCheckbox.setSelection(cfg.writeCXSFitFile);
		
		statusLabel.setText(proc.getStatus());
		configToGUI();
		
		fromDBConfig.update();
		wavelenConfig.update();
		intensityConfig.update();
		instrumentFuncConfig.update();
		
	}

	private void configToGUI(){
		SpecCalConfig config = proc.getConfig();
		
		TableItem[] select = table.getSelection();
		String selectedID = (select == null || select.length < 1) ? null : select[0].getText(COL_NAME);
		
		ArrayList<TrackConfig> trackList;
		synchronized (config) { trackList = (ArrayList<TrackConfig>) config.tracks.clone(); } //clone to avoid collisions/locking
		

		Collections.sort(trackList, new Comparator<TrackConfig>() {
			@Override
			public int compare(TrackConfig o1, TrackConfig o2) {
				return o1.name.compareTo(o2.name);
			}			
		});
		
		int selected = table.getSelectionIndex();

		// use existing table entries, so it doesnt scroll around
		for (int i = 0; i < trackList.size(); i++) {
			TrackConfig track = trackList.get(i);
			
			if (track.name.length() <= 0)
				continue;

			TableItem item;
			if (i < table.getItemCount())
				item = table.getItem(i);
			else
				item = new TableItem(table, SWT.NONE);

			item.setText(COL_HANDLE, "o");
			item.setText(COL_NAME, track.name);
			item.setText(COL_ROW_NUM, Integer.toString(track.row));
			item.setText(COL_LIGHT_PATH, track.lightPath == null ? "" : track.lightPath);
			
			ParamType pt;
			pt = track.getParamType(CalType.WAVELEN); item.setText(COL_WAVELEN, pt != null ? pt.toString() : "");
			pt = track.getParamType(CalType.INTENSITY); item.setText(COL_INTENSITY,  pt != null ? pt.toString() : "");
			pt = track.getParamType(CalType.INSTRUMENT); item.setText(COL_INSTR_WIDTH,  pt != null ? pt.toString() : "");
		}

		while (table.getItemCount() > trackList.size()) {
			table.remove(trackList.size());
		}

		// and finally a blank item for creating new point
		TableItem blankItem = new TableItem(table, SWT.NONE);
		blankItem.setText(COL_HANDLE, "o");
		blankItem.setText(COL_NAME, "<new>");
	}
	


	/**
	 * Copied from 'Snippet123' Copyright (c) 2000, 2004 IBM Corporation and
	 * others. [ org/eclipse/swt/snippets/Snippet124.java ]
	 */
	private void tableMouseDownEvent(Event event) {
		
		Rectangle clientArea = table.getClientArea();
		Point pt = new Point(clientArea.x + event.x, event.y);
		int index = table.getTopIndex();
		while (index < table.getItemCount()) {
			boolean visible = false;
			final TableItem item = table.getItem(index);
			for (int i = 0; i < table.getColumnCount(); i++) {
				Rectangle rect = item.getBounds(i);
				if (rect.contains(pt)) {
					final int column = i;
					if (column != COL_NAME && column != COL_LIGHT_PATH) { //all others are not editable
						generalControllerUpdate();
						return;						
					}
					
					if (tableEditBox != null) {
						tableEditBox.dispose(); // oops, one left over
					}
					tableEditBox = new Text(table.getParent(), SWT.NONE);
					tableItemEditing = item;
					Listener textListener = new Listener() {
						public void handleEvent(final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, tableEditBox.getText());
								tableEditBox.dispose();
								tableEditBox = null;
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, tableEditBox.getText());
									// FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									tableEditBox.dispose();
									tableEditBox = null;
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					tableEditBox.addListener(SWT.FocusOut, textListener);
					tableEditBox.addListener(SWT.Traverse, textListener);
					tableEditor.setEditor(tableEditBox, item, i);
					tableEditBox.setText(item.getText(i));
					tableEditBox.selectAll();
					tableEditBox.setFocus();
					// hacks for SWT4.4 (under linux GTK at least)
					// Textbox won't display if it's a child of the table
					// so we make it a child of the table's parent, but now need
					// to adjust the location
					{
						tableEditBox.moveAbove(table);
						final Point p0 = tableEditBox.getLocation();
						Point p1 = table.getLocation();
						p0.x += p1.x - clientArea.x;
						p0.y += p1.y;// + editBox.getSize().y;
						// p0.x = pt.x + p1.x;
						// p0.y = pt.y + p1.y;
						tableEditBox.setLocation(p0);
						tableEditBox.addListener(SWT.Move, new Listener() {
							@Override
							public void handleEvent(Event event) {
								tableEditBox.setLocation(p0); // TableEditor keeps
															// moving it to
															// relative to the
															// table, so move it
															// back
							}
						});
					}
					return;
				}
				if (!visible && rect.intersects(clientArea)) {
					visible = true;
				}
			}
			if (!visible)
				return;
			index++;
		}
		
		proc.updateAllControllers();
	}

	private void tableColumnModified(TableItem item, int column, String text) {

		SpecCalConfig config = proc.getConfig();
		
		synchronized (config) {
			if (item.isDisposed())
				return;

			String trackName = item.getText(1);

			TrackConfig track = (trackName.length() > 0) ? config.getTrackConfig(trackName) : null;
			if(track == null && column != COL_NAME){
				//somethings broken. Queue an update
				generalControllerUpdate();
				return;
			}
			
			switch (column) {
				case COL_NAME: //name
	
					if (track == null && (trackName.length() <= 0 || trackName.equals("<new>"))) { 
						track = new TrackConfig();
						track.name = text;
						config.tracks.add(track);
	
					} else if (text.length() <= 0) { // deleting point
						config.tracks.remove(track);
	
					} else { // just changing name
						track.name = text;
					}
	
					item.setText(column, text);
	
					break;
	
				case COL_LIGHT_PATH: track.lightPath = text; item.setText(COL_LIGHT_PATH, text); break;
				
			}

			TableItem lastItem = table.getItem(table.getItemCount() - 1);
			if (!lastItem.getText(1).equals("<new>")) {
				TableItem blankItem = new TableItem(table, SWT.NONE);
				blankItem.setText(COL_HANDLE, "o");
				blankItem.setText(COL_NAME, "<new>");
			}

		}
	}


	@Override
	public void destroy() {
		swtGroup.dispose();
		proc.controllerDestroyed(this);
	}

	@Override
	public Object getInterfacingObject() { return swtGroup; }
	@Override
	public SpecCalProcessor getPipe() { return proc; }

	public TrackConfig getSelectedTrack() {
		int selIdx = table.getSelectionIndex();
		if(selIdx < 0) return null;
		TableItem item = table.getItem(selIdx);
		if(item == null) return null;
		String trackName = item.getText(COL_NAME);
		SpecCalConfig config = proc.getConfig();
		return config.getTrackConfig(trackName);
	}
}
