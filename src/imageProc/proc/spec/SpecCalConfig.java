package imageProc.proc.spec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import imageProc.proc.spec.SpecCalConfig.TrackConfig.ParamType;
import imageProc.proc.spec.SpecCalProcessor.CalType;
import signals.w7x.ArchiveDB.Database;

/** Configuration for wavelength, intensity and instrument function calibrations
 * of multiple tracks on a (binned) spectrum.
 *  
 * @author oliford
 *
 */
public class SpecCalConfig {

	/** Configuration of tracks and the parameterisation of calibrations across pixel value.
	 * 'x' is the raw pixel value of across the whole row of the camera, not the binned x
	 * so that the x binning and range can change without effecting the calibration configuration.
	 *  
	 */
	public static class TrackConfig {
		
		/** Name of track (ROI row) to which this config should be applied */
		public String name;
		
		/** Row of this track from current image source */
		public int row;
		
		/** Light path lighting this track on the spectrometer, using the full format (no common table):
		 * 
		 * Table/Component[Endpoint]:ChannelName = [Endpoint]Table/Component[Endpoint]:ChannelName = [Endpoint]Table/Component:ChannelName
		 * 
		 * (Endpoints can be omitted when there's only one)
		 *  */
		public String lightPath;
		
		/** Start and end of attached line of sight */
		public double[] losStart;
		public double[] losUVec;
		
		/** Types of parameterisation */
		public static enum ParamType {
			NONE,
			KNOTS_LINEAR,
			KNOTS_CUBIC,
			POLYNOMIAL,
		};
		
		/** Function type and parameters describing wavelength variation as a function of x */
		public ParamType wavelenParamType;
		public double wavelenParams[][];
				
		/** Function type and parameters describing intensity as a function of x */
		public ParamType intensityParamType;
		public double intensityParams[][];
		
		/** Function type and parameters describing width of Gaussian instrument function as a function of x */
		public ParamType instrumentFunctionWidthParamType;
		public double instrumentFunctionWidthParams[][];
		
		public void addToMap(HashMap<String, Object> map, String prefix){
			map.put(prefix + "/name", name);
			map.put(prefix + "/row", row);
			map.put(prefix + "/lightPath", lightPath);
			map.put(prefix + "/wavelenParamType", wavelenParamType.toString());
			map.put(prefix + "/wavelenParams", wavelenParams);
			map.put(prefix + "/intensityParamType", intensityParamType.toString());
			map.put(prefix + "/intensityParams", intensityParams);
			map.put(prefix + "/instrumentFunctionWidthParamType", instrumentFunctionWidthParamType.toString());
			map.put(prefix + "/instrumentFunctionWidthParams", instrumentFunctionWidthParams);
		}

		public void setParamType(CalType calType, ParamType paramType) {
			switch(calType){
				case WAVELEN: wavelenParamType = paramType; break;
				case INTENSITY: intensityParamType = paramType; break;
				case INSTRUMENT: instrumentFunctionWidthParamType = paramType; break;
				default: throw new IllegalArgumentException("Unrecognise param type");
			}
		}

		public void setNumParams(CalType calType, int nParams) {
			double array[][];
			switch(calType){
				case WAVELEN: array = wavelenParams; break;
				case INTENSITY: array = intensityParams; break;
				case INSTRUMENT: array = instrumentFunctionWidthParams; break;
				default: throw new IllegalArgumentException("Unrecognise param type");
			}
			if(array == null){
				array = new double[2][nParams];
				
			}else if(array[0].length != nParams){
				array = new double[][]{
					Arrays.copyOf(array[0], nParams),
					Arrays.copyOf(array[1], nParams),
				};
			}
			switch(calType){
				case WAVELEN: wavelenParams = array; break;
				case INTENSITY: intensityParams = array; break;
				case INSTRUMENT: instrumentFunctionWidthParams = array; break;
			}
		}
		
		public double[][] getParams(CalType calType){
			switch(calType){
				case WAVELEN: return wavelenParams;
				case INTENSITY: return intensityParams;
				case INSTRUMENT: return instrumentFunctionWidthParams;
				default: throw new IllegalArgumentException("Unrecognise param type");
			}
		}
		
		public ParamType getParamType(CalType calType){
			switch(calType){
				case WAVELEN: return wavelenParamType;
				case INTENSITY: return intensityParamType;
				case INSTRUMENT: return instrumentFunctionWidthParamType;
				default: throw new IllegalArgumentException("Unrecognise param type");
			}
		}
	}
	
	public ArrayList<TrackConfig> tracks = new ArrayList<TrackConfig>();
	
	/** Common shift to wavelength of all rows. Use for fine adjustment e.g. by pre/post discharge fit */
	public double wavelnCommonOffset = 0;
	
	/** Common increase of instrument function sigma */ 
	public double instrumentFunctionWidthOffset = 0.0;
	
	/** Write CXSFit input file on update */
	public boolean writeCXSFitFile = false;
	
	/** Get beam power data and calculate beam intersections */  
	public boolean procBeamData = false;
	
	/** Use given distance along LOS for position information
	 * and pretend it's a beam intersection. Useful for passive fits
	 * NaN = off
	 * (only works if procBeamData is false) */
	public double passivePositionAlongLOS = Double.NaN;
	
	/** Info to autofill the track configuration from the database via W7XSpec (LightPaths,SpecCal etc) */
	public static class ConfigFromDatabase {
		/** Get lightpaths info from database, not JSON file */
		public boolean enable = true;

		/** Set database used to query lightPaths database */
		public Database database;
		public boolean databaseAuto = true;
	
		/** Set table name used to query lightPaths database */
		public String table;
		public boolean tableAuto = true;
	
		/** Spectrometer name */
		public String spectrometer;
		public boolean spectrometerAuto = true;
	
		/** Target name */
		public String target;
		public boolean targetAuto = true;
		
		/** Time (defaults to acquisition time)*/
		public long time;
		public boolean timeAuto = true;
		
		/** Set the ROI names to match the LightPaths component name.
		 * e.g. make "ch32" --> "ComponetXYZ:32" */
		boolean forceROINamesToComponent = true;
		
		boolean loadIntensity = true;
	}
	
	public ConfigFromDatabase fromDB = new ConfigFromDatabase();

	public TrackConfig getTrackConfig(String trackID){
		for(TrackConfig tCfg : tracks)
			if(trackID.equals(tCfg.name))
				return tCfg;
		
		return null;
	}
	
	public HashMap<String, Object> toMap() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		int i = -1;
		for(TrackConfig track : tracks){
			int trackNum = (track.row >= 0) ? track.row : i--;
			track.addToMap(map, "track_" + trackNum);
		}
				
		return map;
	}
}
