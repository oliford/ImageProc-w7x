package imageProc.proc.spec;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.mpg.ipp.codac.w7xtime.NanoTime;
import imageProc.core.ImageProcUtil;
import imageProc.proc.spec.SpecCalConfig.TrackConfig;
import imageProc.proc.spec.SpecCalConfig.TrackConfig.ParamType;
import imageProc.proc.spec.SpecCalProcessor.CalType;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import signals.w7x.ArchiveDB.Database;

public class FromDBSWTController {
	private Group swtInitGroup;
	
	private SpecCalProcessor proc;

	private Text swtTime;
	private Button swtTimeAutoCheckbox;
	private Text swtTable;
	private Button swtTableAutoCheckbox;
	private Combo swtDatabase;
	private Button swtDatabaseAutoCheckbox;
	private Text swtSpectrometer;
	private Button swtSpectrometerAutoCheckbox;
	private Text swtTarget;
	private Button swtTargetAutoCheckbox;
	private Button swtInitFromArchiveCheckbox;
	private Button swtLoadIntensityCheckbox;

	private SpecCalSWTController ctrl;
	
	public FromDBSWTController(Composite parent, int style, SpecCalProcessor proc, SpecCalSWTController parentController, CalType calType) {
		this.proc = proc;
		this.ctrl = parentController;
			
		swtInitGroup = new Group(parent, style);
		swtInitGroup.setText("Param");
		swtInitGroup.setLayout(new GridLayout(6, false));
		ImageProcUtil.addRevealWriggler(swtInitGroup);
		
		swtInitFromArchiveCheckbox = new Button(swtInitGroup, SWT.CHECK);
		swtInitFromArchiveCheckbox.setText("Init table from archive");
		swtInitFromArchiveCheckbox.setToolTipText("Clear the table and reinitialise from Archive: Paths and LOS info from LightPaths and calibration from respective streams");
		swtInitFromArchiveCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 6, 1));
		swtInitFromArchiveCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lLt = new Label(swtInitGroup, SWT.NONE); lLt.setText("Time:");
		swtTime = new Text(swtInitGroup, SWT.NONE);
		swtTime.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1));
		swtTime.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		swtTimeAutoCheckbox = new Button(swtInitGroup, SWT.CHECK);
		swtTimeAutoCheckbox.setText("Auto");
		swtTimeAutoCheckbox.setToolTipText("Determine time automatically from first image timestamp");
		swtTimeAutoCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		swtTimeAutoCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lLT = new Label(swtInitGroup, SWT.NONE); lLT.setText("Table:");
		swtTable = new Text(swtInitGroup, SWT.NONE);
		swtTable.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1));
		swtTable.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		swtTableAutoCheckbox = new Button(swtInitGroup, SWT.CHECK);
		swtTableAutoCheckbox.setText("Auto");
		swtTableAutoCheckbox.setToolTipText("Determine table automatically from W7XLoad datastream path");
		swtTableAutoCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		swtTableAutoCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lD = new Label(swtInitGroup, SWT.NONE); lD.setText("Database:");
		swtDatabase = new Combo(swtInitGroup, SWT.NONE | SWT.READ_ONLY);
		swtDatabase.setItems(Mat.toStringArray(Database.values()));
		swtDatabase.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1));
		swtDatabase.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		swtDatabaseAutoCheckbox = new Button(swtInitGroup, SWT.CHECK);
		swtDatabaseAutoCheckbox.setText("Auto");
		swtDatabaseAutoCheckbox.setToolTipText("Determine database automatically from system");
		swtDatabaseAutoCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		swtDatabaseAutoCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
				
		Label lS = new Label(swtInitGroup, SWT.NONE); lS.setText("Spectrometer:");
		swtSpectrometer = new Text(swtInitGroup, SWT.NONE);
		swtSpectrometer.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1));
		swtSpectrometer.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		swtSpectrometerAutoCheckbox = new Button(swtInitGroup, SWT.CHECK);
		swtSpectrometerAutoCheckbox.setText("Auto");
		swtSpectrometerAutoCheckbox.setToolTipText("Determine spectrometer automatically from W7XLoad datastream");
		swtSpectrometerAutoCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		swtSpectrometerAutoCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lT = new Label(swtInitGroup, SWT.NONE); lT.setText("Target:");
		swtTarget = new Text(swtInitGroup, SWT.NONE);
		swtTarget.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 4, 1));
		swtTarget.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		swtTargetAutoCheckbox = new Button(swtInitGroup, SWT.CHECK);
		swtTargetAutoCheckbox.setText("Auto");
		swtTargetAutoCheckbox.setToolTipText("Determine target automatically from default for spectrometer or from notes SpecCal/Target=xxx");
		swtTargetAutoCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 1, 1));
		swtTargetAutoCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		Label lLI = new Label(swtInitGroup, SWT.NONE); lLI.setText("");
		swtLoadIntensityCheckbox = new Button(swtInitGroup, SWT.CHECK);
		swtLoadIntensityCheckbox.setText("Load intensity");
		swtLoadIntensityCheckbox.setToolTipText("Try to load the intensity calibration for the spectrometer/target and fail if it doesn't exist.");
		swtLoadIntensityCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, false, false, 5, 1));
		swtLoadIntensityCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		
		
		update();
		
	}
	
	
	protected void settingsChangedEvent(Event event) {
		SpecCalConfig cfg = proc.getConfig();
		
		cfg.fromDB.enable = swtInitFromArchiveCheckbox.getSelection();
		
		cfg.fromDB.time = NanoTime.convertUTCDateTimeStringtoNanoseconds(swtTime.getText());
		cfg.fromDB.timeAuto = swtTimeAutoCheckbox.getSelection();
		cfg.fromDB.table = swtTable.getText();
		cfg.fromDB.tableAuto = swtTableAutoCheckbox.getSelection();
		cfg.fromDB.database = Database.fromNameFluffy(swtDatabase.getText());
		cfg.fromDB.databaseAuto = swtDatabaseAutoCheckbox.getSelection();
		cfg.fromDB.spectrometer = swtSpectrometer.getText();
		cfg.fromDB.spectrometerAuto = swtSpectrometerAutoCheckbox.getSelection();
		cfg.fromDB.target = swtTarget.getText();
		cfg.fromDB.targetAuto = swtTargetAutoCheckbox.getSelection();
		cfg.fromDB.loadIntensity = swtLoadIntensityCheckbox.getSelection();
		
		update();
	}


	public void update(){
		SpecCalConfig cfg = proc.getConfig();
		
		swtInitFromArchiveCheckbox.setSelection(cfg.fromDB.enable);
		swtTime.setText(NanoTime.toUTCString(cfg.fromDB.time));
		swtTimeAutoCheckbox.setSelection(cfg.fromDB.timeAuto);
		swtTable.setText(cfg.fromDB.table != null ? cfg.fromDB.table : "");
		swtTableAutoCheckbox.setSelection(cfg.fromDB.tableAuto);
		swtDatabase.setText(cfg.fromDB.database == null ? Database.W7X_ARCHIVE.toString() : cfg.fromDB.database.toString());
		swtDatabaseAutoCheckbox.setSelection(cfg.fromDB.databaseAuto);
		swtSpectrometer.setText(cfg.fromDB.spectrometer != null ? cfg.fromDB.spectrometer : "");
		swtSpectrometerAutoCheckbox.setSelection(cfg.fromDB.spectrometerAuto);
		swtTarget.setText(cfg.fromDB.target != null ? cfg.fromDB.target : "");
		swtTargetAutoCheckbox.setSelection(cfg.fromDB.targetAuto);	
		swtLoadIntensityCheckbox.setSelection(cfg.fromDB.loadIntensity);
		
		swtTime.setEnabled(!cfg.fromDB.timeAuto);
		swtTable.setEnabled(!cfg.fromDB.tableAuto);
		swtDatabase.setEnabled(!cfg.fromDB.databaseAuto);
		swtSpectrometer.setEnabled(!cfg.fromDB.spectrometerAuto);
		swtTarget.setEnabled(!cfg.fromDB.targetAuto);
		
		
	}

	public Control getSWTGroup() { return swtInitGroup; }
	
	
}
