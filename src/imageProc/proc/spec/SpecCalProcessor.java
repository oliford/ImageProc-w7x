package imageProc.proc.spec;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.math3.util.FastMath;
import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import algorithmrepository.Algorithms;
import algorithmrepository.Interpolation1D;
import descriptors.w7x.ArchiveDBDesc;
import edu.ucar.ral.nujan.netcdf.NhDimension;
import edu.ucar.ral.nujan.netcdf.NhException;
import edu.ucar.ral.nujan.netcdf.NhFileWriter;
import edu.ucar.ral.nujan.netcdf.NhGroup;
import edu.ucar.ral.nujan.netcdf.NhVariable;
import imageProc.core.ConfigurableByID;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.MetaDataMap;
import imageProc.core.MetaDataMap.MetaData;
import imageProc.core.EventReciever.Event;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSink;
import imageProc.proc.fastSpec.FastSpecConfig;
import imageProc.proc.fastSpec.FastSpecEntry;
import imageProc.proc.fastSpec.FastSpecProcessor;
import imageProc.proc.seriesAvg.SeriesProcessor;
import imageProc.proc.spec.SpecCalConfig.TrackConfig;
import imageProc.proc.spec.SpecCalConfig.TrackConfig.ParamType;
import imageProc.w7x.W7XUtil;
import imageProc.w7xspec.Defaults;
import imageProc.w7xspec.LightPaths;
import imageProc.w7xspec.LightPaths.Component;
import imageProc.w7xspec.LightPaths.PathInfo;
import imageProc.w7xspec.Metadata;
import imageProc.w7xspec.SpecCal;
import imageProc.w7xspec.SpecCal.ChanFit;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import w7x.archive.ArchiveDBFetcher;

/** SpecCal processor.
 * 
 * All in one processor for the wavelength and intensity calibration of a spectrometer.
 * Calibrations are done per-track (ROI row) and stored against the name of the row 
 * according to the ROI source. 
 * 
 * Some assumptions:
 *  Wavelength varies in x
 *  Spatial line varies in y
 *  
 *  
 *  
 *  Calibrations:
 *    Wavelength: Given knots, linear/cubic interp. Polynomial lambda(x)
 *    Instrument function: knots, linear/cubic, polynomial etc 
 *    
 *    Storage: 
 *       Track name, wavlen coeffs[], intensity coeffs[]
 *       
 *       
 *  Advanced (later)
 *     Wavelength global shift - fit to a single/few channels at specific images (for pre/post-shot recal)
 *     Fit wavelength - predefined cal positions with known wavelength, all existing tracks
 *     Fit intensity - all existing tracks, given range
 *  
 * 
 * @author oliford
 */
public class SpecCalProcessor extends ImgSourceOrSinkImpl implements ImgSink, ConfigurableByID, EventReciever {
	public static enum CalType { WAVELEN, INTENSITY, INSTRUMENT };		
	
	private SpecCalConfig config;
	
	protected boolean autoCalc = false;	
	protected boolean abortCalc = false;
	private String status;	
	private boolean lastCalcComplete = false;
	
	private final double beamOnPowerThreshold = 50000; //50 kW
	
	private static class ROIInfo {
		public String name;
		public int x, y, w, h, bx, by;
	}
	public boolean roisOutputTransposed;
	private ROIInfo[] rois;

	private String lastLoadedConfig;

	public SpecCalProcessor() {
		config = new SpecCalConfig();
		status = "init";
	}
	
	public SpecCalProcessor(ImgSource source, int selectedIndex) {
		this();
		config = new SpecCalConfig();
		setSource(source);
		setSelectedSourceIndex(selectedIndex);
	}
	
	@Override
	public void calc() {		
		abortCalc = false;
		ImageProcUtil.ensureFinalUpdate(this, new Runnable() { @Override public void run() { doCalc(); } });
	}
		
	/** Guaranteed to be called only once at a time */
	protected void doCalc(){
		try{
			lastCalcComplete = false;
			
			setStatus("Calculating calibrations");
			
			getROIInfo();
			
			if(config.wavelnCommonOffset == 0){
				String streamPath = (String) getSeriesMetaData("/w7x/path");
				if(streamPath != null) {
					long nanos[] = W7XUtil.getBestNanotime(connectedSource);
					
					//maybe there's an offset in the notes
					ArchiveDBDesc desc = new ArchiveDBDesc(streamPath);
					desc.setNanosRange(nanos[0], nanos[0]);
					
					String str = Metadata.getMetadataFromNotes(desc, "SpecCal/config/wavelnCommonOffset");
					if(str != null)
						config.wavelnCommonOffset = Algorithms.mustParseDouble(str);
				}
			}

			if(config.fromDB != null && config.fromDB.enable){
				createTrackInfoFromROINames();
				loadLightPathsFromDB();
				generateLineOfSightArrays();
				
				loadWavelengthCalibration();
				loadIntensityCalibration();
				
				generateCalibrationArrays();
				
			}else{
				populateRowNumbers();	
				generateCalibrationArrays();
				
				generateLineOfSightArrays();
			}
					
			/** Calculate 'passive' emission positions: Position of median rEff */
			if(config.procBeamData){
				generateBeamIntersectionArrays();
			}else if(!Double.isNaN(config.passivePositionAlongLOS) && config.passivePositionAlongLOS != 0){
				generatePassivePositionArrays();
				
			}else{
				connectedSource.setSeriesMetaData("SpecCal/linesOfSight/activeBeamIntersectionPos", null, true);
				connectedSource.setSeriesMetaData("SpecCal/linesOfSight/nominalBeamIntersectionPos", null, false);				
			}
			
			ImageProcUtil.getGenericCameraMetadata(connectedSource);
			
			if(config.writeCXSFitFile){
				doWriteCXSFitFile();
			}
			
			updateAllControllers();
			lastCalcComplete = true;
			setStatus("Done");
			
		}catch(RuntimeException err){
			err.printStackTrace();
			setStatus("ERROR: " + err);
		}
	}
	
	/** Get the ROI data from the camera/software binning */ 
	private void getROIInfo(){
		setStatus("Collecting ROI information");
		
		//Find the 'Rois_arrays'
		MetaDataMap metaMap = getCompleteSeriesMetaDataMap();
		
		Object roiNames = null;
		Object roiX = null, roiY = null;
		Object roiW = null, roiH = null;
		Object roiBX = null, roiBY = null;

		for(Entry<String, MetaData> metaEntry : metaMap.entrySet()){
			String name = metaEntry.getKey();
			if(name.contains("Rois_arrays")){
				String roisPath = null;
				try{
					roisPath = name.replaceAll("Rois_arrays/.*", "Rois_arrays");		
	
					roiNames = Mat.fixWeirdContainers(metaMap.get(roisPath + "/names"));

					roiX = Mat.fixWeirdContainers(metaMap.get(roisPath + "/x"), true);
					roiY = Mat.fixWeirdContainers(metaMap.get(roisPath + "/y"), true);
					roiW = Mat.fixWeirdContainers(metaMap.get(roisPath + "/width"), true);
					roiH = Mat.fixWeirdContainers(metaMap.get(roisPath + "/height"), true);
					roiBX = Mat.fixWeirdContainers(metaMap.get(roisPath + "/x_binning"), true);
					roiBY = Mat.fixWeirdContainers(metaMap.get(roisPath + "/y_binning"), true);

					roisOutputTransposed = false;
					Object o = metaMap.get(roisPath + "/transposeOutput");
					if(o != null && o instanceof Boolean){
						roisOutputTransposed = (Boolean)o;
					}


					if(roiNames != null && roiX != null && roiY != null 
							&& roiW != null && roiH != null && roiBX != null && roiBY != null){
						break;
					}
					roiNames = null; roiX = null; roiY = null; roiW = null; roiH = null; roiBX = null; roiBY = null;

				}catch(RuntimeException err){ 
					System.err.println("WARNING: Trouble interpreting ROI arrayss at path '"+roisPath+"': " + err.getMessage());
					roiNames = null; roiX = null; roiY = null; roiW = null; roiH = null; roiBX = null; roiBY = null;

				}
			}
		}
		
		if(roiNames == null) {
			//try special case of single channel (e.g. USB) spectrometers
			//where we can make up some ROI data
			try {
				Img img = connectedSource.getImage(0);
				if(img.getHeight() == 1) {
										
					roiNames = new String[] { "Avaspec:01" }; //ehem, for now
					roiX = new int[] { 0 };
					roiY = new int[] { 0 };
					roiW = new int[] { img.getWidth() }; 
					roiH = new int[] { 1 }; 
					roiBX = new int[] { 1 }; 
					roiBY = new int[] { 1 };
					
				}
			
			}catch(RuntimeException err) {
				logr.log(Level.WARNING, "Error attempting to make up single channel ROI info.", err);
			}
			
			
		}

		if(roiNames == null)
			throw new RuntimeException("No 'Rois_arrays' meta data found. No binning/tracks?");

		if(roisOutputTransposed){
			Object tmp;
			
			tmp = roiX; roiX = roiY; roiY = tmp;
			tmp = roiW; roiW = roiH; roiH = tmp;
			tmp = roiBX; roiBX = roiBY; roiBY = tmp;
		}
		
		String[] roiNamesStr;
		if(roiNames instanceof String[][])
			roiNamesStr = ((String[][])roiNames)[0];
		else
			roiNamesStr = (String[])roiNames;
		
		if(Array.get(roiX, 0).getClass().isArray()) { //ugh nested arrays
			roiX = Array.get(roiX, 0);
			roiY = Array.get(roiY, 0);
			roiW = Array.get(roiW, 0);
			roiH = Array.get(roiH, 0);
			roiBX = Array.get(roiBX, 0);
			roiBY= Array.get(roiBY, 0);
		}
		
		rois = new ROIInfo[roiNamesStr.length];
		for(int i=0; i < roiNamesStr.length; i++){
			rois[i] = new ROIInfo();
			rois[i].name = roiNamesStr[i];
			rois[i].x = ((Number)Array.get(roiX, i)).intValue();
			rois[i].y = ((Number)Array.get(roiY, i)).intValue();
			rois[i].w = ((Number)Array.get(roiW, i)).intValue();
			rois[i].h = ((Number)Array.get(roiH, i)).intValue();
			rois[i].bx = ((Number)Array.get(roiBX, i)).intValue();
			rois[i].by = ((Number)Array.get(roiBY, i)).intValue();
		}
	}
	
	/** if this is based on loaded trackinfo, we need to recreate the row numbers according to the names
		because the ROI enables might be different	*/		
	private void populateRowNumbers(){
		
		//set row numbers of tracks in our config
		for(TrackConfig trackConfig : config.tracks){
			trackConfig.row = -1;				
		}
		
		//fill in row numbers
		for(int i=0; i < rois.length; i++){
			TrackConfig trackConfig = config.getTrackConfig(rois[i].name);
			if(trackConfig == null){
				trackConfig = new TrackConfig();
				trackConfig.name = rois[i].name;	
				config.tracks.add(trackConfig);
			}
			//trackConfig.wavelenParams[1][0] -= 33.718;
			trackConfig.row = i;
		}
	}
	
	/** Generate arrays vs pixel number from calibration data to match current image ROI */ 
	private void generateCalibrationArrays() {
		setStatus("Generating calibration arrays");
				
		
		Img img = connectedSource.getImage(0);
		
		double imgWavelen[][] = new double[img.getHeight()][];		 
		double imgIntensity[][] = new double[img.getHeight()][];
		double imgInsFuncWidth[][] = new double[img.getHeight()][];
		double imgInsFuncPower[][] = new double[img.getHeight()][];
		
		int y = 0;
		for(int iR=0; iR < rois.length; iR++){
			setStatus("Calculating calibrations "+iR+" / "+rois.length);
			
			TrackConfig trackConfig = config.getTrackConfig(rois[iR].name);
			if(trackConfig == null)
				continue; //shouldn't happen
			
			int nX = rois[iR].w / rois[iR].bx;
			int x0 = rois[iR].x;
			int dx = rois[iR].bx;
			
			int k=0;
			double rowWaveLen[]=null, rowIntensity[]=null, rowInsFuncWidth[]=null,rowInsFuncPower[]=null;
			rowWaveLen = deployParameterisation(trackConfig.wavelenParams, 1, trackConfig.wavelenParamType, x0, dx, nX, img.getWidth());
			rowIntensity = deployParameterisation(trackConfig.intensityParams, 1, trackConfig.intensityParamType, x0, dx, nX, img.getWidth());
			rowInsFuncWidth = deployParameterisation(trackConfig.instrumentFunctionWidthParams, 1, trackConfig.instrumentFunctionWidthParamType, x0, dx, nX, img.getWidth());
			if(trackConfig.instrumentFunctionWidthParams != null && trackConfig.instrumentFunctionWidthParams.length > 2)
				rowInsFuncPower = deployParameterisation(trackConfig.instrumentFunctionWidthParams, 2, trackConfig.instrumentFunctionWidthParamType, x0, dx, nX, img.getWidth());
				
			
			//apply common offsets and adjustments
			for(int i=0; i < rowWaveLen.length; i++) {
				rowWaveLen[i] += config.wavelnCommonOffset;
				rowInsFuncWidth[i] += config.instrumentFunctionWidthOffset;
			}
			
			int nRowsInROI = rois[iR].h / rois[iR].by;
			for(int j=0; j < nRowsInROI; j++){
				imgWavelen[y] = rowWaveLen;
				imgIntensity[y] = rowIntensity;
				imgInsFuncWidth[y] = rowInsFuncWidth;
				imgInsFuncPower[y] = rowInsFuncPower;
				y++;
			}
		}	
		ImgSource src = connectedSource;
		
		//we can dump stuff higher up if its just a series processor since that won't change
		//the calibration
		if(src instanceof SeriesProcessor)
			src = ((SeriesProcessor)src).getConnectedSource();
		
		src.setSeriesMetaData("SpecCal/config/wavelnCommonOffset", config.wavelnCommonOffset, false);
		src.setSeriesMetaData("SpecCal/wavelength", imgWavelen, false);
		src.setSeriesMetaData("SpecCal/intensity", imgIntensity, false);
		src.setSeriesMetaData("SpecCal/instrumentFuncWidth", imgInsFuncWidth, false);
		src.setSeriesMetaData("SpecCal/instrumentFuncPower", imgInsFuncPower, false);
		
	}
	
	private void createTrackInfoFromROINames(){
		setStatus("Generating track name");
		config.tracks.clear();
		
		for(int i=0; i < rois.length; i++){
			TrackConfig track = new TrackConfig();
			track.row = i;
			track.name = rois[i].name;
			config.tracks.add(track);
		}
	}
	
	private void loadLightPathsFromDB(){
		
		setStatus("Querying LightPaths webservice");
		
		String streamPath = (String) getSeriesMetaData("/w7x/path");		
		long nanos[] = W7XUtil.getBestNanotime(connectedSource);
		
		if(config.fromDB.tableAuto){
			config.fromDB.table =  imageProc.w7xspec.Defaults.getTableFromDatastream(streamPath);
		}
		
		if(config.fromDB.databaseAuto){
			config.fromDB.database = imageProc.w7xspec.Defaults.getDatabase(config.fromDB.table);

			if(config.fromDB.database == null)
				throw new RuntimeException("Unable to automatically determine database");
		}
				
		if(config.fromDB.spectrometerAuto){
			config.fromDB.spectrometer = null;
			if(streamPath == null)
				throw new RuntimeException("spectrometer not set and no datastream path in metadata with which to guess it");
			
			//get rid of versions in the path, to make the match more likely
			String streamPath2 = streamPath.replaceAll("/V[0-9]*/", "/");
			
			//look for this datastream in the components info
			Component components[] = LightPaths.getComponents(config.fromDB.table, nanos[0]);
			for(Component c : components){
				if(c.datastreamPaths != null){
					for(String path : c.datastreamPaths){
						if(streamPath2.matches("/?w7x/" + path + ".*"))
							config.fromDB.spectrometer = c.name;
					}					
				}
			}
			
			if(config.fromDB.spectrometer == null){
				config.fromDB.spectrometer = imageProc.w7xspec.Defaults.getSpectrometerFromDatastream(streamPath);
				
			}			
		}
		
		if(config.fromDB.targetAuto){
			config.fromDB.target = null;
			
			//see if it's in the Metadata
			config.fromDB.target = (String)connectedSource.getSeriesMetaData("SpecCal/Target");
			
			
			//if not, look in the notes
			if(config.fromDB.target == null && streamPath != null && nanos != null){
				ArchiveDBDesc desc = new ArchiveDBDesc(streamPath);
				desc.setNanosRange(nanos[0], nanos[nanos.length-1]);
				
				config.fromDB.target = Metadata.getMetadataFromNotes(desc, "TARGET");
			}
			
			if(config.fromDB.target == null){
				String systemName = imageProc.w7xspec.Defaults.getFullSystemName(config.fromDB.table);
				config.fromDB.target = imageProc.w7xspec.Defaults.getTarget(systemName, config.fromDB.spectrometer);
			}
			
			if(config.fromDB.target == null)
				throw new RuntimeException("Unable to automatically determine target");
				
		}
		
		if(config.fromDB.timeAuto){			
			config.fromDB.time = nanos[0];
		}
		
		//we can now apply any fixed for the channel names
		String[] chanNames = new String[rois.length];
		for(int i=0; i < rois.length; i++)
			chanNames[i] = rois[i].name;
		
		String systemName = imageProc.w7xspec.Defaults.getFullSystemName(config.fromDB.table);
		Defaults.fixROINames(systemName, config.fromDB.spectrometer, chanNames);
		for(int i=0; i < rois.length; i++)
			rois[i].name = chanNames[i];
		for(TrackConfig track : config.tracks)
			track.name = rois[track.row].name;
			
		ArrayList<String> channels = new ArrayList<String>();		
		for(TrackConfig track : config.tracks){
			
			String parts[] = track.name.split(":");
			if(parts.length < 2)
				throw new RuntimeException("Track name doesn't match 'component:channel': '"+track.name+"'");
			
			if(!config.fromDB.spectrometer.equals(parts[0])){
				if(config.fromDB.forceROINamesToComponent){
					track.name = config.fromDB.spectrometer + ":" + parts[1];
					rois[track.row].name = track.name;
				}else{
					throw new RuntimeException("ROI " +track.name+ " is a component that isn't this device ("+config.fromDB.spectrometer+").");
				}
			}
			
			channels.add(parts[1]);
		}

		PathInfo[] pathInfos = LightPaths.getPathInfo(
				config.fromDB.database,
				config.fromDB.table, 
				config.fromDB.spectrometer, 
				null, 
				channels.toArray(new String[0]), 
				config.fromDB.time, 
				false, //we don't need the path lifetime  
				true); //drop table name from path

		for(PathInfo info : pathInfos){
			String trackID = info.path.get(0).component + ":" + info.path.get(0).channel;

			//find the relevant track				
			TrackConfig track = config.getTrackConfig(trackID);

			track.lightPath = info.pathStr;
			if(info.los != null){
				track.losStart = info.los.start;
				track.losUVec = info.los.uVec;
			}else{
				track.losStart = null;
				track.losUVec = null;
			}
		}


		if(abortCalc)
			throw new RuntimeException("Aborted in LightPaths query");
		
	}

	private void loadWavelengthCalibration() {
				
		long nanos[] = W7XUtil.getBestNanotime(connectedSource);
		
		//SpecCal.getCalibration(table, spectrometer, target, set, nanos[0]);
		String systemName = imageProc.w7xspec.Defaults.getFullSystemName(config.fromDB.table);
		SpecCal sp = new SpecCal(systemName, config.fromDB.spectrometer, 
								 config.fromDB.time, null, config.fromDB.target,
								 !config.fromDB.loadIntensity, //intensity?
								 config.wavelnCommonOffset);
		
		for(TrackConfig track : config.tracks){			
			ChanFit cf = sp.getCalibration(track.name, "lambda");
			
			if(cf == null){
				Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "No lambda calibration found for track '"+track.name+"'");
				
			}else{
				
				track.wavelenParamType = ParamType.POLYNOMIAL;
				track.wavelenParams = new double[2][cf.coeffs.length+1];
				track.wavelenParams[0][0] = -1;
				track.wavelenParams[1][0] = cf.x0;
				for(int j=0; j < cf.coeffs.length; j++){
					track.wavelenParams[0][j+1] = j;
					track.wavelenParams[1][j+1] = cf.coeffs[j]; 
				}
			}
				
			cf = sp.getCalibration(track.name, "Sigma");
			ChanFit cfPower = sp.getCalibration(track.name, "Power");
			
			if(cf == null){
				Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "No sigma calibration found for track '"+track.name+"'");
				
			}else{
				
				track.instrumentFunctionWidthParamType = ParamType.POLYNOMIAL;
				track.instrumentFunctionWidthParams = new double[3][cf.coeffs.length+1];
				track.instrumentFunctionWidthParams[0][0] = -1;
				track.instrumentFunctionWidthParams[1][0] = cf.x0;
				track.instrumentFunctionWidthParams[2][0] = 0;
				for(int j=0; j < cf.coeffs.length; j++){
					track.instrumentFunctionWidthParams[0][j+1] = j;
					track.instrumentFunctionWidthParams[1][j+1] = cf.coeffs[j]; 
					track.instrumentFunctionWidthParams[2][j+1] = (cfPower != null) ? cfPower.coeffs[j] : 2.0; 
				}
			}
			
			cf = sp.getCalibration(track.name, "Intensity");
			
			if(cf == null){
				Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "No intensity calibration found for track '"+track.name+"'");
				
			}else{
				
				track.intensityParamType = ParamType.POLYNOMIAL;
				track.intensityParams = new double[3][cf.coeffs.length+1];
				track.intensityParams[0][0] = -1;
				track.intensityParams[1][0] = cf.x0;
				for(int j=0; j < cf.coeffs.length; j++){
					track.intensityParams[0][j+1] = j;
					track.intensityParams[1][j+1] = cf.coeffs[j]; 
				}
			}
		}
	}

	private void loadIntensityCalibration() {
		//done in loadWavelengthCalibration because SpecCal is there
	}

	private void generateLineOfSightArrays() {
		//Prep metadata for losNames and beam crossing etc
		int imageHeight = connectedSource.getImage(0).getHeight();

		double losStart[][] = new double[imageHeight][3]; 
		double losUVec[][] = new double[imageHeight][3]; 
		String[] losNames = new String[imageHeight];
		String[] lightPaths = new String[imageHeight];
					
		// fill LOS names and fixed geometry
		//defaults unknown for all
		for(int i=0; i < imageHeight; i++){
			losNames[i] = "UNKNOWN_row_"+i;
			lightPaths[i] = "";
			for(int k=0; k < 3; k++){
				losStart[i][k] = Double.NaN;
				losUVec[i][k] = Double.NaN;
			}
		}
		
		for(TrackConfig trackConfig : config.tracks){
			final int i = trackConfig.row;
			if(i < 0)
				continue;
			if(i >= losNames.length) {
				logr.warning("Got track config for row " + i + ", but only " + losNames.length + " rows are present");
				continue;
			}
			
			if(trackConfig.lightPath != null){
				String parts[] = trackConfig.lightPath.split("=");
				if(parts.length > 1) {
					losNames[i] = parts[parts.length-1].trim();
					losNames[i] = losNames[i].replaceAll("\\[.*\\]", "");
					lightPaths[i] = trackConfig.lightPath;
				}
			}
			
			if(trackConfig.losStart != null)
				losStart[i] =  trackConfig.losStart.clone();
				
			if(trackConfig.losUVec != null)
				losUVec[i] = trackConfig.losUVec.clone();

		}			
		
		connectedSource.setSeriesMetaData("SpecCal/linesOfSight/names", losNames, false);				
		connectedSource.setSeriesMetaData("SpecCal/linesOfSight/lightPaths", lightPaths, false);
		connectedSource.setSeriesMetaData("SpecCal/linesOfSight/startPos", losStart, false);
		connectedSource.setSeriesMetaData("SpecCal/linesOfSight/unitVec", losUVec, false);	
	}
	
	private void generateBeamIntersectionArrays() {
		//boolean configWeightedAverageBeamPos = false; //this doesn help, because the effect is only in rho space :/
		
		//los/beam intersection bos - time dependerant because of beam changes
		
		int nImages = connectedSource.getNumImages();		
		int imageHeight = connectedSource.getImage(0).getHeight();
		double[][] losStart = (double[][]) getSeriesMetaData("SpecCal/linesOfSight/startPos");
		double[][] losUVec = (double[][]) getSeriesMetaData("SpecCal/linesOfSight/unitVec");	

		double beamInfo[][][] = null;
		try{
			beamInfo = W7XUtil.getExistingBeamInfo(connectedSource);
			//beamInfo = W7XUtil.getBeamInfo(connectedSource, false);
		}catch(Exception err){
			throw new RuntimeException("No Beam energy for beamOn selection. Add a SeriesProcessor!", err);
		}
		
		if(beamInfo == null)
			throw new RuntimeException("No Beam energy for beamOn selection. Add a SeriesProcessor!");
		//long nanoTime[] = W7XArchiveDBSink.getBestNanotimebase(connectedSource);
		
		double beamPower[][] = beamInfo[2];
		
		//calculate intersection of all beams with all lines of sight
		double maxDistToBeam = 0.500; //maximum distance to a beam to consider it intersecting it
		int nBeams = beamPower.length;
		double beamItersection[][][] = new double[nBeams][imageHeight][4];
		for(int iB=0; iB < nBeams; iB++){
			double beamStart[] = W7XUtil.getBeamStart(iB);
			double beamUVec[] = W7XUtil.getBeamUVec(iB);			
			
			for(int iY=0; iY < imageHeight; iY++){						
				double s = Algorithms.pointOnLineNearestAnotherLine(losStart[iY], losUVec[iY], beamStart, beamUVec);
				double pOnLOS[] = Mat.add(losStart[iY], Mat.mul(losUVec[iY], s));
				
				s = Algorithms.pointOnLineNearestAnotherLine(beamStart, beamUVec, losStart[iY], losUVec[iY]);
				double pOnBeam[] = Mat.add(beamStart, Mat.mul(beamUVec, s));
				
				double distToBeam = Mat.veclength(Mat.subtract(pOnLOS, pOnBeam));
				
				if(distToBeam < maxDistToBeam)		
					beamItersection[iB][iY] = new double[]{ pOnLOS[0], pOnLOS[1], pOnLOS[2], distToBeam };
				else
					beamItersection[iB][iY] = new double[]{ Double.NaN, Double.NaN, Double.NaN, Double.NaN };
			}
			//Mat.mustWriteBinary("/tmp/beamIsect-Q"+iB+".bin", beamItersection[iB], false);
		}
		
		//calculate weighted average position for each time point
		
		boolean isBeamEverOn[] = new boolean[nBeams]; //on and also in view (< threshold distance)
		double losBeamCrossPos[][][] = new double[nImages][imageHeight][3];
		for(int iF=0; iF < nImages; iF++){
			for(int iY=0; iY < imageHeight; iY++){
				for(int k=0; k < 3; k++)
					losBeamCrossPos[iF][iY][k] = 0;
				double sumWeights = 0;
				for(int iB=0; iB < nBeams; iB++){
					if(iB < 4)
						continue;
					//Is this beam on at this time and also in view (< threshold distance from LOS)
					if(beamPower[iB] != null && beamPower[iB][iF] > beamOnPowerThreshold){
						isBeamEverOn[iB] = true;
						
						//weighted average by distance 
						double weight =  1 / beamItersection[iB][iY][3];
						for(int k=0; k < 3; k++)
							losBeamCrossPos[iF][iY][k] += weight * beamItersection[iB][iY][k];
						sumWeights += weight;
					}
				}
				
				for(int k=0; k < 3; k++){
					losBeamCrossPos[iF][iY][k] = (sumWeights > 0) ? (losBeamCrossPos[iF][iY][k] / sumWeights) 
																: Double.NaN;
				}
			}
			//Mat.mustWriteBinary("/tmp/beamIsect-F"+iF+".bin", losBeamCrossPos[iF], false);
			
		}
		
		connectedSource.setSeriesMetaData("SpecCal/linesOfSight/activeBeamIntersectionPos", losBeamCrossPos, true);
		
		//calculate 'nominal' position		
		double nominalBeamCrossPos[][] = new double[imageHeight][3];		
		for(int iY=0; iY < imageHeight; iY++){
			for(int k=0; k < 3; k++)
				nominalBeamCrossPos[iY][k] = 0;
			double sumWeights = 0;
			for(int iB=0; iB < nBeams; iB++){
				if(isBeamEverOn[iB]){
					double weight =  1 / beamItersection[iB][iY][3];
					for(int k=0; k < 3; k++)
						nominalBeamCrossPos[iY][k] += weight * beamItersection[iB][iY][k];
					sumWeights += weight;
				}
			}
			for(int k=0; k < 3; k++){
				nominalBeamCrossPos[iY][k] = (sumWeights > 0) ? (nominalBeamCrossPos[iY][k] / sumWeights) 
															: Double.NaN;
			}
		}
		
		//Mat.mustWriteBinary("/tmp/beamISect-nominal.bin", nominalBeamCrossPos, false);
		connectedSource.setSeriesMetaData("SpecCal/linesOfSight/nominalBeamIntersectionPos", nominalBeamCrossPos, false);
		
	}

	private void generatePassivePositionArrays(){ 
		int nImages = connectedSource.getNumImages();		
		int imageHeight = connectedSource.getImage(0).getHeight();
		double[][] losStart = (double[][]) getSeriesMetaData("SpecCal/linesOfSight/startPos");
		double[][] losUVec = (double[][]) getSeriesMetaData("SpecCal/linesOfSight/unitVec");	
	
		//calculate 'nominal' position		
		double nominalBeamCrossPos[][] = new double[imageHeight][3];
		double losBeamCrossPos[][][] = new double[nImages][imageHeight][3];
		
		for(int iY=0; iY < imageHeight; iY++)
			for(int k=0; k < 3; k++)
				nominalBeamCrossPos[iY][k] = losStart[iY][k] + config.passivePositionAlongLOS * losUVec[iY][k];
			
		for(int iF=0;iF < nImages; iF++)
			for(int iY=0; iY < imageHeight; iY++)
				for(int k=0; k < 3; k++)
					losBeamCrossPos[iF][iY][k] = losStart[iY][k] + config.passivePositionAlongLOS * losUVec[iY][k];
				
		connectedSource.setSeriesMetaData("SpecCal/linesOfSight/activeBeamIntersectionPos", losBeamCrossPos, true);
		connectedSource.setSeriesMetaData("SpecCal/linesOfSight/nominalBeamIntersectionPos", nominalBeamCrossPos, false);
	}
	
	private double[] deployParameterisation(double[][] params, int paramIdx, ParamType paramType, double x0, int dx, int nXrow, int nXimg) {
		double rowWaveLen[] = Mat.fillArray(Double.NaN, nXimg);
		if(paramType == ParamType.KNOTS_LINEAR){
			Interpolation1D interp = new Interpolation1D(params[0], params[paramIdx]);
			
			for(int iX=0; iX < Math.min(nXrow, nXimg); iX++){
				double x = x0 + iX * dx;
				rowWaveLen[iX] = interp.eval(x);
			}
		}else if(paramType == ParamType.POLYNOMIAL){
			double polyX0 = 0;
			
			int nC =  params[0].length;
			//look for polyX0, given by coeffPower = -1
			for(int j=0; j < nC; j++){
				if(params[0][j] == -1) 
					polyX0 = params[1][j];
			}
				
			for(int iX=0; iX < Math.min(nXrow, nXimg); iX++){
				double x = x0 + iX * dx;
				rowWaveLen[iX] = 0;
				for(int j=0; j < nC; j++){
					if(params[0][j] < 0)
						continue;
					rowWaveLen[iX] += params[paramIdx][j] * FastMath.pow(x - polyX0, params[0][j]);
				}
			}
		}else{
			//throw new RuntimeException("Unsupported parameterisation type '"+wavelenParamType+"'");
			System.err.println("Unsupported parameterisation type '"+paramType+"'");
		}
		
		return rowWaveLen;
	}
	

	@Override
	public boolean wasLastCalcComplete(){ return lastCalcComplete; }

	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalAbort:
			abortCalc();	
		default:
		}
	}
	
	@Override
	public void abortCalc(){ 
		abortCalc = true;
	}

	@Override
	public SpecCalProcessor clone() { return new SpecCalProcessor(connectedSource, getSelectedSourceIndex());	}

	public boolean getAutoUpdate() { return this.autoCalc; }

	@Override
	public void imageChangedRangeComplete(int idx) { 
		autoCalc=false;
		if(autoCalc){
			calc();
		}
 	}
	
	@Override
	protected void selectedImageChanged() {
		super.selectedImageChanged();
		if(autoCalc)
			calc();
		
		//maybe GUI needs to know anyway
		updateAllControllers();			
	}

	@Override	
	public void setAutoUpdate(boolean autoCalc) {
		if(!this.autoCalc && autoCalc){
			this.autoCalc = true;
			updateAllControllers();
			calc();			
		}else{
			this.autoCalc = autoCalc;
			updateAllControllers();
		}
	}

	@Override
	public boolean isIdle() { 
		return !ImageProcUtil.isUpdateInProgress(this);
	}

	
	public String getStatus() { return status; }

	private void setStatus(String status) {
		this.status = status;
		updateAllControllers();
	}
	
	public SpecCalConfig getConfig() { return config;	}

	public void configModified(){
		updateAllControllers();
	}
	
	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		json = json.replaceAll("},", "},\n");
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, SpecCalConfig.class);
		
		updateAllControllers();	
	}
	
	@Override
	/** For the sink side */
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			SpecCalSWTController controller = new SpecCalSWTController((Composite)args[0], (Integer)args[1], this);
			controllers.add(controller);
			return controller;
		}
		return null;
	}

	public void setNumParams(CalType calType, int selection) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void updateAllControllers() { super.updateAllControllers();	}

	public void setFromFastSpec(TrackConfig trackAA) {
		//Find the 'Rois_arrays'
		MetaDataMap metaMap = getCompleteSeriesMetaDataMap();
		
		Integer roiX[] = null;
		Integer roiY[] = null;
		boolean outputTransposed = false;
		
		for(Entry<String, MetaData> metaEntry : metaMap.entrySet()){
			String name = metaEntry.getKey();
			if(name.contains("Rois_arrays")){
				try{
					String roisPath = name.replaceAll("Rois_arrays/.*", "Rois_arrays/");					
					roiX = (Integer[]) metaMap.get(roisPath + "/x");
					roiY = (Integer[]) metaMap.get(roisPath + "/y");
					
					outputTransposed = false;
					Object o = metaMap.get(roisPath + "/transposeOutput");
					if(o != null && o instanceof Boolean){
						outputTransposed = (Boolean)o;
					}
						
											
					if(roiX != null && roiY != null){
						break;
					}
				}catch(RuntimeException err){ }
			}
		}
		
		if(roiX == null)
			throw new RuntimeException("No 'Rois_arrays' meta data found. No binning/tracks?");
		
		//set row numbers of tracks in our config
		for(TrackConfig trackConfig : config.tracks){
			trackConfig.row = -1;				
		}
		
		if(outputTransposed){
			Integer[] tmp;
			
			tmp = roiX; roiX = roiY; roiY = tmp;
		}
		
		
		FastSpecConfig fsConfig = null;
		for(ImgSink sink : connectedSource.getConnectedSinks()){
			if(!(sink instanceof FastSpecProcessor))
				continue;
			
			fsConfig = ((FastSpecProcessor)sink).getConfig();			
			break;
		}
		if(fsConfig == null)
			return;
		
		int frameIdx = getSelectedSourceIndex();
		for(TrackConfig track : config.tracks){
			ArrayList<double[]> calPoints = new ArrayList<double[]>();
			for(FastSpecEntry fsEntry : fsConfig.specEntries){
				//look for entries only on this row
				if(fsEntry.y0 == track.row && fsEntry.y1 == (track.row+1)){
					calPoints.add(new double[]{ 
							fsEntry.nominalWavelength,
							fsEntry.fit.X0[frameIdx]
					});
				}
				
			}
			track.wavelenParamType = ParamType.KNOTS_LINEAR;
			track.wavelenParams = new double[2][calPoints.size()];
			for(int i=0; i < calPoints.size(); i++){
				track.wavelenParams[1][i] = calPoints.get(i)[0] / 1e-9;
				track.wavelenParams[0][i] = calPoints.get(i)[1];
				
				//fastspec works on image pixel coords and ignore ccdgeom and ROIs
				//we work under the ROIs so need to take off the 'current' ROI
				track.wavelenParams[0][i] += roiX[0];//
						
				System.out.println(track.row + ": " + track.wavelenParams[1][i] + ", " + track.wavelenParams[0][i]);
			}
		}
		
	}

	private String writeCXSFileSyncObj = new String("writeCXSFileSyncObj");
	/** Deploy a NetCDF4 file storing everything CXSFit needs to know */
	public void writeCXSFitFile() {
		ImageProcUtil.ensureFinalUpdate(writeCXSFileSyncObj, new Runnable() { @Override public void run() { doWriteCXSFitFile(); } });
	}
	
	/** Deploy a NetCDF4 file storing everything CXSFit needs to know */
	public void doWriteCXSFitFile() {
		try {
			setStatus("Writing CXSFit file ...");
			
			long nanoTime[] = W7XArchiveDBSink.getBestNanotimebase(connectedSource);
			int imageWidth = connectedSource.getImage(0).getWidth();
			int imageHeight = connectedSource.getImage(0).getHeight();
			Long t1NanoL = (Long)getSeriesMetaData("w7x/archive/t1Nano");
			long t1Nano = (t1NanoL != null) ? t1NanoL : Long.MIN_VALUE;
			String dbPath = (String) getSeriesMetaData("w7x/path");			
			String programID = (String) getSeriesMetaData("w7x/programID");
			double exposureTimeSecs = Double.NaN;
						
			int nImages = connectedSource.getNumImages(); 
			if(nanoTime.length > nImages){
				System.err.println("WARNING: Truncating nanoTimes from length "+nanoTime.length+" to numImage="+nImages);
				nanoTime = Arrays.copyOf(nanoTime, nImages);
			}
			
			Object origNanos = getSeriesMetaData("/w7x/archive/nanoTime_orig");
			if(origNanos != null){
				nanoTime = (long[])Mat.fixWeirdContainers(origNanos, true);
			}
			

			String losNames[] = (String[]) getSeriesMetaData("SpecCal/linesOfSight/names");
			double losStart[][] = Mat.transpose((double[][]) getSeriesMetaData("SpecCal/linesOfSight/startPos"));
			double losUVec[][] =  Mat.transpose((double[][]) getSeriesMetaData("SpecCal/linesOfSight/unitVec"));
			double losBeamCrossPos[][][] = (double[][][]) getSeriesMetaData("SpecCal/linesOfSight/activeBeamIntersectionPos");
			double losBeamCrossPosNominal[][] = (double[][]) getSeriesMetaData("SpecCal/linesOfSight/nominalBeamIntersectionPos");
			
			double[][] wavelength = (double[][]) getSeriesMetaData("SpecCal/wavelength");
			if(wavelength == null)
				throw new RuntimeException("No wavelength calibration");
			
			//instrument function width. CXSFit takes a single width per track (no change over wavelength)
			double[] insFuncSigma = new double[imageHeight];
			double[][] insFuncWidth2D = (double[][])getSeriesMetaData("SpecCal/instrumentFuncWidth");
			for(int iY=0; iY < imageHeight; iY++){
				//so just average over whole column for now
				for(int iX = 0; iX < (imageWidth-1); iX++){
					//convert to wavelength use dispersion at this pixel
					double dldx = wavelength[iY][iX+1] - wavelength[iY][iX];
					insFuncSigma[iY] += FastMath.abs(insFuncWidth2D[iY][iX] * dldx); 
				}
				insFuncSigma[iY] /= imageWidth;
			}					
						
			//use the dbPath as the spectrometer name
			String specName = dbPath.replaceAll(".*/(.*)_DATASTREAM.*", "$1");
			specName = specName.replaceAll("_Binned", "");
			
			//make a usable 'shot number'
			double shotIsh = (programID == null) ? 0 : Algorithms.mustParseDouble(programID);
			long shotNumber = nanoTime[0]; //fallback to first nanoTime as shot nunber 
			if(shotIsh > 20100000 && shotIsh < 21000000){
				//looks like YYYYMMdd.sss
				shotNumber = (long)(shotIsh * 1000) - 2000_00_00_000L; // as number YYMMddsss
			}
			
			//find the exposure time
			Object o = getSeriesMetaData("AndorCam/config/ExposureTime");
			if(o != null)
				exposureTimeSecs = ((Number)o).doubleValue(); //already in secs
			o = getSeriesMetaData("AndorV2/config/exposureTime");
			if(o != null)
				exposureTimeSecs = ((Number)o).doubleValue(); //already in secs			
			o = getSeriesMetaData("PICam/config/ExposureTime");
			if(o != null)
				exposureTimeSecs = ((Number)o).doubleValue() / 1000; //in ms
			
			String fileName = ArchiveDBFetcher.defaultInstance().getCacheRoot() + 
								"/cxsfit/dataInfo-w7x-"+specName+"-"+shotNumber+".nc";
			
			
			//Beam energy , current, power
			double beamInfo[][][] = W7XUtil.getBeamInfo(connectedSource, false);
			for(double beamSignal[][] : beamInfo){
				for(int iB=0; iB < beamSignal.length; iB++){
					if(beamSignal[iB] == null)
						beamSignal[iB] = new double[nImages];
					else if(beamSignal[iB].length != nImages){
						beamSignal[iB] = Arrays.copyOf(beamSignal[iB], nImages);
					}
				}
			}
						
OneLiners.makePath(fileName);
			
			setStatus("Writing CXSFit file: Preparing NetCDF ...");
			
			NhFileWriter ncdfW = new NhFileWriter(fileName, NhFileWriter.OPT_OVERWRITE);
			
			NhGroup rootGroup = ncdfW.getRootGroup();
			
			NhDimension dTime = rootGroup.addDimension("time", nanoTime.length);
			NhDimension dWidth = rootGroup.addDimension("imageWidth", imageWidth);
			NhDimension dHeight = rootGroup.addDimension("imageHeight", imageHeight);
			NhDimension dCoord = rootGroup.addDimension("coord", 3);
			
			NhDimension dBeamIdx = rootGroup.addDimension("beamIndex", beamInfo[0].length);
			//NhDimension dBeamI = rootGroup.addDimension("beamCurrent", beamInfo[1].length);
			//NhDimension dBeamP = rootGroup.addDimension("beamPower", beamInfo[2].length);
			
			
			NhVariable vNanoTime = rootGroup.addVariable("nanoTime", NhVariable.TP_LONG, new NhDimension[]{ dTime }, null, null, 0);
			NhVariable vWavelength = rootGroup.addVariable("wavelength", NhVariable.TP_DOUBLE, new NhDimension[]{ dHeight, dWidth }, null, null, 0);
			NhVariable vIntensity = rootGroup.addVariable("intensity", NhVariable.TP_DOUBLE, new NhDimension[]{ dHeight, dWidth }, null, null, 0);
			NhVariable vInstrumentFuncSigma = rootGroup.addVariable("instrumentFunctionSigma", NhVariable.TP_DOUBLE, new NhDimension[]{ dHeight }, null, null, 0);
			
			NhVariable vLosName = rootGroup.addVariable("losName", NhVariable.TP_STRING_VAR, new NhDimension[]{ dHeight }, null, null, 0);
			NhVariable vLOSStart = rootGroup.addVariable("losStart", NhVariable.TP_DOUBLE, new NhDimension[]{ dCoord, dHeight  }, null, null, 0);
			NhVariable vBeamCrossPos = rootGroup.addVariable("beamCrossPos", NhVariable.TP_DOUBLE, new NhDimension[]{ dTime, dHeight, dCoord  }, null, null, 0);
			NhVariable vBeamCrossPosNominal = rootGroup.addVariable("beamCrossPosNominal", NhVariable.TP_DOUBLE, new NhDimension[]{ dHeight, dCoord  }, null, null, 0);
			
			NhVariable vBeamEnergy = rootGroup.addVariable("beamEnergy", NhVariable.TP_DOUBLE, new NhDimension[]{ dBeamIdx, dTime }, null, null, 0);
			NhVariable vBeamCurrent = rootGroup.addVariable("beamCurrent", NhVariable.TP_DOUBLE, new NhDimension[]{ dBeamIdx, dTime }, null, null, 0);
			NhVariable vBeamPower = rootGroup.addVariable("beamPower", NhVariable.TP_DOUBLE, new NhDimension[]{ dBeamIdx, dTime }, null, null, 0);
			
			rootGroup.addAttribute("dbPath", NhVariable.TP_STRING_VAR, dbPath);
			rootGroup.addAttribute("t1Nano", NhVariable.TP_LONG, t1Nano);
			rootGroup.addAttribute("exposureTimeSecs", NhVariable.TP_DOUBLE, exposureTimeSecs);
			
			ncdfW.endDefine();
			
			setStatus("Writing CXSFit file: Writing NetCDF ...");
			
			vNanoTime.writeData(null, nanoTime);
			vWavelength.writeData(null, wavelength);
			vIntensity.writeData(null, getSeriesMetaData("SpecCal/intensity"));
			vInstrumentFuncSigma.writeData(null, insFuncSigma);
			
			vLosName.writeData(null, losNames);
			vLOSStart.writeData(null, losStart);
			vBeamCrossPos.writeData(null, losBeamCrossPos);
			vBeamCrossPosNominal.writeData(null, losBeamCrossPosNominal);
			

			vBeamEnergy.writeData(null, beamInfo[0]);			
			vBeamCurrent.writeData(null, beamInfo[1]);
			vBeamPower.writeData(null, beamInfo[2]);
			
			ncdfW.close();
			
			System.out.println("Written CXSFit OK " + nImages + " images.");
			setStatus("CXSFit write OK " + nImages + " images.");
			
		} catch (NhException err) {
			setStatus("Failed to write CXSFit data:" + err.getMessage());
			err.printStackTrace();
		}
		
		updateAllControllers();
	}
}
