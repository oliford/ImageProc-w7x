package imageProc.proc.spec;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imageProc.core.ImageProcUtil;
import imageProc.proc.spec.SpecCalConfig.TrackConfig;
import imageProc.proc.spec.SpecCalConfig.TrackConfig.ParamType;
import imageProc.proc.spec.SpecCalProcessor.CalType;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class ParameterisedCurveSWTController {
	private final static String colNames[] = { "i", "p1", "p2", "p3", "p4" };

	private Group paramGroup;
	
	private CalType calType;
	
	private Table table; 
	private TableEditor tableEditor;
	private TableItem tableItemEditing = null;
	private Text tableEditBox = null;	
	
	private SpecCalProcessor proc;
	
	private Button swtParamNone;
	private Button swtParamLinearRadio;
	private Button swtParamCubicRadio;
	private Button swtParamPolynomialRadio;
	
	private Button swtCopyButton;
	private Button swtPasteButton;
	private Button swtFastSpecButton;
	
	private Spinner swtNumParamsSpinner;
	
	private SpecCalSWTController ctrl;
	
	public ParameterisedCurveSWTController(Composite parent, int style, SpecCalProcessor proc, SpecCalSWTController parentController, CalType calType) {
		this.proc = proc;
		this.ctrl = parentController;
		this.calType = calType;
			
		paramGroup = new Group(parent, style);
		paramGroup.setText("Param");
		paramGroup.setLayout(new GridLayout(6, false));
		ImageProcUtil.addRevealWriggler(paramGroup);

		Label lPT = new Label(paramGroup, SWT.NONE); lPT .setText("Parameterisation type:");
		
		swtParamNone = new Button(paramGroup, SWT.RADIO);
		swtParamNone.setText("None");
		swtParamNone.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtParamNone.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		swtParamLinearRadio = new Button(paramGroup, SWT.RADIO);
		swtParamLinearRadio.setText("Linear");
		swtParamLinearRadio.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtParamLinearRadio.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));		
		
		swtParamCubicRadio = new Button(paramGroup, SWT.RADIO);
		swtParamCubicRadio.setText("Cubic");
		swtParamCubicRadio.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtParamCubicRadio.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
		
		swtParamPolynomialRadio = new Button(paramGroup, SWT.RADIO);
		swtParamPolynomialRadio.setText("Polynomial");
		swtParamPolynomialRadio.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		swtParamPolynomialRadio.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		
		swtCopyButton = new Button(paramGroup, SWT.PUSH);
		swtCopyButton.setText("Copy");
		swtCopyButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { copyButtonEvent(event); } });
		swtCopyButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		
		swtPasteButton = new Button(paramGroup, SWT.PUSH);
		swtPasteButton.setText("Paste");
		swtPasteButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pasteButtonEvent(event); } });
		swtPasteButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		
		if(calType == CalType.WAVELEN){
			swtFastSpecButton = new Button(paramGroup, SWT.PUSH);
			swtFastSpecButton.setText("Set from FastSpec");			
			swtFastSpecButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { fastSpecButtonEvent(event); } });
			swtFastSpecButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		}else{
			Label lFS = new Label(paramGroup, SWT.NONE); lFS.setText("     ");
		}
		

		Label lNP = new Label(paramGroup, SWT.NONE); lNP.setText("Number of points / coefficients");
		lNP.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
		swtNumParamsSpinner = new Spinner(paramGroup, SWT.NONE);
		swtNumParamsSpinner.setValues(3, 1, Integer.MAX_VALUE, 0, 1, 10);
		swtNumParamsSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
		swtNumParamsSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

		table = new Table(paramGroup, SWT.BORDER | SWT.MULTI);		
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn cols[] = new TableColumn[colNames.length];
		for (int i = 0; i < colNames.length; i++) {
			cols[i] = new TableColumn(table, SWT.BORDER | SWT.V_SCROLL);
			cols[i].setText(colNames[i]);
		}

		update();
		
		for (int i = 0; i < colNames.length; i++)
			cols[i].pack();

		table.addListener(SWT.MouseDoubleClick, new Listener() { 
			@Override
			public void handleEvent(Event event) {
				for (int i = 0; i < colNames.length; i++)
					cols[i].pack();
			}
		});
		

		tableEditor = new TableEditor(table);
		tableEditor.horizontalAlignment = SWT.LEFT;
		tableEditor.grabHorizontal = true;
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		table.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { tableMouseDownEvent(event); } });
	}
	
	
	protected void copyButtonEvent(Event event) {
		TrackConfig track = ctrl.getSelectedTrack();
		ParamType paramType = track.getParamType(calType);
		double params[][] = track.getParams(calType);
		
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.create(); 

		String json = gson.toJson(new Object[]{ paramType, params });
		
		Clipboard cb = new Clipboard(getSWTGroup().getDisplay());
		TextTransfer tt = TextTransfer.getInstance();
		cb.setContents(new Object[]{ json }, new Transfer[]{ tt });
		cb.dispose();
	}

	protected void pasteButtonEvent(Event event) {
		Clipboard cb = new Clipboard(getSWTGroup().getDisplay());

		String plainText = (String)cb.getContents(TextTransfer.getInstance());

		System.out.println(plainText);

		cb.dispose();
				
		try{
			Gson gson = new Gson();
			
			Object[] thing = (Object[])gson.fromJson(plainText, Object[].class);
		
			ParamType paramType = ParamType.valueOf((String)thing[0]);
			
			ArrayList lst = (ArrayList)thing[1];
			
			
			double[][] params = new double[2][];
			for(int i=0; i < 2; i++){
				ArrayList lst2 = (ArrayList)lst.get(i);
				params[i] = new double[lst2.size()];
				for(int j=0; j < lst2.size(); j++){
					params[i][j] = ((Number)lst2.get(j)).doubleValue();
				}
			}
			
			TrackConfig track = ctrl.getSelectedTrack();
			track.setParamType(calType, paramType);
			
			
			//track.setParams(calType, params);
			track.wavelenParams = params;
		/*switch(calType){
			case WAVELEN: track.wavelenParams = params; break;
			case INTENSITY: track.intensityParams = params; break;
			case INSTRUMENT: track.instrumentFunctionWidthParams = params;  break;
		}*/
			
		}catch(Exception err){
			//throw new RuntimeException("Error interpreting pasted text. Expecting json for Object[]{ ParamTypeString, double[][] }. Got: '" + plainText+ "'.", err);
			System.err.println("Error interpreting pasted text. Expecting json for Object[]{ ParamTypeString, double[][] }. Got: '" + plainText+ "'. Error was: " + err.getMessage());
		}
		
		proc.updateAllControllers();
	}
	
	private void fastSpecButtonEvent(Event event) {
		TrackConfig track = ctrl.getSelectedTrack();
		
		proc.setFromFastSpec(track);

	}

	protected void settingsChangedEvent(Event event) {
		TrackConfig track = ctrl.getSelectedTrack();
		if(track == null)
			return;
		
		ParamType paramType = swtParamPolynomialRadio.getSelection() ? ParamType.POLYNOMIAL
							: (swtParamCubicRadio.getSelection() ? ParamType.KNOTS_CUBIC 
																 : ParamType.KNOTS_LINEAR);
		track.setParamType(calType, paramType);		
		track.setNumParams(calType, swtNumParamsSpinner.getSelection());
		
		update();
	}


	/**
	 * Copied from 'Snippet123' Copyright (c) 2000, 2004 IBM Corporation and
	 * others. [ org/eclipse/swt/snippets/Snippet124.java ]
	 */
	private void tableMouseDownEvent(Event event) {
		
		Rectangle clientArea = table.getClientArea();
		Point pt = new Point(clientArea.x + event.x, event.y);
		int index = table.getTopIndex();
		while (index < table.getItemCount()) {
			boolean visible = false;
			final TableItem item = table.getItem(index);
			for (int i = 0; i < table.getColumnCount(); i++) {
				Rectangle rect = item.getBounds(i);
				if (rect.contains(pt)) {
					final int column = i;
					if(column <= 0) //not editable
						return;
					if (tableEditBox != null) {
						tableEditBox.dispose(); // oops, one left over
					}
					tableEditBox = new Text(table.getParent(), SWT.NONE);
					tableItemEditing = item;
					Listener textListener = new Listener() {
						public void handleEvent(final Event e) {
							switch (e.type) {
							case SWT.FocusOut:
								tableColumnModified(item, column, tableEditBox.getText());
								tableEditBox.dispose();
								tableEditBox = null;
								tableItemEditing = null;
								break;
							case SWT.Traverse:
								switch (e.detail) {
								case SWT.TRAVERSE_RETURN:
									tableColumnModified(item, column, tableEditBox.getText());
									// FALL THROUGH
								case SWT.TRAVERSE_ESCAPE:
									tableEditBox.dispose();
									tableEditBox = null;
									tableItemEditing = null;
									e.doit = false;
								}
								break;
							}
						}
					};
					tableEditBox.addListener(SWT.FocusOut, textListener);
					tableEditBox.addListener(SWT.Traverse, textListener);
					tableEditor.setEditor(tableEditBox, item, i);
					tableEditBox.setText(item.getText(i));
					tableEditBox.selectAll();
					tableEditBox.setFocus();
					// hacks for SWT4.4 (under linux GTK at least)
					// Textbox won't display if it's a child of the table
					// so we make it a child of the table's parent, but now need
					// to adjust the location
					{
						tableEditBox.moveAbove(table);
						final Point p0 = tableEditBox.getLocation();
						Point p1 = table.getLocation();
						p0.x += p1.x - clientArea.x;
						p0.y += p1.y;// + editBox.getSize().y;
						// p0.x = pt.x + p1.x;
						// p0.y = pt.y + p1.y;
						tableEditBox.setLocation(p0);
						tableEditBox.addListener(SWT.Move, new Listener() {
							@Override
							public void handleEvent(Event event) {
								tableEditBox.setLocation(p0); // TableEditor keeps
															// moving it to
															// relative to the
															// table, so move it
															// back
							}
						});
					}
					return;
				}
				if (!visible && rect.intersects(clientArea)) {
					visible = true;
				}
			}
			if (!visible)
				return;
			index++;
		}
		
		proc.updateAllControllers();
	}
	
	private void tableColumnModified(TableItem item, int column, String text) {
		TrackConfig track = ctrl.getSelectedTrack();
		if(track == null)
			return;
		
		int pIdx = Integer.parseInt(item.getText(0));
		
		double[][] params = track.getParams(calType);
		
		params[column-1][pIdx] = Algorithms.mustParseDouble(text);
		
		update();
	}
		
	public void update(){
		table.removeAll();
		
		TrackConfig track = ctrl.getSelectedTrack();
		if(track == null){
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(" -- No track selected -- ");
			swtParamNone.setSelection(false);
			swtParamLinearRadio.setSelection(false);
			swtParamCubicRadio.setSelection(false);
			swtParamPolynomialRadio.setSelection(false);
			swtNumParamsSpinner.setSelection(0);
			
			swtParamNone.setEnabled(false);
			swtParamLinearRadio.setEnabled(false);
			swtParamCubicRadio.setEnabled(false);
			swtParamPolynomialRadio.setEnabled(false);
			swtNumParamsSpinner.setEnabled(false);
			table.setEnabled(false);
			return;
		}
		
		ParamType paramType = track.getParamType(calType);
		swtParamNone.setSelection(paramType == ParamType.NONE);
		swtParamLinearRadio.setSelection(paramType == ParamType.KNOTS_LINEAR);
		swtParamCubicRadio.setSelection(paramType == ParamType.KNOTS_CUBIC);
		swtParamPolynomialRadio.setSelection(paramType == ParamType.POLYNOMIAL);
		
		double params[][] = track.getParams(calType);
		if(params == null)
			params = new double[2][0];
		
		swtNumParamsSpinner.setSelection(params[0].length);
		
		for(int i=0; i < params[0].length; i++){
			TableItem item = new TableItem(table, SWT.NONE);
			
			item.setText(0, Integer.toString(i));
			for(int j=0; j < params.length; j++){
				item.setText(j+1, Double.toString(params[j][i]));
			}
		}
		
		swtParamNone.setEnabled(true);
		swtParamLinearRadio.setEnabled(true);
		swtParamCubicRadio.setEnabled(true);
		swtParamPolynomialRadio.setEnabled(true);
		swtNumParamsSpinner.setEnabled(true);
		table.setEnabled(true);
	}

	public Control getSWTGroup() { return paramGroup; }
	
	
}
