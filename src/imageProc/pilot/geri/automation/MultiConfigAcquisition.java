package imageProc.pilot.geri.automation;

import imageProc.pilot.geri.GeriPilot;

public class MultiConfigAcquisition extends StandardAcquisition {

	public MultiConfigAcquisition(GeriPilot pilot) {
		super(pilot);
	}

	@Override
	public Class getConfigType() { return MultiConfigAcquisitionConfig.class; }		
	@Override
	public MultiConfigAcquisitionConfig getConfig() { 
		return (MultiConfigAcquisitionConfig)super.getConfig(); 
	}
}
