package imageProc.pilot.geri.automation;

import java.util.ArrayList;

import org.eclipse.swt.graphics.Point;

import imageProc.pilot.geri.ImageProcResource.GeriState;

public class StandardAcquisitionConfig {
	
	/** When to begin (the proper) capture after the GERI IDLE->INIT transition.
	 * If set ( >  0), then the capture will be initialised and aborted
	 * and then re-entered at this point */
	public long programStartToCaptureStartMS = -1;
	
	/** When to begin (the proper) acquisition after the GERI IDLE->INIT transition*/
	public long programStartToAcquireStartMS = 60000;

	/** Run a short capture+acquire at t=0 and abort it in order to check that everything is ready */
	public boolean precapture = false;
	
	/** If not already acquiring, start capture+acquire if GERI state changes to RUN.
	 * Typically the acquire start is timed with programStartToAcquireStartMS to slightly
	 * before the RUN state begins, but if it isn't this will pick it up */
	public boolean acquireStartOnRun = true;

	/** Do a capture+acquire run of this length immediately when the camera is ready to check that it is really working.
	 * Then abort, start capture again and then start acquiring properly */
	public int preAcquireDurationMS = 3000;
	
	/** At which GERI stage to begin streaming. Mostly RUN or POST. Maybe IDLE.*/
	public GeriState beginStreamingInState = GeriState.POST;
	
	/** How long after state is (beginStreamingInState) to start streaming */
	public int beginStreamingDelay = 0;
	
	/** Begin processing modules when given GERI state is reached. Usually POST */
	public GeriState beginProcessingInState = GeriState.POST;
	
	/** How long after state is (beginProcessingInState) to start processing */
	public int beginProcessingDelay = 0;
	
	/** Only begin processing once the save operation is complete */
	public boolean processingOnlyAfterSaveComplete = false;
	
	public static enum SaveCondition {
		NEVER,
		IF_STREAMING_FAILS,
		ALWAYS,;

		public static String[] names() {
			ArrayList<String> strs = new ArrayList<String>();
			for(SaveCondition s : SaveCondition.values()) 
				strs.add(s.name());
			return strs.toArray(new String[SaveCondition.values().length]);
		}

	}
	
	/** When to save the data using the W7XArchiveDBSink module */
	public SaveCondition saveOn = SaveCondition.IF_STREAMING_FAILS;

	/** Abort acqusition when the given state is reached (typically POST or IDLE) */
	public GeriState abortAcquireOnState = GeriState.IDLE;
}
