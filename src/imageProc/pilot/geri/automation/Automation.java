package imageProc.pilot.geri.automation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import imageProc.core.AcquisitionDevice;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.database.geriStream.GeriStreamSink;
import imageProc.database.geriStream.ImageProcImageProvider.ImagePass;
import imageProc.database.geriStream.ImageProcImageProvider.ImageStatus;
import imageProc.pilot.common.PilotStatusComm.StepStatus;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.geri.GeriPilotConfig;
import imageProc.pilot.geri.ImageProcResource.GeriState;
import w7x.archive.ArchiveDBFetcher;

/** 
 * Base class for automation 'scripts' including some common operations.
 * Generally this serves to provide as-simple-as-possible derived classes
 * to effectively script the automation of specific diagnostics
 * 
 * 
 * Events:
 *    GERI state change
 *    WebAPI program start detect
 *    day start/end
 *    generic timer
 *    master abort
 *    arb. failures
 *    
 *    
 * 
 * @author specgui
 */
public abstract class Automation {
	
	private static ArrayList<Class> automationTypes = new ArrayList<Class>();
	public static List<Class> getAutomationTypes() { return automationTypes; }
	static {
		automationTypes.add(StandardAcquisition.class);
		automationTypes.add(MultiConfigAcquisition.class);
	}
	
		
	protected GeriPilot pilot;
	
	protected Thread automationThread = null;
	
	/** General status flag that everything is as it should be.
	 * Used for reporting to codac via GERI state and to flight controller
	 * 
	 * We start with true, since we don't know anything is wrong until we first try to do something
	 */
	protected boolean isOK = true;
	
		
	public Automation(GeriPilot pilot) {
		this.pilot = pilot;
		setup();
	}

	/** Called at class initialisation,  */
	public void setup() { }
	
	public GeriPilotConfig pilotConfig() { return pilot.getConfig(); }
	
	/** Finds a specific module in the source/sink tree 
	 * @param <T>*/
	protected <T> T module(Class<T> cls) {
		ImgSource src = pilot.getConnectedSource();
		while(src instanceof ImgSink) 
			src = ((ImgSourceOrSinkImpl)src).getConnectedSource();
		T ret = module(src, cls);
		if(ret == null)
			throw new RuntimeException("Unable to find module of type '" + cls.getName() + "'");
		return ret;
	}
	
	private <T> T module(ImgSource src, Class<T> cls) {
		
		for(ImgSink sink : src.getConnectedSinks()) {
			if(cls.isInstance(sink))
				return (T)sink;
			
			if(sink instanceof ImgSource) {
				T ret = module((ImgSource)sink, cls);
				if(ret != null)
					return ret;
			}
		}
		return null;
	}
	
	/** Returns the root image source as an acquisition device */
	public AcquisitionDevice acq() { return acquisitionDevice(); }
	
	/** Returns the root image source as an acquisition device */
	public AcquisitionDevice acquisitionDevice() {
		ImgSource src = pilot.getConnectedSource(); 
		while(src instanceof ImgSink) 
			src = ((ImgSourceOrSinkImpl)src).getConnectedSource();
		
		if(!(src instanceof AcquisitionDevice))
			throw new RuntimeException("Root source is not an AcquisitionDevice");
		return (AcquisitionDevice)src;
	}

	/** Wait until the given timestamp (probably accurate to a few ms)*/
	protected void waitUntil(long timestampNS, double offsetSecs) {
		waitUntil(timestampNS, offsetSecs, null);		
	}
	
	/** Wait until the given timestamp (probably accurate to a few ms)*/
	protected void waitUntil(long timestampNS, double offsetSecs, BooleanSupplier b) {
		waitUntil(timestampNS + ((long)(offsetSecs * 1000.0) * 1_000_000L), b);
	}

	/** Wait until the given number of seconds after the given timestamp (probably accurate to a few ms) */
	protected void waitUntil(long timestampNS) {
		waitUntil(timestampNS, null);
	}
	
	/** Wait until the given number of seconds after the given timestamp (probably accurate to a few ms) */
	protected void waitUntil(long timestampNS, BooleanSupplier b) {
		long timestampMS = timestampNS / 1_000_000L;
		try {
			while(System.currentTimeMillis() < timestampMS) {
				if(b != null && b.getAsBoolean())
					return;
				
				Thread.sleep(10);
			};			
		} catch (InterruptedException e) {
			throw new RuntimeException("Interrupted in automation wait.");
		}
	}
	
	protected void delay(double delaySecs) {
		long delayMS = (long)(delaySecs * 1000.0);
		try {
			Thread.sleep(delayMS, 0);
		} catch (InterruptedException e) {
			throw new RuntimeException("Interrupted in automation wait.");
		}
	}
	
	protected void log(String str) {
		pilot.getLog().write("Automation: " + str);
	}
	
	protected void setGeriOK(boolean isOK) {
		this.isOK = isOK;
		pilot.updateAllControllers();
	}
	
	/** Called from pilot when imageProc global abort trigger is fired (e.g. the big abort button is pressed)
	 * Cancel all running tasks. */
	public void triggerAbort() {
		synchronized (tasks) {
			for(Future f : tasks)
				f.cancel(true);
		}
	}
	
	public int getNumberOfRunningTasks() {
		synchronized (tasks) {
			tasks.removeIf(x -> x.isDone());
			return tasks.size();
		}
	}
	
	/** Returns true if any tasks are active, i.e. the automation is doing something or waiting internally for something to complete. */
	public boolean isActive() {
		return getNumberOfRunningTasks() > 0;
	}
	
	/** Handles on tasks that were submitted */
	public ArrayList<Future> tasks = new ArrayList<>();
	
	public void stateChange(long timestamp, GeriState old, GeriState state) {
		ExecutorService pool = ImageProcUtil.getThreadPool();
		tasks.add(pool.submit(() -> stateChangeWorker(timestamp, old, state)));		
	}
	
	private void stateChangeWorker(long timestamp, GeriState old, GeriState state) {
		if(!pilot.getConfig().automationEnable)
			return;
		
		try {		
			//Not really sure why we should do all this, but ok...
			if(state == GeriState.IDLE) {
				if(old == GeriState.RUN)
					geri_post(timestamp);		
				geri_idle(timestamp);
				if(old == GeriState.POST)
					geri_post_to_idle(timestamp);
				
			}else if(state == GeriState.INIT) {
				geri_init(timestamp);
				if(old == GeriState.IDLE)
					geri_idle_to_init(timestamp);
				
			}else if(state == GeriState.PRE) {
				geri_pre(timestamp);
				if(old == GeriState.INIT)
					geri_init_to_pre(timestamp);
				if(old == GeriState.RUN)
					geri_run_to_pre(timestamp);
				
			}else if(state == GeriState.RUN) {
				geri_run(timestamp);
				if(old == GeriState.PRE)
					geri_pre_to_run(timestamp);
				
			}else if(state == GeriState.POST) {
				geri_post(timestamp);
				if(old == GeriState.PRE)
					geri_run_to_post(timestamp);
			}
			
			
		}catch(Exception err) {
			err.printStackTrace();
			log("ERROR in automation on GERI state change: " + err.getMessage());
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Uncaught exception in GERI state change", err);
			isOK = false;
			pilot.getStatusComm().setPilotStatus(StepStatus.ERROR);
		}
		
		pilot.updateAllControllers();
	}
	
	/** Geri state state entry (from any state) */	
	public void geri_idle(long timestamp) { } 
	public void geri_init(long timestamp) { } 
	public void geri_pre(long timestamp) { } 
	public void geri_run(long timestamp) { } 
	public void geri_post(long timestamp) { } 
	
	/** Normal GERI state state changes */	
	public void geri_idle_to_init(long timestamp) { } 
	public void geri_init_to_pre(long timestamp) { } 
	public void geri_pre_to_run(long timestamp) { } 
	public void geri_run_to_post(long timestamp) { }
	public void geri_run_to_pre(long timestamp) { }
	public void geri_post_to_idle(long timestamp) { }

	public Object getConfig() {
		GeriPilotConfig pCfg = pilotConfig();
		
		//if(pCfg.automationConfig == null)
			//return null;
		
		//is already the right type
		if(getConfigType().isInstance(pCfg.automationConfig))
			return pCfg.automationConfig;
			
		//didn't get converted on load
		if(pCfg.automationConfig instanceof LinkedTreeMap) {
			//JSON decompiled thing from GSON that  didn't know what type it was supposed to be, convert to JSON and back again
			Gson gson = new GsonBuilder()
					.serializeSpecialFloatingPointValues()
					.serializeNulls()
					.setPrettyPrinting()
					.create();

			String jsonString = gson.toJson(pCfg.automationConfig);			
			pCfg.automationConfig = gson.fromJson(jsonString, getConfigType());
			return pCfg.automationConfig;
		}
		
		//otherwise wipe it and start with a new one
		if(pCfg.automationConfig != null)
			Logger.getLogger(this.getClass().getName()).warning("config.automationConfig was a " + pCfg.automationConfig.getClass().getSimpleName() +
					", clearing it and replacing it with a new " + getConfigType().getSimpleName());
		
		try {
			pilotConfig().automationConfig = getConfigType().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			pilotConfig().automationConfig = null;
		}
		return pCfg.automationConfig;
	

	}
	

	/** Wait for the streaming and acquisition to complete and update the pilot status as necessary.
	 * Timeout specified by pilot.getConfig().streamWait...TimeoutMS
	 * 
	 * @param geriStream The streamer that we should wait for and check
	 * @param programID The ID of the W7-X program that was acquired.
	 * @return true if every successful, false if something failed or timed out  */
	protected boolean waitForStreamingComplete(GeriStreamSink geriStream, String programID) {
		try {
			
			String lastProgramID = programID; 
						
			pilot.getStatusComm().setSaveStatus(programID, StepStatus.ACTIVE);
			
			long tWaitStart = System.currentTimeMillis();
			int nAcquiredLast = 0, nWrittenLast = 0, nSentLast = 0;
			long tLastChange = tWaitStart;
			boolean complete = false;
			
			GeriPilotConfig config = pilot.getConfig();
			
			long lastAchiveCheck = 0;			
			do {
				
				int nAcquired = 0, nWritten = 0, nSent = 0;
				ArrayList<ImagePass> imageHistory = geriStream.getImageHistory();
				for(ImagePass pass : imageHistory) { 
					for(int i=0; i < pass.status.length; i++) {
						switch(pass.status[i]) {
							case CONFIRMED:
								nWritten++;
							case SENT:
								nSent++;
							case CHANGED:
							case NO_TIMESTAMP:
								nAcquired++;
							default:
						}
					}
				}
				
				boolean acquiring;
				switch(acq().getAcquisitionStatus()) {
					case completeOK: 
					case aborted:
						pilot.getStatusComm().setAcquireStatus(programID, StepStatus.SUCCESS);
						acquiring = false;
						break;
					case errored:
					case notInitied:
						pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
						acquiring = false;
						break;
					case awaitingHardwareTrigger:
					case awaitingSoftwareTrigger:
						pilot.getStatusComm().setAcquireStatus(programID, StepStatus.WAITING);
						acquiring = true;
						break;
					case init:
					case capturing:
					default:
						pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ACTIVE);
						acquiring = true;
						break;					
				}
	
				long t1 = System.currentTimeMillis();
				log("Post wait: t = POST+"+String.format("%5.2f", ((t1 - tWaitStart)/1000.0))+"s: "
												+ "acquiring = " + acquiring +", "
												+ "datastream = " + geriStream.isDataStreamingActive() + ", "
												+ "parlogstream = " + geriStream.isParlogStreamingActive() + ", "
												+ "nWritten=" + nWritten + ", "
												+ "nSent=" + nSent + ", "
												+ "nAcquired=" + nAcquired);
				
				boolean checkDisabled = geriStream.getConfig().datastreamName == null 
										|| geriStream.getConfig().datastreamName.length() == 0;
				
				if(!acquiring && (nWritten >= nAcquired || (checkDisabled && nSent >= nAcquired))) {
					complete = true;
					pilot.getStatusComm().setSaveStatus(programID, StepStatus.SUCCESS);
					log("Acquiring complete and all images confirmed in ArchiveDB");
					break;
				}else {
					pilot.getStatusComm().setSaveStatus(lastProgramID, StepStatus.ACTIVE);
				}
				
				//have the image counts changed?
				if(nAcquired == nAcquiredLast && nSent == nSentLast && nWritten == nWrittenLast) {
					//if not, timeout if its been too long like this
					if((t1 - tLastChange) > config.streamWaitIdleTimeoutMS) {
						log("Timeout idling in acquire/stream wait loop ("+(config.streamWaitIdleTimeoutMS/1000.0)
								+"), giving up and assuming remaining images were lost.");
						complete = false;
						break;
					}
				}else {
					tLastChange = System.currentTimeMillis();
				}
				nAcquiredLast = nAcquired;
				nSentLast = nSent;
				nWrittenLast = nWritten;
				
				//give up at timeout or if the state changes again
				if((t1 - tWaitStart) >= config.streamWaitTimeoutMS) {
					log("Timeout (" + config.streamWaitTimeoutMS/1000.0 + "s) while waiting for acquisition and streaming to complete.");
					complete = false;
					break;
				}
				
				if((pilot.getGERIState() != GeriState.POST && pilot.getGERIState() != GeriState.IDLE)){
					log("State changed to " + pilot.getGERIState() + " while waiting for acquisition and streaming to complete.");
					complete = false;
					break;
				}
				
				//check which images have made it to the archive 
				if((System.currentTimeMillis() - lastAchiveCheck) > pilot.getConfig().archiveCheckPeriodMS){
					try {
						geriStream.checkDataInArchive();
					}catch(Throwable err) {
						log("Exception when checking archive image write:" + err.getMessage());
						Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Exception checking archive", err);
					}
					lastAchiveCheck = System.currentTimeMillis();
				}
				
				try {
					Thread.sleep(config.streamWaitCheckPeriod);
				} catch (InterruptedException err) {
					throw new RuntimeException(err);
				}
				
			}while(true);
			
			if(complete) {
				pilot.getStatusComm().setProcessStatus(lastProgramID, StepStatus.SUCCESS);
				pilot.getStatusComm().setPilotStatus(StepStatus.WAITING);
				
			}else {
				log("Error, timeout or state changed while waiting for acqusition and streaming to complete, marking failure,"
						+ " stopping acquisition and abandoning wait. Streaming might continue for a while.");
			
				if(acq().getAcquisitionStatus() != Status.completeOK && acq().getAcquisitionStatus() != Status.aborted) {
					pilot.getStatusComm().setAcquireStatus(lastProgramID, StepStatus.ERROR);
					acq().abort();
				}
				pilot.getStatusComm().setSaveStatus(lastProgramID, StepStatus.ERROR);
				pilot.getStatusComm().setProcessStatus(lastProgramID, StepStatus.NOT_STARTED);			
			}
			
			
			return complete;
			
		}catch(Throwable err) {
			log("Exception in GERI POST wait loop: " + err.getMessage());
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Exception in POST wait loop", err);
			return false;
			
		}finally {
			//System.out.println("Why am I here?");
		}
	}
	
	/** Gets the program ID from WebAPI for a given timestamp,
	 * or returns {date}.t_{time} if it cannot be fetched.
	 * 
	 * @param timestamp
	 * @return
	 */
	protected String getProgramID(long timestamp) {
		long progInfo[] = null;
		try {
			//progInfo = ArchiveDBFetcher.checkForNewProgramID(timestamp - 2_000_000_000L, 100);
			progInfo = ArchiveDBFetcher.checkForNewProgramID(timestamp - 2_000_000_000L, 
					100, 
					pilotConfig().programIDRequestTimeout,
					pilotConfig().webapiProgramTriggerAddress);

			if(progInfo == null)
				log("progInfo = null");
			else {
				log("progInfo = ["+progInfo.length+"]");
				for(int i=0; i < progInfo.length; i++)
					log("progInfo["+i+"] = " + progInfo[i]);
			}
		}catch(RuntimeException err) {
			progInfo = null;
			log("Error getting programID from webapi: " + err.getMessage());
			err.printStackTrace();
		}
		
		if(progInfo != null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));			
			String dateStr = sdf.format(new Date());
			
			return String.format("%s.%03d", dateStr, progInfo[0]); 

		}else {
			return ImageProcUtil.nanotimeLikeProgramID(timestamp);
		}
	}
	
	/** If the given program ID is the temporary .t_ style, try again to get the real one
	 * and report the appropriate change to the pilotStatus
	 * 
	 * @param programID
	 * @return
	 */
	protected String checkProgramID(String progID, long timestamp) {
		if(!progID.contains("\\.t_"))
			return progID; //already good
		
		//at program start we couldn't get a real programID, try again now
		String newProgID = getProgramID(timestamp);
		
		if(newProgID.equals(progID))
			return newProgID;
		
		//kill status of the temporary fake program
		pilot.getStatusComm().setAcquireStatus(progID, StepStatus.NOT_STARTED);
		pilot.getStatusComm().setSaveStatus(progID, StepStatus.NOT_STARTED);
		pilot.getStatusComm().setProcessStatus(progID, StepStatus.NOT_STARTED);
		
		//the new acq and save get set in a second 
		pilot.getStatusComm().setProcessStatus(newProgID, StepStatus.NOT_STARTED);
		
		return newProgID;
	}


	public abstract Class getConfigType();

	public boolean isOK() { return isOK; }

	private Future processingTask = null;

	/** Starts a task (in other thread) that begins the processing of data by all modules */ 
	protected void beginProcessing(String programID) {
		if(processingTask != null && !processingTask.isCancelled() && !processingTask.isDone()) {
			log("Starting processing, but processing is already in progress.");
			return;
		}
		
		ExecutorService pool = ImageProcUtil.getThreadPool();
		
		processingTask = pool.submit(() -> processChain(programID));
		tasks.add(processingTask); //keep track of this in automation's task list
		
		log("Processing started");
	}	

	private void processChain(String programID) {
		
		try {
			pilot.processChain(programID, true);
		} catch (InterruptedException e) {
			log("Processing interrupted.");
		}
	}

	/** Waits for a started processing chain to complete */
	protected void waitForProcessingToComplete() {
		log("Waiting for processing to complete");
		
		while(processingTask != null && !processingTask.isCancelled() && !processingTask.isDone()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log("Interrupted while waiting for processing to complete.");
				return;
			}
		}
		
				
	}
}
