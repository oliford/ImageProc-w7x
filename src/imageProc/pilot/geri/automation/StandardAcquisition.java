package imageProc.pilot.geri.automation;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jfree.util.Log;

import imageProc.core.AcquisitionDevice;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.ImageProcUtil;
import imageProc.database.geriStream.GeriStreamSink;
import imageProc.database.geriStream.ImageProcImageProvider.ImageStatus;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSink;
import imageProc.pilot.common.PilotStatusComm.StepStatus;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.geri.GeriPilotConfig;
import imageProc.pilot.geri.ImageProcResource;
import imageProc.pilot.geri.ImageProcResource.GeriState;
import imageProc.pilot.geri.automation.StandardAcquisitionConfig.SaveCondition;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.w7x.W7XUtil;
import w7x.archive.ArchiveDBFetcher;

/** Default automation 'script' for standard acquire + save sequence.
 * Begin acquisition at specified time after IDLE->INIT transition 
 *  
 *  */
public class StandardAcquisition extends Automation {	
	
	public StandardAcquisition(GeriPilot pilot) { 
		super(pilot);	
		getConfig();
		pilot.getStatusComm().setAcquireStatus("?", StepStatus.NOT_STARTED);
	}
	
	/* Modules we need to use */
	private GeriStreamSink geriStream;
	
	private String programID = "??";
	private long lastInitNano = 0;
	private long lastRunNano = 0;
	
	/** If currently waiting for trigger, or running, the thread that is doing so */
	private Thread cameraInitThread = null;
	
	@Override
	public void geri_init(long timestamp) {
		lastInitNano = timestamp;
		
		geriStream = module(GeriStreamSink.class);
		
		if(cameraInitThread != null) {
			log("StandardAcqusition automation already active!");
			return;
		}
		
		pilot.setInhibitingAutoconfDuringShot(true);
		
		try {
			cameraInitThread = Thread.currentThread();
			
			geriStream.stopStreaming(true); //can't stream in init, codastation won't open the connections until pre/run, 
			// interrupt to kill any existing write, codastation will reinit the connection at PRE 
			acq().enableAcquire(false); //disable the acquisition switch, this will get enabled at the moment we want to start
			setGeriOK(true); //we're nominally ok while we're trying to do things, until we find something doesnt work
			
			programID = getProgramID(timestamp);
			
			pilot.getStatusComm().setPilotStatus(StepStatus.ACTIVE);
			pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ACTIVE);
			pilot.getStatusComm().setSaveStatus(programID, StepStatus.NOT_STARTED);
			pilot.getStatusComm().setProcessStatus(programID, StepStatus.NOT_STARTED);		
			
			//Make sure camera is open and ready to capture, this also allocates/invalidates all images
			try {
				pilot.attemptAcquisitionPrep(programID, timestamp, true);
				
			}catch(Exception err) { 
				log("Aborted by interrupt");
				setGeriOK(false);
				pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
				pilot.getStatusComm().setSaveStatus(programID, StepStatus.NOT_STARTED);
				pilot.getStatusComm().setProcessStatus(programID, StepStatus.NOT_STARTED);
				lastInitNano = -1; //mark init as failed so that other states dont try to do anything
				pilot.setInhibitingAutoconfDuringShot(false);
				return;
			}
			
			//also check that the camera looks like it is ready
			if(acq().getAcquisitionStatus() == Status.errored ||
					acq().getAcquisitionStatus() == Status.notInitied) {
				log("Camera not ready");
				setGeriOK(false);
				pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
				pilot.getStatusComm().setSaveStatus(programID, StepStatus.NOT_STARTED);
				pilot.getStatusComm().setProcessStatus(programID, StepStatus.NOT_STARTED);
				lastInitNano = -1; //mark init as failed so that other states dont try to do anything
				pilot.setInhibitingAutoconfDuringShot(false);
				return;
			}
			
			
			log("Camera init ok");
			setGeriOK(true);
						
			if(getConfig().programStartToCaptureStartMS > 0) {
				pilot.getStatusComm().setAcquireStatus(programID, StepStatus.WAITING);
				
				if(getConfig().preAcquireDurationMS > 0){
					log("Pre-acquire: Now acquiring for a bit");
					acq().enableAcquire(true);
					
					//wait for acqusition to start, hopefully, but dont bother checking				
					waitUntil((System.currentTimeMillis() + getConfig().preAcquireDurationMS) * 1_000_000L); 
					
					//abort again
					log("Pre-acquire: Aborting and waiting for capture restart delay");				
					acq().abort();
					acq().enableAcquire(false); //and disable again for the second acquire wait
				}
				
				//wait until the timestamp where we should actually start the capture (or we switched to run)				
				waitUntil(timestamp, getConfig().programStartToCaptureStartMS / 1000.0, 
							() -> { return pilot.getGERIState() == ImageProcResource.GeriState.RUN; });
				
				
				try {
					pilot.attemptAcquisitionPrep(programID, timestamp, true);
				}catch(Exception err) { 
					log("Aborted by interrupt");
					setGeriOK(false);
					pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
					pilot.getStatusComm().setSaveStatus(programID, StepStatus.NOT_STARTED);
					pilot.getStatusComm().setProcessStatus(programID, StepStatus.NOT_STARTED);
					lastInitNano = -1; //mark init as failed so that other states dont try to do anything
					pilot.setInhibitingAutoconfDuringShot(false);
					return;
				}
				
				if(acq().getAcquisitionStatus() == Status.errored) {
					log("Camera not ready at re-capture. (This is the 2nd time, it was OK at program start).");
					setGeriOK(false);
					pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
					pilot.getStatusComm().setSaveStatus(programID, StepStatus.NOT_STARTED);
					pilot.getStatusComm().setProcessStatus(programID, StepStatus.NOT_STARTED);
					lastInitNano = -1; //mark init as failed so that other states dont try to do anything
					pilot.setInhibitingAutoconfDuringShot(false);
					return;
				}	
			}
			
			log("Waiting to enable acquire");
			pilot.getStatusComm().setAcquireStatus(programID, StepStatus.WAITING);
			setGeriOK(true);
			//wait until the timestamp where we should actually start the acquisition
			waitUntil(timestamp, getConfig().programStartToAcquireStartMS / 1000.0);
				
			//enable the acquisition switch		
			log("Setting acquire enable");
			acq().enableAcquire(true);
			setGeriOK(true);
			
			pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ACTIVE);
			
			//if for some reason they want to stream immediately, do so. This shouldn't actually work
			// since the codastation cant accept data with timestamps during INIT. If the camera dosent actually start
			//acquiring until after PRE, it'll seem fine
			if(getConfig().beginStreamingInState == GeriState.INIT) {
				log("Streaming started on INIT");
				geriStream.startStreaming();
				//send a parlog now, just in case we never get to RUN or POST		
				geriStream.sendMetadata(timestamp);

				pilot.getStatusComm().setSaveStatus(programID, StepStatus.ACTIVE);	
			}
			
		}finally {
			cameraInitThread = null;
		}
	}

	@Override
	public void geri_pre(long timestamp) {
		if(lastInitNano < 0){
			log("Last init failed, ignoring PRE");		
			return;
		}
		
		if(getConfig().beginStreamingInState == GeriState.PRE) {
			log("Streaming started on PRE");
			geriStream.startStreaming();
			//send a parlog now, just in case we never get to RUN or POST		
			geriStream.sendMetadata(timestamp);

			pilot.getStatusComm().setSaveStatus(programID, StepStatus.ACTIVE);	
		}
				
	}
	
	@Override
	public void geri_run(long timestamp) {
		if(lastInitNano < 0){
			log("Last init failed, ignoring RUN");		
			return;
		}
		
		lastRunNano = timestamp;
		if(getConfig().beginStreamingInState == GeriState.RUN || 
				getConfig().beginStreamingInState == GeriState.PRE ) {
			log("Streaming started on RUN");
			geriStream.startStreaming();
			//send a parlog now, just in case we never get to POST		
			geriStream.sendMetadata(timestamp);
			
			pilot.getStatusComm().setSaveStatus(programID, StepStatus.ACTIVE);
		}
				
		if(getConfig().acquireStartOnRun) {			
			acq().enableAcquire(true);
			pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ACTIVE);
		}
		
	}
	
	@Override
	public void geri_post(long timestamp) {
		if(lastInitNano < 0) {
			log("Last init failed, ignoring POST");		
			return;
		}
		
		if(getConfig().abortAcquireOnState == GeriState.POST) {
			acq().abort();
			pilot.getStatusComm().setAcquireStatus(programID, StepStatus.SUCCESS);

		}
		setGeriOK(true); //things are progressing ok

		//send a parlog now, just in case the later one is too late		
		geriStream.sendMetadata(timestamp);
		
		//make sure streaming is enabled at this point, if it wasn't during pre
		if(!geriStream.isStreamingActive() && (getConfig().beginStreamingInState == GeriState.PRE || 
					getConfig().beginStreamingInState == GeriState.RUN ||
					getConfig().beginStreamingInState == GeriState.POST)) {
			
			log("Streaming started on POST");
			pilot.getStatusComm().setSaveStatus(programID, StepStatus.ACTIVE);	
			geriStream.startStreaming();
			
		}
		
		//have a go at the timebases
		try {
			pilot.createTimebases(lastInitNano, lastRunNano);
		}catch(RuntimeException err) {
			log("Unable to create time baseses: " + err.getMessage());
		}
		
		//begin processing now if so configured
		if(getConfig().beginProcessingInState == GeriState.POST &&
				!getConfig().processingOnlyAfterSaveComplete) {
			log("Processing started on POST");
			beginProcessing(programID);
			pilot.getStatusComm().setProcessStatus(programID, StepStatus.ACTIVE);
		}
		
		//if camera init process is running, abort it
		if(cameraInitThread != null) {
			log("Aborting running camera init due to switch to POST.");
			cameraInitThread.interrupt();
			setGeriOK(false);
		}		
		
		//we can make a first go at making timebases now, although some data might not be in yet
		//we'll do it again in IDLE
		try {
			pilot.createTimebases(lastInitNano, lastRunNano);
		}catch(RuntimeException err) { 
			log("WARNING: Unable to create timebases: " + err.toString());
		}
		
		programID = checkProgramID(programID, lastInitNano);
		
		//wait for the streaming to complete, or the timeouts (pilot.getConfig().streamWaitIdleTimeoutMS and pilot.getConfig().streamWaitTimeoutMS)
		boolean streamOK = waitForStreamingComplete(geriStream, programID);
		
		//if the streaming fails or times out, save it with the local/webapi saving module
		if(!streamOK && (getConfig().saveOn == SaveCondition.IF_STREAMING_FAILS 
				           || getConfig().saveOn == SaveCondition.ALWAYS)) {
			pilot.getStatusComm().setSaveStatus(programID, StepStatus.ERROR);
			
			log("Streaming failed, saving with W7X-Save.");
			module(W7XArchiveDBSink.class).save();
			
			log("Stream/save complete");			
		}
		
		//Begin processing at this point if it's set to only start after saving is complete
		if(getConfig().beginProcessingInState == GeriState.POST &&
				getConfig().processingOnlyAfterSaveComplete) {
			
			beginProcessing(programID);
			pilot.getStatusComm().setProcessStatus(programID, StepStatus.ACTIVE);
		}

		pilot.getStatusComm().setProcessStatus(programID, StepStatus.ACTIVE);
		if(getConfig().beginProcessingInState != null) {
			waitForProcessingToComplete();
		}
		
		//once that is done, success or not, have a final go at the timebases
		try {
			pilot.createTimebases(lastInitNano, lastRunNano);
		}catch(RuntimeException err) { 
			log("WARNING: Unable to create timebases: " + err.toString());
		}
		
		//and send the final, as complete as possible parlog at the last state change		
		geriStream.sendMetadata(timestamp+1);

		pilot.getStatusComm().setProcessStatus(programID, StepStatus.SUCCESS);
		setGeriOK(true); //things seemed to end ok
		pilot.setInhibitingAutoconfDuringShot(false);
	}

	@Override
	public void geri_idle(long timestamp) {
		//if camera init process is running, abort it
		if(cameraInitThread != null) {
			log("Aborting running camera init due to switch to IDLE.");
			cameraInitThread.interrupt();
			acq().abort(); //stop the capture/acquire
			acq().enableAcquire(true); //set acquire flag to avoid user confusion
			
		}
		
		if(getConfig().abortAcquireOnState == GeriState.IDLE) {
			acq().abort();
			pilot.getStatusComm().setAcquireStatus(programID, StepStatus.SUCCESS);
		}
		
		//do the timebases again
		try {
			pilot.createTimebases(lastInitNano, lastRunNano);
		}catch(RuntimeException err) {
			log("Unable to create time baseses: " + err.getMessage());
		}
		
		//everything done, maybe now is a good time to garbage collect?
		Logger logr = Logger.getLogger(this.getClass().getName());
		logr.info("Forcing garbage collection");
		System.gc();
		logr.info("Garbage collection done");
		
		pilot.getStatusComm().setPilotStatus(StepStatus.WAITING);
	}
	
	@Override
	public void geri_run_to_pre(long timestamp) {
		// TODO Auto-generated method stub
		//super.geri_run_to_pre(timestamp);
		
		//for pulse trains etc, it goes back to PRE from RUN and we can stop until the next RUN
		acq().enableAcquire(false);
	}

	@Override
	public Class getConfigType() { return StandardAcquisitionConfig.class; }		
	@Override
	public StandardAcquisitionConfig getConfig() { return (StandardAcquisitionConfig)super.getConfig(); }
}
