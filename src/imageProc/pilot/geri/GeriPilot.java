package imageProc.pilot.geri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import geri.ResourceServer;
import geri.ResourceServer.ServerStateChange;
import geri.providers.ImageProvider;
import geri.providers.SignalProvider;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ConfigurableByID;
import imageProc.core.DatabaseSink;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgSink;
import imageProc.core.EventReciever.Event;
import imageProc.database.geriStream.GeriStreamSink;
import imageProc.pilot.common.CommonPilot;
import imageProc.pilot.common.PilotLog;
import imageProc.pilot.common.PilotStatusComm.StepStatus;
import imageProc.pilot.geri.GeriPilotConfig.StatusReportMode;
import imageProc.pilot.geri.ImageProcResource.GeriState;
import imageProc.pilot.geri.automation.Automation;
import imageProc.pilot.geri.swt.GeriPilotSWTControl;
import imageProc.pilot.w7x.W7XPilot;
import imageProc.pilot.w7x.W7XPilotConfig;
import imageProc.pilot.w7x.swt.W7XPilotSWTControl;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

public class GeriPilot extends CommonPilot implements EventReciever, ConfigurableByID, ServerStateChange {
	
	private GeriPilotConfig config = new GeriPilotConfig();
	
	private ImageProcResource resource  = new ImageProcResource(this);
	private ResourceServer geriServer = new ResourceServer(resource);
	
	private String lastLoadedConfig;
	
	/** The last state that was switched to (i.e. the 'current' state - probably) */
	private GeriState lastState = GeriState.UNKNOWN;
	
	/** Timestamp of last change to PRE state. Streamers should not stream any data before this */
	private long lastPreTransitionTimestamp = -1;

	/** Current automation sequence */
	private Automation automation = null;

	private String codastationStatus = "init";
	
	private ScheduledFuture<?> nextCodastationUpdate;

	/** When the resource server was last started. This is used for restarting the codastation after a timeout and is modified
	 * by some other things too.
	 */
	private long codaResourceConnectTimeoutRefTime = -1;
	
	public GeriPilot() {
		forceCodastationCheck();
	}
	
	public void startResource() {
		if(geriServer.isActive()) {
			log.write("Server already active");
			return;
		}
		codaResourceConnectTimeoutRefTime  = System.currentTimeMillis();
			
		log.write("starting server on port " + config.serverPort);
		setStatus("Server started");
		geriServer.setPort(config.serverPort);
		geriServer.start(this);
		
		if(config.startCodastationOnResourceStart && !isCodastationActive()) {			
			startCodastationService();
			codaResourceConnectTimeoutRefTime = System.currentTimeMillis();
		}
		
	}
	
	/** Called from ResourceServer to say server started or stopped, or a codastation connected or
	 * disconnected */
	@Override
	public void serverStateChanged() {
		if(!isConnected()) 
			lastState = GeriState.UNKNOWN;
		
		if(geriServer.getLastError() != null) {
			logr.log(Level.SEVERE, "Error starting/in GERI resource server", geriServer.getLastError());
			lastState = GeriState.UNKNOWN;
			String str = "Error starting/in GERI resource server" + geriServer.getLastError().getMessage();
			setStatus("ERROR:" + str);
			log.write(str);
		}
		
		updateStatus();
	}

	/** Determine the general status to report to the flight controller from the status of the acquisition module, 
	 *  codastation service, resource server, automation etc */
	public void updateStatus() {		
		
		if(!isAutomationEnabled()) {
			//if master enable is off, then we are ok but just not started
			statusComm.setPilotStatus(StepStatus.NOT_STARTED);
		
		
		}else if(!isConnected() || !isAquisitionOK() || !isAutomationOK()) {
			//if the automation should be enabled, but we are not ready, report an error
			
			statusComm.setPilotStatus(StepStatus.ERROR);
					
		}else if(automation.isActive()) {
			//if the automation is busy doing somthing, or waiting internally for something to complete, then we are active
			statusComm.setPilotStatus(StepStatus.ACTIVE);
		
		}else {
			//otherwise we are ready and waiting for a GERI state change
			statusComm.setPilotStatus(StepStatus.WAITING);
		}
		
			
		updateAllControllers();
	}
	
	public void stopResource() {
		log.write("stopping server");
		setStatus("Server stopped");
		geriServer.stop();
		updateAllControllers();
	}


	public void stateChanged(long timestamp, GeriState old, GeriState state) {
		lastState = state;
		log.write("State changed from " + old + " to " + state);
		
		if(state == GeriState.PRE)
			lastPreTransitionTimestamp = timestamp;
		
		if(automation != null) {
			automation.stateChange(timestamp, old, state);
		}
		
		if(config.commStateChanges && statusComm != null) {
			statusComm.sendGERIState(state.value, timestamp);
		}
		
		updateAllControllers();
	}
	
	@Override
	public void saveConfig(String id){		
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		json = json.replaceAll("},", "},\n");
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
		
		setAutomationProgram(config.automationProgram);
		//need to deal with config.automationConfig

		updateAllControllers();
		
		if(config.startServerOnConfigLoad)
			startResource();
		if(config.statusComm.startOnConfigLoad)
			statusComm.startListening();
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, GeriPilotConfig.class);
		
	}

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			ImagePipeController swtController;
			//if(swtController == null){
				swtController = new GeriPilotSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			//}
			return swtController;
		}else
			return null;
	}
	
	
	/** Status text string as in CommonPilot */
	public String getStatus() { return status; }
	
	private void setStatus(String status){ 
		this.status = status; 
		updateAllControllers();
	}
	
	public void addProvider(SignalProvider provider) {
		resource.addProvider(provider);		
	}

	public boolean isConnected() {
		return geriServer.isConnected();
	}

	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalAbort:
			if(automation != null)
				automation.triggerAbort();
			
			break;
		default:
			break;
		}		
	}
	
	@Override
	/** ImageProc module state, nothing todo with Geri IDLE state */ 
	public boolean isIdle() { return true;	} 
	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }

	@Override
	public GeriPilotConfig getConfig() { return config; }
	
	@Override
	/** Make visible for AugControlIPS */
	public void updateAllControllers() {
		super.updateAllControllers();
	}

	@Override
	protected boolean isControllerAborting() { return false; } //we don't really have a controller that can abort (it's geri)
	
	public boolean isResourceActive() { return resource != null && geriServer != null && geriServer.isActive(); }

	/** Gets the state of the GERI state machine thing */
	public GeriState getGERIState() { return lastState; }

	public int getNumConnections() { return geriServer.getNumConnections(); }
	
	public String getConnectionsInfo() { 
		int nConnections = geriServer.getNumConnections();
		StringBuilder sb = new StringBuilder();
		
		sb.append("n=" + nConnections + "\n");
		for(SignalProvider p : resource.getProviders()) {
			sb.append(p.getId() + ": " + p.getClass().getSimpleName() + ", connected=" + p.isConnected() + ", streaming=" + p.isStreamingActive() + "\n");
		}
				
		return sb.toString();		
		
	}

	public String[] getAutomationNames() {
		List<Class> classes = Automation.getAutomationTypes();
		
		String names[] = new String[classes.size()];
		for(int i=0; i < classes.size(); i++)
			names[i] = classes.get(i).getSimpleName();
		
		return names;
	}
	
	public void setAutomationProgram(String automationName) {
		if(automationName == null || automationName.length() == 0 || automationName.equals(" -- None --")) {
			automation = null;
			config.automationProgram = null;
			updateAllControllers();
			return;			
		}
			
		List<Class> classes = Automation.getAutomationTypes();
		
		for(int i=0; i < classes.size(); i++) {
			Class cls = classes.get(i);
			if(cls.getSimpleName().equals(automationName)) {
				config.automationProgram = cls.getSimpleName();
				if(!cls.isInstance(automation)) {
					try {
						Constructor c = cls.getDeclaredConstructor(GeriPilot.class);
						automation = (Automation) c.newInstance(this);						
					} catch (Exception e) {
						automation = null;
						config.automationProgram = null;
						e.printStackTrace();
					}
				}
			}
			updateAllControllers();
		}
		
	}
	
	public void setAutomationProgram(Automation automation) {
		this.automation = automation;		
		updateAllControllers();
	}

	public Automation getAutomationProgram() { return automation; }

	public void setConfig(GeriPilotConfig newConfig) {
		this.config = newConfig; 
	}

	@Override
	public void forceShot(String programName, long programStartNanos) {
		// TODO Auto-generated method stub		
	}

	public boolean isCodastationActive() { 
		return codastationStatus.contains("active (running)");
	}
	
	public String getCodastationStatus() { return this.codastationStatus; }
	
	public void forceCodastationCheck() {
		if(nextCodastationUpdate != null)
			nextCodastationUpdate.cancel(false);
		
		nextCodastationUpdate = ImageProcUtil.getScheduledThreadPool().schedule(() -> doGetCodastationStatus(), 0, TimeUnit.MILLISECONDS);
		
	}
	
	public void doGetCodastationStatus() {
		
		try {
			//String classLoaderPath = ImageProcUtil.class.getClassLoader().getResource("").getPath();
			//String autoPath = classLoaderPath.replaceAll("(.*)/ImageProc-.*", "$1");		
			//String projectsPath = SettingsManager.defaultGlobal().getPathProperty("minerva.code.path", autoPath);
			
			String command[] = config.systemdAsUser 
									? new String[]{ "systemctl", "--user", "status", "codastation@" + config.codastationID }
									: new String[]{ "sudo", "systemctl", "status", "codastation@" + config.codastationID };
			
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.command(command);
			//processBuilder.directory(new File(projectsPath));
				
			Process process = processBuilder.start();

			BufferedReader reader =
					new BufferedReader(new InputStreamReader(process.getInputStream()));

			while (true) {
				String line = reader.readLine();
				if(line == null) {
					logr.log(Level.WARNING, "Unable to get status of codastation service, no status from systemctl");
					codastationStatus = "Unknown - no status from systemctl";
					
					//int exitCode = process.waitFor();
					return;
				}
				
				if(line.contains("Active: ")) {
					//logr.log(Level.FINE, "Codastation status:" + line);
					codastationStatus = line.strip().substring(8);
					break;
				}
			}

			
		} catch (Exception err) {
			logr.log(Level.WARNING, "Unable to get status of codastation service", err);
			codastationStatus = "Error: " + err.getMessage();
		}finally {
			updateAllControllers();
			//schedule next check 
			//we don't use the regular thing, in case the period changes
			nextCodastationUpdate = ImageProcUtil.getScheduledThreadPool().schedule(() -> doGetCodastationStatus(), config.codastationCheckPeriod, TimeUnit.MILLISECONDS);
			
		}
		
		//Use the opportunity to check if we might want to restart the codastation
		if(config.restartCodastationTimeout > 0) {
			//its been longer than the timeout, resource
			if(isResourceActive()
					&& isCodastationActive()
					&& !geriServer.isConnected()
					&& (System.currentTimeMillis() - codaResourceConnectTimeoutRefTime) > config.restartCodastationTimeout) {
				
				log.write("Resource server running for more than " + config.restartCodastationTimeout + " ms but codastation has not connected, restarting it.");
				
				try {
					Runtime.getRuntime().exec(
							config.systemdAsUser 
								? new String[]{ "systemctl", "--user", "restart", "codastation@" + config.codastationID }
								: new String[]{ "sudo", "systemctl", "restart", "codastation@" + config.codastationID });
					forceCodastationCheck();
					
					//don't try to do this more than once per minute
					codaResourceConnectTimeoutRefTime = System.currentTimeMillis() + 60000;
					
				} catch (IOException err) {
					logr.log(Level.WARNING, "Unable to restart codastation service", err);
				}
			}
		}
		
	}

	public void startCodastationService() {
		log.write("Starting codastation service");
		
		try {
			Runtime.getRuntime().exec(config.systemdAsUser 
					? new String[]{ "systemctl", "--user", "start", "codastation@" + config.codastationID }
					: new String[]{ "sudo", "systemctl", "start", "codastation@" + config.codastationID });
			forceCodastationCheck();
		} catch (IOException err) {
			logr.log(Level.WARNING, "Unable to start codastation service", err);
		}
	}

	public void stopCodastationService() {
		try {
			Runtime.getRuntime().exec(config.systemdAsUser 
					? new String[]{ "systemctl", "--user", "stop", "codastation@" + config.codastationID }
					: new String[]{ "sudo", "systemctl", "stop", "codastation@" + config.codastationID });
			forceCodastationCheck();
		} catch (IOException err) {
			logr.log(Level.WARNING, "Unable to stop codastation service", err);
		}
		
	}

	public void showLogCodastationService() {
		try {
			Runtime.getRuntime().exec(config.systemdAsUser 
					? new String[]{ "xfce4-terminal","-x","journalctl","--user","-fu","codastation@" + config.codastationID }
					: new String[]{ "xfce4-terminal","-x","sudo", "journalctl","-u","codastation@" + config.codastationID , "-f"});
		} catch (IOException err) {
			logr.log(Level.WARNING, "Unable to show log of codastation service", err);
		}
		
	}

	/** Gets the overall status flag (used to report to GERI if everything is ok) */
	public boolean getOverallStatus() {		
		switch(config.statusReportMode) {			
			case ALWAYS_OK: return true;
			case IS_READY: return isAquisitionOK() && isCodastationActive() && isResourceServerOK() && isAutomationOK();
			default: return false;
		}
	}
	
	public boolean isAquisitionOK() {
		if(connectedSource == null || !(connectedSource instanceof AcquisitionDevice))
			return false;
		AcquisitionDevice acq = (AcquisitionDevice)connectedSource;
		AcquisitionDevice.Status stat = acq.getAcquisitionStatus();
		
		switch(stat){
			case init:
			case awaitingSoftwareTrigger:
			case awaitingHardwareTrigger :
			case capturing:
			case aborted:
			case completeOK:
				return true;
			default:
				return false;
		}
	}
	
	private boolean isAutomationEnabled() {
		return config.automationEnable && automation != null;
	}

	public boolean isAutomationOK() {
		return isAutomationEnabled() && automation.isOK();
	}

	public boolean isResourceServerOK() {
		return resource != null && geriServer != null && geriServer.isActive() && geriServer.isConnected();
	}

	@Override
	protected void doSinkSave(DatabaseSink sink, String programID) throws InterruptedException {
		//throw new RuntimeException("GeriPilot.doSinkSave() not implemented. What are we supposed to do here??");
	}
	
	public long getLastPreTransitionTimestamp() { return lastPreTransitionTimestamp; }
}
