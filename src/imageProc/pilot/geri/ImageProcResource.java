package imageProc.pilot.geri;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.FileHandler;
import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.thrift.TException;

import de.mpg.ipp.codac.geri.ResourceException;
import geri.Controllable;
import geri.ResourceImpl;
import geri.ResourceServer;
import geri.Types;
import geri.providers.ImageProvider;
import geri.providers.LogProvider;
import geri.providers.ScalarProvider;
import geri.providers.SignalProvider;
import geri.providers.SignalProvider.Protocol;
import imageProc.core.ByteBufferBackedImage;
import imageProc.pilot.geri.GeriPilotConfig.StatusReportMode;

/** ImageProc as a resource from the point of view of Geri */
public class ImageProcResource extends ResourceImpl {
	private GeriPilot pilot; 
	
	
	private static String controlLog = "{\"tte\": {\"trigger\": {\"channel\": 0, \"width_ns\": 5000000}}, \"acq\": {\"init\": {\"ext_clk\": 10000000, \"clock\": 10000, \"pre\": 0, \"post\": -1}}, \"file\": {\"error\": 0, \"code\": \"tte.trigger.channel = 0\\ntte.trigger.width_ns = 5000000\\nacq.init.ext_clk = 10000000\\nacq.init.clock = 10000\\nacq.init.pre = 0\\nacq.init.post = -1\", \"path\": \"javaLand\"}}";
	//private static String controlLog = "{\"tte\": {\"trigger\": {\"channel\": 0, \"width_ns\": 5000000}}, \"acq\": {\"init\": {\"ext_clk\": 10000000, \"clock\": 10000, \"pre\": 0, \"post\": -1}}, \"file\": {\"error\": 0, \"code\": \"tte.trigger.channel = 0\\ntte.trigger.width_ns = 5000000\\nacq.init.ext_clk = 10000000\\nacq.init.clock = 10000\\nacq.init.pre = 0\\nacq.init.post = -1\", \"path\": \"/home/IPP-HGW/b-oliford/geri-settings.py\"}}";
	
	public static enum GeriState {
		IDLE(0), INIT(1), PRE(2), RUN(3), POST(4), SAFE(-1), UNKNOWN(-99);			
		int value;
		GeriState(){ this.value = -1; }
		GeriState(int value) { this.value = value; }
		public static GeriState byValue(int value) { 
			for(GeriState s : GeriState.values()) 
				if(s.value == value)
					return s;
			return null;
		}
		
		public static String[] names() {
			ArrayList<String> strs = new ArrayList<String>();
			for(GeriState s : GeriState.values()) 
				strs.add(s.name());
			return strs.toArray(new String[GeriState.values().length]);
		}
	}
	
	public class MyControllable extends Controllable {
		
		public MyControllable(int id) {
			super(id);
		}		
		
		@Override
		public void stateChanged(long timestamp, int old, int state) {
			pilot.stateChanged(timestamp, GeriState.byValue(old), GeriState.byValue(state));
			
			if(old == GeriState.IDLE.value && state == GeriState.INIT.value) {
				logr.fine("log->send");
				logCtrl.queue(timestamp, controlLog);
			}
			
			if(old == GeriState.INIT.value && state == GeriState.PRE.value) {
				logr.fine("log->send");
				logCtrl.queue(timestamp, controlLog);
			}			
		}
	}
	
	MyControllable ctrl = new MyControllable(0);
	LogProvider logCtrl = new LogProvider(1, 65531); //is 65535 in C++ example, 65531 in python
	//LogProvider logCam = new LogProvider(2, 65531);
	//RegularImageProvider cam = new RegularImageProvider(3);
	
	ScalarProvider adc = new ScalarProvider(-1, 32, Types.int16);//not used
	
	public ImageProcResource(GeriPilot pilot) {
		try {
			FileHandler fh = new FileHandler("/tmp/resource-ImageProc.log");
			fh.setFormatter(new SimpleFormatter());
			logr.addHandler(fh);
			logr.setLevel(Level.FINEST);
			logr.finest("test");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.pilot = pilot;
		
		controllables.put(ctrl.getId(), ctrl);
		signalProviders.add(logCtrl);
		//signalProviders.add(logCam);
		//other providers are added by GeriStreamSink later on
	}
	
	@Override
	public void initResource(ByteBuffer properties) throws ResourceException, TException {
		pilot.getLog().write("initResource (codastation connected)");
				
		super.initResource(properties);
	}

	public void sendADCSample() {
		int num_samples = adc.getNumSamples();
		int num_channels = adc.getNumChannels();
		if (num_samples > 0) {
			long now = System.currentTimeMillis() * 1_000_000L;
			long[] time = new long[num_samples];
			short[] data = new short[num_channels * num_samples];
			for(int i = 0; i < num_samples; i++) {
				time[i] = now + i * 1000L;
				for(int j = 0; j < num_channels; j++) {
					data[i*num_channels+j] = (short)(i * 0x20 + j);
				}
			}
			logr.fine("ADC->send");
			adc.queue(time, data);
		}		
	}

	public void addProvider(SignalProvider provider) {
		signalProviders.add(provider);
	}
	
	@Override
	protected boolean reportIsOK() {
		return pilot.getOverallStatus();
	}
	
	public ArrayList<SignalProvider> getProviders() { return signalProviders; }
}
