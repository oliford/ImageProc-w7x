package imageProc.pilot.geri.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.geri.GeriPilotConfig;
import imageProc.pilot.geri.ImageProcResource.GeriState;
import imageProc.pilot.geri.automation.StandardAcquisitionConfig;
import imageProc.pilot.geri.automation.StandardAcquisitionConfig.SaveCondition;

public class StandardAcqusitionConfigSWTControl {
	private GeriPilot pilot;

	private Group swtGroup;
	
	private Button precaptureCheckbox;
	private Spinner captureStartSpinner;
	private Spinner acquireStartSpinner;
	private Button acquireStartOnGeriRUNCheckbox;
	private Combo streamingStateCombo;
	private Combo processingStateCombo;
	private Button processingAfterSaveCheckbox;
	private Combo abortStateCombo;
	private Combo saveOnCombo;
	
	public StandardAcqusitionConfigSWTControl(GeriPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("Automation configuration (Standard acqusition)");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
        swtGroup.setLayout(new GridLayout(5, false));

		Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Capture (ms after prog start):\n(t0=1s,t1=61s)");
		captureStartSpinner = new Spinner(swtGroup, SWT.NONE);
		captureStartSpinner.setToolTipText("Time from 'program start' at which to begin capture. If set (>0), the capture will be started at program start, aborted and then restarted at this time.");
		captureStartSpinner.setValues(0, 0, Integer.MAX_VALUE, 0, 10, 60);
		captureStartSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		captureStartSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});
		 
		Label lAS = new Label(swtGroup, SWT.NONE); lAS.setText("Acquire (ms after prog start):\n(t0=1s,t1=61s)");
		acquireStartSpinner = new Spinner(swtGroup, SWT.NONE);
		acquireStartSpinner.setToolTipText("Time from 'program start' at which to begin acqusition. Program start is typically 61s before the plasma breakdown. (may be started earlier by 'acquire on GERI RUN')");
		acquireStartSpinner.setValues(0, 0, Integer.MAX_VALUE, 0, 10, 60);
		acquireStartSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		acquireStartSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		Label lPC = new Label(swtGroup, SWT.NONE); lPC.setText("");
		precaptureCheckbox = new Button(swtGroup, SWT.CHECK);
		precaptureCheckbox.setText("Briefly run capture and acquire at 0s to check that it's working.");
		precaptureCheckbox.setToolTipText("Begin acquisition on GERI RUN, (if it has not already started by acquire start time).");
		precaptureCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		precaptureCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		
		Label lAG = new Label(swtGroup, SWT.NONE); lAG.setText("");
		acquireStartOnGeriRUNCheckbox = new Button(swtGroup, SWT.CHECK);
		acquireStartOnGeriRUNCheckbox.setText("Acquire on GERI state change to RUN");
		acquireStartOnGeriRUNCheckbox.setToolTipText("Begin acquisition on GERI RUN, (if it has not already started by acquire start time).");
		acquireStartOnGeriRUNCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		acquireStartOnGeriRUNCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		Label lAB = new Label(swtGroup, SWT.NONE); lAB.setText("Abort on:");
		abortStateCombo = new Combo(swtGroup, SWT.DROP_DOWN);
		abortStateCombo.setItems(GeriState.names());
		abortStateCombo.setToolTipText("Abort acquisition on given state. Usually POST. Maybe IDLE.");
		abortStateCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		abortStateCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("Streaming from:");
		streamingStateCombo = new Combo(swtGroup, SWT.DROP_DOWN);
		streamingStateCombo.setItems(GeriState.names());
		streamingStateCombo.setToolTipText("At which GERI stage to begin streaming. Mostly RUN or POST. Maybe IDLE.");
		streamingStateCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		streamingStateCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
		
		

		Label lPS = new Label(swtGroup, SWT.NONE); lPS.setText("Processing from:");
		processingStateCombo = new Combo(swtGroup, SWT.DROP_DOWN);
		processingStateCombo.setItems(GeriState.names());
		processingStateCombo.setToolTipText("Begin processing modules when given GERI state is reached. Usually POST");
		processingStateCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
		processingStateCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

		Label lPA = new Label(swtGroup, SWT.NONE); lPA.setText("");
		processingAfterSaveCheckbox = new Button(swtGroup, SWT.CHECK);
		processingAfterSaveCheckbox.setText("Processing only after stream/save complete.");
		processingAfterSaveCheckbox.setToolTipText("Only begin processing once the streaming and/or save operations are complete");
		processingAfterSaveCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		processingAfterSaveCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		Label lST = new Label(swtGroup, SWT.NONE); lST.setText("Use W7XSave:");
		saveOnCombo = new Combo(swtGroup, SWT.NONE);
		saveOnCombo.setItems(SaveCondition.names());
		saveOnCombo.setToolTipText("When to save the data using the W7X Save module.");
		saveOnCombo.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
		saveOnCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

		ImageProcUtil.addRevealWriggler(swtGroup);
        doUpdate();
		
	}

	private void settingsChangedEvent(Event event) {
		GeriPilotConfig config = pilot.getConfig();
		StandardAcquisitionConfig acqCfg = ((StandardAcquisitionConfig)config.automationConfig);

		acqCfg.programStartToCaptureStartMS = captureStartSpinner.getSelection();
		acqCfg.programStartToAcquireStartMS = acquireStartSpinner.getSelection();
		acqCfg.precapture = precaptureCheckbox.getSelection();
		acqCfg.acquireStartOnRun = acquireStartOnGeriRUNCheckbox.getSelection();
		acqCfg.abortAcquireOnState = GeriState.valueOf(abortStateCombo.getText());
		acqCfg.beginStreamingInState = GeriState.valueOf(streamingStateCombo.getText());
		acqCfg.beginProcessingInState = GeriState.valueOf(processingStateCombo.getText());
		acqCfg.processingOnlyAfterSaveComplete = processingAfterSaveCheckbox.getSelection();		
		acqCfg.saveOn = SaveCondition.valueOf(saveOnCombo.getText());		
		
	}
	
	public void doUpdate() {		
		GeriPilotConfig config = pilot.getConfig();
		
		StandardAcquisitionConfig acqCfg = ((StandardAcquisitionConfig)config.automationConfig);
		
		captureStartSpinner.setSelection((int)acqCfg.programStartToCaptureStartMS);
		acquireStartSpinner.setSelection((int)acqCfg.programStartToAcquireStartMS);
		precaptureCheckbox.setSelection(acqCfg.precapture);
		acquireStartOnGeriRUNCheckbox.setSelection(acqCfg.acquireStartOnRun);

		abortStateCombo.setText(acqCfg.abortAcquireOnState.toString());
		streamingStateCombo.setText(acqCfg.beginStreamingInState.toString());			
		processingStateCombo.setText(acqCfg.beginProcessingInState.toString());			

		processingAfterSaveCheckbox.setSelection(acqCfg.processingOnlyAfterSaveComplete);		
		saveOnCombo.setText(acqCfg.saveOn.toString());
	}

	public void destroy() {
		swtGroup.dispose();
	}

	public Control getSWTGroup() { return swtGroup;	}
}
