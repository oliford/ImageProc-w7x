package imageProc.pilot.geri.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import imageProc.control.optecFilterWheel.OptecFilterWheelConfig;
import imageProc.core.ImageProcUtil;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.geri.GeriPilotConfig;

public class FullConfigSWTControl {
	private GeriPilot pilot;

	private Group swtGroup;
	
	private Text configTextbox;
	private Button setButton;
	private Button getButton;
	
	public FullConfigSWTControl(GeriPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		GeriPilotConfig config = pilot.getConfig();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Advanced config");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(6, false));
        
        configTextbox = new Text(swtGroup, SWT.MULTI | SWT.V_SCROLL);
        configTextbox.setToolTipText("JSON config of pilot, including automation. Edit it and click 'set'");
        configTextbox.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
        
        getButton = new Button(swtGroup, SWT.PUSH);
        getButton.setText("Get");
        getButton.setToolTipText("Force an update of the text box with the current config.");
        getButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
        getButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { getButtonEvent(event); } });
        
        setButton = new Button(swtGroup, SWT.PUSH);
        setButton.setText("Set");
        setButton.setToolTipText("Sets the pilot configuration to that JSON in the text box.");
        setButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
        setButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { setButtonEvent(event); } });
        
        ImageProcUtil.addRevealWriggler(swtGroup);
        doUpdate();
	}
	
	protected void setButtonEvent(Event event) {
		String jsonText = configTextbox.getText();
		Gson gson = new Gson();
		GeriPilotConfig newConfig = gson.fromJson(jsonText, GeriPilotConfig.class);
		pilot.setConfig(newConfig);
		
		//force the automation config type
		if(pilot.getAutomationProgram() != null)
			pilot.getAutomationProgram().getConfig();
	}

	protected void getButtonEvent(Event event) {
		configTextbox.setText(getJSON());
	}

	public Control getSWTGroup() { return swtGroup; }

	public void doUpdate() {
		if(!configTextbox.isFocusControl())
			configTextbox.setText(getJSON());
	}

	private String getJSON() {
		Gson gson = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.serializeNulls()
				.setPrettyPrinting()
				.create();
		
		return gson.toJson(pilot.getConfig());
	}
}
