package imageProc.pilot.geri.swt;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import algorithmrepository.dierckx.intW;
import imageProc.core.ImageProcUtil;
import imageProc.core.swt.EditableTable;
import imageProc.core.swt.EditableTable.EditType;
import imageProc.core.swt.EditableTable.TableModifyListener;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.geri.GeriPilotConfig;
import imageProc.pilot.geri.ImageProcResource.GeriState;
import imageProc.pilot.geri.automation.Automation;
import imageProc.pilot.geri.automation.StandardAcquisition;
import imageProc.pilot.geri.automation.StandardAcquisitionConfig;

public class AutomationSWTControl {
	private GeriPilot pilot;

	private Group swtGroup;
	
	private Button automationEnableCheckbox;
	private Combo programSelection;
	private Label statusLabel;
	private Button abortTasksButton;
	private Button forceProcessButton;
	
	public AutomationSWTControl(GeriPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		GeriPilotConfig config = pilot.getConfig();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Program");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(5, false));
        
        Label lAE = new Label(swtGroup, SWT.NONE); lAE.setText("Automation:");
        automationEnableCheckbox = new Button(swtGroup, SWT.CHECK);
        automationEnableCheckbox.setText("Enable automation");
        automationEnableCheckbox.setToolTipText("Enable/disable automation of acquisition, saving and processing etc according to the configuration and GERI state changes.");
        automationEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
        automationEnableCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
                      
        Label lPS = new Label(swtGroup, SWT.NONE); lPS.setText("Program:");
        programSelection = new Combo(swtGroup, SWT.DROP_DOWN);
        programSelection.setText("...");
        programSelection.setToolTipText("Sets the automation program which determines what to do on state changes and other events.");
        programSelection.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
        programSelection.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
        statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("?");
        statusLabel.setToolTipText("Shows the current status of the automation script");
        statusLabel.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { pilot.updateAllControllers(); } });
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));

        Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");
        abortTasksButton = new Button(swtGroup, SWT.PUSH);
        abortTasksButton.setText("Abort Tasks");
        abortTasksButton.setToolTipText("Abort Tasks");
        abortTasksButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { abortTasksButtonEvent(event); } });
        abortTasksButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));

        forceProcessButton = new Button(swtGroup, SWT.PUSH);
        forceProcessButton.setText("Force process");
        forceProcessButton.setToolTipText("Force processing of the entire source/sink chain now");
        forceProcessButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { forceProcesssButtonEvent(event); } });
        forceProcessButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
        
        
        ImageProcUtil.addRevealWriggler(swtGroup);
        doUpdate();
	}
	
	protected void abortTasksButtonEvent(Event event) {
		Automation auto = pilot.getAutomationProgram();		
		if(auto != null)
			auto.triggerAbort();
	}	
	
	protected void forceProcesssButtonEvent(Event event) {
		pilot.forceProcess();
	}

	protected void settingsChangedEvent(Event event) {
		GeriPilotConfig cfg = pilot.getConfig();
		
		cfg.automationEnable = automationEnableCheckbox.getSelection();
		pilot.serverStateChanged(); //changing the enable will change the pilot status from 'waiting' to 'not active'
		pilot.setAutomationProgram(programSelection.getText());		
		
	}

	public Control getSWTGroup() { return swtGroup; }

	public void doUpdate() {
		GeriPilotConfig cfg = pilot.getConfig();
		
		automationEnableCheckbox.setSelection(cfg.automationEnable);
		
		Automation a = pilot.getAutomationProgram();
				
		programSelection.removeAll();
		programSelection.add(" -- None --");
		programSelection.select(0);
		for(String name : pilot.getAutomationNames()) {
			programSelection.add(name);
			if(a != null && name.equals(a.getClass().getSimpleName()))
				programSelection.select(programSelection.getItemCount() - 1);
		}
		
		statusLabel.setText(a == null ? "No automation" : (a.getNumberOfRunningTasks() + " tasks active"));
		
	}
}

