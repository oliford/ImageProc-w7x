package imageProc.pilot.geri.swt;



import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.geri.GeriPilotConfig;
import imageProc.pilot.geri.GeriPilotConfig.StatusReportMode;

public class StateSWTControl {
	private GeriPilot pilot;

	private Group swtGroup;
	
	private Text stateResourceTextbox;
  	private Button startResourceButton; 
  	private Button stopResourceButton;
  	
  	private Text stateCodastationTextbox;
  	private Button startCodastationButton; 
  	private Button stopCodastationButton; 
  	private Button logCodastationButton;
  	private Button restartCodaIfNoConnButton;
  	private Button startCodaOnResourceStartCheckbox;
  	
  	private Text stateConnectionsTextbox;
  	private Text stateGERITextbox;
  	private Combo statusReportCombo;
  	private Button statusReportAcqCheckbox;
  	private Button statusReportCodaCheckbox;
  	private Button statusReportAutomCheckbox;
  	private Button statusReportResourceCheckbox;
  	private Button startOnConfigLoadCheckbox;
  	
  	
	
	public StateSWTControl(GeriPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		GeriPilotConfig config = pilot.getConfig();
				
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("State");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(6, false));
        
        Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Codastation service:");        
        stateCodastationTextbox = new Text(swtGroup, SWT.READ_ONLY);
        stateCodastationTextbox.setText("??");
        stateCodastationTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
        stateCodastationTextbox.addListener(SWT.FocusIn, new Listener() { @Override public void handleEvent(Event event) { pilot.forceCodastationCheck(); } });
        
        startCodastationButton = new Button(swtGroup, SWT.PUSH);
        startCodastationButton.setText("Start");
        startCodastationButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        startCodastationButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pilot.startCodastationService(); } });

        stopCodastationButton = new Button(swtGroup, SWT.PUSH);
        stopCodastationButton.setText("Stop");
        stopCodastationButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        stopCodastationButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pilot.stopCodastationService(); } });
        
        logCodastationButton = new Button(swtGroup, SWT.PUSH);
        logCodastationButton.setText("Log");
        logCodastationButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        logCodastationButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pilot.showLogCodastationService(); } });

        Label lCA = new Label(swtGroup, SWT.NONE); lCA.setText("");        
        startCodaOnResourceStartCheckbox = new Button(swtGroup, SWT.CHECK);
        startCodaOnResourceStartCheckbox.setText("Start with resource server");
        startCodaOnResourceStartCheckbox.setToolTipText("Start codastation if not already running when the resource server is started. (which will happen at start-up if 'start on config load' is set)");
        startCodaOnResourceStartCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
        startCodaOnResourceStartCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
        
        restartCodaIfNoConnButton = new Button(swtGroup, SWT.CHECK);
        restartCodaIfNoConnButton.setText("Restart when not connected.");
        restartCodaIfNoConnButton.setToolTipText("Restart the codastation if it doesn't connect after 20s after the resource server is started.");
        restartCodaIfNoConnButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
        restartCodaIfNoConnButton.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
                
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Resource server (GeriPilot):");        
        stateResourceTextbox = new Text(swtGroup, SWT.READ_ONLY);
        stateResourceTextbox.setText("??");
        stateResourceTextbox.addListener(SWT.FocusIn, new Listener() { @Override public void handleEvent(Event event) { pilot.updateAllControllers(); } });
        stateResourceTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
        
        startResourceButton = new Button(swtGroup, SWT.PUSH);
        startResourceButton.setText("Start");
        startResourceButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
        startResourceButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pilot.startResource(); } });

        stopResourceButton = new Button(swtGroup, SWT.PUSH);
        stopResourceButton.setText("Stop");
        stopResourceButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        stopResourceButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pilot.stopResource(); } });
        
        
        
        Label lIC = new Label(swtGroup, SWT.NONE); lIC.setText("Interconnections:");        
        stateConnectionsTextbox = new Text(swtGroup, SWT.READ_ONLY | SWT.MULTI);
        stateConnectionsTextbox.setText("??\n??\n??\n??\n??\n??");
        stateConnectionsTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
        
        Label lG = new Label(swtGroup, SWT.NONE); lG.setText("GERI State (last switch):");        
        stateGERITextbox = new Text(swtGroup, SWT.READ_ONLY);
        stateGERITextbox.setText("??");
        stateGERITextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
        
        Label lR = new Label(swtGroup, SWT.NONE); lR.setText("Reported state to GERI:");        
        statusReportCombo = new Combo(swtGroup, SWT.NONE);
        statusReportCombo.setItems(StatusReportMode.names());
        statusReportCombo.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        statusReportCombo.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });
        
        statusReportAcqCheckbox = new Button(swtGroup, SWT.CHECK);
        statusReportAcqCheckbox.setText("Acqusition");
        statusReportAcqCheckbox.setToolTipText("Is the acqusition device initialised and not in error state?");
        statusReportAcqCheckbox.setEnabled(false);
        statusReportAcqCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        
        statusReportCodaCheckbox = new Button(swtGroup, SWT.CHECK);
        statusReportCodaCheckbox.setText("Codastation");
        statusReportCodaCheckbox.setToolTipText("Is the codastation systemd service active?");
        statusReportCodaCheckbox.setEnabled(false);
        statusReportCodaCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        
        statusReportResourceCheckbox = new Button(swtGroup, SWT.CHECK);
        statusReportResourceCheckbox.setText("Resource");
        statusReportResourceCheckbox.setToolTipText("Is the ImageProc GERI resource server runnung?");
        statusReportResourceCheckbox.setEnabled(false);
        statusReportResourceCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        
        statusReportAutomCheckbox = new Button(swtGroup, SWT.CHECK);
        statusReportAutomCheckbox.setText("Automation");
        statusReportAutomCheckbox.setToolTipText("Was the last state of the automation processor ok?");
        statusReportAutomCheckbox.setEnabled(false);
        statusReportAutomCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
        
        Label lCL = new Label(swtGroup, SWT.NONE); lCL.setText("On config load:");        
        startOnConfigLoadCheckbox = new Button(swtGroup, SWT.CHECK);
        startOnConfigLoadCheckbox.setText("Start server");
        startOnConfigLoadCheckbox.setSelection(false);
        startOnConfigLoadCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));
        startOnConfigLoadCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); } });

        ImageProcUtil.addRevealWriggler(swtGroup);
        
        //codastation update is not done via the normal update chain
        pilot.forceCodastationCheck();
        
        doUpdate();
	}
	
	public Control getSWTGroup() { return swtGroup; }

	public void doUpdate() {
		GeriPilotConfig config = pilot.getConfig();
		
		stateResourceTextbox.setText(pilot.isResourceActive() ? "Active" : "Inactive");
		stateResourceTextbox.setForeground(stateResourceTextbox.getDisplay().getSystemColor(pilot.isResourceActive() ? SWT.COLOR_BLUE : SWT.COLOR_RED));
		stateCodastationTextbox.setText(pilot.getCodastationStatus());
		stateCodastationTextbox.setForeground(stateResourceTextbox.getDisplay().getSystemColor(pilot.getCodastationStatus().matches("(?i).*running.*") ? SWT.COLOR_BLUE : SWT.COLOR_RED));
		statusReportAcqCheckbox.setSelection(pilot.isAquisitionOK());
		statusReportCodaCheckbox.setSelection(pilot.isCodastationActive());
		statusReportResourceCheckbox.setSelection(pilot.isResourceServerOK());
		statusReportAutomCheckbox.setSelection(pilot.isAutomationOK());
		
		stateConnectionsTextbox.setText(pilot.getConnectionsInfo());
		stateGERITextbox.setText(pilot.getGERIState().name());
		statusReportCombo.setText(config.statusReportMode.name());
		startOnConfigLoadCheckbox.setSelection(config.startServerOnConfigLoad);

		startCodaOnResourceStartCheckbox.setSelection(config.startCodastationOnResourceStart);
		restartCodaIfNoConnButton.setSelection(config.restartCodastationTimeout > 0);
	}
	
	protected void settingsChangedEvent(Event event) {
		GeriPilotConfig config = pilot.getConfig();
		
		config.startServerOnConfigLoad = startOnConfigLoadCheckbox.getSelection();
		config.statusReportMode = StatusReportMode.valueOf(statusReportCombo.getText());
		config.startCodastationOnResourceStart = startCodaOnResourceStartCheckbox.getSelection();
		config.restartCodastationTimeout = restartCodaIfNoConnButton.getSelection() ? 10000 : -1;		
	}
}
