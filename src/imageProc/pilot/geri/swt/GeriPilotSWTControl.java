package imageProc.pilot.geri.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.pilot.common.swt.DaySWTControl;
import imageProc.pilot.common.swt.StatusCommSWTControl;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.geri.GeriPilotConfig;
import imageProc.pilot.geri.automation.StandardAcquisitionConfig;
import imageProc.pilot.w7x.swt.AutoconfigSWTControl;

public class GeriPilotSWTControl implements ImagePipeController {
	private GeriPilot pilot;
	
	private Group swtGroup;

	private Label statusLabel;
	private Text logTextbox;
  	
	private SashForm swtSashForm;
	private CTabFolder swtTabFoler;	
	private CTabItem swtStateTab;	
	private CTabItem swtAutomationTab;
	private CTabItem swtSimpleConfigTab;	
	private CTabItem swtFullConfigTab;	
	private CTabItem swtAutoconfTab;
	private CTabItem swtStatusCommTab;
	private CTabItem swtAutomationConfigTab;
	
	private StateSWTControl stateSWTCtrl;
	private AutomationSWTControl automationSWTCtrl;
	private SimpleConfigSWTControl simpleConfigSWTCtrl;
	private FullConfigSWTControl fullConfigSWTCtrl;
	private AutoconfigSWTControl autoconfSWTCtrl;
	private StatusCommSWTControl statusCommSWTCtrl;
	private Object automationConfigCtrl;
	
	private SWTSettingsControl settingsCtrl;
	
	public GeriPilotSWTControl(GeriPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("GERI Pilot");
		swtGroup.setLayout(new FillLayout());
		
		swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		   
		logTextbox = new Text(swtSashForm, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
        logTextbox.setText(pilot.getLog().toString());
        
        Group swtLowerSashGroup = new Group(swtSashForm, SWT.NONE);
        swtLowerSashGroup.setLayout(new GridLayout(5, false));
		
    	Label lST = new Label(swtLowerSashGroup, SWT.NONE); lST.setText("Status:");
	    statusLabel = new Label(swtLowerSashGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
	    
        Label lSV = new Label(swtLowerSashGroup, SWT.NONE); lSV.setText("Server:");
	    
        swtTabFoler = new CTabFolder(swtLowerSashGroup, SWT.BORDER);
        swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
        
        stateSWTCtrl = new StateSWTControl(pilot, swtTabFoler, SWT.NONE);
        swtStateTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtStateTab.setControl(stateSWTCtrl.getSWTGroup());
        swtStateTab.setText("State");
        
        automationSWTCtrl = new AutomationSWTControl(pilot, swtTabFoler, SWT.NONE);
        swtAutomationTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtAutomationTab.setControl(automationSWTCtrl.getSWTGroup());
        swtAutomationTab.setText("Automation");
        
        simpleConfigSWTCtrl = new SimpleConfigSWTControl(pilot, swtTabFoler, SWT.NONE);
        swtSimpleConfigTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtSimpleConfigTab.setControl(simpleConfigSWTCtrl.getSWTGroup());
        swtSimpleConfigTab.setText("Pilot config");

        fullConfigSWTCtrl = new FullConfigSWTControl(pilot, swtTabFoler, SWT.NONE);
        swtFullConfigTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtFullConfigTab.setControl(fullConfigSWTCtrl.getSWTGroup());
        swtFullConfigTab.setText("Advanced config");

        autoconfSWTCtrl = new AutoconfigSWTControl(pilot, swtTabFoler, SWT.NONE);
        swtAutoconfTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtAutoconfTab.setControl(autoconfSWTCtrl.getSWTGroup());
        swtAutoconfTab.setText("Auto config");
            
        statusCommSWTCtrl = new StatusCommSWTControl(pilot, swtTabFoler, SWT.NONE);
        swtStatusCommTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtStatusCommTab.setControl(statusCommSWTCtrl.getSWTGroup());
        swtStatusCommTab.setText("Flight Controller");
        
        swtAutomationConfigTab = null;
        automationConfigCtrl = null;
        
        settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtLowerSashGroup, SWT.BORDER, pilot);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));

		doUpdate();

	}

	@Override
	public Object getInterfacingObject() { return swtGroup; }

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {			
			@Override
			public void run() {
				if(swtGroup.isDisposed())
					return;
				doUpdate();
			}
		});
	}

	protected void doUpdate() {		
			
		statusLabel.setText("(" + pilot.getStatusComm().getPilotStatus() + ") - " + pilot.getStatus());
		
		String str = pilot.getLog().toString();
		int startPos = logTextbox.getCharCount();
		if(str.length() < startPos){
			logTextbox.setText(str);
			
		}else if(str.length() > startPos){
			String appendString = str.substring(startPos);
			logTextbox.append(appendString);
		}
		
		stateSWTCtrl.doUpdate();
		automationSWTCtrl.doUpdate();
		simpleConfigSWTCtrl.doUpdate();
		autoconfSWTCtrl.doUpdate();
		statusCommSWTCtrl.doUpdate();

		GeriPilotConfig cfg = pilot.getConfig(); 

		if(cfg.automationProgram != null && cfg.automationProgram.equals("StandardAcquisition")) {
			if(automationConfigCtrl == null || !(automationConfigCtrl instanceof StandardAcqusitionConfigSWTControl)) {
				if(automationConfigCtrl != null)
					((StandardAcqusitionConfigSWTControl)automationConfigCtrl).destroy();
				if(swtAutomationConfigTab != null)
					swtAutomationConfigTab.dispose();
				
				
				automationConfigCtrl = new StandardAcqusitionConfigSWTControl(pilot, swtTabFoler, SWT.NONE);
				swtAutomationConfigTab = new CTabItem(swtTabFoler, SWT.NONE);
				swtAutomationConfigTab.setControl(((StandardAcqusitionConfigSWTControl)automationConfigCtrl).getSWTGroup());
				swtAutomationConfigTab.setText("Std. Acq. config");

				
			}
			
			((StandardAcqusitionConfigSWTControl)automationConfigCtrl).doUpdate();
			
		}else {
			if(automationConfigCtrl != null) {
				((StandardAcqusitionConfigSWTControl)automationConfigCtrl).destroy();
				automationConfigCtrl = null;
			}	
			if(swtAutomationConfigTab != null) {
				swtAutomationConfigTab.dispose();
				swtAutomationConfigTab = null;
			}
		}
	}


	@Override
	public void destroy() {
		if(!swtGroup.isDisposed())
			swtGroup.dispose();
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { 	return pilot;	}
	
}
