package imageProc.pilot.geri.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import algorithmrepository.Algorithms;
import imageProc.control.optecFilterWheel.OptecFilterWheelConfig;
import imageProc.core.ImageProcUtil;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.geri.GeriPilotConfig;
import imageProc.pilot.geri.ImageProcResource.GeriState;
import imageProc.pilot.geri.automation.StandardAcquisitionConfig;

public class SimpleConfigSWTControl {
	private GeriPilot pilot;

	private Group swtGroup;

	private Text codastationIDTextbox;
	private Text resourceServerPortTextbox;
	
	
	public SimpleConfigSWTControl(GeriPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		GeriPilotConfig config = pilot.getConfig();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Config");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(5, false));
        
        Label lCI = new Label(swtGroup, SWT.NONE); lCI.setText("Codastation ID:");
        codastationIDTextbox = new Text(swtGroup, SWT.NONE);
        codastationIDTextbox.setToolTipText("ID of the Codastation that will connect to this GERI resource server. e.g. 71301 for QSK ILS_Red. Used primarily to start, stop and monitor the codastation service.");
        codastationIDTextbox.setText("");
        codastationIDTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        codastationIDTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lRP = new Label(swtGroup, SWT.NONE); lRP.setText("Resource port:");
        resourceServerPortTextbox = new Text(swtGroup, SWT.NONE);
        resourceServerPortTextbox.setToolTipText("Port number of this GERI resource server. Usually 30303 for the first system on any PC, then 30304 etc. This must match the setting in the CoDAC configuration database for the sub-component.");
        resourceServerPortTextbox.setText("");
        resourceServerPortTextbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        resourceServerPortTextbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        resourceServerPortTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
                
       
        ImageProcUtil.addRevealWriggler(swtGroup);
        doUpdate();
	}
	

	protected void settingsChangingEvent(Event event) {
		GeriPilotConfig config = pilot.getConfig();
		
		
		config.codastationID = Algorithms.mustParseInt(codastationIDTextbox.getText());
		config.serverPort = Algorithms.mustParseInt(resourceServerPortTextbox.getText());		
	}


	public Control getSWTGroup() { return swtGroup; }

	public void doUpdate() {
		GeriPilotConfig config = pilot.getConfig();
		
		if(!codastationIDTextbox.isFocusControl())
			codastationIDTextbox.setText(Integer.toString(config.codastationID)); 
		if(!resourceServerPortTextbox.isFocusControl())
			resourceServerPortTextbox.setText(Integer.toString(config.serverPort));
	}

}
