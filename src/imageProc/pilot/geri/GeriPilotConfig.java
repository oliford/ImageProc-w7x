package imageProc.pilot.geri;

import java.util.ArrayList;
import java.util.Date;

import imageProc.pilot.common.CommonPilotConfig;
import imageProc.pilot.geri.ImageProcResource.GeriState;
import imageProc.pilot.geri.automation.StandardAcquisitionConfig;

public class GeriPilotConfig extends CommonPilotConfig {
	
	/** TCP Port on which to serve for primary GERI connection */
	public int serverPort = 30303;
	
	/** Automatically starts the GERI resource server when this config is loaded */
	public boolean startServerOnConfigLoad = false;
	
	public boolean automationEnable = true;
	
	public String automationProgram = "StandardAcquisition";
	
	public Object automationConfig = new StandardAcquisitionConfig();
	
	/** Send GERI state changes to flight controller (for distribution) */
	public boolean commStateChanges = false;

	/** ID of codastation. Used for systemctl start/stop/status, e.g. 71312 */
	public int codastationID = 71301;

	/** When waiting for acquisition and streaming to complete, give up after this long of nothing
	 * happening, i.e. no more images acquired and no more streamed. This must be longer than
	 * the frame time of the camera. 
	 * This should be at least a few times longer than archiveCheckPeriodMS
	 * */
	public long streamWaitIdleTimeoutMS = 30_000;
	
	/** How often to check the ArchiveDB to see if the streamed images have turned up. */
	public long archiveCheckPeriodMS = 3_000;

	/** When waiting for acquire/streaming completion, how long to wait in total before giving up.
	 * This can be long as it is basically superceeded by streamWaitIdleTimeout  */
	public double streamWaitTimeoutMS = 240 * 60_000; //ms
	
	/** How often to check up on the acquire/streaming progress abnd display it in the pilot log */
	public long streamWaitCheckPeriod = 1_000; //ms
	
	/** How long to wait for the program ID from webapi before making up a name with the time */
	public int programIDRequestTimeout = 2_000; //ms
	
	/** Webapi address for getting the W7X program. (This is not used for the trigger!) */
	public String webapiProgramTriggerAddress = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/Project.4096/ProgramLabelStart/parms/ProgramLogLabel/progId/_signal.json";

	public static enum StatusReportMode {
		/** Always report the GERI status as not OK */ 
		NOT_OK,
		
		/** Always report the GERI status as OK */
		ALWAYS_OK,
		
		/** Report status as OK if the codastation is running and the acqusition device is initialised and not in error state. */
		IS_READY;

		
		public static String[] names() {
			ArrayList<String> strs = new ArrayList<String>();
			for(StatusReportMode s : StatusReportMode.values()) 
				strs.add(s.name());
			return strs.toArray(new String[StatusReportMode.values().length]);
		}
	}
	
	/** What to report to the codastation about our status */
	public StatusReportMode statusReportMode = StatusReportMode.IS_READY;

	/** Period between checks of codastation status (via systemctl) [ms] */
	public long codastationCheckPeriod = 10000;
	
	/** Whether to start the codastation when resource server starts, (if it isn't already) */
	public boolean startCodastationOnResourceStart = false;

	/** Restart the codastation if it hasn't reconnected to the resource server within this period of 
	 * both the station and the resource server starting. Sometimes the codastation doesn't reopen the connections
	 * if the resource server (re)starts after the codastation. [ms] */
	public int restartCodastationTimeout = 30000;
	
	/** Codastation runs under user level codastation. Otherwise it runs from the root
	 * systemd and needs sudo allowances to start/stop them */
	public boolean systemdAsUser = false;

}
