package imageProc.pilot.w7x;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.widgets.Composite;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import algorithmrepository.Algorithms;
import descriptors.w7x.ArchiveDBDesc;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ConfigurableByID;
import imageProc.core.DatabaseSink;
import imageProc.core.EventReciever;
import imageProc.core.ImagePipeController;
import imageProc.core.ImageProcUtil;
import imageProc.database.gmds.GMDSSource;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.pilot.common.CommonPilot;
import imageProc.pilot.common.PilotStatusComm.StepStatus;
import imageProc.pilot.geri.ImageProcResource.GeriState;
import imageProc.pilot.w7x.swt.W7XPilotSWTControl;
import oneLiners.OneLiners;
import otherSupport.RandomManager;
import otherSupport.SettingsManager;
import w7x.archive.ArchiveDBFetcher;

/** W7X Pilot 
 * 
 * Sets-up and manages acquisition device and other diagnostic hardware 
 * according to status of W7X central control.
 *  
 *  
 * To start out usaing whatever information we can get from anywhere, then later using the t0 prep trigger from the TTE module
 *   - Codac web service?
 *   - 'software trigger' still possible?
 *   - connect PC to TTE module?
 *   - Get t0 status via Red Pitaya?
 *   - Some codac website?
 *   - mdsplus server as last resort
 * 
 * 
 * @author oliford
 *
 */
public class W7XPilot extends CommonPilot implements EventReciever, ConfigurableByID  {
	
	
	private W7XPilotConfig config = new W7XPilotConfig();		
	
	private W7XPilotSWTControl swtController;
	
	public String forceSimulatedProgram = null;
	public long forceSimulatedProgramStart = Long.MIN_VALUE;
	
	
	private ProgramRunner controller;
	private Thread controllerThread;

	/** inShotProc - the thing that drives the camera and saving etc for each shot */
	private InShotControl inShotCtrl;
	
	private boolean isInShotControl = false;

	
	private static NBITriggerReader nbiTriggerReader;
	
	private boolean isRegisteredWithNBITriggerReader = false;
	
	private long lastBroadcastedGERItimestamp = -1;
	private GeriState lastBroadcastedGERIState = GeriState.UNKNOWN;

	/** Old style in-java profile setup */
	public W7XPilot() {		
		this.inShotCtrl = new InShotControl(this);
		
		statusComm.setPilotStatus(StepStatus.NOT_STARTED);

		if(Algorithms.mustParseBoolean(SettingsManager.defaultGlobal().getProperty("imageProc.w7xPilot.inShot.startAtInit","false")))		
			runController();
	}

		
	

	@Override
	public ImagePipeController createPipeController(Class interfacingClass, Object args[], boolean asSink) {
		if(interfacingClass == Composite.class){
			if(swtController == null){
				swtController = new W7XPilotSWTControl(this, (Composite)args[0], (Integer)args[1]);
				controllers.add(swtController);
			}
			return swtController;
		}else
			return null;
	}

	@Override
	public boolean isIdle() { 	return true; 	} //we're always idle
	
	/** The main runner. This is running whenever the auto system is active 
	 * (even on non-operational days)
	 * 
	 * @author oliford
	 */
	private static class ProgramRunner implements Runnable {
		public boolean death = false;		
		private W7XPilot proc;
		private int id;
		private static int nextID = 0;
		
		public ProgramRunner(W7XPilot proc) {
			this.proc = proc;
		}
		
		@Override
		public void run() {
			proc.startAuto();
		}
	}
		
	/** The main auto-operation loop */
	private void startAuto(){	
		log.write("Controller thread running.");
		statusComm.setPilotStatus(StepStatus.WAITING);
		
		boolean surppressNotDayMsg = false;
		while(!controller.death){
			
			if(isInShotDay() || forceSimulatedProgramStart > 0){
				
				try{
					startDay();
				}catch(Throwable err){
					// Nothing should get down this far, but we'll catch in case it does so we don't just exit the thread.
					// This should loop back and restart the day controller (if we're in day)
					log.write("ERROR: Caught unhandled exception in control system somewhere. Will try wait 3secs before trying to re-start the day control. Error was: " + err);
					statusComm.setPilotStatus(StepStatus.ERROR);
					err.printStackTrace();
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) { }
				}finally {
					if(isRegisteredWithNBITriggerReader){
						NBITriggerReader.stop(this);	
						isRegisteredWithNBITriggerReader = false;
					}
				}
				surppressNotDayMsg = false;
			}else{	
				if(!surppressNotDayMsg){
					//message first time we decide not to run
					log.write("Not a shot day, or config is powerOff");
					surppressNotDayMsg = true;
				}
				
				if(controller.death)
					break;
				
				try {
					Thread.sleep(config.dayPollIntervalMS);
				} catch (InterruptedException e) {
					//user is getting bored, tell them we're waiting
					surppressNotDayMsg = false;
				}
			}
		}

		statusComm.setPilotStatus(StepStatus.NOT_STARTED);		
		log.write("Controller thread ended by death signal ");
		
	}
	
	
	/** Run by ProgrammRunner at the start of a shot day for automatic operation */
	private void startDay(){
		setStatus("Day control init");
			
		long waitingStartNanos = ArchiveDBDesc.toNanos(ZonedDateTime.now());
				
		boolean suppressShotWaitMsg = false;  
		while(!controller.death){
			
			statusComm.setPilotStatus(StepStatus.WAITING);			
			setStatus("Waiting for new program");
			if(!suppressShotWaitMsg)
				log.write("Waiting for new program");
		
			String programID = null;
			long progStartNanos = 0;
			

			if(Thread.interrupted()){ //clear interrupted flag
				continue;
			}
			
			if(forceSimulatedProgramStart > 0){ //override to force a program 
				
				programID = forceSimulatedProgram;
				progStartNanos = forceSimulatedProgramStart;
				
				//make damn sure the interrupt is cleared so that this actually happens
				Thread.interrupted();
			}else{
				// Wait for new programID from Archive webAPI
				// (don't do this if forcing, since the webapi might not be working)
				String dateStr = (new SimpleDateFormat("yyyyMMdd")).format(new Date());

				if(config.startOnNBITimer){
					Object progInfo[] = checkForNBITimer(waitingStartNanos, config.shotPollIntervalMS);
					
					if(progInfo != null){
						programID = (String) progInfo[0];
						progStartNanos = (long) progInfo[1];
					}
					
				}
				
				if(config.startOnW7XProgram){
					try {
						
						long progInfo[] = ArchiveDBFetcher.checkForNewProgramID(waitingStartNanos, 
																		config.shotPollIntervalMS, 
																		config.shotPollIntervalMS,
																		config.webapiProgramTriggerAddress);
						
						if(progInfo != null){
							programID = String.format("%s.%03d", dateStr, progInfo[0]); 
							progStartNanos = progInfo[1];
						}
					}catch(RuntimeException err) {
						
						String ipAddr;
						try{
							byte b[] = InetAddress.getByName(new URL(config.webapiProgramTriggerAddress).getHost()).getAddress();
							ipAddr = b[0] + "."	+ b[1] + "." + b[2] + "." + b[3];			
						}catch(IOException err2){ 
							ipAddr = "ResolveFailed(" + err2.getMessage() + ")";
						}
						log.write("ERROR: Exception reading WebAPI program 'trigger'. "
								+ ", url="+config.webapiProgramTriggerAddress
								+ ", ip=" + ipAddr
								+ ", Error=" + err.getMessage() );
						log.write("   Read");
						
						if(config.webapiProgramTriggerAddress.contains("sv-coda-arc-")) {
							int r = (int)RandomManager.instance().nextUniform(10, 17);
							config.webapiProgramTriggerAddress = config.webapiProgramTriggerAddress
																		.replaceAll("sv-coda-arc-[0-9]*", "sv-coda-arc-" + Integer.toString(r));
						}
						
					}
					
				}
				
			}
			
			if(programID == null && config.startOnGERIBroadcastInit) {
				long t = lastBroadcastedGERItimestamp;
				GeriState state = lastBroadcastedGERIState;
				
				
				if( state == GeriState.INIT
						&& t > waitingStartNanos
						&& (System.currentTimeMillis() - t/1_000_000L) > config.geriBroadcastStartDelayMS) {
					//we got bored waiting for the archive, and someone else ran with GERI, so we'll do that
					log.write("WARNING: Starting shot due to GERI state change to INIT broadcast via FlightControl - did the webapi fail?");
					
					progStartNanos = t;
					programID = ImageProcUtil.nanotimeLikeProgramID(t); 			
				}
			}
			
			if(programID != null){				
				if(controller.death){
					log.write("Pilot ignoring shot because death already signalled.");
					break; 
				}
				
				log.write("New program with ID="+programID+" at t0=" + progStartNanos);
				try{
					isInShotControl = true;
					statusComm.setPilotStatus(StepStatus.ACTIVE);
					
					inShotCtrl.start(programID, progStartNanos);		
					
					
				}finally{
								
					isInShotControl = false;	
					forceSimulatedProgram = null;
					forceSimulatedProgramStart = Long.MIN_VALUE;
					waitingStartNanos = progStartNanos + 2; //go from of last shot, in case one has run in the meantime 	
				}
				
				//just finished a shot, now's probably a good time to do the sync
				if(config.clockSyncAfterShot)
					syncToW7XClock();
				
				suppressShotWaitMsg = false;
				
			}else{
				if(!suppressShotWaitMsg)
					log.write("No new program ");
				
				suppressShotWaitMsg = true; //don't keep repeating this
				
				//wait for a bit before polling for next shot
				if(controller.death)
					break;
				try {
					Thread.sleep(config.shotPollIntervalMS);
				} catch (InterruptedException e) {
					//the user appears bored, so tell them we're just waiting
					suppressShotWaitMsg = false;
				}	
			}
			
			
			if(!isInShotDay() && !controller.death){
				checkCameraIsOff();
				break;
			}
		}
		
		statusComm.setPilotStatus(StepStatus.WAITING);
		setStatus("Day control ending.");
		log.write("Day control ending.");
	}
	

	
	public void runController() {
		inhibitDay = null;
		
		if(controller != null){
			killController(true); 
			//don't wait for it, it can take an hour
			
			/* No, actually we have to wait for it since 
				every controller runs in the same W7XPilot,
				ProgamRunner just calls the since entity pilot.
				So, death exists only once for all thread.
				The new thread clears the death flag on start, so
				the old thread won't die
			*/
		}

		controller = new ProgramRunner(this);
		log.write("Starting controller thread");
		controllerThread = new Thread(controller);
		controllerThread.start();
	}
	
	private void killController(boolean waitForDeath) {
		log.write("Killing controller thread ");
		if(controller == null)
			return;
			
		controller.death = true;
		controllerThread.interrupt(); 
		
		try {
			if(waitForDeath)
				while(controllerThread.isAlive())
					Thread.sleep(10);
		} catch (InterruptedException e) { }
	}

	public void forceShot(String programName, long programStartNanos){
		forceSimulatedProgram = programName;
		forceSimulatedProgramStart = programStartNanos;

		if(controller != null && controllerThread.isAlive()){ //if we have a controller already
			controllerThread.interrupt(); //kick it
		}else{
			runController(); //otherwise start a one
		}
	}

	public void forceShot() {		
		long tNow = System.currentTimeMillis();
		forceSimulatedProgram = ImageProcUtil.nanotimeLikeProgramID(tNow * 1_000_000L);
		forceSimulatedProgramStart = (tNow - config.acquireStartDelayMS) * 1_000_000L;
		
		if(controller != null && controllerThread.isAlive()){ //if we have a controller already
			controllerThread.interrupt(); //kick it
		}else{
			runController(); //otherwise start a one
		}
	}
	
	@Override
	/** Make visible for AugControlIPS */
	public void updateAllControllers() {
		super.updateAllControllers();
	}
	
	/** Aborts/kills the day controller, without turning anything off */
	public void abortController() {
		//signal the controller to die, and kill the AUGshotnr process
		killController(false);		
	}

	@Override
	public void destroy() {
		super.destroy();
		killController(false);
		log.close();
		inShotCtrl = null;
		powerCtrl = null;		
	}
	
	public String getStatus() { return status; }
	private void setStatus(String status){ 
		this.status = status; 
		updateAllControllers();
	}
	
	/** Stop whatever delay the controller is currently in */
	public void kickController(){ if(controllerThread != null) controllerThread.interrupt(); }
	
	public InShotControl getInShotCtrl() { return this.inShotCtrl; }
	
	public boolean isInShotControl(){ return isInShotControl; }
	
	String bulkProcessSyncObj = "bulkProcessSyncObj";

	private String lastLoadedConfig;
	
	/** Bulk process W7X source by descriptor:
	 * @param descriptor:
	 *  	date - YYYMMDD all shots on day
	 *  	shotrange - YYYMMDD.SSS-SSS
	 *  	filename - list of shots */
	public void bulkProcessW7X(String descriptor) {
		if(!(connectedSource instanceof W7XArchiveDBSource)){
			log.write("BulkProcess: ERROR: Source isn't W7XArchiveDBSource ");
			return;
		}
		if(controllerThread != null && controllerThread.isAlive()){
			log.write("BulkProcess: ERROR: Controller is running, stop it first!");
			return;
		}	
		boolean isFile = false;
		
		if(descriptor.startsWith("file://")){
			isFile = true;
			descriptor = descriptor.substring(7);			
		}else{
			isFile = (new File(descriptor)).canRead();
		}
			
		if(isFile){
			String ids[] = OneLiners.linesFromFile(descriptor);	
			List<Object> idList = new ArrayList<Object>();
			for(String id : ids)
				idList.add(id);
			
			bulkProcess(idList);
			
			return;
			
		}
		
		Pattern shotRangeOnDay = Pattern.compile("([0-9]*)\\.([0-9]*)-([0-9]*)");
		Pattern dayRange = Pattern.compile("([0-9]*)-([0-9]*)");

		Matcher m = shotRangeOnDay.matcher(descriptor.trim());			
		if(m.matches()) {
			String dateStr = m.group(1);
			int expNum0 = Integer.parseInt(m.group(2));
			int expNum1 = Integer.parseInt(m.group(3));
			
			List<Object> idList = new ArrayList<Object>();
			for(int expNum=expNum0; expNum <= expNum1; expNum++)
				idList.add(dateStr + "." + String.format("%03d", expNum));
			
			bulkProcess(idList);
			
			return;
			
		}
		
		m = dayRange.matcher(descriptor.trim());
		if(m.matches()) {
			String dateStr1 = m.group(1);
			String dateStr2 = m.group(2);
			int expNum0 = 1;
			int expNum1 = 99;
			
			SimpleDateFormat parser = new SimpleDateFormat("yyyyMMdd");
			Date d1,d2;
			try {
				d1 = parser.parse(dateStr1);
				d2 = parser.parse(dateStr2);
			} catch (ParseException e) {
				throw new RuntimeException("Unable to parse date " + dateStr1 + " or " + dateStr2);
			}
			
			List<Object> idList = new ArrayList<Object>();
			
			while(!d1.after(d2)) {
				for(int expNum=expNum0; expNum <= expNum1; expNum++)
					idList.add(parser.format(d1) + "." + String.format("%03d", expNum));
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(d1);
				cal.add( Calendar.DATE, 1 );
				d1 = cal.getTime();
			}
			
			
			bulkProcess(idList);
			
			return;
		}		
		
		throw new RuntimeException("Description '" + descriptor + " ' was not recognised as list file or program range");
		
	}
	
	public void bulkProcessGMDS(int databaseID0, int databaseID1) {
		if(!(connectedSource instanceof GMDSSource)){
			log.write("BulkProcess: ERROR: Source isn't GMDSSource ");
			return;
		}
		if(controllerThread != null && controllerThread.isAlive()){
			log.write("BulkProcess: ERROR: Controller is running, stop it first!");
			return;
		}		
		
		ArrayList<Object> idList = new ArrayList<Object>();
		for(int id=databaseID0; id <= databaseID1; id++)
			idList.add(id);
	
		log.write("Bulk processing GMDSSource from " + databaseID0 + " to "  +databaseID1 + ".");
		bulkProcess(idList);
		
	}
	
	public void bulkProcess(List<Object> idList){
		ImageProcUtil.ensureFinalUpdate(bulkProcessSyncObj, new Runnable() {
			@Override
			public void run() {
				try {
					inShotCtrl.doBulkProcess(idList);
					
				} catch (InterruptedException err) {
					err.printStackTrace();
				}
			}
		});
	}

	@Override
	public boolean getAutoUpdate() { return false; }
	@Override
	public void setAutoUpdate(boolean enable) { }
	@Override
	public void calc() { 	}
	@Override
	public boolean wasLastCalcComplete(){ return true; }
	@Override
	public void abortCalc(){ }
	

	@Override
	public void event(Event event) {
		switch(event) {
		case GlobalAbort:
			abortProcessing();
			break;
		default:
			break;
		}		
	}
	
	@Override
	public String toShortString() { return "W7XPilot"; }

	@Override
	protected boolean isControllerAborting(){ return controller.death; }

	/** Look at signal for NBI trigger from 'Gatner modules' and return if an NBI test shot has started */
	private Object[] checkForNBITimer(long waitingStartNanos, int shotPollIntervalMS2) {

		if(!isRegisteredWithNBITriggerReader){
			NBITriggerReader.start(this);
			isRegisteredWithNBITriggerReader = true;
		}
		
		long lastRisingEdge = NBITriggerReader.getLastRisingEdge();
				
		if(lastRisingEdge < waitingStartNanos){
			//System.out.println(systemID + ": No new trigger");
			return null;
		}
		
		ZonedDateTime triggerTime = ArchiveDBDesc.fromNanos(lastRisingEdge);
		System.out.println(systemID + ": Got NBI trigger at " + triggerTime);
		
		String id = triggerTime.format(DateTimeFormatter.ofPattern("YYYYMMdd"))+
				".NBI_" +
				triggerTime.format(DateTimeFormatter.ofPattern("HHmm"));
		
		return new Object[]{ id, lastRisingEdge };
		
	}
	
	@Override
	public W7XPilotConfig getConfig() { return config;	}

	@Override
	public void saveConfig(String id){
		if(id.startsWith("jsonfile:")){
			saveConfigJSON(id.substring(9));
		}
	}
	
	public void saveConfigJSON(String fileName){
		Gson gson = new GsonBuilder()
						.serializeSpecialFloatingPointValues()
						.serializeNulls()
						.setPrettyPrinting()
						.create(); 

		String json = gson.toJson(config);
		
		json = json.replaceAll("},", "},\n");
		
		OneLiners.textToFile(fileName, json);
	}
	
	@Override
	public void loadConfig(String id){
		if(id.startsWith("jsonfile:"))
			loadConfigJSON(id.substring(9));
		else
			throw new RuntimeException("Unknown config type " + id);
		lastLoadedConfig = id;
		if(config.startOnLoad)
			runController();
		if(config.statusComm.startOnConfigLoad)
			statusComm.startListening();
	}
	
	@Override
	public String getLastLoadedConfig() { return lastLoadedConfig;	}
	
	public void loadConfigJSON(String fileName) {
		String jsonString = OneLiners.fileToText(fileName);
		
		Gson gson = new Gson();
		
		config = gson.fromJson(jsonString, W7XPilotConfig.class);
		if(config.camera.timeoutWaitingForOpen == 0) {
			config.applyProfileDefaults();
			config.camera.timeoutWaitingForOpen = 40000;
		}
		
		updateAllControllers();
	}

	@Override
	protected void broadcastedGeriStateChange(int state, long timestamp) {
		GeriState geriState = GeriState.byValue(state);
		log.write("FlightControl broadcasted GERI state change to " + geriState + " at " + timestamp);
		
		lastBroadcastedGERIState = geriState;
		lastBroadcastedGERItimestamp = timestamp;
		
		if(config.autoconf.abortOnGeriPOST && geriState == GeriState.POST)
			((AcquisitionDevice)connectedSource).abort();
		
		boolean configInterruptOnGERIINIT = true;
		if(configInterruptOnGERIINIT && geriState == GeriState.INIT){
			if(controllerThread != null)
				abortProcessing();
		}
		
	}

	protected void doSinkSave(DatabaseSink sink, String programID) throws InterruptedException {
		inShotCtrl.doSinkSave(sink, programID);
	}
}
