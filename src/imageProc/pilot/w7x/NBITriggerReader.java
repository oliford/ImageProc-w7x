package imageProc.pilot.w7x;

import java.util.ArrayList;

import descriptors.w7x.ArchiveDBDesc;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import signals.w7x.ArchiveDB;
import w7x.archive.ArchiveDBFetcher;

/** Provides NBI trigger from database signal to multiple pilots within this java VM */
public abstract class NBITriggerReader {
	
	public static ArchiveTriggerReader atrs[] = new ArchiveTriggerReader[0];
	public static ArrayList<W7XPilot> pilotsWaiting = new ArrayList<W7XPilot>();
	
	public static synchronized void start(W7XPilot pilot){
		
		if(atrs != null) {
			for(ArchiveTriggerReader atr : atrs)
				atr.stop(false);
		}
	
		//make a new ATR incase the trigger dbPath changed
		String dbPath = pilot.getConfig().nbiTimerSignal;
		if(dbPath == null) { //old way was to take it from profile
			//dbPath = SettingsManager.defaultGlobal().getProperty(
			//		"imageProc.w7xPilot.inShot.nbiTriggerSignal", 
			//		"w7x/ArchiveDB/codac/W7X/ControlStation.2092/BE000_DATASTREAM/16/Trigger_BoxTimerNI21");
			dbPath = SettingsManager.defaultGlobal().getProperty(
					"imageProc.w7xPilot.inShot.nbiTriggerSignal", 
					"w7x/ArchiveDB/codac/W7X/ControlStation.2176/BE000_DATASTREAM/16/Trigger_BoxTimerNI20");
			pilot.getConfig().nbiTimerSignal = dbPath;
		}
		
		String dbPaths[] = dbPath.split("[;:]");
		
		atrs = new ArchiveTriggerReader[dbPaths.length];
		
		for(int i=0; i < atrs.length; i++) {
			
			atrs[i] = new ArchiveTriggerReader(new ArchiveDBDesc(dbPaths[i]));
		
			synchronized (pilotsWaiting) {
				if(pilotsWaiting.contains(pilot))
					System.err.println("Pilot " + pilot + " was already waiting for NBI trigger !?!.");
				else
					pilotsWaiting.add(pilot);
				
				if(!atrs[i].isActive())
					atrs[i].start();
			}
		}
	}
	
	public static void stop(W7XPilot pilot){
		synchronized (pilotsWaiting) {
			if(!pilotsWaiting.remove(pilot))
				System.err.println("Pilot " + pilot + " was not waiting for NBI trigger but wants to stop!?!.");
			
			if(pilotsWaiting.size() == 0){
				System.out.println("Stopping NBI trigger waiting as last pilot has stopped.");

				for(ArchiveTriggerReader atr : atrs)
					atr.stop(false);
			}
		}
	}
	
	private static boolean anyActive() {
		for(ArchiveTriggerReader atr : atrs)
			if(atr.isActive())
				return true;
		
		return false;
	}

	public static long getLastRisingEdge() {
		long lastTrigger = 0;
		
		boolean anyActive = false;
		for(ArchiveTriggerReader atr : atrs) {
			if(atrs != null && atr.isActive()) {				
				anyActive = true;
				long t = atr.getLastRisingEdge();
				if(t > lastTrigger)
					lastTrigger = t;
			}
		}
		if(!anyActive)
			throw new RuntimeException("No ArchiveTriggerReader active.");
		return lastTrigger;
		
	}
	
	/*public static int getLastValue() {
		return (int)atr.getLastRisingEdgeValue();
	}*/
	

	public static void main(String[] args) {
		//hack test for alias resolution - doesnt work :(
		//String path = "w7x/ArchiveDB/views/KKS/CDX21_NBI_Box21/PLC_Data/1Hz/CDX21_SHOT_Nummer Value";
		/*String path = "w7x/ArchiveDB/views/KKS/CDX21_NBI_Box21/PLC_Data/1Hz/St13Off";		
		ArchiveDBDesc desc = new ArchiveDBDesc(path);
		desc.setFromNanos(ArchiveDBDesc.toNanos(2022, 04, 31, 12, 0, 0, 0));
		desc.setToNanos(ArchiveDBDesc.toNanos(2022, 04, 31, 13, 0, 0, 0));
		//desc.setNanosRangeAllTime();
		ArchiveDBDesc desc2 = ArchiveDBFetcher.defaultInstance().resolveAlias(desc);
		System.out.println(desc2);
		*/
		
		ArchiveDBDesc desc2 = new ArchiveDBDesc("w7x/ArchiveDB/codac/W7X/ControlStation.2081/PLC.1_DATASTREAM/1192/CDX21_SHOT_Nummer Value");

		desc2.setFromNanos(ArchiveDBDesc.toNanos(2022, 05, 31, 12, 0, 0, 0));
		desc2.setToNanos(ArchiveDBDesc.toNanos(2022, 05, 31, 13, 0, 0, 0));		
		ArchiveDB sig = (ArchiveDB) ArchiveDBFetcher.defaultInstance().getSig(desc2);
		System.out.println(sig.toString());
		
	}
}
