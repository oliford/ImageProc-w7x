package imageProc.pilot.w7x;

import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.logging.Logger;

import imageProc.core.AcquisitionDevice;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.DatabaseSink;
import imageProc.core.EventReciever;
import imageProc.core.EventReciever.Event;
import imageProc.core.ImageProcUtil;
import imageProc.core.Img;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.database.gmds.GMDSSource;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSink;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSource;
import imageProc.pilot.common.PilotStatusComm.ProgramStatus;
import imageProc.pilot.common.PilotStatusComm.StepStatus;
import imageProc.sources.capture.andorCam.AndorCamSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import w7x.archive.ArchiveDBFetcher;

public class InShotControl {
	private String status = "Init";
	
	private W7XPilot pilot;
	
	public InShotControl(W7XPilot pilot) {
		this.pilot = pilot;
		
		W7XPilotConfig config = pilot.getConfig();
		config.applyProfileDefaults();	
	}
	
	public String getStatus() {
		return null;
	}
	
	/** This is the guts of the controller, it is fired whenever a new shot number is distributed 
	 * @param programID Program ID within day.
	 * @param programStartNanos Starting time of program
	 */
	public void start(String programID, long programStartNanos) {
		W7XPilotConfig config = pilot.getConfig();
		long t0, t1; //local timeout things, not to do with triggers 
						
		pilot.getStatusComm().setAcquireStatus(programID, StepStatus.NOT_STARTED);
		pilot.getStatusComm().setSaveStatus(programID, StepStatus.NOT_STARTED);
		pilot.getStatusComm().setProcessStatus(programID, StepStatus.NOT_STARTED);
		
		ImgSource connectedSource = pilot.getConnectedSource();
		if(!(connectedSource instanceof AcquisitionDevice))
			throw new RuntimeException("Connected source is not an AcquisitionDevice. In-shot control not possible!");
		
		AcquisitionDevice acq = (AcquisitionDevice)connectedSource;
		try{
			if(config.shotInhibit){				
				pilot.getLog().write("InShot: Controller start inhibited");
				return;
			}
			status = "Controller started";
			
			pilot.getLog().write("InShot: Started in-shot for program #" + programID);
			
			//if we are to hit the acq enable later, at some given time, disable it now
			boolean delayedAcquisitionEnable = (config.acquireStartDelayMS > 0);
			acq.enableAcquire(!delayedAcquisitionEnable);
			
			//trigger abort to stop and clear everything
			connectedSource.broadcastEvent(Event.GlobalAbort);
			pilot.getLog().write("InShot["+programID+"]: Triggered global abort (as preparation)");			
			Thread.sleep(config.postTriggerDelay);
			
			pilot.attemptAcquisitionPrep(programID, programStartNanos, delayedAcquisitionEnable);

			//abort and then delay the start of capture 
			//(for dodgy cameras that like to fail randomly while waiting for acquire, like the Andor Neo/Zyla)
			if(config.recaptureDelayMS > 0) {
				acq.abort();

				pilot.getLog().write("InShot["+programID+"]: Aborting and waiting for delayed capture...");
				while(System.currentTimeMillis() < (programStartNanos/1_000_000 + config.recaptureDelayMS)) {
					Thread.sleep(1);
				}
				
				pilot.getLog().write("InShot["+programID+"]: Restarting delayed capture...");
				pilot.attemptAcquisitionPrep(programID, programStartNanos, delayedAcquisitionEnable);
			}
			
			//we are to hit the 'acquire' (something like software trigger) at a given nanoTime
			if(config.acquireStartDelayMS > 0) { 
				while(System.currentTimeMillis() < (programStartNanos/1_000_000 + config.acquireStartDelayMS)) {
					Thread.sleep(1);
				}
				acq.enableAcquire(true); //fire the acqusition
				pilot.getLog().write("InShot["+programID+"]: Acqusition enable triggered.");
			}
			pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ACTIVE);
						
			try{
				pilot.awaitCaptureCompletion(programID);
			
				connectedSource.setSeriesMetaData("inshot/acqStatus", acq.getAcquisitionStatus().toString(), false);
			
			}catch(RuntimeException err){ //timed out or other error waiting for camera to actually take images, or during taking them
				pilot.getLog().write("InShot["+programID+"]: Timed out waiting for acquire on " + connectedSource + ". Aborting it.");
				pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);

				//shot may or may not be over, but we've no way of telling yet, so assume it is.
				//in any case: report the problem, clean things up and try to reset if necessary
				postMortem(programID);

				return; //no point in doing anything else, since resetting the camera probably lost the images
			}
			
			//camera finished taking images
			connectedSource.setSeriesMetaData("inshot/acqStatus", acq.getAcquisitionStatus().toString(), false);
		
			pilot.getLog().write("InShot["+programID+"]: Acquire done on " + connectedSource + ". Captured "+
					acq.getNumAcquiredImages() +" / "+connectedSource.getNumImages()+"images.");
			
			pilot.getStatusComm().setAcquireStatus(programID, StepStatus.SUCCESS);
									
			connectedSource.setSeriesMetaData("inshot/status", "AcquiredOK", false);				
			attemptSave(programID);	
		
		
			//mcastTriggers.kill();
		
			
			int numCaptured = -1;
			if(connectedSource instanceof AndorCamSource){
				numCaptured = ((AndorCamSource)connectedSource).getNumCaptured();
			}
			
			if(numCaptured == 0 || (config.acquisition.checkIfCameraBroken && isCameraBroken())){
				//proc.logWrite("InShot["+programID+"]: Camera is broken, so resetting it and not processing.");
				pilot.getLog().write("InShot["+programID+"]: Camera is broken, RESET INHIBIT IN S/W.");
				pilot.getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
				//resetCamera(t0Nano);
				return;
			}
			
			if(!config.processing.processChainInhibit){
				pilot.getLog().write("InShot["+programID+"]: Waiting " + config.processing.processChainDelayMS + "ms before processing.");
				
				pilot.getStatusComm().setProcessStatus(programID, StepStatus.SUCCESS);
			 	Thread.sleep(config.processing.processChainDelayMS);
			 	
			 	pilot.getStatusComm().setProcessStatus(programID, StepStatus.ACTIVE);
			 	try{
			 		//our own processing consists of making a sensible time vector
			 		pilot.createTimebases(programStartNanos, -1);
			 		
			 		pilot.processChain(programID, true);
			 		
			 		pilot.getStatusComm().setProcessStatus(programID, StepStatus.SUCCESS);
			 		pilot.getLog().write("InShot["+programID+"]: Processing complete.");
			 		
			 	}catch(RuntimeException err){
			 		pilot.getStatusComm().setProcessStatus(programID, StepStatus.ERROR);
					throw err;
			 	}
			}
			
			//abort everything to make sure
			connectedSource.broadcastEvent(Event.GlobalAbort);
			Thread.sleep(config.postTriggerDelay);
			
			//everything done, hopefully now is a good time to garbage collect
			Logger logr = Logger.getLogger(this.getClass().getName());
			logr.info("Forcing garbage collection");
			System.gc();
			logr.info("Garbage collection done");
			
		
		}catch(InterruptedException err){
			status = "Controller error";
			pilot.getLog().write("InShot: Controller interrupted, triggering general abort.");
			connectedSource.broadcastEvent(Event.GlobalAbort);
			
		}catch(Exception err){
			status = "Controller error";
			pilot.getLog().write("InShot: Controller exception: " + err);
			err.printStackTrace();
			connectedSource.broadcastEvent(Event.GlobalAbort);
			
		}finally{
			//any stage statuses remaining on transient states become errored, since
			//it was probably that stage that caused the error
			ProgramStatus stat = pilot.getStatusComm().mustGetProgramState(programID);
			if(stat.acquireStatus == StepStatus.ACTIVE || stat.acquireStatus == StepStatus.WAITING)
				stat.acquireStatus = StepStatus.ERROR;
			if(stat.saveStatus == StepStatus.ACTIVE || stat.saveStatus == StepStatus.WAITING)
				stat.saveStatus = StepStatus.ERROR;
			if(stat.processStatus == StepStatus.ACTIVE || stat.processStatus == StepStatus.WAITING)
				stat.processStatus = StepStatus.ERROR;
		}
				
	}
	
	

	/** Called when acquisition fails. 
	 * Report problem, clean up and make a decision if camera should be reset.
	 * @throws InterruptedException */
	private void postMortem(String programID) throws InterruptedException {
		W7XPilotConfig config = pilot.getConfig();
		boolean resetCamera;
		ImgSource connectedSource = pilot.getConnectedSource();

		AcquisitionDevice acq = (AcquisitionDevice)connectedSource;

		Status aqStatus = acq.getAcquisitionStatus();
		pilot.getLog().write("postMortem(): AcquisitionDevice.getAcquisitionStatus() = " + aqStatus);
		pilot.getLog().write("postMortem(): AcquisitionDevice.getAcquisitionStatusString() = " + acq.getAcquisitionStatusString());

		if(aqStatus != Status.awaitingHardwareTrigger && aqStatus != Status.capturing){

			pilot.getLog().write("InShot["+programID+"]: ERROR: ** AcquisitionDevice was NOT ready to acquire! **");
			//This can't happen!, so only the user can fix this
			connectedSource.setSeriesMetaData("inshot/status", "AcquireNotReady", false);
			resetCamera = true;
		}else{
			if(aqStatus != Status.awaitingHardwareTrigger && aqStatus != Status.capturing){

				//if the camera simply never triggered, that's ok, probably TS06 never came
				//but if the camera died in the middle, we probably need to reset it

				pilot.getLog().write("InShot["+programID+"]: AcquisitionDevice was ready to acquire but didn't start, probably no H/W trigger.");
				connectedSource.setSeriesMetaData("inshot/status", "AcquireNotStarted", false);
				resetCamera = false;
			}else{
				pilot.getLog().write("InShot["+programID+"]: AcquisitionDevice started acquiring but never finished, will reset it.");
				connectedSource.setSeriesMetaData("inshot/status", "AcquireTimedOut", false);
				resetCamera = true;
			}
		}


		connectedSource.broadcastEvent(Event.GlobalAbort);
		Thread.sleep(config.postTriggerDelay);

		attemptSave(programID); //attempt to save whatever we did get first, don't process

		if(resetCamera)
			pilot.resetCamera(programID);
		
		connectedSource.broadcastEvent(Event.GlobalAbort);		
	}

	
	private void attemptSave(String programID) throws InterruptedException {
		ImgSource connectedSource = pilot.getConnectedSource();
		pilot.clearAbortProcessing(); //really save, don't let earlier aborts drop it out
		
		pilot.getStatusComm().setSaveStatus(programID, StepStatus.ACTIVE);
		
		status = "Saving";
		pilot.updateAllControllers();
			
		//find the database things directly connected to the source, and trigger a save
		int nSaved = 0;
		for(ImgSink sink : connectedSource.getConnectedSinks()){
			if(sink instanceof DatabaseSink){
				doSinkSave((DatabaseSink)sink, programID);
				if(((DatabaseSink)sink).wasLastCalcComplete())
					nSaved++;
			}
		}
		if(nSaved == 0){
			pilot.getLog().write("InShot["+programID+"]: WARNING: No DatabaseSink found or none were completely successful, *** IMAGES MAY NOT BE SAVED ***");
			pilot.getStatusComm().setSaveStatus(programID, StepStatus.ERROR);
			
		}else{
			pilot.getStatusComm().setSaveStatus(programID, StepStatus.SUCCESS);			
		}
	}
	
	public void doSinkSave(DatabaseSink sink, String programID) throws InterruptedException {
		W7XPilotConfig config = pilot.getConfig();
		pilot.getLog().write("InShot["+programID+"]: Triggering save on " + sink.getClass().getSimpleName());
		//make something like an ID		
		sink.triggerSave();
		
		long t0 = System.currentTimeMillis();
		long t1 = t0;
		
		//wait for the sink to do something
		while(sink.isIdle() && (t1 - t0) < config.processing.databaseSaveTimeoutMS){
			t1=System.currentTimeMillis();
			Thread.sleep(config.busyPollDelayMS);
			if(pilot.isAbortProcessingSignalled())
				throw new RuntimeException("Processing aborted in doSinkSave()");
		
		}
		pilot.getLog().write("InShot: Save started");
		
		//and for it to stop doing it
		while(!sink.isIdle() && (t1 - t0) < config.processing.databaseSaveTimeoutMS){
			t1=System.currentTimeMillis();
			Thread.sleep(config.busyPollDelayMS);
			if(pilot.isAbortProcessingSignalled())
				throw new RuntimeException("Processing aborted in doSinkSave()");
		}
		
		
		if((t1 - t0) >= config.processing.databaseSaveTimeoutMS){
			pilot.getLog().write("InShot["+programID+"]: Timed out waiting for save on " + sink + ". Aborting it.");
			if(sink instanceof EventReciever)
				((EventReciever)sink).event(Event.GlobalAbort);			
			Thread.sleep(config.postTriggerDelay);
			if(pilot.isAbortProcessingSignalled())
				throw new RuntimeException("Processing aborted in doSinkSave()");
		}else{
			pilot.getLog().write("InShot["+programID+"]: Save done on " + sink);
			if((sink instanceof W7XArchiveDBSink))
				pilot.getLog().write("InShot["+programID+"]: Save status:" + ((W7XArchiveDBSink)sink).getStatus());
		}		
	}
	
	public void doBulkProcess(List<Object> idList) throws InterruptedException{
		W7XPilotConfig config = pilot.getConfig();
		
		pilot.clearAbortProcessing();
		for(Object idObj : idList){
			
			try{
				if(pilot.isAbortProcessingSignalled())
					return;
				
				ImgSource src = pilot.getConnectedSource();
				if(src instanceof GMDSSource){
					int id = (Integer)idObj;
				
					GMDSSource gmdsSrc = (GMDSSource)src;
					pilot.getLog().write("Bulk processing GMDSSource, now at " + id);
					
					String exp = gmdsSrc.getExperiment();
					String path = gmdsSrc.getPath();			
					gmdsSrc.loadTarget(exp, id, path);
					
				}else if(src instanceof W7XArchiveDBSource){
					String progID = (String)idObj;
					long t0, t1;
					
					W7XArchiveDBSource w7xSrc = (W7XArchiveDBSource) src;
					
					if(progID.matches("[0-9]*_[0-9]*")){
						//look like a nanostamp range
						String parts[] = progID.split("_");
						t0 = Long.parseLong(parts[0]);
						t1 = Long.parseLong(parts[1]);
					}else{
						int intDate = (int)Double.parseDouble(progID);
						int year = intDate / 10000; intDate -= year * 10000;
						int month = intDate / 100; intDate -= month * 100;
						int day = intDate;
						int expNum = Integer.parseInt(progID.split("\\.")[1]);
						
						ArchiveDBFetcher adb = w7xSrc.getADBFetcher();
						t0 = adb.getFromNanos(year, month, day, expNum);
						t1 = adb.getUptoNanos(year, month, day, expNum);
						t1 += config.postShotToleranceMS * 1_000_000L;
					}
					pilot.getLog().write("Bulk processing W7XArchiveDBSource, now at progID=" + progID);
					w7xSrc.loadTarget(w7xSrc.getPath(), t0, t1);
					
				}else{ 
					throw new RuntimeException("Unsupported source");
				}
	
				long t0 = System.currentTimeMillis();
				long t1 = t0;
				//wait for the sink to do something
				while(src.isIdle() && (t1 - t0) < config.processing.databaseSaveTimeoutMS){
					t1=System.currentTimeMillis();
					Thread.sleep(config.busyPollDelayMS);
				}
				pilot.getLog().write("InShot.BulkProcess: Started loading from source " + src);
	
				//and for it to stop doing it
				while(!src.isIdle() && (t1 - t0) < config.processing.databaseSaveTimeoutMS){
					t1=System.currentTimeMillis();
					Thread.sleep(config.busyPollDelayMS);
				}
				pilot.getLog().write("InShot.BulkProcess: Loading complete from source " + src);
				
				//check there are actually some images 
				int nImages = src.getNumImages();
				int nImagesExisting = 0;
				for(int i=0; i < nImages; i++){
					if(src.getImage(i) != null && src.getImage(i).isRangeValid()){
						nImagesExisting++;
					}
				}
				if(nImagesExisting <= 0){
					pilot.getLog().write("InShot.BulkProcess: No images for " + idObj);
					continue;
				}
				
				int nMeta = src.getCompleteSeriesMetaDataMap().entrySet().size();
				pilot.getLog().write("InShot.BulkProcess: Loaded " + nImagesExisting + " of " + nImages + " images and " + nMeta + " metadata entries.");
				
				pilot.processChain(idObj.toString(), false);
				
				pilot.getLog().write("InShot.BulkProcess: Processing complete.");
			}catch(RuntimeException err){
				pilot.getLog().write("BulkProcess: ERROR: Exception while processing "+idObj+": " + err);
				err.printStackTrace();
			}
			
		}

	}
	
	/** Checks the images to see if the camera has failed*/
	public boolean isCameraBroken(){
		final int maxZeroPixels = 50; // [A. Burckhard], no idea how he worked this out
		
		ImgSource src = pilot.getConnectedSource();
		Img image = null;
		for(int i=src.getNumImages()-2; i >= 0; i--){
			image = src.getImage(i);
			if(image != null && image.isRangeValid())
				break;
		}
		if(image == null){
			pilot.getLog().write("InShot: isCameraBroken(): No valid images, assuming camera is ok.");
			return false;
		}
		
		int nZeroPixels = 0;
		try{ 
			ReadLock readLock = image.readLock();
			readLock.lockInterruptibly(); 
			try{
				//when the row is longer than the actual number of pixels
				//the end of the row is sometimes 0, which we should ignore for this
				int rowWidth;
				for(rowWidth = image.getWidth(); rowWidth >= 0; rowWidth--){
					if(rowWidth == 0){
						nZeroPixels = rowWidth * image.getHeight(); //all broken, give up
						break;
					}
						
					if(image.getPixelValue(readLock, rowWidth-1, image.getHeight()/2) != 0)
						break;
				}
				
				
				for(int y=0; y < image.getHeight(); y++)
					for(int x=0; x < rowWidth; x++)
						if(image.getPixelValue(readLock, x, y) == 0)
							nZeroPixels++;
				
			
			}finally{
				readLock.unlock();
			}			
		}catch(InterruptedException e){
			pilot.getLog().write("InShot: isCameraBroken(): Interrupted, not sure the decision is right.");			
		}	
		
		String str = "InShot: isCameraBroken(): Found " + nZeroPixels + " pixels with value == 0. ";
		if(nZeroPixels >= maxZeroPixels){
			pilot.getLog().write(str + "** CAMERA IS BROKEN!**");
			return true;
		}else{
			pilot.getLog().write(str + "Camera seems to be fine.");
			return false;
			
		}
		
	}

}
