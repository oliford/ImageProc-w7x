package imageProc.pilot.w7x.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;

import imageProc.core.ImageProcUtil;
import imageProc.pilot.w7x.W7XPilot;
import imageProc.pilot.w7x.W7XPilotConfig;

public class InShotSWTControl {
	private Group swtGroup;
	
	private Label statusLabel;
	private Button forceStartButton; 
	private Button inhibitControlCheckbox;	
	private Spinner acquireStartSpinner;	
	private Spinner captureStartSpinner;	
	private Spinner acquireTimeoutSpinner;
	private Spinner saveTimeoutSpinner;
	private Spinner procTimeoutSpinner;
	private Button reopenAcquiDriverCheckbox;
	private Button resetAcquiPowerCheckbox;
	private Button checkCameraBrokenCheckbox;
	private Spinner powerOffTimeSpinner;
	private Spinner openTimeoutSpinner;
	private Label isCameraBrokenLabel;
	private Button isCameraBrokenButton;
	
	private Spinner processChainDelaySpinner;	
	private Button processChainInhibitCheckbox;
	private Button processChainButton;
	
	private W7XPilot pilot;
	
	public InShotSWTControl(W7XPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		W7XPilotConfig config = pilot.getConfig();

        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Shot Control");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(5, false));
        ImageProcUtil.addRevealWriggler(swtGroup);
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
        statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
        
        Label lSC = new Label(swtGroup, SWT.NONE); lSC.setText("Shot controller:");
        inhibitControlCheckbox = new Button(swtGroup, SWT.CHECK);
        inhibitControlCheckbox.setText("Inhibit shots");
        inhibitControlCheckbox.setToolTipText("Dont respond to shots despite running pilot");
        inhibitControlCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        inhibitControlCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
       
        forceStartButton = new Button(swtGroup, SWT.PUSH);
        forceStartButton.setText("Force shot now");
        forceStartButton.setToolTipText("Forces a complete automation cycle. Begins acquisition immediately, skipping the start delay wait time.");
        forceStartButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        forceStartButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pilot.forceShot(); }});

        Label lCS = new Label(swtGroup, SWT.NONE); lCS.setText("Capture (ms after prog start):\n(t0=1s,t1=61s)");
        captureStartSpinner = new Spinner(swtGroup, SWT.NONE);
        captureStartSpinner.setToolTipText("Time from 'program start' at which to begin capture. If set (>0), the capture will be started at program start, aborted and then restarted at this time.");
        captureStartSpinner.setValues(config.acquireStartDelayMS, 0, Integer.MAX_VALUE, 0, 10, 60);
        captureStartSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        captureStartSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
                
        Label lAS = new Label(swtGroup, SWT.NONE); lAS.setText("Acquire (ms after prog start):\n(t0=1s,t1=61s)");
        acquireStartSpinner = new Spinner(swtGroup, SWT.NONE);
        acquireStartSpinner.setToolTipText("Time from 'program start' at which to begin acqusition. Program start is typically 61s before the plasma breakdown.");
        acquireStartSpinner.setValues(config.acquireStartDelayMS, 0, Integer.MAX_VALUE, 0, 10, 60);
        acquireStartSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        acquireStartSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        
        Label lAT = new Label(swtGroup, SWT.NONE); lAT.setText("Acquire timeout (secs):");
        acquireTimeoutSpinner = new Spinner(swtGroup, SWT.NONE);
        acquireTimeoutSpinner.setToolTipText("If acquisition takes longer than this, assume the camera got stuck or failed, save any data collected and reset everything.");
        acquireTimeoutSpinner.setValues(config.acquisition.cameraCaptureTimeoutMS/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
    	acquireTimeoutSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        acquireTimeoutSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lST = new Label(swtGroup, SWT.NONE); lST.setText("Save timeout (secs):");
        saveTimeoutSpinner = new Spinner(swtGroup, SWT.NONE);
        saveTimeoutSpinner.setToolTipText("If saving takes longer than this, give up on the remaining data and get ready for the next shot.");
        saveTimeoutSpinner.setValues(config.processing.databaseSaveTimeoutMS/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        saveTimeoutSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        saveTimeoutSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lPT = new Label(swtGroup, SWT.NONE); lST.setText("Processing timeout (secs):");
        procTimeoutSpinner = new Spinner(swtGroup, SWT.NONE);
        procTimeoutSpinner.setToolTipText("If processing takes longer than this, give up on the remaining data and get ready for the next shot.");
        procTimeoutSpinner.setValues(config.processing.processingTimeoutMS/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        procTimeoutSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        procTimeoutSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lRC = new Label(swtGroup, SWT.NONE); lRC.setText("When stuck acquiring:");
        reopenAcquiDriverCheckbox = new Button(swtGroup, SWT.CHECK);
        reopenAcquiDriverCheckbox.setSelection(config.camera.openCloseAcquisition);
        reopenAcquiDriverCheckbox.setText("Close+open device");
        reopenAcquiDriverCheckbox.setToolTipText("If camera isn't ready at start, or if it gets stuck when acquiring (acquire timeout) then close and open the camera driver.");
        reopenAcquiDriverCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        reopenAcquiDriverCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
        
        resetAcquiPowerCheckbox = new Button(swtGroup, SWT.CHECK);
        resetAcquiPowerCheckbox.setSelection(config.camera.resetPowerOnFault);
        resetAcquiPowerCheckbox.setText("Cycle power");
        resetAcquiPowerCheckbox.setToolTipText("If camera isn't ready at start, or if it gets stuck when acquiring (acquire timeout) then power off and on the camera (as well as open/close). This requires a power control module.");
        resetAcquiPowerCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        resetAcquiPowerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
        
        //Label lRC2 = new Label(swtGroup, SWT.NONE); lRC2.setText(" ");
        Label lCT = new Label(swtGroup, SWT.NONE); lCT.setText("Off time (secs):");
        powerOffTimeSpinner = new Spinner(swtGroup, SWT.NONE);
        powerOffTimeSpinner.setToolTipText("If camera isn't ready at start, or if it gets stuck when acquiring (acquire timeout) then close and open the camera driver.");
        powerOffTimeSpinner.setValues(config.camera.durationResetPowerOff/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        powerOffTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        powerOffTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lOT = new Label(swtGroup, SWT.NONE); lOT.setText("Open timeout (secs):");
        openTimeoutSpinner = new Spinner(swtGroup, SWT.NONE);
        openTimeoutSpinner.setValues((int)(config.camera.timeoutWaitingForOpen/1000), 1, Integer.MAX_VALUE, 0, 10, 60);
        openTimeoutSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        openTimeoutSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lPCD = new Label(swtGroup, SWT.NONE); lPCD.setText("Process after (secs):");
        processChainDelaySpinner = new Spinner(swtGroup, SWT.NONE);
        processChainDelaySpinner.setToolTipText("How long to wait after acuisition and saving are complete before starting processing of other modules. This is useful if those modules need data that might not be ready for some seconds.");
        processChainDelaySpinner.setValues(config.processing.processChainDelayMS/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        processChainDelaySpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        processChainDelaySpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        processChainInhibitCheckbox = new Button(swtGroup, SWT.CHECK);
        processChainInhibitCheckbox.setSelection(config.processing.processChainInhibit);
        processChainInhibitCheckbox.setText("Inhibit");
        processChainInhibitCheckbox.setToolTipText("Disables processing by misc modules.");
        processChainInhibitCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        processChainInhibitCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
         
        processChainButton = new Button(swtGroup, SWT.PUSH);
        processChainButton.setText("Force process now");
        processChainButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        processChainButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { pilot.forceProcess(); }});
        
        Label lB = new Label(swtGroup, SWT.NONE); lB.setText("");
        checkCameraBrokenCheckbox = new Button(swtGroup, SWT.CHECK);
        checkCameraBrokenCheckbox.setSelection(config.camera.resetPowerOnFault);
        checkCameraBrokenCheckbox.setText("Check camera for 0 pixels to decide if it is 'broken'");
        checkCameraBrokenCheckbox.setToolTipText("Check if camera is broken (has pixels with value 0) and abort saving and processing.");
        checkCameraBrokenCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        checkCameraBrokenCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
        
        Label lCB = new Label(swtGroup, SWT.NONE); lCB.setText("Camera status: ");
        isCameraBrokenLabel = new Label(swtGroup, SWT.NONE); isCameraBrokenLabel.setText("Not checked yet.");
        
        isCameraBrokenButton = new Button(swtGroup, SWT.PUSH);
        isCameraBrokenButton.setText("Check now");
        isCameraBrokenButton.setToolTipText("Check ");
        isCameraBrokenButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
        isCameraBrokenButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { isCameraBrokenButtonEvent(event); }});
        
        ImageProcUtil.addRevealWriggler(swtGroup);
        pilot.updateAllControllers();
	}

	protected void isCameraBrokenButtonEvent(Event event) {
		isCameraBrokenLabel.setText(pilot.getInShotCtrl().isCameraBroken() ? "Yes" : "No");
	}

	protected void settingsChangingEvent(Event event) {
		W7XPilotConfig config = pilot.getConfig();
		
		config.shotInhibit = inhibitControlCheckbox.getSelection();
		config.acquireStartDelayMS = acquireStartSpinner.getSelection();
		config.recaptureDelayMS = captureStartSpinner.getSelection();
		config.acquisition.cameraCaptureTimeoutMS = acquireTimeoutSpinner.getSelection()*1000;
		config.processing.databaseSaveTimeoutMS = saveTimeoutSpinner.getSelection()*1000;
		config.processing.processingTimeoutMS = procTimeoutSpinner.getSelection()*1000;
		config.processing.processChainDelayMS = processChainDelaySpinner.getSelection()*1000;
		config.processing.processChainInhibit = processChainInhibitCheckbox.getSelection();
		config.acquisition.checkIfCameraBroken = checkCameraBrokenCheckbox.getSelection();
		config.camera.openCloseAcquisition = reopenAcquiDriverCheckbox.getSelection();	
		config.camera.resetPowerOnFault = resetAcquiPowerCheckbox.getSelection();	
		config.camera.durationResetPowerOff = powerOffTimeSpinner.getSelection()*1000;
		config.camera.timeoutWaitingForOpen = openTimeoutSpinner.getSelection()*1000;
		
	}
	
	public void doUpdate() {
		W7XPilotConfig config = pilot.getConfig();

		String s = pilot.getInShotCtrl().getStatus();
		statusLabel.setText(s == null ? "null???" : s);	
		
		inhibitControlCheckbox.setSelection(config.shotInhibit);
		acquireStartSpinner.setSelection(config.acquireStartDelayMS);
		captureStartSpinner.setSelection(config.recaptureDelayMS);
		acquireTimeoutSpinner.setSelection(config.acquisition.cameraCaptureTimeoutMS/1000);		
		saveTimeoutSpinner.setSelection(config.processing.databaseSaveTimeoutMS/1000);
		procTimeoutSpinner.setSelection(config.processing.processingTimeoutMS/1000);
		processChainDelaySpinner.setSelection(config.processing.processChainDelayMS/1000);
		processChainInhibitCheckbox.setSelection(config.processing.processChainInhibit);
		checkCameraBrokenCheckbox.setSelection(config.acquisition.checkIfCameraBroken);
		reopenAcquiDriverCheckbox.setSelection(config.camera.openCloseAcquisition);	
		resetAcquiPowerCheckbox.setSelection(config.camera.resetPowerOnFault);	
		powerOffTimeSpinner.setSelection(config.camera.durationResetPowerOff/1000);
		openTimeoutSpinner.setSelection((int)(config.camera.timeoutWaitingForOpen/1000));

	}

	public Control getSWTGroup() { return swtGroup; }
}
