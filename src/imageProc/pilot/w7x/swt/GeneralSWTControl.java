package imageProc.pilot.w7x.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.pilot.w7x.W7XPilot;
import imageProc.pilot.w7x.W7XPilotConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class GeneralSWTControl {
	private Group swtGroup;
	
	private Spinner shotPollSpinner;
	private Button startOnConfigLoadCheckbox;
	private Button startOnW7XProgramCheckbox;
	private Button startOnWNBITriggerCheckbox;
	private Button startOnGeriBroadcastInitCheckbox;
	private Text nbiTriggerSignalTextbox;
	private Text w7xTriggerSignalTextbox;
	private Text geriTriggerDelayTextbox;
	
	private Button allowRemoteConfigButton;
		
	private W7XPilot proc;
	
	public GeneralSWTControl(W7XPilot pilot, Composite parent, int style) {
		this.proc = pilot;
		W7XPilotConfig config = proc.getConfig();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Day");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(3, false));
        
        Label lSP = new Label(swtGroup, SWT.NONE); lSP.setText("Shot poll period (secs):");
        shotPollSpinner = new Spinner(swtGroup, SWT.NONE);
        shotPollSpinner.setValues(config.shotPollIntervalMS/1000, 1, Integer.MAX_VALUE, 0, 10, 60);
        shotPollSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        shotPollSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});

        startOnConfigLoadCheckbox = new Button(swtGroup, SWT.CHECK);
        startOnConfigLoadCheckbox.setText("Start pilot on config load.");
        startOnConfigLoadCheckbox.setSelection(config.startOnLoad);
        startOnConfigLoadCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
        startOnConfigLoadCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });

        Label lSO = new Label(swtGroup, SWT.NONE); lSO.setText("Start on:");
		lSO.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
		
        startOnW7XProgramCheckbox = new Button(swtGroup, SWT.CHECK);
        startOnW7XProgramCheckbox.setText("W7X programs from Web API:");
        startOnW7XProgramCheckbox.setSelection(config.startOnW7XProgram);
        startOnW7XProgramCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
        startOnW7XProgramCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
		
        w7xTriggerSignalTextbox = new Text(swtGroup, SWT.NONE);
		w7xTriggerSignalTextbox.setText("?");
		w7xTriggerSignalTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
		w7xTriggerSignalTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
 
        startOnWNBITriggerCheckbox = new Button(swtGroup, SWT.CHECK);
        startOnWNBITriggerCheckbox.setText("NBI trigger from signal:");
        startOnWNBITriggerCheckbox.setSelection(config.startOnNBITimer);
        startOnWNBITriggerCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		startOnWNBITriggerCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
		    	
		nbiTriggerSignalTextbox = new Text(swtGroup, SWT.NONE);
        nbiTriggerSignalTextbox.setText("?");
        nbiTriggerSignalTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
        nbiTriggerSignalTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        startOnGeriBroadcastInitCheckbox = new Button(swtGroup, SWT.CHECK);
        startOnGeriBroadcastInitCheckbox.setText("Trigger from GERI after(ms):");
        startOnGeriBroadcastInitCheckbox.setToolTipText("If after the given delay no trigger has been found from the other sources, react to any GERI state change to INIT broadcasted via the FlightControl connection. This will start a shot without a correct ID, since that's not available.");
        startOnGeriBroadcastInitCheckbox.setSelection(config.startOnGERIBroadcastInit);
        startOnGeriBroadcastInitCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
        startOnGeriBroadcastInitCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
		 
        geriTriggerDelayTextbox = new Text(swtGroup, SWT.NONE);
        geriTriggerDelayTextbox.setText("?");
        geriTriggerDelayTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 2, 1));
        geriTriggerDelayTextbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        allowRemoteConfigButton = new Button(swtGroup, SWT.CHECK);
        allowRemoteConfigButton.setText("Allow remote configuration by FlgihtControl (ExposureTime, nImage ");
        allowRemoteConfigButton.setSelection(config.allowRemoteConfig);
		allowRemoteConfigButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
		allowRemoteConfigButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
		       
		ImageProcUtil.addRevealWriggler(swtGroup);
		
		pilot.updateAllControllers();
	}



	protected void settingsChangingEvent(Event event) {
		W7XPilotConfig config = proc.getConfig();
		config.shotPollIntervalMS = shotPollSpinner.getSelection()*1000;
		
		config.startOnLoad = startOnConfigLoadCheckbox.getSelection();
		config.startOnW7XProgram = startOnW7XProgramCheckbox.getSelection();
		config.startOnNBITimer = startOnWNBITriggerCheckbox.getSelection();
		config.startOnGERIBroadcastInit = startOnGeriBroadcastInitCheckbox.getSelection();
		config.nbiTimerSignal = nbiTriggerSignalTextbox.getText();
		config.webapiProgramTriggerAddress = w7xTriggerSignalTextbox.getText();
		config.geriBroadcastStartDelayMS = Algorithms.mustParseLong(geriTriggerDelayTextbox.getText());
		config.allowRemoteConfig = allowRemoteConfigButton.getSelection();
		
	}

	public void doUpdate() {
		W7XPilotConfig config = proc.getConfig();
		startOnConfigLoadCheckbox.setSelection(config.startOnLoad);
		allowRemoteConfigButton.setSelection(config.allowRemoteConfig);
		startOnW7XProgramCheckbox.setSelection(config.startOnW7XProgram);
		startOnWNBITriggerCheckbox.setSelection(config.startOnNBITimer);
		startOnGeriBroadcastInitCheckbox.setSelection(config.startOnGERIBroadcastInit);
		if(!nbiTriggerSignalTextbox.isFocusControl())
			nbiTriggerSignalTextbox.setText(config.nbiTimerSignal == null ? "" : config.nbiTimerSignal);
		if(!w7xTriggerSignalTextbox.isFocusControl())
			w7xTriggerSignalTextbox.setText(config.webapiProgramTriggerAddress == null ? "" : config.webapiProgramTriggerAddress);
		if(!geriTriggerDelayTextbox.isFocusControl())
			geriTriggerDelayTextbox.setText(Long.toString(config.geriBroadcastStartDelayMS));
	}

	public Control getSWTGroup() { return swtGroup; }
}
