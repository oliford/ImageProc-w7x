package imageProc.pilot.w7x.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.core.GeneralController;
import imageProc.core.ImagePipeController;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.swt.SWTSettingsControl;
import imageProc.database.json.JSONFileSettingsControl;
import imageProc.pilot.common.swt.DaySWTControl;
import imageProc.pilot.common.swt.StatusCommSWTControl;
import imageProc.pilot.geri.swt.AutomationSWTControl;
import imageProc.pilot.w7x.W7XPilot;

public class W7XPilotSWTControl implements ImagePipeController {
	private Group swtGroup;
	
	private W7XPilot proc;
	
	private Label statusLabel;
	private Text logTextbox;
	private SashForm swtSashForm;
		
	private Button runControllerButton; 
  	private Button nudgeControllerButton; 
  	private Button abortControllerButton; 
  	
	private CTabFolder swtTabFoler;	
	private CTabItem swtDayTab;
	private CTabItem generalDayTab;
	private CTabItem swtInShotTab;
	private CTabItem swtProcTab;
	private CTabItem swtAutoconfTab;
	private CTabItem swtStatusCommTab;
	
	private DaySWTControl daySWTCtrl;
	private GeneralSWTControl generalSWTCtrl;
	private GeneralController ipsController;
	private InShotSWTControl inShotSWTCtrl;
	private AutoconfigSWTControl autoconfSWTCtrl;
	private ProcessingSWTControl procSWTCtrl;
	private StatusCommSWTControl statusCommSWTCtrl;
	
	private SWTSettingsControl settingsCtrl;
	
	public W7XPilotSWTControl(W7XPilot pilot, Composite parent, int style) {
		this.proc = pilot;
		
		swtGroup = new Group(parent, style);
		swtGroup.setText("W7X Pilot");
		swtGroup.setLayout(new FillLayout());
		
		swtSashForm =  new SashForm(swtGroup, SWT.VERTICAL | SWT.BORDER);
		   
		logTextbox = new Text(swtSashForm, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
        logTextbox.setText(proc.getLog().toString());
        
        Group swtLowerSashGroup = new Group(swtSashForm, SWT.NONE);
        swtLowerSashGroup.setLayout(new GridLayout(5, false));
		
    	Label lS = new Label(swtLowerSashGroup, SWT.NONE); lS.setText("Status:");
	    statusLabel = new Label(swtLowerSashGroup, SWT.NONE);
        statusLabel.setText("Init");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
	    
        
        runControllerButton = new Button(swtLowerSashGroup, SWT.PUSH);
		runControllerButton.setText("Start");
		runControllerButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
		runControllerButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.runController(); } });

		nudgeControllerButton = new Button(swtLowerSashGroup, SWT.PUSH);
		nudgeControllerButton.setText("Nudge");
		nudgeControllerButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false,1, 1));
		nudgeControllerButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.kickController(); } });
		
		abortControllerButton = new Button(swtLowerSashGroup, SWT.PUSH);
		abortControllerButton.setText("Stop");
		abortControllerButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false,1, 1));
		abortControllerButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.abortController(); } });
		
		swtTabFoler = new CTabFolder(swtLowerSashGroup, SWT.BORDER);
        swtTabFoler.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1));
        
        generalSWTCtrl = new GeneralSWTControl(proc, swtTabFoler, SWT.NONE);
        generalDayTab = new CTabItem(swtTabFoler, SWT.NONE);
        generalDayTab.setControl(generalSWTCtrl.getSWTGroup());
        generalDayTab.setText("General");
        
        daySWTCtrl = new DaySWTControl(proc, swtTabFoler, SWT.NONE);
        swtDayTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtDayTab.setControl(daySWTCtrl.getSWTGroup());
        swtDayTab.setText("Day");               
               
        inShotSWTCtrl = new InShotSWTControl(proc, swtTabFoler, SWT.NONE);
        swtInShotTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtInShotTab.setControl(inShotSWTCtrl.getSWTGroup());
        swtInShotTab.setText("In Shot");
        
        procSWTCtrl = new ProcessingSWTControl(proc, swtTabFoler, SWT.NONE);
        swtProcTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtProcTab.setControl(procSWTCtrl.getSWTGroup());
        swtProcTab.setText("Processing");
        
        autoconfSWTCtrl = new AutoconfigSWTControl(proc, swtTabFoler, SWT.NONE);
        swtAutoconfTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtAutoconfTab.setControl(autoconfSWTCtrl.getSWTGroup());
        swtAutoconfTab.setText("Auto config");
        
        statusCommSWTCtrl = new StatusCommSWTControl(proc, swtTabFoler, SWT.NONE);
        swtStatusCommTab = new CTabItem(swtTabFoler, SWT.NONE);
        swtStatusCommTab.setControl(statusCommSWTCtrl.getSWTGroup());
        swtStatusCommTab.setText("Flight Controller");
                
        settingsCtrl = new JSONFileSettingsControl();
		settingsCtrl.buildControl(swtLowerSashGroup, SWT.BORDER, proc);
		settingsCtrl.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 5, 1));

	}
	
	@Override
	public Object getInterfacingObject() {
		return swtGroup;
	}

	@Override
	public void generalControllerUpdate() {
		if(swtGroup.isDisposed())
			return;
		
		ImageProcUtil.ensureFinalSWTUpdate(swtGroup.getDisplay(), this, new Runnable() {
			
			@Override
			public void run() {
				doUpdate();
			}
		});
	}

	protected void doUpdate() {
		if(swtGroup.isDisposed())
			return;
			
		statusLabel.setText(proc.getStatus());
		
		int scrollPos = logTextbox.getTopIndex();
		int caretLine = logTextbox.getCaretLineNumber();
		int nLines = logTextbox.getLineCount();
		
		String str = proc.getLog().toString();
		int startPos = logTextbox.getCharCount();
		if(str.length() < startPos){
			logTextbox.setText(str);
		}else{
			String appendString = str.substring(startPos);
			logTextbox.append(appendString);
		}
		
		//if not at end, keep scroll position
		if(caretLine < (nLines - 1))
			logTextbox.setTopIndex(scrollPos);		
		
		if(ipsController != null)
			ipsController.generalControllerUpdate();
		generalSWTCtrl.doUpdate();
		daySWTCtrl.doUpdate();
		inShotSWTCtrl.doUpdate();
		autoconfSWTCtrl.doUpdate();
		statusCommSWTCtrl.doUpdate();
	}


	@Override
	public void destroy() {
		if(!swtGroup.isDisposed())
			swtGroup.dispose();
	}

	@Override
	public ImgSourceOrSinkImpl getPipe() { 	return proc;	}
	
	
}
