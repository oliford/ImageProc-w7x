package imageProc.pilot.w7x.swt;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.AcquisitionDevice;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSource;
import imageProc.pilot.common.CommonPilot;
import imageProc.pilot.common.CommonPilotConfig;
import imageProc.pilot.common.CommonPilotConfig.Autoconfig.Modes;
import imageProc.pilot.w7x.W7XPilot;
import imageProc.pilot.w7x.W7XPilotConfig;

public class AutoconfigSWTControl {
	private Group swtGroup;
	
	private Label statusLabel;
	private Spinner shotLengthSpinner;
	private Spinner extraLengthSpinner;
	private Button acceptFromFlightControlCheckbox;
	private Button enabledInAcqDeviceCheckbox;
	private Button abortOnGeriPOSTCheckbox;
	
	private Group settingModeGroup;
	private Button settingModeRadios[];
	
	private Spinner maxFrameCountSpinner;
	private Spinner frameCountSpinner;
	private Label frameCountReadback; 
	
	private Spinner minFrameTimeSpinner;
	private Spinner frameTimeSpinner;
	private Label frameTimeReadback;
	
	private Button applyNowButton;
	
	private CommonPilot pilot;
	
	private boolean dataChangeInhibit = false;
	
	public AutoconfigSWTControl(CommonPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		CommonPilotConfig config = pilot.getConfig();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Day");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(6, false));
        ImageProcUtil.addRevealWriggler(swtGroup);
        

        Label lB1 = new Label(swtGroup, SWT.NONE); lB1.setText("");        
        acceptFromFlightControlCheckbox = new Button(swtGroup, SWT.CHECK);
        acceptFromFlightControlCheckbox.setText("Accept from flight control");
        acceptFromFlightControlCheckbox.setToolTipText("Use the shot length when it's transmitted from the flight controller");
        acceptFromFlightControlCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
        acceptFromFlightControlCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
        
        Label lB2 = new Label(swtGroup, SWT.NONE); lB2.setText("");      
        enabledInAcqDeviceCheckbox = new Button(swtGroup, SWT.CHECK);
        enabledInAcqDeviceCheckbox.setText("Allowed by acquisition module");
        enabledInAcqDeviceCheckbox.setToolTipText("Shows whether autoconfig is supported, enabled and allowed by the source acquisition module.");
        enabledInAcqDeviceCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { 
	        	ImgSource src = pilot.getConnectedSource();
				enabledInAcqDeviceCheckbox.setSelection(src instanceof AcquisitionDevice && ((AcquisitionDevice)src).isAutoconfigAllowed());			
	        } });
        enabledInAcqDeviceCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
        
        Label lB3 = new Label(swtGroup, SWT.NONE); lB3.setText("");        
        abortOnGeriPOSTCheckbox = new Button(swtGroup, SWT.CHECK);
        abortOnGeriPOSTCheckbox.setText("Abort on GERI POST (from flight control)");
        abortOnGeriPOSTCheckbox.setToolTipText("Abort the acquisition early when flight controller reports that some other GERI pilot's state changed to POST.");
        abortOnGeriPOSTCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
        abortOnGeriPOSTCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); } });
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");        
       	statusLabel = new Label(swtGroup, SWT.NONE);
       	statusLabel.setToolTipText("Status of the autoconfiguration");
       	statusLabel.setText("init\n\n\n.");
       	statusLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
       	statusLabel.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
         

        Label lSL = new Label(swtGroup, SWT.NONE); lSL.setText("Shot length:");        
       	shotLengthSpinner = new Spinner(swtGroup, SWT.NONE);
        shotLengthSpinner.setToolTipText("Shot length. Can be manually entered here, or updated from FlightControl");
        shotLengthSpinner.setValues(-1, -1, Integer.MAX_VALUE, 3, 100, 1000);
        shotLengthSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        shotLengthSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
         
        Label lXL = new Label(swtGroup, SWT.NONE); lXL.setText("Extend:");
        lXL.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        extraLengthSpinner = new Spinner(swtGroup, SWT.NONE);
       	extraLengthSpinner.setToolTipText("Additional time to acquire for (to extend before or after shot)");
       	extraLengthSpinner.setValues(-1, -1, Integer.MAX_VALUE, 3, 100, 1000);
       	extraLengthSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
       	extraLengthSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
         
		

        settingModeGroup = new Group(swtGroup, SWT.BORDER); 
        settingModeGroup.setText("Configuration mode");
        settingModeGroup.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 6, 1));
        settingModeGroup.setLayout(new GridLayout(6, false));
        
        Modes[] modes = Modes.values();
        settingModeRadios = new Button[modes.length];
        for(int i=0; i < modes.length; i++) {
	        settingModeRadios[i] = new Button(settingModeGroup, SWT.RADIO);
	        settingModeRadios[i].setText(modes[i].toString());
	        settingModeRadios[i].setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
	        settingModeRadios[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        }                  
        
        Label lFC = new Label(swtGroup, SWT.NONE); lFC.setText("Frame count: Set:");        
        
        frameCountSpinner = new Spinner(swtGroup, SWT.NONE);
        frameCountSpinner.setToolTipText("Number of frames currently calculated to set (if enabled)");
        frameCountSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 10, 100);
        frameCountSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
        frameCountSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { frameCountEvent(event); }});
        
        frameCountReadback = new Label(swtGroup, SWT.NONE);
        frameCountReadback.setText("Current: ???");
        frameCountReadback.setToolTipText("The current real frame count set at the acquisition module. This is used to calculate the real run time.");
        frameCountReadback.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        
        Label lMF = new Label(swtGroup, SWT.NONE); lMF.setText("max:");
        maxFrameCountSpinner = new Spinner(swtGroup, SWT.NONE);
        maxFrameCountSpinner.setToolTipText("Maximum number of frames the device can record. If setFrameTime is also enabled, "
        		+ "the frames will be elongated when so that this is not exceeded.");
        maxFrameCountSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 10, 60);
        maxFrameCountSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        maxFrameCountSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lFT = new Label(swtGroup, SWT.NONE); lFT.setText("Frame period [ms]:");        
        
        frameTimeSpinner = new Spinner(swtGroup, SWT.NONE);
        frameTimeSpinner.setToolTipText("Frame time to set to the acqusition module (which may not happen)");
        frameTimeSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 100, 1000);
        frameTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false, 1, 1));
        frameTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { frameTimeEvent(event); }});
        
        frameTimeReadback = new Label(swtGroup, SWT.NONE);
        frameTimeReadback.setText("Current: ???");
        frameTimeReadback.setToolTipText("The current real frame time set at the acquisition module. This is used to calculate the real run time.");
        frameTimeReadback.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        
        Label lMT = new Label(swtGroup, SWT.NONE); lMT.setText("min:");
        minFrameTimeSpinner = new Spinner(swtGroup, SWT.NONE);
        minFrameTimeSpinner.setToolTipText("Minimum frame time period in ms to allow when setFrameTime is true.");
        minFrameTimeSpinner.setValues(-1, -1, Integer.MAX_VALUE, 0, 100, 1000);
        minFrameTimeSpinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
        minFrameTimeSpinner.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
        
        Label lA = new Label(swtGroup, SWT.NONE); lA.setText("");        
        applyNowButton = new Button(swtGroup, SWT.NONE);
        applyNowButton.setToolTipText("Calculate the autoconfiguration, attempt to apply the result to the camera and read back the actual run length.");
        applyNowButton.setText("Apply");
        applyNowButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        applyNowButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { applyButtonEvent(event); }});
        
        ImageProcUtil.addRevealWriggler(swtGroup);
		pilot.updateAllControllers();
	}



	protected void applyButtonEvent(Event event) {
		pilot.calcAutoconfig(true);
	}



	protected void frameCountEvent(Event event) {
		//it makes no sense to just set the transient one in the config, so also try to apply it to the source
		CommonPilotConfig config = pilot.getConfig();
		config.autoconf.frameCount = frameCountSpinner.getSelection();
		((AcquisitionDevice)pilot.getConnectedSource()).setConfigParameter("FrameCount", Integer.toString(config.autoconf.frameCount));
		pilot.calcAutoconfig(false);
		pilot.updateAllControllers();
	}
	
	protected void frameTimeEvent(Event event) {
		//it makes no sense to just set the transient one in the config, so also try to apply it to the source
		CommonPilotConfig config = pilot.getConfig();
		config.autoconf.framePeriodUS = frameTimeSpinner.getSelection() * 1000;
		((AcquisitionDevice)pilot.getConnectedSource()).setConfigParameter("FramePeriod", Double.toString(config.autoconf.framePeriodUS / 1.0e6));
		pilot.calcAutoconfig(false);
		pilot.updateAllControllers();
	}

	protected void settingsChangingEvent(Event event) {
		if(dataChangeInhibit)
			return;
		
		dataChangeInhibit = true;
		try {
			
			CommonPilotConfig config = pilot.getConfig();
			config.autoconf.shotLengthMS = shotLengthSpinner.getSelection();
			config.autoconf.extraLengthMS = extraLengthSpinner.getSelection();
	
			config.autoconf.acceptShotLengthFromFlightControl = acceptFromFlightControlCheckbox.getSelection();
			Modes[] modes = Modes.values();
	        for(int i=0; i < modes.length; i++)	
	        	if(settingModeRadios[i].getSelection())
	        		config.autoconf.mode = modes[i];
	        
			config.autoconf.abortOnGeriPOST = abortOnGeriPOSTCheckbox.getSelection();
			config.autoconf.maxFrameCount = maxFrameCountSpinner.getSelection();
			config.autoconf.minFramePeriodUS = minFrameTimeSpinner.getSelection() * 1000;			

			pilot.calcAutoconfig(false);
		}finally {
			dataChangeInhibit = false;
		}
		pilot.updateAllControllers();
	}

	public void doUpdate() {
		if(dataChangeInhibit)
			return;
		
		dataChangeInhibit = true;
		try {
			//pilot.calcAutoconfig(false);
			
			CommonPilotConfig config = pilot.getConfig();
			shotLengthSpinner.setSelection((int)config.autoconf.shotLengthMS);
			extraLengthSpinner.setSelection((int)config.autoconf.extraLengthMS);
			
			String statusStr = pilot.getAutoconfigStatus();
			statusLabel.setText(statusStr);
			statusLabel.setForeground(swtGroup.getDisplay().getSystemColor(
				(statusStr.startsWith("ERROR") ? SWT.COLOR_RED : SWT.COLOR_BLUE)));
			
			acceptFromFlightControlCheckbox.setSelection(config.autoconf.acceptShotLengthFromFlightControl);
			ImgSource src = pilot.getConnectedSource();
			enabledInAcqDeviceCheckbox.setSelection(src instanceof AcquisitionDevice && ((AcquisitionDevice)src).isAutoconfigAllowed());
			abortOnGeriPOSTCheckbox.setSelection(config.autoconf.abortOnGeriPOST);
			
			maxFrameCountSpinner.setSelection(config.autoconf.maxFrameCount);
			frameCountSpinner.setSelection(config.autoconf.frameCount);
			
			Modes[] modes = Modes.values();
	        for(int i=0; i < modes.length; i++)	
	        	settingModeRadios[i].setSelection(config.autoconf.mode == modes[i]);
	        
			frameCountSpinner.setEnabled(config.autoconf.mode != Modes.FrameCount);
						
			minFrameTimeSpinner.setSelection((int)config.autoconf.minFramePeriodUS / 1000);
			frameTimeSpinner.setSelection((int)(config.autoconf.framePeriodUS / 1000.0));
			frameTimeSpinner.setEnabled(config.autoconf.mode != Modes.FrameTime);
			try {
				if(src instanceof AcquisitionDevice) {
					frameCountReadback.setText("Current: " + ((AcquisitionDevice)src).getFrameCount()/1000.0 + " ms");
					frameTimeReadback.setText("Current: " + ((AcquisitionDevice)src).getFramePeriod()/1000.0 + " ms");
				}else {
					frameCountReadback.setText("Current: Not acq. device");
					frameTimeReadback.setText("Current: Not acq. device");
				}
				frameCountReadback.setForeground(frameCountReadback.getDisplay().getSystemColor(SWT.COLOR_BLACK));
				frameTimeReadback.setForeground(frameCountReadback.getDisplay().getSystemColor(SWT.COLOR_BLACK));
				
			}catch(RuntimeException err) {
				frameCountReadback.setText("ERR: " + err.getMessage());
				frameCountReadback.setForeground(frameCountReadback.getDisplay().getSystemColor(SWT.COLOR_RED));
				frameTimeReadback.setText("ERR: " + err.getMessage());
				frameTimeReadback.setForeground(frameTimeReadback.getDisplay().getSystemColor(SWT.COLOR_RED));
				//Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Error getting autoconfig info for GUI.", err);
			}
		
		}finally {
			dataChangeInhibit = false;
		}		
	}

	public Control getSWTGroup() { return swtGroup; }
}
