package imageProc.pilot.w7x.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import algorithmrepository.exceptions.NotImplementedException;
import imageProc.core.ImageProcUtil;
import imageProc.pilot.w7x.W7XPilot;

public class ProcessingSWTControl {
	private Group swtGroup;
	
	private Label statusLabel;
	private Button processChainButton;
	
	private W7XPilot proc;
	
    private Spinner procShot0Spinner;
	private Spinner procShot1Spinner;
	private Button bulkProcessGMDSButton;
	private Button bulkProcessW7XButton;
	private Text procW7XDescTextbox;
	private Text configToText;
	private Button configToButton;

	public ProcessingSWTControl(W7XPilot pilot, Composite parent, int style) {
		this.proc = pilot;
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Processing");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(5, false));
        ImageProcUtil.addRevealWriggler(swtGroup);
        
        processChainButton = new Button(swtGroup, SWT.PUSH);
        processChainButton.setText("Process now");
        processChainButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 5, 1));
        processChainButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.forceProcess(); }});
        
        Label lPS0 = new Label(swtGroup, SWT.NONE); lPS0.setText("With GMDSSource: From:");
        procShot0Spinner = new Spinner(swtGroup, SWT.NONE);
        procShot0Spinner.setValues(1000, 0, Integer.MAX_VALUE, 0, 1, 10);
        procShot0Spinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));

        Label lPS1 = new Label(swtGroup, SWT.NONE); lPS1.setText("to:");
        procShot1Spinner = new Spinner(swtGroup, SWT.NONE);
        procShot1Spinner.setValues(1000, 0, Integer.MAX_VALUE, 0, 1, 10);
        procShot1Spinner.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        
    	bulkProcessGMDSButton = new Button(swtGroup, SWT.PUSH);
    	bulkProcessGMDSButton.setText("Bulk Process");
    	bulkProcessGMDSButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
    	bulkProcessGMDSButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { bulkProcessGMDSButtonEvent(event); } });
    	
    	Label lPS2 = new Label(swtGroup, SWT.NONE); lPS2.setText("W7X Date/range/list-file: ");
    	procW7XDescTextbox = new Text(swtGroup, SWT.NONE);
    	procW7XDescTextbox.setText("20180822.022");
    	procW7XDescTextbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
        
    	bulkProcessW7XButton = new Button(swtGroup, SWT.PUSH);
    	bulkProcessW7XButton.setText("Bulk Process");
    	bulkProcessW7XButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
    	bulkProcessW7XButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { bulkProcessW7XButtonEvent(event); } });
    	
    	Label lCT1 = new Label(swtGroup, SWT.NONE); lCT1.setText("Configure all processors to:");
        configToText = new Text(swtGroup, SWT.NONE);
        configToText.setText("AUG/1000");
        configToText.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
         
        configToButton = new Button(swtGroup, SWT.PUSH);
        configToButton.setText("Config now");
        configToButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        configToButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { bulkConfigButtonEvent(event); } });
     	
        ImageProcUtil.addRevealWriggler(swtGroup);
         pilot.updateAllControllers();
	}

	protected void bulkProcessGMDSButtonEvent(Event event) {
		proc.bulkProcessGMDS(procShot0Spinner.getSelection(), procShot1Spinner.getSelection());
	}
	
	protected void bulkProcessW7XButtonEvent(Event event) {
		proc.bulkProcessW7X(procW7XDescTextbox.getText());
	}
	
	protected void bulkConfigButtonEvent(Event event) {	
		
		//proc.setupToConfig(configToText.getText());
		throw new NotImplementedException();
	}
	
	protected void settingsChangingEvent(Event event) {
		
	}
	
	public void doUpdate() {
		statusLabel.setText(proc.getStatus());	
	}

	public Control getSWTGroup() { return swtGroup; }
}
