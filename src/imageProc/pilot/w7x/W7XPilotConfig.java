package imageProc.pilot.w7x;

import imageProc.pilot.common.CommonPilotConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;

public class W7XPilotConfig extends CommonPilotConfig {

	/** Enable the pilot automatically on load of this config, if not already */
	public boolean startOnLoad = true;
	
	public int shotPollIntervalMS = 5000;
	public int dayPollIntervalMS = 60000; //every minute check what day and time it is... yeah...
	
	public boolean startOnNBITimer = false;
	public boolean startOnW7XProgram = true;
	public boolean startOnGERIBroadcastInit = false;
	
	/** Path to NBI trigger signal. e.g.:
	 *    "w7x/ArchiveDB/codac/W7X/ControlStation.2092/BE000_DATASTREAM/16/Trigger_BoxTimerNI21"; 
	 *    "w7x/ArchiveDB/codac/W7X/ControlStation.2176/BE000_DATASTREAM/16/Trigger_BoxTimerNI20";
	 *    */
	public String nbiTimerSignal; 
	

	/** Webapi address for W7X program start 'trigger' */
	public String webapiProgramTriggerAddress = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/Project.4096/ProgramLabelStart/parms/ProgramLogLabel/progId/_signal.json";

	public long geriBroadcastStartDelayMS = 0;	
	
	public boolean clockSyncAfterShot = false;

	/** Pre shot tolerance for finding shots when doing bulk processing
	 * This should really be the same setting as in the W7XLoad GUI
	 * but.... meh , too hard */
	public long postShotToleranceMS = 60000;	
	
	public boolean shotInhibit = false;
	
	/** How long after T0 to start the capture */
	public int acquireStartDelayMS;

	/** How long after T0 to start the capture again. The capture is attempted once at the trigger (program start)
	 * but then aborted and reattemped at this point, to see if that helps with the failures of the Andor cameras*/	
	public int recaptureDelayMS = -1;

	public void applyProfileDefaults() {
		SettingsManager sMgr = SettingsManager.defaultGlobal();
		camera.openCloseAcquisition = Algorithms.mustParseBoolean(sMgr.getProperty("imageProc.w7xPilot.inShot.fault.reopenAcquisitionOnFault", "False"));
		camera.resetPowerOnFault = Algorithms.mustParseBoolean(sMgr.getProperty("imageProc.w7xPilot.inShot.fault.resetPower", "False"));
		camera.durationResetPowerOff = Algorithms.mustParseInt(sMgr.getProperty("imageProc.w7xPilot.inShot.fault.cameraOffTime", "5000"));
		acquisition.cameraCaptureTimeoutMS = Algorithms.mustParseInt(sMgr.getProperty("imageProc.w7xPilot.inShot.acquireTimeout", "120000")); // 2mins
		processing.databaseSaveTimeoutMS = Algorithms.mustParseInt(sMgr.getProperty("imageProc.w7xPilot.inShot.saveTimeout", "600000"));
		processing.processingTimeoutMS = Algorithms.mustParseInt(sMgr.getProperty("imageProc.w7xPilot.inShot.procTimeout", "600000"));
		processing.processChainDelayMS = Algorithms.mustParseInt(sMgr.getProperty("imageProc.w7xPilot.inShot.processChainDelay", "1000"));
		acquisition.cameraAcquireStartTimeoutMS = Algorithms.mustParseInt(sMgr.getProperty("imageProc.w7xPilot.inShot.acquireStartTimeout", "5000")); //3secs
		acquireStartDelayMS = Algorithms.mustParseInt(sMgr.getProperty("imageProc.w7xPilot.inShot.acquireStartTimeMS", "59000")); //(2 secs before t1)
		postShotToleranceMS = Algorithms.mustParseInt(sMgr.getProperty("imageProc.w7xPilot.inShot.preShotTolerance", "60000"));
		camera.timeoutWaitingForOpen = 30000;
	}

}
