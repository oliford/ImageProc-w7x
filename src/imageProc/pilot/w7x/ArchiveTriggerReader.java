package imageProc.pilot.w7x;

import java.time.ZonedDateTime;
import de.mpg.ipp.codac.signalaccess.Signal;
import de.mpg.ipp.codac.signalaccess.SignalAddress;
import de.mpg.ipp.codac.signalaccess.SignalAddressBuilder;
import de.mpg.ipp.codac.signalaccess.SignalReader;
import de.mpg.ipp.codac.signalaccess.SignalToolsFactory;
import de.mpg.ipp.codac.signalaccess.objylike.ArchieToolsFactory;
import de.mpg.ipp.codac.signalaccess.readoptions.ReadOptions;
import de.mpg.ipp.codac.w7xtime.NanoTime;
import de.mpg.ipp.codac.w7xtime.TimeInterval;
import descriptors.w7x.ArchiveDBDesc;
import otherSupport.RandomManager;
import w7x.archive.ArchiveDBFetcher;

/** Thread to continuously read from a data stream and notify pilots of triggers */
public class ArchiveTriggerReader implements Runnable {
	
	private long lastRisingEdge = Long.MIN_VALUE;
	private long lastFallingEdge = Long.MIN_VALUE;
	private Object lastRisingEdgeValue = null;
	private Object lastFallingEdgeValue = null;
	
	private String database;
	private String dbPath;
	private float threshold = (float)0.5;
	private long initalLookBackMS = 1*60000;
	private int pollPeriodMS = 1200;
	/** How long with no data to reset the archive connection.
	 * For some reason the archive can keep returning no data even when the stream is there.
	 */
	private int nodataTimeoutMS = 20000;
	
	private Thread thread = null;
	private boolean death = false;
		
	public ArchiveTriggerReader(ArchiveDBDesc desc) {
		if(desc.isAlias())
			desc = ArchiveDBFetcher.defaultInstance().resolveAlias(desc);
		this.database = desc.getDatabase().toString();
		this.dbPath = desc.getPartialPath();
	}

	@Override
	public void run() {
		boolean noisy = true;
		try{
			while(!death) {
				
				System.out.println("ArchiveTriggerReader starting with signal '"+database + "/" + dbPath+"'");
				SignalToolsFactory stf = ArchieToolsFactory.remoteArchive(database);		
									
				SignalAddressBuilder sab = stf.makeSignalAddressBuilder();	
					
				//String dbPath = desc.getPartialPath();
				//dbPath = dbPath + ((desc.getCodacScaling() != null) ? ("/" + desc.getCodacScaling().name()) : "");
					
				SignalAddress adr = sab.newBuilder().path(dbPath).getSignalAddress();
				try(SignalReader sr  = stf.makeSignalReader(adr)){
					
					long lastTime = (System.currentTimeMillis() - initalLookBackMS)*1_000_000L;
					
					long maxReadPeriod =  10*60*1000_000_000L;
					while(!death){
						long toNanos = ArchiveDBDesc.toNanos(ZonedDateTime.now());
						
						toNanos = Long.min(toNanos, lastTime + maxReadPeriod);
		
						if(noisy) {
							String infoName = "?";
							if(dbPath.contains("NI20")) {
								infoName = "NI20";
							}else if(dbPath.contains("NI21")) {
								infoName = "NI21";
							}
							System.out.print("Reading " + infoName + " " + NanoTime.toUTCString(lastTime) + " - "
									+ NanoTime.toUTCString(toNanos) + "... ");
						}
						TimeInterval timeInterval = TimeInterval.with(lastTime, toNanos);
					
						ReadOptions readOptions = ReadOptions.fetchAll();
						Signal s = sr.readSignal(timeInterval, readOptions);
			
						
						if (s == null) {
							System.out.println("ArchiveTriggerReader: Read failed");
							
						}else if(s.getSampleCount() <= 1) {
							if(noisy)
								System.out.println("No data ("+s.getSampleCount()+")");
							
							if((toNanos - lastTime) > nodataTimeoutMS * 1_000_000L) {
								try {
									Thread.sleep(5000);
								} catch (InterruptedException e) { }
								//reset the archive connection
								break;
							}
												
						}else{									
							lastTime = processChunk(s, noisy);							
						}
						
						try {
							Thread.sleep(pollPeriodMS);
						} catch (InterruptedException e) { }
					}
				}
			}
		}catch(RuntimeException err){
			System.err.println("ArchiveTriggerReader terminated due to error:");
			err.printStackTrace();			
		}
	}

	private long processChunk(Signal s, boolean noisy) {
		Signal s_t = s.getDimensionSignal(0);	
		
		if(noisy)
			System.out.println("OK. Got "+s.getSampleCount()+" samples " +
						NanoTime.toUTCString(s_t.getValue(long.class, 0)) +
						" - " + 
						NanoTime.toUTCString(s_t.getLastSampleTime()));
		float lastF = s.getValue(float.class, 0);
		for(int i=1; i < s.getSampleCount(); ++i) {					
			long nanoTime = s_t.getValue(Long.class, i);				
			float f = s.getValue(float.class, i);
			if(lastF > threshold && f < threshold){
				lastFallingEdge = nanoTime;
				lastFallingEdgeValue = f;
				System.out.println("ArchiveTriggerReader: Falling edge at " + NanoTime.toUTCString(nanoTime));
			}else if(lastF < threshold && f > threshold){
				lastRisingEdge = nanoTime;
				lastRisingEdgeValue = f;
				System.out.println("ArchiveTriggerReader: Rising edge at " + NanoTime.toUTCString(nanoTime));
			}
			lastF = f;
		}
		return s_t.getLastSampleTime();
	}

	public void start() {
		if(thread == null || !thread.isAlive()){
			thread = new Thread(this);
			death = false;
			thread.start();
		}
	}
	
	public void stop(boolean awaitDeath){
		if(thread == null)
			return;
		
		death = true;
		thread.interrupt();
		if(awaitDeath){
			while(thread.isAlive()){
				death = true;
				thread.interrupt();
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) { }
			}
		}
		thread = null;
	}
	
	
	public static void main(String[] args) {
		//ArchiveDBDesc desc = new ArchiveDBDesc("w7x/ArchiveDB/codac/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/8/Trigger_BoxTimerNI21");
		ArchiveDBDesc desc = new ArchiveDBDesc("w7x/ArchiveDB/codac/W7X/ControlStation.2176/BE000_DATASTREAM/16/Trigger_BoxTimerNI20");
		ArchiveTriggerReader atr = new ArchiveTriggerReader(desc);
		
		long lastTrigger = Long.MIN_VALUE;
		while(true){
			long trigger = atr.lastRisingEdge;
			if(trigger > lastTrigger){
				System.out.println("Got trigger: "+ NanoTime.toUTCString(trigger));
				lastTrigger = trigger;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) { }
		}
	}
	
	public long getLastRisingEdge(){ return lastRisingEdge;	}
	public Object getLastRisingEdgeValue(){ return lastRisingEdgeValue;	}
	public long getLastFallingEdge(){ return lastFallingEdge;	}
	public Object getLastFallingEdgeValue(){ return lastFallingEdgeValue;	}
	
	public boolean isActive() {
		return thread != null && thread.isAlive();
	}
}
