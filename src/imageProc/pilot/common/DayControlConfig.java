package imageProc.pilot.common;

public class DayControlConfig {

	 /** 1) Is it a day marked as a shot day
	 *   2) Does the announcement server say it's a shot day (if they add this)
	 *   3) Is the magnetic field up?
	 *   4) Is the NBI box in some way online (maybe for the NBI systems)
	 *  
	 * Things we can do:
	 *    (de)activate the pilot
	 *    Power on anything via a PowerControl module
	 *    Open/close the acquisition device
	 *    Fire a general event for other sinks?
	 *   Run writeback?
			 */
	
	/** Which days are shot days. Sun,Mon,.... Fri,Sat*/
	public boolean shotDays[] = new boolean[] { false, false, true, true, true, false, false };

	/** If true, check with the announcement server that W7X is in operation */
	public boolean queryAnnouncementServer = false;
	
	/** If > 0, only measure when the main field coils have more current than this */ 
	public double minCoilCurrent = Double.NaN;
	
	/** If true, any module implementing PowerControl will be asked to
	 * switch everything on/off when operation starts/stops  */
	public boolean powerControl = false;
	
	/** Switch on/off only a specific thing. If null, the power controller will be left to decide */
	public String powerControlID = null;
			 
	/** Open/close acquisition device when operation start/stops */
	public boolean openAcquisitionDevice = false; 
	
	
}
