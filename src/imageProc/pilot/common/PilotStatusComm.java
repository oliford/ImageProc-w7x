package imageProc.pilot.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bouncycastle.util.Arrays;

import imageProc.core.AcquisitionDevice;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.PowerControl;
import imageProc.core.TextCommandable;
import imageProc.pilot.w7x.W7XPilot;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

/** Provides information about the pilot status and shot processing history for others to interrogate over a socket */ 
public class PilotStatusComm {
	
	public Logger logr = Logger.getLogger(this.getClass().getName());
	
	private CommonPilot pilot;
	
	private ServerSocket listeningSocket;
	
	private boolean notifyChanges = false;
	
	private String commStatus = "init";
	
	private ConcurrentLinkedQueue<String> notifications = new ConcurrentLinkedQueue<String>();
	
	public static enum StepStatus {
		/** Entry not set */
		UNKNOWN('?'),
		
		/** Step: Not yet begun.
		 * Pilot: Pilot is stopped */
		NOT_STARTED('N'), 

		/** Acquisition: Awaiting software/hardware trigger.
		 * Pilot: Started, waiting to start day control or waiting for a shot */
		WAITING('W'), 
		
		/** Step: Acquring/saving/processing etc is running.
		 * Pilot: In shot control */
		ACTIVE('A'),
		
		/** Step: Done ok 
		 * Pilot: Not used. */		
		SUCCESS('S'), 

		/** Step: Didn't complete 100% successfully.
		 * Pilot: Serious problem, given up entirely. */
		ERROR('E');
		
		public char code;
		private StepStatus(char code) { this.code = code; }			
	};
	
	private StepStatus pilotStatus = StepStatus.UNKNOWN;
	
	public static class ProgramStatus {
		public String id;
		
		public ProgramStatus(String programID) { this.id = programID; }
		
		public StepStatus acquireStatus = StepStatus.UNKNOWN;		
		public StepStatus saveStatus = StepStatus.UNKNOWN;
		public StepStatus processStatus = StepStatus.UNKNOWN;
		
		@Override
		public String toString() {
			return "STAT,ID=" + id + ",AQ=" + acquireStatus.code + ",SV=" + saveStatus.code + ",PR=" + processStatus.code;
		}
	};
	
	private ArrayList<ProgramStatus> progStats = new ArrayList<ProgramStatus>();
	
	public PilotStatusComm(CommonPilot commonPilot) {
		this.pilot = commonPilot;
	}
	
	public void startListening() {
				
		destroy(true, 3000);
		
		//start the listening thread
		listener = new AcceptListener();
		listenThread = new Thread(listener);
		listenThread.start();
	}
	
	ArrayList<Socket> connections = new ArrayList<>();

	private Socket outboundSocket = null;
	
	private class AcceptListener implements Runnable {
		public boolean death = false;
				
		@Override
		public void run() {
			
			ExecutorService pool = ImageProcUtil.getThreadPool();
			
			long lastOutboundAttempt = -1;
						
			try{
				PilotStatusCommConfig config = pilot.getConfig().statusComm;
				config.listenAcceptTimeoutMS = 500;
				
				listeningSocket = new ServerSocket(config.serverPort, 10);
				
				listeningSocket.setSoTimeout(config.listenAcceptTimeoutMS);
				
				logr.fine("PilotStatusComm listening on " + listeningSocket.getLocalSocketAddress());
				commStatus = "Listening... :" + listeningSocket.toString();
				pilot.updateAllControllers();
				
				do{					
					if(config.outboundEnable && config.outboundAddress != null && //we should be connecting
							!(outboundSocket != null && outboundSocket.isConnected()) && //and are not connected
							(System.currentTimeMillis() - lastOutboundAttempt) > config.outboundAttemptPeriod){ // and not recently tried
									
						
						//spin off in worker thread
						pool.execute(() -> { tryOutboundConnect(config.outboundAddress); } );
						
						lastOutboundAttempt = System.currentTimeMillis();
												
					}
					
					try {
						commStatus = "Listening... :" + listeningSocket.toString();
						
						Socket inboundSocket = listeningSocket.accept();
						if(death) break;
						
						logr.log(Level.INFO, "Accepted connection from " + inboundSocket.getRemoteSocketAddress().toString());
						pilot.updateAllControllers();
						
						pool.execute(() -> { newConnection(inboundSocket); } );
						
						
					}catch(IOException err){
						if(death) break; //presumably the error was because the local process is ending nicely
						if(!(err instanceof SocketTimeoutException)) {
							commStatus = "ERROR: Listen failed: " + err.getMessage();							
							logr.log(Level.WARNING, "Error in status comm", err);
							pilot.updateAllControllers();							
						}
						
						/*try {
							Thread.sleep(config.listenAcceptTimeoutMS);
						} catch (InterruptedException e) { }*/
						
					}
					
				}while(!death);

				commStatus = "Stopped.";
				pilot.updateAllControllers();
				
			}catch(Throwable err){
				pilot.getLog().write("Error connecting/listering for flight controller: " + err.getMessage());
				commStatus = "ERROR: " + err.getMessage();
				logr.log(Level.WARNING, "Error connecting/listering for flight controller", err);
				pilot.updateAllControllers();
				throw new RuntimeException(err);
			}finally {
				if(listeningSocket != null && !listeningSocket.isClosed()){
					try {
						if(listeningSocket != null)
							listeningSocket.close();
						if(outboundSocket != null) {
							if(outboundSocket.isConnected())
								outboundSocket.close();
							outboundSocket = null;
						}
						for(Socket socket : connections) {
							if(socket.isConnected())
								socket.close();
							connections.remove(socket);
						}
						
					} catch (IOException e) {
						logr.log(Level.WARNING, "Error listening for status comm", e);						
					}
				}

				pilot.updateAllControllers();
			}
		}
	}
	
	private void tryOutboundConnect(String outboundAddress) {
		try {
			String parts[] = outboundAddress.split(":");
			outboundSocket  = new Socket(parts[0], Integer.parseInt(parts[1]));
			
			logr.log(Level.INFO, "Outbound connection made to " + outboundSocket.getRemoteSocketAddress().toString());
			pilot.updateAllControllers();
			
			newConnection(outboundSocket);
			
					
		}catch(IOException err){
			logr.log(Level.WARNING, "Unable to make outbound connection to " + outboundAddress + ".");
			
		}finally {
			if(outboundSocket != null && outboundSocket.isConnected()) {
				try {
					outboundSocket.close();
				} catch (IOException e) { }
			}
			outboundSocket = null;			
		}
	}
	
	public void destroy(boolean waitForDeath, int waitForDeathTimeout) {
		//make sure there's nothing running already
		if(listener != null) {
			listener.death = true;
			if(listenThread != null && listenThread.isAlive()) {
				listenThread.interrupt();
				
				if(waitForDeath) {
					for(int i=0; i < waitForDeathTimeout / 10; i++) {
						if(listener == null || !listenThread.isAlive())
							break;
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) { }
						logr.log(Level.WARNING, "Listening thread still active after " + waitForDeathTimeout + " ms.");
					}
				}
			}
		}
		
		listener = null;
		listenThread = null;
	}
	
	public void newConnection(Socket socket) {
		connections.add(socket);
			
		try {
			PilotStatusCommConfig config = pilot.getConfig().statusComm;
			
			socket.setSoTimeout(config.commReadTimeoutMS);
			
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			//immediately send the SYSID, so the controller knows who we are
			pw.println("SYSID=" + pilot.getSystemId());
			pw.println("PORT=" + config.serverPort);
			pw.flush();
					 
			pilot.updateAllControllers();
			
			final long inactivityTimeout = 5000;
			long lastCommand = System.currentTimeMillis();
			while(listener != null && !listener.death){		
				
				try{
					pilot.updateAllControllers();
					
					String request = in.readLine();
					
					if(request == null || request.equalsIgnoreCase("CLOSE")){
						socket.close();
						break;
					}									
					
					processCommand(request, pw);
					lastCommand = System.currentTimeMillis();
					
				}catch(SocketTimeoutException err){ 
					
				}
			
				pilot.updateAllControllers();
				
				
				if(listener == null || listener.death || socket.isClosed()) 
					break;			
				
				if((System.currentTimeMillis() - lastCommand) > inactivityTimeout){
					if(socket != null && socket.isConnected()) {
						pw.println("Dropping connection due to " + inactivityTimeout + " ms inactivity.");
						pw.flush();
					}
					
					socket.close();
					logr.log(Level.WARNING, "PilotStatusComm connection dropped due to inactivity.");
					break;
				}
			
				String notifyStr = notifications.poll();
				while(notifyStr != null){
					pw.println(notifyStr);
					pw.flush();
					notifyStr = notifications.poll();
				}
			}
			
			if(socket != null && !socket.isClosed())
				socket.close();
			
			pilot.updateAllControllers();
			logr.log(Level.INFO, "PilotStatusComm connection closed.");
			
		}catch(IOException err) {
			logr.log(Level.WARNING, "ERROR: IOException in PilotStatusComm connection ", err);
			
		}finally {
			connections.remove(socket);			
		}
	}
	
	public void processCommand(String request, PrintWriter pw) {
		try {
			
			
			PilotStatusCommConfig config = pilot.getConfig().statusComm;
			
			if(request.equalsIgnoreCase("PING")){ //request status
				pw.println("PONG");
				
			}else if(request.equalsIgnoreCase("getStatuses")){ //request status of all past programs
				synchronized (progStats) {		
					for(ProgramStatus progStat : progStats){
						pw.println(progStat.toString());
					}
				}
				
			}else if(request.equalsIgnoreCase("pilotStatus")){ //request status
				pw.println("PILOT=" + pilotStatus.code);
				
			}else if(request.equalsIgnoreCase("systemID")){ //get systemID of this acqusition system
				pw.println("SYSID=" + pilot.getSystemId());
				
			}else if(request.equalsIgnoreCase("port")){ //get listening port of this acquisition system
				pw.println("PORT=" + config.serverPort);
				
			}else if(request.equalsIgnoreCase("terminate")) { ///try to gracefully terminate/exit the JVM by ending the GUI. Will be restarted if run inside systemd
				ImageProcUtil.ensureFinalSWTUpdate(ImageProcUtil.getDisplay(), this, () -> { ImageProcUtil.getDisplay().dispose(); } );			
				pw.println("TERM");		
			
			}else if(request.equalsIgnoreCase("kill")) { ///kill/hard-exit the JVM. Will be restarted if run inside systemd
				pw.println("KILL");
				pw.flush();
				System.exit(-1);

			}else if(request.equalsIgnoreCase("reboot")) { ///reboot the PC
				
				ProcessBuilder processBuilder = new ProcessBuilder();
				processBuilder.command("/usr/sbin/reboot");
				processBuilder.redirectOutput();
				processBuilder.redirectErrorStream();
				//processBuilder.directory(new File(projectsPath));
					
				try {
					Process process = processBuilder.start();
					pw.println("REBOOT");
					pw.flush();
					
				} catch (IOException e) {
					
					pw.println("ERROR: " + e.toString());
					pw.flush();
					e.printStackTrace();
				}				
				
			}else if(request.equalsIgnoreCase("readLog")){ //dump the pilot's log
				pw.println("LOG=");
				pw.print(pilot.getLog());
				pw.println("<ENDLOG>");
				
			}else if(request.equalsIgnoreCase("readJavaLog")){ //send the entire java.util.logging output file (or the last N bytes of it)
				String parts[] = request.split("[\\s=]");
				long maxSize = parts.length > 1 ? Algorithms.mustParseLong(parts[1]) : 100_0000_000L;
					
				String logFile = ImageProcUtil.getLogFile();
				if(logFile == null) {
					pw.println("JAVALOG=\nNULL\n<ENDLOG>");
					
				}else {			
					pw.println("JAVALOG=");
					
					File file = new File(logFile);
					try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r")) {
			            long fileLength = file.length() - 1;
			            
			            if(maxSize > 0 && (fileLength - maxSize) > 0) {
			            	//send the XML header first
			            	randomAccessFile.seek(0);
			            	String line = randomAccessFile.readLine();
			            	if(line != null)
								pw.println(line);
								
			            	//now go to the request position
			            	randomAccessFile.seek(fileLength - maxSize);
			            	
			            	//and read until the next record starts
			            	line = randomAccessFile.readLine();
							while(line != null) {								
								line = randomAccessFile.readLine();
								if(line != null && line.contains("<record")) {
									pw.println(line);
									break;
								}
							}
							
			            }
			            
			            //now just feed the file through
			            String line = randomAccessFile.readLine();
						while(line != null) {
							pw.println(line);
							line = randomAccessFile.readLine();					
						}					
						randomAccessFile.close();

			       } catch (IOException e) {
			       		throw new UncheckedIOException(e);
			       }
			    
					pw.println("<ENDLOG>");
				}
			}else if(request.equalsIgnoreCase("syncClock")){ //force syncronisation of clock (e.g. run ntpdate)
				double offset = pilot.doClockSync();
				pw.println("CLK-OFFSET=" + offset);
				
			}else if(request.startsWith("notifyChanges")){ //register to be notified of status changes
				String parts[] = request.split("[\\s=]");
				if(parts.length > 1){
					notifyChanges = Algorithms.mustParseBoolean(parts[1]);
				}
				pw.println("NOTIFY=" + notifyChanges);
				
			}else if(request.startsWith("forceProgram")){ //force the start of a program: forceProgram (programName) (progStartNanos)
				String parts[] = request.split("[\\s=]");
				if(parts.length > 2){
					String programName = parts[1];
					long programStatNanos = Algorithms.mustParseLong(parts[2]);
					if(programStatNanos > 0)
						pilot.forceShot(programName, programStatNanos);
				}
				
			}else if(request.startsWith("ModuleCommand")){
				//passes a specific command to a given module
				//currently just the source/sink module of the given class or interface type
				
				// moduleCommand, moduleName, command, parameteters ....
				// e.g. moduleCommand IsoPlaneControl,setFilterNumber,2
				
				
				String parts[] = request.split("[\\s=,]");
				if(parts.length < 3) {
					pw.println("Usage: ModuleCommand, moduleName, command, parameteters .... ");
					return;
				}
				String moduleName = parts[1];
				
				Object module = findModule(pilot.getConnectedSource(), moduleName);
				if(module == null) {
					pw.println("Module " + moduleName + " not found");
					return;
				}
				
				if(!(module instanceof TextCommandable)) {
					pw.println("Module " + moduleName + " is not a TextCommandable");
					return;
				}
				
				((TextCommandable)module).command(request.substring(request.indexOf(parts[2])), pw);
				
				
			}else if(request.startsWith("Power")){
				
				PowerControl powerControl = (PowerControl)findModule(pilot.getConnectedSource(), PowerControl.class.getSimpleName());
				
				if(powerControl == null) {
					pw.println("No PowerControl found");
					return;
				}
				
				String parts[] = request.split("[\\s=,]");
				String subCommand = parts[1];
					
				if(parts.length > 1) {
					if(parts[1].equalsIgnoreCase("get")) {
						pw.println("Power isOn=" + powerControl.isOn(parts.length > 2 ? parts[2] : null));
						return;
						
					}else if(subCommand.equalsIgnoreCase("on")) {
						powerControl.switchOn(parts.length > 2 ? parts[2] : null);
						pw.println("OK Power on");
						return;
						
					}else if(subCommand.equalsIgnoreCase("off")) {
						powerControl.switchOff(parts.length > 2 ? parts[2] : null);
						pw.println("OK Power off");
						return;
					}
				}
				pw.println("Usage: Power, {get/on/off}, [ID]");
				
			}else if(request.startsWith("getConfigParam")){
				//(OLD) configuration parameters of the Acqusition module
					
				String parts[] = request.split("[\\s=,]");
				ImgSource source = pilot.getConnectedSource();
				if(source instanceof AcquisitionDevice){
					try{
						((AcquisitionDevice)source).getConfigParameter(parts[1]);
						pw.println("GETCONFIGPARAM=OK");
					}catch(Throwable err){
						pw.println("GETCONFIGPARAM=ERROR:" + err.getMessage());
					}
				}else{
					pw.println("GETCONFIGPARAM=ERROR:Not AcquisitionDevice");
				}
				
			}else if(request.startsWith("setConfigParam")){ 
				//(OLD) configuration parameters of the Acqusition module
				
				if(!pilot.getConfig().allowRemoteConfig){
					pw.println("SETCONFIGPARAM=INHIBITED");				
				}else{
					String parts[] = request.split("[\\s=,]");
					
					try {
						if(parts[1].equals("shotLength")) {
							
							pilot.setShotLength(Double.parseDouble(parts[2]));
							
						}else {
							
							ImgSource source = pilot.getConnectedSource();
							if(source instanceof AcquisitionDevice){
								((AcquisitionDevice)source).setConfigParameter(parts[1], parts[2]);
								pw.println("SETCONFIGPARAM=OK");
								
							}else{
								pw.println("SETCONFIGPARAM=ERROR:Not AcquisitionDevice");
							}
						}
					}catch(Throwable err){
						pw.println("SETCONFIGPARAM=ERROR:" + err.getMessage());
					}
				}		
			}else if(request.startsWith("geriState")) {
				String parts[] = request.split("[\\s=,]");
				try {
					int state = Integer.parseInt(parts[1]);
					long timestamp = Long.parseLong(parts[2]);
					
					pilot.broadcastedGeriStateChange(state, timestamp);				
				}catch(NumberFormatException err) {
					
				}
			}else{
				pw.println("Unknown command");
			}
			
		} finally {
			pw.flush();
		}
	}

	private Object findModule(ImgSource src, String moduleName) {
		
		if(isOneOf(src.getClass(), moduleName))
			return src;
		
		for(ImgSink sink : src.getConnectedSinks()) {
			if(isOneOf(sink.getClass(), moduleName))
				return sink;
			
			if(sink instanceof ImgSource) {
				Object ret = findModule((ImgSource)sink, moduleName);
				if(ret != null)
					return ret;
			}
		}
		
		return null;		
	}

	private boolean isOneOf(Class cls, String moduleName) {		
		
		while(cls != null) {		
			if(moduleName.equals(cls.getSimpleName()))
				return true;
			
			for(Class iface : cls.getInterfaces()) {
				if(moduleName.equals(iface.getSimpleName()))
					return true;
			}
			
			cls = cls.getSuperclass();			
		}
		return false;
	}

	private AcceptListener listener;
	private Thread listenThread;
	
	public ProgramStatus mustGetProgramState(String programID){
		synchronized (progStats) {		
			for(ProgramStatus progStat : progStats){
				if(progStat.id == programID)
					return progStat;
			}	
		
		
			ProgramStatus stat = new ProgramStatus(programID);
			progStats.add(stat);		
			return stat;
		}
	}
	
	public void setAcquireStatus(String programID, StepStatus status){ 
		ProgramStatus progStat = mustGetProgramState(programID);
		progStat.acquireStatus = status;
		if(notifyChanges)
			notifications.add(progStat.toString());
	}
	
	public void setSaveStatus(String programID, StepStatus status){ 
		ProgramStatus progStat = mustGetProgramState(programID);
		progStat.saveStatus = status;	

		if(notifyChanges)
			notifications.add(progStat.toString());
	}
	
	public void setProcessStatus(String programID, StepStatus status){ 
		ProgramStatus progStat= mustGetProgramState(programID);
		progStat.processStatus = status;

		if(notifyChanges)
			notifications.add(progStat.toString());
	}
	
	public void setPilotStatus(StepStatus status){ 
		this.pilotStatus = status;
		if(notifyChanges)
			notifications.add("PILOT=" + status.code);
	}
	
	public void sendGERIState(int state, long timestamp) {
		notifications.add("GERISTATE=" + state + "," + timestamp);
	}
		
	public String getCommStatus() { return commStatus; }
	
	public ArrayList<Socket> getConnections() { return connections; }

	public StepStatus getPilotStatus() { return this.pilotStatus;}
	
}
