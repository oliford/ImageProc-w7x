package imageProc.pilot.common;

public class PilotStatusCommConfig {
	
	/** Port number to listen for connections from the flight controller */
	public int serverPort = 9752;
	
	/** Address to attempt to connect out to (i.e. the flight controller) */
	public String outboundAddress = "sv-coda-wsvc-28.ipp-hgw.mpg.de:9752";
	
	/** Periodically attempt to connect to the given address. */
	public boolean outboundEnable = true;

	/** Start automatically when the config is loaded (also on startup if a config is loaded then) */
	public boolean startOnConfigLoad = true;

	/** Timeout for reading from communication socket */ 
	public int commReadTimeoutMS = 500;
	
	/** Timeout for waiting for incoming connections before cycling. This should be short
	 * enough to allow it to be killed quickly */ 
	public int listenAcceptTimeoutMS = 500;

	/** Period in which to attempt outbound connection to FlightController */
	public long outboundAttemptPeriod = 5000;

}
