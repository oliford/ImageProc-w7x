package imageProc.pilot.common;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;

import imageProc.core.ImgSourceOrSinkImpl;
import otherSupport.SettingsManager;

public class PilotLog {
	
	private CommonPilot pilot;

	private StringBuffer buffer = new StringBuffer(1048576);
	
	private String logFilePath;
	private FileOutputStream logFileStream;
	private PrintWriter logFileWriter;
	private Calendar logDate = null;
	
	private Logger logr = Logger.getLogger(this.getClass().getName());

	private final static SimpleDateFormat timestampFormat = new SimpleDateFormat("dd/MM HH:mm:ss");
	
	public PilotLog(CommonPilot pilot) {
		this.pilot = pilot;
		logFilePath = SettingsManager.defaultGlobal().getPathProperty("imageProc.w7xPilot.logPath", System.getProperty("java.io.tmpdir") + "/imageProc/w7XPilotLogFiles");
		
	}
	
	public void write(String str){
		Calendar now = Calendar.getInstance();
		String timeStamp = timestampFormat.format(now.getTime());
		String lineStr = timeStamp + ": " + str;
		
		if(pilot.getConfig().mirrorToJavaLogger) {
			logr.fine(lineStr);
		}
		
		try{
			if(logDate == null || now.get(Calendar.DAY_OF_YEAR) != logDate.get(Calendar.DAY_OF_YEAR)){
				//want a new log file
				if(logFileStream != null){
					logFileStream.close();
					logFileWriter.close();
				}
				//can't make a sensible log if there's no source yet (problem with constrcution)
				if(pilot.getConnectedSource() == null)
					return;
				String logFileName = "w7xPilot-" + 
							(new SimpleDateFormat("yyyy-MM-dd")).format(now.getTime()) + "-" +
							pilot.getConnectedSource().toShortString() + "_" + pilot.getConnectedSource().hashCode()
						+ ".log";
				logFileStream = new FileOutputStream(logFilePath + "/" + logFileName, true);
				logFileWriter = new PrintWriter(logFileStream, true);
				
				logDate = now;
			}
			
			logFileWriter.println(lineStr);
		} catch (Exception err) {
			//can't do much else without risking messing things up
			err.printStackTrace();
		}
		
		if(buffer.length() > 1048576){
			//getting too big
			buffer = new StringBuffer(1048576);
			buffer.append("-- Log clear, as it exceeded 1MB --");
		}
		
		buffer.append(lineStr + "\n");
		pilot.updateAllControllers();
	}

	public void close() {
		logFileWriter.close();
		try { logFileStream.close(); } catch (IOException e) { }
		
	}

	@Override
	public String toString() {
		return buffer.toString();
	}
}
