package imageProc.pilot.common;

import imageProc.core.EventReciever;

/** Code for determining if we're in operation and should be measuring.
 * 
 * Things we can check:
 *   1) Is it a day marked as a shot day
 *   2) --X-- Does the announcement server say it's a shot day (if they add this) --X--
 *   3) Is the magnetic field up?
 *   4) Is the NBI box in some way online (maybe for the NBI systems)
 *  
 * Things we can do:
 *    (de)activate the pilot
 *    Power on anything via a PowerControl module
 *    Open/close the acquisition device
 *    Fire a general event for other sinks?
 *    Run writeback?
 * 
 * @author oliford
 *
 */
public class DayControl {
		
	private CommonPilot pilot;
	
	public DayControl(CommonPilot pilot) {
		this.pilot = pilot;
	}
}
