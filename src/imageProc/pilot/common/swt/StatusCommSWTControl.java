package imageProc.pilot.common.swt;

import java.net.Socket;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.pilot.common.CommonPilot;
import imageProc.pilot.common.PilotStatusCommConfig;
import imageProc.pilot.w7x.W7XPilot;

public class StatusCommSWTControl {
	private Group swtGroup;
	
	private Label statusLabel;
	
	private Spinner portNumber;
	private Button startButton;
	private Button stopButton;
	private Button listenOnConfigLoad;
	
	private Button outboundEnableCheckbox;
	private Text outboundAddressCheckbox;
	
	
	private CommonPilot pilot;
	
	public StatusCommSWTControl(CommonPilot pilot, Composite parent, int style) {
		this.pilot = pilot;
		
		swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Flight Control");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(5, false));
        ImageProcUtil.addRevealWriggler(swtGroup);
        
        Label lS = new Label(swtGroup, SWT.NONE); lS.setText("Status:");
        statusLabel = new Label(swtGroup, SWT.NONE);
        statusLabel.setText("Init\n.\n.\n.\n");
        statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 4, 1));
        statusLabel.addListener(SWT.MouseDown, new Listener() { @Override public void handleEvent(Event event) { doUpdate(); }});
       
        Label lP = new Label(swtGroup, SWT.NONE); lP.setText("Port:");
        portNumber = new Spinner(swtGroup, SWT.NONE);
        portNumber.setValues(0, 1024, 65535, 0, 1, 10);
        portNumber.setToolTipText("Port number to listen for connections from the flight controller");
        portNumber.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        portNumber.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

        Label lCL = new Label(swtGroup, SWT.NONE); lCL.setText("");
        listenOnConfigLoad = new Button(swtGroup, SWT.CHECK);
        listenOnConfigLoad.setText("Start on config load");
        listenOnConfigLoad.setToolTipText("Start automatically when the config is loaded (also on startup if a config is loaded then)");
        listenOnConfigLoad.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 4, 1));
        listenOnConfigLoad.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

        Label lSS = new Label(swtGroup, SWT.NONE); lSS.setText("");
        startButton = new Button(swtGroup, SWT.PUSH);
        startButton.setText("Start listening");
        startButton.setToolTipText("Start the listening socket and attempt to connect if outbound enabled.");
        startButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        startButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { startButtonEvent(event); }});

        stopButton = new Button(swtGroup, SWT.PUSH);
        stopButton.setText("Stop listening");
        stopButton.setToolTipText("Disconnect any active connection and stop listening.");
        stopButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 1));
        stopButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { stopButtonEvent(event); }});

        Label lO = new Label(swtGroup, SWT.NONE); lO.setText("Outbound:");
        outboundAddressCheckbox = new Text(swtGroup, SWT.NONE);
        outboundAddressCheckbox.setText("Start listening");
        outboundAddressCheckbox.setToolTipText("Address to attempt to connect out to (i.e. the flight controller)");
        outboundAddressCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 3, 1));
        outboundAddressCheckbox.addListener(SWT.FocusOut, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

        outboundEnableCheckbox = new Button(swtGroup, SWT.CHECK);
        outboundEnableCheckbox.setText("Enable");
        outboundEnableCheckbox.setToolTipText("Periodically attempt to connect to the given address.");
        outboundEnableCheckbox.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 1));
        outboundEnableCheckbox.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangedEvent(event); }});

	}
	
	protected void settingsChangedEvent(Event event) {
		PilotStatusCommConfig config = pilot.getConfig().statusComm;
		
		config.serverPort = portNumber.getSelection();
		config.startOnConfigLoad = listenOnConfigLoad.getSelection();
		config.outboundAddress = outboundAddressCheckbox.getText();
		config.outboundEnable = outboundEnableCheckbox.getSelection();
		
		doUpdate();
	}

	protected void stopButtonEvent(Event event) {
		pilot.getStatusComm().destroy(false, 0);
		doUpdate();
	}

	protected void startButtonEvent(Event event) {
		pilot.getStatusComm().startListening();
		doUpdate();
	}

	public Control getSWTGroup() { return swtGroup; }
	
	public void doUpdate() {
				
		StringBuffer b = new StringBuffer(pilot.getStatusComm().getCommStatus());
		
		ArrayList<Socket> connections = pilot.getStatusComm().getConnections();
		b.append(" " + connections.size() + " connections:\n");
		
		int j=0;
		for(Socket socket : connections) 
			b.append(" " + j++ + ": " + (socket.isConnected() ? socket.getRemoteSocketAddress().toString() : "(not connected)") + "\n");
				
		for(int i=5; i > connections.size(); i--)
			b.append("\n");
		
		statusLabel.setText(b.toString());
		
		ImageProcUtil.setColorByStatus(pilot.getStatusComm().getCommStatus(), statusLabel);
		
		PilotStatusCommConfig config = pilot.getConfig().statusComm;
		
		portNumber.setSelection(config.serverPort);
		listenOnConfigLoad.setSelection(config.startOnConfigLoad);
		outboundAddressCheckbox.setText(config.outboundAddress);
		outboundEnableCheckbox.setSelection(config.outboundEnable);
	}
}