package imageProc.pilot.common.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import imageProc.core.ImageProcUtil;
import imageProc.pilot.common.CommonPilot;
import imageProc.pilot.common.CommonPilotConfig;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class DaySWTControl {
	private Group swtGroup;
	
	private Button shotDayCheckboxes[];
	private Button dayInhibitButton;
	
	
	
	private CommonPilot proc;
	
	public DaySWTControl(CommonPilot pilot, Composite parent, int style) {
		this.proc = pilot;
		CommonPilotConfig config = proc.getConfig();
		
        swtGroup = new Group(parent, SWT.BORDER);
        swtGroup.setText("Day");
        swtGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        swtGroup.setLayout(new GridLayout(3, false));
        
		String days[] = new String[]{ "Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat" };
		shotDayCheckboxes = new Button[7];
		 
		for(int i=0; i < 7; i++){
			shotDayCheckboxes[i] = new Button(swtGroup, SWT.CHECK);
			shotDayCheckboxes[i].setText(days[i]);;
			shotDayCheckboxes[i].setSelection(config.dayControl.shotDays[i]);
			shotDayCheckboxes[i].addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { settingsChangingEvent(event); }});
		}
		
		dayInhibitButton = new Button(swtGroup, SWT.PUSH);
		dayInhibitButton.setText("Inhibit Day (No shots today)");
		dayInhibitButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1));
		dayInhibitButton.addListener(SWT.Selection, new Listener() { @Override public void handleEvent(Event event) { proc.inhibitToday(); } });
		
		ImageProcUtil.addRevealWriggler(swtGroup);
		
		pilot.updateAllControllers();
	}



	protected void settingsChangingEvent(Event event) {
		CommonPilotConfig config = proc.getConfig();
		
		config.dayControl.shotDays = new boolean[7];
		for(int i=0; i < 7; i++){
			config.dayControl.shotDays[i] = shotDayCheckboxes[i].getSelection();
		}				
	}

	public void doUpdate() {
		CommonPilotConfig config = proc.getConfig();
		
		if(config.dayControl.shotDays == null)
			config.dayControl.shotDays = new boolean[7];
		for(int i=0; i < 7; i++){
			shotDayCheckboxes[i].setSelection(config.dayControl.shotDays[i]);
		}
		
	}

	public Control getSWTGroup() { return swtGroup; }
}
