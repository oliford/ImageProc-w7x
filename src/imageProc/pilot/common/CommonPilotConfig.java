package imageProc.pilot.common;

import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;

public class CommonPilotConfig {
	
	/** How long before giving up on the clock syncronisation command (ntpdate) */
	public int clockSyncCommandTimeoutMS = 60000;

	/** Allow FlightController to issure remote config commands */
	public boolean allowRemoteConfig = false;

	/** Time to allow modules to start responding to internal triggers or commands.  
	 * The Controller fires start or aborts, then waits this long before continuing */
	public int postTriggerDelay = 3000;
	
	/** Time the controller waits between polling sources and sinks for their busy status */
	public int busyPollDelayMS = 500; // 500ms
	
	/** Configuration of when it's a measuring time and turning things on and off */
	public DayControlConfig dayControl = new DayControlConfig();
	
	/** Timing parameters for manipulation of the camera */
	public static class CameraInitConfig {
		/** If true, acquisition driver is opened/closed on start/end of shot period and on faults */
		public boolean openCloseAcquisition = true;
		
		/** Turn on and off power to camera when a fault is detected */
		public boolean resetPowerOnFault = false;
		
		/** Time to switch camera off for when it stops in the middle of a shot [ms] */
		public int durationResetPowerOff = 5000;
		
		/** How long to wait after turning camera on before trying to open it [ms] */ 
		public int delayBetweenPowerOnAndOpen = 10000;
				
		/** When reinitialising the camera, if it is a ConfigurableByID, then configure it with this ID (e.g. json file). If null, leave it as already configured. */
		public String configID;

		/** When opening camera driver, how long to wait before considering it broken [ms] */
		public long timeoutWaitingForOpen = 30000;
	}
	
	public CameraInitConfig camera = new CameraInitConfig();
	
	public static class AcquisitionPrep {
		/**Check if camera is broken by looking for pixels with 0 value. If it is, treat it as if it had an error during acquire */
		public boolean checkIfCameraBroken = true;
		
		/**If the camera is still capturing after this long, without starting acquiring, abort the capture */
		public int cameraCaptureTimeoutMS = 120000;
		
		/**If the camera is still acquiring after this long, abort the capture */
		public int cameraAcquireStartTimeoutMS = 5000;

		/** Number of times to try open/close of camera before giving up entirely */
		public int maxPrepAttempts = 5;
	}
	
	public AcquisitionPrep acquisition = new AcquisitionPrep();
	
	public static class Autoconfig {
		/** Inhibit the setting of camera parameters until save is complete, or failed.
		 * This stops the metadata being set wrong for an already acquired or currently 
		 * being acquired shot. 
		 * 
		 * This is implemented in the pilots, */
		public boolean inhibitUntilSave = true;
		
		/** Current/last set shot length to use for autoconfig [ms] */
		public long shotLengthMS = 10000;
		
		/** Additional time to acquire for (to extend before or after shot) */
		public long extraLengthMS = 1000;
		
		/** Usethe shot length when it's transmitted from the flight controller */
		public boolean acceptShotLengthFromFlightControl = false;
		
		/** Abort the acquisition early when flight controller reports that some
		 * other GERI pilot's state changes to POST. */
		public boolean abortOnGeriPOST = false;
		
		public static enum Modes {
			/** Autoconfiguarion disabled */
			None,
			
			/** Modifies the number of frames to cover the set shoty length */
			FrameCount,
			
			/** Modifies the frame time spacing (and/or exposure time), depending on the acquisition device
			 * according to the shot length. If setFrameCount is also true, frame time is modified only 
			 * when the max number of frames would be exceeded. */
			FrameTime,
			
			/** Special handling by looking up run length in a table
			 * for exp time, frame time and frame count */
			TableRunLengthLookup,
			
			/** Special modes for hot-fix use */
			Special1,
			Special2,				
		}
		
		public Modes mode = Modes.None;
				
		/** Maximum number of frames the device can record. If setFrameTime is also enabled, 
		 * the frames will be elongated when so that this is not exceeded.*/
		public int maxFrameCount = 10000;
		
		/** Minimum frame time spacing to allow when setFrameTime is true [us]. */ 
		public long minFramePeriodUS = 1000;

		/** Last calculated frame count */
		public int frameCount = -1;
		
		/** Last calculated frame period [us] */
		public long framePeriodUS = -1;
				
		/** Table lookup for Modes.TableRunLengthLookup
		 * [0] = Run length [s],
		 * [1] = Exposure time [ms]
		 * [2] = Frame time [ms]
		 * [3] = Frame count  [#]*/
		public double[][] tableLookup;
	}
	
	public Autoconfig autoconf = new Autoconfig();
	
	public class ProcessingConfig {
		public int processingTimeoutMS = 600000;
		public int databaseSaveTimeoutMS = 600000;
		
		public int processChainDelayMS = 1000;
		
		public boolean processChainInhibit = false;
		
		/** If a step in the processing chain fails, abort the processing. Otherwise, continue to next module */
		public boolean abortProcessingOnModuleTimeout = true;
	}
	
	public ProcessingConfig processing = new ProcessingConfig();

	/** Configuration of the StatusComm submoudle, which communicates with the flight controller */ 
	public PilotStatusCommConfig statusComm = new PilotStatusCommConfig();

	/** Write pilot log entries to main java.util.logging Logger */
	public boolean mirrorToJavaLogger = true;
	
}
