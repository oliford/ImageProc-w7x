package imageProc.pilot.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import algorithmrepository.ExtrapolationMode;
import algorithmrepository.Interpolation1D;
import algorithmrepository.InterpolationMode;
import algorithmrepository.LinearInterpolation1D;
import imageProc.core.AcquisitionDevice;
import imageProc.core.ConfigurableByID;
import imageProc.core.DatabaseSink;
import imageProc.core.EventReciever;
import imageProc.core.ImageProcUtil;
import imageProc.core.ImgSourceOrSinkImpl;
import imageProc.core.ImgProcPipe;
import imageProc.core.ImgSink;
import imageProc.core.ImgSource;
import imageProc.core.PowerControl;
import imageProc.core.TextCommandable;
import imageProc.core.AcquisitionDevice.Status;
import imageProc.core.EventReciever.Event;
import imageProc.database.geriStream.GeriStreamSink;
import imageProc.database.w7xArchiveDB.W7XArchiveDBSink;
import imageProc.pilot.common.CommonPilotConfig.Autoconfig.Modes;
import imageProc.pilot.common.PilotStatusComm.StepStatus;
import imageProc.pilot.geri.GeriPilot;
import imageProc.pilot.w7x.W7XPilotConfig;
import imageProc.proc.softwareBinning.SoftwareROIsProcessor;
import imageProc.sources.capture.andorCam.AndorCamSource;
import imageProc.sources.capture.base.CaptureSource;
import imageProc.sources.capture.sensicam.SensicamSource;
import oneLiners.OneLiners;
import algorithmrepository.Algorithms;
import algorithmrepository.Mat;
import otherSupport.SettingsManager;
import w7x.archive.ArchiveDBFetcher;

/** common code for W7XPilot and GeriPilot, and maybe one day also AUGPilot or others?? */
public abstract class CommonPilot extends ImgSourceOrSinkImpl implements ImgSink, TextCommandable {
	protected PilotLog log = new PilotLog(this);
	
	public CommonPilot() {
		systemID  = SettingsManager.defaultGlobal().getProperty("imageProc.w7xPilot.systemID","Unknown");
		
		this.statusComm = new PilotStatusComm(this);
	}

	public PilotLog getLog() { return log; }
	
	/** Generic status text of module to be displayed in GUI */
	protected String status = "Init";
	
	protected String autoconfigStatus = "Not attempted";
	
	protected String systemID = "Unknown";

	protected PilotStatusComm statusComm;
	
	protected DayControl dayControl;

	/** Controller for a remote power switch to turn on/off camera at start/end 
	 * of day or if it's not responding */
	protected PowerControl powerCtrl;

	/** Last run length calculated by autoconf, according to the actually set camera parameters */
	private int lastCalculatedRunLength;

	public abstract CommonPilotConfig getConfig();

	/** Signal worker processing to stop. Since this is an ensureFinalUpdate() call, the next process will then start.
	 * The user just has to hammer away at Abort */
	private boolean abortProcessing = false;
	
	/** Set to today to show that it isn't a shot day, even if it fits the normal criteria */
	protected Calendar inhibitDay = null; 
	
	/** Would we currently be inhibiting the autoconfiguration if
	 * config.autoconfig.inhibitUntilSave is set. i.e. are we in a shot
		acquiring or not yet saved??
	 */
	protected boolean inhibitingAutoconfDuringShot = false;
	
	/** Execute ntpdate to force sync of local clock to W7X NTP server
	 * because the NTP deamon seems not to work */
	private final static String clockSyncObj = new String("clockSyncObj");
	public void syncToW7XClock(){
		
		ImageProcUtil.ensureFinalUpdate(clockSyncObj, new Runnable() {
			@Override
			public void run() { doClockSync(); }
		});
	}
	
	public double doClockSync() {
		try{
			String line;
			String cmd = SettingsManager.defaultGlobal().getProperty("imageProc.w7xPilot.clockSyncCommand", "none");
			if(cmd == null || cmd.length() <= 0 || cmd.equals("none")) //disabled
				return Double.NaN;

			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader input =
					new BufferedReader
					(new InputStreamReader(p.getInputStream()));
			
			p.waitFor(getConfig().clockSyncCommandTimeoutMS, TimeUnit.MILLISECONDS);
			
			double offset = 0;
			StringBuffer allOutput = new StringBuffer();
			while ((line = input.readLine()) != null) {
				String parts[] = line.split(" ");
				for(int  i=0; i<parts.length; i++){
					if("offset".equals(parts[i]) && i < (parts.length-1)){
						offset = Algorithms.mustParseDouble(parts[i+1]);
						break;
					}
				}
				allOutput.append(line + "\n");
			}
			input.close();
			
			if(offset != 0){
				log.write("NTP sync complete, offset = " + offset + " ms");
				return offset;
				
			}else{
				log.write("Unrecognised response from clock sync command '"+cmd+"': " + allOutput.toString());
				return Double.NaN;
			}
			
		}catch (Exception err) {
			err.printStackTrace();
			return Double.NaN;
		}

	}
		
	public void checkCameraIsSetup(String cameraConfigID){
		CommonPilotConfig config = getConfig();
		ImgSource src = getConnectedSource();
		if(!(src instanceof AcquisitionDevice))
			throw new RuntimeException("Image source is not an acquisition device");
		
		AcquisitionDevice cam = (AcquisitionDevice)src;
		
		if(cam.getAcquisitionStatus() != Status.notInitied && cam.getAcquisitionStatus() != Status.errored){ //and it's now connected ('open')
			log.write("Acqusition device appears to be ready. ("+cam.getAcquisitionStatus() + ", "+ cam.getAcquisitionStatusString() + ")");
			return;
		}
			
		//if we have a power controller, check the power is on 
		try{
			if(powerCtrl == null)
				log.write("No power controller");
			
			else if(powerCtrl.switchOn(null)){; //check the power is on
				
				log.write("Camera was (probably) off, waiting "+config.camera.delayBetweenPowerOnAndOpen+" ms for it to warm up.");
				
				Thread.interrupted();//don't skip this delay
				try{ Thread.sleep(getConfig().camera.delayBetweenPowerOnAndOpen); }catch(InterruptedException err){} //wait for it to wake up
			}else{
				log.write("Necessary power already all on.");
				
			}
		}catch(RuntimeException err){
			log.write("ERROR: Couldn't check/switch on camera power, will try to set it up anyway: " + err);
			err.printStackTrace();
		}
		
		
		log.write("Setting up camera to '"+cameraConfigID+"'");
		Thread.interrupted();//clear thread interrupted flag so that we don't skip delays here
		
		if(getConfig().camera.openCloseAcquisition){
			try{
				if(cam instanceof ConfigurableByID)
					((ConfigurableByID)cam).loadConfig(cameraConfigID);
				
				cam.open(config.camera.timeoutWaitingForOpen); //and set it up
			}catch(Exception err){
				if(isControllerAborting()){
					log.write("ERROR: Exception setting up camera. Controller is aborting though, so this might be intentional. " + err);
					return;
				}
					
				err.printStackTrace();
				log.write("ERROR: Exception setting up camera in config prep, will make one attempt to reset it: " + err);
				try{
					resetCamera("Config");
				}catch(InterruptedException err2){ }
			}
		}
		
		//make sure it's ready to be triggered by InShot (for 'Capture' based sources)
		if(src instanceof CaptureSource)
			((CaptureSource)src).enableEventResponse(true, false);		
	}
	
	protected abstract boolean isControllerAborting();

	protected void checkCameraIsOff(){
		CommonPilotConfig config = getConfig();
		
		log.write("Setting camera off");
		//might want to do rads check or something here
		
		
		//The andor camera driver will crash the entire JVM if
		//the camera is switched off while the driver is connected
		if(connectedSource instanceof AcquisitionDevice && 
				config.camera.openCloseAcquisition){
			
			((AcquisitionDevice)connectedSource).close();
			Thread.interrupted();
			try { Thread.sleep(1000); } catch (InterruptedException e) { }
		}
		
		try{
			if(powerCtrl != null && config.camera.resetPowerOnFault)
				powerCtrl.switchOff(null);
		}catch(RuntimeException err){
			log.write("ERROR: Camera power-off failed");
		}
	}
		
	public void resetCamera(String programID) throws InterruptedException {		
		CommonPilotConfig config = getConfig();
		AcquisitionDevice acq = (AcquisitionDevice)connectedSource;
		
		status = "Resetting acquisition device";
							
		if(config.camera.openCloseAcquisition){
			log.write("InShot["+programID+"]: Closing camera.");
			acq.close();
		}		
		
		if(config.camera.resetPowerOnFault){ //... && connectedSource.getImage(0) != null){ ... Why????
			
			try{
				log.write("InShot: Power cycling camera for " + config.camera.durationResetPowerOff + " ms");
				powerCtrl.switchOff(null);
				try{ 
					Thread.sleep(config.camera.durationResetPowerOff); 
				}catch(InterruptedException err){ //we must leave when poked! 
					throw new RuntimeException("Interrupted while power cycling camera"); 
				}
				powerCtrl.switchOn(null);
				log.write("InShot: Camera power cycle done.");
			}catch(RuntimeException err){
				log.write("InShot: ERROR: Couldn't cycle camera power, will try to set it up again anyway: " + err);
				err.printStackTrace();
			}
		}
		
		log.write("InShot["+programID+"]: Waiting " + config.camera.delayBetweenPowerOnAndOpen + " ms for camera to init/settle.");
		try{
			Thread.sleep(config.camera.delayBetweenPowerOnAndOpen);
		}catch(InterruptedException err){ 
			throw new RuntimeException("Interrupted while waiting for camera to warm up"); //we must leave when poked!
		} 
		
		if(config.camera.openCloseAcquisition){
			if(config.camera.configID != null && acq instanceof ConfigurableByID) {
				((ConfigurableByID)acq).loadConfig(config.camera.configID);
				log.write("InShot["+programID+"]: Configured camera to " + config.camera.configID);
			}
			
			log.write("InShot["+programID+"]: Opening camera...");		
			acq.open(config.camera.timeoutWaitingForOpen);
			log.write("InShot["+programID+"]: Opened camera");
			
			
			
		}
		
	}
	
	public PowerControl getPowerControl() { return powerCtrl; }
	public PilotStatusComm getStatusComm() { return statusComm;	}

	/** Prepare the source for acquisition
	 * @param allowAcquireNotReady If true, acquisition will be started later,
	 *    	     so don't worry if the camera is still 'waiting for software trigger' (the acquire checkbox)
	 * @return true if all is OK, false if it failed too many times
	 * @throws InterruptedException 
	 */
	public boolean attemptAcquisitionPrep(String programID, long t0Nano, boolean allowWaitingForAcquire) throws InterruptedException{
		CommonPilotConfig config = getConfig();
		
		for(int nPrepAttempts=0; nPrepAttempts < (config.acquisition.maxPrepAttempts+1); nPrepAttempts++){ //start preparation loop
			
			if(attemptAcquisitionPrepSingle(programID, t0Nano, allowWaitingForAcquire)) {
				return true; //prep OK, ready to acquire
			}else{
				if(nPrepAttempts >= config.acquisition.maxPrepAttempts){ //too many attempts, give up
					for(int j=0; j < 5; j++) //really big message, this is a critical failure
						getLog().write("InShot["+programID+"]: ERROR: ****!!!!!***** Tried to reset camera "+config.acquisition.maxPrepAttempts+" times, giving up! ****!!!!!***** ");
					
					getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
					getStatusComm().setPilotStatus(StepStatus.ERROR);			
					
					return false;
				}
				resetCamera(programID);
			}	
		}
		return false; //shouldn't get here
	}
	
	private boolean attemptAcquisitionPrepSingle(String programID, long t0Nano, boolean allowWaitingForAcquire) throws InterruptedException{
		CommonPilotConfig config = getConfig();
		long t0,t1;
		
		ImgSource connectedSource = getConnectedSource();
		AcquisitionDevice acq = (AcquisitionDevice)connectedSource;
	
		//set new shot number
		connectedSource.getCompleteSeriesMetaDataMap().clear();
		connectedSource.setSeriesMetaData("w7x/t0Nano", (Long)t0Nano, false);
		connectedSource.setSeriesMetaData("w7x/programID", programID, false);
		
		
						
		getLog().write("InShot["+programID+"]: Cleared metadata and set");
			
		try{ //Preparation loop
			
			t0 = System.currentTimeMillis();
			t1 = t0;
			//need to wait for cameras not to be busy first, otherwise it might not respond to the capture signal
			getLog().write("InShot["+programID+"]: Waiting for camera to stop being busy, in order to start capture, status = " 
															+ acq.getAcquisitionStatus());
			while(!connectedSource.isIdle() && (t1 - t0) < config.acquisition.cameraAcquireStartTimeoutMS){
				t1=System.currentTimeMillis();
				Thread.sleep(config.busyPollDelayMS);
			}
			
			getLog().write("InShot["+programID+"]: Triggering capture begin (config and allocate)");	
			acq.startCapture();
			t0 = System.currentTimeMillis();
			t1 = t0;
			
			getLog().write("InShot["+programID+"]: Waiting for camera to start, status = " + acq.getAcquisitionStatus());
			//wait for the source to do something
			while(connectedSource.isIdle() && (t1 - t0) < config.acquisition.cameraAcquireStartTimeoutMS){
				//look for errors, but give it a moment to clear any old error state
				if((t1 - t0) > config.busyPollDelayMS && (acq.getAcquisitionStatus() == Status.notInitied || acq.getAcquisitionStatus() == Status.errored))
					throw new RuntimeException("AcquisitionDevice status is " + acq.getAcquisitionStatus());
				t1=System.currentTimeMillis();
				Thread.sleep(config.busyPollDelayMS);
			}
			status = "Camera acquire ready";
			getLog().write("InShot["+programID+"]: Camera module started doing something... (!isIdle), status = " + acq.getAcquisitionStatus());
			
			//The camera module is now busy, but it'll take it a little while to configure the camera
			//until which it won't say it's ready. So wait for it to be ready (or time out)
			t0 = System.currentTimeMillis();
			t1 = t0;				
			while((t1 - t0) < config.acquisition.cameraAcquireStartTimeoutMS){
				if(acq.getAcquisitionStatus() == Status.errored){
					//camera reports an error trying to arm,
					throw new RuntimeException("Camera status is error: " + acq.getAcquisitionStatusString());
					
				}
				if(acq.getAcquisitionStatus() == Status.awaitingHardwareTrigger 
						|| acq.getAcquisitionStatus() == Status.capturing 
						|| (acq.getAcquisitionStatus() == Status.awaitingSoftwareTrigger && allowWaitingForAcquire) ){ 
					//ok, it's ready, drop out of the retry loop
					status = "Camera ready";
					
					getStatusComm().setAcquireStatus(programID, StepStatus.WAITING);					
					getLog().write("InShot["+programID+"]: Acquire ready (running, or armed waiting for S/W or H/W trigger)");					
					return true;  //We're done, all is well
				}
				
				t1=System.currentTimeMillis();
				Thread.sleep(config.busyPollDelayMS);
			}
			
			
		}catch(RuntimeException err){
			getLog().write("InShot["+programID+"]: Caught Exception during camera set-up, dropping through to reset camera: " + err);	

			getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
			err.printStackTrace();
		}
		
		//more hackery for Andor camera at AUG. This should be a general thing ImgSource
		
		//config didn't engage or camera is otherwise not armed, reset it				
		getLog().write("InShot["+programID+"]: ERROR: ** AcquisitionDevice was NOT ready to acquire! **" + acq.getAcquisitionStatusString());
		getLog().write("InShot["+programID+"]: acq.getAcquisitionStatus() = " + acq.getAcquisitionStatus());
		getLog().write("InShot["+programID+"]: acq.getAcquisitionStatusString() = " + acq.getAcquisitionStatusString());
		connectedSource.setSeriesMetaData("inshot/status", "AcquireNotReady", true);
		
		getStatusComm().setAcquireStatus(programID, StepStatus.ERROR);
		
		return false;
	}

	public void awaitCaptureCompletion(String programID) throws InterruptedException {
		CommonPilotConfig config = getConfig();
		long t0, t1; 
		
		if(connectedSource instanceof AcquisitionDevice) {
			AcquisitionDevice acq = (AcquisitionDevice)connectedSource;
			getLog().write("InShot["+programID+"]: Waiting for capture to start...");
			
			t0 = System.currentTimeMillis();
			t1 = t0;
			while(acq.getAcquisitionStatus() != Status.capturing && (t1 - t0) < config.acquisition.cameraCaptureTimeoutMS){
				Thread.sleep(config.busyPollDelayMS);
				t1=System.currentTimeMillis();
			}
			
			getLog().write("InShot["+programID+"]: Capture started, waiting for it to complete...");
			while(acq.getAcquisitionStatus() == Status.capturing && (t1 - t0) < config.acquisition.cameraCaptureTimeoutMS){
				Thread.sleep(config.busyPollDelayMS);
				t1=System.currentTimeMillis();
			}
			
		}else {
			// Old pre acqui dev stuff, probably not used now
			getLog().write("InShot["+programID+"]: Waiting for capture to start... and finish.");
		
			
			//Wait for it to stop acquiring, or for the capture timeout
			t0 = System.currentTimeMillis();
			t1 = t0;
			while(!connectedSource.isIdle() && (t1 - t0) < config.acquisition.cameraCaptureTimeoutMS){
				Thread.sleep(config.busyPollDelayMS);
				t1=System.currentTimeMillis();
			}
		}
			
		if((t1 - t0) >= config.acquisition.cameraCaptureTimeoutMS)
			throw new RuntimeException("Capture took longer than configured timeout ("+config.acquisition.cameraCaptureTimeoutMS+" ms)");
	}

	/** Acquisition system identification/name */
	public String getSystemId(){ return systemID; }

	public abstract void forceShot(String programName, long programStartNanos);

	/** GERI state change notification coming from the FlightController
	 * this is from a GeriPilot of probably a different ImageProc 
		and might be useful if this one doesn't get a trigger 
	 * @param state
	 * @param timestamp
	 */
	protected void broadcastedGeriStateChange(int state, long timestamp) {

	}

	/** Called from FlightControl when shot length is manually set */
	public void setShotLength(double shotLength) {
		CommonPilotConfig config = getConfig();
		if(!config.autoconf.acceptShotLengthFromFlightControl)
			return;
		
		config.autoconf.shotLengthMS = (long)(shotLength * 1000.0);
		calcAutoconfig(true);
		
		updateAllControllers();
	}
	
	private void setAutoconfigStatus(String autoconfigStatus) {
		this.autoconfigStatus = autoconfigStatus;
		updateAllControllers();
	}
	
	public String getAutoconfigStatus() { return autoconfigStatus;	}
	
	/** Calculate the required frame count and/or period according to the auto configuration settings.
	 * @param apply If true, attempt to apply the calculated settings to the acquisition device.
	 */
	public void calcAutoconfig(boolean apply) {
		CommonPilotConfig config = getConfig();
		
		if(config.autoconf.mode == null || config.autoconf.mode == Modes.None) {
			setAutoconfigStatus("Disabled");
			return;
		}

		try {
			if(!(connectedSource instanceof AcquisitionDevice)) 
				throw new RuntimeException("Can not autoconfigure "+connectedSource+", as it is not an AcquisitionDevice");
			
			AcquisitionDevice acq = (AcquisitionDevice)connectedSource;
			
			if(config.autoconf.inhibitUntilSave && (inhibitingAutoconfDuringShot
					|| acq.getAcquisitionStatus() == Status.init 
					|| acq.getAcquisitionStatus() == Status.awaitingHardwareTrigger 
					|| acq.getAcquisitionStatus() == Status.awaitingSoftwareTrigger 
					|| acq.getAcquisitionStatus() == Status.capturing)) {
				logr.fine("Rejected autoconfiguration attempt because we're still in a shot or are acquiring.");				
			}
			
			long framePeriodUS = acq.getFramePeriod();
			int frameCount = acq.getFrameCount();
			
			long targetRunLenMS = config.autoconf.shotLengthMS + config.autoconf.extraLengthMS;
					
			switch(config.autoconf.mode) {
			case FrameCount:
				if(framePeriodUS <= 0)
					return;
				
				frameCount = (int)Math.ceil(targetRunLenMS*1000.0 / framePeriodUS);
				
				if(frameCount > config.autoconf.maxFrameCount)
					frameCount = config.autoconf.maxFrameCount;
				
				
				lastCalculatedRunLength = (int)((frameCount * framePeriodUS) / 1000.0);
				
				setAutoconfigStatus("set frame count = " + frameCount + " --> run length = " + lastCalculatedRunLength + " ms");	
				
				config.autoconf.frameCount = frameCount;
				if(!acq.isAutoconfigAllowed())
					throw new RuntimeException("Autoconfig disabled at source");
				
				acq.setFrameCount(frameCount);
				
								
				break;
				
			case FrameTime:
				if(frameCount <= 0)
					return;
				
				framePeriodUS = targetRunLenMS*1000 / frameCount;
				
				if(framePeriodUS < config.autoconf.minFramePeriodUS) {
					framePeriodUS = config.autoconf.minFramePeriodUS;
					
					//if frameCount was set, it will now be too many
					/*if(config.autoconf.setFrameCount) {
						frameCount = (int)Math.ceil(targetRunLenMS*1000.0 / framePeriodUS);
						
						if(frameCount > config.autoconf.maxFrameCount)
							frameCount = config.autoconf.maxFrameCount;
					}*/
					
				}
				
				lastCalculatedRunLength = (int)((frameCount * framePeriodUS) / 1000.0);
				
				setAutoconfigStatus("Setting frame time = " + (framePeriodUS/1000.0) + " ms --> run length = " + lastCalculatedRunLength + " ms");	
				
				config.autoconf.framePeriodUS = framePeriodUS;
				if(!acq.isAutoconfigAllowed())
					throw new RuntimeException("Autoconfig disabled at source");
				
				acq.setFramePeriod(framePeriodUS);
				
				
				break;
				
			case TableRunLengthLookup:
				
				
				double tab[][] = config.autoconf.tableLookup;
				if(tab == null || tab.length != 4)
					throw new RuntimeException("No defined lookup table or not 4 rows!");
				
				double lenSecs = targetRunLenMS / 1e3;
				double expTimeMS = 0;
				
				frameCount = Integer.MAX_VALUE;
				if(lenSecs < tab[0][tab[0].length-1]) {
									
					Interpolation1D expTimeInterp = new Interpolation1D(tab[0], tab[1], InterpolationMode.LINEAR, ExtrapolationMode.EXCEPTION, 0);
					Interpolation1D frameTimeInterp = new Interpolation1D(tab[0], tab[2], InterpolationMode.LINEAR, ExtrapolationMode.EXCEPTION, 0);
					Interpolation1D frameCountInterp = new Interpolation1D(tab[0], tab[3], InterpolationMode.LINEAR, ExtrapolationMode.EXCEPTION, 0);
					
					
					expTimeMS = expTimeInterp.eval(lenSecs);
					framePeriodUS = (long)(frameTimeInterp.eval(lenSecs) * 1000.0);
					frameCount = (int)frameCountInterp.eval(lenSecs);
					
				}
				
				if(frameCount == Integer.MAX_VALUE || frameCount > config.autoconf.maxFrameCount) {
					frameCount = config.autoconf.maxFrameCount;
					framePeriodUS = (long)(targetRunLenMS * 1000L / frameCount);
					expTimeMS = 0.95 * framePeriodUS / 1000.0; // erm.. made up 					
				}
				
				
	
				config.autoconf.framePeriodUS = framePeriodUS;
				config.autoconf.frameCount = frameCount;
				lastCalculatedRunLength = (int)((frameCount * framePeriodUS) / 1000.0);
				
				setAutoconfigStatus("Setting from table. Frame time = " + (framePeriodUS/1000.0) + " ms, Frame count = " + frameCount + "\n"
						+ "Exposure = " + expTimeMS + " ms\n   --> run length = " + lastCalculatedRunLength + " ms");	
				
				
				if(!acq.isAutoconfigAllowed())
					throw new RuntimeException("Autoconfig disabled at source");
				
				acq.setFramePeriod(framePeriodUS);
				acq.setFrameCount(frameCount);
				try {
					acq.setExposureTime((long) (expTimeMS*1e3));
				}catch(RuntimeException err) {
					logr.log(Level.WARNING, "Can't set exposure time", err);
				}
	
				break;
				
			default:
				throw new RuntimeException("Unknown/unsupported autoconfig mode:" + config.autoconf.mode);
			}
			
			log.write("Auto config set image parameters: shot = " 
					+ (config.autoconf.shotLengthMS / 1000.0) + " s --> "
					+ frameCount + "x " + (framePeriodUS / 1000.0) + " ms"
					+ " = " + (frameCount * framePeriodUS / 1000000.0) + " s run length.");
			
			
		}catch(RuntimeException err) {
			logr.log(Level.WARNING, "ERROR setting autoconfiguration", err);
			setAutoconfigStatus("ERROR:" + err.getMessage());
		}
		
		//other sink modules that want to know the total shot length (controllers etc)
		try {
			
			for(ImgSink sink : connectedSource.getConnectedSinks()) {
				distributeShotLength(sink);
			}
			
		}catch(RuntimeException err) {
			logr.log(Level.WARNING, "ERROR setting shot length to sinks", err);			
		}
		
		
	}
	
	private void distributeShotLength(ImgSink sink) {
		CommonPilotConfig config = getConfig();
		
		if(sink instanceof ShotLengthReceiver) {
			((ShotLengthReceiver)sink).setShotLength(config.autoconf.shotLengthMS);
		}
			
		if(sink instanceof ImgSource) {
			for(ImgSink sink2 : ((ImgSource)sink).getConnectedSinks()) {
				distributeShotLength(sink2);
			}
		}
	}

	public int getLastCalculatedRunLength() { return lastCalculatedRunLength;	}

	/** Create some sensible timebases
	 * inshot/timeFromProgramStart - from programStart
	 * inshot/timeFromT1 - if W7X program (check again here, incase we triggered on something else)
	 * Sets "inshot/time" to the best of these.
	 * Sets "/time" to timeFromT1 if the T1 trigger exists. 
	 * 
	 * @param programStartNano	Nanotime of program start. (e.g. from W7XArchive or GERI INIT timestamp)
	 * @param geriRUNNano 	GERI RUN timestamp if GERI, otherwise 0 or -ve
	 */
	public void createTimebases(long programStartNano, long geriRUNNano) {
		
		//first we need some kind of nanotime from the camera
		long nanoTime[] = W7XArchiveDBSink.getBestNanotimebase(connectedSource);
				
		int nImages = connectedSource.getNumImages();		
		double[] time = new double[nImages];
		
		// timeFromPilotTrigger = time from program start trigger (whatever that was, e.g. W7X program start or NBI start)
		try{
			for(int i=0; i < nImages; i++) {
				time[i] = (double)(nanoTime[i] - programStartNano) / 1e9;
			}			
			connectedSource.setSeriesMetaData("inshot/timeFromPilotTrigger", time, true);
			
			//start with this as the best 'time'			 
			connectedSource.setSeriesMetaData("time", time, true);  //is useful but rubbish timebase
			
		}catch(RuntimeException err){ 
			logr.log(Level.WARNING, "Unable to generate 'inshot/timeFromPilotTrigger': ", err);
		}
		
		if(geriRUNNano > 0) {
			// timeFromGeriRun = time from program start trigger (whatever that was, e.g. W7X program start or NBI start)
			try{
				for(int i=0; i < nImages; i++) {
					time[i] = (double)(nanoTime[i] - geriRUNNano) / 1e9;
				}			
				connectedSource.setSeriesMetaData("inshot/timeFromGeriRun", time, true);
				
				//this is better than timeFromPilotTrigger 
				connectedSource.setSeriesMetaData("time", time, true);  //is an okish timebase
			}catch(RuntimeException err){
				logr.log(Level.WARNING, "Unable to generate 'inshot/timeFromGeriRun': ", err);
			}
		}

		//timeFromT1 = time from T1, taken again now from the program start of anything up to 2 minutes
		//before the start of the first image
		//We use this because the passed in programstart might not be the w7x program trigger 
		// (e.g. NBI trigger) and the database won't yet give us all the proper program information  
		try{
			//long t1 = ArchiveDBFetcher.defaultInstance().getT1TriggerCorrespondingToNano(nanoTime[0]); //not yet available
			long info[] = ArchiveDBFetcher.checkForNewProgramID(nanoTime[0] - 2*60_000_000_000L, 0);
			if(info == null){
				throw new RuntimeException("No program starting in 2 minutes before acquisition start, so no timeFromLastT1");
			}
				
			long progID = info[0];			
			long t0 = info[1];
			long t1 = t0 + 61_000_000_000L; //erm... hardcoded :/
			log.write("Program '"+progID+"' started "
					+ ((double)(nanoTime[0] - t0)  / 1_000_000_000L) + " seconds before acquisition start and "
					+ ((double)(programStartNano - t0) / 1_000_000_000L) + " seconds before program start trigger.");
			
			double[] timeFromT0 = new double[nImages];
			double[] timeFromT1 = new double[nImages];
			
			for(int i=0; i < nImages; i++) {
				timeFromT0[i] = (double)(nanoTime[i] - t0) / 1e9; 
				timeFromT1[i] = (double)(nanoTime[i] - t1) / 1e9; 
			}			
			connectedSource.setSeriesMetaData("inshot/timeFromT0", timeFromT0, true);
			connectedSource.setSeriesMetaData("inshot/timeFromT1", timeFromT1, true);
			connectedSource.setSeriesMetaData("inshot/time", timeFromT1, true); // is the besty timebase we can do here
			
		}catch(RuntimeException err){ 
			log.write("WARNING: Unable to generate 'inshot/timeFromLastT0/1'. Couldn't find a t0 trigger for this time region: " + err.getMessage());			
			logr.log(Level.WARNING, "Unable to generate 'inshot/timeFromLastT0/1'. Couldn't find a t0 trigger for this time region", err);
		}
		
	}
	

	/**
	 * @param skipDatabases Don't process DatabaseSink modules at lowest level (e.g. because its been done already)	
	 * @throws InterruptedException
	 */
	public void processChain(String programID, boolean skipDatabases) throws InterruptedException {
		processChain(connectedSource, programID, skipDatabases);
	}
	
	/**
	 * @param skipDatabases Don't process DatabaseSink modules at lowest level (e.g. because its been done already)	
	 * @throws InterruptedException
	 */
	public void processChain(ImgSource base, String programID, boolean skipDatabases) throws InterruptedException {
		abortProcessing = false;
		log.write("pilot.processing["+programID+"].processChain("+base+")");
		
		/*if(base instanceof SoftwareROIsProcessor) {
			System.out.println("MERP!");
		}*/
		
		CommonPilotConfig config = getConfig();
		
		for(ImgSink sink : base.getConnectedSinks()){
			if(sink == this) //don't process this (the pilot) sink
				continue;
			
			//hack to save parlogs in GeirStreams of processed data
			if(sink instanceof GeriStreamSink && !(base instanceof AcquisitionDevice)) {
				log.write("pilot.processing[\"+programID+\"].ProcessChain: Enabling streaming and writing parlog for GeriStream of processed data: " + base.toShortString() + " --> " + sink.toString());
				GeriStreamSink streamer = (GeriStreamSink)sink;
				streamer.sendMetadata(streamer.getImageProvider().getLatestNanotime());
				streamer.startStreaming();
				continue; //'processing' this makes no sense currently
			}
			
			if(sink instanceof DatabaseSink){
				if(skipDatabases)
					continue; // writers on the lowest level have already been done 
				
				doSinkSave((DatabaseSink)sink, programID);
				
			}
			
			if(!sink.isIdle()){
				log.write("pilot.processing["+programID+"].ProcessChain: BUSY: " + sink + " is already processing, waiting for it to finish");
				//continue;
			}else {
			
				sink.calc(); //start processing
			}
			

			long databaseSaveStartTimeoutMS = 1000;
			long t0 = System.currentTimeMillis();
			long t1 = t0;
			//wait for the sink to do something
			while(sink.isIdle() && (t1 - t0) < databaseSaveStartTimeoutMS){
				t1=System.currentTimeMillis();
				Thread.sleep(config.busyPollDelayMS);
				if(abortProcessing)
					throw new RuntimeException("Processing aborted in processChain()");
			}
			log.write("pilot.processing["+programID+"].ProcessChain: START: " + sink);
			
			//and for it to stop doing it
			while(!sink.isIdle() && (t1 - t0) < config.processing.databaseSaveTimeoutMS){
				t1=System.currentTimeMillis();
				Thread.sleep(config.busyPollDelayMS);
				if(abortProcessing)
					throw new RuntimeException("Processing aborted in processChain()");
			}
			boolean procPipeComplete = sink.wasLastCalcComplete();
			
			if(!procPipeComplete){
				log.write("pilot.processing["+programID+"].ProcessChain: ERROR: " + sink + ". It didn't seem to complete properly. Aborting this chain.");
				sink.abortCalc();
				if(sink instanceof EventReciever) //also trigger a full/chain abort if it supports it
					((EventReciever)sink).event(Event.GlobalAbort);
				Thread.sleep(config.postTriggerDelay);
				if(abortProcessing)
					throw new RuntimeException("Processing aborted in processChain()");
				//if(abortOnProcFailure)
					//throw new 
				continue; //give up on that chain
				
			}else if((t1 - t0) >= config.processing.processingTimeoutMS){
				log.write("InShot.ProcessChain: TIMEOUT: " + sink + ". Aborting this chain.");
				sink.abortCalc();
				if(sink instanceof EventReciever) //also trigger a full/chain abort if it supports it
					((EventReciever)sink).event(Event.GlobalAbort);
				Thread.sleep(config.postTriggerDelay);
				if(abortProcessing)
					throw new RuntimeException("Processing aborted in processChain()");
				if(config.processing.abortProcessingOnModuleTimeout)
					throw new RuntimeException("Processing failed. Aborting");
				//continue; //give up on that chain
			}else{
				log.write("pilot.processing["+programID+"].ProcessChain: COMPLETE: " + sink);
			}
			
			if(sink instanceof ImgProcPipe){
				
				ImgProcPipe pipe = ((ImgProcPipe)sink);
								
				//now do it's children (including database saving, regardless)
				processChain(pipe, programID, false);
				
			}
		}
	}
	
	private String forceProcessSyncObj = "forceProcessSyncObj";
	public void forceProcess(){
		//we might not have our own thread, so run this in it's own processing unit

		abortProcessing = true; //abort any that are running
		//processChain() will not get run until it notices and dies
		
		ImageProcUtil.ensureFinalUpdate(forceProcessSyncObj, new Runnable() {

			@Override
			public void run() {
				try{
					processChain("Forced", false);
					log.write("ForceProcess: Processing complete.");
				}catch(RuntimeException err){
					log.write("Error during processing chain: " + err);
					err.printStackTrace();
				}catch(InterruptedException err){ 
					log.write("Interrupted during processing chain. Aborting it.");
				}
			}
		});
	}

	@Override
	public void command(String command, PrintWriter pwReturn) {
		
		try {
			//separate by ',' '=' or whitespace
			String parts[] = command.split("[\\s=,]");
			
			if(parts[0].equalsIgnoreCase("resetCamera")) {
				log.write("FlightController commanded resetCamera");
				try {
					resetCamera("RemoteTextCommand");
				} catch (InterruptedException e) {
					pwReturn.println("resetCamera interrupted.");
				}
				
			}else if(parts[0].equalsIgnoreCase("checkCameraIsSetup")) {
				log.write("FlightController commanded checkCameraIsSetup("+parts[1]+")");
				checkCameraIsSetup(parts[1]);
			}
				
		}catch(RuntimeException err){
			logr.log(Level.WARNING, "Error processing module text command." + command, err);
			pwReturn.println("ERROR: " + err.toString());
		}
	}	

	public void clearAbortProcessing() { abortProcessing = false;	}
	public void abortProcessing() { abortProcessing = true;	}	
	public boolean isAbortProcessingSignalled() { return abortProcessing; }
	
	protected abstract void doSinkSave(DatabaseSink sink, String programID) throws InterruptedException;
	
	public boolean isInShotDay(){
		CommonPilotConfig config = getConfig();
		Calendar now = Calendar.getInstance();
		
		if(inhibitDay != null){
			if(now.get(Calendar.YEAR) == inhibitDay.get(Calendar.YEAR) &&
				now.get(Calendar.DAY_OF_YEAR) == inhibitDay.get(Calendar.DAY_OF_YEAR)){
					return false; // not today!
			}		
		}
		
		boolean isShotDay;
		switch(now.get(Calendar.DAY_OF_WEEK)){ //not sure the number is reliable
			case Calendar.SUNDAY: isShotDay = config.dayControl.shotDays[0]; break;
			case Calendar.MONDAY: isShotDay = config.dayControl.shotDays[1]; break;
			case Calendar.TUESDAY: isShotDay = config.dayControl.shotDays[2]; break;
			case Calendar.WEDNESDAY: isShotDay = config.dayControl.shotDays[3]; break;
			case Calendar.THURSDAY: isShotDay = config.dayControl.shotDays[4]; break;
			case Calendar.FRIDAY: isShotDay = config.dayControl.shotDays[5]; break;
			case Calendar.SATURDAY: isShotDay = config.dayControl.shotDays[6]; break;
			default:
				throw new RuntimeException("ERROR: Someone invented a new day of the week.");
		}
		return isShotDay;
	}

	public void inhibitToday(){ 	
		inhibitDay = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
		
		log.write("Inhibiting current day " + formatter.format(inhibitDay.getTime()) + " (Assuming not shot day)");		
	}
	
	public void setInhibitingAutoconfDuringShot(boolean inhibitingAutoconfDuringShot) { this.inhibitingAutoconfDuringShot = inhibitingAutoconfDuringShot;	}
	public boolean getInhibitingAutoconfDuringShot(boolean inhibitingAutoconfDuringShot) { return this.inhibitingAutoconfDuringShot;	}
	
}
