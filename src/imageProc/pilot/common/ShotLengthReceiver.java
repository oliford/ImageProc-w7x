package imageProc.pilot.common;

/** Any module that wants to know the shot length from the pilot autoconjfiguration
 * .Often comes from flight control or maybe later from CoDAC announcement server. 
 * @author specgui
 *
 */
public interface ShotLengthReceiver {
	public void setShotLength(long shotLengthMS);

}
